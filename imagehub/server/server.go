package server

import (
	"net/http"
	"net/http/pprof"
	"time"

	"terminus.io/dice/dice/imagehub/conf"
	"terminus.io/dice/dice/imagehub/handlers"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type ImageServer struct {
	router *mux.Router
}

func NewImageServer() *ImageServer {
	router := mux.NewRouter()
	router.UseEncodedPath()
	return &ImageServer{
		router: router,
	}
}

func (s *ImageServer) ListenAndServe() error {

	s.registerHandlers()

	srv := &http.Server{
		Addr:              conf.ImageConf.ListenAddr,
		Handler:           s.router,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		ReadHeaderTimeout: 60 * time.Second,
	}
	err := srv.ListenAndServe()
	if err != nil {
		logrus.Errorf("failed to listen and serve: %v", err)
	}
	return err
}

func (s *ImageServer) registerHandlers() {
	// Register Handlers
	s.register("/api/images", http.MethodPost, handlers.AddImageHandler)
	s.register("/api/images/{imageIdOrImage}", http.MethodGet, handlers.GetImageHandler)
	s.register("/api/images/{imageIdOrImage}", http.MethodPut, handlers.UpdateImageHandler)
	s.register("/api/images/{imageIdOrImage}", http.MethodDelete, handlers.DeleteImageHandler)
	s.register("/api/images", http.MethodGet, handlers.ListImageHandler)
	// Search API Design standard，please refer:https://developer.github.com/v3/search/
	s.register("/api/search/images", http.MethodGet, handlers.SearchImageHandler)

	// Regsiter internal handlers for debug & monitor
	subroute := s.router.PathPrefix("/debug/").Subrouter()
	subroute.HandleFunc("/pprof/profile", pprof.Profile)
	subroute.HandleFunc("/pprof/trace", pprof.Trace)
	subroute.HandleFunc("/pprof/block", pprof.Handler("block").ServeHTTP)
	subroute.HandleFunc("/pprof/heap", pprof.Handler("heap").ServeHTTP)
	subroute.HandleFunc("/pprof/goroutine", pprof.Handler("goroutine").ServeHTTP)
	subroute.HandleFunc("/pprof/threadcreate", pprof.Handler("threadcreate").ServeHTTP)
}

func (s *ImageServer) register(routeName string, httpMethod string, handler func(w http.ResponseWriter, r *http.Request)) {
	s.router.Path(routeName).Methods(httpMethod).HandlerFunc(handler)
}
