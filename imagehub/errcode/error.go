package errcode

type ImageErrorCode string

const (
	NETWORK_ERROR         ImageErrorCode = "IMG0001"
	INTERNAL_SERVER_ERROR ImageErrorCode = "IMG0002"
	PERMISSION_DENIED     ImageErrorCode = "IMG0003"
	RESOURCE_NOT_FOUND    ImageErrorCode = "IMG0004"
	HEADER_MISSING        ImageErrorCode = "IMG0005"
	BODY_MISSING          ImageErrorCode = "IMG0006"
	BODY_INVALID_FORMAT   ImageErrorCode = "IMG0007"
	PARAM_MISSING         ImageErrorCode = "IMG0008"
	PARAM_INVALID_FORMAT  ImageErrorCode = "IMG0009"
	PARAM_INVALID         ImageErrorCode = "IMG0010"
)
