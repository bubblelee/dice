package conf

import (
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	ImageConf *Conf
)

type Conf struct {
	ListenAddr      string
	ClusterURL      string
	DebugMode       bool
	MaxTimeReserved string // Image reverved max time that is not used，eg: 24h, 10m
	GCInterval      int64  // Image garbage collection interval, unit: h
	GCSwitch        bool   // Image garbage collection switch, default: on
	DbInfo          *DbConf
}

// Load env & inititalize db client
func LoadConfig() error {
	listenAddr := os.Getenv("IMAGE_LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = ":10000"
	}

	clusterURL := os.Getenv("CLUSTER_URL")
	if clusterURL == "" {
		clusterURL = "http://dice.colony-officer.marathon.l4lb.thisdcos.directory:9029"
	}

	debugMode := strings.ToLower(os.Getenv("IMAGE_DEBUG_MODE"))
	var debug bool
	if debugMode == "true" || debugMode == "1" {
		debug = true
	}

	maxTimeReverved := os.Getenv("IMAGE_MAX_TIME_RESERVED")
	if maxTimeReverved == "" {
		maxTimeReverved = "168h"
	}

	gcIntervalStr := os.Getenv("IMAGE_GC_INTERVAL")
	if gcIntervalStr == "" {
		gcIntervalStr = "24"
	}
	gcInterval, err := strconv.ParseInt(gcIntervalStr, 10, 64)
	if err != nil {
		logrus.Errorf("%v", err)
		return errors.Errorf("Env IMAGE_GC_INTERVAL can't convert to integer.")
	}

	gcSwitch := true
	gcSwitchStr := strings.ToLower(os.Getenv("IMAGE_GC_SWITCH"))
	if gcSwitchStr == "false" || gcSwitchStr == "0" {
		gcSwitch = false
	}

	// Load db config
	host := os.Getenv("IMAGE_DB_HOST")
	if host == "" {
		logrus.Error("Env IMAGE_DB_HOST not configued.")
		return errors.Errorf("Env IMAGE_DB_HOST not configued.")
	}
	port := os.Getenv("IMAGE_DB_PORT")
	if port == "" {
		logrus.Warn("Env IMAGE_DB_PORT not configued, use default value:3306.")
		port = "3306"
	}
	database := os.Getenv("IMAGE_DB_NAME")
	if database == "" {
		logrus.Error("Env IMAGE_DB_NAME not configued.")
		return errors.Errorf("Env IMAGE_DB_NAME not configued.")
	}
	username := os.Getenv("IMAGE_DB_USERNAME")
	if username == "" {
		logrus.Warn("Env IMAGE_DB_USERNAME not configued, use default value:root.")
		username = "root"
	}
	password := os.Getenv("IMAGE_DB_PASSWORD")
	if password == "" {
		logrus.Error("Env IMAGE_DB_PASSWORD not configued.")
		return errors.Errorf("Env IMAGE_DB_PASSWORD not configued.")
	}

	dbInfo := &DbConf{
		Host:     host,
		Port:     port,
		Database: database,
		Username: username,
		Password: password,
	}
	// Initial Database Client
	if err := initDbClient(dbInfo); err != nil {
		return err
	}

	ImageConf = &Conf{
		ListenAddr:      listenAddr,
		ClusterURL:      clusterURL,
		DebugMode:       debug,
		MaxTimeReserved: maxTimeReverved,
		GCInterval:      gcInterval,
		GCSwitch:        gcSwitch,
		DbInfo:          dbInfo,
	}
	return nil
}
