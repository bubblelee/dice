package conf

import (
	"fmt"
	"time"

	"terminus.io/dice/dice/imagehub/entity"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/sirupsen/logrus"
)

var (
	DbClient *gorm.DB
)

// Database configuration
type DbConf struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}

// Initialize Database Client
func initDbClient(dbConf *DbConf) error {
	url := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConf.Username, dbConf.Password, dbConf.Host, dbConf.Port, dbConf.Database)

	db, err := gorm.Open("mysql", url)
	if err != nil {
		logrus.Errorf("Connecting database error: %v", err)
		return err
	}

	// connection pool
	db.DB().SetMaxIdleConns(10)
	db.DB().SetConnMaxLifetime(200)
	db.DB().SetConnMaxLifetime(time.Hour)

	// ORM
	if !db.HasTable(&entity.Image{}) {
		if err := db.CreateTable(&entity.Image{}).Error; err != nil {
			logrus.Errorf("Create table error: %v", err)
			return err
		}
	}

	DbClient = db

	return nil
}
