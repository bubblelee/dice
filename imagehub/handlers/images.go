package handlers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"terminus.io/dice/dice/imagehub/cache"
	"terminus.io/dice/dice/imagehub/conf"
	"terminus.io/dice/dice/imagehub/entity"
	"terminus.io/dice/dice/imagehub/errcode"
	"terminus.io/dice/dice/imagehub/response"
	"terminus.io/dice/dice/imagehub/types"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// TODO 参数校验优化，增加异常场景处理
func GetImageHandler(w http.ResponseWriter, r *http.Request) {
	_, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	var image entity.Image
	vars := mux.Vars(r)
	imageIdOrImage, err := url.QueryUnescape(vars["imageIdOrImage"])
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID, "ImageId is invalid.")
		return
	}

	idOrImage, err := strconv.ParseInt(imageIdOrImage, 10, 64)
	if err == nil {
		conf.DbClient.Where("id = ?", idOrImage).Where("org_id = ?", orgId).First(&image)
	} else {
		conf.DbClient.Where("image_url = ?", imageIdOrImage).Where("org_id = ?", orgId).First(&image)
	}
	// Check imageId valid
	if image.ID == 0 {
		response.Error(w, http.StatusNotFound, errcode.RESOURCE_NOT_FOUND, "Image not found")
		return
	}

	response.Success(w, image)
}

func AddImageHandler(w http.ResponseWriter, r *http.Request) {
	userId, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	var imageRequest types.ImageRequest
	if r.Body == nil {
		response.Error(w, http.StatusBadRequest, errcode.BODY_MISSING, "Please send a request body")
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&imageRequest); err != nil {
		response.Error(w, http.StatusBadRequest, errcode.BODY_INVALID_FORMAT, "Request body invalid format")
		return
	}

	if userId != imageRequest.OperatorId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "User info can't match")
		return
	}
	if orgId != imageRequest.OrgId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "Org info can't match")
		return
	}

	var image entity.Image
	imageRequest.ToImage(&image)
	image.Status = string(types.CREATED)
	conf.DbClient.Create(&image)

	respBody := make(map[string]interface{})
	respBody["imageId"] = image.ID
	response.Success(w, respBody)
}

func UpdateImageHandler(w http.ResponseWriter, r *http.Request) {
	userId, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	vars := mux.Vars(r)
	var image entity.Image

	imageIdOrImage, err := url.QueryUnescape(vars["imageIdOrImage"])
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID, "ImageId is invalid.")
		return
	}
	idOrImage, err := strconv.ParseInt(imageIdOrImage, 10, 64)
	if err == nil {
		conf.DbClient.Where("id = ?", idOrImage).Where("org_id = ?", orgId).First(&image)
	} else {
		conf.DbClient.Where("image_url = ?", imageIdOrImage).Where("org_id = ?", orgId).First(&image)
	}

	// Check imageId valid
	if image.ID == 0 {
		response.Error(w, http.StatusNotFound, errcode.RESOURCE_NOT_FOUND, "Image not found")
		return
	}

	var imageRequest types.ImageRequest
	if r.Body == nil {
		response.Error(w, http.StatusBadRequest, errcode.BODY_MISSING, "Please send a request body")
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&imageRequest); err != nil {
		response.Error(w, http.StatusBadRequest, errcode.BODY_INVALID_FORMAT, "Request body invalid format")
		return
	}

	if userId != imageRequest.OperatorId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "User info can't match")
		return
	}
	if orgId != imageRequest.OrgId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "Org info can't match")
		return
	}

	if imageRequest.Status != types.DEPLOYED || imageRequest.Status != types.DEPRECATED {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID, "Param status is invalid, available value: [DEPLOYED,DEPRECATED]")
		return
	}

	imageRequest.ToImage(&image)
	conf.DbClient.Save(&image)

	respBody := make(map[string]interface{})
	respBody["msg"] = "update succ"
	response.Success(w, respBody)
}

func DeleteImageHandler(w http.ResponseWriter, r *http.Request) {
	userId, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	vars := mux.Vars(r)
	var image entity.Image

	imageIdOrImage, err := url.QueryUnescape(vars["imageIdOrImage"])
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID, "ImageId is invalid.")
		return
	}
	idOrImage, err := strconv.ParseInt(imageIdOrImage, 10, 64)
	if err == nil {
		conf.DbClient.Where("id = ?", idOrImage).Where("org_id = ?", orgId).First(&image)
	} else {
		conf.DbClient.Where("image_url = ?", imageIdOrImage).Where("org_id = ?", orgId).First(&image)
	}

	// Check imageId valid
	if image.ID == 0 {
		response.Error(w, http.StatusNotFound, errcode.RESOURCE_NOT_FOUND, "Image not found")
		return
	}

	if userId != image.OperatorId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "User info can't match")
		return
	}
	if orgId != image.OrgId {
		response.Error(w, http.StatusForbidden, errcode.PERMISSION_DENIED, "Org info can't match")
		return
	}

	// Delete image manifest
	var registryClient *httpclient.HTTPClient
	if strings.HasPrefix(image.ImageUrl, "https") { // HTTPS
		// TODO https load ca & certificate & privatekey
		registryClient = httpclient.New(httpclient.WithHTTPS())
	} else { // HTTP
		registryClient = httpclient.New()
	}
	registryUrl, err := cache.Urls.GetRegistryUrl(image.ClusterName)
	if err != nil {
		logrus.Errorf("Get registry url error for cluster: %s, (%v)", image.ClusterName, err)
		response.Error(w, http.StatusBadRequest, errcode.NETWORK_ERROR, "Delete manifest error")
		return
	}
	manifestPath := "/v2/" + image.ImageName + "/manifests/" + image.ImageTag
	resp, err := registryClient.Delete(registryUrl).
		Path(manifestPath).
		Header("Content-Type", "application/json").
		Do().
		DiscardBody()
	if err != nil {
		logrus.Errorf("failed to delete manifest: %v", err)
		response.Error(w, http.StatusInternalServerError, errcode.INTERNAL_SERVER_ERROR, "Delete fail error")
		return
	}
	if resp.StatusCode() != http.StatusOK {
		logrus.Errorf("Delete manifest fail, code: %s", resp.StatusCode())
		response.Error(w, http.StatusBadRequest, errcode.NETWORK_ERROR, "Delete manifest error")
		return
	}

	result := conf.DbClient.Where("id = ?", image.ID).Delete(&entity.Image{})
	response.Success(w, result.RowsAffected)
}

func ListImageHandler(w http.ResponseWriter, r *http.Request) {
	_, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	pageSize := r.URL.Query().Get("pageSize")
	if pageSize == "" {
		pageSize = "20"
	}
	pageNum := r.URL.Query().Get("pageNum")
	if pageNum == "" {
		pageNum = "1"
	}

	size, err := strconv.Atoi(pageSize)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID_FORMAT, "Param pageSize invalid format")
		return
	}
	num, err := strconv.Atoi(pageNum)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID_FORMAT, "Param pageNum invalid format")
		return
	}

	var images []entity.Image
	conf.DbClient.Where("org_id = ?", orgId).Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)

	response.Success(w, images)
}

// Usage: /api/search/images?q=app:test
func SearchImageHandler(w http.ResponseWriter, r *http.Request) {
	_, orgId, err := getPermissonHeader(w, r)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.HEADER_MISSING, "Header: [User-ID] or [Org-Id] is missing.")
		return
	}

	query := r.URL.Query().Get("q")

	pageSize := r.URL.Query().Get("pageSize")
	if pageSize == "" {
		pageSize = "20"
	}
	pageNum := r.URL.Query().Get("pageNum")
	if pageNum == "" {
		pageNum = "1"
	}

	size, err := strconv.Atoi(pageSize)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID_FORMAT, "Param pageSize invalid format")
		return
	}
	num, err := strconv.Atoi(pageNum)
	if err != nil {
		response.Error(w, http.StatusBadRequest, errcode.PARAM_INVALID_FORMAT, "Param pageNum invalid format")
		return
	}

	var images []entity.Image
	if query == "" {
		conf.DbClient.Where("org_id = ?", orgId).Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
	} else if strings.Contains(query, ":") {
		list := strings.Split(query, ":")
		keyword := list[0]
		value := list[1]
		switch keyword {
		case "app":
			// 此搜索方式会走全表扫描。 若后续数据较多，可将搜索方式调整为: key + "%
			conf.DbClient.Where("org_id = ?", orgId).Where("app_name LIKE ?", "%"+value+"%").Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
		case "service":
			conf.DbClient.Where("org_id = ?", orgId).Where("service_name LIKE ?", "%"+value+"%").Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
		case "branch":
			conf.DbClient.Where("org_id = ?", orgId).Where("git_branch LIKE ?", "%"+value+"%").Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
		default:
			conf.DbClient.Where("org_id = ?", orgId).Where("image_url LIKE ?", "%"+query+"%").Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
		}
	} else {
		conf.DbClient.Where("org_id = ?", orgId).Where("image_url LIKE ?", "%"+query+"%").Order("updated_at DESC").Offset((num - 1) * size).Limit(size).Find(&images)
	}

	response.Success(w, images)
}

func getPermissonHeader(w http.ResponseWriter, r *http.Request) (userId, orgId int64, err error) {
	uId, err := strconv.ParseInt(r.Header.Get("User-ID"), 10, 64)
	if err != nil {
		logrus.Errorf("Get header:User-ID error, %v", err)
		return 0, 0, err
	}

	oId, err := strconv.ParseInt(r.Header.Get("Org-ID"), 10, 64)
	if err != nil {
		logrus.Errorf("Get header:Org-ID error, %v", err)
		return 0, 0, err
	}

	return uId, oId, nil
}
