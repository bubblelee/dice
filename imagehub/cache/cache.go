package cache

import (
	"net/http"

	"terminus.io/dice/dice/imagehub/conf"
	"terminus.io/dice/dice/imagehub/response"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/sirupsen/logrus"
)

var (
	Urls *UrlCache
)

func init() {
	Urls = &UrlCache{}
}

type UrlCache struct {
	RegistryURL map[string]string
}

func (uc *UrlCache) GetRegistryUrl(clusterName string) (string, error) {
	if _, ok := uc.RegistryURL[clusterName]; ok {
		return uc.RegistryURL[clusterName], nil
	}

	// If registry url doesn't exist, get it from cluster api
	registryClient := httpclient.New()
	var clusterResponse response.ClusterResponse
	path := "/api/clusters/" + clusterName
	resp, err := registryClient.Get(conf.ImageConf.ClusterURL).
		Path(path).
		Header("Content-Type", "application/json").
		Do().
		JSON(&clusterResponse)
	if err != nil {
		logrus.Errorf("Request cluster info error: %v, clusterName: %s", err, clusterName)
		return "", err
	}
	if resp.StatusCode() != http.StatusOK {
		logrus.Errorf("Failed to get cluster info, clusterName: %s, statusCode: %s", clusterName, resp.StatusCode())
		return "", err
	}
	uc.RegistryURL[clusterName] = clusterResponse.Data.URLs["registry"]

	return uc.RegistryURL[clusterName], nil
}
