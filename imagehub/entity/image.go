package entity

type Image struct {
	BaseModel
	ImageName    string `json:"imageName" gorm:"type:varchar(128);not null"` // 镜像名称
	ImageTag     string `json:"imageTag" gorm:"type:varchar(64)"`            // 镜像Tag
	ImageUrl     string `json:"imageUrl" gorm:"not null"`                    // 镜像地址
	Status       string `json:"status" gorm:"type:varchar(20)"`              // 镜像状态
	OrgId        int64  `json:"orgId" gorm:"index:idx_org_id"`               // 所属企业
	ClusterName  string `json:"clusterName" gorm:"type:varchar(40)"`         // 所属集群
	ProjectId    int64  `json:"projectId"`                                   // 所属项目
	AppId        int64  `json:"appId"`                                       // 所属应用
	AppName      string `json:"appName" gorm:"type:varchar(100)"`            // 应用名称
	RuntimeId    int64  `json:"runtimeId"`
	DeploymentId int64  `json:"deploymentId"`
	ServiceName  string `json:"serviceName" gorm:"type:varchar(100)"` // 服务名称
	GitRepo      string `json:"gitRepo"`                              // 镜像构建所用代码仓库地址
	GitBranch    string `json:"gitBranch" gorm:"type:varchar(64)"`
	GitCommitId  string `json:"gitCommitId" gorm:"type:varchar(64)"`
	Env          string `json:"env" gorm:"type:varchar(20)"` // Runtime环境: DEV/TEST/STAGING/PROD
	OperatorId   int64  `json:"operatorId"`
	Operator     string `json:"operator" gorm:"type:varchar(100)"` // 操作用户名称
}

// Set table name
func (Image) TableName() string {
	return "ps_images"
}
