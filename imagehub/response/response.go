package response

import (
	"encoding/json"
	"net/http"

	"terminus.io/dice/dice/colony/officer/models/cluster"
	"terminus.io/dice/dice/imagehub/errcode"

	"github.com/sirupsen/logrus"
)

// 内部约定的Resposne格式, 更多，请参考: https://yuque.antfin-inc.com/terminus_paas_dev/paas/fozwef
type Response struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data,omitempty"`
	Err     ErrorMsg    `json:"err,omitempty"`
}
type ErrorMsg struct {
	Code    string      `json:"code,omitempty"`
	Msg     string      `json:"msg,omitempty"`
	Context interface{} `json:"ctx,omitempty"`
}

func Success(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "applicaton/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusOK)

	response := Response{
		Success: true,
		Data:    data,
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		logrus.Errorf("response parse to json err: %v", err)
	}

	w.Write(jsonResponse)
}

// Error Response
func Error(w http.ResponseWriter, statusCode int, code errcode.ImageErrorCode, errorMsg string) {
	w.Header().Set("Content-Type", "applicaton/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(statusCode)

	response := &Response{
		Success: false,
		Err: ErrorMsg{
			Code: string(code),
			Msg:  errorMsg,
		},
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		logrus.Errorf("response parse to json err: %v", err)
	}

	w.Write(jsonResponse)
}

// Receive cluster info
type ClusterResponse struct {
	Data cluster.Cluster `json:"data"`
}
