package cron

import (
	"net/http"
	"strings"
	"time"

	"terminus.io/dice/dice/imagehub/cache"
	"terminus.io/dice/dice/imagehub/conf"
	"terminus.io/dice/dice/imagehub/entity"
	"terminus.io/dice/dice/imagehub/types"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/sirupsen/logrus"
)

func ImageGCCron() {

	d := time.Duration(conf.ImageConf.GCInterval) * time.Hour // GC internal

	location, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		logrus.Panic(err)
	}
	t, err := time.ParseInLocation("15:04:05", "02:00:00", location)
	if err != nil {
		logrus.Panic(err)
	}

	now := time.Now()
	t = time.Date(now.Year(), now.Month(), now.Day(), t.Hour(), t.Minute(), t.Second(), 0, location)
	if now.After(t) {
		t = t.Add((now.Sub(t)/d + 1) * d)
	}

	c := time.Tick(t.Sub(now))
	go func() {
		for range c {
			removeDeprecatedImages(time.Now())
		}
	}()
}

func removeDeprecatedImages(now time.Time) {
	d, _ := time.ParseDuration("-" + conf.ImageConf.MaxTimeReserved) // one week before, eg: -168h
	before := now.Add(d)

	var images []entity.Image
	conf.DbClient.Not("status = ?", types.DEPLOYED).Where("updated_at < ?", before).Find(&images)
	for _, image := range images {
		logrus.Infof("[Cron]Deleting image: %v", image)
		// Delete image manifest
		var registryClient *httpclient.HTTPClient
		if strings.HasPrefix(image.ImageUrl, "https") { // HTTPS
			// TODO https load ca & certificate & privatekey
			registryClient = httpclient.New(httpclient.WithHTTPS())
		} else { // HTTP
			registryClient = httpclient.New()
		}
		registryUrl, err := cache.Urls.GetRegistryUrl(image.ClusterName)
		if err != nil {
			logrus.Errorf("Get registry url error for cluster: %s, (%v)", image.ClusterName, err)
			continue
		}
		manifestPath := "/v2/" + image.ImageName + "/manifests/" + image.ImageTag
		resp, err := registryClient.Delete(registryUrl).
			Path(manifestPath).
			Header("Content-Type", "application/json").
			Do().
			DiscardBody()
		if err != nil {
			logrus.Errorf("failed to delete manifest: %v", err)
			continue
		}
		if resp.StatusCode() != http.StatusOK {
			logrus.Errorf("Delete manifest fail, code: %s", resp.StatusCode())
			continue
		}

		// Delete image metadata
		conf.DbClient.Where("id = ?", image.ID).Delete(&entity.Image{})
	}
}
