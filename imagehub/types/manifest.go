package types

// Docker Registry manifest V2, Schema 2 format
// more, please refer: https://docs.docker.com/registry/spec/manifest-v2-2/
type Maniest struct {
	SchemaVersion string           `json:"schemaVersion"`
	MediaType     string           `json:"mediaType"`
	Config        ManifestConfig   `json:"config"`
	Layers        []ManifestConfig `json:"layers"`
}

type ManifestConfig struct {
	MediaType string `json:"mediaType"`
	Size      int64  `json:"size"`
	Digest    string `json:"digest"`
}
