// For http request & response
package types

import (
	"terminus.io/dice/dice/imagehub/entity"
)

type ImageStatus string
type RuntimeEnv string

const (
	CREATED    ImageStatus = "CREATED"    // Pipeline推至registry，但还未被部署
	DEPLOYED   ImageStatus = "DEPLOYED"   // 已被部署使用
	DEPRECATED ImageStatus = "DEPRECATED" // 部署后，服务已删除

	DEV     RuntimeEnv = "DEV"
	TEST    RuntimeEnv = "TEST"
	STAGING RuntimeEnv = "STAGING"
	PROD    RuntimeEnv = "PROD"
)

// CI/Deploy回调时请求体
type ImageRequest struct {
	ID           int64       `json:"id,omitempty"`
	ImageName    string      `json:"imageName"`
	ImageTag     string      `json:"imageTag"`
	ImageUrl     string      `json:"imageUrl"`
	Status       ImageStatus `json:"status,omitempty"`
	OrgId        int64       `json:"orgId"`
	ClusterName  string      `json:"clusterName"`
	ProjectId    int64       `json:"projectId"`
	AppId        int64       `json:"appId"`
	AppName      string      `json:"appName"`
	RuntimeId    int64       `json:"runtimeId,omitempty"`
	DeploymentId int64       `json:"deploymentId,omitempty"`
	ServiceName  string      `json:"serviceName,omitempty"`
	GitRepo      string      `json:"gitRepo,omitempty"`
	GitBranch    string      `json:"gitBranch,omitempty"`
	GitCommitId  string      `json:"gitCommitId,omitempty"`
	Env          RuntimeEnv  `json:"env,omitempty"`
	OperatorId   int64       `json:"operatorId"`
	Operator     string      `json:"operator"`
}

func (ir *ImageRequest) ToImage(image *entity.Image) {
	image.ImageName = ir.ImageName
	image.ImageTag = ir.ImageTag
	image.ImageUrl = ir.ImageUrl
	image.Status = string(ir.Status)
	image.OrgId = ir.OrgId
	image.ClusterName = ir.ClusterName
	image.ProjectId = ir.ProjectId
	image.AppId = ir.AppId
	image.AppName = ir.AppName
	image.RuntimeId = ir.RuntimeId
	image.DeploymentId = ir.DeploymentId
	image.ServiceName = ir.ServiceName
	image.GitRepo = ir.GitRepo
	image.GitBranch = ir.GitBranch
	image.GitCommitId = ir.GitCommitId
	image.Env = string(ir.Env)
	image.OperatorId = ir.OperatorId
	image.Operator = ir.Operator
}
