package main

import (
	"terminus.io/dice/dice/imagehub/conf"
	"terminus.io/dice/dice/imagehub/cron"
	"terminus.io/dice/dice/imagehub/server"
	"terminus.io/dice/dice/pkg/dumpstack"
	"terminus.io/dice/dice/pkg/version"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.Infof(version.String())
	dumpstack.Open()

	// Load configuration & initialize db client
	if err := conf.LoadConfig(); err != nil {
		panic(err)
	}

	// Image garbage collection at interval
	if conf.ImageConf.GCSwitch { // default: true
		cron.ImageGCCron()
	}

	if conf.ImageConf.DebugMode {
		logrus.SetLevel(logrus.DebugLevel)
	}
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:     false,
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05.000000000",
	})

	srv := server.NewImageServer()
	if err := srv.ListenAndServe(); err != nil {
		panic(err)
	}
}
