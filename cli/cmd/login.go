package cmd

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/exec"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/status"
)

var LOGIN = Command{
	Name:      "login",
	ShortHelp: "用户登陆",
	Run:       RunLogin,
}

func RunLogin(ctx *Context) error {
	username := inputNormal("username: ")
	password := inputPWD("password: ")
	v := make(url.Values)
	v.Set("username", username)
	v.Set("password", password)

	var body bytes.Buffer
	res, err := ctx.HttpClient.Post(ctx.Conf.OPENAPIServerAddr).
		Path("/login").
		FormBody(v).Do().Body(&body)
	if err != nil {
		return fmt.Errorf("login fail: %v\n", err)
	}
	if res.StatusCode() == 401 {
		return errors.New("login: auth fail")
	}
	if !res.IsOK() {
		return fmt.Errorf("login fail: response code: %d\n", res.StatusCode())
	}
	var s status.StatusInfo
	d := json.NewDecoder(&body)
	if err := d.Decode(&s); err != nil {
		return fmt.Errorf("login: decode login response fail: %v", err)
	}

	if err := status.StoreStatus(s); err != nil {
		return fmt.Errorf("login: StoreSession fail: %v", err)
	}

	fmt.Println("login: success")
	return nil

}

func inputPWD(prompt string) string {
	cmd := exec.Command("stty", "-echo")
	cmd.Stdin = os.Stdin
	if err := cmd.Run(); err != nil {
		panic(err)
	}
	defer func() {
		cmd := exec.Command("stty", "echo")
		cmd.Stdin = os.Stdin
		if err := cmd.Run(); err != nil {
			panic(err)
		}
		fmt.Println("")
	}()
	return inputNormal(prompt)
}

func inputNormal(prompt string) string {
	fmt.Printf(prompt)
	r := bufio.NewReader(os.Stdin)
	input, err := r.ReadString('\n')
	if err != nil {
		panic(err)
	}
	return input[:len(input)-1]
}
