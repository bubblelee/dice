/*
webhook 命名：
1. /<orgid>/<projectid>/<app-id>/<hook-name>
2. /<orgid>/<projectid>/<hook-name>
3. /<orgid>/<hook-name>
*/
package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/pkg/terminal/table"
)

var WEBHOOK = Command{
	Name:      "webhook",
	ShortHelp: "webhook list",
	Run:       RunWebhook,
}

func RunWebhook(ctx *Context) error {
	orgID, err := ctx.CurrentOrg()
	if err != nil {
		return fmt.Errorf("CurrentOrg: %v", err)
	}
	projects, err := ctx.CurrentProjects()
	if err != nil {
		return fmt.Errorf("CurrentProjects: %v", err)
	}
	apps, err := ctx.CurrentApplications("", true)
	if err != nil {
		return fmt.Errorf("CurrentApplications: %v", err)
	}

	hooks, err := getWebhooks(ctx, "/"+strconv.Itoa(orgID)+"/")
	if err != nil {
		return err
	}
	data := [][]string{}
	for name := range hooks {
		trimName := strings.Trim(name, "/")
		parts := strings.Split(trimName, "/")
		switch len(parts) {
		case 2: // /<org-id>/<hook-name>
			data = append(data, []string{"", "", parts[1]})
		case 3: // /<org-id>/<project-id>/<hook-name>
			pid, err := strconv.Atoi(parts[1])
			if err != nil {
				data = append(data, []string{parts[1], "", parts[2]})
				break
			}
			p := projects.FindByID(pid)
			if p == nil {
				data = append(data, []string{parts[1], "", parts[2]})
				break
			}
			data = append(data, []string{p.Name + "(" + parts[1] + ")", "", parts[2]})
		case 4: // /<org-id>/<project-id>/<app-id>/<hook-name>
			pid, err := strconv.Atoi(parts[1])
			if err != nil {
				data = append(data, []string{parts[1], parts[2], parts[3]})
				break
			}
			p := projects.FindByID(pid)
			if p == nil {
				data = append(data, []string{parts[1], parts[2], parts[3]})
				break
			}
			appid, err := strconv.Atoi(parts[2])
			if err != nil {
				data = append(data, []string{parts[1], parts[2], parts[3]})
				break
			}
			app := apps.FindByID(appid)
			if app == nil {
				data = append(data, []string{parts[1], parts[2], parts[3]})
				break
			}
			data = append(data, []string{p.Name + "(" + parts[1] + ")", app.Name + "(" + parts[2] + ")", parts[3]})
		default:
			data = append(data, []string{"", "", name})
		}
	}

	header := []string{"Project", "Application", "Hook"}
	return table.NewTable().Header(header).Data(data).Flush()
}

func getWebhooks(ctx *Context, key string) (map[string]map[string]interface{}, error) {
	var body bytes.Buffer
	r, err := ctx.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Path("/api/eventbox/webhook").
		Param("key", key).
		Header("Accept", "application/json").
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: ctx.StatusInfo.SessionID}).
		Do().Body(&body)
	if err != nil {
		return nil, err
	}
	if !r.IsOK() {
		return nil, fmt.Errorf("getWebhooks: statuscode: %d", r.StatusCode())
	}
	var whs map[string]map[string]interface{}
	if err := json.Unmarshal(body.Bytes(), &whs); err != nil {
		return nil, err
	}
	return whs, nil
}
