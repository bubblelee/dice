package cmd

import (
	"os"
	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/parser/diceyml"
)

var PARSE = Command{
	Name:      "parse",
	ShortHelp: "解析dice.yml, 合并(merge)不同环境内容",
	LongHelp:  "如果没有 --dev, --test, --staging, --prod 这些flag， 那么不做merge",
	Flags: []Flag{
		StringFlag{"f", "file", "dice-yml path ", ""},
		StringFlag{"s", "str", "dice-yml content", ""},
		BoolFlag{"", "dev", "merge dev yml", false},
		BoolFlag{"", "test", "merge test yml", false},
		BoolFlag{"", "staging", "merge staging yml", false},
		BoolFlag{"", "prod", "merge production yml", false},
		BoolFlag{"", "yml", "output yml or json?", false},
	},
	Run: RunParse,
}

func RunParse(ctx *Context, ymlPath string, ymlContent string, dev, test, staging, prod, outputYml bool) error {
	var yml []byte
	var err error
	if ymlPath != "" {
		yml, err = readYml(ymlPath)
		if err != nil {
			return err
		}
	} else if ymlContent != "" {
		yml = []byte(ymlContent)
	} else {
		ymlPath, err = ctx.DiceYml()
		if err != nil {
			return err
		}
		yml, err = readYml(ymlPath)
		if err != nil {
			return err
		}
	}
	dyml, err := diceyml.New(yml, true)
	if err != nil {
		return err
	}
	if dev {
		if err := dyml.MergeEnv("development"); err != nil {
			return err
		}
	} else if test {
		if err := dyml.MergeEnv("test"); err != nil {
			return err
		}
	} else if staging {
		if err := dyml.MergeEnv("staging"); err != nil {
			return err
		}
	} else if prod {
		if err := dyml.MergeEnv("production"); err != nil {
			return err
		}
	}
	var res string
	if outputYml {
		res, err = dyml.YAML()
		if err != nil {
			return err
		}
	} else {
		res, err = dyml.JSON()
		if err != nil {
			return err
		}
	}
	os.Stdout.WriteString(res)
	return nil
}
