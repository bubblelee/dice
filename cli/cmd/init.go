package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/dicedir"
)

var INIT = Command{
	Name:      "init",
	ShortHelp: "在当前目录下初始化 dice 项目",
	LongHelp: strings.TrimSpace(`
1. 当前目录下创建 .dice 目录
2. 在.dice目录中创建 dice.yml`),
	Run: RunInit,
}

func RunInit(ctx *Context) error {
	if f, err := os.Stat(dicedir.ProjectDiceDir); err == nil && f.IsDir() {
		return fmt.Errorf("reinitialized existing dice project")
	}

	pdir, err := dicedir.CreateProjectDiceDir()
	if err != nil {
		return err
	}
	diceYmls := []string{
		filepath.Join(pdir, "dice.yml"),
	}
	for _, diceYml := range diceYmls {
		f, err := os.OpenFile(diceYml, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
		if err != nil {
			return fmt.Errorf("createfile %s fail", diceYml)
		}
		_, err = f.WriteString(diceymlTemplate)
		if err != nil {
			return fmt.Errorf("write [%s] fail: %v", diceYml, err)
		}
	}
	fmt.Println("init dice project success")
	return nil
}

var diceymlTemplate = strings.TrimSpace(`
# see also: https://yuque.antfin-inc.com/docs/share/c64fafbb-b38e-4b5f-b06a-e4cc07918cc2

version: 2.0

# envs: 环境变量
# 可选：YES
# 说明：此处的 envs 设置全局环境变量，全局环境变量将被下文定义的所有 services 继承；环境变量采用 "key: value" 形式定义，
# 全局环境并不是必须的。
envs:
  # EXAMPLE_ENV: true

# services: 服务
# 可选：NO
# 说明：services 用于定义一个应用里的所有 service，一个正常应用至少需要定义一个 service。
services:

  # showcase-front 是一个 service 的名字，你需要根据自己的服务在这里填写正确的名字
  showcase-front:

    # image: Docker 镜像
    # 可选: YES
    # 说明：如果使用 Dice 的 CI 平台来构建镜像、一键部署，那么此处就不需要填镜像。
    image: nginx:latest

    # cmd: 服务启动命令
    # 可选：YES
    cmd: # ./start.sh

    # ports: 服务监控端口
    # 可选：NO
    # 说明：可以配置多个端口
    ports:
      - 8080

    # expose: 服务导出
    # 可选：YES
    # 说明：服务需要被导出的时候，就需要写上 expose 描述，expose 只能导出 80 和 443 端口，也就是服务最终被用户访问的是
    # 80 或者 443 端口。一般需要被导出的服务都是终端服务，也就是直接面向用户访问的服务，后端服务一般来说不需要导出。
    # expose 端口的流量会被转发到 ports 里的第一个端口上。
    expose:
      - 80
      - 443

    # envs: 环境变量
    # 可选：YES
    # 说明：service 内的 envs 用于定义服务私有的环境变量，可以覆盖全局环境变量中的定义。
    envs:
      # TERMINUS_TRACE_ENABLE: false
      # TERMINUS_APP_NAME: showcase-front-app

    # hosts：host 绑定
    # 可选：YES
    # 说明：hosts 配置将被写入 /etc/hosts 文件。
    hosts:
      # - 127.0.0.1 www.terminus.io

    # resources: 资源
    # 可选：NO
    resources:
      cpu: 0.2
      # 单位 MB
      mem: 256
      # 单位 MB
      disk: 512

    # volumes:
    # 可选：YES
    # 说明：
    volumes:
      # - /home/admin/logs

    # deployments: 部署
    # 可选：NO
    # 说明：deployments 定义服务的部署配置
    deployments:

      # replicas: 实例数
      # 可选：NO
      # 说明：replicas 不能小于 1，replicas 定义了服务需要被部署几个容器实例。
      replicas: 1

      # policys: 部署策略
      # 可选：YES
      # 说明：可配置的值只有如下三种：shuffle, affinity, unique；shuffle 表示打散实例，避免部署在同一宿主机上，shuffle 也是
      # 默认值，一般都不需要配置此项；affinity 表示实例采用亲和部署，也就是部署到同机上；unique 表示唯一部署，也就是一个宿主机
      # 只能部署一个实例。
      policys: shuffle

      # labels: 部署标签
      # 可选：YES
      labels:
        a: b

    # depends_on: 服务依赖
    # 可选：YES
    # 说明：depends_on 用于描述一个应用里的服务相互依赖关系，被依赖的 service 必须被定义，最终所有 service 不能
    # 出现循环依赖。
    # depends_on:
    #   - another-service

    # health_check: 健康检查
    # 可选：NO
    # 说明：支持2种方式: http, exec
    health_check:
      # 可选：YES
      http:
        port: 80
        path: /status
        duration: 120
      # 可选：YES
      exec:
        cmd: curl http:127.0.0.1:7070/status
        duration: 120
# addons: 附加资源
# 可选：YES
# 说明：
addons:
    # mysql:
    #   # plan:
    #   # <addon类型>:<规格>
    #   # 可选规格:
    #   # 1. basic
    #   # 2. professional
    #   # 3. ultimate
    #   plan: mysql:basic
    #   # 各种 addon 有不同的 option
    #   options:
    #     version: 5.7.23
    #     create_dbs: blog,blog2

# 以下字段描述各个环境各自配置
environments:
  development:
  test:
  staging:
  production:

`)
