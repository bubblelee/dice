package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/parser/diceyml"
)

var COMPOSE = Command{
	Name:      "compose",
	ShortHelp: "将 base.yml 和 env.yml 组合， 生成yml",
	LongHelp:  "默认从.dice目录中获取相关yaml文件",
	Flags: []Flag{
		BoolFlag{"", "dev", "开发环境", false},
		BoolFlag{"", "test", "测试环境", false},
		BoolFlag{"", "staging", "预发环境", false},
		BoolFlag{"", "prod", "生产环境", false},
		BoolFlag{"P", "persistent", "同时写入dice.yml中", false},
	},
	Run: RunCompose,
}

func RunCompose(ctx *Context, dev, test, staging, prod, persistent bool) error {
	basePath, err := ctx.DiceYml()
	if err != nil {
		return err
	}
	yml, err := readYml(basePath)
	if err != nil {
		return err
	}
	base, err := diceyml.New(yml, true)
	if err != nil {
		return err
	}
	if dev {
		devf, err := ctx.DevDiceYml()
		if err != nil {
			return err
		}
		yml, err := readYml(devf)
		if err != nil {
			return err
		}
		env, err := diceyml.New(yml, false)
		if err != nil {
			return err
		}
		if err := base.Compose("development", env); err != nil {
			return fmt.Errorf("compose fail:%v\n", err)
		}
	}
	if test {
		testf, err := ctx.TestDiceYml()
		if err != nil {
			return err
		}
		yml, err := readYml(testf)
		if err != nil {
			return err
		}
		env, err := diceyml.New(yml, false)
		if err != nil {
			return err
		}
		if err := base.Compose("test", env); err != nil {
			return fmt.Errorf("compose fail:%v\n", err)
		}
	}
	if staging {
		stagingf, err := ctx.StagingDiceYml()
		if err != nil {
			return err
		}
		yml, err := readYml(stagingf)
		if err != nil {
			return err
		}
		env, err := diceyml.New(yml, false)
		if err != nil {
			return err
		}
		if err := base.Compose("staging", env); err != nil {
			return fmt.Errorf("compose fail:%v\n", err)
		}
	}
	if prod {
		prodf, err := ctx.ProdDiceYml()
		if err != nil {
			return err
		}
		yml, err := readYml(prodf)
		if err != nil {
			return err
		}
		env, err := diceyml.New(yml, false)
		if err != nil {
			return err
		}
		if err := base.Compose("production", env); err != nil {
			return fmt.Errorf("compose fail:%v\n", err)
		}
	}
	res, err := base.YAML()
	if err != nil {
		return fmt.Errorf("convert to yaml fail: %v\n", err)
	}
	os.Stdout.WriteString(res)
	if persistent {
		diceyml, err := ctx.DiceYml()
		if err != nil {
			return err
		}
		f, err := os.OpenFile(diceyml, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
		if err != nil {
			return err
		}
		f.WriteString(res)
	}
	return nil
}

func readYml(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("failed to open %s\n", path)
	}
	content, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("failed to read file %s\n", path)
	}
	return content, nil
}
