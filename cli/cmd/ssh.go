package cmd

import (
	"errors"
	"strings"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/colony/soldier/client"
)

var SSH = Command{
	Name:      "ssh",
	ShortHelp: "在集群节点执行命令",
	Args: []Arg{
		StringArg{}.Name("cluster_id|cluster_name"),
		StringArg{}.Name("[user@]host"),
	},
	Flags: []Flag{
		IntFlag{"p", "port", "port to connect to on the node", 22},
	},
	Run: RunSSH,
}

func RunSSH(ctx *Context, clusterIDOrName string, host string, port int) error {
	if port < 1 || port > 65535 {
		return errors.New("invalid port")
	}

	u, err := GetColonySoldierURL(ctx, clusterIDOrName, ctx.StatusInfo)
	if err != nil {
		return err
	}
	user := "root"
	if i := strings.IndexByte(host, '@'); i != -1 {
		user = host[:i]
		host = host[i+1:]
	}
	if user == "" {
		return errors.New("user required")
	}
	if host == "" {
		return errors.New("host required")
	}

	a := client.Action{
		Name: "ssh",
		Args: client.ActionSSH{
			Host: host,
			Port: port,
			User: user,
		},
	}
	a.Run(u)
	return nil

}
