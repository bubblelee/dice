package cmd

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/status"
	"terminus.io/dice/dice/colony/soldier/client"
)

var DOCKER = Command{
	Name:      "docker",
	ShortHelp: "在集群容器执行命令",
	Args: []Arg{
		StringArg{}.Name("cluster_id|cluster_name"),
		StringArg{}.Name("container_id|container_ip"),
	},
	Flags: []Flag{
		StringFlag{"", "host", "dockerd host", ""},
		IntFlag{"p", "port", "dockerd port", 2375},
	},
	Run: RunDocker,
}

func RunDocker(ctx *Context, clusterIDOrName string, containerIDOrIP string, host string, port int) error {
	if port < 1 || port > 65535 {
		return errors.New("invalid port")
	}
	u, err := GetColonySoldierURL(ctx, clusterIDOrName, ctx.StatusInfo)
	if err != nil {
		return err
	}
	a := client.Action{
		Name: "docker",
		Args: client.ActionDocker{
			Host:      host,
			Port:      port,
			Container: containerIDOrIP,
		},
	}
	a.Run(u)
	return nil

}

func GetColonySoldierURL(ctx *Context, clusterIDOrName string, info status.StatusInfo) (string, error) {
	if strings.HasPrefix(clusterIDOrName, "http://") ||
		strings.HasPrefix(clusterIDOrName, "https://") ||
		strings.HasPrefix(clusterIDOrName, "ws://") ||
		strings.HasPrefix(clusterIDOrName, "wss://") {
		return clusterIDOrName, nil
	}

	var body bytes.Buffer
	r, err := ctx.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: info.SessionID}).
		Path("/api/clusters/" + clusterIDOrName).Do().Body(&body)
	if err != nil {
		return "", fmt.Errorf("get cluster detail fail: %s", err.Error())
	}
	if !r.IsOK() {
		return "", fmt.Errorf("get cluster detail fail: status code: %d", r.StatusCode())
	}

	var response struct {
		Err *struct {
			Code string `json:"code"`
			Msg  string `json:"msg"`
		} `json:"err"`
		Data *struct {
			ID   string
			Name string
			URLs *struct {
				ColonySoldier string `json:"colonySoldier"`
			} `json:"urls"`
		} `json:"data"`
	}
	d := json.NewDecoder(&body)
	if err := d.Decode(&response); err != nil {
		return "", fmt.Errorf("get cluster detail fail: decode response fail: %s", err.Error())
	}
	if response.Err != nil {
		return "", fmt.Errorf("get cluster detail fail: code: %s, msg: %s",
			response.Err.Code, response.Err.Msg)
	}
	if response.Data == nil || response.Data.URLs == nil || response.Data.URLs.ColonySoldier == "" {
		return "", fmt.Errorf("get cluster detail fail: no colony soldier url")
	}

	return response.Data.URLs.ColonySoldier, nil
}
