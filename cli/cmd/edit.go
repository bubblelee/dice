package cmd

import (
	"fmt"
	"os"
	"os/exec"

	. "terminus.io/dice/dice/cli/command"
)

var EDIT = Command{
	Name:      "edit",
	ShortHelp: "编辑dice.yml文件",
	Flags: []Flag{
		BoolFlag{"", "dev", "开发环境", false},
		BoolFlag{"", "test", "测试环境", false},
		BoolFlag{"", "staging", "预发环境", false},
		BoolFlag{"", "prod", "生产环境", false},
	},
	Run: RunEdit,
}

func RunEdit(ctx *Context, dev, test, staging, prod bool) error {
	var ymlPath string
	var err error
	if dev {
		ymlPath, err = ctx.DevDiceYml()
		if err != nil {
			return fmt.Errorf("not found dice development yml")
		}
	} else if test {
		ymlPath, err = ctx.TestDiceYml()
		if err != nil {
			return fmt.Errorf("not found dice test yml")
		}
	} else if staging {
		ymlPath, err = ctx.StagingDiceYml()
		if err != nil {
			return fmt.Errorf("not found dice staging yml")
		}
	} else if prod {
		ymlPath, err = ctx.ProdDiceYml()
		if err != nil {
			return fmt.Errorf("not found dice production yml")
		}
	} else {
		ymlPath, err = ctx.DiceYml()
		if err != nil {
			return fmt.Errorf("not found dice.yml")
		}
	}
	editor, ok := os.LookupEnv("EDITOR")
	if !ok || editor == "" {
		if _, err := exec.LookPath("vim"); err == nil {
			editor = "vim"
		} else {
			editor = "vi"
		}
	}
	originf, err := os.Stat(ymlPath)
	if err != nil {
		originf = nil
	}
	cmd := exec.Command(editor, ymlPath)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	f, err := os.Stat(ymlPath)
	if err != nil {
		return fmt.Errorf("not found [%s]: %v", ymlPath, err)
	}
	if originf == nil || f.ModTime().After(originf.ModTime()) {
		fmt.Printf("edited [%s]\n", ymlPath)
	}
	return err
}
