package cmd

import (
	"fmt"

	. "terminus.io/dice/dice/cli/command"
)

var STATUS = Command{
	Name:      "status",
	ShortHelp: "当前用户信息",
	Run:       RunStatus,
}

func RunStatus(ctx *Context) error {
	info := ctx.StatusInfo
	fmt.Printf(`Name: %s
ID: %d
Session: %s
`, info.NickName, info.ID, info.SessionID)
	return nil

}
