package cmd

import (
	"fmt"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/pkg/version"
)

var VERSION = Command{
	Name: "version",
	Run:  RunVersion,
}

// TODO: add openapi version
func RunVersion(ctx *Context) error {
	fmt.Println("dice client:")
	fmt.Println(version.String())
	fmt.Println("")
	fmt.Printf("OPENAPI: %s\n", ctx.Conf.OPENAPIServerAddr)
	return nil
}
