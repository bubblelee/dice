package cmd

import (
	"strconv"

	. "terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/pkg/terminal/table"

	"github.com/pkg/errors"
)

var LIST = Command{
	Name:      "ls",
	ShortHelp: "list projects and applications",
	Args: []Arg{
		StringArg{}.Name("project").Option(),
	},
	Flags: []Flag{
		BoolFlag{"a", "all", "列出所有应用（application）", false},
	},
	Run: RunList,
}

func RunList(ctx *Context, project string, all bool) error {
	var apps Applications
	var projects Projects
	var err error
	if all || project != "" { // list project app
		if apps, err = ctx.CurrentApplications(project, all); err != nil {
			return errors.Wrap(err, "ls")
		}
	} else { // list all projects
		if projects, err = ctx.CurrentProjects(); err != nil {
			return errors.Wrap(err, "ls")
		}

	}
	if projects != nil {
		data := [][]string{}
		for _, p := range projects {
			data = append(data, []string{strconv.Itoa(p.ID), p.Name, strconv.Itoa(p.Stats.CountApplications), strconv.Itoa(p.Stats.CountRuntimes), strconv.Itoa(p.Stats.CountMembers), strconv.Itoa(p.OrgID), p.Desc})
		}

		return table.NewTable().Header([]string{"project ID", "project name", "app num", "runtime num", "member num", "org ID", "desc"}).Data(data).Flush()
	}
	if apps != nil {
		data := [][]string{}
		for _, app := range apps {
			data = append(data, []string{strconv.Itoa(app.ID), app.Name, strconv.Itoa(app.OrgID), strconv.Itoa(app.ProjectID), app.Desc})
		}
		return table.NewTable().Header([]string{"app id", "app name", "org ID", "project ID", "desc"}).Data(data).Flush()
	}
	return nil
}
