package cmd

import (
	"fmt"
	"os"
	"strings"

	. "terminus.io/dice/dice/cli/command"
	v2 "terminus.io/dice/dice/parser/diceyml"
	"terminus.io/dice/dice/parser/migrate"

	yaml "gopkg.in/yaml.v2"
)

var MIGRATE = Command{
	Name:      "migrate",
	ShortHelp: "由 v1 版本 dice.yml 生成 v2 版本",
	Args: []Arg{
		StringArg{}.Name("v1-dice-yml"),
	},
	Run: RunMigrate,
}

func RunMigrate(ctx *Context, v1ymlPath string) error {
	v1yml, err := os.Open(v1ymlPath)
	if err != nil {
		return fmt.Errorf("open %s failed\n", v1yml)
	}
	mr, err := migrate.Migrate(v1yml)
	if err != nil {
		return fmt.Errorf("migrate failed: %v\n", err)
	}

	diceyml, err := ctx.DiceYml()
	if err != nil {
		return fmt.Errorf("需要先在项目根目录执行 [dice init]")
	}
	devdiceyml, _ := ctx.DevDiceYml()
	testdiceyml, _ := ctx.TestDiceYml()
	stagingdiceyml, _ := ctx.StagingDiceYml()
	proddiceyml, _ := ctx.ProdDiceYml()

	output := map[string]v2.Object{diceyml: mr.Composed, devdiceyml: mr.DevSplit, testdiceyml: mr.TestSplit, stagingdiceyml: mr.StagingSplit, proddiceyml: mr.ProdSplit}

	for name, obj := range output {
		f, err := os.OpenFile(name, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
		if err != nil {
			return fmt.Errorf("openfile failed: %v\n", err)
		}
		out, err := yaml.Marshal(obj)
		if err != nil {
			return fmt.Errorf("yaml marshal failed: %v\n", err)
		}
		f.Write(out)
	}
	outputStr := strings.Join([]string{diceyml, devdiceyml, testdiceyml, stagingdiceyml, proddiceyml}, "\n")

	fmt.Printf("generate files:\n" + outputStr)
	return nil
}
