package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"

	. "terminus.io/dice/dice/cli/command"
)

var DEPLOY = Command{
	Name:      "deploy",
	ShortHelp: "部署 runtime",
	Args: []Arg{
		StringArg{}.Name("dice-yml"),
		StringArg{}.Name("runtime-name"),
		IntArg{}.Name("application-id"),
	},
	Flags: []Flag{
		BoolFlag{"", "dev", "开发环境", false},
		BoolFlag{"", "test", "测试环境", false},
		BoolFlag{"", "staging", "预发环境", false},
		BoolFlag{"", "prod", "生产环境", false},
	},
	Run: RunDeploy,
}

type deployBody struct {
	ApplicationID string `json:"applicationId"`
	Name          string `json:"name"`
	Executor      string `json:"executor"`
	Source        string `json:"source"`
	DiceYml       string `json:"diceYml"`
	WorkSpace     string `json:"workspace"`
	OperatorId    string `json:"operatorId"`
}

type deployResponse struct {
	DeploymentID  int `json:"deploymentId"`
	ApplicationID int `json:"applicationId"`
	RuntimeID     int `json:"runtimeId"`
}

func RunDeploy(ctx *Context, ymlPath string, runtimeName string, applicationID int, dev, test, staging, prod bool) error {
	f, err := os.Open(ymlPath)
	if err != nil {
		return fmt.Errorf("deploy: %v\n", err)
	}
	diceYml, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("deploy: %v\n", err)
	}
	workspace := "DEV"
	if prod {
		workspace = "PROD"
	} else if staging {
		workspace = "STAGING"
	} else if test {
		workspace = "TEST"
	}

	reqbody := deployBody{
		ApplicationID: strconv.Itoa(applicationID),
		Name:          runtimeName,
		Executor:      "MARATHONFORTERMINUSY",
		Source:        "dice-cli",
		DiceYml:       string(diceYml),
		WorkSpace:     workspace,
		OperatorId:    strconv.Itoa(ctx.StatusInfo.ID),
	}
	var body bytes.Buffer
	r, err := ctx.HttpClient.Post(ctx.Conf.OPENAPIServerAddr).
		Path("/dice/deployments").Header("Content-Type", "application/json").
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: ctx.StatusInfo.SessionID}).
		JSONBody(reqbody).Do().Body(&body)
	if err != nil {
		return fmt.Errorf("deploy fail: %v", err)
	}
	if !r.IsOK() {
		return fmt.Errorf("deploy fail: status code: %d, body: %v", r.StatusCode(), body.String())
	}
	var response deployResponse
	d := json.NewDecoder(&body)
	if err := d.Decode(&response); err != nil {
		buffered, err := ioutil.ReadAll(d.Buffered())
		if err != nil {
			return fmt.Errorf("deploy fail: decode response fail: %v", err)
		} else {
			return fmt.Errorf("deploy fail: decode response fail: %v, buffered: %v", err, buffered)
		}
	}
	fmt.Printf(`deploy success:
deploymentID: %d
applicationID: %d
runtimeID: %d
`, response.DeploymentID, response.ApplicationID, response.RuntimeID)

	return nil

}
