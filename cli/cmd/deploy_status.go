package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	. "terminus.io/dice/dice/cli/command"
)

var DEPLOY_STATUS = Command{
	ParentName: "DEPLOY",
	Name:       "status",
	ShortHelp:  "查询部署状态",
	Args: []Arg{
		IntArg{}.Name("deploymentID"),
	},
	Run: RunDeployStatus,
}

// {"deploymentId":51522,"status":"OK","runtime":{"services":{},"endpoints":{"web":{"host":"web.test-3519.services.v1.runtimes.marathon.l4lb.thisdcos.directory","ports":[8080]}}}}
type statusResponse struct {
	DeploymentID int         `json:"deploymentId"`
	Status       string      `json:"status"`
	Runtime      interface{} `json:"runtime"`
}

func RunDeployStatus(ctx *Context, deployID int) error {
	info := ctx.StatusInfo
	var body bytes.Buffer
	r, err := ctx.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Path("/dice/deployments/" + strconv.Itoa(deployID)).
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: info.SessionID}).
		Do().Body(&body)
	if err != nil {
		return fmt.Errorf("deploy status: %v\n", err)
	}
	if !r.IsOK() {
		return fmt.Errorf("deploy status: status code %d, body: %v\n", r.StatusCode(), body.String())
	}
	var response statusResponse
	d := json.NewDecoder(&body)
	if err := d.Decode(&response); err != nil {
		buffered, err := ioutil.ReadAll(d.Buffered())
		if err != nil {
			return fmt.Errorf("deploy status: decode response fail: %v\n", err)
		} else {
			return fmt.Errorf("deploy status: decode response fail: %v, buffered: %v\n", err, buffered)
		}
	}

	runtime, err := json.MarshalIndent(response.Runtime, "", "  ")
	if err != nil {
		return fmt.Errorf("deploy status: marshalindent fail\n")
	}

	fmt.Printf(`Status: %s
Runtime: %v
`, response.Status, string(runtime))
	return nil
}
