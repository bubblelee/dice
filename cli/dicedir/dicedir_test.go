package dicedir

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindGlobalDiceDir(t *testing.T) {
	u, _ := user.Current()
	assert.Nil(t, os.Mkdir(u.HomeDir+"/.dice_config", 0666))
	defer func() { os.Remove(u.HomeDir + "/.dice_config") }()

	_, err := FindGlobalDiceDir()
	assert.Nil(t, err)
}

func TestFindProjectDiceDir(t *testing.T) {
	u, _ := user.Current()
	assert.Nil(t, os.Mkdir(u.HomeDir+"/.dice", 0666))
	defer func() { os.Remove(u.HomeDir + "/.dice") }()

	curr, err := os.Getwd()
	upper := filepath.Dir(curr)
	assert.Nil(t, err)
	assert.Nil(t, os.Mkdir(upper+"/.dice", 0666))
	defer func() { os.Remove(upper + "/.dice") }()

	s, err := FindProjectDiceDir()
	assert.Nil(t, err)
	fmt.Printf("%+v\n", s) // debug print

}
