package main

import (
	"terminus.io/dice/dice/cli/command"
	_ "terminus.io/dice/dice/cli/generated_cmd"
)

func main() {
	command.Execute()
}
