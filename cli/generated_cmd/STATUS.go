// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var STATUSCmd = &cobra.Command{
	Use:   "status ",
        Short: "当前用户信息",
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		STATUSctx = command.GetContext()
		return cmd.RunStatus(STATUSctx)
	},
}

var (
	STATUSctx *command.Context
)

func init() {
	command.RootCmd.AddCommand(STATUSCmd)
}

