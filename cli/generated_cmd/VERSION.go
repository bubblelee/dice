// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var VERSIONCmd = &cobra.Command{
	Use:   "version ",
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		VERSIONctx = command.GetContext()
		return cmd.RunVersion(VERSIONctx)
	},
}

var (
	VERSIONctx *command.Context
)

func init() {
	command.RootCmd.AddCommand(VERSIONCmd)
}

