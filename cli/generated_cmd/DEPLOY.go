// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"strconv"
	"terminus.io/dice/dice/cli/cmd"
)

var DEPLOYCmd = &cobra.Command{
	Use:   "deploy <dice-yml> <runtime-name> <application-id>",
        Short: "部署 runtime",
	Args:  cobra.RangeArgs(3, 3),
	RunE: func(_ *cobra.Command, args []string) error {
		DEPLOYctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.StringArg{}).Validate(0, args[0]); err != nil { return err }
                        DEPLOYArg0 = args[0]
                }
                if len(args)-1 >= 1 {
                        if err := (command.StringArg{}).Validate(1, args[1]); err != nil { return err }
                        DEPLOYArg1 = args[1]
                }
                if len(args)-1 >= 2 {
                        if err := (command.IntArg{}).Validate(2, args[2]); err != nil { return err }
                        DEPLOYArg2, _ = strconv.Atoi(args[2])
                }
		return cmd.RunDeploy(DEPLOYctx, DEPLOYArg0, DEPLOYArg1, DEPLOYArg2, DEPLOYdevFlag, DEPLOYtestFlag, DEPLOYstagingFlag, DEPLOYprodFlag)
	},
}

var (
	DEPLOYctx *command.Context
        DEPLOYdevFlag bool
        DEPLOYtestFlag bool
        DEPLOYstagingFlag bool
        DEPLOYprodFlag bool
        DEPLOYArg0 string
        DEPLOYArg1 string
        DEPLOYArg2 int
)

func init() {
	DEPLOYCmd.Flags().BoolVarP(&DEPLOYdevFlag, "dev", "", false, "开发环境")

	DEPLOYCmd.Flags().BoolVarP(&DEPLOYtestFlag, "test", "", false, "测试环境")

	DEPLOYCmd.Flags().BoolVarP(&DEPLOYstagingFlag, "staging", "", false, "预发环境")

	DEPLOYCmd.Flags().BoolVarP(&DEPLOYprodFlag, "prod", "", false, "生产环境")

	command.RootCmd.AddCommand(DEPLOYCmd)
}

