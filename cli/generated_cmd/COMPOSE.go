// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var COMPOSECmd = &cobra.Command{
	Use:   "compose ",
        Short: "将 base.yml 和 env.yml 组合， 生成yml",
        Long: `默认从.dice目录中获取相关yaml文件`,
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		COMPOSEctx = command.GetContext()
		return cmd.RunCompose(COMPOSEctx, COMPOSEdevFlag, COMPOSEtestFlag, COMPOSEstagingFlag, COMPOSEprodFlag, COMPOSEpersistentFlag)
	},
}

var (
	COMPOSEctx *command.Context
        COMPOSEdevFlag bool
        COMPOSEtestFlag bool
        COMPOSEstagingFlag bool
        COMPOSEprodFlag bool
        COMPOSEpersistentFlag bool
)

func init() {
	COMPOSECmd.Flags().BoolVarP(&COMPOSEdevFlag, "dev", "", false, "开发环境")

	COMPOSECmd.Flags().BoolVarP(&COMPOSEtestFlag, "test", "", false, "测试环境")

	COMPOSECmd.Flags().BoolVarP(&COMPOSEstagingFlag, "staging", "", false, "预发环境")

	COMPOSECmd.Flags().BoolVarP(&COMPOSEprodFlag, "prod", "", false, "生产环境")

	COMPOSECmd.Flags().BoolVarP(&COMPOSEpersistentFlag, "persistent", "P", false, "同时写入dice.yml中")

	command.RootCmd.AddCommand(COMPOSECmd)
}

