// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var SSHCmd = &cobra.Command{
	Use:   "ssh <cluster_id|cluster_name> <[user@]host>",
        Short: "在集群节点执行命令",
	Args:  cobra.RangeArgs(2, 2),
	RunE: func(_ *cobra.Command, args []string) error {
		SSHctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.StringArg{}).Validate(0, args[0]); err != nil { return err }
                        SSHArg0 = args[0]
                }
                if len(args)-1 >= 1 {
                        if err := (command.StringArg{}).Validate(1, args[1]); err != nil { return err }
                        SSHArg1 = args[1]
                }
		return cmd.RunSSH(SSHctx, SSHArg0, SSHArg1, SSHportFlag)
	},
}

var (
	SSHctx *command.Context
        SSHportFlag int
        SSHArg0 string
        SSHArg1 string
)

func init() {
	SSHCmd.Flags().IntVarP(&SSHportFlag, "port", "p", 22, "port to connect to on the node")

	command.RootCmd.AddCommand(SSHCmd)
}

