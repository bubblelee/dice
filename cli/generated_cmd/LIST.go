// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var LISTCmd = &cobra.Command{
	Use:   "ls [project]",
        Short: "list projects and applications",
	Args:  cobra.RangeArgs(0, 1),
	RunE: func(_ *cobra.Command, args []string) error {
		LISTctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.StringArg{}).Validate(0, args[0]); err != nil { return err }
                        LISTArg0 = args[0]
                }
		return cmd.RunList(LISTctx, LISTArg0, LISTallFlag)
	},
}

var (
	LISTctx *command.Context
        LISTallFlag bool
        LISTArg0 string
)

func init() {
	LISTCmd.Flags().BoolVarP(&LISTallFlag, "all", "a", false, "列出所有应用（application）")

	command.RootCmd.AddCommand(LISTCmd)
}

