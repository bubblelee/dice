// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"strconv"
	"terminus.io/dice/dice/cli/cmd"
)

var DEPLOY_STATUSCmd = &cobra.Command{
	Use:   "status <deploymentID>",
        Short: "查询部署状态",
	Args:  cobra.RangeArgs(1, 1),
	RunE: func(_ *cobra.Command, args []string) error {
		DEPLOY_STATUSctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.IntArg{}).Validate(0, args[0]); err != nil { return err }
                        DEPLOY_STATUSArg0, _ = strconv.Atoi(args[0])
                }
		return cmd.RunDeployStatus(DEPLOY_STATUSctx, DEPLOY_STATUSArg0)
	},
}

var (
	DEPLOY_STATUSctx *command.Context
        DEPLOY_STATUSArg0 int
)

func init() {
	DEPLOYCmd.AddCommand(DEPLOY_STATUSCmd)
}

