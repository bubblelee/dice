// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var WEBHOOKCmd = &cobra.Command{
	Use:   "webhook ",
        Short: "webhook list",
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		WEBHOOKctx = command.GetContext()
		return cmd.RunWebhook(WEBHOOKctx)
	},
}

var (
	WEBHOOKctx *command.Context
)

func init() {
	command.RootCmd.AddCommand(WEBHOOKCmd)
}

