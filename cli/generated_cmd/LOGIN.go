// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var LOGINCmd = &cobra.Command{
	Use:   "login ",
        Short: "用户登陆",
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		LOGINctx = command.GetContext()
		return cmd.RunLogin(LOGINctx)
	},
}

var (
	LOGINctx *command.Context
)

func init() {
	command.RootCmd.AddCommand(LOGINCmd)
}

