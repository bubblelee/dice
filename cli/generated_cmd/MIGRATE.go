// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var MIGRATECmd = &cobra.Command{
	Use:   "migrate <v1-dice-yml>",
        Short: "由 v1 版本 dice.yml 生成 v2 版本",
	Args:  cobra.RangeArgs(1, 1),
	RunE: func(_ *cobra.Command, args []string) error {
		MIGRATEctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.StringArg{}).Validate(0, args[0]); err != nil { return err }
                        MIGRATEArg0 = args[0]
                }
		return cmd.RunMigrate(MIGRATEctx, MIGRATEArg0)
	},
}

var (
	MIGRATEctx *command.Context
        MIGRATEArg0 string
)

func init() {
	command.RootCmd.AddCommand(MIGRATECmd)
}

