// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var EDITCmd = &cobra.Command{
	Use:   "edit ",
        Short: "编辑dice.yml文件",
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		EDITctx = command.GetContext()
		return cmd.RunEdit(EDITctx, EDITdevFlag, EDITtestFlag, EDITstagingFlag, EDITprodFlag)
	},
}

var (
	EDITctx *command.Context
        EDITdevFlag bool
        EDITtestFlag bool
        EDITstagingFlag bool
        EDITprodFlag bool
)

func init() {
	EDITCmd.Flags().BoolVarP(&EDITdevFlag, "dev", "", false, "开发环境")

	EDITCmd.Flags().BoolVarP(&EDITtestFlag, "test", "", false, "测试环境")

	EDITCmd.Flags().BoolVarP(&EDITstagingFlag, "staging", "", false, "预发环境")

	EDITCmd.Flags().BoolVarP(&EDITprodFlag, "prod", "", false, "生产环境")

	command.RootCmd.AddCommand(EDITCmd)
}

