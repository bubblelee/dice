// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var PARSECmd = &cobra.Command{
	Use:   "parse ",
        Short: "解析dice.yml, 合并(merge)不同环境内容",
        Long: `如果没有 --dev, --test, --staging, --prod 这些flag， 那么不做merge`,
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		PARSEctx = command.GetContext()
		return cmd.RunParse(PARSEctx, PARSEfileFlag, PARSEstrFlag, PARSEdevFlag, PARSEtestFlag, PARSEstagingFlag, PARSEprodFlag, PARSEymlFlag)
	},
}

var (
	PARSEctx *command.Context
        PARSEfileFlag string
        PARSEstrFlag string
        PARSEdevFlag bool
        PARSEtestFlag bool
        PARSEstagingFlag bool
        PARSEprodFlag bool
        PARSEymlFlag bool
)

func init() {
	PARSECmd.Flags().StringVarP(&PARSEfileFlag, "file", "f", "", "dice-yml path ")

	PARSECmd.Flags().StringVarP(&PARSEstrFlag, "str", "s", "", "dice-yml content")

	PARSECmd.Flags().BoolVarP(&PARSEdevFlag, "dev", "", false, "merge dev yml")

	PARSECmd.Flags().BoolVarP(&PARSEtestFlag, "test", "", false, "merge test yml")

	PARSECmd.Flags().BoolVarP(&PARSEstagingFlag, "staging", "", false, "merge staging yml")

	PARSECmd.Flags().BoolVarP(&PARSEprodFlag, "prod", "", false, "merge production yml")

	PARSECmd.Flags().BoolVarP(&PARSEymlFlag, "yml", "", false, "output yml or json?")

	command.RootCmd.AddCommand(PARSECmd)
}

