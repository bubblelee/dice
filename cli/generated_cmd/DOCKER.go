// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var DOCKERCmd = &cobra.Command{
	Use:   "docker <cluster_id|cluster_name> <container_id|container_ip>",
        Short: "在集群容器执行命令",
	Args:  cobra.RangeArgs(2, 2),
	RunE: func(_ *cobra.Command, args []string) error {
		DOCKERctx = command.GetContext()
                if len(args)-1 >= 0 {
                        if err := (command.StringArg{}).Validate(0, args[0]); err != nil { return err }
                        DOCKERArg0 = args[0]
                }
                if len(args)-1 >= 1 {
                        if err := (command.StringArg{}).Validate(1, args[1]); err != nil { return err }
                        DOCKERArg1 = args[1]
                }
		return cmd.RunDocker(DOCKERctx, DOCKERArg0, DOCKERArg1, DOCKERhostFlag, DOCKERportFlag)
	},
}

var (
	DOCKERctx *command.Context
        DOCKERhostFlag string
        DOCKERportFlag int
        DOCKERArg0 string
        DOCKERArg1 string
)

func init() {
	DOCKERCmd.Flags().StringVarP(&DOCKERhostFlag, "host", "", "", "dockerd host")

	DOCKERCmd.Flags().IntVarP(&DOCKERportFlag, "port", "p", 2375, "dockerd port")

	command.RootCmd.AddCommand(DOCKERCmd)
}

