// GENERATED FILE, DO NOT EDIT
package cmd

import (
	"github.com/spf13/cobra"
	"terminus.io/dice/dice/cli/command"
	"terminus.io/dice/dice/cli/cmd"
)

var INITCmd = &cobra.Command{
	Use:   "init ",
        Short: "在当前目录下初始化 dice 项目",
        Long: `1. 当前目录下创建 .dice 目录
2. 在.dice目录中创建 dice.yml`,
	Args:  cobra.RangeArgs(0, 0),
	RunE: func(_ *cobra.Command, args []string) error {
		INITctx = command.GetContext()
		return cmd.RunInit(INITctx)
	},
}

var (
	INITctx *command.Context
)

func init() {
	command.RootCmd.AddCommand(INITCmd)
}

