package main

import (
        . "terminus.io/dice/dice/cli/command"
        . "terminus.io/dice/dice/cli/cmd"
)

var CMDs = []Command{
	COMPOSE,
	DEPLOY,
	DEPLOY_STATUS,
	DOCKER,
	EDIT,
	INIT,
	LIST,
	LOGIN,
	MIGRATE,
	PARSE,
	SSH,
	STATUS,
	VERSION,
	WEBHOOK,
}
var CMDNames = []string{
	"COMPOSE",
	"DEPLOY",
	"DEPLOY_STATUS",
	"DOCKER",
	"EDIT",
	"INIT",
	"LIST",
	"LOGIN",
	"MIGRATE",
	"PARSE",
	"SSH",
	"STATUS",
	"VERSION",
	"WEBHOOK",
}