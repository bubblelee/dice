package main

import (
	"fmt"
	"os"
	"reflect"
	"runtime"
	"strings"
	"text/template"

	"terminus.io/dice/dice/cli/command"
)

//go:generate go run collect/collect.go
//go:generate go run generate.go collectCMDs.go validate.go
func main() {
	outputPath := "../../generated_cmd/"
	if err := os.RemoveAll(outputPath); err != nil {
		panic(err)
	}
	if err := os.Mkdir(outputPath, 0777); err != nil {
		panic(err)
	}
	for idx, cmd := range CMDs {
		if err := validate(cmd, CMDNames[idx]); err != nil {
			panic(err)
		}
		var buf strings.Builder
		arg := genTemplateArg(cmd, CMDNames[idx])
		if err := CMDtemplate.Execute(&buf, arg); err != nil {
			panic(err)
		}

		f, err := os.OpenFile(outputPath+CMDNames[idx]+".go", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
		if err != nil {
			panic(err)
		}
		f.WriteString(buf.String())
		fmt.Printf("generated [%s]\n", CMDNames[idx])
	}
}

func genTemplateArg(cmd command.Command, cmdname string) templateArg {
	runcmd := ""
	if cmd.Run != nil {
		splited := strings.Split(runtime.FuncForPC(reflect.ValueOf(cmd.Run).Pointer()).Name(), "/")
		runcmd = splited[len(splited)-1]
	}
	parent := "command.RootCmd"
	if cmd.ParentName != "" {
		parent = cmd.ParentName + "Cmd"
	}
	argmin, argmax := argNum(cmd.Args)
	r := templateArg{
		Imports:    imports(cmd),
		Usage:      genUsage(cmd),
		ShortHelp:  cmd.ShortHelp,
		LongHelp:   cmd.LongHelp,
		Args:       cmd.Args,
		ArgsNumMin: argmin,
		ArgsNumMax: argmax,
		Flags:      cmd.Flags,
		Name:       cmdname,
		RunCMD:     runcmd,
		ParentCmd:  parent,

		ArgType:   argType,
		FlagType:  flagType,
		Underline: underline,
	}
	return r
}

func genUsage(cmd command.Command) string {
	args := []string{}
	for _, arg := range cmd.Args {
		if arg.IsOption() {
			args = append(args, "["+arg.GetName()+"]")
		} else {
			args = append(args, "<"+arg.GetName()+">")
		}
	}
	return fmt.Sprintf("%s %s", cmd.Name, strings.Join(args, " "))
}
func argNum(args []command.Arg) (int, int) {
	var min, max int
	for _, arg := range args {
		if !arg.IsOption() {
			min++
		}
		max++
	}
	return min, max
}

func argType(arg command.Arg) string {
	switch arg.(type) {
	case command.IntArg:
		return "int"
	case command.StringArg:
		return "string"
	case command.FloatArg:
		return "float64"
	case command.IPArg:
		return "ip"
	default:
		panic("should not be here")
	}
}

func flagType(flag command.Flag) string {
	switch flag.(type) {
	case command.IntFlag:
		return "int"
	case command.StringFlag:
		return "string"
	case command.BoolFlag:
		return "bool"
	case command.FloatFlag:
		return "float64"
	case command.IPFlag:
		return "ip"
	case command.StringListFlag:
		return "[]string"
	default:
		panic("should not be here")
	}

}
func imports(cmd command.Command) []string {
	r := []string{
		"github.com/spf13/cobra",
		"terminus.io/dice/dice/cli/command",
	}
	if needStrconv(cmd) {
		r = append(r, "strconv")
	}
	if needNet(cmd) {
		r = append(r, "net")
	}
	if needCmd(cmd) {
		r = append(r, "terminus.io/dice/dice/cli/cmd")
	}
	return r
}

type templateArg struct {
	Imports    []string
	Usage      string
	ShortHelp  string
	LongHelp   string
	Args       []command.Arg
	ArgsNumMin int
	ArgsNumMax int
	Flags      []command.Flag
	Name       string
	RunCMD     string
	ParentCmd  string

	ArgType   func(command.Arg) string
	FlagType  func(command.Flag) string
	Underline func(string) string
}

var CMDtemplate = template.Must(template.New("spec").Parse(`// GENERATED FILE, DO NOT EDIT
package cmd

import (
{{- range $_, $v := .Imports }}
	"{{$v}}"
{{- end}}
)

var {{.Name}}Cmd = &cobra.Command{
	Use:   "{{.Usage}}",
        {{- if .ShortHelp}}
        Short: "{{.ShortHelp}}",
        {{- end}}
        {{- if .LongHelp}}
        Long: ` + "`{{.LongHelp}}`" + `,
        {{- end}}
        {{- if .RunCMD}}
	Args:  cobra.RangeArgs({{$.ArgsNumMin}}, {{$.ArgsNumMax}}),
	RunE: func(_ *cobra.Command, args []string) error {
		{{.Name}}ctx = command.GetContext()
                {{- range $idx, $v := .Args}}
                if len(args)-1 >= {{$idx}} {
                        {{- if eq (call $.ArgType $v) "string"}}
                        if err := (command.StringArg{}).Validate({{$idx}}, args[{{$idx}}]); err != nil { return err }
                        {{$.Name}}Arg{{$idx}} = args[{{$idx}}]
                        {{- else if eq (call $.ArgType $v) "int"}}
                        if err := (command.IntArg{}).Validate({{$idx}}, args[{{$idx}}]); err != nil { return err }
                        {{$.Name}}Arg{{$idx}}, _ = strconv.Atoi(args[{{$idx}}])
                        {{- else if eq (call $.ArgType $v) "ip"}}
                        if err := (command.IPArg{}).Validate({{$idx}}, args[{{$idx}}]); err != nil { return err }
                        {{$.Name}}Arg{{$idx}} = net.IP([]byte(args[{{$idx}}]))
                        {{- else if eq (call $.ArgType $v) "float64"}}
                        if err := (command.FloatArg{}).Validate({{$idx}}, args[{{$idx}}]); err != nil { return err }
                        {{$.Name}}Arg{{$idx}}, _ = strconv.ParseFloat(args[{{$idx}}], 64)
                        {{- end}}
                }
                {{- end}}
		return {{.RunCMD}}({{.Name}}ctx{{range $idx, $v := .Args}}, {{$.Name}}Arg{{$idx}}{{end}}{{range $_, $v := .Flags}}, {{$.Name}}{{call $.Underline $v.Name}}Flag{{end}})
	},
        {{- end}}
}

var (
	{{.Name}}ctx *command.Context

{{- range $_, $v := .Flags}}
        {{$.Name}}{{call $.Underline $v.Name}}Flag {{(call $.FlagType $v)}}
{{- end}}

{{- range $idx, $v := .Args}}
        {{$.Name}}Arg{{$idx}} {{(call $.ArgType $v)}}
{{- end}}
)

func init() {
{{- range $_, $v := .Flags}}
	{{- if eq (call $.FlagType $v) "string"}}
	{{$.Name}}Cmd.Flags().StringVarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", "{{$v.DefaultValue}}", "{{$v.Doc}}")
	{{- else if eq (call $.FlagType $v) "bool"}}
	{{$.Name}}Cmd.Flags().BoolVarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", {{$v.DefaultValue}}, "{{$v.Doc}}")
	{{- else if eq (call $.FlagType $v) "int"}}
	{{$.Name}}Cmd.Flags().IntVarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", {{$v.DefaultValue}}, "{{$v.Doc}}")
	{{- else if eq (call $.FlagType $v) "float64"}}
	{{$.Name}}Cmd.Flags().Float64VarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", {{$v.DefaultValue}}, "{{$v.Doc}}")
	{{- else if eq (call $.FlagType $v) "ip"}}
	{{$.Name}}Cmd.Flags().IPVarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", net.IP([]byte("{{$v.DefaultValue}}")), "{{$v.Doc}}")
	{{- else if eq (call $.FlagType $v) "[]string"}}
	{{$.Name}}Cmd.Flags().StringSliceVarP(&{{$.Name}}{{call $.Underline $v.Name}}Flag, "{{$v.Name}}", "{{$v.Short}}", {{$v.DefaultV}}, "{{$v.Doc}}")
        {{- end}}
{{end}}
	{{.ParentCmd}}.AddCommand({{.Name}}Cmd)
}

`))

func needStrconv(cmd command.Command) bool {
	for _, arg := range cmd.Args {
		if _, ok := arg.(command.IntArg); ok {
			return true
		}
		if _, ok := arg.(command.FloatArg); ok {
			return true
		}
	}
	return false
}

func needNet(cmd command.Command) bool {
	for _, arg := range cmd.Args {
		if _, ok := arg.(command.IPArg); ok {
			return true
		}
	}
	for _, flag := range cmd.Flags {
		if _, ok := flag.(command.IPFlag); ok {
			return true
		}
	}
	return false
}

func needCmd(cmd command.Command) bool {
	return cmd.Run != nil
}

func underline(s string) string {
	return strings.Replace(s, "-", "_", -1)
}
