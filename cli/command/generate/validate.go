package main

import (
	"terminus.io/dice/dice/cli/command"

	"github.com/pkg/errors"
)

var (
	ErrName                = errors.New("not provide Name")
	ErrOptionalArgPosition = errors.New("optional Arg should be the last arg")
	ErrOptionalArgNum      = errors.New("too many optional arg, support only 1 optional arg yet")
)

func validate(cmd command.Command, fname string) error {
	if cmd.Name == "" {
		return errors.Wrap(ErrName, fname)
	}

	optionalArgNum := 0
	for _, arg := range cmd.Args {
		if arg.IsOption() {
			optionalArgNum++
		} else {
			if optionalArgNum > 0 {
				return errors.Wrap(ErrOptionalArgPosition, fname)
			}
		}
	}
	if optionalArgNum > 1 {
		return errors.Wrap(ErrOptionalArgNum, fname)
	}
	return nil
}
