package command

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"

	"terminus.io/dice/dice/cli/dicedir"
	"terminus.io/dice/dice/cli/status"
	"terminus.io/dice/dice/pkg/httpclient"
)

var ctx = Context{}

func GetContext() *Context {
	return &ctx
}

type Context struct {
	status.StatusInfo
	status.Conf
	Debug      bool
	HttpClient *httpclient.HTTPClient
}

func (c *Context) CurrentOrg() (int, error) {
	var body bytes.Buffer
	r, err := c.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Path("/api/openapi/users/current").
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: c.StatusInfo.SessionID}).
		Do().Body(&body)
	if err != nil {
		return 0, fmt.Errorf("CurrentOrg: %v\n", err)
	}
	if !r.IsOK() {
		return 0, fmt.Errorf("CurrentOrg: statuscode: %d, body: %v\n", r.StatusCode(), body.String())
	}
	var res struct {
		orgID int `json:"orgId"`
	}
	d := json.NewDecoder(&body)
	if err := d.Decode(&res); err != nil {
		return 0, fmt.Errorf("CurrentOrg: decode fail: %v, buffered: %v", err, d.Buffered())
	}
	return res.orgID, nil
}

type Project struct {
	Creator     string `json:"creator"`
	Desc        string `json:"desc"`
	DisplayName string `json:"displayName"`
	ID          int    `json:"id"`
	Logo        string `json:"logo"`
	Name        string `json:"name"`
	OrgID       int    `json:"orgId"`
	Stats       *struct {
		CountApplications int    `json:"countApplications"`
		CountMembers      int    `json:"countMembers"`
		CountRuntimes     int    `json:"countRuntimes"`
		TimeLastModified  string `json:"timeLastModified"`
	} `json:"stats"`
}
type Projects []Project

func (ps Projects) IDs() []int {
	r := []int{}
	for _, p := range ps {
		r = append(r, p.ID)
	}
	return r
}

func (ps Projects) FindByName(name string) *Project {
	name = strings.TrimSpace(name)
	for _, p := range ps {
		if strings.TrimSpace(p.Name) == name ||
			strings.TrimSpace(p.DisplayName) == name {
			return &p
		}
	}
	return nil
}

func (ps Projects) FindByID(id int) *Project {
	for _, p := range ps {
		if p.ID == id {
			return &p
		}
	}
	return nil
}

// name first
func (ps Projects) FindByNameOrID(k string) *Project {
	r := ps.FindByName(k)
	if r == nil {
		id, err := strconv.Atoi(k)
		if err != nil {
			return nil
		}
		r = ps.FindByID(id)
	}
	return r
}

func (c *Context) CurrentProjects() (Projects, error) {
	var body bytes.Buffer
	r, err := ctx.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Path("/api/admin/users/current/projects").
		Header("Accept", "application/vnd.dice+json; version=1.0").
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: ctx.SessionID}).Do().
		Body(&body)
	if err != nil {
		return nil, err
	}
	if !r.IsOK() {
		return nil, fmt.Errorf("status code: %d, body: %v", r.StatusCode(), body.String())
	}
	var res Projects
	d := json.NewDecoder(&body)
	if err := d.Decode(&res); err != nil {
		return nil, fmt.Errorf("deploy fail: decode response fail: %v, buffered: %v", err, d.Buffered())
	}
	return res, nil
}

type Application struct {
	Desc        string `json:"desc"`
	ID          int    `json:"id"`
	Logo        string `json:"logo"`
	Name        string `json:"name"`
	OrgID       int    `json:"orgId"`
	ProjectID   int    `json:"projectId"`
	ProjectName string `json:"projectName"`
	Repo        string `json:"repo"`
	Stats       *struct {
		CountApplications int    `json:"countApplications"`
		CountMembers      int    `json:"countMembers"`
		CountRuntimes     int    `json:"countRuntimes"`
		TimeLastModified  string `json:"timeLastModified"`
	} `json"stats"`
}

type Applications []Application

func (apps Applications) IDs() []int {
	r := []int{}
	for _, app := range apps {
		r = append(r, app.ID)
	}
	return r
}

func (apps Applications) FindByID(id int) *Application {
	for _, app := range apps {
		if app.ID == id {
			return &app
		}
	}
	return nil
}

func (c *Context) CurrentApplications(projectNameOrID string, all bool) (Applications, error) {
	allProjects, err := c.CurrentProjects()
	if err != nil {
		return nil, err
	}
	projectIDs := []int{}
	if all {
		projectIDs = allProjects.IDs()
	} else {
		p := allProjects.FindByNameOrID(projectNameOrID)
		if p == nil {
			return nil, nil
		}
		projectIDs = []int{p.ID}

	}
	var r Applications
	for _, prjid := range projectIDs {
		apps, err := listApplicationAux(c, prjid)
		if err != nil {
			return nil, err
		}
		r = append(r, apps...)
	}
	return r, nil
}

func listApplicationAux(ctx *Context, projectID int) (Applications, error) {
	var body bytes.Buffer
	path := fmt.Sprintf("/api/admin/group-projects/%d/applications", projectID)
	r, err := ctx.HttpClient.Get(ctx.Conf.OPENAPIServerAddr).
		Path(path).
		Header("Accept", "application/vnd.dice+json; version=1.0").
		Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: ctx.SessionID}).Do().
		Body(&body)
	if err != nil {
		return nil, err
	}
	if !r.IsOK() {
		return nil, fmt.Errorf("status code: %d, body: %v\n", r.StatusCode(), body.String())
	}
	var apps Applications
	d := json.NewDecoder(&body)
	if err := d.Decode(&apps); err != nil {
		return nil, fmt.Errorf("deploy fail: decode response fail: %v, buffered: %v", err, d.Buffered())
	}
	return apps, nil
}

// 项目 .dice 目录下的 dice.yml
func defaultYml(env string) (string, error) {
	pdir, err := dicedir.FindProjectDiceDir()
	if err != nil {
		return "", fmt.Errorf("FindProjectDiceDir: %v", err)
	}
	var envfilename string
	switch env {
	case "dev":
		envfilename = "dice_development.yml"
	case "test":
		envfilename = "dice_test.yml"
	case "staging":
		envfilename = "dice_staging.yml"
	case "prod":
		envfilename = "dice_production.yml"
	default:
		envfilename = "dice.yml"
	}
	ymlPath := filepath.Join(pdir, envfilename)

	return ymlPath, nil
}

func (c *Context) DiceYml() (string, error) {
	return defaultYml("")
}
func (c *Context) DevDiceYml() (string, error) {
	return defaultYml("dev")
}
func (c *Context) TestDiceYml() (string, error) {
	return defaultYml("test")
}
func (c *Context) StagingDiceYml() (string, error) {
	return defaultYml("staging")
}
func (c *Context) ProdDiceYml() (string, error) {
	return defaultYml("prod")
}
