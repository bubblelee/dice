package command

import (
	"fmt"
	"net"
	"strconv"
)

const (
	validateErrStr = "validate arg(%d) fail: %v, should be %s type"
)

type Arg interface {
	Validate(idx int, src string) error
	GetName() string
	// set name
	Name(name string) Arg
	// is an optional arg?
	Option() Arg

	IsOption() bool
}

type CommonArg struct {
	name string
	opt  bool
}

type StringArg struct {
	CommonArg
}
type IntArg struct {
	CommonArg
}
type FloatArg struct {
	CommonArg
}
type IPArg struct {
	CommonArg
}

func (a StringArg) GetName() string {
	return a.name
}
func (a IntArg) GetName() string {
	return a.name
}
func (a FloatArg) GetName() string {
	return a.name
}
func (a IPArg) GetName() string {
	return a.name
}

func (a StringArg) Name(name string) Arg {
	a.name = name
	return a
}
func (a IntArg) Name(name string) Arg {
	a.name = name
	return a
}
func (a FloatArg) Name(name string) Arg {
	a.name = name
	return a
}
func (a IPArg) Name(name string) Arg {
	a.name = name
	return a
}

func (a StringArg) Option() Arg {
	a.opt = true
	return a
}
func (a IntArg) Option() Arg {
	a.opt = true
	return a
}
func (a FloatArg) Option() Arg {
	a.opt = true
	return a
}
func (a IPArg) Option() Arg {
	a.opt = true
	return a
}
func (a StringArg) IsOption() bool {
	return a.opt
}
func (a IntArg) IsOption() bool {
	return a.opt
}
func (a FloatArg) IsOption() bool {
	return a.opt
}
func (a IPArg) IsOption() bool {
	return a.opt
}

func (StringArg) Validate(idx int, src string) error {
	return nil
}
func (IntArg) Validate(idx int, src string) error {
	_, err := strconv.Atoi(src)
	if err != nil {
		return fmt.Errorf(validateErrStr, idx, err, "int")
	}
	return nil
}
func (FloatArg) Validate(idx int, src string) error {
	_, err := strconv.ParseFloat(src, 64)
	if err != nil {
		return fmt.Errorf(validateErrStr, idx, err, "float")
	}
	return nil
}
func (IPArg) Validate(idx int, src string) error {
	ip := net.ParseIP(src)
	if ip.String() == "<nil>" {
		return fmt.Errorf(validateErrStr, idx, "parseIP fail", "ip")
	}
	return nil
}
