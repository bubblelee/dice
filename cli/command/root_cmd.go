package command

import (
	"fmt"
	"os"
	"strings"

	"terminus.io/dice/dice/cli/status"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/spf13/cobra"
)

var loginWhiteListCmds = strings.Join([]string{"login", "parse", "compose", "migrate"}, ",")

// rootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:          "dice",
	Short:        "dice's commandline client",
	SilenceUsage: true,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if !status.ConfExist() {
			if err := status.InitConf(); err != nil {
				return err
			}
		}
		conf, err := status.GetConf()
		if err != nil {
			return err
		}
		ctx.Conf = conf
		ctx.Debug = debugFlag
		if debugFlag {
			ctx.HttpClient = httpclient.New(httpclient.WithDebug(os.Stdout))
		} else {
			ctx.HttpClient = httpclient.New()
		}
		if strings.Contains(loginWhiteListCmds, strings.Split(cmd.Use, " ")[0]) {
			return nil
		}
		info, err := status.GetStatus()
		if err != nil {
			return fmt.Errorf("not login yet")
		}
		ctx.StatusInfo = info
		return nil
	},
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

var debugFlag bool

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	RootCmd.PersistentFlags().BoolVar(&debugFlag, "debug", false, "toggle debug mode")
	if err := RootCmd.Execute(); err != nil {
		os.Exit(0)
	}
}

func init() {}
