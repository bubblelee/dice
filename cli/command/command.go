package command

type Command struct {
	ParentName string
	Name       string
	ShortHelp  string
	LongHelp   string
	/* args:
	        []Arg {
		       IPArg{},
		       StringArg{},
		       BoolArg{},
		}
	*/
	Args []Arg
	/* flags:
	        []Flag{
	                StringFlag{"H", "host", "1.2.3.4", "doc"},
	                BoolFlag{"A", "another", true, "doc"},
	                IntFlag{"O", "ohyoyo", 1, "doc"},
		}
	*/
	Flags []Flag
	/* actually type:
		func(ctx Context, arg1 IPArg, arg2 string, arg3 bool, host string, anotherone bool, ohyoyo int) error

	`host`, `anotherone`, `ohyoyo` is generated by `Flags`
	*/
	Run interface{}
}
