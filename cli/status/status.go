package status

import (
	"encoding/json"
	"os"
	"path/filepath"

	"terminus.io/dice/dice/cli/dicedir"

	yaml "gopkg.in/yaml.v2"
)

const (
	sessionFile = "session"
	confFile    = "conf"
)

type UserInfo struct {
	ID          int    `json:"id"`
	Email       string `json:"email"`
	NickName    string `json:"nickName"`
	Enabled     bool   `json:"enabled"`
	CreatedAt   string `json:"createdAt"`
	UpdatedAt   string `json:"updatedAt"`
	LastLoginAt string `json:"lastLoginAt"`
}

type StatusInfo struct {
	SessionID string `json:"sessionid"`
	UserInfo
}

func GetStatus() (StatusInfo, error) {
	ddir, err := dicedir.FindGlobalDiceDir()
	if err != nil {
		return StatusInfo{}, err
	}
	sessionPath := filepath.Join(ddir, sessionFile)
	f, err := os.Open(sessionPath)
	if err != nil {
		return StatusInfo{}, err
	}
	var stat StatusInfo
	if err := json.NewDecoder(f).Decode(&stat); err != nil {
		return StatusInfo{}, err
	}
	return stat, nil
}

func StoreStatus(stat StatusInfo) error {
	ddir, err := dicedir.FindGlobalDiceDir()
	if err == dicedir.NotExist {
		ddir, err = dicedir.CreateGlobalDiceDir()
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	sessionPath := filepath.Join(ddir, sessionFile)
	f, err := os.OpenFile(sessionPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	defer f.Close()
	content, err := json.MarshalIndent(stat, "", "  ")
	if err != nil {
		return err
	}
	f.Write(content)
	return nil
}

type Conf struct {
	OPENAPIServerAddr string `yaml:"openapiServerAddr"`
}

func GetConf() (Conf, error) {
	ddir, err := dicedir.FindGlobalDiceDir()
	if err != nil {
		return Conf{}, err
	}
	confPath := filepath.Join(ddir, confFile)
	f, err := os.Open(confPath)
	if err != nil {
		return Conf{}, err
	}
	var conf Conf
	if err := yaml.NewDecoder(f).Decode(&conf); err != nil {
		return Conf{}, err
	}
	return conf, nil
}

func ConfExist() bool {
	ddir, err := dicedir.FindGlobalDiceDir()
	if err != nil {
		return false
	}
	confPath := filepath.Join(ddir, confFile)
	f, err := os.Stat(confPath)
	if err != nil || f.IsDir() {
		return false
	}
	return true
}

func StoreConf(conf Conf) error {
	ddir, err := dicedir.FindGlobalDiceDir()
	if err == dicedir.NotExist {
		ddir, err = dicedir.CreateGlobalDiceDir()
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}
	confPath := filepath.Join(ddir, confFile)
	f, err := os.OpenFile(confPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		return err
	}
	defer f.Close()
	content, err := yaml.Marshal(conf)
	if err != nil {
		return err
	}
	f.Write(content)
	return nil
}

func DefaultConf() Conf {
	return Conf{
		OPENAPIServerAddr: "openapi.terminus.io",
	}
}

func InitConf() error {
	conf := DefaultConf()
	return StoreConf(conf)
}
