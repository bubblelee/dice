package settings

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
)

type M = map[string]interface{}
type A = []interface{}

var (
	HTTPAddr      = ":9029"
	LogLevel      = logrus.InfoLevel
	PIDFile       = ""
	MySQLHost     = ""
	MySQLPort     = 3306
	MySQLUser     = ""
	MySQLPassword = ""
	MySQLDatabase = ""
	SchedulerAddr = ""
	URLs          = map[string]string{
		"dcos":          "[proto]://dcos.[domain]",
		"gittar":        "http://analyzer:dice@dicegittar.marathon.l4lb.thisdcos.directory:5566",
		"colonySoldier": "http://colony-soldier.[domain]",
		"nexus":         "http://nexus.[domain]",
		"registry":      "http://hub.docker.[domain]",
	}
)

func init() {
	if s := os.Getenv("HTTP_ADDR"); s != "" {
		HTTPAddr = s
	}
	if s := os.Getenv("LOG_LEVEL"); s != "" {
		var err error
		LogLevel, err = logrus.ParseLevel(s)
		if err != nil {
			logrus.Fatalln(err)
		}
	}
	if s := os.Getenv("PID_FILE"); s != "" {
		PIDFile = s
	}

	if s := os.Getenv("MYSQL_HOST"); s != "" {
		MySQLHost = s
	}
	if s := os.Getenv("MYSQL_PORT"); s != "" {
		i, err := strconv.Atoi(s)
		if err != nil {
			logrus.Fatalln(err)
		}
		MySQLPort = i
	}
	if s := os.Getenv("MYSQL_USER"); s != "" {
		MySQLUser = s
	}
	if s := os.Getenv("MYSQL_PASSWORD"); s != "" {
		MySQLPassword = s
	}
	if s := os.Getenv("MYSQL_DATABASE"); s != "" {
		MySQLDatabase = s
	}
	if s := os.Getenv("SCHEDULER_ADDR"); s != "" {
		SchedulerAddr = s
	}
}

var (
	mu sync.Mutex
	db *sql.DB
)

func Open() (*sql.DB, error) {
	mu.Lock()
	defer mu.Unlock()
	if db != nil {
		return db, nil
	}

	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true&charset=utf8mb4",
		MySQLUser, MySQLPassword, MySQLHost, MySQLPort, MySQLDatabase)
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(512)
	db.SetMaxIdleConns(32)
	db.SetConnMaxLifetime(time.Hour)

	return db, nil
}

func Init() {
	logrus.SetLevel(LogLevel)

	if PIDFile != "" {
		b := []byte(strconv.Itoa(os.Getpid()))
		if err := ioutil.WriteFile(PIDFile, b, 0644); err != nil {
			logrus.Fatalln(err)
		}
	}
}

var (
	exitChan                 = make(chan struct{})
	ExitChan <-chan struct{} = exitChan
)

func Wait() {
	nc := make(chan os.Signal, 1)
	signal.Notify(nc, syscall.SIGINT, syscall.SIGTERM)
	logrus.Infoln("wait signal")
	<-nc
	logrus.Infoln("signal notify")

	close(exitChan)
	time.Sleep(time.Second)

	if db != nil {
		if err := db.Close(); err != nil {
			logrus.Errorln(err)
		}
	}

	if PIDFile != "" {
		if err := os.Remove(PIDFile); err != nil {
			logrus.Errorln(err)
		}
	}
}
