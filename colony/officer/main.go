package main

import (
	"net/http"

	"terminus.io/dice/dice/colony/officer/controllers/cluster"
	"terminus.io/dice/dice/colony/officer/settings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {
	settings.Init()

	r := mux.NewRouter()
	// TODO: Accept, Request-ID
	s := r.PathPrefix("/api").Subrouter()
	cluster.Route(s)

	go func() {
		logrus.Infoln("http serve", settings.HTTPAddr)
		http.Handle("/", r)
		err := http.ListenAndServe(settings.HTTPAddr, r)
		if err != nil {
			logrus.Fatalln(err)
		}
	}()

	settings.Wait()
}
