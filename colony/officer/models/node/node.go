package node

import "time"

type Node struct {
	ID        string    `json:"id"`
	ClusterID string    `json:"cluster_id"`
	Type      string    `json:"type"` // master, public, private
	IP        string    `json:"ip"`
	Tag       string    `json:"tag"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
