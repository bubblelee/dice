CREATE TABLE co_clusters (
	id INTEGER NOT NULL AUTO_INCREMENT,
	org_id INTEGER NOT NULL,
	name VARCHAR(41) NOT NULL,
	type ENUM('k8s', 'dcos', 'edas') NOT NULL,
	logo TEXT NOT NULL,
	description TEXT NOT NULL,
	wildcard_domain VARCHAR(255) NOT NULL DEFAULT '',
	urls TEXT NOT NULL,
	`system` TEXT NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
) ENGINE InnoDB CHARACTER SET utf8mb4;
CREATE UNIQUE INDEX co_clusters_name ON co_clusters (name);
