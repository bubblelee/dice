package platform

type Auth struct {
	Type     string `json:"type"` // basic, token
	Username string `json:"username"`
	Password string `json:"password"`
}

type MySQL struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
}

type Redis struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Password string `json:"password"`
}

type Component struct {
	Id         string                 `json:"id"`
	CPUs       float64                `json:"cpus"`
	Mem        int                    `json:"mem"`
	Instances  int                    `json:"instances"`
	Host       string                 `json:"host"`
	Vars       map[string]string      `json:"vars"`
	Definition map[string]interface{} `json:"definition"`
}

type Platform struct {
	DeployType string      `json:"deployType"` // saas, full
	MySQL      MySQL       `json:"mysql"`
	Redis      Redis       `json:"redis"`
	Auth       Auth        `json:"auth"`
	Components []Component `json:"components"`
}
