package cluster

import (
	"net/url"
	"regexp"
	"strings"
)

func isLowerNumber(s string, maxLength int) bool {
	n := len(s)
	if n < 1 || n > maxLength {
		return false
	}
	for i := 0; i < n; i++ {
		if !(s[i] >= '0' && s[i] <= '9') && !(s[i] >= 'a' && s[i] <= 'z') {
			return false
		}
	}
	return true
}

func IsName(s string) bool {
	i := strings.IndexByte(s, '-')
	if i == -1 {
		return false
	}
	return isLowerNumber(s[:i], 20) && isLowerNumber(s[i+1:], 20)
}

var domainRegexp = regexp.MustCompile(`^(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.([a-zA-Z]{2,6}|[a-zA-Z0-9-]{2,30}\.[a-zA-Z
 ]{2,3})$`)

func IsValid(message AddClusterRequest) string {
	if !IsName(message.Name) {
		return "invalid cluster name"
	}
	if !domainRegexp.MatchString(message.WildcardDomain) {
		return "invalid wildcard domain"
	}
	_, err := url.Parse(message.Logo)

	if err != nil {
		return "invalid logo url"
	}
	if message.Description != "" && len(message.Description) > 255 {
		return "description is more than 255 characters"
	}

	return ""
}
