package cluster

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
	"terminus.io/dice/dice/colony/officer/models/cluster/platform"
	"terminus.io/dice/dice/colony/officer/models/cluster/resource"
	"terminus.io/dice/dice/colony/officer/models/cluster/system"
	"terminus.io/dice/dice/colony/officer/settings"
	"terminus.io/dice/dice/pkg/httpclient"
)

type Cluster struct {
	ID             int                `json:"id"`
	OrgID          int                `json:"orgID"`
	Name           string             `json:"name"`
	Type           string             `json:"type"` // k8s, dcos, edas
	Logo           string             `json:"logo"`
	Description    string             `json:"description"`
	WildcardDomain string             `json:"wildcardDomain"`
	URLs           map[string]string  `json:"urls"`
	Resource       *resource.Aliyun   `json:"resource"`
	System         *system.System     `json:"system"`
	Platform       *platform.Platform `json:"platform"`
	CreatedAt      time.Time          `json:"createdAt"`
	UpdatedAt      time.Time          `json:"updatedAt"`
}

type AddClusterRequest struct {
	OrgID          int    `json:"orgID"`
	Name           string `json:"Name"`
	Type           string `json:"type"`
	Logo           string `json:"logo"`
	Description    string `json:"description"`
	HTTPS          bool   `json:"https"`
	WildcardDomain string `json:"wildcardDomain"`
}

type SchedulerRequest struct {
	Cluster string            `json:"cluster"`
	Options map[string]string `json:"options"`
}

const SQLGetClustersByOrgID = `SELECT
	id,
	name,
	type,
	wildcard_domain,
	urls,
	logo,
	description,
	created_at,
	updated_at
FROM co_clusters
WHERE org_id = ?
ORDER BY created_at desc`

func GetClustersByOrgID(orgID int) (a []settings.M, err error) {
	// TODO: cache

	db, err := settings.Open()
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	rows, err := db.QueryContext(ctx, SQLGetClustersByOrgID, orgID)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = rows.Close()
		if err != nil {
			a = nil
		}
	}()

	var r Cluster
	var ub []byte

	for rows.Next() {
		err = rows.Scan(
			&r.ID,
			&r.Name,
			&r.Type,
			&r.WildcardDomain,
			&ub,
			&r.Logo,
			&r.Description,
			&r.CreatedAt,
			&r.UpdatedAt,
		)
		err = json.Unmarshal(ub, &r.URLs)
		if err != nil {
			return nil, err
		}
		if err != nil {
			return nil, err
		}

		var https bool
		if strings.Contains(r.URLs["dcos"], "https") {
			https = true
		}
		m := settings.M{
			"id":             r.ID,
			"name":           r.Name,
			"type":           r.Type,
			"wildcardDomain": r.WildcardDomain,
			"logo":           r.Logo,
			"description":    r.Description,
			"https":          https,
			"createdAt":      r.CreatedAt,
			"updatedAt":      r.UpdatedAt,
		}
		a = append(a, m)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return a, nil
}

const SQLGetClusterBy = `SELECT
	id,
	org_id,
	name,
	type,
	logo,
	description,
	wildcard_domain,
	urls,
	` + "`system`" + `,
	created_at,
	updated_at
FROM co_clusters
WHERE %s = ?` // id or name

func getCluster(c string, v interface{}) (*Cluster, error) {
	// TODO: cache

	db, err := settings.Open()
	if err != nil {
		return nil, err
	}

	var row Cluster
	var ub, sb []byte

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = db.QueryRowContext(ctx, fmt.Sprintf(SQLGetClusterBy, c), v).Scan(
		&row.ID,
		&row.OrgID,
		&row.Name,
		&row.Type,
		&row.Logo,
		&row.Description,
		&row.WildcardDomain,
		&ub,
		&sb,
		&row.CreatedAt,
		&row.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	if len(ub) > 0 {
		err = json.Unmarshal(ub, &row.URLs)
		if err != nil {
			return nil, err
		}
	} else {
		row.URLs = make(map[string]string)
	}
	if len(sb) > 0 {
		err = json.Unmarshal(sb, &row.System)
		if err != nil {
			return nil, err
		}
	} else {
		row.System = nil
	}

	return &row, nil
}

func GetClusterByID(id int) (*Cluster, error) {
	return getCluster("id", id)
}

func GetClusterByName(name string) (*Cluster, error) {
	return getCluster("name", name)
}

const SQLFindCluster = `SELECT org_id FROM co_clusters where name = ?`

const SQLAddCluster = `INSERT INTO co_clusters (
  org_id,
  name,
  type,
  logo,
  description,
  wildcard_domain,
  urls
) VALUES (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)`

const SQLUpdateCluster = `UPDATE co_clusters SET type = ?,
  logo = ?,
  description = ?,
  wildcard_domain = ?,
  urls = ? where name = ?`

func (req AddClusterRequest) Insert(r *http.Request) (int, error) {
	db, err := settings.Open()
	if err != nil {
		return 0, err
	}

	urls := make(map[string]string, len(settings.URLs))
	for k, v := range settings.URLs {
		if k == "dcos" {
			if req.HTTPS && strings.Contains(v, "[proto]") {
				dcosUrl := strings.Replace(v, "[proto]", "https", -1)
				urls[k] = strings.Replace(dcosUrl, "[domain]", req.WildcardDomain, -1)
			}
			if req.HTTPS && strings.Contains(v, "http") {
				urls[k] = strings.Replace(v, "http", "https", -1)
			}
			if !req.HTTPS && strings.Contains(v, "https") {
				urls[k] = strings.Replace(v, "https", "http", -1)
			}
		} else {
			urls[k] = strings.Replace(v, "[domain]", req.WildcardDomain, -1)
		}
	}
	b, err := json.Marshal(urls)
	if err != nil {
		return 0, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	var result sql.Result
	var name string
	err = db.QueryRowContext(ctx, SQLFindCluster, req.Name).Scan(&name)
	if r.Method == "PUT" {
		if err == sql.ErrNoRows {
			return 0, errors.New("not a valid cluster name!")
		}

		result, err = db.ExecContext(ctx, SQLUpdateCluster,
			req.Type,
			req.Logo,
			req.Description,
			req.WildcardDomain,
			b,
			req.Name,
		)
		if err != nil {
			return 0, err
		}
	} else if r.Method == "POST" {
		if err == nil {
			return 0, errors.New("existing cluster name!")
		}

		result, err = db.ExecContext(ctx, SQLAddCluster,
			req.OrgID,
			req.Name,
			req.Type,
			req.Logo,
			req.Description,
			req.WildcardDomain,
			b,
		)
		if err != nil {
			return 0, err
		}
	} else {
		return 0, errors.New("invalid http method!")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}

func (req SchedulerRequest) CallScheduler() error {
	resp, err := httpclient.New().Post(settings.SchedulerAddr).Path("/clusters").JSONBody(req).Do().DiscardBody()
	if err != nil {
		return err
	}
	if !resp.IsOK() {
		return errors.New("create cluster failed from scheduler..")
	}
	return nil
}
