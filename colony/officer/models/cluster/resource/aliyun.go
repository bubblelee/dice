package resource

type VPC struct {
	CIDRBlock string `json:"cidrBlock"`
}

type ECS struct {
	NodeType           string `json:"nodeType"`           // master, private, public
	InstanceType       string `json:"instanceType"`       // https://help.aliyun.com/document_detail/25378.html
	InstanceChargeType string `json:"instanceChargeType"` // PrePaid, PostPaid
	SystemDiskSize     int    `json:"systemDiskSize"`
	Amount             int    `json:"amount"`
}

type RDS struct {
	EngineVersion     string            `json:"engineVersion"`   // 5.5, 5.6, 5.7
	DBInstanceClass   string            `json:"dbInstanceClass"` // https://help.aliyun.com/document_detail/26312.html
	DBInstanceStorage int               `json:"dbInstanceStorage"`
	PayType           string            `json:"payType"` // Postpaid, Prepaid
	AccountName       string            `json:"accountName"`
	DBName            string            `json:"dbName"`
	Parameters        map[string]string `json:"parameters"`
}

type Redis struct {
	InstanceClass string `json:"instanceClass"` // https://help.aliyun.com/document_detail/61135.html
	ChargeType    string `json:"chargeType"`    // PrePaid, PostPaid
	EngineVersion string `json:"engineVersion"` // 2.8, 4.0
}

type Aliyun struct {
	AccessKeyId     string  `json:"accessKeyId"`
	AccessKeySecret string  `json:"accessKeySecret"`
	RegionId        string  `json:"regionId"`
	ZoneId          string  `json:"zoneId"`
	VPC             VPC     `json:"vpc"`
	ECS             []ECS   `json:"ecs"`
	RDS             []RDS   `json:"rds"`
	Redis           []Redis `json:"redis"`
}
