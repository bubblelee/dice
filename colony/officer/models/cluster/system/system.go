package system

import (
	"fmt"
	"sort"
	"strings"
	"time"
)

type SSH struct {
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
}

type FPS struct {
	Host  string `json:"host"`
	Port  int    `json:"port"`
	Proxy bool   `json:"proxy"`
}

func (x FPS) Addr() string {
	return fmt.Sprintf("%s:%d", x.Host, x.Port)
}

type Docker struct {
	DataRoot  string `json:"dataRoot"`
	BIP       string `json:"bip"`
	FixedCIDR string `json:"fixedCIDR"`
}

type Gluster struct {
	Version string   `json:"version"`
	Hosts   []string `json:"hosts"`
	Server  bool     `json:"server"`
	Brick   string   `json:"brick"`
}

type Storage struct {
	NAS        string  `json:"nas"`
	Gluster    Gluster `json:"gluster"`
	MountPoint string  `json:"mountPoint"`
}

type Overlay struct {
	Name   string `json:"name"`
	Subnet string `json:"subnet"`
	Prefix int    `json:"prefix"`
}

type DCOS struct {
	Version   string    `json:"version"`
	Resolvers []string  `json:"resolvers"`
	Overlays  []Overlay `json:"overlays"`
}

type Cluster struct {
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"createdAt"`
}

type Node struct {
	Type string `json:"type"` // master, public, private
	IP   string `json:"ip"`
	Tag  string `json:"tag"`
}

func (x Node) Name(c string) string {
	a := strings.Split(x.IP, ".")
	return fmt.Sprintf("%s-%s-%03s%03s%03s%03s", c, x.Type, a[0], a[1], a[2], a[3])
}

type Nodes []Node

func (a Nodes) Master() Node {
	for _, n := range a {
		if n.Type == "master" {
			return n
		}
	}
	panic("no master")
}

func (a Nodes) Len() int {
	return len(a)
}

func (a Nodes) Less(i, j int) bool {
	if a[i].Type < a[j].Type {
		return true
	} else if a[i].Type > a[j].Type {
		return false
	}
	return a[i].IP < a[j].IP
}

func (a Nodes) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type System struct {
	SSH      SSH      `json:"ssh"`
	FPS      FPS      `json:"fps"`
	Storage  Storage  `json:"storage"`
	Docker   Docker   `json:"docker"`
	DCOS     DCOS     `json:"dcos"`
	Account  string   `json:"account"`
	Cluster  *Cluster `json:"cluster,omitempty"`
	Nodes    Nodes    `json:"nodes,omitempty"`
	NewNodes Nodes    `json:"-"`
}

func (x System) PrepareHosts() []string {
	m := make(map[string]struct{}, len(x.Nodes))
	for _, n := range x.Nodes {
		m[n.IP] = struct{}{}
	}
	if x.Storage.Gluster.Server {
		for _, host := range x.Storage.Gluster.Hosts {
			m[host] = struct{}{}
		}
	}
	a := make([]string, 0, len(m))
	for host := range m {
		a = append(a, host)
	}
	sort.Strings(a)
	return a
}
