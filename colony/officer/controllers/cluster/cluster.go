package cluster

import (
	"encoding/json"
	"net/http"
	"strconv"

	"terminus.io/dice/dice/colony/officer/models/cluster"
	"terminus.io/dice/dice/colony/officer/settings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func Route(r *mux.Router) {
	r.Methods("GET").Path("/clusters/{idOrName}").HandlerFunc(getCluster)
	r.Methods("GET").Path("/clusters").HandlerFunc(getOrgClusters)
	r.Methods("POST").Path("/clusters").HandlerFunc(addCluster)
	r.Methods("PUT").Path("/clusters").HandlerFunc(addCluster)
}

func WriteJSON(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	b, err := json.Marshal(v)
	if err != nil {
		logrus.Debugln(err)
	}
	_, err = w.Write(b)
	if err != nil {
		logrus.Debugln(err)
	}
}

func WriteData(w http.ResponseWriter, v interface{}) {
	WriteJSON(w, settings.M{
		"success": true,
		"data":    v,
	})
}

func WriteErr(w http.ResponseWriter, code, msg string) {
	WriteJSON(w, settings.M{
		"success": false,
		"err": settings.M{
			"code": code,
			"msg":  msg,
		},
	})
}

func getOrgClusters(w http.ResponseWriter, r *http.Request) {
	orgID, err := strconv.Atoi(r.FormValue("orgID"))
	if err != nil {
		WriteErr(w, "400", "invalid org id")
		return
	}

	rows, err := cluster.GetClustersByOrgID(orgID)
	if err != nil {
		WriteErr(w, "500", err.Error())
		return
	}

	WriteData(w, rows)
}

func getCluster(w http.ResponseWriter, r *http.Request) {
	var row *cluster.Cluster

	s := mux.Vars(r)["idOrName"]

	id, err := strconv.Atoi(s)
	if err == nil {
		row, err = cluster.GetClusterByID(id)
	} else if cluster.IsName(s) {
		row, err = cluster.GetClusterByName(s)
	} else {
		WriteErr(w, "400", "neither id nor name")
		return
	}
	if err != nil {
		WriteErr(w, "500", err.Error())
		return
	}

	WriteData(w, row)
}

func addCluster(w http.ResponseWriter, r *http.Request) {
	var request cluster.AddClusterRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		WriteErr(w, "400", err.Error())
		return
	}
	if err := cluster.IsValid(request); err != "" {
		WriteErr(w, "400", err)
		return
	}
	id, err := request.Insert(r)
	if err != nil {
		WriteErr(w, "500", err.Error())
		return
	}
	proto := "http"
	if request.HTTPS {
		proto = "https"
	}
	schedulerRequest := cluster.SchedulerRequest{
		Cluster: request.Name,
		Options: map[string]string{
			"ADDR": proto + "://dcos." + request.WildcardDomain,
		},
	}
	err = schedulerRequest.CallScheduler()
	if err != nil {
		WriteErr(w, "500", err.Error())
		return
	}
	WriteData(w, id)
}
