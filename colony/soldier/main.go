package main

import (
	"net/http"

	"terminus.io/dice/dice/colony/soldier/settings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {
	settings.Init()

	r := mux.NewRouter()
	r.HandleFunc("/terminal", terminal)
	//r.Handle("/{f:(?:.+)}", http.FileServer(http.Dir("assets")))

	go func() {
		logrus.Infoln("http serve", settings.HTTPAddr)
		http.Handle("/", r)
		err := http.ListenAndServe(settings.HTTPAddr, r)
		if err != nil {
			logrus.Fatalln(err)
		}
	}()

	settings.Wait()
}
