package settings

import (
	"io/ioutil"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	HTTPAddr = ":9028"
	LogLevel = logrus.InfoLevel
	PIDFile  = ""
)

func init() {
	if s := os.Getenv("HTTP_ADDR"); s != "" {
		HTTPAddr = s
	}
	if s := os.Getenv("LOG_LEVEL"); s != "" {
		var err error
		LogLevel, err = logrus.ParseLevel(s)
		if err != nil {
			logrus.Fatalln(err)
		}
	}
	if s := os.Getenv("PID_FILE"); s != "" {
		PIDFile = s
	}
}

func Init() {
	logrus.SetLevel(LogLevel)

	if PIDFile != "" {
		err := ioutil.WriteFile(PIDFile, []byte(strconv.Itoa(os.Getpid())), 0644)
		if err != nil {
			logrus.Fatalln(err)
		}
	}
}

var (
	exitChan                 = make(chan struct{})
	ExitChan <-chan struct{} = exitChan
)

func Wait() {
	nc := make(chan os.Signal, 1)
	signal.Notify(nc, syscall.SIGINT, syscall.SIGTERM)
	logrus.Infoln("wait signal")
	<-nc
	logrus.Infoln("signal notify")

	close(exitChan)
	time.Sleep(time.Second)

	if PIDFile != "" {
		err := os.Remove(PIDFile)
		if err != nil {
			logrus.Errorln(err)
		}
	}
}
