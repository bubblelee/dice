package conf

import (
	"errors"
	"os"
	"strconv"
	"strings"
	"sync"
)

type Conf struct {
	ListenAddr    string `default:":9528", env:"LISTEN_ADDR"`
	PoolSize      int    `default:"100", env:"POOL_SIZE"`
	SchedulerAddr string `default:"127.0.0.1:6666", env:"SCHEDULER_ADDR"`
	MysqlAddr     string `default:"rm-bp17ar40w6824r8m0o.mysql.rds.aliyuncs.com", env:"MYSQL_ADDR"`
	DbName        string `default:"dice-test", env:"DBNAME"`
	DbUser        string `default:"dice", env:"DBUSER"`
	DbPWD         string `default:"Hello1234", env:"DBPWD"`
	Debug         bool   `default:"true", env:"DEBUG"`
}

var (
	C                 Conf
	ListenAddrLock    sync.Once
	PoolSizeLock      sync.Once
	SchedulerAddrLock sync.Once
	MysqlAddrLock     sync.Once
	DbNameLock        sync.Once
	DbUserLock        sync.Once
	DbPWDLock         sync.Once
	DebugLock         sync.Once
)

func boolFromString(s string) (bool, error) {
	switch strings.ToLower(s) {
	case "true", "t":
		return true, nil
	case "false", "f":
		return false, nil
	}
	return false, errors.New("illegal bool tag value")
}

func ListenAddr() string {
	ListenAddrLock.Do(func() {
		e := os.Getenv("LISTEN_ADDR")
		if e == "" {
			e = ":9528"
		}

		i := e

		C.ListenAddr = i
	})
	return C.ListenAddr
}

func PoolSize() int {
	PoolSizeLock.Do(func() {
		e := os.Getenv("POOL_SIZE")
		if e == "" {
			e = "100"
		}

		i, err := strconv.Atoi(e)
		if err != nil {
			panic("conf PoolSize parse failed!")
		}

		C.PoolSize = i
	})
	return C.PoolSize
}

func SchedulerAddr() string {
	SchedulerAddrLock.Do(func() {
		e := os.Getenv("SCHEDULER_ADDR")
		if e == "" {
			e = "127.0.0.1:6666"
		}

		i := e

		C.SchedulerAddr = i
	})
	return C.SchedulerAddr
}

func MysqlAddr() string {
	MysqlAddrLock.Do(func() {
		e := os.Getenv("MYSQL_ADDR")
		if e == "" {
			e = "rm-bp17ar40w6824r8m0o.mysql.rds.aliyuncs.com"
		}

		i := e

		C.MysqlAddr = i
	})
	return C.MysqlAddr
}

func DbName() string {
	DbNameLock.Do(func() {
		e := os.Getenv("DBNAME")
		if e == "" {
			e = "dice-test"
		}

		i := e

		C.DbName = i
	})
	return C.DbName
}

func DbUser() string {
	DbUserLock.Do(func() {
		e := os.Getenv("DBUSER")
		if e == "" {
			e = "dice"
		}

		i := e

		C.DbUser = i
	})
	return C.DbUser
}

func DbPWD() string {
	DbPWDLock.Do(func() {
		e := os.Getenv("DBPWD")
		if e == "" {
			e = "Hello1234"
		}

		i := e

		C.DbPWD = i
	})
	return C.DbPWD
}

func Debug() bool {
	DebugLock.Do(func() {
		e := os.Getenv("DEBUG")
		if e == "" {
			e = "true"
		}

		i, err := boolFromString(e)
		if err != nil {
			panic("conf Debug parse failed! " + err.Error())
		}

		C.Debug = i
	})
	return C.Debug
}
