package testutil

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/eventbox/conf"
	"terminus.io/dice/dice/eventbox/constant"
	"terminus.io/dice/dice/eventbox/subscriber/fake"
	"terminus.io/dice/dice/eventbox/types"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/pkg/jsonstore"
)

var originListenAddr string

func init() {
	originListenAddr = conf.ListenAddr()
}

var sender = "eventbox-self"
var notifier, _ = api.New(sender, nil)

func GenContent(content string) string {
	return content + fmt.Sprintf("%d", time.Now().UnixNano())
}

func InputEtcd(content string) error {
	return notifier.Send(content, api.WithDest(map[string]interface{}{
		"FAKE": ""}))
}

func InputHTTP(content string) (*httpclient.Response, *bytes.Buffer, error) {
	return InputHTTPRaw(content, originListenAddr[1:], map[types.LabelKey]interface{}{"FAKE": ""})
}

func InputHTTPRaw(content string, port string, dest map[types.LabelKey]interface{}) (*httpclient.Response, *bytes.Buffer, error) {
	var buf bytes.Buffer
	resp, err := httpclient.New().Post("127.0.0.1:"+port).Path("/api/dice/eventbox/message/create").Header("Accept", "application/json").JSONBody(types.Message{
		Sender:  sender,
		Labels:  dest,
		Content: content,
		Time:    time.Now().UnixNano(),
	}).Do().Body(&buf)
	return resp, &buf, err
}

func OutputDD(content string) (*httpclient.Response, *bytes.Buffer, error) {
	return OutputDDWithPort(content, originListenAddr[1:])
}

func OutputDDWithPort(content string, port string) (*httpclient.Response, *bytes.Buffer, error) {
	return InputHTTPRaw(content, port, map[types.LabelKey]interface{}{"DINGDING": []string{"https://oapi.dingtalk.com/robot/send?access_token=361cc157e552b4e10080d680f1fd4beff97722ea329b2da20e171fb2976b873f"}})

}

var (
	fakefile     *os.File
	fakefileLock sync.Once
)

func FakeFileContains(content string) bool {
	s, err := ioutil.ReadFile(fake.FakeTestFilePath)
	if err != nil {
		panic(err)
	}
	all := string(s)
	return strings.Contains(all, content)
}

func IsCleanEtcd() bool {
	js, err := jsonstore.New()
	if err != nil {
		panic(err)
	}
	r, err := js.ListKeys(context.TODO(), constant.MessageDir)
	if err != nil {
		panic(err)
	}
	return len(r) == 0
}

// return (kill_function, error)
func RunAHTTPServer(port int, content string) (func() error, error) {
	content_ := strings.Replace(content, "\"", "\\\"", -1)
	socat := fmt.Sprintf("socat tcp-listen:%d,reuseaddr,fork \"exec:printf \\'HTTP/1.0 200 OK\\r\\nContent-Type\\:application/json\\r\\n\\r\\n%s\\'\"", port, content_)
	cmd := exec.Command("sh", "-c", socat)
	err := cmd.Start()
	time.Sleep(500 * time.Millisecond)
	return cmd.Process.Kill, err
}
