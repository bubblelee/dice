package monitor

import (
	"container/ring"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	std = New()
)

//go:generate stringer -type=InfoType
type InfoType int

const (
	EtcdInput InfoType = iota
	EtcdInputDrop
	HTTPInput
	DINGDINGOutput
	MYSQLOutput
	HTTPOutput
	LastType
)

type MonitorInfo struct {
	Tp        InfoType
	Detail    string
	timestamp time.Time
}

type Monitor struct {
	sync.Mutex
	infoCache map[InfoType]*ring.Ring

	logger *logrus.Logger
}

func New() *Monitor {
	m := &Monitor{logger: logrus.New()}
	m.infoCache = map[InfoType]*ring.Ring{}
	for i := 0; i < int(LastType); i++ {
		m.infoCache[InfoType(i)] = ring.New(100)
	}

	go func() {
		for {
			m.Log()
			m.clear()
			time.Sleep(5 * time.Minute)
		}
	}()
	return m
}

func (m *Monitor) Notify(info MonitorInfo) {
	m.Lock()
	defer m.Unlock()

	info.timestamp = time.Now()
	m.infoCache[info.Tp] = m.infoCache[info.Tp].Next()
	m.infoCache[info.Tp].Value = info
}

func (m *Monitor) Log() {
	m.Lock()
	defer m.Unlock()

	var buf strings.Builder
	for tp, infoRing := range m.infoCache {
		buf.WriteString(fmt.Sprintf("[%s]: ", tp.String()))
		count := 0
		infoRing.Do(func(info interface{}) {
			if info == nil {
				return
			}
			count++
		})
		buf.WriteString(fmt.Sprintf("%d    ", count))
	}
	m.logger.Infof(buf.String())
}

// periodUpdate will clear m.infoCache
func (m *Monitor) clear() {
	m.Lock()
	defer m.Unlock()

	for _, r := range m.infoCache {
		length := r.Len()
		for i := 0; i < length; i++ {
			r.Value = nil
			r = r.Next()
		}
	}
}

func Notify(info MonitorInfo) {
	std.Notify(info)
}
