package monitor

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"time"
)

func TestMonitor(t *testing.T) {
	time.Sleep(100 * time.Millisecond)
	Notify(MonitorInfo{Tp: EtcdInput})
	assert.Equal(t, 100, std.infoCache[EtcdInput].Len())
	std.Log()

}
