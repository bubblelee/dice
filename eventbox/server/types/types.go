package types

import (
	"context"
	"net/http"
)

type Responser interface {
	GetStatus() int
	GetContent() interface{}
}

type Endpoint struct {
	Path    string
	Method  string
	Handler func(context.Context, *http.Request, map[string]string) (Responser, error)
}

type HTTPResponse struct {
	Status  int
	Content interface{}
}

func (r HTTPResponse) GetStatus() int {
	return r.Status
}

func (r HTTPResponse) GetContent() interface{} {
	return r.Content
}
