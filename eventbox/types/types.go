package types

import (
	"strings"
)

type LabelKey string

type Message struct {
	Sender  string                   `json:"sender"`
	Content interface{}              `json:"content"`
	Labels  map[LabelKey]interface{} `json:"labels"`
	Time    int64                    `json:"time,omitempty"` // UnixNano
}

func (m *Message) Before(t int64) bool {
	return m.Time < t
}

func (k LabelKey) HasPrefix(s string) bool {
	k_ := k.Normalize()
	s_ := LabelKey(s).Normalize()
	return strings.HasPrefix(string(k_), s_)
}

func (k LabelKey) Normalize() string {
	if !strings.HasPrefix(string(k), "/") {
		return "/" + string(k)
	}
	return string(k)
}

func (k LabelKey) NormalizeLabelKey() LabelKey {
	return LabelKey(k.Normalize())
}
