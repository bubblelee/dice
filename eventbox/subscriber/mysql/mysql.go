/*
不同于其他 subscriber
mysqlsubscriber 得关心 publish content 的内容，并且有一定的约束

规定 content 的格式为：

map[string]string

分别是键值对

*/
package mysql

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"terminus.io/dice/dice/eventbox/conf"
	"terminus.io/dice/dice/eventbox/monitor"
	"terminus.io/dice/dice/eventbox/subscriber"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type MysqlDest string // just a table name

type MysqlContent map[string]string

type MysqlSubscriber struct {
	db *sql.DB
}

func New() (subscriber.Subscriber, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s", conf.DbUser(), conf.DbPWD(), conf.MysqlAddr(), conf.DbName())
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	return &MysqlSubscriber{db: db}, nil
}

func (m *MysqlSubscriber) Publish(dest string, content string, time int64) []error {
	monitor.Notify(monitor.MonitorInfo{Tp: monitor.MYSQLOutput})

	var content_ MysqlContent
	if err := json.Unmarshal([]byte(content), &content_); err != nil {
		return []error{errors.Wrap(err, "unmarshal content failed")}
	}
	var table MysqlDest
	if err := json.NewDecoder(strings.NewReader(dest)).Decode(&table); err != nil {
		return []error{errors.Wrap(err, "decode dest failed")}
	}
	err := insert(m.db, string(table), content_)
	if err != nil {
		return []error{err}
	}
	return []error{}
}

func (m *MysqlSubscriber) Status() interface{} {
	return nil
}

func (m *MysqlSubscriber) Name() string {
	return "MYSQL"
}

func insert(db *sql.DB, table string, content MysqlContent) error {
	keys := []string{}
	values := []interface{}{}
	template := "`%s`=?"

	for k, v := range content {
		k_ := fmt.Sprintf(template, k)
		keys = append(keys, k_)
		values = append(values, v)
	}
	stmt := "INSERT " + table + " SET " + strings.Join(keys, ",")

	statment, err := db.Prepare(stmt)
	if err != nil {
		return err
	}
	defer func() {
		if err := statment.Close(); err != nil {
			logrus.Errorf("mysql statment close failed: %v", err)
		}
	}()
	_, err = statment.Exec(values...)
	if err != nil {
		return err
	}
	return nil
}
