package http

import (
	"bytes"
	"encoding/json"
	"net/url"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/monitor"
	"terminus.io/dice/dice/eventbox/subscriber"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	defaultPublishTimeout = 5 * time.Second
)

// urls
type Dest []string

type HTTPSubscriber struct{}

func New() subscriber.Subscriber {
	return &HTTPSubscriber{}
}

func (s *HTTPSubscriber) Publish(dest string, content string, timestamp int64) []error {
	monitor.Notify(monitor.MonitorInfo{Tp: monitor.HTTPOutput})

	var d Dest
	dest_ := []byte(dest)
	if err := json.NewDecoder(bytes.NewReader(dest_)).Decode(&d); err != nil {
		return []error{err}
	}
	errs := make(chan error, len(d))
	var wg sync.WaitGroup
	wg.Add(len(d))
	for _, url_ := range d {
		go func() {
			defer wg.Done()
			if !strings.HasPrefix(url_, "http") {
				url_ = "http://" + url_
			}
			parsedUrl, err := url.Parse(url_)
			if err != nil {
				errs <- errors.Wrapf(err, "url: %s", url_)
				return
			}
			logrus.Debugf("http request url: %s", url_)
			buf := bytes.NewBufferString(content)
			resp, err := httpclient.New().Post(parsedUrl.Host).Path(parsedUrl.Path).
				Header("Content-Type", "application/json").RawBody(buf).Do().DiscardBody()
			if err != nil {
				errs <- errors.Wrapf(err, "url: %s", url_)
				return
			}
			if !resp.IsOK() {
				errs <- errors.Errorf("url: %s, response: %d", url_, resp.StatusCode())
			}
		}()
	}
	wg.Wait()
	close(errs)
	es := []error{}
	for e := range errs {
		es = append(es, e)
	}
	return es
}

func (s *HTTPSubscriber) Status() interface{} {
	return nil
}

func (s *HTTPSubscriber) Name() string {
	return "HTTP"
}
