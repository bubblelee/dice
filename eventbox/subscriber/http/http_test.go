package http

import (
	"encoding/json"
	"testing"

	. "terminus.io/dice/dice/eventbox/testutil"

	"github.com/stretchr/testify/assert"
)

func TestHTTPOutput(t *testing.T) {
	sub := New()

	kill, err := RunAHTTPServer(12345, "")
	assert.Nil(t, err)
	defer kill()

	dest, err := json.Marshal([]string{"http://127.0.0.1:12345/aaa"})
	assert.Nil(t, err)

	errs := sub.Publish(string(dest), "content", 666)

	assert.True(t, len(errs) == 0)
}

func TestURLNotStartWithSchema(t *testing.T) {
	sub := New()

	kill, err := RunAHTTPServer(12345, "")
	assert.Nil(t, err)
	defer kill()

	dest, err := json.Marshal([]string{"127.0.0.1:12345/aaa"})
	assert.Nil(t, err)
	errs := sub.Publish(string(dest), "content", 666)

	assert.True(t, len(errs) == 0)
}
