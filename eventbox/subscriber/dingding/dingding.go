package dingding

import (
	"bytes"
	"encoding/json"
	"net/url"

	"terminus.io/dice/dice/eventbox/monitor"
	"terminus.io/dice/dice/eventbox/subscriber"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

// example dingding message:
// {
//      "msgtype": "text",
//      "text": {
//          "content": "我就是我,  @1825718XXXX 是不一样的烟火"
//      },
//      "at": {
//          "atMobiles": [
//              "1825718XXXX"
//          ],
//          "isAtAll": false
//      }
//  }
type DDContent struct {
	Content string `json:"content"`
}
type DDAt struct {
	AtMobiles []string `json:"atMobiles"`
	IsAtAll   bool     `json:"isAtAll"`
}

type DDMessage struct {
	Msgtype string    `json:"msgtype"`
	Text    DDContent `json:"text"`
	At      DDAt      `json:"at"`
}

type DDDest []string

type DDSubscriber struct{}

func New() subscriber.Subscriber {
	return &DDSubscriber{}
}

// example URL:https://oapi.dingtalk.com/robot/send?access_token=361cc157e552b4e10080d680f1fd4beff97722ea329b2da20e171fb2976b873f
func (d *DDSubscriber) Publish(dest string, content string, time int64) []error {
	monitor.Notify(monitor.MonitorInfo{Tp: monitor.DINGDINGOutput})
	errs := []error{}

	var content_ string
	if err := json.Unmarshal([]byte(content), &content_); err != nil {
		// content 可能不是 string json， 所以 err 也没关系
	} else {
		// 对于 string json， 使钉钉显示的时候不带2个引号
		content = content_
	}
	m := DDMessage{
		Msgtype: "text",
		Text:    DDContent{content},
		At:      DDAt{[]string{}, false},
	}
	dest_ := []byte(dest)
	var urls DDDest
	if err := json.NewDecoder(bytes.NewReader(dest_)).Decode(&urls); err != nil {
		return []error{errors.Wrap(err, "DingDing publish dest illegal")}
	}
	for _, u := range urls {
		parsed, err := url.Parse(u)
		if err != nil {
			err = errors.Errorf("DingDing publish: %v, err:%v", u, err)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		token_, ok := parsed.Query()["access_token"]
		if !ok {
			err = errors.Errorf("DingDing publish: %v, not provide access_token", u)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		if len(token_) == 0 {
			err = errors.Errorf("DingDing publish: %v, not provide access_token", u)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		token := token_[0]
		var buf bytes.Buffer
		resp, err := httpclient.New(httpclient.WithHTTPS()).
			Post(parsed.Host).
			Path(parsed.Path).
			Param("access_token", token).
			Header("Content-Type", "application/json;charset=utf-8").
			JSONBody(m).
			Do().Body(&buf)
		if err != nil {
			err = errors.Errorf("DingDing publish: %v , err:%v", u, err)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		if !resp.IsOK() {
			err = errors.Errorf("DingDing publish: %v, httpcode:%", u, resp.StatusCode())
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		body, err := ioutil.ReadAll(&buf)
		if err != nil {
			err = errors.Errorf("DingDing publish: %v, err: %v", u, err)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		var v map[string]interface{}
		if err := json.Unmarshal(body, &v); err != nil {
			err = errors.Errorf("DingDing publish: %v, err: %v", u, err)
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
		errcode, ok := v["errcode"]
		if !ok {
			logrus.Warningf("DingDing publish: %v, no errcode in response body: %v", u, string(body))
			continue
		}
		errmsg, ok := v["errmsg"]
		if !ok {
			logrus.Warningf("DingDing publish: %v, no errmsg in response body: %v", u, string(body))
			continue
		}
		errcode_ := int(errcode.(float64))
		if errcode_ != 0 {
			err = errors.Errorf("DingDing publish: %v, DingDing errcode: %d, errmsg: %s", u, errcode_, errmsg.(string))
			logrus.Error(err)
			errs = append(errs, err)
			continue
		}
	}
	return errs
}

func (d *DDSubscriber) Status() interface{} {
	return nil
}

func (d *DDSubscriber) Name() string {
	return "DINGDING"
}
