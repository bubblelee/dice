// +build dingding

package dingding

import (
	"encoding/json"
	"math/rand"
	"sync"
	"testing"
	"time"

	. "terminus.io/dice/dice/eventbox/testutil"

	"github.com/stretchr/testify/assert"
)

var ddUrl = "https://oapi.dingtalk.com/robot/send?access_token=361cc157e552b4e10080d680f1fd4beff97722ea329b2da20e171fb2976b873f"

// 根据钉钉文档， 每分钟限制 20 条消息
func TestSendMany(t *testing.T) {
	sub := New()
	es := make(chan []error, 20)
	var wg sync.WaitGroup
	rand.Seed(time.Now().Unix())
	count := 3
	wg.Add(count)
	f := func() {
		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		urls, err := json.Marshal([]string{ddUrl})
		assert.Nil(t, err)
		es <- sub.Publish(string(urls), GenContent("test"), time.Now().UnixNano())
		wg.Done()
	}
	for i := 0; i < count; i++ {
		go f()
	}

	go func() {
		wg.Wait()
		close(es)
	}()
	for e := range es {
		assert.Equal(t, 0, len(e))
	}
}
