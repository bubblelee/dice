# 这个文件放一些用来测试的 curl 命令等

# dingding
curl -XPOST 'http://diceeventbox.marathon.l4lb.thisdcos.directory:9528/api/dice/eventbox/message/create' -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{"sender":"curl", "content":[1,2,3], "labels":{"DINGDING":["https://oapi.dingtalk.com/robot/send?access_token=361cc157e552b4e10080d680f1fd4beff97722ea329b2da20e171fb2976b873f"]}}'


# mysql
curl -XPOST 'http://diceeventbox.marathon.l4lb.thisdcos.directory:9528/api/dice/eventbox/message/create' -H 'Accept: application/json' -H 'Content-Type: application/json' -d '{"sender":"curl", "content":{"target_id" : "1", "type" : "P", "operator" : "operator1", "desc" : "desc1", "action": "action1" , "context": "context1"}, "labels":{"MYSQL":"ps_audit_logs"}}'
