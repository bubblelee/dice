// default register labels here
package label

import (
	"terminus.io/dice/dice/eventbox/types"
)

var DefaultLabels = map[types.LabelKey]map[types.LabelKey]interface{}{
	"/TEST_LABEL": {"FAKE": "", "label2": ""},
}
