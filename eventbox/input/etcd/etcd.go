package etcd

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/constant"
	"terminus.io/dice/dice/eventbox/dispatcher/errors"
	"terminus.io/dice/dice/eventbox/input"
	"terminus.io/dice/dice/eventbox/monitor"
	"terminus.io/dice/dice/eventbox/types"
	"terminus.io/dice/dice/pkg/dlock"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/pkg/jsonstore/storetypes"

	"github.com/sirupsen/logrus"
)

type EtcdInput struct {
	js      jsonstore.JsonStoreWithWatch
	handler func(*types.Message) *errors.DispatchError
	// 保证只有1个 eventbox 实例能 watch
	lock *dlock.DLock
	// 用来通知 stop
	stopCh    chan struct{}
	runningWg sync.WaitGroup
	// 用于过滤相同事件, 使用 jsonstore lru backend, key: content-md5, value: 第一次出现的unixnano时间
	filter jsonstore.JsonStore
}

func New() (input.Input, error) {
	js, err := jsonstore.New()
	if err != nil {
		return nil, err
	}
	js_ := js.(jsonstore.JsonStoreWithWatch)
	lock, err := dlock.New(constant.EtcdLockKey)
	if err != nil {
		return nil, err
	}
	filter, err := jsonstore.New(
		jsonstore.UseLruStore(100),
		jsonstore.UseMemStore())
	if err != nil {
		return nil, err
	}
	return &EtcdInput{
		js:     js_,
		stopCh: make(chan struct{}),
		lock:   lock,
		filter: filter,
	}, nil
}

func (e *EtcdInput) Name() string {
	return "ETCD"
}

// 处理 etcd 中已经存在的消息:
// 重新 etcd.put 一遍所有存在的消息
// 使 watch 能监听到
// NOTE: 只 put 在当前时间之前的消息
func (e *EtcdInput) restore(watching chan int64) {
	logrus.Info("Etcdinput restore start")
	defer logrus.Info("Etcdinput restore end")
	ctx := context.Background()
	var before int64
	// TODO:  better impl?
	// put a useless message to make sure etcd watch has started
	for {
		time.Sleep(500 * time.Millisecond)
		e.js.Put(ctx, constant.MessageDir+"/000",
			types.Message{
				Sender: "eventbox-self",
				Labels: map[types.LabelKey]interface{}{
					"FAKE": "restore",
				},
				Content: "restore",
				Time:    0})
		var ok bool
		before, ok = <-watching
		if ok {
			break
		}

	}
	e.js.ForEach(ctx, constant.MessageDir, types.Message{},
		func(k string, v interface{}) error {
			if !(v.(*types.Message)).Before(before) {
				return nil
			}
			if err := e.js.Put(ctx, k, v); err != nil {
				logrus.Errorf("Etcdinput restore: %v", err)
			}
			return nil
		})
}
func (e *EtcdInput) retry(f func() error) {
	err := f()
	if err != nil {
		logrus.Errorf("Etcdinput: %v", err)
		e.retry(f)
	}
}

func (e *EtcdInput) Start(handler input.Handler) error {
	e.handler = handler
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		<-e.stopCh
		cancel()
	}()
	cleanup, err := input.OnlyOne(ctx, e.lock)
	if err != nil {
		logrus.Errorf("Etcdinput etcdlock: %v", err)
	}

	watching := make(chan int64)
	var watchingOnce sync.Once
	f := func() error {
		err := e.js.Watch(ctx, constant.MessageDir, true, true, false, types.Message{},
			func(key string, value interface{}, _ storetypes.ChangeType) error {
				watchingOnce.Do(func() {
					watching <- time.Now().UnixNano()
					close(watching)
					logrus.Info("Etcdinput: start watching")
				})
				defer func() {
					var m types.Message
					err := e.js.Remove(ctx, key, &m)
					if err != nil {
						logrus.Errorf("Etcdinput: remove key(%s) failed: %v", key, err)
					}
				}()
				message := value.(*types.Message)
				monitor.Notify(monitor.MonitorInfo{Tp: monitor.EtcdInput})
				if !filter(e.filter, message) {
					// drop
					monitor.Notify(monitor.MonitorInfo{Tp: monitor.EtcdInputDrop})
					return nil
				}
				go func() {
					derr := e.handler(message)
					if derr != nil && !derr.IsOK() {
						logrus.Errorf("%v", derr)
					}
				}()
				return nil
			})
		return err
	}
	go e.restore(watching)
	e.runningWg.Add(1)
	e.retry(f)
	cleanup()
	e.runningWg.Done()
	e.runningWg.Wait()
	logrus.Info("Etcdinput start() done")
	return nil
}

func (e *EtcdInput) Stop() error {
	e.stopCh <- struct{}{}
	logrus.Info("Etcdinput: stopping")
	e.runningWg.Wait()
	logrus.Info("Etcdinput: stopped")

	return nil
}

// 过滤 5s 内相同的 message
// true: pass
// false: filter
func filter(lru jsonstore.JsonStore, m *types.Message) bool {
	h := md5.New()
	m_ := *m
	m_.Time = 0 // when compute md5, ignore Time field
	content, err := json.Marshal(m_)
	if err != nil {
		logrus.Errorf("Etcdinput: filter: %v", err)
		return true
	}
	h.Write(content)
	md5 := string(h.Sum(nil))
	t := new(int64)
	if err := lru.Get(context.Background(), md5, t); err != nil {
		// not found same message in filter, so is pass, and put it in filter
		if err := lru.Put(context.Background(), md5, time.Now().UnixNano()); err != nil {
			logrus.Errorf("Etcdinput: filter: put %s failed, %v", md5, err)
		}
		return true
	}
	t_ := time.Unix(0, *t)
	if !t_.After(time.Now().Add(-5 * time.Second)) {
		// too old , delete it
		if err := lru.Remove(context.Background(), md5, t); err != nil {
			logrus.Errorf("Etcdinput: filter: remove %s failed, %v", md5, err)
		}
		// add current
		if err := lru.Put(context.Background(), md5, time.Now().UnixNano()); err != nil {
			logrus.Errorf("Etcdinput: filter: put %s failed, %v", md5, err)
		}
		return true
	}

	// here, we found the same message in filter, so drop it
	return false
}
