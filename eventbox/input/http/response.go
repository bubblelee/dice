package http

import (
	"net/http"

	"terminus.io/dice/dice/eventbox/dispatcher/errors"
	"terminus.io/dice/dice/eventbox/server/types"

	"github.com/sirupsen/logrus"
)

func genResponse(dispatchErrs *errors.DispatchError) types.HTTPResponse {
	if len(dispatchErrs.BackendErrs) > 0 {
		logrus.Errorf("dispatcher backenderr: %v", dispatchErrs.BackendErrs)
		return types.HTTPResponse{http.StatusBadRequest, dispatchErrs.BackendErrs}
	}
	if dispatchErrs.FilterErr != nil {
		logrus.Errorf("dispatcher filterErr: %v", dispatchErrs.FilterErr)
		return types.HTTPResponse{http.StatusBadRequest, dispatchErrs.FilterErr.Error()}
	}
	return types.HTTPResponse{http.StatusOK, ""}
}
