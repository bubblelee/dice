package dispatcher

import (
	"terminus.io/dice/dice/eventbox/dispatcher/errors"
	"terminus.io/dice/dice/eventbox/dispatcher/filters"
	"terminus.io/dice/dice/eventbox/types"

	"github.com/sirupsen/logrus"
)

// dispatcher 内部处理 message 的相关逻辑

//               +-------------------------+
//               |     Router              |
//               |                         |
//               |     +-------------+     |      +-------------+
//               |     |             |     |      |  backend    |
// src ----------+---->|      A      +-----+------>  message    |
//               |     |             |     |      |  consumer   |
//               |     +-------------+     |      | e.g.dingding|
//               +-------------------------+      +-------------+
// A: []filter

type Router struct {
	dispatcher *DispatcherImpl
	filters    []filters.Filter
}

func NewRouter(dispatcher *DispatcherImpl) (*Router, error) {
	r := &Router{
		dispatcher: dispatcher,
		filters:    []filters.Filter{},
	}

	unifyLabelsFilter := filters.NewUnifyLabelsFilter()
	registerFilter := filters.NewRegisterFilter(dispatcher.GetRegister())
	lastFilter := filters.NewLastFilter(dispatcher.GetSubscribersPool(), dispatcher.GetSubscribers())

	r.RegisterFilter(unifyLabelsFilter)
	r.RegisterFilter(registerFilter)
	r.RegisterFilter(lastFilter)

	return r, nil
}

func (r *Router) RegisterFilter(f filters.Filter) {
	logrus.Infof("Router register filter [%s]", f.Name())
	r.filters = append(r.filters, f)
}

func (r *Router) Route(m *types.Message) *errors.DispatchError {
	for i, f := range r.filters {
		derr := f.Filter(m)
		if derr != nil && !derr.IsOK() {
			if len(r.filters)-1 != i {
				logrus.Warnf("Route: %v", derr)
			}
			return derr
		}
		if derr != nil && derr.FilterInfo != "" {
			logrus.Warnf("Route: FilterInfo: %s", derr.FilterInfo)
		}
	}
	return errors.New()
}

func (r *Router) GetFilters() []filters.Filter {
	return r.filters
}
