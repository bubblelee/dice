package filters

import (
	"terminus.io/dice/dice/eventbox/dispatcher/errors"
	"terminus.io/dice/dice/eventbox/types"
)

type UnifyLabelsFilter struct{}

func NewUnifyLabelsFilter() Filter {
	return &UnifyLabelsFilter{}
}

func (*UnifyLabelsFilter) Name() string {
	return "UnifyLabelsFilter"
}

func (*UnifyLabelsFilter) Filter(m *types.Message) *errors.DispatchError {
	labels := m.Labels
	normalizedLabels := map[types.LabelKey]interface{}{}
	for k, v := range labels {
		normalizedKey := k.NormalizeLabelKey()
		normalizedLabels[normalizedKey] = v
	}
	m.Labels = normalizedLabels
	return nil
}
