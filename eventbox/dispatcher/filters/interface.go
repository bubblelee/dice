package filters

import (
	"terminus.io/dice/dice/eventbox/dispatcher/errors"
	"terminus.io/dice/dice/eventbox/types"
)

type Filter interface {
	Filter(m *types.Message) *errors.DispatchError
	Name() string
}
