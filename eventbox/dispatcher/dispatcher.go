package dispatcher

import (
	"sync"

	"terminus.io/dice/dice/eventbox/conf"
	"terminus.io/dice/dice/eventbox/input"
	etcdinput "terminus.io/dice/dice/eventbox/input/etcd"
	httpinput "terminus.io/dice/dice/eventbox/input/http"
	"terminus.io/dice/dice/eventbox/register"
	"terminus.io/dice/dice/eventbox/server"
	"terminus.io/dice/dice/eventbox/subscriber"
	dingdingsubscriber "terminus.io/dice/dice/eventbox/subscriber/dingding"
	fakesubscriber "terminus.io/dice/dice/eventbox/subscriber/fake"
	httpsubscriber "terminus.io/dice/dice/eventbox/subscriber/http"
	mysqlsubscriber "terminus.io/dice/dice/eventbox/subscriber/mysql"
	"terminus.io/dice/dice/pkg/goroutinepool"
	"terminus.io/dice/dice/pkg/jsonstore"

	"github.com/sirupsen/logrus"
)

type Dispatcher interface {
	RegisterSubscriber(s subscriber.Subscriber)
	RegisterInput(input.Input)
	Start() // block until stopped
	Stop()  // block until stopped
}

type DispatcherImpl struct {
	js              jsonstore.JsonStoreWithWatch
	subscribers     map[string]subscriber.Subscriber
	subscriberspool map[string]*goroutinepool.GoroutinePool
	router          *Router
	register        register.Register
	inputs          []input.Input
	httpserver      *server.Server

	runningWg sync.WaitGroup
}

func New() (Dispatcher, error) {
	js, err := jsonstore.New()
	if err != nil {
		return nil, err
	}
	dispatcher := DispatcherImpl{
		js:              js.(jsonstore.JsonStoreWithWatch),
		subscribers:     make(map[string]subscriber.Subscriber),
		subscriberspool: make(map[string]*goroutinepool.GoroutinePool),
	}

	etcdi, err := etcdinput.New()
	if err != nil {
		return nil, err
	}
	httpi, err := httpinput.New()
	if err != nil {
		return nil, err
	}
	fakeS, err := fakesubscriber.New(fakesubscriber.FakeTestFilePath)
	if err != nil {
		return nil, err
	}
	httpS := httpsubscriber.New()
	dingdingS := dingdingsubscriber.New()
	mysqlS, err := mysqlsubscriber.New()
	if err != nil {
		return nil, err
	}
	dispatcher.RegisterInput(etcdi)
	dispatcher.RegisterInput(httpi)

	dispatcher.RegisterSubscriber(fakeS)
	dispatcher.RegisterSubscriber(httpS)
	dispatcher.RegisterSubscriber(dingdingS)
	dispatcher.RegisterSubscriber(mysqlS)

	for name := range dispatcher.subscribers {
		dispatcher.subscriberspool[name] = goroutinepool.New(conf.PoolSize())
	}

	reg, err := register.New()
	if err != nil {
		return nil, err
	}
	dispatcher.register = reg

	server, err := server.New()
	if err != nil {
		return nil, err
	}
	regHTTP := register.NewHTTP(reg)
	// add endpoints here
	server.AddEndPoints(httpi.GetHTTPEndPoints())
	server.AddEndPoints(regHTTP.GetHTTPEndPoints())
	dispatcher.httpserver = server

	router, err := NewRouter(&dispatcher)
	if err != nil {
		return nil, err
	}
	dispatcher.router = router

	return &dispatcher, nil
}

func (d *DispatcherImpl) GetRegister() register.Register {
	return d.register
}

func (d *DispatcherImpl) GetSubscribers() map[string]subscriber.Subscriber {
	return d.subscribers
}

func (d *DispatcherImpl) GetSubscribersPool() map[string]*goroutinepool.GoroutinePool {
	return d.subscriberspool
}
func (d *DispatcherImpl) RegisterSubscriber(s subscriber.Subscriber) {
	logrus.Infof("Dispatcher: register subscriber [%s]", s.Name())
	d.subscribers[s.Name()] = s
}

func (d *DispatcherImpl) RegisterInput(i input.Input) {
	logrus.Infof("Dispatcher: register input [%s]", i.Name())
	d.inputs = append(d.inputs, i)
}

func (d *DispatcherImpl) Start() {
	var err error
	for _, pool := range d.subscriberspool {
		pool.Start()
	}
	d.runningWg.Add(len(d.inputs) + 1)
	for _, i := range d.inputs {
		go func(i input.Input) {
			err = i.Start(d.router.Route)
			if err != nil {
				logrus.Errorf("dispatcher: start %s err:%v", i.Name(), err)
			}
			d.runningWg.Done()
		}(i)
	}
	// start httpserver
	go func() {
		if err := d.httpserver.Start(); err != nil {
			logrus.Errorf("dispatcher: start httpserver: %v", err)
		}
		d.runningWg.Done()
	}()
	d.runningWg.Wait()
}

// 1. 关闭所有输入端 (httpserver, inputs)
// 2. 等待 pool 里的所有消息发送完
// 3. 关闭 pool
// 4. 关闭 register
func (d *DispatcherImpl) Stop() {
	logrus.Info("Dispatcher: stopping")
	defer logrus.Info("Dispatcher: stopped")
	// stop httpserver first
	if err := d.httpserver.Stop(); err != nil {
		logrus.Errorf("dispatcher: stop httpserver: %v", err)
	}
	// stop inputs
	for _, i := range d.inputs {
		if err := i.Stop(); err != nil {
			logrus.Errorf("dispatcher: stop %s err:%v", i.Name(), err)
		}
	}

	// it will block until all things done
	for _, pool := range d.subscriberspool {
		pool.Stop()
	}
}
