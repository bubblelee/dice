package main

import (
	"os"
	"os/signal"
	"syscall"

	"terminus.io/dice/dice/eventbox/dispatcher"
	"terminus.io/dice/dice/pkg/dumpstack"
	"terminus.io/dice/dice/pkg/version"

	"github.com/sirupsen/logrus"
)

func main() {
	dumpstack.Open()

	logrus.Infof(version.String())

	dp, err := dispatcher.New()
	if err != nil {
		panic(err)
	}

	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		for range sig {
			dp.Stop()
			os.Exit(0)
		}
	}()

	dp.Start()
}
