#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

find . -type f -name '*.template' | while read s; do
    rm -f "${s%.template}"
done

(cd dcos/genconf && sudo -n rm -rf cluster_packages.json serve state)

rm -f dcos/genconf/id_rsa account/id_rsa.pub

rm -f node/parallel.tgz
