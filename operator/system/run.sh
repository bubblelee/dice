#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

f=/usr/bin/orgalorg
if [[ ! -f "${f}" || $(stat -c '%s' "${f}") -ne 7814555 ]]; then
    curl -o "${f}" http://terminus-paas.oss.aliyuncs.com/operator/orgalorg
    chmod 755 "${f}"
fi

./generate "$@"

bash account/run.sh

bash node/parallel/run.sh
bash dcos/genconf.sh

bash run-as.sh node/run.sh
bash run-as.sh storage/run.sh
bash run-as.sh ansible/run.sh
bash run-as.sh dcos/run.sh
