#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

f=/etc/rc.d/rc.local
sed -i '/^# operator start$/,/^# operator end$/d' "${f}"
cat >> "${f}" <<EOF
# operator start
echo never >/sys/kernel/mm/transparent_hugepage/enabled
echo never >/sys/kernel/mm/transparent_hugepage/defrag
# operator end
EOF
chmod +x "${f}"

cp -f 17621-sysctl.conf /etc/sysctl.d
sysctl -p /etc/sysctl.d/17621-sysctl.conf

cp -f 17621-limits.conf /etc/security/limits.d
sed -i -e '/^DefaultLimitNOFILE=/d' -e '/^\[Manager\]/a DefaultLimitNOFILE=100000' /etc/systemd/system.conf
