#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

if [[ "$(getenforce)" != 'Disabled' ]]; then
    setenforce 0
fi
sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config

systemctl disable firewalld
systemctl stop firewalld

if ! getent group nogroup >/dev/null 2>&1; then
    groupadd nogroup
fi

if ! lsmod | grep -q overlay; then
    modprobe overlay
    mkdir -p /etc/modules-load.d
    echo overlay >/etc/modules-load.d/overlay.conf
fi

mkdir -p /etc/docker
cp -f daemon.json /etc/docker
cp -f docker.service /etc/systemd/system

p=docker-ce-17.06.2.ce
if rpm -q --quiet "${p}"; then
    systemctl daemon-reload
    rpm -q "${p}"
else
    yum install -y "${p}"
    systemctl daemon-reload
    systemctl enable docker
    systemctl restart docker
fi
