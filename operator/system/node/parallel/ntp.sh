#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

if rpm -q --quiet ntp; then
    rpm -q ntp
else
    yum install -y ntp
fi
systemctl stop ntpd
ntpd -gq
systemctl restart ntpd
