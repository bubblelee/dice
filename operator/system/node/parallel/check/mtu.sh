#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

for i in /sys/class/net/*/mtu; do
    j="${i#/sys/class/net/}"
    j="${j%/mtu}"
    k=1500
    if readlink -f "${i}" | fgrep -q /sys/devices/virtual/net/; then
        case "${j}" in
            lo)
                k=65536
                ;;
            docker0)
                ;;
            dummy0)
                ;;
            minuteman)
                ;;
            spartan)
                ;;
            d-dcos)
                ;;
            vtep1024)
                ;;
            *)
                continue
                ;;
        esac
    fi
    l=$(head -n 1 "${i}")
    echo "INFO mtu ${j} ${l}"
    if [[ "${k}" -ne "${l}" ]]; then
        echo "ERROR mtu ${j}" >&2
        exit 1
    fi
done
