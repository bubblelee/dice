#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

distro=''
if [[ -f /etc/os-release ]]; then
    source /etc/os-release
    distro="${ID}"
fi
ver=''
if [[ -f /etc/redhat-release ]]; then
    ver=$(egrep -m 1 -o '[0-9]+(\.[0-9]+)+' /etc/redhat-release | head -n 1)
fi
kernel=$(uname -r | egrep -m 1 -o '[0-9]+(\.[0-9]+)+' | head -n 1)
cpu=$(nproc --all)
mem=$(free -h -w | awk '{if($1=="Mem:")print $2}')
rp=$(lsblk -n -o SIZE,MOUNTPOINT | awk '{if(NF==2&&$2=="/")print $1}')

echo "INFO distro ${distro}"
echo "INFO ver ${ver}"
echo "INFO kernel ${kernel}"
echo "INFO cpu ${cpu}"
echo "INFO mem ${mem}"
echo "INFO root partition ${rp}"

if [[ "${distro}" != 'centos' ]]; then
    echo 'ERROR distro' >&2
    exit 1
fi

v1=$(echo "${ver}" | cut -d. -f1)
v2=$(echo "${ver}" | cut -d. -f2)
if [[ "${v1}" -lt 7 ]] || [[ "${v1}" -eq 7 && "${v2}" -lt 2 ]]; then
    echo 'ERROR ver' >&2
    exit 1
fi

v1=$(echo "${kernel}" | cut -d. -f1)
v2=$(echo "${kernel}" | cut -d. -f2)
if [[ "${v1}" -lt 3 ]] || [[ "${v1}" -eq 3 && "${v2}" -lt 10 ]]; then
    echo 'ERROR kernel' >&2
    exit 1
fi

bash mtu.sh
