#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

bash check/run.sh
bash systune/run.sh
bash yum/run.sh
bash docker/run.sh
bash ntp.sh
