package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"

	"terminus.io/dice/dice/colony/officer/models/cluster/system"
)

func main() {
	var data system.System
	b, err := ioutil.ReadFile("system.json")
	if err != nil {
		log.Fatalln(err)
	}
	err = json.Unmarshal(b, &data)
	if err != nil {
		log.Fatalln(err)
	}
	sort.Sort(data.Nodes)
	data.NewNodes = make(system.Nodes, 0, len(os.Args)-1)
Loop:
	for _, ip := range os.Args[1:] {
		for _, node := range data.NewNodes {
			if ip == node.IP {
				continue Loop
			}
		}
		for _, node := range data.Nodes {
			if ip == node.IP {
				data.NewNodes = append(data.NewNodes, node)
				continue Loop
			}
		}
		log.Fatalln(ip)
	}
	sort.Sort(data.NewNodes)
	const suffix = ".template"
	err = filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() || !strings.HasSuffix(path, suffix) {
			return nil
		}
		name := path[:len(path)-len(suffix)]
		fmt.Println(name)
		t, err := template.New(filepath.Base(path)).Option("missingkey=error").Funcs(template.FuncMap{
			"inc": func(i int) int {
				return i + 1
			},
		}).ParseFiles(path)
		if err != nil {
			return err
		}
		f, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			return err
		}
		defer f.Close()
		return t.Execute(f, data)
	})
	if err != nil {
		log.Fatalln(err)
	}
}
