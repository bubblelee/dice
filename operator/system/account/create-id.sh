#!/bin/bash

set -o errexit -o nounset -o pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

if ! getent passwd "$1" >/dev/null 2>&1; then
    useradd -d "/home/$1" -m -s /bin/bash "$1"
    echo "$1 ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/$1"
fi

f="$(getent passwd "$1" | cut -d: -f6)/.ssh/id_rsa"
if [[ ! -f "${f}" ]]; then
    sudo -n -u "$1" ssh-keygen -t rsa -f "${f}" -N '' -C "$1@operator"
fi

cp -n "${f}" ../dcos/genconf
cp -n "${f}.pub" .
