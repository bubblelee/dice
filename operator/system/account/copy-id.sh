#!/bin/bash

set -o errexit -o nounset -o pipefail

if ! getent passwd "$1" >/dev/null 2>&1; then
    useradd -d "/home/$1" -m -s /bin/bash "$1"
    echo "$1 ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/$1"
fi

d="$(getent passwd "$1" | cut -d: -f6)/.ssh"
if [[ ! -d "${d}" ]]; then
    mkdir "${d}"
    chown "$1:" "${d}"
    chmod 700 "${d}"
fi

f="${d}/authorized_keys"
if [[ ! -f "${f}" ]]; then
    touch "${f}"
    chown "${1}:" "${f}"
    chmod 600 "${f}"
fi

if ! fgrep -q "$2" "${f}"; then
    echo "$2" >> "${f}"
fi
