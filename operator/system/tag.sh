#!/bin/bash

set -o errexit -o nounset -o pipefail

if [[ "$#" == 3 && "$2" == "$3" || "$#" -lt 1 || "$#" -gt 3 ]]; then
    exit 1
fi

public=0
force=0
for s in "${@:2}"; do
    if [[ "${s}" == 'public' ]]; then
        public=1
    elif [[ "${s}" == 'force' ]]; then
        force=1
    else
        exit 1
    fi
done

f=/var/lib/dcos/mesos-slave-common
if [[ -d "${f}" ]]; then
    rmdir "${f}"
fi
mkdir -p /var/lib/dcos
echo "MESOS_ATTRIBUTES=$1" > "${f}"
s=dcos-mesos-slave
if [[ "${public}" -eq 1 ]]; then
    s=dcos-mesos-slave-public
fi
systemctl kill -s SIGUSR1 "${s}"
systemctl stop "${s}"
if [[ "${force}" -eq 1 ]]; then
    rm -f /var/lib/mesos/slave/meta/slaves/latest
fi
systemctl restart "${s}"
