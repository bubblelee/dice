package main

import (
	"log"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/elazarl/goproxy"
)

var (
	root = os.Getenv("FPS_ROOT")
	host = os.Getenv("FPS_HOST")
	addr = os.Getenv("FPS_ADDR")
)

func main() {
	p := goproxy.NewProxyHttpServer()
	h := http.NewServeMux()
	h.HandleFunc("/_system/add", systemAdd)
	h.HandleFunc("/_system/tag", systemTag)
	h.Handle("/", http.FileServer(http.Dir(root)))
	log.Println("http serve", addr)
	err := http.ListenAndServe(addr, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if stripHostPort(r.Host) == host {
			h.ServeHTTP(w, r)
		} else {
			p.ServeHTTP(w, r)
		}
	}))
	if err != nil {
		log.Fatalln(err)
	}
}

// stripHostPort returns h without any trailing ":<port>".
func stripHostPort(h string) string {
	// If no port on host, return unchanged
	if strings.IndexByte(h, ':') == -1 {
		return h
	}
	host, _, err := net.SplitHostPort(h)
	if err != nil {
		return h // on error, return unchanged
	}
	return host
}
