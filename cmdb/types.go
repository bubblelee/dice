package main

import (
	"terminus.io/dice/dice/cmdb/api"
)

// QueryHostResponse host
type QueryHostResponse struct {
	Result *api.CmHost `json:"result"`
}

// ClusterHostsResponse host list
type ClusterHostsResponse struct {
	Result *[]api.CmHost `json:"result"`
}

// QueryContainerResponse container
type QueryContainerResponse struct {
	Result *api.CmContainer `json:"result"`
}

// GetContainersResponse container list
type GetContainersResponse struct {
	Result *[]api.CmContainer `json:"result"`
}

// PutServiceResponse service
type PutServiceResponse struct {
	Result *api.CmService `json:"result"`
}

// ClusterUsageResponse container usage
type ClusterUsageResponse struct {
	Result *api.ClusterUsage `json:"result"`
}

// HostUsageResponse every host usage from cluster
type HostUsageResponse struct {
	Result *api.HostUsage `json:"result"`
}

// HostsUsageResponse every host usage from cluster
type HostsUsageResponse struct {
	Result []*api.HostUsage `json:"result"`
}

// ComponentUsageResponse every component usage from cluster
type ComponentUsageResponse struct {
	Result []*api.ComponentUsage `json:"result"`
}

// AddOnUsageUsageResponse every addon usage from cluster
type AddOnUsageUsageResponse struct {
	Result []*api.AddOnUsage `json:"result"`
}

// ProjectUsageResponse every project usage from cluster
type ProjectUsageResponse struct {
	Result []*api.ProjectUsage `json:"result"`
}

// ApplicationUsageResponse every application usage from project
type ApplicationUsageResponse struct {
	Result []*api.ApplicationUsage `json:"result"`
}

// RuntimeUsageResponse every runtime usage from application
type RuntimeUsageResponse struct {
	Result []*api.RuntimeUsage `json:"result"`
}

// ServiceUsageResponse every service usage from runtime
type ServiceUsageResponse struct {
	Result []*api.ServiceUsage `json:"result"`
}

// InstanceUsageResponse every service usage from runtime
type InstanceUsageResponse struct {
	Result *api.ContainerUsage `json:"result"`
}

// SearchResult resource
type SearchResult struct {
	Result []*api.Resource `json:"result"`
}

// PipelineSummaryResponse 构建总览
type PipelineSummaryResponse struct {
	Result *api.PipelineSummary `json:"result"`
}

// TaskSummaryResponse 实例汇总
type TaskSummaryResponse struct {
	Result *api.TaskSummary `json:"result"`
}

// PipelineDynamicResponse pipeline 动态信息
type PipelineDynamicResponse struct {
	Result *api.DynamicMsgSlice `json:"result"`
}

// ResourceStatisticsResponse 统计资源使用情况的response
type ResourceStatisticsResponse struct {
	Result *[]api.ResourceStatistics `json:"result"`
}

// instanceRequestType 获取实例输入的请求参数
type instanceRequestType string

const (
	INSTANCE_REQUEST_TYPE_SERVICE     instanceRequestType = "service"
	INSTANCE_REQUEST_TYPE_RUNTIME     instanceRequestType = "runtime"
	INSTANCE_REQUEST_TYPE_APPLICATION instanceRequestType = "application"
	INSTANCE_REQUEST_TYPE_PROJECT     instanceRequestType = "project"
	INSTANCE_REQUEST_TYPE_ORG         instanceRequestType = "org"
	INSTANCE_REQUEST_TYPE_CLUSTER     instanceRequestType = "cluster"
	INSTANCE_REQUEST_TYPE_COMPONENT   instanceRequestType = "component"
	INSTANCE_REQUEST_TYPE_ADDON       instanceRequestType = "addon"
	INSTANCE_REQUEST_TYPE_HOST        instanceRequestType = "host"
)

var allInstanceTypes = []instanceRequestType{
	INSTANCE_REQUEST_TYPE_SERVICE,
	INSTANCE_REQUEST_TYPE_RUNTIME,
	INSTANCE_REQUEST_TYPE_APPLICATION,
	INSTANCE_REQUEST_TYPE_PROJECT,
	INSTANCE_REQUEST_TYPE_ORG,
	INSTANCE_REQUEST_TYPE_CLUSTER,
	INSTANCE_REQUEST_TYPE_COMPONENT,
	INSTANCE_REQUEST_TYPE_ADDON,
	INSTANCE_REQUEST_TYPE_HOST,
}

// statisticsRequestType 获取 statistics
type statisticsRequestType string

const (
	STATISTICS_REQUEST_TYPE_PLATFORM statisticsRequestType = "platform"
	STATISTICS_REQUEST_TYPE_ORG      statisticsRequestType = "org"
	STATISTICS_REQUEST_TYPE_PROJECT  statisticsRequestType = "project"
)

var allStatisticsRequestTypes = []statisticsRequestType{
	STATISTICS_REQUEST_TYPE_PLATFORM,
	STATISTICS_REQUEST_TYPE_ORG,
	STATISTICS_REQUEST_TYPE_PROJECT,
}
