package main

import (
	"net/http"
	"strconv"
	"time"

	"terminus.io/dice/dice/cmdb/conf"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type cluster struct {
	ID             int               `json:"id"`
	OrgID          int               `json:"orgID"`
	Name           string            `json:"name"`
	Type           string            `json:"type"` // k8s, dcos, edas
	Logo           string            `json:"logo"`
	Description    string            `json:"description"`
	WildcardDomain string            `json:"wildcardDomain"`
	URLs           map[string]string `json:"urls"`
	CreatedAt      time.Time         `json:"createdAt"`
	UpdatedAt      time.Time         `json:"updatedAt"`
}

type clusterResponse struct {
	Data    cluster `json:"data"`
	Success bool    `json:"success"`
}

func authentication(r *http.Request) (bool, error) {
	if r == nil {
		return false, errors.Errorf("invalid param: request is nil")
	}

	// 特权模式
	previleged := r.Header.Get("Previleged")
	if previleged == "true" {
		logrus.Infof("%s %s: previleged is true", r.Method, r.URL.String())
		return true, nil
	}

	// Client-ID & Client-Name
	// 如果使用 token 的方式，先跳过鉴权
	clientID := r.Header.Get("Client-ID")
	if clientID != "" {
		logrus.Infof("%s %s: token mode", r.Method, r.URL.String())
		return true, nil
	}

	// USER-ID & ORG-ID
	// 如果没有传 USER-ID, 则默认为内部调用，直接跳过鉴权
	userID := r.Header.Get("USER-ID")
	if userID == "" {
		logrus.Infof("%s %s: skip authentication", r.Method, r.URL.String())
		return true, nil
	}

	orgID := r.Header.Get("ORG-ID")
	if orgID == "" {
		return false, errors.Errorf("missing param: ORG-ID")
	}

	var orgDest string
	vars := mux.Vars(r)

	// get cluster from url
	clusterID, ok := vars["cluster"]
	if ok {
		// get org from the colony-officer
		var result clusterResponse
		resp, err := httpclient.New().Get(conf.Officer).Path("/api/clusters/" + clusterID).Do().JSON(&result)
		if err != nil {
			return false, errors.Errorf("failed to get orgID from colony officer: %v", err)
		}

		if !resp.IsOK() {
			return false, errors.Errorf("failed to get orgID from colony officer, statusCode: %s", resp.StatusCode())
		}

		orgDest = strconv.Itoa(result.Data.OrgID)
	} else {
		var ok bool
		var orgs []string
		// get org from query
		vals := r.URL.Query()
		if orgs, ok = vals["org"]; !ok {
			return false, errors.Errorf("missing param: cluster or org")
		}
		orgDest = orgs[0]
	}

	// 不考虑传多个 org 的情况，每次都只取第一个进行鉴权
	if orgID != orgDest {
		err := errors.Errorf("%s %s: authentication failure, src orgID: %s, dest orgID: %s",
			r.Method, r.URL.String(), orgID, orgDest)
		logrus.Error(err)
		return false, err
	}

	return true, nil
}
