// +build !default

package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAuthentication(t *testing.T) {
	var auth bool
	var err error

	req, err := http.NewRequest("GET", "/api/test", nil)
	if err != nil {
		t.Fatal(err)
	}

	// return false
	auth, err = authentication(req)
	assert.Equal(t, false, auth)

	// test Previleged
	req.Header.Set("Previleged", "true")
	auth, err = authentication(req)
	assert.Nil(t, err)
	assert.Equal(t, true, auth)

	// test token
	req.Header.Set("Client-ID", "123")
	auth, err = authentication(req)
	assert.Nil(t, err)
	assert.Equal(t, true, auth)

	// test USER-ID
}
