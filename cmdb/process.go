package main

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"terminus.io/dice/dice/cmdb/api"
	"terminus.io/dice/dice/cmdb/conf"
)

func (s *Server) consumer() {

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":        conf.Borkers,
		"group.id":                 conf.Group,
		"auto.offset.reset":        "latest",
		"auto.commit.interval.ms":  1000,
		"message.send.max.retries": 10000000,
	})
	if err != nil {
		logrus.Errorf("failed to create new kafka consumer: %v", err)
		time.Sleep(5 * time.Second)
		return
	}
	defer func() {
		if err = c.Close(); err != nil {
			logrus.Error(err)
		}
	}()

	topics := []string{conf.HostTopic, conf.ContainerTopic}

	logrus.Info(topics)

	if err = c.SubscribeTopics(topics, nil); err != nil {
		logrus.Errorf("failed to subscribe kafka topics: %v", err)
		// TODO: ???
		time.Sleep(5 * time.Second)
		return
	}

	for {
		msg, err := c.ReadMessage(-1)
		if err != nil {
			logrus.Errorf("failed to read kafka message: %v", err)
			break
		}

		switch *msg.TopicPartition.Topic {

		case conf.HostTopic:
			s.processHost(msg)

		case conf.ContainerTopic:
			s.processContainer(msg)

		default:
			logrus.Warnf("invalid kafka topic: %s", *msg.TopicPartition.Topic)
		}
	}
}

func (s *Server) cleanUpHostContainers(cluster, ip string) error {
	logrus.Infof("Start to clean up containers, host.ip: %s, cluster: %s", ip, cluster)

	// cleanUp 清理指定宿主机上的所有 containers
	return s.db.DeleteAllContainersByHost(context.Background(), cluster, ip)
}

func (s *Server) cleanUpHosts(hosts []*api.CmHost) error {
	var err error

	// cleanUp 清理指定宿主机上的所有 containers
	for _, host := range hosts {
		logrus.Infof("Start to clean up host, name: %s, ip: %s, cluster: %s", host.Name, host.PrivateAddr, host.Cluster)
		if err = s.db.DeleteHost(context.Background(), host.Cluster, host.Name); err != nil {
			return err
		}
	}

	return nil
}

func checkTime(t1, t2 int64) error {
	if t1 < t2 {
		delta := (t2 - t1)
		deltaSeconds := delta / (int64(time.Second) / int64(time.Nanosecond))

		if deltaSeconds > 3600 {
			return errors.New("message timeout")
		}

		return nil
	}

	return errors.New("system clock not sync")
}

func (s *Server) processHost(msg *kafka.Message) {
	var hostMsg api.HostMSG

	if err := json.Unmarshal(msg.Value, &hostMsg); err != nil {
		logrus.Warnf("failed to unmarshal host message: ", err)
		return
	}

	if err := checkTime(hostMsg.TimeStamp, time.Now().UnixNano()); err != nil {
		logrus.Errorf("host message expired! error: %v, metrics: %s", err, string(msg.Value))
		return
	}

	hosts := hostMsg.Data
	msgType := hostMsg.Type

	logrus.Debugf("read kafka message: topic = %s, type = %s, hosts = %v", conf.HostTopic, msgType, hosts)

	// 10min 一次数据全量更新，此前需要先删除该宿主机上的所有 containers 信息
	if msgType == "all" {
		if err := s.cleanUpHosts(hosts); err != nil {
			logrus.Errorf("Failed to cleanup hosts: %v", err)
		}
	}

	for _, h := range hosts {
		ctx := context.Background()

		if err := s.db.RemoveOrUpdateHost(ctx, msgType, h); err != nil {
			logrus.Errorf("failed to update host: %s, cluster: %s, ip: %s, error: ", h.Name, h.Cluster, h.PrivateAddr, err)
		}
		logrus.Debugf("successfully update host: %s, cluster: %s, ip: %s!", h.Name, h.Cluster, h.PrivateAddr)
	}
}

func (s *Server) processContainer(msg *kafka.Message) {
	var containerMsg api.ContainerMSG

	if err := json.Unmarshal(msg.Value, &containerMsg); err != nil {
		logrus.Errorf("failed to unmarshal container message: %v", err)
		return
	}

	if err := checkTime(containerMsg.TimeStamp, time.Now().UnixNano()); err != nil {
		logrus.Errorf("container message expired! error: %v, metrics: %s", err, string(msg.Value))
		return
	}

	containers := containerMsg.Data
	msgType := containerMsg.Type
	ip := containerMsg.HostIP
	cluster := containerMsg.Cluster

	logrus.Debugf("read kafka message: topic = %s, type = %s, containers = %v", conf.ContainerTopic, msgType, containers)

	// 10min 一次数据全量更新，此前需要先删除该宿主机上的所有 containers 信息
	if msgType == "all" {
		if err := s.cleanUpHostContainers(cluster, ip); err != nil {
			logrus.Errorf("Failed to cleanup host containers: %v", err)
		}
	}

	for _, c := range containers {
		ctx := context.Background()

		c.DiceComponent = strings.Trim(c.DiceComponent, "/")
		c.DiceComponent = strings.Replace(c.DiceComponent, "/", "_", -1)
		c.DiceAddon = strings.Trim(c.DiceAddon, "/")
		c.DiceAddon = strings.Replace(c.DiceAddon, "/", "_", -1)

		if err := s.db.UpdateContainer(ctx, msgType, c); err != nil {
			logrus.Errorf("failed to update container ID: %s, cluster: %s, host: %s, container IP: %s, error: %s",
				c.ID, c.Cluster, c.HostPrivateIPAddr, c.IPAddress, err)
		}
		logrus.Debugf("successfully update container ID: %s, cluster: %s, host: %s, container IP: %s",
			c.ID, c.Cluster, c.HostPrivateIPAddr, c.IPAddress)
	}
}
