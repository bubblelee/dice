package main

import (
	"context"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"terminus.io/dice/dice/cmdb/api"
)

// SearchPut 查询关键字
type SearchPut struct {
	Keyword string `json:"keyword"` // 关键字
}

// Search 全局资源查询
func (s *Server) Search(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	var data SearchPut
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest, Content: "decode request body failed!"}, err
	}

	logrus.Debugf("search keyword: %s", data.Keyword)

	result, err := s.db.GlobalSearch(ctx, cluster, data.Keyword)
	if err != nil {
		return HTTPResponse{Status: http.StatusBadRequest, Content: "search failed!"}, err
	}

	if len(result) == 0 {
		return HTTPResponse{
			Status: http.StatusOK,
			Content: SearchResult{
				Result: make([]*api.Resource, 0),
			},
		}, nil
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: SearchResult{
			Result: result,
		},
	}, nil
}
