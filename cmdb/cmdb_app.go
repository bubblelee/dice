package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"terminus.io/dice/dice/cmdb/api"
)

// 创建service元数据，由调度器来填写
func (s *Server) serviceCreateOrUpdate(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var (
		service api.CmService
		body    []byte
		err     error
	)

	if body, err = ioutil.ReadAll(r.Body); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest, Content: "read http request body error."}, err
	}

	if err = json.Unmarshal(body, &service); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest, Content: "unmarshal failed."}, err
	}

	service.Cluster = vars["cluster"]
	service.DiceProject = vars["project"]
	service.DiceApplication = vars["application"]
	service.DiceRuntime = vars["runtime"]
	service.DiceService = vars["service"]

	if err = s.db.CreateOrUpdateService(ctx, &service); err != nil {
		return HTTPResponse{Status: http.StatusInternalServerError, Content: "update service to database failed."}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PutServiceResponse{
			Result: &service,
		},
	}, err
}
