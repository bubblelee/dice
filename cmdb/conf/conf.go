package conf

import (
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

var (
	// Debug control the log's level.
	Debug bool

	// ListenAddr specify the server's listen address, eg: ":9091"
	ListenAddr string

	// Borkers kafka
	Borkers        string
	ContainerTopic string
	HostTopic      string
	Group          string

	// DbDial mysql
	DbDial DbDialInfo

	// Officer colony-officer
	Officer string
)

// DbDialInfo mysql
type DbDialInfo struct {
	// ip:port
	Host string

	// auth
	Username string
	Password string

	Database string

	PoolLimit int
}

// Get returns the value of a env variable as string.
func Get(k string) string {
	return os.Getenv(k)
}

// GetBool returns the value of a env variable as bool.
func GetBool(k string) bool {
	v := Get(k)
	v = strings.ToLower(v)
	return v == "true" || v == "1"
}

// GetInt returns the value of a env variable as int.
func GetInt(k string) (int, error) {
	v := Get(k)

	if v == "" {
		return 0, errors.Errorf("invalid config: %s=%s", k, v)
	}
	n, err := strconv.Atoi(v)
	if err != nil {
		return 0, errors.Wrapf(err, "invalid config: %s=%s", k, v)
	}
	return n, nil
}

// Parse parse the configure option from env variables.
func Parse() error {
	var err error

	Debug = parseDebug()

	Borkers, HostTopic, ContainerTopic, Group = parseKafka()

	if ListenAddr, err = parseListenAddr(); err != nil {
		return err
	}

	if DbDial, err = parseDatabase(); err != nil {
		return err
	}

	Officer = parseColonyOfficer()

	return nil
}

func parseDebug() bool {
	return GetBool("METASERVER_DEBUG")
}

func parseListenAddr() (string, error) {
	v := Get("METASERVER_LISTEN_ADDR")
	if v == "" {
		v = ":9093"
	}
	return v, nil
}

func parseKafka() (cmdbBorkers, cmdbHostTopic, cmdbContainerTopic, cmdbGroup string) {
	borkers := Get("CMDB_BORKERS")
	if borkers == "" {
		borkers = "spotnewkafka.marathon.l4lb.thisdcos.directory:9093"
	}
	hostTopic := Get("CMDB_HOST_TOPIC")
	if hostTopic == "" {
		hostTopic = "spot_meta_host"
	}
	containerTopic := Get("CMDB_CONTAINER_TOPIC")
	if containerTopic == "" {
		containerTopic = "spot_meta_container"
	}
	group := Get("CMDB_GROUP")
	if group == "" {
		group = "cmdb_group"
	}
	return borkers, hostTopic, containerTopic, group
}

// dbDSN = "gorm:gorm@tcp(localhost:9910)/gorm?charset=utf8&parseTime=True"
func parseDatabase() (DbDialInfo, error) {
	var dail DbDialInfo

	host := Get("CMDB_DB_HOST")
	if host == "" {
		host = "127.0.0.1:3306"
	}
	dail.Host = host

	user := Get("CMDB_DB_USER")
	if user == "" {
		user = "root"
	}
	dail.Username = user

	passwd := Get("CMDB_DB_PASSWD")
	if passwd == "" {
		return dail, errors.Errorf("please set CMDB_DB_PASSWD !!!")
	}
	dail.Password = passwd

	dbName := Get("CMDB_DB_NAME")
	if dbName == "" {
		return dail, errors.Errorf("please set CMDB_DB_NAME !!!")
	}
	dail.Database = dbName

	return dail, nil
}

func parseColonyOfficer() string {
	v := Get("COLONY_OFFICER")
	if v == "" {
		v = "dice.colony-officer.marathon.l4lb.thisdcos.directory:9029"
	}

	return v
}
