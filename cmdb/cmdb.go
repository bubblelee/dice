package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/pprof"
	"strings"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/sirupsen/logrus"

	"terminus.io/dice/dice/cmdb/api"
	"terminus.io/dice/dice/cmdb/conf"
	"terminus.io/dice/dice/pkg/jsonstore"
)

// Responser 是一个统一的Handler返回接口
type Responser interface {
	GetStatus() int
	GetContent() interface{}
}

// GetStatus 返回http状态码
func (r HTTPResponse) GetStatus() int {
	return r.Status
}

// GetContent 返回上下文
func (r HTTPResponse) GetContent() interface{} {
	return r.Content
}

// HTTPResponse http 返回值的标准结构体
type HTTPResponse struct {
	Status  int
	Content interface{}
}

// Server 定义http服务端
type Server struct {
	router     *mux.Router
	listenAddr string
	store      jsonstore.JsonStore
	db         *api.DbClient
}

// NewServer creates a server instance, it be used to handle http request and offer
// a api gateway role.
func NewServer(addr string) *Server {
	if addr == "" {
		panic("create cmdb server: invalid params.")
	}

	store, err := jsonstore.New()
	if err != nil {
		panic(err)
	}

	db, err := api.NewDBClient(&conf.DbDial)
	if err != nil {
		panic(err)
	}

	server := &Server{
		router:     mux.NewRouter(),
		listenAddr: addr,
		store:      store,
		db:         db,
	}

	return server
}

// ListenAndServe boot the server to listen and accept requests.
func (s *Server) ListenAndServe() error {
	defer func() {
		if err := s.db.Close(); err != nil {
			logrus.Error(err)
		}
	}()

	s.initEndpoints()
	s.runConsumer()
	go func() {
		api.Count()
	}()

	srv := &http.Server{
		Addr:              s.listenAddr,
		Handler:           s.router,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		ReadHeaderTimeout: 60 * time.Second,
	}
	err := srv.ListenAndServe()
	if err != nil {
		logrus.Errorf("failed to listen and serve: %s", err)
	}
	return err
}

func (s *Server) runConsumer() {
	go func() {
		for {
			logrus.Info("start Consumer....")
			s.consumer()
		}
	}()
}

func (s *Server) close(r *http.Request) {
	if err := r.Body.Close(); err != nil {
		logrus.Error(err)
	}
}

// initEndpoints adds the all routes of endpoint.
func (s *Server) initEndpoints() {
	var endpoints = []struct {
		Path string

		Method  string
		Handler func(context.Context, *http.Request, map[string]string) (Responser, error)
	}{
		// usage
		{"/clusters/{cluster}/usage", http.MethodGet, s.clusterUsage},
		{"/clusters/{cluster}/hosts/{host}/usage", http.MethodGet, s.hostUsage},
		{"/clusters/{cluster}/hosts-usage", http.MethodGet, s.hostsUsage},
		{"/clusters/{cluster}/instances/{instance}/usage", http.MethodGet, s.instanceUsage},
		{"/clusters/{cluster}/instances-usage", http.MethodGet, s.instancesUsage},

		// hosts
		{"/clusters/{cluster}/hosts", http.MethodGet, s.allHostsByCluster},
		{"/clusters/{cluster}/hosts/{host}", http.MethodGet, s.queryHost},

		// instances
		{"/clusters/{cluster}/instances/{instance}", http.MethodGet, s.queryContainer},
		{"/clusters/{cluster}/instances", http.MethodGet, s.getContainers},

		// search
		{"/clusters/{cluster}/search", http.MethodPost, s.Search},

		// statistics
		{"/statistics/deployment", http.MethodGet, s.deploymentStatistics},
		{"/statistics/build", http.MethodGet, s.buildStatistics},
		{"/statistics/task", http.MethodGet, s.taskStatistics},
		{"/statistics/deploy-dynamic", http.MethodGet, s.deployDynamic},
		{"/statistics/build-dynamic", http.MethodGet, s.buildDynamic},
		{"/statistics/resource", http.MethodGet, s.resourceStatistics},

		// TODO: remove?
		{"/service/{cluster}/{project}/{application}/{runtime}/{service}", http.MethodPut, s.serviceCreateOrUpdate},

		// Old api
		// TODO: 接口对接完之后，需要删除这部分接口
		{"/dice/host/{cluster}", http.MethodGet, s.allHostsByCluster},
		{"/dice/host/{cluster}/{host}", http.MethodGet, s.queryHost},
		{"/dice/host/{cluster}/{host}/container", http.MethodGet, s.hostContainersByOld},

		{"/dice/container/{cluster}", http.MethodGet, s.clusterContainers},
		{"/dice/container/{cluster}/{project}", http.MethodGet, s.projectContainersByOld},
		{"/dice/container/{cluster}/{project}/{application}", http.MethodGet, s.applicationContainersByOld},
		{"/dice/container/{cluster}/{project}/{application}/{runtime}", http.MethodGet, s.runtimeContainersByOld},
		{"/dice/container/{cluster}/{project}/{application}/{runtime}/{service}", http.MethodGet, s.serviceContainersByOld},

		{"/dice/containers", http.MethodGet, s.getContainers},

		{"/dice/component/{cluster}", http.MethodGet, s.clusterComponentContainers},
		{"/dice/component/{cluster}/{component}", http.MethodGet, s.componentContainersByOld},

		{"/dice/addon/{cluster}", http.MethodGet, s.clusterAddonContainers},
		{"/dice/addon/{cluster}/{addon}", http.MethodGet, s.addonContainersByOld},

		{"/dice/usage/{cluster}", http.MethodGet, s.clusterUsage},
		{"/dice/usage/{cluster}/host", http.MethodGet, s.hostsUsage},
		{"/dice/usage/{cluster}/component", http.MethodGet, s.componentUsage},
		{"/dice/usage/{cluster}/addon", http.MethodGet, s.addonUsage},
		{"/dice/usage/{cluster}/project", http.MethodGet, s.projectUsage},
		{"/dice/usage/{cluster}/{project}/application", http.MethodGet, s.applicationUsageByOld},
		{"/dice/usage/{cluster}/{project}/{application}/runtime", http.MethodGet, s.runtimeUsageByOld},
		{"/dice/usage/{cluster}/{project}/{application}/{runtime}/service", http.MethodGet, s.serviceUsageByOld},

		{"/dice/resource/{cluster}/search", http.MethodPost, s.Search},

		// overview
		// +platform
		{"/dice/overview/deploy", http.MethodGet, s.platformDeployOverview},
		{"/dice/overview/build", http.MethodGet, s.platformBuildOverview},
		{"/dice/overview/task", http.MethodGet, s.platformTaskOverview},
		{"/dice/overview/dynamic/deploy", http.MethodGet, s.platformDeployDynamic},
		{"/dice/overview/dynamic/build", http.MethodGet, s.platformBuildDynamic},
		// +org
		{"/dice/overview/deploy/org/{orgId}", http.MethodGet, s.orgDeployOverview},
		{"/dice/overview/build/org/{orgId}", http.MethodGet, s.orgBuildOverview},
		{"/dice/overview/task/org/{orgId}", http.MethodGet, s.orgTaskOverview},
		{"/dice/overview/dynamic/deploy/org/{orgId}", http.MethodGet, s.orgDeployDynamic},
		{"/dice/overview/dynamic/build/org/{orgId}", http.MethodGet, s.orgBuildDynamic},
		// +project
		{"/dice/overview/deploy/project/{projectId}", http.MethodGet, s.projectDeployOverview},
		{"/dice/overview/build/project/{projectId}", http.MethodGet, s.projectBuildOverview},
		{"/dice/overview/task/project/{projectId}", http.MethodGet, s.projectTaskOverview},
		{"/dice/overview/dynamic/deploy/project/{projectId}", http.MethodGet, s.projectDeployDynamic},
		{"/dice/overview/dynamic/build/project/{projectId}", http.MethodGet, s.projectBuildDynamic},
		// Old api END
	}

	// 为所有subpath添加/api/dice的head
	for _, ep := range endpoints {
		s.router.PathPrefix("/api").Path(ep.Path).Methods(ep.Method).HandlerFunc(s.internal(ep.Handler))
		logrus.Infof("added endpoint: %s %s", ep.Method, ep.Path)
	}

	subroute := s.router.PathPrefix("/debug/").Subrouter()
	subroute.HandleFunc("/pprof/profile", pprof.Profile)
	subroute.HandleFunc("/pprof/trace", pprof.Trace)
	subroute.HandleFunc("/pprof/block", pprof.Handler("block").ServeHTTP)
	subroute.HandleFunc("/pprof/heap", pprof.Handler("heap").ServeHTTP)
	subroute.HandleFunc("/pprof/goroutine", pprof.Handler("goroutine").ServeHTTP)
	subroute.HandleFunc("/pprof/threadcreate", pprof.Handler("threadcreate").ServeHTTP)
}

func (s *Server) internal(handler func(context.Context, *http.Request, map[string]string) (Responser, error)) http.HandlerFunc {
	pctx := context.Background()

	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithCancel(pctx)
		defer cancel()
		defer s.close(r)

		start := time.Now()
		logrus.Infof("start %s %s", r.Method, r.URL.String())
		defer func() {
			logrus.Infof("end %s %s (took %v)", r.Method, r.URL.String(), time.Since(start))
		}()

		acceptHeader := r.Header.Get("Accept")
		if !strings.Contains(acceptHeader, "application/vnd.dice+json") || !strings.Contains(acceptHeader, "version=1.0") {
			w.WriteHeader(http.StatusBadRequest)
			if _, err := w.Write([]byte("error header type !!")); err != nil {
				logrus.Error(err)
			}
			return
		}

		// authentication
		auth, err := authentication(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			errMsg := fmt.Sprintf("authentication error: %v", err)
			if _, err := w.Write([]byte(errMsg)); err != nil {
				logrus.Error(err)
			}
			return
		}

		if !auth {
			w.WriteHeader(http.StatusBadRequest)
			if _, err := w.Write([]byte("authentication failed !!")); err != nil {
				logrus.Error(err)
			}
			return
		}

		response, err := handler(ctx, r, mux.Vars(r))
		if err != nil {
			logrus.Errorf("failed to handle request: %s (%v)", r.URL.String(), err)

			if response != nil {
				w.WriteHeader(response.GetStatus())
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}

			if _, err = io.WriteString(w, err.Error()); err != nil {
				logrus.Error(err)
			}
			return
		}
		if response == nil || response.GetContent() == nil {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.GetStatus())

		encoder := json.NewEncoder(w)

		// pretty
		vals := r.URL.Query()
		pretty, ok := vals["pretty"]
		if ok && strings.Compare(pretty[0], "true") == 0 {
			encoder.SetIndent("", "    ")
		}

		if err := encoder.Encode(response.GetContent()); err != nil {
			logrus.Errorf("failed to send response: %s (%v)", r.URL.String(), err)
			return
		}
	}
}
