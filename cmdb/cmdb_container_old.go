package main

import (
	"context"
	"net/http"
)

// 获取指定 host 下所有 containers 的信息
func (s *Server) hostContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]
	host := vars["host"]

	result, err := s.db.AllContainersByHost(ctx, cluster, []string{host})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get host contaniers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 project 下所有 containers
func (s *Server) projectContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	project := vars["project"]

	result, err := s.db.AllContainersByProject(ctx, []string{project})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 appliaction 下所有 containers
func (s *Server) applicationContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	application := vars["application"]

	result, err := s.db.AllContainersByApplication(ctx, []string{application})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get application containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 runtime 下所有 containers
func (s *Server) runtimeContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	runtime := vars["runtime"]

	result, err := s.db.AllContainersByRuntime(ctx, []string{runtime})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get runtime containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 service 下所有 containers
func (s *Server) serviceContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	runtime := vars["runtime"]
	service := vars["service"]

	result, err := s.db.AllContainersByService(ctx, runtime, []string{service})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get service containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取某个集群下的所有 components 中的所有 containers 信息
func (s *Server) clusterComponentContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.AllComponentsByCluster(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get cluster components from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 component 下所有的containers
func (s *Server) componentContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]
	component := vars["component"]

	result, err := s.db.AllContainersByComponent(ctx, cluster, []string{component})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get component contianers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取某个集群下的所有 addons 中的所有 containers 信息
func (s *Server) clusterAddonContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.AllAddonsByCluster(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get cluster addons from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 component 下所有的containers
func (s *Server) addonContainersByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]
	addon := vars["addon"]

	result, err := s.db.AllContainersByAddon(ctx, cluster, []string{addon})
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get addon containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}
