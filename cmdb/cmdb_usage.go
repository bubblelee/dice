package main

import (
	"context"
	"fmt"
	"net/http"

	"terminus.io/dice/dice/cmdb/api"

	"github.com/pkg/errors"
)

// 获取指定集群的资源分配情况
func (s *Server) clusterUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.ClusterUsage(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get cluster usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ClusterUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取指定集群的宿主机资源分配情况
func (s *Server) hostsUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.AllHostsUsageByCluster(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get host usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: HostsUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取指定宿主机的资源分配情况
func (s *Server) hostUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]
	host := vars["host"]

	result, err := s.db.HostUsageByCluster(ctx, cluster, host)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get host usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: HostUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取指定集群中各个组件的资源分配情况
func (s *Server) componentUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.ComponentUsage
	var err error

	cluster := vars["cluster"]
	if cluster == "" {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: cluster",
		}, errors.Errorf("missing param: cluster")
	}

	vals := r.URL.Query()
	components, ok := vals["component"]
	if !ok {
		result, err = s.db.AllComponentsUsageByCluster(ctx, cluster)
	} else {
		result, err = s.db.ComponentsUsage(ctx, cluster, components)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get components usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ComponentUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取指定集群中各个addon的资源分配情况
func (s *Server) addonUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.AddOnUsage
	var err error

	cluster := vars["cluster"]
	if cluster == "" {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: cluster",
		}, errors.Errorf("missing param: cluster")
	}

	vals := r.URL.Query()
	addons, ok := vals["addon"]
	if !ok {
		result, err = s.db.AllAddonsUsageByCluster(ctx, cluster)
	} else {
		result, err = s.db.AddonsUsage(ctx, cluster, addons)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get addons usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: AddOnUsageUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取project的资源分配情况
func (s *Server) projectUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.ProjectUsage
	var err error

	cluster := vars["cluster"]
	if cluster == "" {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: cluster",
		}, errors.Errorf("missing param: cluster")
	}

	vals := r.URL.Query()
	project, ok := vals["project"]
	if !ok {
		result, err = s.db.AllProjectUsageByCluster(ctx, cluster)
	} else {
		result, err = s.db.ProjectsUsage(ctx, project)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ProjectUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取application的资源分配情况
func (s *Server) applicationUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.ApplicationUsage
	var err error

	vals := r.URL.Query()
	application, ok := vals["application"]
	if !ok {
		project, ok := vals["project"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: project",
			}, errors.Errorf("missing param: project")
		}

		if len(project) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too mangy param: project",
			}, errors.Errorf("too mangy param: project")
		}
		result, err = s.db.AllApplicationUsageByProject(ctx, project[0])
	} else {
		result, err = s.db.ApplicationsUsage(ctx, application)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get application usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ApplicationUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取runtime的资源分配情况
func (s *Server) runtimeUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.RuntimeUsage
	var err error

	vals := r.URL.Query()
	runtime, ok := vals["runtime"]
	if !ok {
		application, ok := vals["application"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: application",
			}, errors.Errorf("missing param: application")
		}

		if len(application) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too mangy param: application",
			}, errors.Errorf("too mangy param: application")
		}
		result, err = s.db.AllRuntimeUsageByApplication(ctx, application[0])
	} else {
		result, err = s.db.RuntimesUsage(ctx, runtime)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get runtime usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: RuntimeUsageResponse{
			Result: result,
		},
	}, nil
}

// 获取service的资源分配情况
func (s *Server) serviceUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result []*api.ServiceUsage
	var err error

	vals := r.URL.Query()
	runtime, ok := vals["runtime"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: runtime",
		}, errors.Errorf("missing param: runtime")
	}

	if len(runtime) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too mangy param: runtime",
		}, errors.Errorf("too mangy param: runtime")
	}

	service, ok := vals["service"]
	if !ok {

		result, err = s.db.AllServiceUsageByRuntime(ctx, runtime[0])
	} else {
		result, err = s.db.ServicesUsage(ctx, runtime[0], service)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get service usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ServiceUsageResponse{
			Result: result,
		},
	}, nil
}

func (s *Server) instanceUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result *api.ContainerUsage
	var err error

	cluster := vars["cluster"]
	instance := vars["instance"]

	result, err = s.db.ContainerUsage(ctx, cluster, instance)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get single instance usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: InstanceUsageResponse{
			Result: result,
		},
	}, nil
}

func (s *Server) instancesUsage(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var requestType []string
	var ok bool

	vals := r.URL.Query()
	requestType, ok = vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(requestType) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch instanceRequestType(requestType[0]) {
	case INSTANCE_REQUEST_TYPE_SERVICE:
		return s.serviceUsage(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_RUNTIME:
		return s.runtimeUsage(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_APPLICATION:
		return s.applicationUsage(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_PROJECT:
		return s.projectUsage(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_COMPONENT:
		return s.componentUsage(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_ADDON:
		return s.addonUsage(ctx, r, vars)
	default:
		content := fmt.Sprintf("invalid type param(%s), support: %v", requestType, allInstanceTypes)
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: content,
		}, errors.Errorf(content)
	}
}
