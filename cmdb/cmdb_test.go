// +build !default

package main

import (
	"encoding/json"
	"math"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"terminus.io/dice/dice/cmdb/conf"

	"github.com/gavv/httpexpect"
	"github.com/stretchr/testify/assert"
	"terminus.io/dice/dice/cmdb/api"
)

const (
	letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	cluster     = "TEST"
)

var (
	header = map[string]string{"Accept": "application/vnd.dice+json", "version": "1.0"}

	headKey = "/cmdb/"

	e *httpexpect.Expect

	t *testing.T
)

func randomIP(size int) []string {
	var result []string

	rand.Seed(time.Now().UnixNano())
	for i := 1; i <= size; i++ {
		a := strconv.Itoa(rand.Intn(254))
		b := strconv.Itoa(rand.Intn(254))
		c := strconv.Itoa(rand.Intn(254))
		d := strconv.Itoa(rand.Intn(254))
		ip := a + "." + b + "." + c + "." + d
		result = append(result, ip)
	}
	return result
}

func RandStringBytes(n, size int) []string {
	var result []string

	rand.Seed(time.Now().UnixNano())
	for i := 1; i <= size; i++ {
		b := make([]byte, n)
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}
		result = append(result, string(b))
	}
	return result
}

func ProduceTestHost(number int, cluster string, endpoint *httpexpect.Expect, deleted bool) []string {
	var (
		host      api.CmHost
		list      []string
		hostIps   = randomIP(10)
		hostNames []string
	)

	for i := 0; i < number; i++ {
		hostNames = append(hostNames, "test"+strconv.Itoa(i+1))
	}

	for i := 0; i < number; i++ {
		host.PrivateAddr = hostIps[i]
		host.Cpus = 160
		host.Memory = 34359738368
		host.Disk = 107374182400
		host.Cluster = cluster
		host.Deleted = deleted
		host.Birthday = time.Now().UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
		host.Name = hostNames[i]
		host.TimeStamp = time.Now().UnixNano()
		host.Labels = "MESOS_ATTRIBUTES=dice-tags:any,dev,locked,platform;\n"
		body := map[string]interface{}{
			"key":   headKey + "host/" + host.Cluster + "/" + host.PrivateAddr,
			"value": host,
		}
		endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(body).Expect().Status(http.StatusOK)
		list = append(list, host.PrivateAddr)
	}
	return list
}

func ProduceTestContainer(number int, cluster string, addon bool, component bool, deleted bool, list []string, endpoint *httpexpect.Expect) {
	if component {
		for _, ip := range list {

			var (
				componentContainerIps = randomIP(10)
				componentContainerID  = RandStringBytes(10, 15)
				components            = RandStringBytes(5, 10)
			)

			for i := 0; i < number; i++ {
				con := &api.CmContainer{
					ID:                componentContainerID[i],
					HostPrivateIPAddr: ip,
					CPU:               0.04,
					Memory:            1073741824,
					Disk:              536870912,
					Cluster:           cluster,
					DiceComponent:     components[i],
					IPAddress:         componentContainerIps[i],
					Deleted:           deleted,
					TimeStamp:         time.Now().UnixNano(),
				}

				//conIndex := &ComponentIndex{
				//	Create: time.Now().String(),
				//}
				indexBody := map[string]interface{}{
					"key": headKey + "component/" + con.Cluster + "/" + con.DiceComponent + "/" + con.HostPrivateIPAddr + "/" + con.IPAddress + "/" + con.ID,
					//"value": conIndex,
				}
				body := map[string]interface{}{
					"key":   headKey + "container/" + con.Cluster + "/" + con.HostPrivateIPAddr + "/" + con.IPAddress + "/" + con.ID,
					"value": con,
				}
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(body).Expect().Status(http.StatusOK)
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(indexBody).Expect().Status(http.StatusOK)
			}
		}
	} else if addon {
		for _, ip := range list {

			var (
				addonContainerIps = randomIP(10)
				addonContainerID  = RandStringBytes(10, 15)
				addons            = RandStringBytes(5, 10)
			)

			for i := 0; i < number; i++ {
				add := &api.CmContainer{
					ID:                addonContainerID[i],
					HostPrivateIPAddr: ip,
					CPU:               0.04,
					Memory:            1073741824,
					Disk:              536870912,
					Cluster:           cluster,
					DiceAddon:         addons[i],
					IPAddress:         addonContainerIps[i],
					Deleted:           deleted,
					TimeStamp:         time.Now().UnixNano(),
				}
				//conIndex := &AddOnIndex{
				//	Create: time.Now().String(),
				//}
				indexBody := map[string]interface{}{
					"key": headKey + "addon/" + add.Cluster + "/" + add.DiceAddon + "/" + add.HostPrivateIPAddr + "/" + add.IPAddress + "/" + add.ID,
					//"value": conIndex,
				}
				body := map[string]interface{}{
					"key":   headKey + "container/" + add.Cluster + "/" + add.HostPrivateIPAddr + "/" + add.IPAddress + "/" + add.ID,
					"value": add,
				}
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(body).Expect().Status(http.StatusOK)
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(indexBody).Expect().Status(http.StatusOK)
			}
		}
	} else {
		for _, ip := range list {

			var (
				containerIps    = randomIP(10)
				containerID     = RandStringBytes(10, 15)
				projects        = RandStringBytes(5, 10)
				applications    = RandStringBytes(5, 10)
				runtimes        = RandStringBytes(5, 10)
				services        = RandStringBytes(5, 10)
				projectName     = RandStringBytes(5, 10)
				applicationName = RandStringBytes(5, 10)
				runtimeName     = RandStringBytes(5, 10)
			)

			for i := 0; i < number; i++ {
				con := &api.CmContainer{
					ID:                  containerID[i],
					HostPrivateIPAddr:   ip,
					CPU:                 0.04,
					Memory:              1073741824,
					Disk:                536870912,
					Cluster:             cluster,
					DiceProject:         projects[i],
					DiceApplication:     applications[i],
					DiceRuntime:         runtimes[i],
					DiceService:         services[i],
					DiceProjectName:     projectName[i],
					DiceApplicationName: applicationName[i],
					DiceRuntimeName:     runtimeName[i],
					IPAddress:           containerIps[i],
					Deleted:             deleted,
					TimeStamp:           time.Now().UnixNano(),
				}
				body := map[string]interface{}{
					"key":   headKey + "container/" + con.Cluster + "/" + con.HostPrivateIPAddr + "/" + con.IPAddress + "/" + con.ID,
					"value": con,
				}
				indexBody := map[string]interface{}{
					"key":   headKey + "index/" + con.Cluster + "/" + con.DiceProject + "/" + con.DiceApplication + "/" + con.DiceRuntime + "/" + con.DiceService + "/" + con.HostPrivateIPAddr + "/" + con.IPAddress + "/" + con.ID,
					"value": "test",
				}
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(body).Expect().Status(http.StatusOK)
				endpoint.PUT("/api/dice/resource/put").WithHeaders(header).WithJSON(indexBody).Expect().Status(http.StatusOK)
			}
		}
	}
}

func CheckHosts(cluster string, endpoint *httpexpect.Expect) (disk, memory, cpu float64, err error) {
	var (
		response    ClusterHostsResponse
		hosts       []*api.CmHost
		totalDisk   int64
		totalCPU    float64
		totalMemory int64
	)

	repos := endpoint.GET("/api/dice/host/" + cluster).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	obj.ContainsKey("result").Value("result").Array().Length().Equal(10)

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err = json.Unmarshal(bytes, &hosts)

	for _, host := range hosts {
		totalDisk += host.Disk
		totalMemory += host.Memory
		totalCPU += host.Cpus
	}

	disk = math.Trunc(float64(totalDisk/1024/1024/1024)*1e2) * 1e-2
	memory = math.Trunc(float64(totalMemory/1024/1024/1024)*1e2) * 1e-2
	cpu = math.Trunc(totalCPU*1e2) * 1e-2

	return disk, memory, cpu, err

}

func CheckContainers(cluster string, endpoint *httpexpect.Expect) (disk, memory int64, cpu float64, err error) {
	var (
		response    GetContainersResponse
		containers  []*api.CmContainer
		totalDisk   int64
		totalCPU    float64
		totalMemory int64
	)

	repos := endpoint.GET("/api/dice/container/" + cluster).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	obj.ContainsKey("result").Value("result").Array().Length().Equal(300)

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err = json.Unmarshal(bytes, &containers)

	for _, container := range containers {
		totalDisk += container.Disk
		totalMemory += container.Memory
		totalCPU += container.CPU
	}

	return totalDisk, totalMemory, totalCPU, err
}

func CheckComponentContainers(cluster string, endpoint *httpexpect.Expect) (disk, memory int64, cpu float64, err error) {
	var (
		response    GetContainersResponse
		containers  []*api.CmContainer
		totalDisk   int64
		totalCPU    float64
		totalMemory int64
	)

	repos := endpoint.GET("/api/dice/component/" + cluster).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	obj.ContainsKey("result").Value("result").Array().Length().Equal(100)

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err = json.Unmarshal(bytes, &containers)

	for _, container := range containers {
		totalDisk += container.Disk
		totalMemory += container.Memory
		totalCPU += container.CPU
	}

	return totalDisk, totalMemory, totalCPU, err
}

func CheckAddonContainers(cluster string, endpoint *httpexpect.Expect) (disk, memory int64, cpu float64, err error) {
	var (
		response    GetContainersResponse
		containers  []*api.CmContainer
		totalDisk   int64
		totalCPU    float64
		totalMemory int64
	)

	repos := endpoint.GET("/api/dice/addon/" + cluster).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	obj.ContainsKey("result").Value("result").Array().Length().Equal(100)

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err = json.Unmarshal(bytes, &containers)

	for _, container := range containers {
		totalDisk += container.Disk
		totalMemory += container.Memory
		totalCPU += container.CPU
	}

	return totalDisk, totalMemory, totalCPU, err
}

func GetClusterUsage(cluster string, endpoint *httpexpect.Expect) (api.ClusterUsage, error) {
	var (
		response ClusterUsageResponse
		usage    api.ClusterUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetComponentUsage(cluster string, endpoint *httpexpect.Expect) ([]*api.ComponentUsage, error) {
	var (
		response ComponentUsageResponse
		usage    []*api.ComponentUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/component").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetAddOnUsage(cluster string, endpoint *httpexpect.Expect) ([]*api.AddOnUsage, error) {
	var (
		response AddOnUsageUsageResponse
		usage    []*api.AddOnUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/addon").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetHostUsage(cluster string, endpoint *httpexpect.Expect) ([]*api.HostUsage, error) {
	var (
		response HostsUsageResponse
		usage    []*api.HostUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/host").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetProjectUsage(cluster string, endpoint *httpexpect.Expect) ([]*api.ProjectUsage, error) {
	var (
		response ProjectUsageResponse
		usage    []*api.ProjectUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/project").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetApplicationUsage(cluster, project string, endpoint *httpexpect.Expect) ([]*api.ApplicationUsage, error) {
	var (
		response ApplicationUsageResponse
		usage    []*api.ApplicationUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/" + project + "/application").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetRunTimeUsage(cluster, project, application string, endpoint *httpexpect.Expect) ([]*api.RuntimeUsage, error) {
	var (
		response RuntimeUsageResponse
		usage    []*api.RuntimeUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/" + project + "/" + application + "/runtime").
		WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetServiceUsage(cluster, project, application, runtime string, endpoint *httpexpect.Expect) ([]*api.ServiceUsage, error) {
	var (
		response ServiceUsageResponse
		usage    []*api.ServiceUsage
	)

	repos := endpoint.GET("/api/dice/usage/" + cluster + "/" + project + "/" + application + "/" + runtime + "/service").
		WithHeaders(header).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &usage)

	return usage, err
}

func GetSearchResult(cluster string, hostAddr string, endpoint *httpexpect.Expect) ([]*api.Resource, error) {
	var (
		response SearchResult
		resource []*api.Resource
	)

	object := map[string]interface{}{"keyword": hostAddr}

	repos := endpoint.POST("/api/dice/resource/" + cluster + "/search").
		WithHeaders(header).WithJSON(object).Expect().Status(http.StatusOK).JSON()

	repos.Schema(response)

	obj := repos.Object()

	result := obj.ContainsKey("result").Value("result").Raw()

	bytes, _ := json.Marshal(result)

	err := json.Unmarshal(bytes, &resource)

	return resource, err
}

func checkComponentUsage() {
	var (
		compDisk   float64
		compMemory float64
		compCPU    float64
	)

	componentUsage, err := GetComponentUsage(cluster, e)
	assert.Nil(t, err)
	for _, usage := range componentUsage {
		compDisk += usage.Disk
		compMemory += usage.Memory
		compCPU += usage.CPU
	}

	componentDisk, componentMemory, componentCPU, err := CheckComponentContainers(cluster, e)
	assert.Nil(t, err)

	assert.Equal(t, compDisk, math.Trunc(float64(componentDisk/1024/1024)*1e2)*1e-2)
	assert.Equal(t, compMemory, math.Trunc(float64(componentMemory/1024/1024)*1e2)*1e-2)
	assert.Equal(t, math.Trunc(compCPU*1e2)*1e-2, math.Trunc(componentCPU*1e2)*1e-2)
}

func checkAddonUsage() {
	var (
		addonDisk   float64
		addonMemory float64
		addonCPU    float64
	)

	addonUsage, err := GetAddOnUsage(cluster, e)
	assert.Nil(t, err)
	for _, usage := range addonUsage {
		addonDisk += usage.Disk
		addonMemory += usage.Memory
		addonCPU += usage.CPU
	}

	addonsDisk, addonsMemory, addonsCPU, err := CheckAddonContainers(cluster, e)
	assert.Nil(t, err)

	assert.Equal(t, addonDisk, math.Trunc(float64(addonsDisk/1024/1024)*1e2)*1e-2)
	assert.Equal(t, addonMemory, math.Trunc(float64(addonsMemory/1024/1024)*1e2)*1e-2)
	assert.Equal(t, math.Trunc(addonCPU*1e2)*1e-2, math.Trunc(addonsCPU*1e2)*1e-2)
}

func checkHostUsage() {
	var hostContainersResponse GetContainersResponse
	hostUsage, err := GetHostUsage(cluster, e)
	assert.Nil(t, err)
	for _, usage := range hostUsage {
		var (
			containers []*api.CmContainer
			hostDisk   int64
			hostMemory int64
			hostCPU    float64
		)

		repos := e.GET("/api/dice/host/" + cluster + "/" + usage.IPAddress + "/container").WithHeaders(header).Expect().Status(http.StatusOK).JSON()

		repos.Schema(hostContainersResponse)

		obj := repos.Object()

		result := obj.ContainsKey("result").Value("result").Raw()

		bytes, _ := json.Marshal(result)

		err = json.Unmarshal(bytes, &containers)

		assert.Nil(t, err)

		for _, container := range containers {
			hostDisk += container.Disk
			hostMemory += container.Memory
			hostCPU += container.CPU
		}

		assert.Equal(t, usage.UsedDisk, math.Trunc(float64(hostDisk/1024/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.UsedMemory, math.Trunc(float64(hostMemory/1024/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.UsedCPU, api.Round(hostCPU, 2))
	}
}

func checkProjectUsage() []*api.ProjectUsage {
	var projectContainersResponse GetContainersResponse
	projectUsage, err := GetProjectUsage(cluster, e)
	assert.Nil(t, err)

	for _, usage := range projectUsage {
		var (
			containers []*api.CmContainer
			proDisk    int64
			proMemory  int64
			proCPU     float64
		)

		repos := e.GET("/api/dice/container/" + cluster + "/" + usage.ID).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

		repos.Schema(projectContainersResponse)

		obj := repos.Object()

		result := obj.ContainsKey("result").Value("result").Raw()

		bytes, _ := json.Marshal(result)

		json.Unmarshal(bytes, &containers)

		for _, container := range containers {
			proDisk += container.Disk
			proMemory += container.Memory
			proCPU += container.CPU
		}

		assert.Equal(t, usage.Disk, math.Trunc(float64(proDisk/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.Memory, math.Trunc(float64(proMemory/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.CPU, math.Trunc(proCPU*1e2)*1e-2)
	}
	return projectUsage
}

func checkApplicationUsage(project string) []*api.ApplicationUsage {
	var applicationContainersResponse GetContainersResponse
	applicationUsage, err := GetApplicationUsage(cluster, project, e)
	assert.Nil(t, err)

	for _, usage := range applicationUsage {
		var (
			containers []*api.CmContainer
			appDisk    int64
			appMemory  int64
			appCPU     float64
		)

		repos := e.GET("/api/dice/container/" + cluster + "/" + project + "/" + usage.ID).WithHeaders(header).Expect().Status(http.StatusOK).JSON()

		repos.Schema(applicationContainersResponse)

		obj := repos.Object()

		result := obj.ContainsKey("result").Value("result").Raw()

		bytes, _ := json.Marshal(result)

		json.Unmarshal(bytes, &containers)

		for _, container := range containers {
			appDisk += container.Disk
			appMemory += container.Memory
			appCPU += container.CPU
		}

		assert.Equal(t, usage.Disk, math.Trunc(float64(appDisk/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.Memory, math.Trunc(float64(appMemory/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.CPU, math.Trunc(appCPU*1e2)*1e-2)
	}
	return applicationUsage
}

func checkRuntimeUsage(project, application string) []*api.RuntimeUsage {
	var runtimeContainersResponse GetContainersResponse
	runtimeUsage, err := GetRunTimeUsage(cluster, project, application, e)
	assert.Nil(t, err)

	for _, usage := range runtimeUsage {
		var (
			containers    []*api.CmContainer
			runtimeDisk   int64
			runtimeMemory int64
			runtimeCPU    float64
		)

		repos := e.GET("/api/dice/container/" + cluster + "/" + project + "/" + application + "/" + usage.ID).
			WithHeaders(header).Expect().Status(http.StatusOK).JSON()

		repos.Schema(runtimeContainersResponse)

		obj := repos.Object()

		result := obj.ContainsKey("result").Value("result").Raw()

		bytes, _ := json.Marshal(result)

		json.Unmarshal(bytes, &containers)

		for _, container := range containers {
			runtimeDisk += container.Disk
			runtimeMemory += container.Memory
			runtimeCPU += container.CPU
		}

		assert.Equal(t, usage.Disk, math.Trunc(float64(runtimeDisk/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.Memory, math.Trunc(float64(runtimeMemory/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.CPU, math.Trunc(runtimeCPU*1e2)*1e-2)
	}
	return runtimeUsage
}

func checkServiceUsage(project, application, runtime string) {
	var serviceContainersResponse GetContainersResponse
	serviceUsage, err := GetServiceUsage(cluster, project, application, runtime, e)
	assert.Nil(t, err)

	for _, usage := range serviceUsage {
		var (
			containers    []*api.CmContainer
			serviceDisk   int64
			serviceMemory int64
			serviceCPU    float64
		)

		repos := e.GET("/api/dice/container/" + cluster + "/" + project + "/" + application + "/" + runtime + "/" + usage.Name).
			WithHeaders(header).Expect().Status(http.StatusOK).JSON()

		repos.Schema(serviceContainersResponse)

		obj := repos.Object()

		result := obj.ContainsKey("result").Value("result").Raw()

		bytes, _ := json.Marshal(result)

		json.Unmarshal(bytes, &containers)

		for _, container := range containers {
			serviceDisk += container.Disk
			serviceMemory += container.Memory
			serviceCPU += container.CPU
		}

		assert.Equal(t, usage.Disk, math.Trunc(float64(serviceDisk/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.Memory, math.Trunc(float64(serviceMemory/1024/1024)*1e2)*1e-2)
		assert.Equal(t, usage.CPU, math.Trunc(serviceCPU*1e2)*1e-2)
	}
}

func Test(t *testing.T) {
	err := conf.Parse()
	assert.Nil(t, err)

	server := NewServer("")
	server.initEndpoints()

	testserver := httptest.NewServer(server.router)
	defer testserver.Close()

	e := httpexpect.New(t, testserver.URL)

	// 添加一些测试数据

	list := ProduceTestHost(10, cluster, e, false)
	//ProduceTestApp(10, t, e)

	// 判断添加的主机数量是否正确
	disk, memory, cpu, err := CheckHosts(cluster, e)
	assert.Nil(t, err)

	// 添加一些服务容器与组件容器，判断添加的数量是否正确
	ProduceTestContainer(10, cluster, false, false, false, list, e)
	ProduceTestContainer(10, cluster, true, false, false, list, e)
	ProduceTestContainer(10, cluster, false, true, false, list, e)

	serviceDisk, serviceMemory, serviceCPU, err := CheckContainers(cluster, e)
	assert.Nil(t, err)

	usage, err := GetClusterUsage(cluster, e)
	assert.Nil(t, err)

	// 验证集群资源分配情况
	assert.Equal(t, usage.UsedDisk, math.Trunc(float64(serviceDisk/1024/1024/1024)*1e2)*1e-2)
	assert.Equal(t, usage.UsedMemory, math.Trunc(float64(serviceMemory/1024/1024/1024)*1e2)*1e-2)
	assert.Equal(t, usage.UsedCPU, math.Trunc(api.Round(serviceCPU, 2)*1e2)*1e-2)
	assert.Equal(t, usage.TotalDisk, disk)
	assert.Equal(t, usage.TotalMemory, memory)
	assert.Equal(t, usage.TotalCPU, cpu)

	// 验证组件资源分配情况
	checkComponentUsage()

	// 验证中间件资源分配情况
	checkAddonUsage()

	// 验证主机资源分配情况
	checkHostUsage()

	// 验证project资源分配情况
	projectUsage := checkProjectUsage()

	// 验证application资源分配情况
	project := projectUsage[0].ID
	applicationUsage := checkApplicationUsage(project)

	// 验证runtime资源分配情况
	application := applicationUsage[0].ID
	runtimeUsage := checkRuntimeUsage(project, application)

	// 验证service资源分配情况
	runtime := runtimeUsage[0].ID
	checkServiceUsage(project, application, runtime)

	resource, err := GetSearchResult(cluster, list[0], e)
	assert.Nil(t, err)
	assert.Equal(t, len(resource), 31) // 1 host, 10 addons, 10 components, 10 service container...

}
