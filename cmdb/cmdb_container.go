package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"terminus.io/dice/dice/cmdb/api"
)

// 获取指定 container ID 信息
// params:
// - instance: 暂时只支持 container id
func (s *Server) queryContainer(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]
	instance := vars["instance"]

	result, err := s.db.QueryContainer(ctx, cluster, instance)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to query single container from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: QueryContainerResponse{
			Result: result,
		},
	}, nil
}

// 获取指定条件下的批量 containers
func (s *Server) getContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var requestType []string
	var ok bool

	vals := r.URL.Query()
	requestType, ok = vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(requestType) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch instanceRequestType(requestType[0]) {
	case INSTANCE_REQUEST_TYPE_SERVICE:
		return s.serviceContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_RUNTIME:
		return s.runtimeContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_APPLICATION:
		return s.applicationContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_PROJECT:
		return s.projectContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_HOST:
		return s.hostContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_ORG:
		return s.orgContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_COMPONENT:
		return s.componentContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_ADDON:
		return s.addonContainers(ctx, r, vars)
	case INSTANCE_REQUEST_TYPE_CLUSTER:
		return s.clusterContainers(ctx, r, vars)
	default:
		content := fmt.Sprintf("invalid type param(%s), support: %v", requestType, allInstanceTypes)
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: content,
		}, errors.Errorf(content)
	}
}

// 获取整个 cluster 所有的 containers
func (s *Server) clusterContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.AllContainersByCluster(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get cluster containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 host 下所有 containers 的信息
func (s *Server) hostContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	vals := r.URL.Query()
	host, ok := vals["host"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: host",
		}, errors.Errorf("missing param: host")
	}

	result, err := s.db.AllContainersByHost(ctx, cluster, host)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get host contaniers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 org 下所有 containers
func (s *Server) orgContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	vals := r.URL.Query()

	org, ok := vals["org"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: org",
		}, errors.Errorf("missing param: org")
	}

	if len(org) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: org",
		}, errors.Errorf("too many param: org")
	}

	result, err := s.db.AllContainersByOrg(ctx, org[0])
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 project 下所有 containers
func (s *Server) projectContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	vals := r.URL.Query()

	project, ok := vals["project"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: project",
		}, errors.Errorf("missing param: project")
	}

	result, err := s.db.AllContainersByProject(ctx, project)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 appliaction 下所有 containers
func (s *Server) applicationContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	vals := r.URL.Query()

	application, ok := vals["application"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: application",
		}, errors.Errorf("missing param: application")
	}

	result, err := s.db.AllContainersByApplication(ctx, application)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get application containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 runtime 下所有 containers
func (s *Server) runtimeContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	vals := r.URL.Query()

	runtime, ok := vals["runtime"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: runtime",
		}, errors.Errorf("missing param: runtime")
	}

	result, err := s.db.AllContainersByRuntime(ctx, runtime)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get runtime containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 service 下所有 containers
func (s *Server) serviceContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	vals := r.URL.Query()

	runtime, ok := vals["runtime"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: runtime",
		}, errors.Errorf("missing param: runtime")
	}

	if len(runtime) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: runtime",
		}, errors.Errorf("too many params: runtime")
	}

	service, ok := vals["service"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "input error: service",
		}, errors.Errorf("input error: service")
	}

	result, err := s.db.AllContainersByService(ctx, runtime[0], service)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get service containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 component 下所有的containers
func (s *Server) componentContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result *[]api.CmContainer
	var err error

	cluster := vars["cluster"]

	vals := r.URL.Query()
	component, ok := vals["component"]
	if !ok {
		result, err = s.db.AllComponentsByCluster(ctx, cluster)
	} else {
		result, err = s.db.AllContainersByComponent(ctx, cluster, component)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get component contianers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}

// 获取指定 component 下所有的containers
func (s *Server) addonContainers(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result *[]api.CmContainer
	var err error

	cluster := vars["cluster"]

	vals := r.URL.Query()
	addon, ok := vals["addon"]
	if !ok {
		result, err = s.db.AllAddonsByCluster(ctx, cluster)
	} else {
		result, err = s.db.AllContainersByAddon(ctx, cluster, addon)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get addon containers from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: GetContainersResponse{
			Result: result,
		},
	}, nil
}
