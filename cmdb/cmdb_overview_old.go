package main

import (
	"context"
	"net/http"
)

// 获取整个平台的部署总览信息
func (s *Server) platformDeployOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	result, err := s.db.DeploymentSummaryByPlatform(ctx)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get platform deployment overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定企业的部署总览信息
func (s *Server) orgDeployOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	orgId := vars["orgId"]

	result, err := s.db.DeploymentSummaryByOrg(ctx, orgId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org deployment overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定项目的部署总览信息
func (s *Server) projectDeployOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	projectId := vars["projectId"]

	result, err := s.db.DeploymentSummaryByProject(ctx, projectId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project deployment overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取整个平台的构建总览信息
func (s *Server) platformBuildOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	result, err := s.db.BuildSummaryByPlatform(ctx)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get platform build overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定企业的构建总览信息
func (s *Server) orgBuildOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	orgId := vars["orgId"]

	result, err := s.db.BuildSummaryByOrg(ctx, orgId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org build overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定项目的构建总览信息
func (s *Server) projectBuildOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	projectId := vars["projectId"]

	result, err := s.db.BuildSummaryByProject(ctx, projectId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project build overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取整个平台的 task 总览信息
func (s *Server) platformTaskOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	result, err := s.db.TaskSummaryByPlatform(ctx)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get platform task overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: TaskSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定企业的 task 总览信息
func (s *Server) orgTaskOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	orgId := vars["orgId"]

	result, err := s.db.TaskSummaryByOrg(ctx, orgId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org task overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: TaskSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取指定项目的 task 总览信息
func (s *Server) projectTaskOverview(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	projectId := vars["projectId"]

	result, err := s.db.TaskSummaryByProject(ctx, projectId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project task overview info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: TaskSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取整个平台的部署动态
func (s *Server) platformDeployDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	result, err := s.db.DeployDynamicByPlatform(ctx)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get platform deploy dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 获取指定企业的部署动态
func (s *Server) orgDeployDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	orgId := vars["orgId"]

	result, err := s.db.DeployDynamicByOrg(ctx, orgId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org deploy dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 获取指定项目的部署动态
func (s *Server) projectDeployDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	projectId := vars["projectId"]

	result, err := s.db.DeployDynamicByProject(ctx, projectId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project deploy dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 获取整个平台的构建动态
func (s *Server) platformBuildDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	result, err := s.db.BuildDynamicByPlatform(ctx)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get platform build dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 获取指定企业的构建动态
func (s *Server) orgBuildDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	orgId := vars["orgId"]

	result, err := s.db.BuildDynamicByOrg(ctx, orgId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get org build dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 获取指定项目的构建动态
func (s *Server) projectBuildDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	projectId := vars["projectId"]

	result, err := s.db.BuildDynamicByProject(ctx, projectId)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get project build dynamic info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}
