// +build !default

package api

import (
	"context"
	"os"
	"testing"

	"terminus.io/dice/dice/cmdb/conf"

	"github.com/stretchr/testify/assert"
)

func newDBClient() (*DbClient, error) {
	os.Setenv("CMDB_DB_HOST", "9.0.11.12:3306")
	os.Setenv("CMDB_DB_USER", "root")
	os.Setenv("CMDB_DB_PASSWD", "123456")
	os.Setenv("CMDB_DB_NAME", "dice")

	defer func() {
		os.Unsetenv("CMDB_DB_HOST")
		os.Unsetenv("CMDB_DB_USER")
		os.Unsetenv("CMDB_DB_PASSWD")
		os.Unsetenv("CMDB_DB_NAME")
	}()

	if err := conf.Parse(); err != nil {
		return nil, err
	}

	return NewDBClient(&conf.DbDial)
}

func TestDeploymentSummaryByPlatform(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeploymentSummaryByPlatform(context.Background())
	assert.Nil(t, err)

	t.Logf("TestDeploymentSummaryByPlatform result: %+v", summary)
}

func TestDeploymentSummaryByOrg(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeploymentSummaryByOrg(context.Background(), "1")
	assert.Nil(t, err)

	t.Logf("TestDeploymentSummaryByPlatform result: %+v", summary)
}

func TestDeploymentSummaryByProject(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeploymentSummaryByProject(context.Background(), "5")
	assert.Nil(t, err)

	t.Logf("TestDeploymentSummaryByPlatform result: %+v", summary)
}

func TestBuildSummaryByPlatform(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildSummaryByPlatform(context.Background())
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByPlatform result: %+v", summary)
}

func TestBuildSummaryByOrg(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildSummaryByOrg(context.Background(), "1")
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByOrg result: %+v", summary)
}

func TestBuildSummaryByProject(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildSummaryByProject(context.Background(), "5")
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByProject result: %+v", summary)
}

func TestTaskSummaryByPlatform(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.TaskSummaryByPlatform(context.Background())
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByPlatform result: %+v", summary)
}

func TestTaskSummaryByOrg(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.TaskSummaryByOrg(context.Background(), "1")
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByOrg result: %+v", summary)
}

func TestTaskSummaryByProject(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.TaskSummaryByProject(context.Background(), "5")
	assert.Nil(t, err)

	t.Logf("TestBuildSummaryByProject result: %+v", summary)
}

func TestDeployDynamicByPlatform(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeployDynamicByPlatform(context.Background())
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}

func TestDeployDynamicByOrg(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeployDynamicByOrg(context.Background(), "1")
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}

func TestDeployDynamicByProject(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.DeployDynamicByProject(context.Background(), "5")
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}

func TestBuildDynamicByPlatform(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildDynamicByPlatform(context.Background())
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}

func TestBuildDynamicByOrg(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildDynamicByOrg(context.Background(), "1")
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}

func TestBuildDynamicByProject(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	summary, err := db.BuildDynamicByProject(context.Background(), "5")
	assert.Nil(t, err)

	t.Logf("TestDeployDynamicByPlatform result: %+v", summary)
}
