package api

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	// mysql orm
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"terminus.io/dice/dice/cmdb/conf"
)

func newDB(c *conf.DbDialInfo) (*gorm.DB, error) {
	if c == nil {
		return nil, errors.Errorf("invalid params")
	}

	url := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		c.Username, c.Password, c.Host, c.Database)

	db, err := gorm.Open("mysql", url)
	if err != nil {
		return nil, err
	}

	// connection pool
	db.DB().SetMaxIdleConns(10)
	db.DB().SetConnMaxLifetime(200)
	db.DB().SetConnMaxLifetime(time.Hour)

	// TODO: CreateTableIsExisted
	// 创建所有的表
	if !db.HasTable(&CmHost{}) {
		if err := db.CreateTable(&CmHost{}).Error; err != nil {
			return nil, err
		}
	}

	if !db.HasTable(&CmContainer{}) {
		if err := db.CreateTable(&CmContainer{}).Error; err != nil {
			return nil, err
		}
	}

	if !db.HasTable(&CmService{}) {
		if err := db.CreateTable(&CmService{}).Error; err != nil {
			return nil, err
		}
	}

	return db, nil
}

// NewDBClient create new db client
func NewDBClient(c *conf.DbDialInfo) (*DbClient, error) {
	var err error

	client := &DbClient{}
	client.DB, err = newDB(c)

	return client, err
}
