package api

import (
	"context"
	"strings"

	"github.com/pkg/errors"
)

// UpdateContainer 更新container元数据
func (db *DbClient) UpdateContainer(ctx context.Context, messageType string, c *CmContainer) error {
	var err error

	if err = db.deleteOrUpdateContainer(ctx, c); err != nil {
		return err
	}

	if messageType == "diff" {
		containerDiffCount++
	}
	containerCount++

	return nil
}

// deleteOrUpdateContainer 更新操作分为两种 更新或删除
func (db *DbClient) deleteOrUpdateContainer(ctx context.Context, c *CmContainer) error {
	var err error

	if c.Deleted {
		if err = db.deleteContainer(ctx, c); err != nil {
			return err
		}
	} else {
		if !c.isDiceContainer() {
			c.setUnknown()
		}

		if err = db.updateContainer(ctx, c); err != nil {
			return err
		}
	}
	return nil
}

// updateContainer 更新 container 信息
func (db *DbClient) updateContainer(ctx context.Context, c *CmContainer) error {
	return db.Where("cluster = ? AND dice_project = ? AND dice_runtime = ? AND dice_service = ? AND container_id = ?",
		c.Cluster, c.DiceProject, c.DiceRuntime, c.DiceService, c.ID).Save(c).Error
}

// deleteContainer 删除 container 信息
func (db *DbClient) deleteContainer(ctx context.Context, c *CmContainer) error {
	return db.Where("cluster = ? AND dice_project = ? AND dice_runtime = ? AND dice_service = ? AND container_id = ?",
		c.Cluster, c.DiceProject, c.DiceRuntime, c.DiceService, c.ID).Delete(CmContainer{}).Error
}

// QueryContainer 获取单个容器信息
func (db *DbClient) QueryContainer(ctx context.Context, cluster string, id string) (*CmContainer, error) {
	var container CmContainer
	var err error

	if cluster == "" {
		return nil, errors.Errorf("invalid params: cluster is null")
	}

	if len(id) < 12 {
		return nil, errors.Errorf("invalid params: container_id's length < 12")
	}

	length := len(id)

	if err = db.Where("cluster = ? AND substring(container_id, 1, ?) = ?", cluster, length, id).First(&container).Error; err != nil {
		return nil, err
	}

	return &container, nil
}

// AllProjectsContainersByCluster 获取整个 cluster 所有通过 dice 创建的 containers
func (db *DbClient) AllProjectsContainersByCluster(ctx context.Context, cluster string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" {
		return nil, errors.Errorf("missing cluster params")
	}

	if err := db.Where("cluster = ? AND dice_project is not null AND dice_project <> ''", cluster).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByCluster 获取整个 cluster 所有的 containers
func (db *DbClient) AllContainersByCluster(ctx context.Context, cluster string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" {
		return nil, errors.Errorf("missing cluster params")
	}

	if err := db.Where("cluster = ?", cluster).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByHost 获取指定 host 下所有 containers
func (db *DbClient) AllContainersByHost(ctx context.Context, cluster string, host []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" || len(host) == 0 {
		return nil, errors.Errorf("missing params: cluster = %s, host = %v", cluster, host)
	}

	if err := db.Where("cluster = ? AND host_private_ip_addr in (?)", cluster, host).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// DeleteAllContainersByHost 删除指定 host 下所有 containers
func (db *DbClient) DeleteAllContainersByHost(ctx context.Context, cluster, host string) error {
	return db.Where("cluster = ? AND host_private_ip_addr = ?", cluster, host).Delete(CmContainer{}).Error
}

// AllContainersByOrg 获取指定 org 下所有 containers
func (db *DbClient) AllContainersByOrg(ctx context.Context, org string) (*[]CmContainer, error) {
	var containers []CmContainer

	if org == "" {
		return nil, errors.Errorf("missing org params")
	}

	if err := db.Where("dice_org = ?", org).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByProject 获取指定 project 下所有 containers
func (db *DbClient) AllContainersByProject(ctx context.Context, project []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if len(project) == 0 {
		return nil, errors.Errorf("missing project params")
	}

	if err := db.Where("dice_project in (?)", project).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByApplication 获取指定 appliaction 下所有 containers
func (db *DbClient) AllContainersByApplication(ctx context.Context, app []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if len(app) == 0 {
		return nil, errors.Errorf("missing application params")
	}

	if err := db.Where("dice_application in (?)", app).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByRuntime 获取指定 runtime 下所有 containers
func (db *DbClient) AllContainersByRuntime(ctx context.Context, runtime []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if len(runtime) == 0 {
		return nil, errors.Errorf("missing runtime params")
	}

	if err := db.Where("dice_runtime in (?)", runtime).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByService 获取指定 service 下所有 containers
func (db *DbClient) AllContainersByService(ctx context.Context, runtime string, service []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if runtime == "" || len(service) == 0 {
		return nil, errors.Errorf("invalid params: runtime = %s, service = %v", runtime, service)
	}

	if err := db.Where("dice_runtime = ? AND dice_service in (?)", runtime, service).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllComponentsByCluster 获取集群所有的 dice components containers
func (db *DbClient) AllComponentsByCluster(ctx context.Context, cluster string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	if err := db.Where("cluster = ? AND dice_component is not null AND dice_component <> ''", cluster).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByComponent 获取指定 component 下所有的containers
func (db *DbClient) AllContainersByComponent(ctx context.Context, cluster string, component []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" || len(component) == 0 {
		return nil, errors.Errorf("invalid param: cluster = %s, component = %v", cluster, component)
	}

	if err := db.Where("cluster = ? AND dice_component in (?)", cluster, component).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllAddonsByCluster 获取集群所有的 dice addons containers
func (db *DbClient) AllAddonsByCluster(ctx context.Context, cluster string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	if err := db.Where("cluster = ? AND dice_addon is not null AND dice_addon <> ''", cluster).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

// AllContainersByAddon 获取指定 component 下所有的containers
func (db *DbClient) AllContainersByAddon(ctx context.Context, cluster string, addon []string) (*[]CmContainer, error) {
	var containers []CmContainer

	if cluster == "" || len(addon) == 0 {
		return nil, errors.Errorf("invalid param: cluster = %s, addon = %v", cluster, addon)
	}

	if err := db.Where("cluster = ? AND dice_addon in (?)", cluster, addon).Find(&containers).Error; err != nil {
		return nil, err
	}

	for i, container := range containers {
		containers[i].CPU = Round(container.CPU, 2)
	}

	return &containers, nil
}

func (container *CmContainer) isDiceContainer() bool {
	if container.DiceComponent != "" || container.DiceAddon != "" || (container.DiceService != "" &&
		container.DiceApplication != "" && container.DiceRuntime != "" && container.DiceApplicationName != "" &&
		container.DiceRuntimeName != "" && container.DiceProject != "" && container.DiceProjectName != "") {
		return true
	}

	return false
}

func (container *CmContainer) setUnknown() {
	//container.DiceOrg = "unknown"
	container.DiceProject = unknownType
	container.DiceProjectName = unknownType
	container.DiceApplication = unknownType
	container.DiceApplicationName = unknownType
	container.DiceRuntime = unknownType
	container.DiceRuntimeName = unknownType
	container.DiceService = unknownType
}

func (container *CmContainer) check(value string) bool {
	splitStr := "/"
	containerStr := container.DiceProject + splitStr +
		container.DiceProjectName + splitStr +
		container.DiceApplication + splitStr +
		container.DiceApplicationName + splitStr +
		container.DiceRuntime + splitStr +
		container.DiceRuntimeName + splitStr +
		container.DiceService + splitStr +
		container.DiceComponent + splitStr +
		container.DiceAddon + splitStr +
		container.ID + splitStr +
		container.HostPrivateIPAddr + splitStr +
		container.Image + splitStr +
		container.IPAddress

	return strings.Contains(containerStr, value)
}

func (container *CmContainer) hasSearchValue(value string) bool {
	if container == nil {
		return false
	}

	if value == "*" {
		return true
	}

	return container.check(value)
}
