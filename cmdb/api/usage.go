package api

import (
	"context"
	"math"
	"sort"
	"strings"

	"github.com/pkg/errors"
)

// ClusterUsage 获取集群资源分配情况
func (db *DbClient) ClusterUsage(ctx context.Context, cluster string) (*ClusterUsage, error) {

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	hosts, err := db.AllHostsByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	var totalCPU float64
	var totalMem int64
	var totalDisk int64

	for _, host := range *hosts {
		totalCPU += host.Cpus
		totalMem += host.Memory
		totalDisk += host.Disk
	}

	containers, err := db.AllContainersByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	var usedCPU float64
	var usedMem int64
	var usedDisk int64

	for _, container := range *containers {
		usedCPU += container.CPU
		usedMem += container.Memory
		usedDisk += container.Disk
	}

	result := &ClusterUsage{
		TotalMemory: math.Trunc(float64(totalMem/1024/1024/1024)*1e2) * 1e-2,
		TotalCPU:    totalCPU,
		TotalDisk:   math.Trunc(float64(totalDisk/1024/1024/1024)*1e2) * 1e-2,
		UsedMemory:  math.Trunc(float64(usedMem/1024/1024/1024)*1e2) * 1e-2,
		UsedCPU:     Round(usedCPU, 2),
		UsedDisk:    math.Trunc(float64(usedDisk/1024/1024/1024)*1e2) * 1e-2,
	}

	return result, nil
}

// HostUsageByCluster 获取指定主机的资源分配情况列表
func (db *DbClient) HostUsageByCluster(ctx context.Context, cluster, addr string) (*HostUsage, error) {
	var (
		usedCPU  float64
		usedMem  int64
		usedDisk int64
		tasks    int
		usage    *HostUsage
	)

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	host, err := db.QueryHost(ctx, cluster, addr)
	if err != nil {
		return nil, err
	}

	ip := host.PrivateAddr

	containers, err := db.AllContainersByHost(ctx, cluster, []string{ip})
	if err != nil {
		return nil, err
	}

	for _, container := range *containers {
		usedCPU += container.CPU
		usedDisk += container.Disk
		usedMem += container.Memory
		tasks++
	}

	// 标签整理
	host.makeLabel()

	usage = &HostUsage{
		HostName:  host.Name,
		IPAddress: ip,
		TotalCPU:  host.Cpus,
		//TotalMemory: math.Trunc(float64(host.Memory/1024/1024/1024)*1e2) * 1e-2,
		TotalMemory: math.Floor((float64(host.Memory) / 1024 / 1024 / 1024) + 0.5),
		//TotalDisk:   math.Trunc(float64(host.Disk/1024/1024/1024)*1e2) * 1e-2,
		TotalDisk: math.Floor((float64(host.Disk) / 1024 / 1024 / 1024) + 0.5),
		UsedCPU:   Round(usedCPU, 2),
		//UsedMemory:  math.Trunc(float64(usedMem/1024/1024/1024)*1e2) * 1e-2,
		UsedMemory: math.Floor((float64(usedMem) / 1024 / 1024 / 1024) + 0.5),
		//UsedDisk:    math.Trunc(float64(usedDisk/1024/1024/1024)*1e2) * 1e-2,
		UsedDisk:  math.Floor((float64(usedDisk) / 1024 / 1024 / 1024) + 0.5),
		Labels:    host.Labels,
		Tasks:     tasks,
		CreatedAt: host.Birthday,
	}

	return usage, nil
}

// AllHostsUsageByCluster 获取指定集群下主机资源分配情况列表
func (db *DbClient) AllHostsUsageByCluster(ctx context.Context, cluster string) ([]*HostUsage, error) {
	var result []*HostUsage

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	hosts, err := db.AllHostsByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	for _, host := range *hosts {
		var (
			usedCPU  float64
			usedMem  int64
			usedDisk int64
			tasks    int
			usage    *HostUsage
		)

		ip := host.PrivateAddr

		containers, err := db.AllContainersByHost(ctx, cluster, []string{ip})
		if err != nil {
			return nil, err
		}

		for _, container := range *containers {
			usedCPU += container.CPU
			usedDisk += container.Disk
			usedMem += container.Memory
			tasks++
		}

		// 标签整理
		host.makeLabel()

		usage = &HostUsage{
			HostName:    host.Name,
			IPAddress:   ip,
			TotalCPU:    host.Cpus,
			TotalMemory: math.Floor((float64(host.Memory) / 1024 / 1024 / 1024) + 0.5),
			TotalDisk:   math.Floor((float64(host.Disk) / 1024 / 1024 / 1024) + 0.5),
			UsedCPU:     Round(usedCPU, 2),
			UsedMemory:  math.Floor((float64(usedMem) / 1024 / 1024 / 1024) + 0.5),
			UsedDisk:    math.Floor((float64(usedDisk) / 1024 / 1024 / 1024) + 0.5),
			Labels:      host.Labels,
			Tasks:       tasks,
			CreatedAt:   host.Birthday,
		}
		result = append(result, usage)
	}

	return result, nil
}

// AllComponentsUsageByCluster 获取指定集群下组件资源分配情况列表
func (db *DbClient) AllComponentsUsageByCluster(ctx context.Context, cluster string) ([]*ComponentUsage, error) {

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	containers, err := db.AllComponentsByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	components := make(map[string]*ComponentUsage)

	for _, container := range *containers {
		nameString := container.DiceComponent

		if com, ok := components[nameString]; ok {
			com.Instance++
			com.CPU += container.CPU
			com.Disk += float64(container.Disk)
			com.Memory += float64(container.Memory)
			components[nameString] = com
		} else {
			var component ComponentUsage
			components[nameString] = &component
			component.Name = nameString
			component.Instance++
			component.CPU += container.CPU
			component.Disk += float64(container.Disk)
			component.Memory += float64(container.Memory)
		}
	}

	var keys []string

	for key, component := range components {
		keys = append(keys, key)
		component.Memory = math.Trunc(component.Memory/1024/1024*1e2) * 1e-2
		component.Disk = math.Trunc(component.Disk/1024/1024*1e2) * 1e-2
		component.CPU = Round(component.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ComponentUsage, len(keys))

	for i, k := range keys {
		sorted[i] = components[k]
	}

	return sorted, nil
}

// ComponentsUsage 获取指定集群下指定components资源分配情况列表
func (db *DbClient) ComponentsUsage(ctx context.Context, cluster string, components []string) ([]*ComponentUsage, error) {

	if cluster == "" || len(components) == 0 {
		return nil, errors.Errorf("invalid param: cluster = %s, components = %v", cluster, components)
	}

	containers, err := db.AllContainersByComponent(ctx, cluster, components)
	if err != nil {
		return nil, err
	}

	compUsage := make(map[string]*ComponentUsage)

	for _, container := range *containers {
		nameString := container.DiceComponent

		if com, ok := compUsage[nameString]; ok {
			com.Instance++
			com.CPU += container.CPU
			com.Disk += float64(container.Disk)
			com.Memory += float64(container.Memory)
			compUsage[nameString] = com
		} else {
			var component ComponentUsage
			compUsage[nameString] = &component
			component.Name = nameString
			component.Instance++
			component.CPU += container.CPU
			component.Disk += float64(container.Disk)
			component.Memory += float64(container.Memory)
		}
	}

	var keys []string

	for key, component := range compUsage {
		keys = append(keys, key)
		component.Memory = math.Trunc(component.Memory/1024/1024*1e2) * 1e-2
		component.Disk = math.Trunc(component.Disk/1024/1024*1e2) * 1e-2
		component.CPU = Round(component.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ComponentUsage, len(keys))

	for i, k := range keys {
		sorted[i] = compUsage[k]
	}

	return sorted, nil
}

// AllAddonsUsageByCluster 获取指定集群下中间件资源分配情况列表
func (db *DbClient) AllAddonsUsageByCluster(ctx context.Context, cluster string) ([]*AddOnUsage, error) {

	if cluster == "" {
		return nil, errors.Errorf("invalid param: cluster is nil")
	}

	containers, err := db.AllAddonsByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	addons := make(map[string]*AddOnUsage)

	for _, container := range *containers {
		nameString := strings.Replace(container.DiceAddon, "_", "/", -1)
		nameString = "/" + nameString

		if add, ok := addons[nameString]; ok {
			add.Instance++
			add.CPU += container.CPU
			add.Disk += float64(container.Disk)
			add.Memory += float64(container.Memory)
			addons[nameString] = add
		} else {
			var addon AddOnUsage
			addons[nameString] = &addon
			addon.Name = nameString
			addon.Instance++
			addon.CPU += container.CPU
			addon.Disk += float64(container.Disk)
			addon.Memory += float64(container.Memory)
		}
	}

	var keys []string

	for key, addon := range addons {
		keys = append(keys, key)
		addon.Memory = math.Trunc(addon.Memory/1024/1024*1e2) * 1e-2
		addon.Disk = math.Trunc(addon.Disk/1024/1024*1e2) * 1e-2
		addon.CPU = Round(addon.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*AddOnUsage, len(keys))

	for i, k := range keys {
		sorted[i] = addons[k]
	}

	return sorted, nil
}

// AddonsUsage 获取指定集群下指定的中间件资源分配情况列表
func (db *DbClient) AddonsUsage(ctx context.Context, cluster string, addons []string) ([]*AddOnUsage, error) {

	if cluster == "" || len(addons) == 0 {
		return nil, errors.Errorf("invalid param: cluster = %s, addons = %v", cluster, addons)
	}

	containers, err := db.AllContainersByAddon(ctx, cluster, addons)
	if err != nil {
		return nil, err
	}

	addonsUsage := make(map[string]*AddOnUsage)

	for _, container := range *containers {
		nameString := container.DiceAddon

		if add, ok := addonsUsage[nameString]; ok {
			add.Instance++
			add.CPU += container.CPU
			add.Disk += float64(container.Disk)
			add.Memory += float64(container.Memory)
			addonsUsage[nameString] = add
		} else {
			var addon AddOnUsage
			addonsUsage[nameString] = &addon
			addon.Name = nameString
			addon.Instance++
			addon.CPU += container.CPU
			addon.Disk += float64(container.Disk)
			addon.Memory += float64(container.Memory)
		}
	}

	var keys []string

	for key, addon := range addonsUsage {
		keys = append(keys, key)
		addon.Memory = math.Trunc(addon.Memory/1024/1024*1e2) * 1e-2
		addon.Disk = math.Trunc(addon.Disk/1024/1024*1e2) * 1e-2
		addon.CPU = Round(addon.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*AddOnUsage, len(keys))

	for i, k := range keys {
		sorted[i] = addonsUsage[k]
	}

	return sorted, nil
}

// AllProjectUsageByCluster 获取指定集群下所有project资源分配情况列表
func (db *DbClient) AllProjectUsageByCluster(ctx context.Context, cluster string) ([]*ProjectUsage, error) {

	containers, err := db.AllProjectsContainersByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	projects := make(map[string]*ProjectUsage)

	for _, container := range *containers {
		proID := container.DiceProject

		if pro, ok := projects[proID]; ok {
			pro.Instance++
			pro.CPU += container.CPU
			pro.Disk += float64(container.Disk)
			pro.Memory += float64(container.Memory)
			projects[proID] = pro
		} else {
			var project ProjectUsage

			projects[proID] = &project
			project.ID = proID
			project.Name = container.DiceProjectName
			project.Instance++
			project.CPU += container.CPU
			project.Disk += float64(container.Disk)
			project.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, project := range projects {
		keys = append(keys, key)
		project.Memory = math.Trunc(project.Memory/1024/1024*1e2) * 1e-2
		project.Disk = math.Trunc(project.Disk/1024/1024*1e2) * 1e-2
		project.CPU = Round(project.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ProjectUsage, len(keys))

	for i, k := range keys {
		sorted[i] = projects[k]
	}

	return sorted, nil
}

// ProjectsUsage 获取指定集群下所有project资源分配情况列表
func (db *DbClient) ProjectsUsage(ctx context.Context, project []string) ([]*ProjectUsage, error) {

	if len(project) == 0 {
		return nil, errors.Errorf("missing project params")
	}

	containers, err := db.AllContainersByProject(ctx, project)
	if err != nil {
		return nil, err
	}

	projects := make(map[string]*ProjectUsage)

	for _, container := range *containers {
		proID := container.DiceProject

		if pro, ok := projects[proID]; ok {
			pro.Instance++
			pro.CPU += container.CPU
			pro.Disk += float64(container.Disk)
			pro.Memory += float64(container.Memory)
			projects[proID] = pro
		} else {
			var project ProjectUsage

			projects[proID] = &project
			project.ID = proID
			project.Name = container.DiceProjectName
			project.Instance++
			project.CPU += container.CPU
			project.Disk += float64(container.Disk)
			project.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, project := range projects {
		keys = append(keys, key)
		project.Memory = math.Trunc(project.Memory/1024/1024*1e2) * 1e-2
		project.Disk = math.Trunc(project.Disk/1024/1024*1e2) * 1e-2
		project.CPU = Round(project.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ProjectUsage, len(keys))

	for i, k := range keys {
		sorted[i] = projects[k]
	}

	return sorted, nil
}

// AllApplicationUsageByProject 获取指定集群下所有application资源分配情况列表
func (db *DbClient) AllApplicationUsageByProject(ctx context.Context, project string) ([]*ApplicationUsage, error) {

	if len(project) == 0 {
		return nil, errors.Errorf("missing project params")
	}

	containers, err := db.AllContainersByProject(ctx, []string{project})
	if err != nil {
		return nil, err
	}

	applications := make(map[string]*ApplicationUsage)

	for _, container := range *containers {
		appID := container.DiceApplication

		if app, ok := applications[appID]; ok {
			app.Instance++
			app.CPU += container.CPU
			app.Disk += float64(container.Disk)
			app.Memory += float64(container.Memory)
			applications[appID] = app
		} else {
			var application ApplicationUsage
			applications[appID] = &application
			application.ID = appID
			application.Name = container.DiceApplicationName
			application.Instance++
			application.CPU += container.CPU
			application.Disk += float64(container.Disk)
			application.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, application := range applications {
		keys = append(keys, key)
		application.Memory = math.Trunc(application.Memory/1024/1024*1e2) * 1e-2
		application.Disk = math.Trunc(application.Disk/1024/1024*1e2) * 1e-2
		application.CPU = Round(application.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ApplicationUsage, len(keys))

	for i, k := range keys {
		sorted[i] = applications[k]
	}

	return sorted, nil
}

// ApplicationsUsage 获取指定集群下所有application资源分配情况列表
func (db *DbClient) ApplicationsUsage(ctx context.Context, application []string) ([]*ApplicationUsage, error) {

	if len(application) == 0 {
		return nil, errors.Errorf("missing application params")
	}

	containers, err := db.AllContainersByApplication(ctx, application)
	if err != nil {
		return nil, err
	}

	applications := make(map[string]*ApplicationUsage)

	for _, container := range *containers {
		appID := container.DiceApplication

		if app, ok := applications[appID]; ok {
			app.Instance++
			app.CPU += container.CPU
			app.Disk += float64(container.Disk)
			app.Memory += float64(container.Memory)
			applications[appID] = app
		} else {
			var application ApplicationUsage
			applications[appID] = &application
			application.ID = appID
			application.Name = container.DiceApplicationName
			application.Instance++
			application.CPU += container.CPU
			application.Disk += float64(container.Disk)
			application.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, application := range applications {
		keys = append(keys, key)
		application.Memory = math.Trunc(application.Memory/1024/1024*1e2) * 1e-2
		application.Disk = math.Trunc(application.Disk/1024/1024*1e2) * 1e-2
		application.CPU = Round(application.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ApplicationUsage, len(keys))

	for i, k := range keys {
		sorted[i] = applications[k]
	}

	return sorted, nil
}

// AllRuntimeUsageByApplication 获取指定application下所有runtime资源分配情况列表
func (db *DbClient) AllRuntimeUsageByApplication(ctx context.Context, application string) ([]*RuntimeUsage, error) {

	if len(application) == 0 {
		return nil, errors.Errorf("missing application params")
	}

	containers, err := db.AllContainersByApplication(ctx, []string{application})
	if err != nil {
		return nil, err
	}

	runtimes := make(map[string]*RuntimeUsage)

	for _, container := range *containers {
		runID := container.DiceRuntime

		if run, ok := runtimes[runID]; ok {
			run.Instance++
			run.CPU += container.CPU
			run.Disk += float64(container.Disk)
			run.Memory += float64(container.Memory)
			runtimes[runID] = run
		} else {
			var runtime RuntimeUsage
			runtimes[runID] = &runtime
			runtime.ID = runID
			runtime.Name = container.DiceRuntimeName
			runtime.Instance++
			runtime.CPU += container.CPU
			runtime.Disk += float64(container.Disk)
			runtime.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, runtime := range runtimes {
		keys = append(keys, key)
		runtime.Memory = math.Trunc(runtime.Memory/1024/1024*1e2) * 1e-2
		runtime.Disk = math.Trunc(runtime.Disk/1024/1024*1e2) * 1e-2
		runtime.CPU = Round(runtime.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*RuntimeUsage, len(keys))

	for i, k := range keys {
		sorted[i] = runtimes[k]
	}

	return sorted, nil
}

// RuntimesUsage 获取指定runtime资源分配情况列表
func (db *DbClient) RuntimesUsage(ctx context.Context, runtime []string) ([]*RuntimeUsage, error) {

	if len(runtime) == 0 {
		return nil, errors.Errorf("missing runtime params")
	}

	containers, err := db.AllContainersByRuntime(ctx, runtime)
	if err != nil {
		return nil, err
	}

	runUsage := make(map[string]*RuntimeUsage)

	for _, container := range *containers {
		runID := container.DiceRuntime

		if run, ok := runUsage[runID]; ok {
			run.Instance++
			run.CPU += container.CPU
			run.Disk += float64(container.Disk)
			run.Memory += float64(container.Memory)
			runUsage[runID] = run
		} else {
			var runtime RuntimeUsage
			runUsage[runID] = &runtime
			runtime.ID = runID
			runtime.Name = container.DiceRuntimeName
			runtime.Instance++
			runtime.CPU += container.CPU
			runtime.Disk += float64(container.Disk)
			runtime.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, runtime := range runUsage {
		keys = append(keys, key)
		runtime.Memory = math.Trunc(runtime.Memory/1024/1024*1e2) * 1e-2
		runtime.Disk = math.Trunc(runtime.Disk/1024/1024*1e2) * 1e-2
		runtime.CPU = Round(runtime.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*RuntimeUsage, len(keys))

	for i, k := range keys {
		sorted[i] = runUsage[k]
	}

	return sorted, nil
}

// AllServiceUsageByRuntime 获取指定runtime下所有service资源分配情况列表
func (db *DbClient) AllServiceUsageByRuntime(ctx context.Context, runtime string) ([]*ServiceUsage, error) {

	if len(runtime) == 0 {
		return nil, errors.Errorf("missing runtime params")
	}

	containers, err := db.AllContainersByRuntime(ctx, []string{runtime})
	if err != nil {
		return nil, err
	}

	services := make(map[string]*ServiceUsage)

	for _, container := range *containers {
		serviceName := container.DiceService

		if ser, ok := services[serviceName]; ok {
			ser.Instance++
			ser.CPU += container.CPU
			ser.Disk += float64(container.Disk)
			ser.Memory += float64(container.Memory)
			services[serviceName] = ser
		} else {
			var service ServiceUsage
			services[serviceName] = &service
			service.Name = serviceName
			service.Instance++
			service.CPU += container.CPU
			service.Disk += float64(container.Disk)
			service.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, service := range services {
		keys = append(keys, key)
		service.Memory = math.Trunc(service.Memory/1024/1024*1e2) * 1e-2
		service.Disk = math.Trunc(service.Disk/1024/1024*1e2) * 1e-2
		service.CPU = Round(service.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ServiceUsage, len(keys))

	for i, k := range keys {
		sorted[i] = services[k]
	}

	return sorted, nil
}

// ServicesUsage 获取指定service资源分配情况列表
func (db *DbClient) ServicesUsage(ctx context.Context, runtime string, service []string) ([]*ServiceUsage, error) {

	if len(runtime) == 0 || len(service) == 0 {
		return nil, errors.Errorf("invalid params: runtime = %s, service = %v", runtime, service)
	}

	containers, err := db.AllContainersByService(ctx, runtime, service)
	if err != nil {
		return nil, err
	}

	services := make(map[string]*ServiceUsage)

	for _, container := range *containers {
		serviceName := container.DiceService

		if ser, ok := services[serviceName]; ok {
			ser.Instance++
			ser.CPU += container.CPU
			ser.Disk += float64(container.Disk)
			ser.Memory += float64(container.Memory)
			services[serviceName] = ser
		} else {
			var service ServiceUsage
			services[serviceName] = &service
			service.Name = serviceName
			service.Instance++
			service.CPU += container.CPU
			service.Disk += float64(container.Disk)
			service.Memory += float64(container.Memory)
		}
	}
	var keys []string

	for key, service := range services {
		keys = append(keys, key)
		service.Memory = math.Trunc(service.Memory/1024/1024*1e2) * 1e-2
		service.Disk = math.Trunc(service.Disk/1024/1024*1e2) * 1e-2
		service.CPU = Round(service.CPU, 2)
	}
	sort.Strings(keys)

	sorted := make([]*ServiceUsage, len(keys))

	for i, k := range keys {
		sorted[i] = services[k]
	}

	return sorted, nil
}

func (db *DbClient) ContainerUsage(ctx context.Context, cluster string, id string) (*ContainerUsage, error) {

	if cluster == "" || id == "" {
		return nil, errors.Errorf("invalid params: cluster = %s, id = %s", cluster, id)
	}

	c, err := db.QueryContainer(ctx, cluster, id)
	if err != nil {
		return nil, err
	}

	cUsage := &ContainerUsage{
		ID:     c.ID,
		Memory: float64(c.Memory),
		CPU:    c.CPU,
		Disk:   float64(c.Disk),
	}

	return cUsage, nil
}
