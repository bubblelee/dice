package api

import (
	"context"
	"sort"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// TODO: add cancel
// 构建 & 部署信息
type PipelineSummary struct {
	Success     int64 `json:"success"`
	Failed      int64 `json:"failed"`
	Running     int64 `json:"running"`
	Unscheduled int64 `json:"unscheduled"`
	// 当前成功率
	SuccRate float32 `json:"succ_rate"`
	// 上个周期成功率
	HistorySuccRate float32    `json:"history_succ_rate"`
	Details         DataPoints `json:"details"`
}

// TaskSummary 实例信息
type TaskSummary struct {
	Running  int64      `json:"running"`
	Abnormal int64      `json:"abnormal"`
	Details  DataPoints `json:"datails"`
}

// DataPoints 时间轴数据
type DataPoints struct {
	Time    []time.Duration `json:"time"`
	Results []Data          `json:"results"`
}

// Data 对应的类型及其数据
type Data struct {
	Type ActiviteDateType `json:"type"`
	Data []int64          `json:"data"`
}

// ActiviteDateType activite 表数据类型
type ActiviteDateType string

const (
	// 部署成功次数统计
	ACTIVITE_DATE_TYPE_DEPLOYSUCC ActiviteDateType = "deploySuccNum"
	// 部署失败次数统计
	ACTIVITE_DATE_TYPE_DEPLOYFAILED ActiviteDateType = "deployFailedNum"
	// 构建成功次数统计
	ACTIVITE_DATE_TYPE_PACKSUCC ActiviteDateType = "packSuccNum"
	// 构建失败次数统计
	ACTIVITE_DATE_TYPE_PACKFAILED ActiviteDateType = "packFailedNum"
	// 构建时间统计
	ACTIVITE_DATE_TYPE_PACKTIME ActiviteDateType = "packTime"
	// task 数量统计
	ACTIVITE_DATE_TYPE_TASKNUM ActiviteDateType = "taskNum"
)

// ActivitePeriod 时间周期
type ActivitePeriod string

const (
	ACTIVITE_PERIOD_YEAR    ActivitePeriod = "year"
	ACTIVITE_PERIOD_QUARTER ActivitePeriod = "quarter"
	ACTIVITE_PERIOD_MOUTH   ActivitePeriod = "year"
	// default
	ACTIVITE_PERIOD_WEEKLY ActivitePeriod = "weekly"
	ACTIVITE_PERIOD_DAY    ActivitePeriod = "day"
)

// DynamicMsg 构建 & 部署动态信息
type DynamicMsg struct {
	UpdatedAt       time.Time     `json:"-"`
	Timestamp       time.Duration `json:"timestamp"`
	OrgID           int           `json:"org_id"`
	ProjectID       int64         `json:"project_id"`
	ApplicationId   int64         `json:"application_id"`
	RuntimeId       int64         `json:"runtime_id"`
	UserId          int64         `json:"user_id"`
	BuildId         int64         `json:"build_id"`
	ApplicationName string        `json:"application_name"`
	GitBranch       string        `json:"git_branch"`
	Action          DynamicAction `json:"action"`
	Status          DynamicStatus `json:"status"`
}

type DynamicMsgSlice []DynamicMsg

func (c DynamicMsgSlice) Len() int {
	return len(c)
}

func (c DynamicMsgSlice) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c DynamicMsgSlice) Less(i, j int) bool {
	return c[i].Timestamp < c[j].Timestamp
}

type DynamicAction string

const (
	// 部署
	DYNAMIC_ACTION_DEPLOY DynamicAction = "deploy"
	// 构建
	DYNAMIC_ACTION_PIPELINE DynamicAction = "build"
)

type DynamicStatus string

const (
	DYNAMIC_STATUS_RUNNING  DynamicStatus = "running"
	DYNAMIC_STATUS_SUCCESS  DynamicStatus = "success"
	DYNAMIC_STATUS_FAILED   DynamicStatus = "failed"
	DYNAMIC_STATUS_CANCELED DynamicStatus = "canceled"
)

var deployStatusMap = map[DeploymentStatus]DynamicStatus{
	DEPLOYMENT_STATUS_INIT:      DYNAMIC_STATUS_RUNNING,
	DEPLOYMENT_STATUS_WAITING:   DYNAMIC_STATUS_RUNNING,
	DEPLOYMENT_STATUS_DEPLOYING: DYNAMIC_STATUS_RUNNING,
	DEPLOYMENT_STATUS_OK:        DYNAMIC_STATUS_SUCCESS,
	DEPLOYMENT_STATUS_FAILED:    DYNAMIC_STATUS_FAILED,
	DEPLOYMENT_STATUS_CANCELED:  DYNAMIC_STATUS_CANCELED,
}

var buildStatusMap = map[BuildStatus]DynamicStatus{
	BUILD_STATUS_INIT:          DYNAMIC_STATUS_RUNNING,
	BUILD_STATUS_PACKING:       DYNAMIC_STATUS_RUNNING,
	BUILD_STATUS_UNSCHEDULABLE: DYNAMIC_STATUS_RUNNING,
	BUILD_STATUS_OK:            DYNAMIC_STATUS_SUCCESS,
	BUILD_STATUS_FAILED:        DYNAMIC_STATUS_FAILED,
	BUILD_STATUS_CANCELED:      DYNAMIC_STATUS_CANCELED,
}

type buildTime struct {
	totalTime int64
	num       int
}

// ResourceStatistics 资源统计信息
type ResourceStatistics struct {
	Name        string                 `json:"name"`
	Type        resourceStatisticsType `json:"type"`
	TotalCPU    float64                `json:"totalCPU"`
	TotalMemory int64                  `json:"totalMemory"`
	TotalDisk   int64                  `json:"totalDisk"`
	CPUPercent  float32                `json:"cpuPercent"`
	MemPercent  float32                `json:"memPercent"`
	Details     resourceDetailSlice    `json:"details"`
}

type resourceStatisticsType string

const (
	resourceStatisticsProject     resourceStatisticsType = "project"
	resourceStatisticsApplication resourceStatisticsType = "application"
)

// ResourceDetail 资源详情
type ResourceDetail struct {
	// project or runtime name
	Name          string `json:"name"`
	ProjectID     string `json:"projectID"`
	ApplicationID string `json:"applicationID"`
	RuntimeID     string `json:"runtimeID,omitempty"`
	// key包括: DEV, TEST, STAGING, PROD, TOTAL
	ResourceValues map[string]ResourceValue `json:"values"`
}

// ResourceValue 资源信息: cpu、mem
type ResourceValue struct {
	CPU    float64 `json:"cpu"`
	Memory int64   `json:"memory"`
	Disk   int64   `json:"disk`
}

// 自定义 interface{},用于实现 []int64 的排序
type resourceDetailSlice []ResourceDetail

func (c resourceDetailSlice) Len() int {
	return len(c)
}

func (c resourceDetailSlice) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c resourceDetailSlice) Less(i, j int) bool {
	return c[i].ResourceValues["TOTAL"].Memory < c[j].ResourceValues["TOTAL"].Memory
}

// DeploymentSummaryByPlatform 平台级别部署信息统计
func (db *DbClient) DeploymentSummaryByPlatform(ctx context.Context) (*PipelineSummary, error) {
	var deployments []PsV2Deployments
	var err error

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	if err = db.Not("status", DEPLOYMENT_STATUS_CANCELED).Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&deployments).Error; err != nil {
		return nil, err
	}

	summary, err := generateDeployResult(&deployments)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekDeploySuccRateByPlatform()
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// DeploymentSummaryByOrg 企业级别部署信息统计
func (db *DbClient) DeploymentSummaryByOrg(ctx context.Context, orgID string) (*PipelineSummary, error) {
	var deployments []PsV2Deployments
	var err error

	if len(orgID) == 0 {
		return nil, errors.Errorf("invalid param: orgID is null")
	}

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	if err = db.Not("status", DEPLOYMENT_STATUS_CANCELED).Where("created_at BETWEEN ? AND ?", lastWeek, today).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE org_id = ?))", orgID).
		Find(&deployments).Error; err != nil {
		return nil, err
	}

	summary, err := generateDeployResult(&deployments)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekDeploySuccRateByOrg(orgID)
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// DeploymentSummaryByProject 项目级别部署信息统计
func (db *DbClient) DeploymentSummaryByProject(ctx context.Context, projectId string) (*PipelineSummary, error) {
	var deployments []PsV2Deployments
	var err error

	if len(projectId) == 0 {
		return nil, errors.Errorf("invalid param: projectId is null")
	}

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	if err = db.Not("status", DEPLOYMENT_STATUS_CANCELED).
		Where("created_at BETWEEN ? AND ?", lastWeek, today).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE project_id = ?))", projectId).
		Find(&deployments).Error; err != nil {
		return nil, err
	}

	summary, err := generateDeployResult(&deployments)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekDeploySuccRateByProject(projectId)
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// BuildSummaryByPlatform 平台级别的构建信息统计
func (db *DbClient) BuildSummaryByPlatform(ctx context.Context) (*PipelineSummary, error) {
	var (
		activities []PsActivities
		builds     []PsV2Builds
		err        error
	)

	actions := []ActiviteAction{B_START, B_FAILED, B_END}

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	if err = db.Where("action in (?) AND created_at BETWEEN ? AND ?", actions, lastWeek, today).Find(&activities).Error; err != nil {
		return nil, err
	}

	if err = db.Not("status", BUILD_STATUS_CANCELED).Where("created_at BETWEEN ? AND ?", lastWeek, today).Find(&builds).Error; err != nil {
		return nil, err
	}

	summary, err := generateBuildResult(&activities, &builds)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekBuildSuccRateByPlatform()
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// BuildSummaryByOrg 企业级别的构建信息统计
func (db *DbClient) BuildSummaryByOrg(ctx context.Context, orgID string) (*PipelineSummary, error) {
	var (
		activities []PsActivities
		builds     []PsV2Builds
		err        error
	)

	if len(orgID) == 0 {
		return nil, errors.Errorf("invalid param: orgID is null")
	}

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	actions := []ActiviteAction{B_START, B_FAILED, B_END}
	if err = db.Where("action in (?) AND org_id = ? AND created_at BETWEEN ? AND ?", actions, orgID, lastWeek, today).
		Find(&activities).Error; err != nil {
		return nil, err
	}

	if err = db.Not("status", BUILD_STATUS_CANCELED).
		Where("created_at BETWEEN ? AND ?", lastWeek, today).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE org_id = ?))", orgID).
		Find(&builds).Error; err != nil {
		return nil, err
	}

	summary, err := generateBuildResult(&activities, &builds)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekBuildSuccRateByOrg(orgID)
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// BuildSummaryByProject 项目级别的构建信息统计
func (db *DbClient) BuildSummaryByProject(ctx context.Context, projectId string) (*PipelineSummary, error) {
	var (
		activities []PsActivities
		builds     []PsV2Builds
		err        error
	)

	if len(projectId) == 0 {
		return nil, errors.Errorf("invalid param: projectId is null")
	}

	cstZone := time.FixedZone("CST", 8*3600) // 东八区
	now := time.Now()
	today := now.In(cstZone).Format("2006-01-02 15:04:05")
	lastWeek := now.AddDate(0, 0, -6).In(cstZone).Format("2006-01-02")

	actions := []ActiviteAction{B_START, B_FAILED, B_END}
	if err = db.Where("action in (?) AND project_id = ? AND created_at BETWEEN ? AND ?", actions, projectId, lastWeek, today).
		Find(&activities).Error; err != nil {
		return nil, err
	}

	if err = db.Not("status", BUILD_STATUS_CANCELED).
		Where("created_at BETWEEN ? AND ?", lastWeek, today).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE project_id = ?))", projectId).
		Find(&builds).Error; err != nil {
		return nil, err
	}

	summary, err := generateBuildResult(&activities, &builds)
	if err != nil {
		return nil, err
	}

	summary.HistorySuccRate, err = db.lastWeekBuildSuccRateByProject(projectId)
	if err != nil {
		return nil, err
	}

	return summary, nil
}

// TaskSummaryByPlatform 平台级别的实例信息统计
func (db *DbClient) TaskSummaryByPlatform(ctx context.Context) (*TaskSummary, error) {
	var containers []CmContainer
	var err error

	if err = db.Where("dice_project is not null AND dice_project <> ''").Find(&containers).Error; err != nil {
		return nil, err
	}

	return generateTaskResult(&containers)
}

// TaskSummaryByOrg 企业级别的实例信息统计
func (db *DbClient) TaskSummaryByOrg(ctx context.Context, orgID string) (*TaskSummary, error) {
	var containers []CmContainer
	var err error

	if len(orgID) == 0 {
		return nil, errors.Errorf("invalid params: orgID is null")
	}

	if err = db.Where("dice_org = ?", orgID).Find(&containers).Error; err != nil {
		return nil, err
	}

	return generateTaskResult(&containers)
}

// TaskSummaryByProject 项目级别的实例信息统计
func (db *DbClient) TaskSummaryByProject(ctx context.Context, projectId string) (*TaskSummary, error) {
	var containers []CmContainer
	var err error

	if len(projectId) == 0 {
		return nil, errors.Errorf("invalid params: projectId is null")
	}

	if err = db.Where("dice_project = ?", projectId).Find(&containers).Error; err != nil {
		return nil, err
	}

	return generateTaskResult(&containers)
}

// TODO: deploy 与 build 独立，不应该判断 build_id
// DeployDynamicByPlatform 平台级别的部署动态统计
func (db *DbClient) DeployDynamicByPlatform(ctx context.Context) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	sql := `select a.updated_at, a.status, a.build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, d.git_branch
		from (
  		select runtime_id, operator, build_id, status, updated_at
  		from ps_v2_deployments
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id
		join ps_v2_builds as d on d.id = a.build_id
	`

	if err = db.Raw(sql).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_DEPLOY
		dMsgs[i].Status = deployStatusMap[DeploymentStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("platform deploy dynamic msgs: %v", dMsgs)

	return &dMsgs, nil
}

// DeployDynamicByOrg 企业级别的部署动态统计
func (db *DbClient) DeployDynamicByOrg(ctx context.Context, orgID string) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	if len(orgID) == 0 {
		return nil, errors.Errorf("invalid params: orgID is null")
	}

	sql := `select a.updated_at, a.status, a.build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, d.git_branch
		from (
  		select runtime_id, operator, build_id, status, updated_at
  		from ps_v2_deployments
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id and org_id = ?
		join ps_v2_builds as d on d.id = a.build_id
	`

	if err = db.Raw(sql, orgID).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_DEPLOY
		dMsgs[i].Status = deployStatusMap[DeploymentStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("org(%s) deploy dynamic msgs: %v", orgID, dMsgs)

	return &dMsgs, nil
}

// DeployDynamicByProject 项目级别的部署动态统计
func (db *DbClient) DeployDynamicByProject(ctx context.Context, projectId string) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	if len(projectId) == 0 {
		return nil, errors.Errorf("invalid params: projectId is null")
	}

	sql := `select a.updated_at, a.status, a.build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, d.git_branch
		from (
  		select runtime_id, operator, build_id, status, updated_at
  		from ps_v2_deployments
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id and c.project_id = ?
		join ps_v2_builds as d on d.id = a.build_id
	`

	if err = db.Raw(sql, projectId).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_DEPLOY
		dMsgs[i].Status = deployStatusMap[DeploymentStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("project(%s) deploy dynamic msgs: %v", projectId, dMsgs)

	return &dMsgs, nil
}

// TODO: 构建动态先只从 ps_v2_builds 表中获取，等 CI 更新之后再换对应的表
// BuildDynamicByPlatform 平台级别的构建动态统计
func (db *DbClient) BuildDynamicByPlatform(ctx context.Context) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	sql := `select a.updated_at, a.status, a.id as build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, a.git_branch
		from (
  		select runtime_id, operator, id, status, git_branch, updated_at
  		from ps_v2_builds
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id
	`

	if err = db.Raw(sql).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_PIPELINE
		dMsgs[i].Status = buildStatusMap[BuildStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("platform build dynamic msgs: %v", dMsgs)

	return &dMsgs, nil
}

// TODO: 构建动态先只从 ps_v2_builds 表中获取，等 CI 更新之后再换对应的表
// BuildDynamicByOrg 企业级别的构建动态统计
func (db *DbClient) BuildDynamicByOrg(ctx context.Context, orgID string) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	if len(orgID) == 0 {
		return nil, errors.Errorf("invalid params: orgID is null")
	}

	sql := `select a.updated_at, a.status, a.id as build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, a.git_branch
		from (
  		select runtime_id, operator, id, status, git_branch, updated_at
  		from ps_v2_builds
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id and org_id = ?
	`

	if err = db.Raw(sql, orgID).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_PIPELINE
		dMsgs[i].Status = buildStatusMap[BuildStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("org(%s) build dynamic msgs: %v", orgID, dMsgs)

	return &dMsgs, nil
}

// TODO: 考虑平台是否需要统计 unknown 的资源
// ResourceStatisticsByPlatform 平台级别的项目资源使用统计
func (db *DbClient) ResourceStatisticsByPlatform(ctx context.Context) (*[]ResourceStatistics, error) {
	var containers []CmContainer
	var err error

	sql := `select a.*, b.env
		from (
  		select *
  		from cm_containers
  		where dice_runtime is not null AND dice_runtime <> ''
		) as a
		join ps_v2_project_runtimes as b on b.id = a.dice_runtime
	`

	if err = db.Raw(sql).Scan(&containers).Error; err != nil {
		return nil, err
	}

	return generateProjectResourceStatistics(&containers)
}

// ResourceStatisticsByOrg 企业级别的项目资源使用统计
func (db *DbClient) ResourceStatisticsByOrg(ctx context.Context, orgID string) (*[]ResourceStatistics, error) {
	var containers []CmContainer
	var err error

	sql := `select a.*, b.env
		from (
  		select *
  		from cm_containers
  		where dice_org = ?
		) as a
		join ps_v2_project_runtimes as b on b.id = a.dice_runtime
	`

	if err = db.Raw(sql, orgID).Scan(&containers).Error; err != nil {
		return nil, err
	}

	return generateProjectResourceStatistics(&containers)
}

// ResourceStatisticsByProject 项目级别的应用资源使用统计
func (db *DbClient) ResourceStatisticsByProject(ctx context.Context, projectID string) (*[]ResourceStatistics, error) {
	var containers []CmContainer
	var err error

	sql := `select a.*, b.env
		from (
  		select *
  		from cm_containers
  		where dice_project = ?
		) as a
		join ps_v2_project_runtimes as b on b.id = a.dice_runtime
	`

	if err = db.Raw(sql, projectID).Scan(&containers).Error; err != nil {
		return nil, err
	}

	return generateApplicationResourceStatistics(&containers)
}

func generateProjectResourceStatistics(containers *[]CmContainer) (*[]ResourceStatistics, error) {
	var res []ResourceStatistics

	if containers == nil {
		return nil, errors.Errorf("invalid params: containers is null")
	}

	type appResource struct {
		DiceProject string
		ResourceDetail
	}

	projects := make(map[string]*ResourceStatistics)
	apps := make(map[string]*appResource)

	for _, c := range *containers {
		// project
		if proj, ok := projects[c.DiceProject]; ok {
			proj.TotalCPU += c.CPU
			proj.TotalMemory += c.Memory
			proj.TotalDisk += c.Disk
		} else {
			var proj ResourceStatistics
			projects[c.DiceProject] = &proj
			proj.Type = resourceStatisticsProject
			proj.Name = c.DiceProjectName
			proj.TotalCPU += c.CPU
			proj.TotalMemory += c.Memory
			proj.TotalDisk += c.Disk
		}

		// application
		if app, ok := apps[c.DiceApplication]; ok {
			value := app.ResourceValues[c.Env]
			value.CPU += c.CPU
			value.Memory += c.Memory
			value.Disk += c.Disk
			app.ResourceValues[c.Env] = value
		} else {
			var app appResource
			app.ResourceValues = make(map[string]ResourceValue)
			app.DiceProject = c.DiceProject
			app.Name = c.DiceApplicationName
			app.ProjectID = c.DiceProject
			app.ApplicationID = c.DiceApplication

			value := app.ResourceValues[c.Env]
			value.CPU += c.CPU
			value.Memory += c.Memory
			value.Disk += c.Disk
			app.ResourceValues[c.Env] = value
			apps[c.DiceApplication] = &app
		}

		app := apps[c.DiceApplication]
		totalValue := app.ResourceValues["TOTAL"]
		totalValue.CPU += c.CPU
		totalValue.Memory += c.Memory
		totalValue.Disk += c.Disk
		app.ResourceValues["TOTAL"] = totalValue
	}

	for _, v := range apps {
		if proj, ok := projects[v.DiceProject]; ok {
			proj.Details = append(proj.Details, v.ResourceDetail)
		}
	}

	for _, v := range projects {
		_ = sort.Reverse(v.Details)
		res = append(res, *v)
	}

	return &res, nil
}

func generateApplicationResourceStatistics(containers *[]CmContainer) (*[]ResourceStatistics, error) {
	var res []ResourceStatistics

	if containers == nil {
		return nil, errors.Errorf("invalid params: containers is null")
	}

	type runResource struct {
		DiceApplication string
		ResourceDetail
	}

	apps := make(map[string]*ResourceStatistics)
	runs := make(map[string]*runResource)

	for _, c := range *containers {
		// application
		if app, ok := apps[c.DiceApplication]; ok {
			app.TotalCPU += c.CPU
			app.TotalMemory += c.Memory
			app.TotalDisk += c.Disk
		} else {
			var app ResourceStatistics
			apps[c.DiceApplication] = &app
			app.Type = resourceStatisticsApplication
			app.Name = c.DiceApplicationName
			app.TotalCPU += c.CPU
			app.TotalMemory += c.Memory
			app.TotalDisk += c.Disk
		}

		// runtime
		if run, ok := runs[c.DiceRuntime]; ok {
			value := run.ResourceValues[c.Env]
			value.CPU += c.CPU
			value.Memory += c.Memory
			value.Disk += c.Disk
			run.ResourceValues[c.Env] = value
		} else {
			var run runResource
			run.ResourceValues = make(map[string]ResourceValue)
			run.DiceApplication = c.DiceApplication
			run.Name = c.DiceRuntimeName
			run.ProjectID = c.DiceProject
			run.ApplicationID = c.DiceApplication
			run.RuntimeID = c.DiceRuntime

			value := run.ResourceValues[c.Env]
			value.CPU += c.CPU
			value.Memory += c.Memory
			value.Disk += c.Disk
			run.ResourceValues[c.Env] = value
			runs[c.DiceRuntime] = &run
		}
		run := runs[c.DiceRuntime]
		totalValue := run.ResourceValues["TOTAL"]
		totalValue.CPU += c.CPU
		totalValue.Memory += c.Memory
		totalValue.Disk += c.Disk
		run.ResourceValues["TOTAL"] = totalValue
	}

	for _, v := range runs {
		if app, ok := apps[v.DiceApplication]; ok {
			app.Details = append(app.Details, v.ResourceDetail)
		}
	}

	for _, v := range apps {
		sort.Sort(v.Details)
		res = append(res, *v)
	}

	return &res, nil
}

// TODO: 构建动态先只从 ps_v2_builds 表中获取，等 CI 更新之后再换对应的表
// BuildDynamicByProject 项目级别的构建动态统计
func (db *DbClient) BuildDynamicByProject(ctx context.Context, projectId string) (*DynamicMsgSlice, error) {
	var dMsgs DynamicMsgSlice
	var err error

	if len(projectId) == 0 {
		return nil, errors.Errorf("invalid params: projectId is null")
	}

	sql := `select a.updated_at, a.status, a.id as build_id, a.operator as user_id, a.runtime_id, b.project_id as app_id, c.project_id as project_id, c.org_id as org_id, c.name as application_name, a.git_branch
		from (
  		select runtime_id, operator, id, status, git_branch, updated_at
  		from ps_v2_builds
  		order by id desc
  		limit 10
		) as a
		join ps_v2_project_runtimes as b on b.id = a.runtime_id
		join ps_projects as c on c.id = b.project_id and c.project_id = ?
	`

	if err = db.Raw(sql, projectId).Scan(&dMsgs).Error; err != nil {
		return nil, err
	}

	for i, msg := range dMsgs {
		dMsgs[i].Timestamp = time.Duration(dMsgs[i].UpdatedAt.Unix())
		dMsgs[i].Action = DYNAMIC_ACTION_PIPELINE
		dMsgs[i].Status = buildStatusMap[BuildStatus(msg.Status)]
	}

	sort.Sort(dMsgs)

	logrus.Debugf("project(%s) build dynamic msgs: %v", projectId, dMsgs)

	return &dMsgs, nil
}

// TODO: support period: year / month / weekly / day
// default: weekly
func generateDeployResult(deps *[]PsV2Deployments) (*PipelineSummary, error) {
	var summary PipelineSummary

	if deps == nil {
		return nil, errors.Errorf("invalid params: deployments is nil")
	}

	succMap, failedMap, _ := initWeeklyMap()

	for _, dep := range *deps {
		t := dep.CreatedAt
		dayTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC).Unix()

		switch dep.Status {
		case DEPLOYMENT_STATUS_OK:
			summary.Success++
			if _, ok := succMap[dayTime]; ok {
				succMap[dayTime]++
			}
		case DEPLOYMENT_STATUS_FAILED:
			summary.Failed++
			if _, ok := failedMap[dayTime]; ok {
				failedMap[dayTime]++
			}
		case DEPLOYMENT_STATUS_DEPLOYING:
			summary.Running++
		case DEPLOYMENT_STATUS_WAITING:
			summary.Running++
			summary.Unscheduled++
		// ignore init
		case DEPLOYMENT_STATUS_INIT:
		default:
			return nil, errors.Errorf("invalid deployment status: %s", dep.Status)
		}
	}

	total := float32(summary.Success + summary.Failed)
	if total > 0 {
		summary.SuccRate = float32(summary.Success) / total
	}

	succData := Data{Type: ACTIVITE_DATE_TYPE_DEPLOYSUCC}
	failedData := Data{Type: ACTIVITE_DATE_TYPE_DEPLOYFAILED}

	// 对 succMap key 进行排序
	keys := SortInt64Map(succMap)

	for _, k := range keys {
		summary.Details.Time = append(summary.Details.Time, time.Duration(k))
		succData.Data = append(succData.Data, succMap[k])
		failedData.Data = append(failedData.Data, failedMap[k])
	}

	summary.Details.Results = append(summary.Details.Results, succData)
	summary.Details.Results = append(summary.Details.Results, failedData)

	return &summary, nil
}

func generateBuildResult(activities *[]PsActivities, builds *[]PsV2Builds) (*PipelineSummary, error) {
	var (
		summary  PipelineSummary
		startNum int64
	)

	if activities == nil || builds == nil {
		return nil, errors.Errorf("invalid params: activities or builds is nil")
	}

	succMap, failedMap, buildTimes := initWeeklyMap()

	// TODO: 计算 pipeline 打包时间（start - end）
	for _, activite := range *activities {
		t := activite.CreatedAt
		dayTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC).Unix()

		switch activite.Action {
		case B_START:
			startNum++
		case B_FAILED:
			summary.Failed++
			if _, ok := failedMap[dayTime]; ok {
				failedMap[dayTime]++
			}
		case B_END:
			summary.Success++
			if _, ok := succMap[dayTime]; ok {
				succMap[dayTime]++
			}
		default:
			return nil, errors.Errorf("invalid activite action: %s", activite.Action)
		}
	}

	// activities表running的计算方式
	summary.Running = startNum - summary.Success - summary.Failed

	for _, build := range *builds {
		t := build.CreatedAt
		dayTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC).Unix()

		switch build.Status {
		case BUILD_STATUS_INIT, BUILD_STATUS_PACKING:
			summary.Running++
		case BUILD_STATUS_UNSCHEDULABLE:
			summary.Running++
			summary.Unscheduled++
		case BUILD_STATUS_FAILED:
			summary.Failed++
			if _, ok := failedMap[dayTime]; ok {
				failedMap[dayTime]++
			}
		case BUILD_STATUS_OK:
			summary.Success++
			if _, ok := succMap[dayTime]; ok {
				succMap[dayTime]++
			}

			buildTime := int64(build.UpdatedAt.Sub(build.CreatedAt).Seconds())
			// 过滤打包时间小于 10s 的任务，比如打包去重的任务时间 = 0
			if _, ok := buildTimes[dayTime]; ok && (buildTime > 10) {
				buildTimes[dayTime].totalTime += buildTime
				buildTimes[dayTime].num++
			}
		default:
			return nil, errors.Errorf("invalid build status: %s", build.Status)
		}
	}

	total := float32(summary.Success + summary.Failed)
	if total > 0 {
		summary.SuccRate = float32(summary.Success) / total
	}

	succData := Data{Type: ACTIVITE_DATE_TYPE_PACKSUCC}
	failedData := Data{Type: ACTIVITE_DATE_TYPE_PACKFAILED}
	timeData := Data{Type: ACTIVITE_DATE_TYPE_PACKTIME}

	keys := SortInt64Map(succMap)

	for _, k := range keys {
		summary.Details.Time = append(summary.Details.Time, time.Duration(k))
		succData.Data = append(succData.Data, succMap[k])
		failedData.Data = append(failedData.Data, failedMap[k])

		length := buildTimes[k].num
		aveBuildTime := int64(0)
		if length > 0 {
			aveBuildTime = buildTimes[k].totalTime / int64(length)
		}
		timeData.Data = append(timeData.Data, aveBuildTime)
	}

	summary.Details.Results = append(summary.Details.Results, succData)
	summary.Details.Results = append(summary.Details.Results, failedData)
	summary.Details.Results = append(summary.Details.Results, timeData)

	return &summary, nil
}

func generateTaskResult(containers *[]CmContainer) (*TaskSummary, error) {
	var summary TaskSummary

	if containers == nil {
		return nil, errors.Errorf("invalid params: containers is nil")
	}

	runMap, _, _ := initWeeklyMap()

	for _, c := range *containers {
		t, err := time.Parse(time.RFC3339Nano, c.StartedAt)
		if err != nil {
			logrus.Errorf("time format error, expect: %s, actual: %s", time.RFC3339Nano, c.StartedAt)
			continue
		}

		dayTime := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC).Unix()

		if c.Status == "running" {
			if _, ok := runMap[dayTime]; ok {
				runMap[dayTime]++
			}
			summary.Running++
		}
	}

	// 对 runMap key 进行排序
	keys := SortInt64Map(runMap)

	points := Data{Type: ACTIVITE_DATE_TYPE_TASKNUM}
	for _, k := range keys {
		summary.Details.Time = append(summary.Details.Time, time.Duration(k))
		points.Data = append(points.Data, runMap[k])
	}

	summary.Details.Results = append(summary.Details.Results, points)

	return &summary, nil
}

func (db *DbClient) lastWeekDeploySuccRateByPlatform() (float32, error) {
	var deployments []PsV2Deployments
	var err error
	var succRate float32

	status := []DeploymentStatus{DEPLOYMENT_STATUS_FAILED, DEPLOYMENT_STATUS_OK}

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).Find(&deployments).Error; err != nil {
		return succRate, err
	}

	return calcDeploySuccessRate(&deployments)
}

func (db *DbClient) lastWeekDeploySuccRateByOrg(orgID string) (float32, error) {
	var deployments []PsV2Deployments
	var err error
	var succRate float32

	status := []DeploymentStatus{DEPLOYMENT_STATUS_FAILED, DEPLOYMENT_STATUS_OK}

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE org_id = ?))", orgID).
		Find(&deployments).Error; err != nil {
		return succRate, err
	}

	return calcDeploySuccessRate(&deployments)
}

func (db *DbClient) lastWeekDeploySuccRateByProject(projectId string) (float32, error) {
	var deployments []PsV2Deployments
	var err error
	var succRate float32

	status := []DeploymentStatus{DEPLOYMENT_STATUS_FAILED, DEPLOYMENT_STATUS_OK}

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	// ps_projects.project_id 才是 project ID
	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE project_id = ?))", projectId).
		Find(&deployments).Error; err != nil {
		return succRate, err
	}

	return calcDeploySuccessRate(&deployments)
}

func calcDeploySuccessRate(deps *[]PsV2Deployments) (float32, error) {
	var succRate float32
	var succNum int64

	if deps == nil {
		return succRate, errors.Errorf("invalid params, deployments is nil")
	}

	for _, dep := range *deps {
		if dep.Status == DEPLOYMENT_STATUS_OK {
			succNum++
		}
	}

	total := len(*deps)
	if total > 0 {
		succRate = float32(succNum) / float32(total)
	}

	return succRate, nil
}

func (db *DbClient) lastWeekBuildSuccRateByPlatform() (float32, error) {
	var activities []PsActivities
	var builds []PsV2Builds
	var err error
	var succRate float32

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	actions := []ActiviteAction{B_FAILED, B_END}
	if err = db.Where("action in (?) AND created_at BETWEEN ? AND ?", actions, twoWeek, lastWeek).Find(&activities).Error; err != nil {
		return succRate, err
	}

	status := []BuildStatus{BUILD_STATUS_FAILED, BUILD_STATUS_OK}
	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).Find(&builds).Error; err != nil {
		return succRate, err
	}

	return calcBuildSuccessRate(&activities, &builds)
}

func (db *DbClient) lastWeekBuildSuccRateByOrg(orgID string) (float32, error) {
	var activities []PsActivities
	var builds []PsV2Builds
	var err error
	var succRate float32

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	actions := []ActiviteAction{B_FAILED, B_END}
	if err = db.Where("action in (?) AND org_id = ? AND created_at BETWEEN ? AND ?", actions, orgID, twoWeek, lastWeek).
		Find(&activities).Error; err != nil {
		return succRate, err
	}

	status := []BuildStatus{BUILD_STATUS_FAILED, BUILD_STATUS_OK}
	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE org_id = ?))", orgID).
		Find(&builds).Error; err != nil {
		return succRate, err
	}

	return calcBuildSuccessRate(&activities, &builds)
}

func (db *DbClient) lastWeekBuildSuccRateByProject(projectId string) (float32, error) {
	var activities []PsActivities
	var builds []PsV2Builds
	var err error
	var succRate float32

	now := time.Now()
	lastWeek := now.AddDate(0, 0, -6).Format("2006-01-02")
	twoWeek := now.AddDate(0, 0, -13).Format("2006-01-02")

	actions := []ActiviteAction{B_FAILED, B_END}
	if err = db.Where("action in (?) AND project_id = ? AND created_at BETWEEN ? AND ?", actions, projectId, twoWeek, lastWeek).
		Find(&activities).Error; err != nil {
		return succRate, err
	}

	status := []BuildStatus{BUILD_STATUS_FAILED, BUILD_STATUS_OK}
	// ps_projects.project_id 才是 project ID
	if err = db.Where("status in (?) AND created_at BETWEEN ? AND ?", status, twoWeek, lastWeek).
		Where("runtime_id IN (SELECT id FROM ps_v2_project_runtimes WHERE project_id IN (SELECT id FROM ps_projects WHERE project_id = ?))", projectId).
		Find(&builds).Error; err != nil {
		return succRate, err
	}

	return calcBuildSuccessRate(&activities, &builds)
}

func calcBuildSuccessRate(activities *[]PsActivities, builds *[]PsV2Builds) (float32, error) {
	var succRate float32
	var succNum int64

	if activities == nil || builds == nil {
		return succRate, errors.Errorf("invalid params, deployments is nil")
	}

	for _, activite := range *activities {
		if activite.Action == B_END {
			succNum++
		}
	}

	for _, build := range *builds {
		if build.Status == BUILD_STATUS_OK {
			succNum++
		}
	}

	total := len(*activities) + len(*builds)
	if total > 0 {
		succRate = float32(succNum) / float32(total)
	}

	return succRate, nil
}

func initWeeklyMap() (map[int64]int64, map[int64]int64, map[int64]*buildTime) {
	succ := make(map[int64]int64)
	failed := make(map[int64]int64)
	times := make(map[int64]*buildTime)

	now := time.Now().UTC()

	for i := 0; i < 7; i++ {
		year, month, day := now.AddDate(0, 0, -i).Date()
		key := time.Date(year, month, day, 0, 0, 0, 0, time.UTC).Unix()
		succ[key] = 0
		failed[key] = 0
		times[key] = &buildTime{totalTime: 0, num: 0}
	}

	return succ, failed, times
}
