package api

import (
	"context"
	"github.com/pkg/errors"
)

// CreateOrUpdateService 更新服务信息
func (db *DbClient) CreateOrUpdateService(ctx context.Context, service *CmService) error {
	var err error

	if service == nil {
		return errors.Errorf("invalid params: service is nil")
	}

	if err = db.Save(service).Error; err != nil {
		return err
	}

	return nil
}
