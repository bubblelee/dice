package api

import (
	"context"
	"strings"

	"github.com/pkg/errors"
)

const (
	hostType      = "host"
	serviceType   = "service"
	componentType = "component"
	addonType     = "addon"
	unknownType   = "unknown"
)

// Resource 查找到的资源
type Resource struct {
	Type     string      `json:"type"`
	Resource interface{} `json:"resource"`
}

// ServiceResource dice上的资源
type ServiceResource struct {
	Name             string              `json:"name"`
	ProjectUsage     *ProjectUsage       `json:"project_usage"`
	ApplicationUsage []*ApplicationUsage `json:"application_usage"`
	RuntimeUsage     []*RuntimeUsage     `json:"runtime_usage"`
	ServiceUsage     []*ServiceUsage     `json:"service_usage"`
	Resource         []*CmContainer      `json:"resource"`
}

// ProjectCache 大项目资源缓存
type ProjectCache struct {
	Usage       *ProjectUsage
	Application map[string]interface{}
	Runtime     map[string]interface{}
	Services    map[string]interface{}
	Resource    []*CmContainer
}

// ExtraUsage 额外资源占用率
type ExtraUsage struct {
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
}

// ExtraResource 额外的资源，例如中间件，组件
type ExtraResource struct {
	Type     string         `json:"type"`
	Usage    *ExtraUsage    `json:"usage"`
	Resource []*CmContainer `json:"resource"`
}

// GlobalSearch 全局搜索
func (db *DbClient) GlobalSearch(ctx context.Context, cluster, keyword string) ([]*Resource, error) {
	var result []*Resource
	var hostResult []*Resource
	var searchType, searchValue string
	var err error

	if len(strings.Split(keyword, ":")) == 2 {
		value := strings.Split(keyword, ":")
		searchType = strings.ToLower(value[0])
		searchValue = value[1]
	} else {
		keyword = strings.ToLower(keyword)
		if keyword == hostType || keyword == "container" || keyword == componentType ||
			keyword == serviceType || keyword == addonType {
			searchType = keyword
			searchValue = "*"
		}
	}

	if len(searchValue) > 0 {
		return db.typeSearch(ctx, cluster, searchType, searchValue)

	}

	if result, err = db.containerSearch(ctx, cluster, keyword); err != nil {
		return nil, err
	}

	if hostResult, err = db.hostSearch(ctx, cluster, keyword); err != nil {
		return nil, err
	}

	result = append(result, hostResult...)

	return result, nil
}

func (db *DbClient) typeSearch(ctx context.Context, cluster string, searchType, searchValue string) ([]*Resource, error) {

	if cluster == "" || searchType == "" || searchValue == "" {
		return nil, errors.Errorf("invalid params: cluster=%s, searchType=%s, searchValue=%s", cluster, searchType, searchValue)
	}

	switch searchType {
	case hostType:
		return db.hostSearch(ctx, cluster, searchValue)
	case componentType, addonType:
		return db.componentOrAddonSearch(ctx, cluster, searchType, searchValue)
	case serviceType:
		return db.serviceSearch(ctx, cluster, searchValue)
	case "container":
		return db.containerSearch(ctx, cluster, searchValue)
	default:
		return nil, nil
	}
}

func (db *DbClient) hostSearch(ctx context.Context, cluster string, value string) ([]*Resource, error) {
	var result []*Resource

	hosts, err := db.AllHostsByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	for _, host := range *hosts {
		var resource Resource
		if host.hasSearchValue(value) {
			diceLabels := strings.Split(host.Labels, ";")[0]
			host.Labels = strings.Split(diceLabels, ":")[1]

			resource.Type = hostType
			resource.Resource = host
			result = append(result, &resource)
		}
	}

	return result, nil
}

func (db *DbClient) componentOrAddonSearch(ctx context.Context, cluster, searchType, searchValue string) ([]*Resource, error) {
	var result []*Resource
	var containers *[]CmContainer
	var cName string
	var err error

	switch searchType {
	case componentType:
		containers, err = db.AllComponentsByCluster(ctx, cluster)
		if err != nil {
			return nil, err
		}
	case addonType:
		containers, err = db.AllAddonsByCluster(ctx, cluster)
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.Errorf("mismatch search type: %s", searchType)
	}

	extras := make(map[string]*ExtraResource)

	for _, container := range *containers {
		if searchType == addonType {
			cName = container.DiceAddon
		} else {
			cName = container.DiceComponent
		}
		if container.hasSearchValue(searchValue) {
			extraMerge(&container, extras, searchType, cName)
		}
	}

	for _, extra := range extras {
		var resource Resource
		resource.Type = extra.Type
		resource.Resource = extra
		result = append(result, &resource)
	}
	return result, nil
}

func (db *DbClient) serviceSearch(ctx context.Context, cluster string, searchValue string) ([]*Resource, error) {
	var result []*Resource
	var containers *[]CmContainer
	var err error

	containers, err = db.AllProjectsContainersByCluster(ctx, cluster)
	if err != nil {
		return nil, err
	}

	projects := make(map[string]*ProjectCache)
	applications := make(map[string]*ApplicationUsage)
	runtimes := make(map[string]*RuntimeUsage)
	servs := make(map[string]*ServiceUsage)

	for i, container := range *containers {
		if container.hasSearchValue(searchValue) {
			sortOutServices(&(*containers)[i], projects, applications, runtimes, servs)
		}
	}

	result = serviceMerge(projects, applications, runtimes, servs)

	return result, nil
}

func (db *DbClient) containerSearch(ctx context.Context, cluster string, searchValue string) ([]*Resource, error) {
	var result []*Resource
	var resultComponent []*Resource
	var resultAddon []*Resource
	var err error

	if result, err = db.serviceSearch(ctx, cluster, searchValue); err != nil {
		return nil, err
	}

	if resultComponent, err = db.componentOrAddonSearch(ctx, cluster, "component", searchValue); err != nil {
		return nil, err
	}

	if resultAddon, err = db.componentOrAddonSearch(ctx, cluster, "addon", searchValue); err != nil {
		return nil, err
	}

	result = append(result, resultComponent...)
	result = append(result, resultAddon...)

	return result, nil
}

func extraMerge(container *CmContainer, extras map[string]*ExtraResource, extraType, name string) {
	var extra ExtraResource
	if ex, ok := extras[name]; ok {
		ex.Resource = append(ex.Resource, container)
		ex.Usage.Memory += float64(container.Memory)
		ex.Usage.Disk += float64(container.Disk)
		ex.Usage.CPU += container.CPU
		ex.Usage.Instance++
		extras[name] = ex
	} else {
		extras[name] = &extra
		extra.Type = extraType
		extra.Resource = make([]*CmContainer, 0)
		extra.Resource = append(extra.Resource, container)
		extra.Usage = &ExtraUsage{
			Instance: 1,
			Name:     name,
			Memory:   float64(container.Memory),
			Disk:     float64(container.Disk),
			CPU:      container.CPU,
		}
	}
}

func serviceMerge(projects map[string]*ProjectCache, applications map[string]*ApplicationUsage, runtimes map[string]*RuntimeUsage, services map[string]*ServiceUsage) []*Resource {
	var result []*Resource

	for name, pro := range projects {
		var serviceResource ServiceResource
		var resource Resource

		serviceResource.Name = name
		serviceResource.Resource = pro.Resource
		serviceResource.ProjectUsage = pro.Usage

		for fullName := range pro.Application {
			serviceResource.ApplicationUsage = append(serviceResource.ApplicationUsage, applications[fullName])
		}

		for fullName := range pro.Runtime {
			serviceResource.RuntimeUsage = append(serviceResource.RuntimeUsage, runtimes[fullName])
		}

		for fullName := range pro.Services {
			serviceResource.ServiceUsage = append(serviceResource.ServiceUsage, services[fullName])
		}
		resource.Type = serviceType
		resource.Resource = serviceResource
		result = append(result, &resource)
	}
	return result
}

func sortOutServices(container *CmContainer, projects map[string]*ProjectCache, applications map[string]*ApplicationUsage, runtimes map[string]*RuntimeUsage, services map[string]*ServiceUsage) {
	var appFullName, runtimeFullName, serviceFullName string

	name := container.DiceProjectName

	if ser, ok := projects[name]; ok {
		ser.Resource = append(ser.Resource, container)
		ser.Usage.Instance++
		ser.Usage.CPU += container.CPU
		ser.Usage.Disk += float64(container.Disk)
		ser.Usage.Memory += float64(container.Memory)

		appFullName = name + "/" + container.DiceApplicationName

		if _, ok := ser.Application[appFullName]; ok {
			app := applications[appFullName]
			app.Instance++
			app.CPU += container.CPU
			app.Disk += float64(container.Disk)
			app.Memory += float64(container.Memory)
		} else {
			var app ApplicationUsage
			ser.Application[appFullName] = struct{}{}
			applications[appFullName] = &app
			app.ID = container.DiceApplication
			app.Name = container.DiceApplicationName
			app.Instance++
			app.CPU += container.CPU
			app.Disk += float64(container.Disk)
			app.Memory += float64(container.Memory)
		}

		runtimeFullName = appFullName + "/" + container.DiceRuntimeName

		if _, ok := ser.Runtime[runtimeFullName]; ok {
			runtime := runtimes[runtimeFullName]
			runtime.Instance++
			runtime.CPU += container.CPU
			runtime.Disk += float64(container.Disk)
			runtime.Memory += float64(container.Memory)
		} else {
			var runtime RuntimeUsage
			ser.Runtime[runtimeFullName] = struct{}{}
			runtimes[runtimeFullName] = &runtime
			runtime.ID = container.DiceRuntime
			runtime.Name = container.DiceRuntimeName
			runtime.Application = container.DiceApplication
			runtime.Instance++
			runtime.CPU += container.CPU
			runtime.Disk += float64(container.Disk)
			runtime.Memory += float64(container.Memory)
		}

		serviceFullName = runtimeFullName + "/" + container.DiceService

		if _, ok := ser.Services[serviceFullName]; ok {
			service := services[serviceFullName]
			service.Instance++
			service.CPU += container.CPU
			service.Disk += float64(container.Disk)
			service.Memory += float64(container.Memory)
		} else {
			var service ServiceUsage
			ser.Services[serviceFullName] = struct{}{}
			services[serviceFullName] = &service
			service.Name = container.DiceService
			service.Runtime = container.DiceRuntime
			service.Instance++
			service.CPU += container.CPU
			service.Disk += float64(container.Disk)
			service.Memory += float64(container.Memory)
		}
	} else {
		var proCache ProjectCache
		projects[name] = &proCache
		proCache.Resource = make([]*CmContainer, 0)
		proCache.Resource = append(proCache.Resource, container)
		proCache.Usage = &ProjectUsage{
			Instance: 1,
			CPU:      container.CPU,
			Disk:     float64(container.Disk),
			Memory:   float64(container.Memory),
			ID:       container.DiceProject,
			Name:     container.DiceProjectName,
		}

		appFullName = name + "/" + container.DiceApplicationName

		var app ApplicationUsage
		proCache.Application = make(map[string]interface{})
		proCache.Application[appFullName] = struct{}{}
		applications[appFullName] = &app
		app.ID = container.DiceApplication
		app.Name = container.DiceApplicationName
		app.Instance++
		app.CPU += container.CPU
		app.Disk += float64(container.Disk)
		app.Memory += float64(container.Memory)

		runtimeFullName = appFullName + "/" + container.DiceRuntimeName

		var runtime RuntimeUsage
		proCache.Runtime = make(map[string]interface{})
		proCache.Runtime[runtimeFullName] = struct{}{}
		runtimes[runtimeFullName] = &runtime
		runtime.ID = container.DiceRuntime
		runtime.Name = container.DiceRuntimeName
		runtime.Application = container.DiceApplication
		runtime.Instance++
		runtime.CPU += container.CPU
		runtime.Disk += float64(container.Disk)
		runtime.Memory += float64(container.Memory)

		serviceFullName = runtimeFullName + "/" + container.DiceService

		var service ServiceUsage
		proCache.Services = make(map[string]interface{})
		proCache.Services[serviceFullName] = struct{}{}
		services[serviceFullName] = &service
		service.Name = container.DiceService
		service.Runtime = container.DiceRuntime
		service.Instance++
		service.CPU += container.CPU
		service.Disk += float64(container.Disk)
		service.Memory += float64(container.Memory)
	}
}
