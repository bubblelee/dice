// +build !default

package api

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAllContainersOnlyByService(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	runtime := "1214"
	services := []string{
		"blog-service",
		"showcase-front",
	}

	summary, err := db.AllContainersByService(context.Background(), runtime, services)
	assert.Nil(t, err)

	t.Logf("TestAllContainersOnlyByService result: %+v", summary)
}
