// +build !default

package api

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUpdateHost(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	host := &CmHost{
		Cluster:       "terminus-test",
		Name:          "terminus-test-lbl",
		Cpus:          2.0,
		Memory:        16658149376,
		Disk:          16658149376,
		PrivateAddr:   "10.168.0.102",
		OS:            "caicai",
		KernelVersion: "caicai",
		Deleted:       false,
	}

	// update
	err = db.RemoveOrUpdateHost(context.Background(), "diff", host)
	assert.Nil(t, err)

	// create
	host.PrivateAddr = "10.168.0.199"
	err = db.RemoveOrUpdateHost(context.Background(), "diff", host)
	assert.Nil(t, err)
}

func TestRemoveHost(t *testing.T) {
	db, err := newDBClient()
	assert.Nil(t, err)

	host := &CmHost{
		Cluster:       "terminus-test",
		Name:          "terminus-test-lbl",
		Cpus:          2.0,
		Memory:        16658149376,
		Disk:          16658149376,
		PrivateAddr:   "10.168.0.199",
		OS:            "caicai",
		KernelVersion: "caicai",
		Deleted:       true,
	}

	// delete
	err = db.RemoveOrUpdateHost(context.Background(), "diff", host)
	assert.Nil(t, err)
}
