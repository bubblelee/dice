package api

import (
	"time"

	"github.com/jinzhu/gorm"
)

// DbClient db client
type DbClient struct {
	*gorm.DB
}

// ModelHeader metadata header
type ModelHeader struct {
	ID        int64 `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

// CmHost 主机元数据
type CmHost struct {
	ModelHeader   `json:"-"`
	Name          string  `json:"hostname"`          // 主机名
	Cluster       string  `json:"cluster_full_name"` // 集群名字
	Cpus          float64 `json:"cpus"`              // 总CPU个数
	Memory        int64   `json:"memory"`            // 总内存数（字节）
	Disk          int64   `json:"disk"`              // 磁盘大小（字节）
	PrivateAddr   string  `json:"private_addr"`      // 内网地址
	Labels        string  `json:"labels"`            // 环境标签
	OS            string  `json:"os"`                // 操作系统类型
	KernelVersion string  `json:"kernel_version"`    // 内核版本
	SystemTime    string  `json:"system_time"`       // 系统时间
	Birthday      int64   `json:"created_at"`        // 创建时间（operator定义）
	Deleted       bool    `json:"deleted"`           // 资源是否被删除
	TimeStamp     int64   `json:"timestamp"`         // 消息本身的时间戳
}

// HostMSG kafka主机数据格式
type HostMSG struct {
	Type      string    `json:"type"`
	TimeStamp int64     `json:"timestamp"`
	Cluster   string    `json:"cluster_full_name"`
	Data      []*CmHost `json:"data"`
}

// CmContainer 容器元数据
type CmContainer struct {
	ModelHeader `json:"-"`
	ID          string `json:"id" gorm:"column:container_id"` // 容器ID
	Deleted     bool   `json:"deleted"`                       // 资源是否被删除
	// TODO: 时间戳？
	StartedAt           string  `json:"started_at"`             // 容器启动时间
	Cluster             string  `json:"cluster_full_name"`      // 集群名
	HostPrivateIPAddr   string  `json:"host_private_addr"`      // 宿主机内网地址
	IPAddress           string  `json:"ip_addr"`                // 容器IP地址
	Image               string  `json:"image_name"`             // 容器镜像名
	CPU                 float64 `json:"cpu"`                    // 分配的cpu
	Memory              int64   `json:"memory"`                 // 分配的内存（字节）
	Disk                int64   `json:"disk"`                   // 分配的磁盘空间（字节）
	DiceOrg             string  `json:"dice_org"`               // 所在的组织
	DiceProject         string  `json:"dice_project"`           // 所在大项目
	DiceApplication     string  `json:"dice_application"`       // 所在项目
	DiceRuntime         string  `json:"dice_runtime"`           // 所在runtime
	DiceService         string  `json:"dice_service"`           // 所属应用
	DiceProjectName     string  `json:"dice_project_name"`      // 所在大项目名称
	DiceApplicationName string  `json:"dice_application_name"`  // 所在项目
	DiceRuntimeName     string  `json:"dice_runtime_name"`      // 所在runtime
	DiceComponent       string  `json:"dice_component"`         // 组件名
	DiceAddon           string  `json:"dice_addon"`             // 中间件名
	Status              string  `json:"status"`                 // 前期定义为docker状态（后期期望能表示服务状态）
	TimeStamp           int64   `json:"timestamp"`              // 消息本身的时间戳
	Env                 string  `json:"env,omitempty" gorm:"-"` // 该容器由哪个环境发布(dev, test, staging, prod)
}

// ContainerMSG kafka容器信息格式
type ContainerMSG struct {
	Type      string         `json:"type"`
	TimeStamp int64          `json:"timestamp"`
	Cluster   string         `json:"cluster_full_name"`
	HostIP    string         `json:"host_private_addr"`
	Data      []*CmContainer `json:"data"`
}

// CmService 服务
type CmService struct {
	ModelHeader     `json:"-"`
	Cluster         string `json:"-"`            // 集群名
	DiceProject     string `json:"-"`            // 所在大项目名称
	DiceApplication string `json:"-"`            // 所在项目
	DiceRuntime     string `json:"-"`            // 所在runtime
	DiceService     string `json:"-"`            // 所属应用
	PrivateAddr     string `json:"private_addr"` // 服务内部地址(lb)
	PublicAddr      string `json:"public_addr"`  // 服务对外地址(lb)
}

// ClusterUsage 容器资源分配
type ClusterUsage struct {
	TotalMemory float64 `json:"total_memory"`
	TotalCPU    float64 `json:"total_cpu"`
	TotalDisk   float64 `json:"total_disk"`
	UsedMemory  float64 `json:"used_memory"`
	UsedCPU     float64 `json:"used_cpu"`
	UsedDisk    float64 `json:"used_disk"`
}

// HostUsage 主机资源分配
type HostUsage struct {
	HostName    string  `json:"host_name"`
	IPAddress   string  `json:"ip_address"`
	TotalMemory float64 `json:"total_memory"`
	TotalCPU    float64 `json:"total_cpu"`
	TotalDisk   float64 `json:"total_disk"`
	UsedMemory  float64 `json:"used_memory"`
	UsedCPU     float64 `json:"used_cpu"`
	UsedDisk    float64 `json:"used_disk"`
	Labels      string  `json:"labels"`
	Tasks       int     `json:"tasks"`
	CreatedAt   int64   `json:"created_at"` // 创建时间
}

// ContainerUsage 容器资源分配
type ContainerUsage struct {
	ID     string  `json:"id"`
	Memory float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk   float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU    float64 `json:"cpu"`
}

// ComponentUsage 组件资源分配
type ComponentUsage struct {
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
}

// AddOnUsage 中间件资源分配
type AddOnUsage struct {
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
}

// ProjectUsage 大项目资源分配
type ProjectUsage struct {
	ID       string  `json:"id"`
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
}

// ApplicationUsage 项目资源分配
type ApplicationUsage struct {
	ID       string  `json:"id"`
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
}

// RuntimeUsage runtime资源分配
type RuntimeUsage struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Instance    int     `json:"instance"`
	Memory      float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk        float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU         float64 `json:"cpu"`
	Application string  `json:"application,omitempty"`
}

// ServiceUsage 服务资源分配
type ServiceUsage struct {
	Name     string  `json:"name"`
	Instance int     `json:"instance"`
	Memory   float64 `json:"memory"` // 分配的内存大小单位（MB）
	Disk     float64 `json:"disk"`   // 分配的磁盘大小单位（MB）
	CPU      float64 `json:"cpu"`
	Runtime  string  `json:"runtime,omitempty"`
}

// PsActivities 数据库表 ps_activities
type PsActivities struct {
	ModelHeader   `json:"-"`
	OrgId         int64          `json:"orgId"`
	ProjectId     int64          `json:"projectId"`
	ApplicationId int64          `json:"applicationId"`
	RuntimeId     int64          `json:"runtimeId"`
	BuildId       int64          `json:"buildId"`
	UserId        string         `json:"userId"`
	Type          ActiviteType   `json:"type"`
	Action        ActiviteAction `json:"action"`
	Operator      string         `json:"operator"`
	Desc          string         `json:"desc"`
	Context       interface{}    `json:"context"`
}

type ActiviteAction string

const (
	// application
	A_ADD           ActiviteAction = "A_ADD"
	A_BUILD_CREATE  ActiviteAction = "A_BUILD_CREATE"
	A_BUILD_START   ActiviteAction = "A_BUILD_START"
	A_BUILD_SUCCESS ActiviteAction = "A_BUILD_SUCCESS"
	A_DEL           ActiviteAction = "A_DEL"
	A_MEMBER_ADD    ActiviteAction = "A_MEMBER_ADD"
	A_MEMBER_DEL    ActiviteAction = "A_MEMBER_DEL"
	A_MEMBER_MOD    ActiviteAction = "A_MEMBER_MOD"
	A_MOD           ActiviteAction = "A_MOD"

	// pipeline pack
	B_CREATE ActiviteAction = "B_CREATE"
	B_START  ActiviteAction = "B_START"
	B_CANCEL ActiviteAction = "B_CANCEL"
	B_FAILED ActiviteAction = "B_FAILED"
	B_END    ActiviteAction = "B_END"
)

type ActiviteType string

const (
	ACTIVITE_TYPE_APPLICATION ActiviteType = "A"
	ACTIVITE_TYPE_PACKER      ActiviteType = "B"
	ACTIVITE_TYPE_ORG         ActiviteType = "O"
	ACTIVITE_TYPE_PROJECT     ActiviteType = "P"
	ACTIVITE_TYPE_RUNTIME     ActiviteType = "R"
	ACTIVITE_TYPE_SYSTEM      ActiviteType = "S"
	ACTIVITE_TYPE_USER        ActiviteType = "U"
)

// ++ A
type ApplicationAddContext struct {
	App        Application `json:"application"`
	Operator   string      `json:"operator"`
	ActionName string      `json:"actionName"`
}

type Application struct {
	Id          int64    `json:"id"`
	Name        string   `json:"name"`
	OrgID       int      `json:"orgId"`
	ProjectId   int64    `json:"projectId"`
	ProjectName string   `json:"projectName"`
	Logo        string   `json:"logo"`
	Desc        string   `json:"desc"`
	Stats       AppStats `json:"stats"`
	Repo        string   `json:"repo"`
}

// AppStats 应用状态
type AppStats struct {
	CountRuntimes    int    `json:"countRuntimes"`
	CountMembers     int    `json:"countMembers"`
	TimeLastModified string `json:"timeLastMOdified"`
}

// ++ B
type PipelinePackerContext struct {
	Id string `json:"id"`
	// 代码分支
	Branch string `json:"branch"`
	// 打包环境(dev/test/staging/prod)
	Env string `json:"env"`
}

// PsV2Deployments 部署数据库表
type PsV2Deployments struct {
	ModelHeader
	RuntimeId        int64            `json:"runtime_id"`
	BuildId          string           `json:"build_id"`
	Status           DeploymentStatus `json:"status"`
	Step             string           `json:"step"`
	Operator         string           `json:"operator"`
	PushAbility      int8             `json:"push_ability"`
	AbilityRuntimeId int64            `json:"ability_runtime_id"`
}

// DeploymentStatus 部署状态
type DeploymentStatus string

const (
	DEPLOYMENT_STATUS_INIT      DeploymentStatus = "INIT"
	DEPLOYMENT_STATUS_WAITING   DeploymentStatus = "WAITING"
	DEPLOYMENT_STATUS_DEPLOYING DeploymentStatus = "DEPLOYING"
	DEPLOYMENT_STATUS_OK        DeploymentStatus = "OK"
	DEPLOYMENT_STATUS_FAILED    DeploymentStatus = "FAILED"
	DEPLOYMENT_STATUS_CANCELED  DeploymentStatus = "CANCELED"
)

// PsV2Builds 构建数据表
type PsV2Builds struct {
	ModelHeader
	RuntimeId int64       `json:"runtime_id"`
	Status    BuildStatus `json:"status"`
	Operator  string      `json:"operator"`
	GitRepo   string      `json:"git_repo"`
	GitBranch string      `json:"git_branch"`
	GitCommit string      `json:"git_commit"`
	Env       string      `json:"env"`
}

type BuildStatus string

const (
	BUILD_STATUS_INIT          BuildStatus = "INIT"
	BUILD_STATUS_PACKING       BuildStatus = "PACKING"
	BUILD_STATUS_UNSCHEDULABLE BuildStatus = "UNSCHEDULABLE"
	BUILD_STATUS_OK            BuildStatus = "OK"
	BUILD_STATUS_FAILED        BuildStatus = "FAILED"
	BUILD_STATUS_CANCELED      BuildStatus = "CANCELED"
)
