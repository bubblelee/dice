package api

import (
	"context"
	"strings"

	"github.com/pkg/errors"
)

// RemoveOrUpdateHost host信息更新
func (c *DbClient) RemoveOrUpdateHost(ctx context.Context, msgType string, h *CmHost) error {
	var err error

	if h == nil {
		return errors.Errorf("invalid params: host is nil")
	}

	// TODO: 确认是否需要清理该宿主机上的所有的容器
	if h.Deleted {
		if err = c.deleteHost(ctx, h); err != nil {
			return errors.Errorf("failed to delete host(%s): %v", h.PrivateAddr, err)
		}
	} else {
		if err = c.updateHost(ctx, h); err != nil {
			return errors.Errorf("failed to update host(%s): %v", h.PrivateAddr, err)
		}
	}

	if msgType == "diff" && !h.Deleted {
		hostDiffCount++
	}
	hostCount++

	return nil
}

func (c *DbClient) updateHost(ctx context.Context, h *CmHost) error {
	var count int
	var err error

	// 基于 IP 统计 host 数量，用于觉得是新增还是更新 host
	if err = c.Model(&CmHost{}).Where("cluster = ? AND private_addr = ?", h.Cluster, h.PrivateAddr).Count(&count).Error; err != nil {
		return err
	}

	// 新增 host
	if count == 0 {
		return c.Save(h).Error
	}

	// 更新 host
	return c.Model(&CmHost{}).Where("cluster = ? AND private_addr = ?", h.Cluster, h.PrivateAddr).Updates(h).Error
}

func (c *DbClient) deleteHost(ctx context.Context, h *CmHost) error {
	return c.Where("cluster = ? AND private_addr = ?", h.Cluster, h.PrivateAddr).Delete(CmHost{}).Error
}

// QueryHost 获取单个host的信息
func (c *DbClient) QueryHost(ctx context.Context, cluster, addr string) (*CmHost, error) {
	var h CmHost
	var err error

	if cluster == "" || addr == "" {
		return nil, errors.Errorf("invalid params: cluster = %s, addr = %s", cluster, addr)
	}

	if err = c.Where("cluster = ? AND private_addr = ?", cluster, addr).Find(&h).Error; err != nil {
		return nil, err
	}

	return &h, nil
}

// AllHostsByCluster 获取指定集群下所有的host信息
func (c *DbClient) AllHostsByCluster(ctx context.Context, cluster string) (*[]CmHost, error) {
	var hosts []CmHost
	var err error

	if cluster == "" {
		return nil, errors.Errorf("invalid params: cluster = %s", cluster)
	}

	if err = c.Where("cluster = ?", cluster).Find(&hosts).Error; err != nil {
		return nil, err
	}

	return &hosts, nil
}

// DeleteHost 删除指定集群下所有 hosts
func (c *DbClient) DeleteHost(ctx context.Context, cluster, name string) error {

	if cluster == "" || name == "" {
		return errors.Errorf("invalid params: cluster = %s, host.name = %s", cluster, name)
	}

	return c.Where("cluster = ? AND name = ?", cluster, name).Delete(CmHost{}).Error
}

func (h *CmHost) makeLabel() {
	var currentLabel string
	diceLabels := strings.Split(h.Labels, ";")
	for _, diceLabel := range diceLabels {
		if strings.Contains(diceLabel, "dice_tags:") {
			currentLabel = strings.Split(diceLabel, "\n")[0]
			break
		}
	}
	if currentLabel == "" {
		currentLabel = "dice_tags:any"
	}
	h.Labels = strings.Split(currentLabel, ":")[1]
	labels := strings.Split(h.Labels, ",")
	var newLabels, anyLabel, lockLabel, otherLabels []string
	for _, label := range labels {
		if label == "any" {
			anyLabel = append(anyLabel, label)
		} else if label == "locked" {
			lockLabel = append(lockLabel, label)
		} else {
			otherLabels = append(otherLabels, label)
		}
	}

	newLabels = append(newLabels, lockLabel...)
	newLabels = append(newLabels, anyLabel...)
	newLabels = append(newLabels, otherLabels...)
	h.Labels = strings.Join(newLabels, ",")
}

func (h *CmHost) hasSearchValue(value string) bool {
	if h == nil {
		return false
	}

	if value == "*" {
		return true
	}

	if strings.Contains(h.Name, value) ||
		strings.Contains(h.PrivateAddr, value) {
		return true
	}

	return false
}
