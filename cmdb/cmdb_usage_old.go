package main

import (
	"context"
	"net/http"
)

func (s *Server) applicationUsageByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	project := vars["project"]

	result, err := s.db.AllApplicationUsageByProject(ctx, project)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "failed to get application usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ApplicationUsageResponse{
			Result: result,
		},
	}, nil
}

func (s *Server) runtimeUsageByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	application := vars["application"]

	result, err := s.db.AllRuntimeUsageByApplication(ctx, application)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "failed to get runtime usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: RuntimeUsageResponse{
			Result: result,
		},
	}, nil
}

func (s *Server) serviceUsageByOld(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	runtime := vars["runtime"]

	result, err := s.db.AllServiceUsageByRuntime(ctx, runtime)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "failed to get service usage from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ServiceUsageResponse{
			Result: result,
		},
	}, nil
}
