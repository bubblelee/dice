package main

import (
	"context"
	"net/http"

	"terminus.io/dice/dice/cmdb/api"
)

// 获取指定集群下某个host的信息
func (s *Server) queryHost(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var (
		host *api.CmHost
		err  error
	)

	cluster := vars["cluster"]
	addr := vars["host"]

	if host, err = s.db.QueryHost(ctx, cluster, addr); err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get host from database!",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: QueryHostResponse{
			Result: host,
		},
	}, nil
}

// 获取指定集群下所有host的信息
func (s *Server) allHostsByCluster(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := vars["cluster"]

	result, err := s.db.AllHostsByCluster(ctx, cluster)
	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get cluster's hosts from database!",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ClusterHostsResponse{
			Result: result,
		},
	}, nil
}
