package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"terminus.io/dice/dice/cmdb/api"
)

type statisticsKind string

const (
	STATISTICS_KIND_DEPLOY statisticsKind = "deploy"
	STATISTICS_KIND_BUILD  statisticsKind = "build"
)

var allStatisticsKinds = []statisticsKind{
	STATISTICS_KIND_DEPLOY,
	STATISTICS_KIND_BUILD,
}

// 获取部署总览信息
func (s *Server) deploymentStatistics(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return s.deployAndBuildStatistics(ctx, r, vars, STATISTICS_KIND_DEPLOY)
}

// 获取构建总览信息
func (s *Server) buildStatistics(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return s.deployAndBuildStatistics(ctx, r, vars, STATISTICS_KIND_BUILD)
}

func (s *Server) deployAndBuildStatistics(ctx context.Context, r *http.Request, vars map[string]string, kind statisticsKind) (Responser, error) {
	var result *api.PipelineSummary
	var err error

	vals := r.URL.Query()
	typeVal, ok := vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(typeVal) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch statisticsRequestType(typeVal[0]) {
	case STATISTICS_REQUEST_TYPE_PLATFORM:
		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeploymentSummaryByPlatform(ctx)
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildSummaryByPlatform(ctx)
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	case STATISTICS_REQUEST_TYPE_ORG:
		org, ok := vals["org"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: org",
			}, errors.Errorf("missing param: org")
		}

		if len(org) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: org",
			}, errors.Errorf("too many params: org")
		}

		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeploymentSummaryByOrg(ctx, org[0])
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildSummaryByOrg(ctx, org[0])
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	case STATISTICS_REQUEST_TYPE_PROJECT:
		project, ok := vals["project"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: project",
			}, errors.Errorf("missing param: project")
		}

		if len(project) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: project",
			}, errors.Errorf("too many params: project")
		}

		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeploymentSummaryByProject(ctx, project[0])
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildSummaryByProject(ctx, project[0])
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	default:
		err = errors.Errorf("invalid type param(%s), support: %v", typeVal, allStatisticsRequestTypes)
		logrus.Errorf("Failed to get %s statistics info: %v", kind, err)
	}

	if err != nil {
		content := fmt.Sprintf("failed to get %s statistics info from database.", kind)
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: content,
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取 task 总览信息
func (s *Server) taskStatistics(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result *api.TaskSummary
	var err error

	vals := r.URL.Query()
	typeVal, ok := vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(typeVal) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch statisticsRequestType(typeVal[0]) {
	case STATISTICS_REQUEST_TYPE_PLATFORM:
		result, err = s.db.TaskSummaryByPlatform(ctx)
	case STATISTICS_REQUEST_TYPE_ORG:
		org, ok := vals["org"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: org",
			}, errors.Errorf("missing param: org")
		}

		if len(org) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: org",
			}, errors.Errorf("too many params: org")
		}

		result, err = s.db.TaskSummaryByOrg(ctx, org[0])
	case STATISTICS_REQUEST_TYPE_PROJECT:
		project, ok := vals["project"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: project",
			}, errors.Errorf("missing param: project")
		}

		if len(project) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: project",
			}, errors.Errorf("too many params: project")
		}

		result, err = s.db.TaskSummaryByProject(ctx, project[0])
	default:
		err = errors.Errorf("invalid type param(%s), support: %v", typeVal, allStatisticsRequestTypes)
		logrus.Errorf("Failed to get task statistics info: %v", err)
	}

	if err != nil {
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: "failed to get task statistics info from database.",
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: TaskSummaryResponse{
			Result: result,
		},
	}, nil
}

// 获取部署动态
func (s *Server) deployDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return s.deployAndBuildDynamicStatistics(ctx, r, vars, STATISTICS_KIND_DEPLOY)
}

// 获取构建动态
func (s *Server) buildDynamic(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return s.deployAndBuildDynamicStatistics(ctx, r, vars, STATISTICS_KIND_BUILD)
}

// 获取pipeline动态
func (s *Server) deployAndBuildDynamicStatistics(ctx context.Context, r *http.Request, vars map[string]string, kind statisticsKind) (Responser, error) {
	var result *api.DynamicMsgSlice
	var err error

	vals := r.URL.Query()
	typeVal, ok := vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(typeVal) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch statisticsRequestType(typeVal[0]) {
	case STATISTICS_REQUEST_TYPE_PLATFORM:
		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeployDynamicByPlatform(ctx)
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildDynamicByPlatform(ctx)
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	case STATISTICS_REQUEST_TYPE_ORG:
		org, ok := vals["org"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: org",
			}, errors.Errorf("missing param: org")
		}

		if len(org) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: org",
			}, errors.Errorf("too many params: org")
		}

		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeployDynamicByOrg(ctx, org[0])
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildDynamicByOrg(ctx, org[0])
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	case STATISTICS_REQUEST_TYPE_PROJECT:
		project, ok := vals["project"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: project",
			}, errors.Errorf("missing param: project")
		}

		if len(project) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: project",
			}, errors.Errorf("too many params: project")
		}

		switch kind {
		case STATISTICS_KIND_DEPLOY:
			result, err = s.db.DeployDynamicByProject(ctx, project[0])
		case STATISTICS_KIND_BUILD:
			result, err = s.db.BuildDynamicByProject(ctx, project[0])
		default:
			err = errors.Errorf("invalid kind(%s), support: %v", kind, allStatisticsKinds)
		}
	default:
		err = errors.Errorf("invalid type param(%s), support: %v", typeVal, allStatisticsRequestTypes)
		logrus.Errorf("Failed to get %s dynamic info: %v", kind, err)
	}

	if err != nil {
		content := fmt.Sprintf("failed to get %s dynamic info from database.", kind)
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: content,
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: PipelineDynamicResponse{
			Result: result,
		},
	}, nil
}

// 统计资源使用情况
func (s *Server) resourceStatistics(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	var result *[]api.ResourceStatistics
	var err error

	vals := r.URL.Query()
	typeVal, ok := vals["type"]
	if !ok {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "missing param: type",
		}, errors.Errorf("missing param: type")
	}

	if len(typeVal) > 1 {
		return HTTPResponse{
			Status:  http.StatusBadRequest,
			Content: "too many params: type",
		}, errors.Errorf("too many params: type")
	}

	switch statisticsRequestType(typeVal[0]) {
	case STATISTICS_REQUEST_TYPE_PLATFORM:
		result, err = s.db.ResourceStatisticsByPlatform(ctx)
	case STATISTICS_REQUEST_TYPE_ORG:
		org, ok := vals["org"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: org",
			}, errors.Errorf("missing param: org")
		}

		if len(org) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: org",
			}, errors.Errorf("too many params: org")
		}

		result, err = s.db.ResourceStatisticsByOrg(ctx, org[0])
	case STATISTICS_REQUEST_TYPE_PROJECT:
		project, ok := vals["project"]
		if !ok {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "missing param: project",
			}, errors.Errorf("missing param: project")
		}

		if len(project) > 1 {
			return HTTPResponse{
				Status:  http.StatusBadRequest,
				Content: "too many params: project",
			}, errors.Errorf("too many params: project")
		}

		result, err = s.db.ResourceStatisticsByProject(ctx, project[0])
	default:
		err = errors.Errorf("invalid type param(%s), support: %v", typeVal[0], allStatisticsRequestTypes)
		logrus.Errorf("Failed to get resource statistics info: %v", err)
	}

	if err != nil {
		content := fmt.Sprintf("failed to get %s resource statistics info from database.", typeVal[0])
		return HTTPResponse{
			Status:  http.StatusInternalServerError,
			Content: content,
		}, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: ResourceStatisticsResponse{
			Result: result,
		},
	}, nil
}
