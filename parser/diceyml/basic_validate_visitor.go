package diceyml

import (
	"path"

	"github.com/pkg/errors"
)

type BasicValidateVisitor struct {
	DefaultVisitor
	currentService string
	currentAddOn   string
	collectErrors  []error
}

func NewBasicValidateVisitor() DiceYmlVisitor {
	return &BasicValidateVisitor{}
}

func (o *BasicValidateVisitor) VisitServices(v DiceYmlVisitor, obj *Services) {
	for name, v_ := range *obj {
		o.currentService = name
		v_.Accept(v)
	}
	o.currentService = ""
}
func (o *BasicValidateVisitor) VisitService(v DiceYmlVisitor, obj *Service) {
	if o.currentService == "" {
		panic("should not be empty")
	}
	// 可能在parse之后，由insertimage插入这个字段
	// if obj.Image == "" {
	// 	o.collectErrors = append(o.collectErrors, errors.Wrap(invalidImage, o.currentService))
	// }
	for _, port := range obj.Ports {
		if port <= 0 {
			o.collectErrors = append(o.collectErrors, errors.Wrap(invalidPort, o.currentService))
			break
		}
	}
	for _, port := range obj.Expose {
		if port <= 0 {
			o.collectErrors = append(o.collectErrors, errors.Wrap(invalidExpose, o.currentService))
			break
		}
	}
	for _, vol := range obj.Volumes {
		if !path.IsAbs(vol) {
			o.collectErrors = append(o.collectErrors, errors.Wrap(invalidVolume, o.currentService))
			break
		}
	}
}

func (o *BasicValidateVisitor) VisitResources(v DiceYmlVisitor, obj *Resources) {
	if o.currentService == "" {
		panic("should not be empty")
	}
	if obj.CPU <= 0 {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidCPU, o.currentService))
	}
	if obj.Mem <= 0 {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidMem, o.currentService))
	}
	if obj.Disk < 0 {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidDisk, o.currentService))
	}
}

func (o *BasicValidateVisitor) VisitDeployments(v DiceYmlVisitor, obj *Deployments) {
	if o.currentService == "" {
		panic("should not be empty")
	}
	if obj.Replicas <= 0 {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidReplicas, o.currentService))
	}
	if obj.Policys != "" && obj.Policys != "shuffle" && obj.Policys != "affinity" && obj.Policys != "unique" {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidPolicy, o.currentService))
	}
}

func (o *BasicValidateVisitor) VisitAddOns(v DiceYmlVisitor, obj *AddOns) {
	for name, v_ := range *obj {
		o.currentAddOn = name
		v_.Accept(v)
	}
	o.currentAddOn = ""
}

func (o *BasicValidateVisitor) VisitAddOn(v DiceYmlVisitor, obj *AddOn) {
	if o.currentAddOn == "" {
		panic("should not be empty")
	}
	if obj.Plan == "" {
		o.collectErrors = append(o.collectErrors, errors.Wrap(invalidAddonPlan, o.currentAddOn))
	}
}

func BasicValidate(obj *Object) []error {
	visitor := NewBasicValidateVisitor()
	obj.Accept(visitor)
	return visitor.(*BasicValidateVisitor).collectErrors
}
