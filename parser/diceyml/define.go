package diceyml

type Object struct {
	Version      string            `yaml:"version" json:"version"`
	Envs         map[string]string `yaml:"envs" json:"envs,omitempty"`
	Services     Services          `yaml:"services" json:"services,omitempty"`
	AddOns       AddOns            `yaml:"addons" json:"addons,omitempty"`
	Environments EnvObjects        `yaml:"environments,omitempty" json:"-"`
}
type EnvObjects map[string]*EnvObject
type AddOns map[string]*AddOn
type Services map[string]*Service

type EnvObject struct {
	Envs     map[string]string `yaml:"envs,omitempty" json:"-"`
	Services Services          `yaml:"services,omitempty" json:"services,omitempty"`
	AddOns   AddOns            `yaml:"addons,omitempty" json:"addons,omitempty"`
}

type AddOn struct {
	Plan    string            `yaml:"plan,omitempty" json:"plan"`
	As      string            `yaml:"as,omitempty" json:"as,omitempty"`
	Options map[string]string `yaml:"options,omitempty" json:"options,omitempty"`
	Image   string            `yaml:"image,omitempty" json:"image,omitempty"`
}

type Service struct {
	Image       string            `yaml:"image,omitempty" json:"image"`
	Cmd         string            `yaml:"cmd,omitempty" json:"cmd"`
	Ports       []int             `yaml:"ports,omitempty" json:"ports,omitempty"`
	Envs        map[string]string `yaml:"envs,omitempty" json:"envs,omitempty"`
	Hosts       []string          `yaml:"hosts,omitempty" json:"hosts,omitempty"`
	Resources   Resources         `yaml:"resources,omitempty" json:"resources"`
	Volumes     []string          `yaml:"volumes,omitempty" json:"volumes,omitempty"`
	Deployments Deployments       `yaml:"deployments,omitempty" json:"deployments"`
	DependsOn   []string          `yaml:"depends_on,omitempty" json:"depends_on,omitempty"`
	Expose      []int             `yaml:"expose,omitempty" json:"expose,omitempty"`
	HealthCheck HealthCheck       `yaml:"health_check,omitempty" json:"health_check"`
}

type HealthCheck struct {
	HTTP *HTTPCheck `yaml:"http,omitempty" json:"http,omitempty"`
	Exec *ExecCheck `yaml:"exec,omitempty" json:"exec,omitempty"`
}

type HTTPCheck struct {
	Port     int    `yaml:"port,omitempty" json:"port,omitempty"`
	Path     string `yaml:"path,omitempty" json:"path,omitempty"`
	Duration int    `yaml:"duration,omitempty" json:"duration,omitempty"`
}

type ExecCheck struct {
	Cmd      string `yaml:"cmd,omitempty" json:"cmd,omitempty"`
	Duration int    `yaml:"duration,omitempty" json:"duration,omitempty"`
}

type Resources struct {
	CPU    float64 `yaml:"cpu,omitempty" json:"cpu"`
	MaxCPU float64 `yaml:"max_cpu,omitempty" json:"max_cpu"`
	Mem    int     `yaml:"mem,omitempty" json:"mem"`
	Disk   int     `yaml:"disk,omitempty" json:"disk"`
}

type Deployments struct {
	Replicas int               `yaml:"replicas,omitempty" json:"replicas"`
	Policys  string            `yaml:"policys,omitempty" json:"policys"`
	Labels   map[string]string `yaml:"labels,omitempty" json:"labels,omitempty"`
}

type DiceYmlVisitor interface {
	VisitObject(v DiceYmlVisitor, obj *Object)
	VisitEnvObject(v DiceYmlVisitor, obj *EnvObject)
	VisitEnvObjects(v DiceYmlVisitor, obj *EnvObjects)
	VisitService(v DiceYmlVisitor, obj *Service)
	VisitServices(v DiceYmlVisitor, obj *Services)
	VisitAddOn(v DiceYmlVisitor, obj *AddOn)
	VisitAddOns(v DiceYmlVisitor, obj *AddOns)
	VisitResources(v DiceYmlVisitor, obj *Resources)
	VisitHealthCheck(v DiceYmlVisitor, obj *HealthCheck)
	VisitHTTPCheck(v DiceYmlVisitor, obj *HTTPCheck)
	VisitExecCheck(v DiceYmlVisitor, obj *ExecCheck)
	VisitDeployments(v DiceYmlVisitor, obj *Deployments)
}

func (obj *Object) Accept(v DiceYmlVisitor) {
	if obj.Services == nil {
		obj.Services = Services{}
	}
	obj.Services.Accept(v)
	if obj.AddOns == nil {
		obj.AddOns = AddOns{}
	}
	obj.AddOns.Accept(v)
	if obj.Environments == nil {
		obj.Environments = EnvObjects{}
	}
	obj.Environments.Accept(v)
	v.VisitObject(v, obj)
}

// 默认不会像 Object 一样默认去遍历 Services, AddOns
// 比如validate，envobject中的service可能是不全的，但也是正确的
// 如果需要遍历envobject下的service和addon，需要手动去遍历
func (obj *EnvObject) Accept(v DiceYmlVisitor) {
	v.VisitEnvObject(v, obj)
}
func (obj *EnvObjects) Accept(v DiceYmlVisitor) {
	v.VisitEnvObjects(v, obj)
}
func (obj *Service) Accept(v DiceYmlVisitor) {
	obj.Resources.Accept(v)
	obj.Deployments.Accept(v)
	obj.HealthCheck.Accept(v)
	v.VisitService(v, obj)
}
func (obj *Services) Accept(v DiceYmlVisitor) {
	v.VisitServices(v, obj)
}
func (obj *AddOn) Accept(v DiceYmlVisitor) {
	v.VisitAddOn(v, obj)
}
func (obj *AddOns) Accept(v DiceYmlVisitor) {
	v.VisitAddOns(v, obj)
}
func (obj *Resources) Accept(v DiceYmlVisitor) {
	v.VisitResources(v, obj)
}
func (obj *HealthCheck) Accept(v DiceYmlVisitor) {
	if obj.HTTP == nil {
		obj.HTTP = new(HTTPCheck)
	}
	obj.HTTP.Accept(v)
	if obj.Exec == nil {
		obj.Exec = new(ExecCheck)
	}
	obj.Exec.Accept(v)

	v.VisitHealthCheck(v, obj)
}
func (obj *HTTPCheck) Accept(v DiceYmlVisitor) {
	v.VisitHTTPCheck(v, obj)
}
func (obj *ExecCheck) Accept(v DiceYmlVisitor) {
	v.VisitExecCheck(v, obj)
}

func (obj *Deployments) Accept(v DiceYmlVisitor) {
	v.VisitDeployments(v, obj)
}

type DefaultVisitor struct{}

func (*DefaultVisitor) VisitObject(v DiceYmlVisitor, obj *Object)           {}
func (*DefaultVisitor) VisitEnvObject(v DiceYmlVisitor, obj *EnvObject)     {}
func (*DefaultVisitor) VisitEnvObjects(v DiceYmlVisitor, obj *EnvObjects)   {}
func (*DefaultVisitor) VisitService(v DiceYmlVisitor, obj *Service)         {}
func (*DefaultVisitor) VisitServices(v DiceYmlVisitor, obj *Services)       {}
func (*DefaultVisitor) VisitAddOn(v DiceYmlVisitor, obj *AddOn)             {}
func (*DefaultVisitor) VisitAddOns(v DiceYmlVisitor, obj *AddOns)           {}
func (*DefaultVisitor) VisitResources(v DiceYmlVisitor, obj *Resources)     {}
func (*DefaultVisitor) VisitHealthCheck(v DiceYmlVisitor, obj *HealthCheck) {}
func (*DefaultVisitor) VisitHTTPCheck(v DiceYmlVisitor, obj *HTTPCheck)     {}
func (*DefaultVisitor) VisitExecCheck(v DiceYmlVisitor, obj *ExecCheck)     {}
func (*DefaultVisitor) VisitDeployments(v DiceYmlVisitor, obj *Deployments) {}
