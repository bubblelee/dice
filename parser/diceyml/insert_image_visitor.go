package diceyml

import (
	"fmt"

	"github.com/pkg/errors"
)

type InsertImageVisitor struct {
	DefaultVisitor
	currentService string
	collectError   error
	images         map[string]string
	envs           map[string]map[string]string
}

func NewInsertImageVisitor(images map[string]string, envs map[string]map[string]string) DiceYmlVisitor {
	return &InsertImageVisitor{images: images, envs: envs}
}

func (o *InsertImageVisitor) VisitServices(v DiceYmlVisitor, obj *Services) {
	for name, v_ := range *obj {
		o.currentService = name
		v_.Accept(v)
	}

	o.currentService = ""
	if len(o.images) > 0 {
		images := fmt.Sprintf("%v", o.images)
		o.collectError = errors.Wrap(notfoundService, images)
	}
}

func (o *InsertImageVisitor) VisitService(v DiceYmlVisitor, obj *Service) {
	image, ok := o.images[o.currentService]
	if !ok {
		return
	}
	obj.Image = image
	if tmpenvs, ok := o.envs[o.currentService]; ok && len(tmpenvs) != 0 {
		if obj.Envs == nil {
			obj.Envs = make(map[string]string)
		}
		for k, v := range tmpenvs {
			obj.Envs[k] = v
		}
	}
	delete(o.images, o.currentService)
}

func InsertImage(obj *Object, images map[string]string, envs map[string]map[string]string) error {
	visitor := NewInsertImageVisitor(images, envs)
	obj.Accept(visitor)
	return visitor.(*InsertImageVisitor).collectError
}
