// 找出依赖却不存在的 service
package diceyml

type FindLackServiceVisitor struct {
	DefaultVisitor
	lack []string
}

func NewFindLackServiceVisitor() DiceYmlVisitor {
	return &FindLackServiceVisitor{}
}

func (o *FindLackServiceVisitor) VisitServices(v DiceYmlVisitor, obj *Services) {
	lack := []string{}
	for _, srv := range *obj {
		for _, name := range srv.DependsOn {
			if _, ok := (*obj)[name]; !ok {
				lack = append(lack, name)
			}
		}
	}
	o.lack = lack
}

func FindLackService(obj *Object) []string {
	visitor := NewFindLackServiceVisitor()
	obj.Accept(visitor)
	return visitor.(*FindLackServiceVisitor).lack
}
