package diceyml

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var yml = `version: 2.0

envs:
  TERMINUS_APP_NAME: PMP
  TERMINUS_TRACE_ENABLE: true
  TRACE_SAMPLE: 1

services:
  pmp-ui:
    ports:
    - 80
    expose:
    - 80
    deployments:
      replicas: 1
    depends_on:
    - pmp-backend
    resources:
      cpu: 0.1
      mem: 256
      health_check:
        exec:
          cmd: echo 1

  pmp-backend:
    deployments:
      replicas: 1
    ports:
    - 5080
    resources:
      cpu: 0.1
      mem: 512
      health_check:
        exec:
          cmd: echo 1

addons:
#  mysql:
#    plan: mysql:small
#    options:
#      create_dbs: pmp
  pmp-redis1:
    plan: redis:small
    image: redis:alpine
  pmp-zk:
    plan: zookeeper:medium
`

func TestDiceYmlObj(t *testing.T) {
	d, err := New([]byte(yml), false)
	assert.Nil(t, err)
	obj := d.Obj()
	assert.Equal(t, "zookeeper:medium", obj.AddOns["pmp-zk"].Plan)
}
