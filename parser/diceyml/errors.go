package diceyml

var (
	notfoundService  = errortype("not found service in yaml")
	invalidService   = errortype("invalid service defined in yaml")
	notfoundVersion  = errortype("not found version in yaml")
	invalidReplicas  = errortype("invalid replicas defined in yaml")
	invalidPolicy    = errortype("invalid policy defined in yaml")
	invalidCPU       = errortype("invalid cpu defined in yaml")
	invalidMaxCPU    = errortype("invalid max cpu defined in yaml")
	invalidMem       = errortype("invalid memory defined in yaml")
	invalidDisk      = errortype("invalid disk defined in yaml")
	invalidPort      = errortype("invalid port defined in yaml")
	invalidExpose    = errortype("invalid expose defined in yaml")
	invalidVolume    = errortype("invalid volume defined in yaml")
	invalidAddonPlan = errortype("invalid addon plan in yaml")
	invalidImage     = errortype("invalid image defined in yaml")
)

type errortype string

func (e errortype) Error() string {
	return string(e)
}
