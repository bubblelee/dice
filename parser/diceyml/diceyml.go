package diceyml

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

type EnvType int

const (
	BaseEnv EnvType = iota
	DevEnv
	TestEnv
	StagingEnv
	ProdEnv
)

func (e EnvType) String() string {
	switch e {
	case BaseEnv:
		return "base"
	case DevEnv:
		return "development"
	case TestEnv:
		return "test"
	case StagingEnv:
		return "staging"
	case ProdEnv:
		return "production"
	default:
		panic("should not be here!")
	}
}

type DiceYaml struct {
	data     []byte
	obj      *Object
	validate bool
}

// 当传的是某个环境的yml时，validate=false
func New(b []byte, validate bool) (*DiceYaml, error) {

	d := &DiceYaml{
		data:     b,
		validate: validate,
	}
	if err := d.Parse(validate); err != nil {
		return nil, err
	}
	return d, nil
}

func (d *DiceYaml) Parse(validate bool) error {
	var obj Object
	if err := yaml.Unmarshal(d.data, &obj); err != nil {
		return errors.Wrap(err, "fail to yaml unmarshal")
	}
	if validate {
		err := polishE(&obj)
		if err != nil {
			return err
		}
	}
	d.obj = &obj
	return nil
}

func (d *DiceYaml) Obj() *Object {
	return CopyObj(d.obj)
}

func (d *DiceYaml) MergeEnv(env string) error {
	if d.obj == nil {
		if err := d.Parse(false); err != nil {
			return err
		}
	}
	MergeEnv(d.obj, env)
	ExpandGlobalEnv(d.obj)
	if err := polishE(d.obj); err != nil {
		return err
	}
	return nil
}

func (d *DiceYaml) Compose(env string, yml *DiceYaml) error {
	if d.obj == nil {
		if err := d.Parse(false); err != nil {
			return err
		}
	}
	Compose(d.obj, yml.obj, env)
	if err := polishE(d.obj); err != nil {
		return err
	}
	return nil
}

func (d *DiceYaml) InsertImage(images map[string]string, envs map[string]map[string]string) error {
	if d.obj == nil {
		if err := d.Parse(false); err != nil {
			return err
		}
	}
	if err := InsertImage(d.obj, images, envs); err != nil {
		return err
	}
	if err := polishE(d.obj); err != nil {
		return err
	}

	return nil
}

func (d *DiceYaml) InsertAddonOptions(env EnvType, addon string, options map[string]string) error {
	if d.obj == nil {
		if err := d.Parse(false); err != nil {
			return err
		}
	}

	InsertAddonOptions(d.obj, env, addon, options)
	if err := polishE(d.obj); err != nil {
		return err
	}
	return nil
}

func (d *DiceYaml) JSON() (string, error) {
	b, err := json.Marshal(d.obj)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (d *DiceYaml) YAML() (string, error) {
	b, err := yaml.Marshal(d.obj)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func polish(obj *Object) []error {
	errs := []error{}
	errs = BasicValidate(obj)
	if len(errs) > 0 {
		return errs
	}
	lacks := FindLackService(obj)
	if len(lacks) > 0 {
		e := fmt.Errorf("found lacked services: %v\n\n", lacks)
		errs = append(errs, e)
		return errs
	}

	has, chain := FindCycle(obj)
	if has {
		e := fmt.Errorf("found cycle: %+v\n\n", chain)
		errs = append(errs, e)
		return errs
	}
	// 存在正常的情况下，某个service是orphan的
	// orphans := FindOrphan(obj)
	// if len(orphans) > 0 {
	// 	e := fmt.Errorf("found orphans: %+v\n\n", orphans)
	// 	errs = append(errs, e)
	// 	return errs
	// }
	return nil
}

func polishE(obj *Object) error {
	errs := polish(obj)
	if len(errs) > 0 {
		es := []string{}
		for _, e := range errs {
			es = append(es, e.Error())
		}
		s, err := prettyPrint(es)
		if err != nil {
			return err
		}
		return fmt.Errorf("%v", s)
	}
	return nil
}

func prettyPrint(v interface{}) (string, error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return "", err
	}
	return string(b), nil
}
