package migrate

import (
	"reflect"

	v2 "terminus.io/dice/dice/parser/diceyml"
	v1 "terminus.io/dice/dice/parser/migrate/define"
)

type BasicVisitor struct {
	v1.DefaultVisitor
	v2obj           *v2.Object
	currentEndpoint string
	currentService  string
	currentAddon    string
}

func NewBasicVisitor(v2obj *v2.Object) *BasicVisitor {
	return &BasicVisitor{
		v2obj: v2obj,
	}
}

func (o *BasicVisitor) VisitObject(v v1.DiceYmlVisitor, obj *v1.Object) {
	o.v2obj.Version = "2.0"
	overrideIfNotZero(obj.GlobalEnv, &o.v2obj.Envs)
}
func (o *BasicVisitor) VisitEndpoint(v v1.DiceYmlVisitor, obj *v1.Endpoint) {
	if o.currentEndpoint == "" {
		panic("currentEndpoint should not be empty")
	}
	overrideIfNotZero(obj.Cmd, &o.v2obj.Services[o.currentEndpoint].Cmd)
	overrideIfNotZero(obj.Image, &o.v2obj.Services[o.currentEndpoint].Image)
	overrideIfNotZero(obj.Ports, &o.v2obj.Services[o.currentEndpoint].Ports)
	overrideIfNotZero(obj.Environment, &o.v2obj.Services[o.currentEndpoint].Envs)
	overrideIfNotZero(obj.Hosts, &o.v2obj.Services[o.currentEndpoint].Hosts)
	overrideIfNotZero(obj.Volumes, &o.v2obj.Services[o.currentEndpoint].Volumes)
	overrideIfNotZero(obj.Scale, &o.v2obj.Services[o.currentEndpoint].Deployments.Replicas)
	if o.v2obj.Services[o.currentEndpoint].Deployments.Replicas == 0 {
		o.v2obj.Services[o.currentEndpoint].Deployments.Replicas = 1
	}
	overrideIfNotZero(obj.Depends, &o.v2obj.Services[o.currentEndpoint].DependsOn)
	o.v2obj.Services[o.currentEndpoint].Expose = []int{80, 443}
}
func (o *BasicVisitor) VisitEndpoints(v v1.DiceYmlVisitor, obj *v1.Endpoints) {
	if o.v2obj.Services == nil {
		o.v2obj.Services = map[string]*v2.Service{}
	}

	for k, v_ := range *obj {
		o.currentEndpoint = k
		if _, ok := o.v2obj.Services[o.currentEndpoint]; !ok {
			o.v2obj.Services[o.currentEndpoint] = new(v2.Service)
		}
		v_.Accept(v)
	}
	o.currentEndpoint = ""
}
func (o *BasicVisitor) VisitResources(v v1.DiceYmlVisitor, obj *v1.Resources) {
	if o.currentEndpoint != "" {
		overrideIfNotZero(obj.Cpu, &o.v2obj.Services[o.currentEndpoint].Resources.CPU)
		overrideIfNotZero(obj.Cpu, &o.v2obj.Services[o.currentEndpoint].Resources.MaxCPU)
		overrideIfNotZero(obj.Mem, &o.v2obj.Services[o.currentEndpoint].Resources.Mem)
		overrideIfNotZero(obj.Disk, &o.v2obj.Services[o.currentEndpoint].Resources.Disk)
	} else if o.currentService != "" {
		overrideIfNotZero(obj.Cpu, &o.v2obj.Services[o.currentService].Resources.CPU)
		overrideIfNotZero(obj.Cpu, &o.v2obj.Services[o.currentService].Resources.MaxCPU)
		overrideIfNotZero(obj.Mem, &o.v2obj.Services[o.currentService].Resources.Mem)
		overrideIfNotZero(obj.Disk, &o.v2obj.Services[o.currentService].Resources.Disk)
	}
}
func (o *BasicVisitor) VisitService(v v1.DiceYmlVisitor, obj *v1.Service) {
	if o.currentService == "" {
		panic("currentService should not be empty")
	}
	overrideIfNotZero(obj.Cmd, &o.v2obj.Services[o.currentService].Cmd)
	overrideIfNotZero(obj.Image, &o.v2obj.Services[o.currentService].Image)
	overrideIfNotZero(obj.Ports, &o.v2obj.Services[o.currentService].Ports)
	overrideIfNotZero(obj.Scale, &o.v2obj.Services[o.currentService].Deployments.Replicas)
	if o.v2obj.Services[o.currentService].Deployments.Replicas == 0 {
		o.v2obj.Services[o.currentService].Deployments.Replicas = 1
	}
	overrideIfNotZero(obj.Environment, &o.v2obj.Services[o.currentService].Envs)
	overrideIfNotZero(obj.Hosts, &o.v2obj.Services[o.currentService].Hosts)
	overrideIfNotZero(obj.Volumes, &o.v2obj.Services[o.currentService].Volumes)
	overrideIfNotZero(obj.Depends, &o.v2obj.Services[o.currentService].DependsOn)
}
func (o *BasicVisitor) VisitServices(v v1.DiceYmlVisitor, obj *v1.Services) {
	if o.v2obj.Services == nil {
		o.v2obj.Services = map[string]*v2.Service{}
	}
	for k, v_ := range *obj {
		o.currentService = k
		if _, ok := o.v2obj.Services[o.currentService]; !ok {
			o.v2obj.Services[o.currentService] = new(v2.Service)
		}
		v_.Accept(v)
	}
	o.currentService = ""
}
func (o *BasicVisitor) VisitAddon(v v1.DiceYmlVisitor, obj *v1.Addon) {
	if o.currentAddon == "" {
		panic("currentAddon should not be empty")
	}
	overrideIfNotZero(obj.Plan, &o.v2obj.AddOns[o.currentAddon].Plan)
	overrideIfNotZero(obj.As, &o.v2obj.AddOns[o.currentAddon].As)
	overrideIfNotZero(obj.Options, &o.v2obj.AddOns[o.currentAddon].Options)
	overrideIfNotZero(obj.Image, &o.v2obj.AddOns[o.currentAddon].Image)
}
func (o *BasicVisitor) VisitAddons(v v1.DiceYmlVisitor, obj *v1.Addons) {
	if o.v2obj.AddOns == nil {
		o.v2obj.AddOns = map[string]*v2.AddOn{}
	}
	for k, v_ := range *obj {
		o.currentAddon = k
		if _, ok := o.v2obj.AddOns[o.currentAddon]; !ok {
			o.v2obj.AddOns[o.currentAddon] = new(v2.AddOn)
		}
		v_.Accept(v)
	}
	o.currentAddon = ""
}

func isZero(v interface{}) bool {
	switch reflect.TypeOf(v).Kind() {
	case reflect.Int:
		return v.(int) == 0
	case reflect.Int64:
		return v.(int64) == 0
	case reflect.Float64:
		return v.(float64) == 0
	case reflect.String:
		return v.(string) == ""
	case reflect.Map:
		return reflect.ValueOf(v).Len() == 0
	case reflect.Slice:
		return reflect.ValueOf(v).Len() == 0
	default:
		panic("not support kind: " + reflect.TypeOf(v).Kind().String())
	}
}

func overrideIfNotZero(src, dst interface{}) {
	if isZero(src) {
		return
	}
	override(&src, dst)
}
