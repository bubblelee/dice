// 生成不同环境的各个yaml (development, test, staging, production)
package migrate

import (
	v2 "terminus.io/dice/dice/parser/diceyml"
	v1 "terminus.io/dice/dice/parser/migrate/define"

	yaml "gopkg.in/yaml.v2"
)

type GenOverridedVisitor struct {
	v1.DefaultVisitor
	basic       *v2.Object
	development *v2.Object
	test        *v2.Object
	staging     *v2.Object
	production  *v2.Object
}

func NewGenOverridedVisitor(basic, development, test, staging, production *v2.Object) *GenOverridedVisitor {
	return &GenOverridedVisitor{
		basic:       basic,
		development: development,
		test:        test,
		staging:     staging,
		production:  production,
	}
}

func (o *GenOverridedVisitor) VisitObject(v v1.DiceYmlVisitor, obj *v1.Object) {
	for k, v_ := range obj.Environment {
		var next *v2.Object
		if k == "development" {
			override(o.basic, o.development)
			next = o.development
		} else if k == "test" {
			override(o.basic, o.test)
			next = o.test
		} else if k == "staging" {
			override(o.basic, o.staging)
			next = o.staging
		} else if k == "production" {
			override(o.basic, o.production)
			next = o.production
		}
		if next != nil {
			basic := NewBasicVisitor(next)
			v_.Accept(basic)
		}
	}
}

// 不会改变 dst 原有的 field（如果src中没有相应field）
func override(src, dst interface{}) {
	r, err := yaml.Marshal(src)
	if err != nil {
		panic("override marshal")
	}
	if err := yaml.Unmarshal(r, dst); err != nil {
		panic("override unmarshal")
	}
}
