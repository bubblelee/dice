package migrate

import (
	"io"
	"io/ioutil"

	v2 "terminus.io/dice/dice/parser/diceyml"
	v1 "terminus.io/dice/dice/parser/migrate/define"

	yaml "gopkg.in/yaml.v2"
)

type MigrateResult struct {
	Base         v2.Object
	DevSplit     v2.Object
	TestSplit    v2.Object
	StagingSplit v2.Object
	ProdSplit    v2.Object
	Composed     v2.Object
}

func Migrate(r io.Reader) (MigrateResult, error) {
	content, _ := ioutil.ReadAll(r)
	var v v1.Object
	if err := yaml.Unmarshal(content, &v); err != nil {
		return MigrateResult{}, err
	}
	var basic, development_split, test_split, staging_split, production_split v2.Object
	var composed *v2.Object
	basicVisitor := NewBasicVisitor(&basic)
	v.Accept(basicVisitor)
	genOverridedVisitor2 := NewGenOverridedVisitor(&v2.Object{}, &development_split, &test_split, &staging_split, &production_split)
	v.Accept(genOverridedVisitor2) // 生成未合并的各个环境的 yml
	composed = v2.CopyObj(&basic)
	for k, obj := range map[string]*v2.Object{
		"development": &development_split,
		"test":        &test_split,
		"staging":     &staging_split,
		"production":  &production_split,
	} {
		v2.Compose(composed, obj, k)
	}

	modifyVolumesVisitor := NewModifyVolumeVisitor()
	for _, obj := range []v2.Object{basic, development_split, test_split, staging_split, production_split, *composed} {
		obj.Accept(modifyVolumesVisitor)
	}
	return MigrateResult{
		Base:         basic,
		DevSplit:     development_split,
		TestSplit:    test_split,
		StagingSplit: staging_split,
		ProdSplit:    production_split,
		Composed:     *composed,
	}, nil
}
