package define

// TODO: Questions
// 1. endpoints 有 hosts 吗
// 2. 哪些可选

// old version dice.yaml(1.0)
type Object struct {
	Name        string              `yaml:"name,omitempty"`
	Description string              `yaml:"description,omitempty"`
	Keywords    []string            `yaml:"keywords,omitempty"`
	Website     string              `yaml:"website,omitempty"`
	Repository  string              `yaml:"repository,omitempty"`
	Logo        string              `yaml:"logo,omitempty"`
	Scripts     map[string][]string `yaml:"scripts,omitempty"`
	Endpoints   Endpoints           `yaml:"endpoints,omitempty"`
	Services    Services            `yaml:"services,omitempty"`
	Addons      Addons              `yaml:"addons,omitempty"`
	GlobalEnv   map[string]string   `yaml:"global_env,omitempty"`
	EnvFiles    []string            `yaml:"env_files,omitempty"`
	Environment map[string]Object   `yaml:"environment,omitempty"`
}
type Endpoints map[string]*Endpoint
type Services map[string]*Service
type Addons map[string]*Addon
type Endpoint struct {
	Image   string   `yaml:"image,omitempty"`
	Cmd     string   `yaml:"cmd,omitempty"`
	Context string   `yaml:"context,omitempty"`
	Ports   []int    `yaml:"ports,omitempty"`
	Hosts   []string `yaml:"hosts,omitempty"`

	Scale       int               `yaml:"scale,omitempty"`
	Depends     []string          `yaml:"depends,omitempty"`
	Environment map[string]string `yaml:"environment,omitempty"`
	Resources   Resources         `yaml:"resources,omitempty"`
	Buildpack   Buildpack         `yaml:"buildpack,omitempty"`
	Volumes     []string          `yaml:"volumes,omitempty"`
}

type Resources struct {
	Cpu  float64 `yaml:"cpu,omitempty"`
	Mem  int     `yaml:"mem,omitempty"`
	Disk int     `yaml:"disk,omitempty"`
}

type Buildpack struct {
	BpRepo string   `yaml:"bp_repo,omitempty"`
	BpVer  string   `yaml:"bp_ver,omitempty"`
	BpArgs []string `yaml:"bp_args,omitempty"`
}

type Service struct {
	Image       string            `yaml:"image,omitempty"`
	Cmd         string            `yaml:"cmd,omitempty"`
	Context     string            `yaml:"context,omitempty"`
	ModulePath  string            `yaml:"module_path,omitempty"`
	Ports       []int             `yaml:"ports,omitempty"`
	Scale       int               `yaml:"scale,omitempty"`
	Depends     []string          `yaml:"depends,omitempty"`
	Environment map[string]string `yaml:"environment,omitempty"`
	Resources   Resources         `yaml:"resources,omitempty"`
	Hosts       []string          `yaml:"hosts,omitempty"`
	Buildpack   Buildpack         `yaml:"buildpack,omitempty"`
	Volumes     []string          `yaml:"volumes,omitempty"`
}

type Addon struct {
	Plan    string            `yaml:"plan,omitempty"`
	As      string            `yaml:"as,omitempty"`
	Options map[string]string `yaml:"options,omitempty"`
	Image   string            `yaml:"image,omitempty"`
}

type DiceYmlVisitor interface {
	VisitObject(v DiceYmlVisitor, obj *Object)
	VisitEndpoint(v DiceYmlVisitor, obj *Endpoint)
	VisitEndpoints(v DiceYmlVisitor, obj *Endpoints)
	VisitResources(v DiceYmlVisitor, obj *Resources)
	VisitBuildpack(v DiceYmlVisitor, obj *Buildpack)
	VisitService(v DiceYmlVisitor, obj *Service)
	VisitServices(v DiceYmlVisitor, obj *Services)
	VisitAddon(v DiceYmlVisitor, obj *Addon)
	VisitAddons(v DiceYmlVisitor, obj *Addons)
}

func (obj *Object) Accept(v DiceYmlVisitor) {
	obj.Endpoints.Accept(v)
	obj.Services.Accept(v)
	obj.Addons.Accept(v)
	v.VisitObject(v, obj)
}

func (obj *Endpoints) Accept(v DiceYmlVisitor) {
	v.VisitEndpoints(v, obj)
}
func (obj *Services) Accept(v DiceYmlVisitor) {
	v.VisitServices(v, obj)
}
func (obj *Addons) Accept(v DiceYmlVisitor) {
	v.VisitAddons(v, obj)
}
func (obj *Endpoint) Accept(v DiceYmlVisitor) {
	obj.Resources.Accept(v)
	obj.Buildpack.Accept(v)
	v.VisitEndpoint(v, obj)

}
func (obj *Resources) Accept(v DiceYmlVisitor) {
	v.VisitResources(v, obj)
}
func (obj *Buildpack) Accept(v DiceYmlVisitor) {
	v.VisitBuildpack(v, obj)
}
func (obj *Service) Accept(v DiceYmlVisitor) {
	obj.Resources.Accept(v)
	obj.Buildpack.Accept(v)
	v.VisitService(v, obj)
}
func (obj *Addon) Accept(v DiceYmlVisitor) {
	v.VisitAddon(v, obj)
}

type DefaultVisitor struct{}

func (*DefaultVisitor) VisitObject(v DiceYmlVisitor, obj *Object)       {}
func (*DefaultVisitor) VisitEndpoint(v DiceYmlVisitor, obj *Endpoint)   {}
func (*DefaultVisitor) VisitEndpoints(v DiceYmlVisitor, obj *Endpoints) {}
func (*DefaultVisitor) VisitResources(v DiceYmlVisitor, obj *Resources) {}
func (*DefaultVisitor) VisitBuildpack(v DiceYmlVisitor, obj *Buildpack) {}
func (*DefaultVisitor) VisitService(v DiceYmlVisitor, obj *Service)     {}
func (*DefaultVisitor) VisitServices(v DiceYmlVisitor, obj *Services)   {}
func (*DefaultVisitor) VisitAddon(v DiceYmlVisitor, obj *Addon)         {}
func (*DefaultVisitor) VisitAddons(v DiceYmlVisitor, obj *Addons)       {}

// global_env			-> envs
// endpoints:XXX		-> services:XXX
// endpoints:XXX:cmd		-> services:XXX:cmd
// endpoints:XXX:image		-> services:XXX:image
// endpoints:XXX:ports		-> services:XXX:ports
// endpoints:XXX:environment	-> services:XXX:envs
// endpoints:XXX:hosts		-> services:XXX:hosts
// endpoints:XXX:resources	-> services:XXX:resources
// endpoints:XXX:volumes	-> services:XXX:volumes
// None				-> services:XXX:deployments
// endpoints:XXX:scale		-> services:XXX:deployments:replicas
// None				-> services:XXX:deployments:policys
// services:YYY			-> services:XXX:depends_on:YYY
// None				-> services:XXX:expose (none if backend service)
// None				-> services:XXX:health_check
// addons:XXX			-> addons:XXX
// addons:XXX:plan		-> addons:XXX:plan
// addons:XXX:as		-> addons:XXX:as
// addons:XXX:options		-> addons:XXX:options
// addons:XXX:image		-> addons:XXX:image
