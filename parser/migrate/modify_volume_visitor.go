// v1 volume: host-path:container-path
// v2 volume: container-path

package migrate

import (
	"strings"

	v2 "terminus.io/dice/dice/parser/diceyml"
)

type ModifyVolumeVisitor struct {
	v2.DefaultVisitor
}

func NewModifyVolumeVisitor() v2.DiceYmlVisitor {
	return &ModifyVolumeVisitor{}
}

func (*ModifyVolumeVisitor) VisitService(v v2.DiceYmlVisitor, obj *v2.Service) {
	for i, vol := range obj.Volumes {
		paths := strings.Split(vol, ":")
		if len(paths) == 1 {
			return
		}
		if len(paths) == 2 {
			container := paths[1]
			obj.Volumes[i] = container
		}
	}
}

func (*ModifyVolumeVisitor) VisitServices(v v2.DiceYmlVisitor, obj *v2.Services) {
	for _, v_ := range *obj {
		v_.Accept(v)
	}
}
