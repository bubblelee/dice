package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/executor"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func (s *Server) epCreateRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	create := RuntimeCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&create); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest}, err
	}
	runtime := spec.Runtime(create)
	runtime.CreatedTime = time.Now().Unix()
	runtime.LastModifiedTime = runtime.CreatedTime
	if runtime.Namespace == "" {
		runtime.Namespace = "default"
	}
	runtime.Status = spec.StatusCreated

	// TODO: Authentication, authorization, admission control
	if !validateRuntimeName(runtime.Name) || !validateRuntimeNamespace(runtime.Namespace) {
		return HTTPResponse{
			Status: http.StatusUnprocessableEntity,
			Content: CreateRuntimeResponse{
				Name:  runtime.Name,
				Error: "invalid Name or Namespace",
			},
		}, nil
	}

	if err := s.store.Put(ctx, makeRuntimeKey(runtime.Namespace, runtime.Name), runtime); err != nil {
		return nil, err
	}

	// build match tags and exclude tags
	runtime.Labels = appendServiceTags(runtime.Labels)

	if _, err := s.handleRuntime(ctx, &runtime, TaskCreate); err != nil {
		return nil, err
	}

	// 不轮询最新状态，etcd只存储相应元数据信息及期望值
	// runtime最新状态是希望上层通过GET接口来查询

	return HTTPResponse{
		Status: http.StatusOK,
		Content: CreateRuntimeResponse{
			Name: runtime.Name,
		},
	}, nil

	//version := "TODO"
	// TODO: update version to etcd if version needed
}

func (s *Server) epUpdateRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// 根据全量更新来实现
	// upRuntime 从上层request body解析出，反映上层期望的runtime
	upRuntime := spec.Runtime{}
	if err := json.NewDecoder(r.Body).Decode(&upRuntime); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest}, err
	}

	name := vars["name"]
	namespace := vars["namespace"]

	if len(upRuntime.Services) == 0 || name != upRuntime.Name || namespace != upRuntime.Namespace {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: UpdateRuntimeResponse{
				Error: fmt.Sprintf("update request body services empty, or namespace/name not matched"),
			},
		}, nil
	}

	// oldRuntime 从etcd中获取，反映当前runtime
	oldRuntime := spec.Runtime{}
	if err := s.store.Get(ctx, makeRuntimeKey(namespace, name), &oldRuntime); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: UpdateRuntimeResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s/%s) from etcd, err: %v", namespace, name, err),
			},
		}, nil
	}

	// 清理 cache, 会触发一次新的部署
	s.l.Lock()
	_, ok := s.runtimeMap[oldRuntime.Namespace+"--"+oldRuntime.Name]
	if ok {
		delete(s.runtimeMap, oldRuntime.Namespace+"--"+oldRuntime.Name)
	}
	s.l.Unlock()

	diffAndPatchRuntime(&upRuntime, &oldRuntime)

	if err := s.store.Put(ctx, makeRuntimeKey(oldRuntime.Namespace, oldRuntime.Name), oldRuntime); err != nil {
		return nil, err
	}

	// build match tags and exclude tags
	oldRuntime.Labels = appendServiceTags(oldRuntime.Labels)

	if _, err := s.handleRuntime(ctx, &oldRuntime, TaskUpdate); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: UpdateRuntimeResponse{
			Name: oldRuntime.Name,
		},
	}, nil
}

// 暂时约定上层通过epUpdateRuntime来实现
func (s *Server) epUpdateService(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// TODO
	return nil, nil
}

func (s *Server) epRestartRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// imagine the request's body is empty
	name := vars["name"]
	namespace := vars["namespace"]
	runtime := spec.Runtime{}

	if err := s.store.Get(ctx, makeRuntimeKey(namespace, name), &runtime); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: RestartRuntimeResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s/%s) from etcd, err: %v", namespace, name, err),
			},
		}, nil
	}

	// 清理 cache, 会触发一次新的部署
	s.l.Lock()
	_, ok := s.runtimeMap[runtime.Namespace+"--"+runtime.Name]
	if ok {
		delete(s.runtimeMap, runtime.Namespace+"--"+runtime.Name)
	}
	s.l.Unlock()

	if runtime.Extra == nil {
		runtime.Extra = make(map[string]string)
	}
	runtime.Extra[LastRestartTime] = time.Now().String()

	if err := s.store.Put(ctx, makeRuntimeKey(runtime.Namespace, runtime.Name), runtime); err != nil {
		return nil, err
	}

	// build match tags and exclude tags
	runtime.Labels = appendServiceTags(runtime.Labels)

	if _, err := s.handleRuntime(ctx, &runtime, TaskUpdate); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: RestartRuntimeResponse{
			Name: runtime.Name,
		},
	}, nil
	return nil, nil
}

func (s *Server) epCancelAction(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// TODO
	return nil, nil
}

func (s *Server) epDeleteRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	runtime := spec.Runtime{}
	if err := s.store.Get(ctx, makeRuntimeKey(namespace, name), &runtime); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: DeleteRuntimeResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s/%s) from etcd, err: %v", namespace, name, err),
			},
		}, nil
	}
	if _, err := s.handleRuntime(ctx, &runtime, TaskDestroy); err != nil {
		return nil, err
	}
	if err := s.store.Remove(ctx, makeRuntimeKey(namespace, name), &runtime); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: DeleteRuntimeResponse{
			Name: name,
		},
	}, nil
}

func (s *Server) epGetRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]
	runtime := spec.Runtime{}

	if err := s.store.Get(ctx, makeRuntimeKey(namespace, name), &runtime); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s/%s) from etcd, err: %v", namespace, name, err),
			},
		}, nil
	}

	s.l.Lock()
	run, ok := s.runtimeMap[runtime.Namespace+"--"+runtime.Name]
	s.l.Unlock()
	if ok {
		return HTTPResponse{
			Status:  http.StatusOK,
			Content: *run,
		}, nil
	}

	result, err := s.handleRuntime(ctx, &runtime, TaskInspect)
	if err != nil {
		return nil, err
	}

	if result.extra == nil {
		err = errors.Errorf("Cannot get runtime(%v/%v) info from TaskInspect", runtime.Namespace, runtime.Name)
		logrus.Errorf(err.Error())

		// return empty response for testing
		return HTTPResponse{
			Status:  http.StatusOK,
			Content: spec.Runtime{},
		}, nil
	}
	newRuntime := result.extra.(*spec.Runtime)

	s.l.Lock()
	s.runtimeMap[runtime.Namespace+"--"+runtime.Name] = newRuntime
	s.l.Unlock()

	return HTTPResponse{
		Status:  http.StatusOK,
		Content: *newRuntime,
	}, nil
}

func (s *Server) handleRuntime(ctx context.Context, runtime *spec.Runtime, taskAction int) (TaskResponse, error) {
	var result TaskResponse

	if err := setRuntimeExecutorByCluster(runtime); err != nil {
		return result, err
	}

	// put the task in scheduler's buffered channel
	// handle the task in schduler's loop
	task, err := s.sched.Send(ctx, TaskRequest{
		ExecutorKind: getServiceExecutorKindByName(runtime.Executor),
		ExecutorName: runtime.Executor,
		Action:       taskAction,
		Spec:         *runtime,
	})
	if err != nil {
		return result, err
	}

	// get response from task's channel
	if result = task.Wait(ctx); result.Err() != nil {
		return result, result.Err()
	}

	return result, nil
}

// upRuntime may not be complete
func diffAndPatchRuntime(upRuntime *spec.Runtime, oldRuntime *spec.Runtime) {
	// generate LastModifiedTime according to current time
	oldRuntime.LastModifiedTime = time.Now().Unix()

	oldRuntime.Labels = upRuntime.Labels
	oldRuntime.ServiceDiscoveryKind = upRuntime.ServiceDiscoveryKind

	// TODO: refactor it, separate data and status into different etcd key
	// 全量更新
	oldRuntime.Services = upRuntime.Services
}

func appendServiceTags(labels map[string]string) map[string]string {
	matchTags := make([]string, 0)
	if labels["SERVICE_TYPE"] == "STATELESS" {
		matchTags = append(matchTags, spec.TagServiceStateless)
	} else if labels["SERVICE_TYPE"] == "STATEFUL" {
		matchTags = append(matchTags, spec.TagServiceStateful)
	}
	if labels == nil {
		labels = make(map[string]string)
	}
	labels[spec.LabelMatchTags] = strings.Join(matchTags, ",")
	labels[spec.LabelExcludeTags] = spec.TagLocked + "," + spec.TagPlatform
	return labels
}

// to suppress the error, to be the same with origin semantic
func getServiceExecutorKindByName(name string) string {
	e, err := executor.GetManager().Get(executortypes.Name(name))
	if err != nil {
		return conf.DefaultRuntimeExecutor
	}
	return string(e.Kind())
}

func (s *Server) epPrefixGetRuntime(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// e.g. "94-test-2478"
	prefix := vars["prefix"]
	var prefixKey string

	// TODO: FIX ME. 默认用services这个namespace
	if !strings.HasPrefix(prefix, "/dice/service/") {
		prefixKey = "/dice/service/services/" + prefix
	}

	return s.prefixGetRuntime(ctx, prefixKey)
}

func (s *Server) epPrefixGetRuntimeWithNamespace(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	// e.g. "services/94-test-2478"
	namespace := vars["namespace"]
	prefix := vars["prefix"]
	var prefixKey string

	if !strings.HasPrefix(prefix, "/dice/service/") {
		prefixKey = "/dice/service/" + namespace + "/" + prefix
	}

	return s.prefixGetRuntime(ctx, prefixKey)
}

func (s *Server) prefixGetRuntime(ctx context.Context, prefixKey string) (Responser, error) {
	kvs := make(map[string]interface{})
	runtime := &spec.Runtime{}

	saveKV := func(k string, v interface{}) error {
		kvs[k] = v
		return nil
	}

	if err := s.store.ForEach(ctx, prefixKey, spec.Runtime{}, saveKV); err != nil {
		return HTTPResponse{
			Status: http.StatusInternalServerError,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("Cannot get prefix(%s) from etcd, err: %v", prefixKey, err),
			},
		}, err
	}

	logrus.Debugf("prefixKey(%s) get kvs len: %v", prefixKey, len(kvs))

	if len(kvs) == 0 {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("not found for prefix(%s) from etcd", prefixKey),
			},
		}, errors.Errorf("not found for prefix(%s) from etcd, err: %v", prefixKey)
	}

	if len(kvs) > 1 {
		var keys []string
		for k := range kvs {
			keys = append(keys, k)
		}
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("more than one key matched for prefix(%s) from etcd, matched keys: %v", prefixKey, keys),
			},
		}, errors.Errorf("more than one key matched for prefix(%s) from etcd, matched keys: %v", prefixKey, keys)
	}

	// only one pair
	for _, v := range kvs {
		runtime = v.(*spec.Runtime)
	}

	//logrus.Debugf("get runtime: %+v", runtime)
	result, err := s.handleRuntime(ctx, runtime, TaskInspect)
	if err != nil {
		return nil, err
	}

	if result.extra == nil {
		err = errors.Errorf("get EMPTY runtimeinfo(%v/%v) from TaskInspect", runtime.Namespace, runtime.Name)
		logrus.Errorf(err.Error())

		return HTTPResponse{
			Status:  http.StatusOK,
			Content: spec.Runtime{},
		}, err
	}
	newRuntime := result.extra.(*spec.Runtime)

	return HTTPResponse{
		Status:  http.StatusOK,
		Content: *newRuntime,
	}, nil
}

func (s *Server) epGetRuntimeStatus(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]
	runtime := spec.Runtime{}

	if err := s.store.Get(ctx, makeRuntimeKey(namespace, name), &runtime); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s/%s) from etcd, err: %v", namespace, name, err),
			},
		}, nil
	}

	result, err := s.handleRuntime(ctx, &runtime, TaskInspect)
	if err != nil {
		return nil, err
	}

	multiStatus := spec.MultiLayerStatus{
		Namespace: namespace,
		Name:      name,
	}

	// 返回空的runtime
	if result.extra == nil {
		logrus.Errorf("got runtime(%v/%v) empty, executor: %s", runtime.Namespace, runtime.Name, runtime.Executor)
		return nil, errors.Errorf("got runtime(%v/%v) but found it empty", runtime.Namespace, runtime.Name)
	}

	newRuntime := result.extra.(*spec.Runtime)
	multiStatus.Status = string(newRuntime.Status)
	multiStatus.More = make(map[string]string)
	for _, service := range newRuntime.Services {
		multiStatus.More[service.Name] = string(service.Status)
	}

	return HTTPResponse{
		Status:  http.StatusOK,
		Content: multiStatus,
	}, nil
}
