package events

import (
	"context"
	"sync"

	"terminus.io/dice/dice/pkg/jsonstore"
)

const (
	// status_update_event 定义的状态
	KILLING     = "TASK_KILLING"
	KILLED      = "TASK_KILLED"
	RUNNING     = "TASK_RUNNING"
	FINISHED    = "TASK_FINISHED"
	FAILED      = "TASK_FAILED"
	STARTING    = "TASK_STARTING"
	STAGING     = "TASK_STAGING"
	DROPPED     = "TASK_DROPPED"
	UNKNOWN     = "TASK_UNKNOWN"
	UNREACHABLE = "TASK_UNREACHABLE"

	// health_status_changed_event 封装后的状态
	HEALTHY   = "Healthy"
	UNHEALTHY = "UnHealthy"

	WATCHED_DIR = "/dice/service/"

	// 调用eventbox时发送方名字的后缀, 用于区分发事件的阶段
	// 初始化executor阶段发送的事件
	SUFFIX_INIT = "_INIT"
	// 周期性补偿阶段发的事件
	SUFFIX_PERIOD = "_PERIOD"
	// 其他时期的普通时间到的事件
	SUFFIX_NORMAL = "_NORMAL"
	// 暂时为edas事件指定的前缀
	SUFFIX_EDAS = "_EDAS"

	// 事件类型
	// 计算得出的事件，对应SUFFIX_NORMAL
	EVENTS_INCR = "increment"
	// 初始化或者周期性补偿的事件，对应SUFFIX_INIT和SUFFIX_PERIOD
	EVENTS_TOTAL = "total"

	// console接受状态事件的地址
	CONSOLE_EVENT_ADDR = "http://open.dice.marathon.l4lb.thisdcos.directory:8081/api/events/runtime/status/actions/sync"
)

type GetExecutorJsonStoreFn func(string) (*sync.Map, error)
type GetExecutorLocalStoreFn func(string) (*sync.Map, error)
type SetCallbackFn func(string) error

type EventMgr struct {
	ctx context.Context
	// watch jsonstore的/dice/service/路径，同步到executor的本地缓存中
	// 带回调函数，用于计算增量状态事件
	store jsonstore.JsonStoreWithWatch
	// 记录所有executor的callback函数的map
	executorCbMap sync.Map
	// MemEtcdStore 不带回调函数，用于初始化和定期补偿事件
	MemEtcdStore jsonstore.JsonStore
}

// 塞到eventbox里的结构体
type RuntimeEvent struct {
	RuntimeName     string          `json:"runtimeName"`
	ServiceStatuses []ServiceStatus `json:"serviceStatuses,omitempty"`
	// 临时字段，标识对应的runtime是否被删除
	IsDeleted bool   `json:"isDeleted,omitempty"`
	EventType string `json:"eventType,omitempty"`
}

type ServiceStatus struct {
	ServiceName      string           `json:"serviceName"`
	ServiceStatus    string           `json:"serviceStatus,omitempty"`
	Replica          int              `json:"replica, omitempty"`
	InstanceStatuses []InstanceStatus `json:"instanceStatuses,omitempty"`
}

type InstanceStatus struct {
	ID             string `json:"id,omitempty"`
	Ip             string `json:"ip,omitempty"`
	InstanceStatus string `json:"instanceStatus,omitempty"`
	// 扩展字段
	Extra map[string]interface{} `json:"extra,omitempty"`
}

type EventLayer struct {
	InstanceId          string
	BelongedServiceName string
	BelongedRuntimeName string
}

type WindowStatus struct {
	key        string
	statusType string
}
