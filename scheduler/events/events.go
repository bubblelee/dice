package events

import (
	"context"
	"os"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/pkg/jsonstore/storetypes"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var eventMgr EventMgr

func GetEventManager() *EventMgr {
	return &eventMgr
}

func (m *EventMgr) RegisterSelf(name string, cb executortypes.EventCbFn) error {
	if _, ok := m.executorCbMap.Load(name); ok {
		return errors.Errorf("duplicate executor registering for executor(%s)", name)
	}

	logrus.Debugf("executor(%s) cb addr: %v", name, cb)
	m.executorCbMap.Store(name, cb)
	start := time.Now()
	defer func() {
		logrus.Infof("in RegisterSelf executor(%s) into events took %v", name, time.Since(start))
	}()
	return nil
}

func init() {
	start := time.Now()
	// TODO: need set timeout
	eventMgr.ctx = context.Background()
	eventMgr.executorCbMap = sync.Map{}

	allExecutorCallback := func(key string, value interface{}, t storetypes.ChangeType) {
		f := func(k, v interface{}) bool {
			v.(executortypes.EventCbFn)(key, value, t)
			return true
		}
		eventMgr.executorCbMap.Range(f)
	}
	option := jsonstore.UseMemEtcdStore(eventMgr.ctx, WATCHED_DIR, allExecutorCallback, spec.Runtime{})
	// events的init先于executor的注册
	memstore, err := jsonstore.New(option)
	if err != nil {
		logrus.Errorf("new jsonstore using memetcd err: %v", err)
		return
	}
	eventMgr.store = memstore.(jsonstore.JsonStoreWithWatch)

	globalEventOption := jsonstore.UseMemEtcdStore(eventMgr.ctx, WATCHED_DIR, nil, spec.Runtime{})
	globalEventStore, err := jsonstore.New(globalEventOption)
	if err != nil {
		logrus.Errorf("new jsonstore using memetcd for glolalStore err: %v", err)
		return
	}
	eventMgr.MemEtcdStore = globalEventStore

	defer func() {
		logrus.Infof("in package events init took %v", time.Since(start))
	}()
}

func getLayerInfoFromEvent(id string, eventType string) (*EventLayer, error) {
	err := errors.Errorf("event's taskId(%s) format error", id)

	// status_update_event的id(taskId)格式: runtimes_v1_services_staging-773_web.e622bf15-9300-11e8-ad54-70b3d5800001
	// health_status_changed_event的id(instanceId)格式: runtimes_v1_services_staging-790_web.marathon-0ad6d3ce-946c-11e8-ad54-70b3d5800001
	strs := strings.Split(id, ".")
	if len(strs) < 2 {
		return nil, err
	}
	var e EventLayer
	fields := strings.Split(strs[0], "_")
	if l := len(fields); l > 3 {
		if eventType == "health_status_changed_event" {
			healthID := strings.Replace(strs[1], "marathon-", "", 1)
			//if healthID == strs[1] {
			//	return nil, errors.Errorf("instanceId(%s) not began with marathon-", strs[1])
			//}
			e.InstanceId = healthID
		} else if eventType == "status_update_event" {
			e.InstanceId = strs[1]
		}
		e.BelongedServiceName = fields[l-1]
		e.BelongedRuntimeName = fields[l-3] + "/" + fields[l-2]
		return &e, nil
	}
	return nil, err
}

func HandleOneExecutorEvent(name string, ch chan *spec.StatusEvent, lstore *sync.Map, cb executortypes.EventCbFn) {
	logrus.Infof("in HandleOneExecutorEvent executor: %s, lstore addr: %p", name, lstore)

	evm := GetEventManager()

	if err := evm.RegisterSelf(name, cb); err != nil {
		logrus.Errorf("register executor failed, err: %v", err)
		return
	}

	if strings.Contains(name, "EDAS") {
		//handleEdasEvent(name, lstore)
		return
	}

	notifier, err := api.New(name+SUFFIX_NORMAL, nil)
	if err != nil {
		logrus.Errorf("executor(%s) call eventbox new api error: %v", name, err)
		return
	}

	eventAddr, ok := os.LookupEnv("CONSOLE_EVENT_ADDR")
	if !ok {
		eventAddr = CONSOLE_EVENT_ADDR
	}

	// cache window's keys
	var eventsInWindow []WindowStatus
	nowTime := time.Now()
	//windowTime := time.NewTimer(10 * time.Second)
	for {
		// 时间窗口 + 统计状态
		if time.Since(nowTime) >= 10*time.Second {
			nowTime = time.Now()
			//windowTime = time.NewTimer(10 * time.Second)

			logrus.Infof("executor(%s) 10 second timeout2", name)
			// firstly send event
			for _, k := range eventsInWindow {
				re, ok := lstore.Load(k.key)
				if !ok {
					logrus.Errorf("statistics and cannot get key(%s) from lstore, type: %v", k.key, k.statusType)
					continue
				}
				ev := re.(RuntimeEvent)
				computeRuntimeStatus(&ev)
				ev.EventType = EVENTS_INCR

				err = notifier.Send(ev, api.WithDest(map[string]interface{}{"HTTP": []string{eventAddr}}))
				if err != nil {
					logrus.Errorf("eventbox send api err: %v", err)
					continue
				}

				logrus.Infof("scheduler send an ev: %+v", ev)

				// 清理该事件中记录的KILLED/FINISHED/FAILED的实例，避免重复发送
				for i := range ev.ServiceStatuses {
					for j := len(ev.ServiceStatuses[i].InstanceStatuses) - 1; j >= 0; j-- {
						is := ev.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus
						if is == "Killed" || is == "Finished" || is == "Failed" {
							ev.ServiceStatuses[i].InstanceStatuses = append(ev.ServiceStatuses[i].InstanceStatuses[:j],
								ev.ServiceStatuses[i].InstanceStatuses[j+1:]...)
						}
					}
				}
				lstore.Store(k, ev)
			}
			// clear the window
			if len(eventsInWindow) == 0 {
				logrus.Debugf("empty window for event statistics of executor(%s)", name)
			} else {
				eventsInWindow = nil
			}
		}

		select {
		case e := <-ch:
			if e.Cluster != name {
				logrus.Infof("event's executor(%s) not matched this executor(%s)", e.Cluster, name)
				break
			}
			// 过滤KILLING/STARTING事件
			if e.Status == KILLING || e.Status == STARTING || e.Status == STAGING || e.Status == DROPPED {
				break
			}

			logrus.Infof("events from executor(%s): %+v", name, *e)

			var eKey string
			var err error
			if e.Type == "health_status_changed_event" {
				if eKey, err = handleHealthStatusChanged(e, lstore); err != nil {
					logrus.Errorf("executor(%s) handle health status changed event err: %v", name, err)
					break
				}
			} else if e.Type == "status_update_event" {
				if eKey, err = handleStausUpdateEvent(e, lstore); err != nil {
					logrus.Errorf("executor(%s) handle status update event err: %v", name, err)
					break
				}
			}

			// 记录该时间窗口内的事件，用于时间到期后发送某类状态事件
			evExisted := false
			for i, ev := range eventsInWindow {
				if ev.key == eKey {
					eventsInWindow[i].statusType = e.Status
					evExisted = true
					break
				}
			}
			if !evExisted {
				eventsInWindow = append(eventsInWindow, WindowStatus{eKey, e.Status})
			}
		default:
			time.Sleep(1 * time.Second)

		}
	}
}

func handleStausUpdateEvent(e *spec.StatusEvent, lstore *sync.Map) (string, error) {
	layer, err := getLayerInfoFromEvent(e.TaskId, e.Type)
	if err != nil {
		return "", errors.Errorf("got layer info from event error: %v", err)
	}
	eKey := layer.BelongedRuntimeName
	eInstanceId := layer.InstanceId
	srvName := layer.BelongedServiceName

	re, ok := lstore.Load(eKey)
	if !ok {
		return "", errors.Errorf("key(%s) not found in lstore, event type: %v, status: %s, drop it as not found in lstore", eKey, e.Type, e.Status)
	}

	run := re.(RuntimeEvent)
	logrus.Infof("eKey(%s) from lstore event struct: %+v, ev instance id: %s, status: %s", eKey, run, eInstanceId, e.Status)

	foundEvInSrv := false
	for i, srv := range run.ServiceStatuses {
		// 事件隶属于该service下的某个instance
		// logrus.Infof("srv.SericeName: %v, srvName: %v", srv.ServiceName, srvName)
		// 用户定义的service如果是大写，对应到marathon中去创建时候会被转化成小写
		if strings.ToLower(srv.ServiceName) == strings.ToLower(srvName) {
			foundEvInSrv = true
			foundInstance := false
			for j, instance := range srv.InstanceStatuses {
				if instance.ID == e.TaskId {
					foundInstance = true
					switch e.Status {
					case KILLED:
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Killed"

					case RUNNING:
						// 作为某些应用健康检查比较薄弱(如只检查端口)情况下的补偿
						if run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus != "Healthy" {
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Running"
						}

					case FINISHED:
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Finished"

					case FAILED:
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Failed"

					default:
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = e.Status
						logrus.Debugf("service(%s) instance(%s) has default status: %s",
							srv.ServiceName, instance.ID, e.Status)
					}

					run.ServiceStatuses[i].InstanceStatuses[j].Ip = e.IP
					break
				}
			}

			// instanceId 未记录在当前sevice的status中
			if !foundInstance {
				hasVacancy := false
				for j, instance := range run.ServiceStatuses[i].InstanceStatuses {
					if len(instance.ID) == 0 {

						switch e.Status {
						case RUNNING:
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Running"

						case FAILED:
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Failed"

						case FINISHED:
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Finished"

						case KILLED:
							// 正常情况下KILLING/KILLED的instance之前会被记录
							logrus.Errorf("event taskID(%s) not found in previous status but its status is %s", eInstanceId, e.Status)
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Killed"

						default:
							//continue
							run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = e.Status
							logrus.Debugf("service(%s) instance(%s) has other status: %s",
								srv.ServiceName, instance.ID, e.Status)
						}

						//run.ServiceStatuses[i].InstanceStatuses[j].ID = eInstanceId
						run.ServiceStatuses[i].InstanceStatuses[j].ID = e.TaskId
						run.ServiceStatuses[i].InstanceStatuses[j].Ip = e.IP
						hasVacancy = true
						break
					}
				}

				if !hasVacancy {
					var status string
					if e.Status == RUNNING {
						status = "Running"
					} else if e.Status == FAILED {
						status = "Failed"
					} else if e.Status == FINISHED {
						status = "Finished"
					} else if e.Status == KILLED {
						status = "Killed"
					}

					run.ServiceStatuses[i].InstanceStatuses = append(run.ServiceStatuses[i].InstanceStatuses,
						InstanceStatus{
							//ID:             eInstanceId,
							ID:             e.TaskId,
							InstanceStatus: status,
							Ip:             e.IP,
						})
				}
			}
			lstore.Store(eKey, run)
			break
		}
	}

	// 理论上该情况不会出现
	if !foundEvInSrv {
		return "", errors.Errorf("event taskId(%s) instance(%s) not found in runtime(%s)'s any service",
			e.TaskId, eInstanceId, run.RuntimeName)
	}
	return eKey, nil
}

func handleHealthStatusChanged(e *spec.StatusEvent, lstore *sync.Map) (string, error) {
	layer, err := getLayerInfoFromEvent(e.TaskId, e.Type)
	if err != nil {
		return "", errors.Errorf("got layer info from event error: %v", err)
	}
	eKey := layer.BelongedRuntimeName
	eInstanceId := layer.InstanceId
	srvName := layer.BelongedServiceName

	re, ok := lstore.Load(eKey)
	if !ok {
		return "", errors.Errorf("key(%s) not found in lstore, event type: %v, status: %v, drop it as not found in lstore", eKey, e.Type, e.Status)
	}

	run := re.(RuntimeEvent)
	logrus.Infof("eKey(%s) from lstore event struct: %+v, ev instance id: %s, status: %s", eKey, run, eInstanceId, e.Status)

	foundEvInSrv := false
	for i, srv := range run.ServiceStatuses {
		// 事件隶属于该service下的某个instance
		if strings.ToLower(srv.ServiceName) == strings.ToLower(srvName) {
			foundEvInSrv = true
			foundInstance := false
			for j, instance := range srv.InstanceStatuses {
				//if instance.ID == eInstanceId {
				if instance.ID == e.TaskId {
					foundInstance = true
					if e.Status == HEALTHY {
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Healthy"
					} else if e.Status == UNHEALTHY {
						run.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "UnHealthy"
					}
					break
				}
			}

			// instanceId 未记录在当前sevice的status中,理论上不会发生
			if !foundInstance {
				return "", errors.Errorf("healthy instance(%s) not found in service(%s)", e.TaskId, srvName)
			}
			lstore.Store(eKey, run)
			break
		}
	}

	// 理论上该情况不会出现
	if !foundEvInSrv {
		return "", errors.Errorf("event taskId(%s) not found in runtime(%s)'s any service",
			e.TaskId, run.RuntimeName)
	}
	return eKey, nil
}

func computeRuntimeStatus(e *RuntimeEvent) {
	logrus.Infof("event(%s) in computeRuntimeStatus", e.RuntimeName)
	for i, srv := range e.ServiceStatuses {
		readyReplica := 0
		healthyReplica := 0

		// 表明runtime被删除
		if e.IsDeleted {
			e.ServiceStatuses[i].ServiceStatus = "Deleted"
			continue
		}

		for _, instance := range srv.InstanceStatuses {
			if instance.InstanceStatus == "Running" {
				readyReplica++
			}
			if instance.InstanceStatus == "Healthy" {
				readyReplica++
				healthyReplica++
			}
		}
		logrus.Debugf("runtime(%s)'s service(%s) readyReplica: %v, srv.Replica: %v, healthyReplica: %v",
			e.RuntimeName, srv.ServiceName, readyReplica, srv.Replica, healthyReplica)

		if readyReplica == srv.Replica {
			e.ServiceStatuses[i].ServiceStatus = "Running"
			if healthyReplica == srv.Replica {
				e.ServiceStatuses[i].ServiceStatus = string(spec.StatusReady)
			}
		} else {
			// Progressing?
			e.ServiceStatuses[i].ServiceStatus = string(spec.StatusProgressing)
		}
	}
}
