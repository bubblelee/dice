package main

import (
	"terminus.io/dice/dice/scheduler/spec"
)

type JobCreateRequest spec.JobFromUser

type JobCreateResponse struct {
	Name  string   `json:"name"`
	Error string   `json:"error"`
	Job   spec.Job `json:"job"`
}

type JobStartResponse struct {
	Name  string   `json:"name"`
	Error string   `json:"error"`
	Job   spec.Job `json:"job"`
}

type JobStopResponse struct {
	Name  string `json:"name"`
	Error string `json:"error"`
}

type JobDeleteResponse struct {
	Name  string `json:"name"`
	Error string `json:"error"`
}

type JobBatchRequest struct {
	Names []string `json:"names"`
}

type JobBatchResponse struct {
	Names []string   `json:"names"`
	Error string     `json:"error"`
	Jobs  []spec.Job `json:"jobs"`
}
