package main

import "terminus.io/dice/dice/scheduler/spec"

type RuntimeCreateRequest spec.Runtime

type ActionResponse struct {
	Version string `json:"version"`
}

type CreateRuntimeResponse struct {
	ActionResponse
	Name  string `json:"name"`
	Error string `json:"error"`
}

type GetRuntimeErrorResponse struct {
	Error string `json:"error"`
}

type RestartRuntimeResponse struct {
	Name  string `json:"name"`
	Error string `json:"error"`
}

type UpdateRuntimeResponse struct {
	Name  string `json:"name"`
	Error string `json:"error"`
}

type DeleteRuntimeResponse struct {
	Name  string `json:"name"`
	Error string `json:"error"`
}

// TODO: modify me.
type ClusterInfo struct {
	// 集群名称, e.g. "terminus-y"
	Cluster string `json:"cluster,omitempty"`
	// executor名称, e.g. MARATHONFORTERMINUS
	Name string `json:"name,omitempty"`
	// executor类型，对应plugins，e.g. MARATHON, METRONOME, K8S, EDAS
	Kind string `json:"kind,omitempty"`

	// options可包含如下
	//"ADDR": "master.mesos/service/marathon",
	//"PREFIX": "/runtimes/v1",
	//"VERSION":"1.6.0",
	//"PUBLICIPGROUP":"external",
	//"ENABLETAG":"true",
	//"PRESERVEPROJECTS":"58"
	//"CA_CRT"
	//"CLIENT_CRT"
	//"CLIENT_KEY"

	Options map[string]string `json:"options,omitempty"`
}
