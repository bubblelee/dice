package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"

	"terminus.io/dice/dice/scheduler/spec"
)

func (s *Server) epCreatePlatform(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	sg := spec.ServiceGroup{}
	if err := json.NewDecoder(r.Body).Decode(&sg); err != nil {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: UpdateRuntimeResponse{
				Error: err.Error(),
			},
		}, nil
	}
	sg.CreatedTime = time.Now().Unix()
	sg.LastModifiedTime = sg.CreatedTime
	sg.Status = spec.StatusCreated

	if !validateRuntimeName(sg.Name) {
		return HTTPResponse{
			Status: http.StatusUnprocessableEntity,
			Content: CreateRuntimeResponse{
				Name:  sg.Name,
				Error: "invalid Name",
			},
		}, nil
	}

	if err := s.store.Put(ctx, makePlatformKey(sg.Name), sg); err != nil {
		return nil, err
	}

	// add "MATCH_TAGS":"platform"
	sg.Labels[spec.LabelMatchTags] = spec.TagPlatform

	rt := spec.Runtime(sg)
	if _, err := s.handleRuntime(ctx, &rt, TaskCreate); err != nil {
		return nil, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: CreateRuntimeResponse{
			Name: sg.Name,
		},
	}, nil
}

func (s *Server) epDeletePlatform(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]

	sg := spec.ServiceGroup{}
	if err := s.store.Get(ctx, makePlatformKey(name), &sg); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: DeleteRuntimeResponse{
				Error: fmt.Sprintf("Cannot get serviceGroup(%s) from etcd, err: %v", name, err),
			},
		}, nil
	}
	rt := spec.Runtime(sg)
	if _, err := s.handleRuntime(ctx, &rt, TaskDestroy); err != nil {
		logrus.Errorf("delete platform component(%s) failed", name)
		return nil, err
	}

	if err := s.store.Remove(ctx, makePlatformKey(name), &sg); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: DeleteRuntimeResponse{
			Name: name,
		},
	}, nil
}

func (s *Server) epGetPlatform(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	sg := spec.ServiceGroup{}

	if err := s.store.Get(ctx, makePlatformKey(name), &sg); err != nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("Cannot get runtime(%s) from etcd, err: %v", name, err),
			},
		}, nil
	}

	rt := spec.Runtime(sg)
	result, err := s.handleRuntime(ctx, &rt, TaskInspect)
	if err != nil {
		return nil, err
	}

	if result.extra == nil {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: GetRuntimeErrorResponse{
				Error: fmt.Sprintf("Cannot get runtime(%v) info from TaskInspect", name),
			},
		}, nil
	}
	sg = *(result.extra.(*spec.ServiceGroup))
	return HTTPResponse{
		Status:  http.StatusOK,
		Content: sg,
	}, nil

}

func (s *Server) epUpdatePlatform(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	sg := spec.ServiceGroup{}
	if err := json.NewDecoder(r.Body).Decode(&sg); err != nil {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: UpdateRuntimeResponse{
				Error: err.Error(),
			},
		}, nil
	}

	name := vars["name"]
	if name != sg.Name {
		return HTTPResponse{
			Status: http.StatusNotFound,
			Content: UpdateRuntimeResponse{
				Error: fmt.Sprintf("name not matched, http id(%s), req body name(%s)", name, sg.Name),
			},
		}, nil
	}

	// add "MATCH_TAGS":"platform"
	sg.Labels[spec.LabelMatchTags] = spec.TagPlatform
	sg.LastModifiedTime = time.Now().Unix()

	rt := spec.Runtime(sg)
	if err := s.store.Put(ctx, makePlatformKey(name), rt); err != nil {
		return nil, err
	}

	if _, err := s.handleRuntime(ctx, &rt, TaskUpdate); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: UpdateRuntimeResponse{
			Name: name,
		},
	}, nil

	return nil, nil
}

func (s *Server) epRestartPlatform(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return nil, nil
}
