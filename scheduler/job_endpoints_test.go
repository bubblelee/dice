package main

import (
	"context"
	"encoding/hex"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"

	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/bouk/monkey"
	"github.com/gavv/httpexpect"
	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestJob(t *testing.T) {
	os.Setenv("DEFAULT_JOB_EXECUTOR", "DEMO")
	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")
	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
		os.Unsetenv("DEFAULT_JOB_EXECUTOR")
	}()

	err := conf.Parse()
	assert.Nil(t, err)

	server := NewServer("")
	server.initEndpoints()

	testserver := httptest.NewServer(server.router)
	defer testserver.Close()

	e := httpexpect.New(t, testserver.URL)

	jobs := []JobCreateRequest{
		{
			Name:   "test-name",
			Image:  "ubuntu",
			Cmd:    "sleep 3600",
			CPU:    0.1,
			Memory: 1234,
			Binds: []spec.Bind{
				{
					ContainerPath: "/data",
					HostPath:      "/netdata/jobtest/data1",
					ReadOnly:      true,
				},
			},
		},
		{
			Name:   "test-name1",
			Image:  "ubuntu1",
			Cmd:    "sleep 3600",
			CPU:    0.1,
			Memory: 1234,
		},
		{
			Name:   "test-name2",
			Image:  "ubuntu2",
			Cmd:    "sleep 3600",
			CPU:    0.1,
			Memory: 1234,
		},
	}

	jobTmp := spec.Job{
		JobFromUser: spec.JobFromUser{
			Env: map[string]string{
				"testKey": "testValue",
			},
		},
	}

	for _, j := range jobs {
		// delete job
		e.DELETE("/v1/job/default/" + j.Name + "/delete").
			Expect().
			Status(http.StatusOK)

		// create job
		e.PUT("/v1/job/create").WithJSON(j).
			Expect().
			Status(http.StatusOK).
			JSON().Object().Value("name").Equal(j.Name)

		// get job
		e.GET("/v1/job/default/"+j.Name).
			Expect().
			Status(http.StatusOK).
			JSON().Object().
			ValueEqual("name", j.Name).
			ValueEqual("image", j.Image).
			ValueEqual("cmd", j.Cmd).
			ValueEqual("cpu", j.CPU).
			ValueEqual("memory", j.Memory)

		// start job
		e.POST("/v1/job/default/"+j.Name+"/start").WithJSON(jobTmp).
			Expect().
			Status(http.StatusOK).
			JSON().Object().
			ValueEqual("name", j.Name).
			Value("job").Object().
			ValueEqual("name", j.Name).
			ValueEqual("image", j.Image).
			ValueEqual("cmd", j.Cmd).
			ValueEqual("cpu", j.CPU).
			ValueEqual("memory", j.Memory).
			ValueEqual("status", spec.StatusRunning)

		// stop job
		e.POST("/v1/job/default/"+j.Name+"/stop").
			Expect().
			Status(http.StatusOK).
			JSON().Object().
			ValueEqual("name", j.Name)
	}

	// create job, but the job is existed.
	e.PUT("/v1/job/create").WithJSON(jobs[0]).
		Expect().
		Status(http.StatusOK)

	// list jobs
	e.GET("/v1/jobs/default").
		Expect().
		Status(http.StatusOK).
		JSON().Array().
		Length().Equal(3)

	// pipline jobs
	e.POST("/v1/jobs/default/pipeline").WithJSON([]string{"test-name", "test-name1", "test-name2"}).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ValueEqual("error", "").
		Value("jobs").Array().Length().Equal(3)

	// concurrent jobs
	e.POST("/v1/jobs/default/concurrent").WithJSON([]string{"test-name", "test-name1", "test-name2"}).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ValueEqual("error", "").
		Value("jobs").Array().Length().Equal(3)
}

func TestFetchJobStatus(t *testing.T) {
	err := conf.Parse()
	assert.Nil(t, err)

	server := NewServer("")
	server.initEndpoints()

	job := spec.Job{
		JobFromUser: spec.JobFromUser{
			Name:      "test",
			Image:     "ubuntu",
			Namespace: "default",
			Executor:  "notexist",
		},
	}

	_, err = server.fetchJobStatus(context.Background(), &job)
	assert.NotNil(t, err)
}

func TestAppendJobTags(t *testing.T) {
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "job",
		"EXCLUDE_TAGS": "locked,platform",
	}, appendJobTags(nil))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "job",
		"EXCLUDE_TAGS": "locked,platform",
	}, appendJobTags(map[string]string{}))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "job,pack",
		"EXCLUDE_TAGS": "locked,platform",
		"PACK":         "true",
	}, appendJobTags(map[string]string{
		"PACK": "true",
	}))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "job",
		"EXCLUDE_TAGS": "locked,platform",
		"PACK":         "false",
	}, appendJobTags(map[string]string{
		"PACK": "false",
	}))

	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "job",
		"EXCLUDE_TAGS": "nopack,locked,platform",
	}, appendJobTags(map[string]string{
		"EXCLUDE_TAGS": "nopack",
	}))
}

func Test_modifyLabelsWithJobKind(t *testing.T) {
	labels := map[string]string{
		"MATCH_TAGS":      "job",
		"EXCLUDE_TAGS":    "locked,platform",
		spec.LabelJobKind: "bigdata",
	}
	modifyLabelsWithJobKind(labels)
	expectedLabels := map[string]string{
		"MATCH_TAGS":      "job,bigdata",
		"EXCLUDE_TAGS":    "locked,platform",
		spec.LabelJobKind: "bigdata",
	}
	if !reflect.DeepEqual(labels, expectedLabels) {
		t.Errorf("Expected to get %v, but got %v", expectedLabels, labels)
	}
}

func TestPrepareJob(t *testing.T) {
	conf.Parse()
	UUID := uuid.UUID{0x6b, 0xa7, 0xb8, 0x10, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8}
	expecteStr := hex.EncodeToString(UUID.Bytes())
	monkey.Patch(uuid.NewV4, func() uuid.UUID { return UUID })
	job := spec.Job{
		JobFromUser: spec.JobFromUser{
			Name:      "test",
			Image:     "ubuntu",
			Namespace: "default",
			Executor:  "notexist",
			Env:       map[string]string{},
		},
	}
	prepareJob(&job.JobFromUser)
	if job.JobFromUser.ID != expecteStr {
		t.Errorf("Job ID is expected to be %v, but got %s", expecteStr, job.JobFromUser.ID)
	}

	if job.JobFromUser.Env[conf.TraceLogEnv] != expecteStr {
		t.Errorf("value of Env %s should be , but got %s", expecteStr, job.JobFromUser.Env[conf.TraceLogEnv])
	}
}

func TestPrepareJobIDExist(t *testing.T) {
	conf.Parse()
	UUIDStr := "6f47f5d42ba942eda7c5e18a7a4d1644"
	job := spec.Job{
		JobFromUser: spec.JobFromUser{
			ID:        UUIDStr,
			Name:      "test",
			Image:     "ubuntu",
			Namespace: "default",
			Executor:  "notexist",
			Env:       map[string]string{},
		},
	}
	prepareJob(&job.JobFromUser)
	if job.JobFromUser.ID != UUIDStr {
		t.Errorf("UUID of job is expected to be %s, but get %s", UUIDStr, job.JobFromUser.ID)
	}
}
