package main

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/pprof"
	"sync"
	"time"

	notifierapi "terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type Responser interface {
	GetStatus() int
	GetContent() interface{}
}

type HTTPResponse struct {
	Status  int
	Content interface{}
}

func (r HTTPResponse) GetStatus() int {
	return r.Status
}

func (r HTTPResponse) GetContent() interface{} {
	return r.Content
}

type Server struct {
	router     *mux.Router
	listenAddr string
	sched      *Sched
	store      jsonstore.JsonStore
	runtimeMap map[string]*spec.Runtime
	l          sync.Mutex
	notifier   notifierapi.Notifier
}

// NewServer creates a server instance, it be used to handle http request and offer
// a api gateway role.
func NewServer(addr string) *Server {
	option := jsonstore.UseMemEtcdStore(context.Background(), "/dice", nil, nil)
	store, err := jsonstore.New(option)
	if err != nil {
		panic(err)
	}
	executorConfigs, err := conf.LoadExecutorConfig(store)
	if err != nil {
		panic(err)
	}
	sched, err := NewSched(executorConfigs)

	if err != nil {
		panic(err)
	}
	notifier, err := notifierapi.New("dice-scheduler", nil)
	if err != nil {
		panic(err)
	}
	server := &Server{
		router:     mux.NewRouter(),
		listenAddr: addr,
		sched:      sched,
		store:      store,
		runtimeMap: make(map[string]*spec.Runtime),
		l:          sync.Mutex{},
		notifier:   notifier,
	}

	go server.clearMap()

	return server
}

func (s *Server) clearMap() {
	logrus.Infof("enter clearMap")
	for {
		select {
		case <-time.After(30 * time.Second):
			ks := make([]string, 0)
			for k := range s.runtimeMap {
				//delete(s.runtimeMap, k)
				ks = append(ks, k)
			}

			for _, k := range ks {
				s.l.Lock()
				delete(s.runtimeMap, k)
				s.l.Unlock()
			}
			//s.runtimeMap = make(map[string]*spec.Runtime)
		}
	}
}

// ListendAndServe boot the server to lisen and accept requests.
func (s *Server) ListenAndServe() error {
	s.initEndpoints()
	go getDCOSTokenAuthPeriodically()

	srv := &http.Server{
		Addr:              s.listenAddr,
		Handler:           s.router,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		ReadHeaderTimeout: 60 * time.Second,
	}
	err := srv.ListenAndServe()
	if err != nil {
		logrus.Errorf("failed to listen and serve: %s", err)
	}
	return err
}

// initEndpoints adds the all routes of endpoint.
func (s *Server) initEndpoints() {
	var endpoints = []struct {
		Path    string
		Method  string
		Handler func(context.Context, *http.Request, map[string]string) (Responser, error)
	}{
		// system endpoints
		{"/info", http.MethodGet, s.epInfo},

		// job endpoints
		{"/v1/job/create", http.MethodPut, s.epCreateJob},
		{"/v1/job/{namespace}/{name}/start", http.MethodPost, s.epStartJob},
		{"/v1/job/{namespace}/{name}/stop", http.MethodPost, s.epStopJob},
		{"/v1/job/{namespace}/{name}/delete", http.MethodDelete, s.epDeleteJob},
		{"/v1/job/{namespace}/{name}", http.MethodGet, s.epGetJob},
		{"/v1/jobs/{namespace}/pipeline", http.MethodPost, s.epJobPipeline},
		{"/v1/jobs/{namespace}/concurrent", http.MethodPost, s.epJobConcurrent},
		{"/v1/jobs/{namespace}", http.MethodGet, s.epListJob},
		{"/v1/jobs/flow/create", http.MethodPost, s.epJobFlowCreate},
		{"/v1/jobs/flow/{namespace}/{name}", http.MethodGet, s.epJobFlowStatus},
		{"/v1/jobs/flow/{namespace}/{name}/delete", http.MethodDelete, s.epJobFlowDelete},

		// service endpoints
		{"/v1/runtime/create", http.MethodPost, s.epCreateRuntime},
		{"/v1/runtime/{namespace}/{name}/update", http.MethodPut, s.epUpdateRuntime},
		{"/v1/runtime/{namespace}/{name}/service/{serviceName}/update", http.MethodPut, s.epUpdateService},
		{"/v1/runtime/{namespace}/{name}/restart", http.MethodPost, s.epRestartRuntime},
		{"/v1/runtime/{namespace}/{name}/cancel", http.MethodPost, s.epCancelAction},
		{"/v1/runtime/{namespace}/{name}/delete", http.MethodDelete, s.epDeleteRuntime},
		{"/v1/runtime/{namespace}/{name}", http.MethodGet, s.epGetRuntime},

		// runtimeinfo endpoints for display
		{"/v1/runtimeinfo/{prefix}", http.MethodGet, s.epPrefixGetRuntime},
		{"/v1/runtimeinfo/{namespace}/{prefix}", http.MethodGet, s.epPrefixGetRuntimeWithNamespace},
		{"/v1/runtimeinfo/status/{namespace}/{name}", http.MethodGet, s.epGetRuntimeStatus},

		// for notifying endpoints
		//{"/v1/notify/job/{namespace}/{name}", http.MethodGet, s.epGetJobStatusForNotify},

		// platform endpoints
		{"/platform", http.MethodPost, s.epCreatePlatform},
		{"/platform/{name}", http.MethodDelete, s.epDeletePlatform},
		{"/platform/{name}", http.MethodGet, s.epGetPlatform},
		{"/platform/{name}", http.MethodPut, s.epUpdatePlatform},
		{"/platform/{name}/actions/restart", http.MethodPost, s.epRestartPlatform},

		// 创建 dcos 集群(会创建对应的 marathon executor 及 metronome executor)
		// 或者 仅创建一个 executor
		{"/clusters", http.MethodPost, s.epCreateCluster},
		// 删除 cluster(删除所有隶属于该 cluster 的 executors)
		{"/clusters/{name}", http.MethodDelete, s.epDeleteCluster},
	}

	for _, ep := range endpoints {
		s.router.Path(ep.Path).Methods(ep.Method).HandlerFunc(s.internal(ep.Handler))
		logrus.Infof("added endpoint: %s %s", ep.Method, ep.Path)
	}

	subroute := s.router.PathPrefix("/debug/").Subrouter()
	subroute.HandleFunc("/pprof/profile", pprof.Profile)
	subroute.HandleFunc("/pprof/trace", pprof.Trace)
	subroute.HandleFunc("/pprof/block", pprof.Handler("block").ServeHTTP)
	subroute.HandleFunc("/pprof/heap", pprof.Handler("heap").ServeHTTP)
	subroute.HandleFunc("/pprof/goroutine", pprof.Handler("goroutine").ServeHTTP)
	subroute.HandleFunc("/pprof/threadcreate", pprof.Handler("threadcreate").ServeHTTP)
}

func (s *Server) internal(handler func(context.Context, *http.Request, map[string]string) (Responser, error)) http.HandlerFunc {
	pctx := context.Background()

	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithCancel(pctx)
		defer cancel()
		defer r.Body.Close()

		start := time.Now()
		logrus.Infof("start %s %s", r.Method, r.URL.String())
		defer func() {
			logrus.Infof("end %s %s (took %v)", r.Method, r.URL.String(), time.Since(start))
		}()

		response, err := handler(ctx, r, mux.Vars(r))
		if err != nil {
			logrus.Errorf("failed to handle request: %s (%v)", r.URL.String(), err)

			if response != nil {
				w.WriteHeader(response.GetStatus())
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}
			io.WriteString(w, err.Error())
			return
		}
		if response == nil || response.GetContent() == nil && response.GetStatus() != http.StatusNotFound {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.GetStatus())

		if err := json.NewEncoder(w).Encode(response.GetContent()); err != nil {
			logrus.Errorf("failed to send response: %s (%v)", r.URL.String(), err)
			return
		}
	}
}

// TODO
func (s *Server) epInfo(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	return HTTPResponse{
		Status:  http.StatusOK,
		Content: "todo",
	}, nil
}
