package main

import (
	"os"

	"terminus.io/dice/dice/pkg/dumpstack"
	_ "terminus.io/dice/dice/pkg/monitor"
	"terminus.io/dice/dice/pkg/version"
	"terminus.io/dice/dice/scheduler/conf"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.Infof(version.String())
	// parse configure base on env variables.
	if err := conf.Parse(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
	// control log's level.
	if conf.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05.000000000",
	})
	// open the function of dump stack
	dumpstack.Open()

	logrus.Infof("start the service and listen on address: \"%s\"", conf.ListenAddr)

	logrus.Errorf("[alert] starting scheduler instance")

	if err := NewServer(conf.ListenAddr).ListenAndServe(); err != nil {
		logrus.Error(err)
		os.Exit(2)
	}
}
