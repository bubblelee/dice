package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	// 集群配置信息在 etcd 中的路径前缀
	CLUSTER_PREFIX = "/dice/scheduler/configs/cluster/"
	// 标识 marathon executor 的后缀
	// e.g. /dice/scheduler/configs/cluster/terminus-y-service-marathon
	CLUSTER_MARATHON_SUFFIX = "-service-marathon"
	// 标识 metronome executor 的后缀
	// e.g. /dice/scheduler/configs/cluster/terminus-y-job-metronome
	CLUSTER_METRONOME_SUFFIX = "-job-metronome"
	// 标识 edas executor 的后缀
	// e.g. /dice/scheduler/configs/cluster/bsd-edas-service-edas
	CLUSTER_EDAS_SUFFIX = "-service-edas"
)

func (s *Server) epCreateCluster(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := ClusterInfo{}
	if err := json.NewDecoder(r.Body).Decode(&cluster); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest}, err
	}

	logrus.Infof("epCreateCluster clusterInfo: %+v", cluster)

	var content string
	create := func(key string, c ClusterInfo) (Responser, error) {
		if err := s.store.Put(ctx, key, c); err != nil {
			content = fmt.Sprintf("write key(%s) to etcd error: %v", key, err)
			return makeHttpResp(http.StatusInternalServerError, content, err)
		}

		logrus.Infof("cluster executor(%s) created successfully, content: %+v", c.Name, c)

		content = fmt.Sprintf("cluster executor(%s) created successfully, content: %+v", c.Name, c)
		return makeHttpResp(http.StatusOK, content, nil)
	}

	// 两种方式创建集群
	// 1, executorName + executorKind 方式, 创建一个executor
	if len(cluster.Name) > 0 && len(cluster.Kind) > 0 {
		path, err := getEtcdPath(cluster.Cluster, cluster.Kind)
		if err != nil {
			content = fmt.Sprintf("cluster(%s)'s Kind(%s) not matched", cluster.Name, cluster.Kind)
			return makeHttpResp(http.StatusBadRequest, content, nil)
		}

		return create(path, cluster)
	}

	// 2, 传入 cluster 方式，创建对应的 service executor 和 job executor, 只限于 dcos 集群
	if len(cluster.Cluster) == 0 {
		content := fmt.Sprintf("cluster id not set, cluster name: %s, cluster kind", cluster.Name, cluster.Kind)
		return makeHttpResp(http.StatusBadRequest, content, nil)
	}

	srv := cluster
	srv.Kind = "MARATHON"
	srv.Name = generateExecutorByCluster(cluster.Cluster, "MARATHON")
	dcosAddr := cluster.Options["ADDR"]
	srv.Options["ADDR"] = dcosAddr + "/service/marathon"

	resp, err := create(CLUSTER_PREFIX+cluster.Cluster+CLUSTER_MARATHON_SUFFIX, srv)
	if err != nil {
		return resp, err
	}

	job := cluster
	job.Kind = "METRONOME"
	job.Name = generateExecutorByCluster(cluster.Cluster, "METRONOME")
	job.Options["ADDR"] = dcosAddr + "/service/metronome"

	resp, err = create(CLUSTER_PREFIX+cluster.Cluster+CLUSTER_METRONOME_SUFFIX, job)
	if err != nil {
		return resp, err
	}

	content = fmt.Sprintf("cluster(%s) created both marathon and metronome executor successfully", cluster.Cluster)
	return makeHttpResp(http.StatusOK, content, nil)
}

func (s *Server) epDeleteCluster(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	cluster := ClusterInfo{}
	name := vars["name"]

	logrus.Infof("epDeleteCluster intends to delete cluster: %s", name)

	keys := make([]string, 0)
	findKeyBelongedToCluster := func(k string, v interface{}) error {
		c, ok := v.(*ClusterInfo)
		if !ok {
			logrus.Errorf("key(%s) related value(%+v) is not type of ClusterInfo", k, v)
			return nil
		}
		if c.Cluster == name {
			keys = append(keys, k)
		}
		return nil
	}

	if err := s.store.ForEach(ctx, CLUSTER_PREFIX, ClusterInfo{}, findKeyBelongedToCluster); err != nil {
		content := fmt.Sprintf("cluster(%s) range prefix(%s) from etcd got err: %v", name, CLUSTER_PREFIX, err)
		return makeHttpResp(http.StatusInternalServerError, content, err)
	}

	if len(keys) == 0 {
		content := fmt.Sprintf("Cannot find executor belonged to cluster(%s)", name)
		return makeHttpResp(http.StatusNotFound, content, nil)
	}

	for _, k := range keys {
		if err := s.store.Remove(ctx, k, &cluster); err != nil {
			content := fmt.Sprintf("delete cluster(%s)'s key(%s) from etcd error: %v", name, k, err)
			return makeHttpResp(http.StatusInternalServerError, content, err)
		}
		logrus.Info("delete cluster(%s)'s key(%s) successfully", name, k)
	}

	return makeHttpResp(http.StatusOK, fmt.Sprintf("cluster(%s) deleted successfully", name), nil)
}

func getEtcdPath(cluster string, executorType string) (string, error) {
	switch executorType {
	case "MARATHON":
		return CLUSTER_PREFIX + cluster + CLUSTER_MARATHON_SUFFIX, nil
	case "METRONOME":
		return CLUSTER_PREFIX + cluster + CLUSTER_METRONOME_SUFFIX, nil
	case "EDAS":
		return CLUSTER_PREFIX + cluster + CLUSTER_EDAS_SUFFIX, nil
	}
	return "", errors.Errorf("cluster(%s)'s executorType(%s) not matched", cluster, executorType)
}

func generateExecutorByCluster(cluster string, executorType string) string {
	//e.g. MARATHONFORTERIMINUYS, METRONOMEFORTERMINUS
	return executorType + "FOR" + strings.ToUpper(cluster)
}

func makeHttpResp(statusCode int, content string, err error) (HTTPResponse, error) {
	return HTTPResponse{
		Status:  statusCode,
		Content: content,
	}, err
}

// runtime 的 executor 为空时，根据 cluster 值设置 executor
func setRuntimeExecutorByCluster(runtime *spec.Runtime) error {
	if len(runtime.Executor) > 0 {
		return nil
	}
	if len(runtime.ClusterName) == 0 {
		return errors.Errorf("runtime(%s/%s) neither executor nor cluster is set", runtime.Namespace, runtime.Name)
	}

	runtime.Executor = generateExecutorByCluster(runtime.ClusterName, "MARATHON")

	logrus.Infof("generate executor(%s) for runtime(%s) in cluster(%s)", runtime.Executor, runtime.Name, runtime.ClusterName)
	return nil
}

// job 的 executor 为空时，根据 cluster 值设置 executor
func setJobExecutorByCluster(job *spec.Job) error {
	if len(job.Executor) > 0 {
		return nil
	}
	if len(job.ClusterName) == 0 {
		return errors.Errorf("job(%s/%s) neither executor nor cluster is set", job.Namespace, job.Name)
	}

	job.Executor = generateExecutorByCluster(job.ClusterName, "METRONOME")

	logrus.Infof("generate executor(%s) for job(%s) in cluster(%s)", job.Executor, job.Name, job.ClusterName)
	return nil
}
