package main

import (
	"fmt"
	"sort"
	"testing"

	"terminus.io/dice/dice/scheduler/spec"

	"github.com/stretchr/testify/assert"
)

func TestParseJobFlowPipeline(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Image:   "ubuntu",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: nil,
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name1",
				Image:   "ubuntu1",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name0"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name2",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name1"},
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	flows, err := server.parseJobFlow(jobs)
	assert.Nil(t, err)
	assert.Equal(t, 3, len(flows))

	for i, f := range flows {
		assert.Equal(t, 1, len(f))
		assert.Equal(t, fmt.Sprintf("test-name%d", i), f[0].Name)
	}
}

func TestParseJobFlowConcurrent(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name0",
				Image:  "ubuntu",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name1",
				Image:  "ubuntu1",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name2",
				Image:  "ubuntu2",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	flows, err := server.parseJobFlow(jobs)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(flows))
	assert.Equal(t, 3, len(flows[0]))

	names := []string{}
	for _, job := range flows[0] {
		names = append(names, job.Name)
	}
	sort.Strings(names)
	for i, name := range names {
		assert.Equal(t, name, jobs[i].Name)
	}
}

func TestParseJobFlowMix(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Depends: []string{"test-name1"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name1",
				Depends: []string{"test-name2", "test-name3"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name2",
				Depends: []string{"test-name3"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name: "test-name3",
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	flows, err := server.parseJobFlow(jobs)
	assert.Nil(t, err)
	assert.Equal(t, 4, len(flows))

	batch := flows[0]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name3", batch[0].Name)

	batch = flows[1]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name2", batch[0].Name)

	batch = flows[2]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name1", batch[0].Name)

	batch = flows[3]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name0", batch[0].Name)
}

func TestParseJobFlowMix1(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Depends: []string{"test-name1"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name1",
				Depends: []string{"test-name3"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name2",
				Depends: []string{"test-name3"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name: "test-name3",
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	flows, err := server.parseJobFlow(jobs)
	assert.Nil(t, err)
	assert.Equal(t, 3, len(flows))

	batch := flows[0]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name3", batch[0].Name)

	batch = flows[1]
	assert.Equal(t, 2, len(batch))
	tmp := []string{batch[0].Name, batch[1].Name}
	sort.Strings(tmp)
	assert.Equal(t, "test-name1", tmp[0])
	assert.Equal(t, "test-name2", tmp[1])

	batch = flows[2]
	assert.Equal(t, 1, len(batch))
	assert.Equal(t, "test-name0", batch[0].Name)
}
func TestParseJobFlowDependSelf(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Image:   "ubuntu",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name0"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name1",
				Image:  "ubuntu1",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name2",
				Image:  "ubuntu2",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	_, err := server.parseJobFlow(jobs)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "dead loop")
}

func TestParseJobFlowDeadloop(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Image:   "ubuntu",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name2"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name1",
				Image:   "ubuntu1",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name0"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name2",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name1"},
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	_, err := server.parseJobFlow(jobs)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "dead loop")
}

func TestParseJobFlowDependNotfound(t *testing.T) {
	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:    "test-name0",
				Image:   "ubuntu",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"test-name2", "notexist"},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name1",
				Image:  "ubuntu1",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name2",
				Image:  "ubuntu2",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		},
	}

	server := NewServer("")
	server.initEndpoints()

	_, err := server.parseJobFlow(jobs)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "not found job")
}
