package main

import (
	"context"
	"fmt"

	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
)

const (
	TaskCreate = iota
	TaskDestroy
	TaskStatus
	TaskRemove
	TaskUpdate
	TaskInspect
)

type TaskRequest struct {
	Spec         interface{}
	ExecutorKind string
	ExecutorName string
	Action       int
}

type TaskResponse struct {
	err   error
	desc  spec.StatusDesc
	extra interface{}
}

func (tr *TaskResponse) Err() error {
	return tr.err
}

func (tr *TaskResponse) Status() spec.StatusDesc {
	return tr.desc
}

type Result interface {
	Wait(ctx context.Context) TaskResponse
}

type Task struct {
	TaskRequest
	ctx      context.Context
	executor executortypes.Executor
	c        chan TaskResponse
}

func (t *Task) Wait(ctx context.Context) TaskResponse {
	select {
	case resp := <-t.c:
		return resp
	case <-ctx.Done():
		return TaskResponse{
			err: ctx.Err(),
		}
	}
}

func (t *Task) Run(ctx context.Context) TaskResponse {
	executor := t.executor
	if executor == nil {
		return TaskResponse{
			err: errors.New("not found executor"),
		}
	}

	switch t.Action {
	case TaskCreate:
		var (
			resp interface{}
			err  error
		)
		if resp, err = executor.Create(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}
		return TaskResponse{
			extra: resp,
			err:   err,
		}
	case TaskUpdate:
		var (
			resp interface{}
			err  error
		)
		if resp, err = executor.Update(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}
		return TaskResponse{
			extra: resp,
			err:   err,
		}
	case TaskDestroy:
		if err := executor.Destroy(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}
	case TaskRemove:
		if err := executor.Remove(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}
	case TaskStatus:
		var (
			desc spec.StatusDesc
			err  error
		)
		if desc, err = executor.Status(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}

		return TaskResponse{
			err:  err,
			desc: desc,
		}
	case TaskInspect:
		var (
			resp interface{}
			err  error
		)
		if resp, err = executor.Inspect(ctx, t.Spec); err != nil {
			return TaskResponse{
				err: err,
			}
		}
		return TaskResponse{
			extra: resp,
			err:   err,
		}
	default:
		return TaskResponse{
			err: errors.Errorf("invlaid action: %d", t.Action),
		}
	}

	return TaskResponse{}
}

func (t *Task) String() string {
	return fmt.Sprintf("executor %s/%s (action: %d)", t.ExecutorKind, t.ExecutorName, t.Action)
}
