package spec

// Job defines a Job.
type Job struct {
	CreatedTime    int64       `json:"created_time"`
	LastStartTime  int64       `json:"last_start_time"`
	LastFinishTime int64       `json:"last_finish_time"`
	LastModify     string      `json:"last_modify"`
	Result         interface{} `json:"result,omitempty"`
	JobFromUser
	StatusDesc
}

type JobFromUser struct {
	Name         string            `json:"name"`
	ID           string            `json:"id,omitempty"` // if Job has owner, e.g. jobflow, it's ID can be specified.
	CallBackUrls []string          `json:"callbackurls,omitempty"`
	Image        string            `json:"image"`
	Cmd          string            `json:"cmd"`
	CPU          float64           `json:"cpu"`
	Memory       float64           `json:"memory"`
	Namespace    string            `json:"namespace"` // the default namespace is "default"
	Labels       map[string]string `json:"labels,omitempty"`
	Extra        map[string]string `json:"extra,omitempty"`
	Env          map[string]string `json:"env,omitempty"`
	Binds        []Bind            `json:"binds,omitempty"`
	Executor     string            `json:"executor"`
	ClusterName  string            `json:"clusterName"`
	Depends      []string          `json:"depends,omitempty"` // JobName
}
