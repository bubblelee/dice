package spec

type StatusCode string

const (
	// 底层调度器异常，或者容器异常导致无法拿到 status
	StatusError  StatusCode = "Error"
	StatusUnkonw StatusCode = "Unknown"

	StatusCreated StatusCode = "Created"
	StatusStopped StatusCode = "Stopped"

	// StatusUnschedulable means that the scheduler can't schedule the job right now.
	// for example:
	// 1. due to resources;
	// 2. due to scheduler inactive (chronos / metronome..)
	StatusUnschedulable   StatusCode = "Unschedulable"
	StatusRunning         StatusCode = "Running"
	StatusStoppedOnOK     StatusCode = "StoppedOnOK"
	StatusStoppedOnFailed StatusCode = "StoppedOnFailed"
	StatusStoppedByKilled StatusCode = "StoppedByKilled"

	// status only for services, TODO: refactor or union with jobs
	StatusReady       StatusCode = "Ready"
	StatusProgressing StatusCode = "Progressing"
	StatusFailing     StatusCode = "Failing"
	// 服务创建过程中出错，系统清理并删除了runtime
	StatusErrorAndDeleted StatusCode = "Error"
)

const (
	LabelMatchTags   = "MATCH_TAGS"
	LabelExcludeTags = "EXCLUDE_TAGS"
	LabelPack        = "PACK"
	LabelJobKind     = "JOB_KIND"
)

const (
	TagAny              = "any"
	TagLocked           = "locked"
	TagPlatform         = "platform"
	TagPack             = "pack"
	TagJob              = "job"
	TagServiceStateless = "service-stateless"
	TagServiceStateful  = "service-stateful"
	TagProjectPrefix    = "project-" // is a prefix, dynamic tag, e.g. project-41
	TagStageDev         = "stage-dev"
	TagStageTest        = "stage-test"
	TagStageStaging     = "stage-staging"
	TagStageProd        = "stage-prod"
	TagWorkspacePrefix  = "workspace-"
)

type StatusDesc struct {
	Status      StatusCode `json:"status"`
	LastMessage string     `json:"last_message,omitempty"`
}

type Bind struct {
	ContainerPath string `json:"containerPath"`
	HostPath      string `json:"hostPath"`

	// Optional: Defaults to false (read/write)
	ReadOnly bool `json:"readOnly,omitempty"`
}
