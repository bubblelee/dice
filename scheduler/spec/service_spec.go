package spec

// The Runtime of a Dice
type Runtime struct {
	// runtime create time
	CreatedTime int64 `json:"created_time"`
	// last modified (update) time
	LastModifiedTime int64 `json:"last_modified_time"`
	// executor for scheduling (e.g. marathon)
	Executor string `json:"executor"`
	// substitute for "Executor" field
	ClusterName string `json:"clusterName"`
	// version to tracing changes (create, update, etc.)
	Version string `json:"version,omitempty"`
	Force   bool   `json:"force,omitempty"`
	// current usage for Extra:
	// 1. record last restart time, to implement RESTART api through PUT api
	Extra map[string]string `json:"extra,omitempty"`
	// ubiquitous dice
	Dice
	// status of this runtime
	StatusDesc
}

// Ubiquitous dice entity (we call it dice.json)
type Dice struct {
	// name of dice, namespace + name is unique
	Name string `json:"name"`
	// namespace of dice, namespace + name is unique
	Namespace string `json:"namespace"`
	// labels for extension and some tags
	Labels map[string]string `json:"labels"`
	// bunch of services running together with dependencies each other
	Services []Service `json:"services"`
	// service discovery kind: VIP, PROXY, NONE
	ServiceDiscoveryKind string `json:"serviceDiscoveryKind"`
	// Defines the way dice do env injection.
	//
	// GLOBAL:
	//   each service can see every services
	// DEPEND:
	//   each service can see what he depends (XXX_HOST, XXX_PORT)
	ServiceDiscoveryMode string `json:"serviceDiscoveryMode,omitempty"`
}

// One single Service which is the minimum scheduling unit
type Service struct {
	// unique name between services in one Dice (Runtime)
	Name string `json:"name"`
	// namespace of service, equal to the namespace in Dice
	Namespace string `json:"namespace,omitempty"`
	// docker's image url
	Image string `json:"image"`
	// docker's CMD
	Cmd string `json:"cmd,omitempty"`
	// port list user-defined, we export these ports on our VIP
	Ports []int `json:"ports"`
	// only exists if serviceDiscoveryKind is PROXY
	// can not modify directly, assigned by dice
	ProxyPorts []int `json:"proxyPorts,omitempty"`
	// virtual ip
	// can not modify directly, assigned by dice
	Vip string `json:"vip"`
	// only exists if serviceDiscoveryKind is PROXY
	// can not modify directly, assigned by dice
	ProxyIp string `json:"proxyIp,omitempty"`
	// TODO: refactor it, currently only work with label X_ENABLE_PUBLIC_IP=true
	PublicIp string `json:"publicIp,omitempty"`
	// instances of containers should running
	Scale int `json:"scale"`
	// resources like cpu, mem, disk
	Resources Resources `json:"resources"`
	// list of service names depends by this service, used for dependency scheduling
	Depends []string `json:"depends,omitempty"`
	// environment variables inject into container
	Env map[string]string `json:"env"`
	// labels for extension and some tags
	Labels map[string]string `json:"labels"`
	// disk bind (mount) configuration, hostPath only
	Binds []ServiceBind `json:"binds,omitempty"`
	// Volumes intends to replace Binds
	Volumes []Volume `json:"volumes,omitempty"`
	// hosts append into /etc/hosts
	Hosts []string `json:"hosts,omitempty"`
	// health check
	HealthCheck *HealthCheck `json:"healthCheck"`
	//
	NewHealthCheck *NewHealthCheck `json:"health_check,omitempty"`
	// instance info, only for display
	// marathon 中对应一个task, k8s中对应一个pod
	InstanceInfos []InstanceInfo `json:"instanceInfos,omitempty"`
	// TODO: status should not show in Service spec, Service spec should only contains static description
	StatusDesc
}

// resources that container used
type Resources struct {
	// cpu sharing
	Cpu float64 `json:"cpu"`
	// memory usage
	Mem float64 `json:"mem"`
	// disk usage
	Disk float64 `json:"disk"`
}

// health check to check container healthy
type HealthCheck struct {
	// healthCheck kinds: HTTP, HTTPS, TCP, COMMAND
	Kind string `json:"kind,omitempty"`
	// port for HTTP, HTTPS, TCP
	Port int `json:"port,omitempty"`
	// path for HTTP, HTTPS
	Path string `json:"path,omitempty"`
	// command for COMMAND
	Command string `json:"command,omitempty"`
}

type ServiceBind struct {
	Bind
	// TODO: refactor it, currently just copy the marathon struct
	Persistent *PersistentVolume `json:"persistent,omitempty"`
}

type PersistentVolume struct {
	Type        string     `json:"type,omitempty"`
	Size        int        `json:"size,omitempty"`
	MaxSize     int        `json:"maxSize,omitempty"`
	Constraints [][]string `json:"constraints,omitempty"`
}

type HttpHealthCheck struct {
	Port int    `json:"port,omitempty"`
	Path string `json:"path,omitempty"`
	//单位是秒
	Duration int `json:"duration,omitempty"`
}

type ExecHealthCheck struct {
	Cmd string `json:"cmd,omitempty"`
	//单位是秒
	Duration int `json:"duration,omitempty"`
}

// 暂定支持"HTTP" 和 "COMMAND"两种方式
type NewHealthCheck struct {
	HttpHealthCheck *HttpHealthCheck `json:"http,omitempty"`
	ExecHealthCheck *ExecHealthCheck `json:"exec,omitempty"`
}

//hostPath使用Bind来实现，除hostPath外的使用volume
type Volume struct {
	// 支持云盘, 本地盘，其类型值分别为cloud, local
	// 当前marahon支持以上两种，而k8s只支持云盘
	VolumeType string `json:"volumeType,omitempty"`

	// 非hostPath类型卷需要指定卷的大小，格式请遵循"20Gi", "512Mi"等
	Storage string `json:"storage,omitempty"`

	// 挂载到容器中的卷路径
	ContainerPath string `json:"containerPath"`

	// --- START: k8s only ---
	// 以下字段暂不需要设置或修改，如果需要修改，提前沟通
	// 依赖阿里k8s服务的storageclass实现
	// 参考 https://help.aliyun.com/document_detail/63955.html?spm=a2c4g.11186623.6.632.OP015I

	// 当指定使用云盘并使用k8s来创建时，选择云盘类型，
	// 可选项包括为：cloud, cloud_efficiency, cloud_ssd, available
	// 其中available 会对高效、SSD、普通云盘依次尝试创建，直到创建成功
	// 当前默认使用cloud
	CloudType string `json:"cloudType,omitempty"`

	// 创建storageclass时，选择region，目前已知的可选项：cn-hangzhou
	// 当前默认不设置
	// Regionid string `json:"regionid,omitempty"`

	// 创建storageclass时，选择zone，目前已知的可选项：cn-hangzhou-b
	// 当前默认不设置
	// Zoneid string `json:"zoneid,omitempty"`
	// --- END: k8s only ---
}

type InstanceInfo struct {
	Id     string `json:"id,omitempty"`
	Status string `json:"status,omitempty"`
	Ip     string `json:"ip,omitempty"`
}

type MultiLayerStatus struct {
	// runtime namespace
	Namespace string `json:"namespace"`
	// runtime name
	Name string `json:"name"`
	// runtime status
	Status string `json:"status,omitempty"`
	// 扩展字段，比如存储runtime下每个服务的名字及状态
	More map[string]string `json:"more,omitempty"`
}

type JobStatusNotify struct {
	// runtime namespace
	Namespace string `json:"namespace"`
	// runtime name
	Name string `json:"name"`
	// the "nofity" get the status and post it to this url
	Addrs []string `json:"addrs"`
	// runtime status
	Status string `json:"status,omitempty"`
	// 扩展字段，比如存储runtime下每个服务的名字及状态
	More map[string]string `json:"more,omitempty"`
}

type StatusEvent struct {
	Type string `json:"type"`
	// ID由{runtimeName}.{serviceName}.{dockerID}生成
	ID      string `json:"id,omitempty"`
	IP      string `json:"ip,omitempty"`
	Status  string `json:"status"`
	TaskId  string `json:"taskId,omitempty"`
	Cluster string `json:"cluster,omitempty"`
}

type ServiceGroup Runtime
