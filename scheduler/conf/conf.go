package conf

import (
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

const (
	executorPrefix = "EXECUTOR_"
)

var (
	// Debug control the log's level.
	Debug bool

	// set the goroutine pool's size
	PoolSize int

	// ListenAddr specify the server's listen address, eg: ":9091"
	ListenAddr string

	// DefaultJobExecutor defines the default setting for job.
	DefaultJobExecutor string

	DefaultRuntimeExecutor string

	// TraceLogEnv shows the key of environment variable defined for tracing log
	TraceLogEnv string
)

// Get returns the value of a env variable as string.
func Get(k string) string {
	return os.Getenv(k)
}

// GetBool returns the value of a env variable as bool.
func GetBool(k string) bool {
	v := Get(k)
	v = strings.ToLower(v)
	return v == "true" || v == "1"
}

// GetInt returns the value of a env variable as int.
func GetInt(k string) (int, error) {
	v := Get(k)

	if v == "" {
		return 0, errors.Errorf("invalid config: %s=%s", k, v)
	}
	n, err := strconv.Atoi(v)
	if err != nil {
		return 0, errors.Wrapf(err, "invalid config: %s=%s", k, v)
	}
	return n, nil
}

// GetInt64 returns the value of a env variable as int64.
func GetInt64(k string) (int64, error) {
	v := Get(k)

	if v == "" {
		return 0, errors.Errorf("invalid config: %s=%s", k, v)
	}
	n, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return 0, errors.Wrapf(err, "invalid config: %s=%s", k, v)
	}
	return n, nil
}

// Parse parse the configure option from env variables.
func Parse() error {
	var err error

	Debug = parseDebug()

	if ListenAddr, err = parseListenAddr(); err != nil {
		return err
	}
	if DefaultJobExecutor, err = pareseDefaultJobExecutor(); err != nil {
		return err
	}
	if DefaultRuntimeExecutor, err = pareseDefaultRuntimeExecutor(); err != nil {
		return err
	}
	PoolSize = parsePoolSize()

	TraceLogEnv = parseTraceLogEnv()

	return nil
}

func parseTraceLogEnv() string {
	v := Get("TRACELOGENV")
	if v == "" {
		v = "TERMINUS_DEFINE_TAG"
	}
	return v
}

func parseDebug() bool {
	return GetBool("DEBUG")
}

func parseListenAddr() (string, error) {
	v := Get("LISTEN_ADDR")
	if v == "" {
		v = ":9091"
	}
	return v, nil
}

func pareseDefaultJobExecutor() (string, error) {
	v := Get("DEFAULT_JOB_EXECUTOR")
	if v == "" {
		v = "CHRONOS"
	}
	return v, nil
}

func pareseDefaultRuntimeExecutor() (string, error) {
	v := Get("DEFAULT_RUNTIME_EXECUTOR")
	if v == "" {
		v = "MARATHON"
	}
	return v, nil
}

func parsePoolSize() int {
	v, err := GetInt("POOL_SIZE")
	if err != nil {
		v = 500
	}
	if v <= 0 {
		v = 500
	}
	return v
}
