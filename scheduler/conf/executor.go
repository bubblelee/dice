package conf

import (
	"context"

	"terminus.io/dice/dice/pkg/jsonstore"
)

// EXECUTOR_K8S_DICE1_KEY1=VALUE1
// EXECUTOR_K8S_DICE1_KEY2=VALUE2
//
// EXECUTOR_MARATHON_DICE2_KEY1=VALUE1
// EXECUTOR_MARATHON_DICE2_KEY2=VALUE2
//
type ExecutorConfig struct {
	Kind    string            `json:"kind,omitempty"`
	Name    string            `json:"name,omitempty"`
	Options map[string]string `json:"options,omitempty"`
}

// ParseExecutors returns the configure options of executors, they must contain the prefix "EXECUTOR_".
func LoadExecutorConfig(store jsonstore.JsonStore) (chan ExecutorConfig, error) {
	configs := make(chan ExecutorConfig, 1000)
	handle := func(key string, o interface{}) error {
		configs <- *(o.(*ExecutorConfig))
		return nil
	}
	if err := store.ForEach(context.Background(), "/dice/scheduler/configs/cluster/", ExecutorConfig{}, handle); err != nil {
		return nil, err
	}
	return configs, nil
}
