package conf

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGlobalEnv(t *testing.T) {
	os.Setenv("DEBUG", "true")
	os.Setenv("LISTEN_ADDR", "*:9090")

	err := Parse()
	assert.Nil(t, err)
	assert.Equal(t, true, Debug)
	assert.Equal(t, "*:9090", ListenAddr)
}
