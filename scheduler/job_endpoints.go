package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/executor"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

const (
	NotFoundSuffix = "not found"
)

func prepareJob(job *spec.JobFromUser) {
	// job ID may be specified by job owner, e.g. jobflow
	if job.ID == "" {
		uuidV4 := uuid.NewV4()
		job.ID = hex.EncodeToString(uuidV4.Bytes())
	}

	// Do not overwrite environment
	if job.Env != nil && job.Env[conf.TraceLogEnv] == "" && job.ID != "" {
		job.Env[conf.TraceLogEnv] = job.ID
	}
}

func (s *Server) epCreateJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	create := JobCreateRequest{}
	if err := json.NewDecoder(r.Body).Decode(&create); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest}, err
	}
	logrus.Infof("epCreateJob job: %+v", create)
	job := spec.Job{
		JobFromUser: spec.JobFromUser(create),
		CreatedTime: time.Now().Unix(),
		LastModify:  time.Now().String(),
	}
	prepareJob(&job.JobFromUser)
	if job.Namespace == "" {
		job.Namespace = "default"
	}
	job.Status = spec.StatusCreated

	if !validateJobName(job.Name) && !validateJobNamespace(job.Namespace) {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: JobCreateResponse{
				Name:  job.Name,
				Error: "invalid job name",
			},
		}, nil
	}

	// 获取jobStatus，判断是否处于Running
	// 处于Running的话，不更新job到store
	update, err := s.fetchJobStatus(ctx, &job)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to fetch job status: %s", job.Name)
	}
	if update {
		if err := s.store.Put(ctx, makeJobKey(job.Namespace, job.Name), &job); err != nil {
			return nil, err
		}
	}
	if job.Status == spec.StatusRunning {
		return nil, errors.New("job is running")
	}

	if err := s.store.Put(ctx, makeJobKey(job.Namespace, job.Name), job); err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobCreateResponse{
			Job:  job,
			Name: job.Name,
		},
	}, nil
}

func (s *Server) epStartJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	job := spec.Job{}
	if err := s.store.Get(ctx, makeJobKey(namespace, name), &job); err != nil {
		if strings.HasSuffix(err.Error(), NotFoundSuffix) {
			return HTTPResponse{
				Status: http.StatusNotFound,
			}, err
		}
		return nil, err
	}

	job.LastModify = time.Now().String()
	job.LastStartTime = time.Now().Unix()
	job.Status = spec.StatusUnschedulable

	if err := s.store.Put(ctx, makeJobKey(job.Namespace, job.Name), job); err != nil {
		return nil, err
	}

	// FIXME: maybe need to deep copy
	jobTmp := spec.Job{}
	if err := json.NewDecoder(r.Body).Decode(&jobTmp); err != nil {
		if r.ContentLength != 0 {
			logrus.Errorf("failed to decode the start input json. job=%s", job.Name)
			return HTTPResponse{Status: http.StatusBadRequest}, err
		}
	} else {
		logrus.Debugf("job start input body: %+v", jobTmp)
		if job.Env == nil {
			job.Env = make(map[string]string)
		}
		for k, v := range jobTmp.Env {
			job.Env[k] = v
		}
	}

	// build job match tags & exclude tags
	job.Labels = appendJobTags(job.Labels)

	if _, err := s.handleJobTask(ctx, &job, TaskCreate); err != nil {
		return nil, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobStartResponse{
			Name: name,
			Job:  job,
		},
	}, nil
}

func (s *Server) epStopJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	job := spec.Job{}

	if err := s.store.Get(ctx, makeJobKey(namespace, name), &job); err != nil {
		if strings.HasSuffix(err.Error(), NotFoundSuffix) {
			return HTTPResponse{
				Status: http.StatusNotFound,
			}, err
		}
		return nil, err
	}

	if _, err := s.handleJobTask(ctx, &job, TaskDestroy); err != nil {
		return nil, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobStopResponse{
			Name: name,
		},
	}, nil
}

func (s *Server) epDeleteJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	job := spec.Job{}

	// 多次删除后job为空结构体, jsonstore的remove接口可以再添加一个返回值判断job是否被填充
	if err := s.store.Remove(ctx, makeJobKey(namespace, name), &job); err != nil {
		return nil, err
	}

	if len(job.Name) == 0 {
		return HTTPResponse{
			Status: http.StatusOK,
			Content: JobDeleteResponse{
				Name:  name,
				Error: "that name not found",
			},
		}, nil
	}

	if err := s.deleteJob(ctx, &job); err != nil {
		return nil, err
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobDeleteResponse{
			Name: name,
		},
	}, nil
}

func (s *Server) epJobPipeline(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	namespace := vars["namespace"]

	var names []string
	if err := json.NewDecoder(r.Body).Decode(&names); err != nil {
		return nil, err
	}
	// max job number is 10 by supported.
	if len(names) > 10 {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: JobBatchResponse{
				Names: names,
				Error: "the jobs are too many, max number is 10.",
			},
		}, nil
	}

	// read all jobs from store.
	jobs := make([]spec.Job, len(names))

	for i, name := range names {
		if err := s.store.Get(ctx, makeJobKey(namespace, name), &jobs[i]); err != nil {
			if strings.HasSuffix(err.Error(), NotFoundSuffix) {
				return HTTPResponse{
					Status: http.StatusNotFound,
				}, err
			}
			return nil, err
		}
	}

	// start all jobs one by one.
	for i := range jobs {
		job := &jobs[i]
		if err := s.startOneJob(ctx, job); err != nil {
			job.LastMessage = err.Error()

			return HTTPResponse{
				Status: http.StatusInternalServerError,
				Content: JobBatchResponse{
					Names: names,
					Jobs:  jobs,
					Error: "failed to pipeline jobs",
				},
			}, nil
		}
	}

	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobBatchResponse{
			Names: names,
			Jobs:  jobs,
		},
	}, nil
}

func (s *Server) epJobConcurrent(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	namespace := vars["namespace"]

	var names []string
	if err := json.NewDecoder(r.Body).Decode(&names); err != nil {
		return nil, err
	}
	// max job number is 10 by supported.
	if len(names) > 10 {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: JobBatchResponse{
				Names: names,
				Error: "the jobs are too many, max number is 10.",
			},
		}, nil
	}

	// read all jobs from store.
	jobs := make([]spec.Job, len(names))

	for i, name := range names {
		if err := s.store.Get(ctx, makeJobKey(namespace, name), &jobs[i]); err != nil {
			if strings.HasSuffix(err.Error(), NotFoundSuffix) {
				return HTTPResponse{
					Status: http.StatusNotFound,
				}, err
			}
			return nil, err
		}
	}

	var wg sync.WaitGroup

	for i := range jobs {
		wg.Add(1)

		go func(j int) {
			defer wg.Done()

			job := &jobs[j]
			if err := s.startOneJob(ctx, job); err != nil {
				job.LastMessage = err.Error()
			}
		}(i)
	}

	wg.Wait()

	return HTTPResponse{
		Status: http.StatusOK,
		Content: JobBatchResponse{
			Names: names,
			Jobs:  jobs,
		},
	}, nil
}

func (s *Server) epListJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	namespace := vars["namespace"]

	jobs := make([]*spec.Job, 0, 100)

	err := s.store.ForEach(ctx, makeJobKey(namespace, ""), spec.Job{}, func(key string, j interface{}) error {
		job := j.(*spec.Job)
		update, err := s.fetchJobStatus(ctx, job)
		if err != nil {
			return err
		}
		if update {
			if err := s.store.Put(ctx, makeJobKey(job.Namespace, job.Name), job); err != nil {
				return err
			}
		}
		jobs = append(jobs, j.(*spec.Job))
		return nil
	})
	if err != nil {
		return nil, err
	}
	return HTTPResponse{
		Status:  http.StatusOK,
		Content: jobs,
	}, nil
}

func (s *Server) epGetJob(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	job := spec.Job{}

	if err := s.store.Get(ctx, makeJobKey(namespace, name), &job); err != nil {
		if strings.HasSuffix(err.Error(), NotFoundSuffix) {
			return HTTPResponse{
				Status: http.StatusNotFound,
			}, err
		}
		return nil, err
	}

	update, err := s.fetchJobStatus(ctx, &job)
	if err != nil {
		return nil, err
	}
	if update {
		if err := s.store.Put(ctx, makeJobKey(namespace, name), &job); err != nil {
			return nil, err
		}
	}

	return HTTPResponse{
		Status:  http.StatusOK,
		Content: job,
	}, nil
}

func (s *Server) startOneJob(ctx context.Context, job *spec.Job) error {
	job.LastStartTime = time.Now().Unix()
	job.Status = spec.StatusRunning

	if err := s.store.Put(ctx, makeJobKey(job.Namespace, job.Name), job); err != nil {
		logrus.Warnf("failed to update job status: %s (%v)", job.Name, err)
	}

	// build job match tags & exclude tags
	job.Labels = appendJobTags(job.Labels)

	_, err := s.handleJobTask(ctx, job, TaskCreate)
	return err
}

func (s *Server) fetchJobStatus(ctx context.Context, job *spec.Job) (bool, error) {
	result, err := s.handleJobTask(ctx, job, TaskStatus)
	if err != nil {
		return false, err
	}

	if result.Status().Status == spec.StatusCode("") {
		return false, nil
	}

	if job.StatusDesc.Status != result.Status().Status || job.StatusDesc.LastMessage != result.Status().LastMessage {
		job.LastModify = time.Now().String()
		job.StatusDesc = result.Status()
		return true, nil
	}
	return false, nil
}

func (s *Server) deleteJob(ctx context.Context, job *spec.Job) error {
	_, err := s.handleJobTask(ctx, job, TaskRemove)
	return err
}

// add job kind to label based on which, scheduler can schedule different kind of job resources.
func modifyLabelsWithJobKind(labels map[string]string) {
	if kind, ok := labels[spec.LabelJobKind]; ok {
		matchTagsStr := labels[spec.LabelMatchTags]
		matchTagsSlice := strings.Split(matchTagsStr, ",")
		matchTagsSlice = append(matchTagsSlice, kind)
		matchTags := strings.Join(matchTagsSlice, ",")
		labels[spec.LabelMatchTags] = matchTags
	}
}

func appendJobTags(labels map[string]string) map[string]string {
	if labels == nil {
		labels = make(map[string]string)
	}
	matchTags := make([]string, 0)
	matchTags = append(matchTags, spec.TagJob)
	if labels[spec.LabelPack] == "true" {
		matchTags = append(matchTags, spec.TagPack)
	}
	labels[spec.LabelMatchTags] = strings.Join(matchTags, ",")
	if _, ok := labels[spec.LabelExcludeTags]; ok {
		labels[spec.LabelExcludeTags] = labels[spec.LabelExcludeTags] + "," + spec.TagLocked + "," + spec.TagPlatform
	} else {
		labels[spec.LabelExcludeTags] = spec.TagLocked + "," + spec.TagPlatform
	}

	modifyLabelsWithJobKind(labels)
	return labels
}

func (s *Server) epGetJobStatusForNotify(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	name := vars["name"]
	namespace := vars["namespace"]

	job := spec.Job{}

	if err := s.store.Get(ctx, makeJobKey(namespace, name), &job); err != nil {
		return nil, err
	}

	if _, err := s.fetchJobStatus(ctx, &job); err != nil {
		return nil, err
	}

	notify := spec.JobStatusNotify{
		Namespace: namespace,
		Name:      name,
	}
	notify.Status = string(job.Status)
	// TODO: for testing
	notify.Addrs = []string{"http://testschedulerfibonacci.dice.marathon.l4lb.thisdcos.directory:9091/v1/notify/posttest"}

	return HTTPResponse{
		Status:  http.StatusOK,
		Content: notify,
	}, nil
}

func (s *Server) handleJobTask(ctx context.Context, job *spec.Job, action int) (TaskResponse, error) {
	if err := setJobExecutorByCluster(job); err != nil {
		return TaskResponse{}, err
	}

	task, err := s.sched.Send(ctx, TaskRequest{
		ExecutorKind: executor.GetJobExecutorKindByName(job.Executor),
		ExecutorName: job.Executor,
		Action:       action,
		Spec:         *job,
	})
	if err != nil {
		return TaskResponse{}, err
	}

	result := task.Wait(ctx)
	return result, result.Err()
}
