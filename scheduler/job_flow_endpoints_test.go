package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/gavv/httpexpect"
	"github.com/stretchr/testify/assert"
)

// 此 case 的 job 依赖关系如下图：
//
// { job1 } -> { job2, job3, job4  } -> { job5 } -> { job6 } -> { job7, job8 } -> { job9 }

func TestJobFlowCreateAndStatus(t *testing.T) {
	os.Setenv("DEFAULT_JOB_EXECUTOR", "DEMO")
	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")
	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
		os.Unsetenv("DEFAULT_JOB_EXECUTOR")
	}()

	err := conf.Parse()
	assert.Nil(t, err)

	server := NewServer("")
	server.initEndpoints()

	testserver := httptest.NewServer(server.router)
	defer testserver.Close()

	e := httpexpect.New(t, testserver.URL)

	flow := jobspec.JobFlowRequest{
		Name: "123456789",
		Jobs: []spec.JobFromUser{
			{
				Name:   "job1",
				Image:  "ubuntu2",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
			{
				Name:    "job2",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job1"},
			},
			{
				Name:    "job3",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job1"},
			},
			{
				Name:    "job4",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job1"},
			},
			{
				Name:    "job5",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job2", "job3", "job4"},
			},
			{
				Name:    "job6",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job5"},
			},
			{
				Name:    "job7",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job6"},
			},
			{
				Name:    "job8",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job6"},
			},
			{
				Name:    "job9",
				Image:   "ubuntu2",
				Cmd:     "sleep 3600",
				CPU:     0.1,
				Memory:  1234,
				Depends: []string{"job7", "job8"},
			},
		},
	}

	e.POST("/v1/jobs/flow/create").WithJSON(flow).
		Expect().
		Status(http.StatusOK).
		JSON().Object().
		ValueEqual("status", "Running")

	for {
		body := e.GET("/v1/jobs/flow/default/123456789").Expect().Body().Raw()

		var flowResp jobspec.JobFlowResponse
		if err := json.Unmarshal([]byte(body), &flowResp); err != nil {
			assert.Nil(t, err)
		}
		if flowResp.Status != spec.StatusRunning {
			break
		}
		time.Sleep(time.Second)
	}

	e.DELETE("/v1/jobs/flow/default/123456789/delete").
		Expect().
		Status(http.StatusOK)
}
