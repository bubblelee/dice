package main

import (
	"context"
	"time"

	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/executor"
	"terminus.io/dice/dice/scheduler/executor/executortypes"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Sched struct {
	mgr *executor.Manager
}

func NewSched(configs chan conf.ExecutorConfig) (*Sched, error) {
	mgr := executor.GetManager()
	if err := mgr.Initialize(configs); err != nil {
		return nil, err
	}
	s := &Sched{
		mgr: executor.GetManager(),
	}
	go s.PrintPoolUsage()

	return s, nil
}

func (s *Sched) PrintPoolUsage() {
	logrus.Infof("enter PrintPoolUsage")
	for {
		select {
		case <-time.After(1 * time.Minute):
			s.mgr.PrintPoolUsage()
		}
	}
}

func (s *Sched) runTask(ctx context.Context, task *Task) error {
	// find a right executor
	executor, err := s.FindExecutor(task.ExecutorName, task.ExecutorKind)
	if err != nil {
		s.Return(task.ctx, task, TaskResponse{
			err: err,
		})
		return err
	}

	if task.ExecutorName == "" {
		task.ExecutorName = string(executor.Name())
	}
	task.executor = executor

	logrus.Infof("start to run the task: %s", task)

	var resp TaskResponse
	defer func() {
		s.Return(ctx, task, resp)
		logrus.Infof("finish to run the task: %s", task)
	}()
	if resp = task.Run(ctx); resp.Err() != nil {
		return resp.Err()
	}
	return nil
}

func (s *Sched) Send(ctx context.Context, req TaskRequest) (Result, error) {
	task := &Task{
		TaskRequest: req,
		c:           make(chan TaskResponse, 1),
		ctx:         ctx,
	}

	err := s.mgr.Pool(executortypes.Name(task.ExecutorName)).GoWithTimeout(func() {
		if err := s.runTask(ctx, task); err != nil {
			logrus.Errorf("failed to execute task: %s (%v)", task, err)
		}
	}, time.Second*5)
	if err != nil {
		return nil, err
	}

	return task, nil
}

func (s *Sched) Return(ctx context.Context, t *Task, resp TaskResponse) {
	t.c <- resp
	close(t.c)
}

func (s *Sched) ListExecutors() []executortypes.Executor {
	return s.mgr.ListExecutors()
}

func (s *Sched) FindExecutor(name, kind string) (executortypes.Executor, error) {
	if name != "" {
		executor, err := s.mgr.Get(executortypes.Name(name))
		if err != nil {
			return nil, err
		}
		return executor, nil
	}

	executors := s.mgr.GetByKind(executortypes.Kind(kind))
	// FIXME not random
	if len(executors) != 0 {
		return executors[0], nil
	}
	return nil, errors.Errorf("not found executor: %s", kind)
}
