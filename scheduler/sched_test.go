package main

import (
	"context"
	"os"
	"testing"
	"time"

	"terminus.io/dice/dice/scheduler/executor/executortypes"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

type testObject struct {
	A int
	B string
}

func TestSched(t *testing.T) {
	// name := executortypes.Name("DEMOEXEC")
	kind := executortypes.Kind("DEMO")

	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")

	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
	}()

	sched, err := NewSched(nil)
	assert.Nil(t, err)

	task, err := sched.Send(context.Background(), TaskRequest{
		Action:       TaskCreate,
		ExecutorKind: string(kind),
		Spec: testObject{
			A: 1,
			B: "1",
		},
	})
	assert.Nil(t, err)

	_ = task.Wait(context.Background())
	//assert.NotNil(t, err)

	// object, ok := result.(testObject)
	// assert.Equal(t, true, ok)
	// assert.Equal(t, 1, object.A)
	// assert.Equal(t, "1", object.B)
}

// 异常测试：循环测试100+不存在的exector，确认task.Wait是否会卡住
func TestSchedNotFoundExecutor(t *testing.T) {
	var task Result
	var err error

	name := executortypes.Name("DEMOEXECNOTEXSIT")
	kind := executortypes.Kind("DEMO")

	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")

	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
	}()

	sched, err := NewSched(nil)
	assert.Nil(t, err)

	for i := 1; i < 101; i++ {
		task, err = sched.Send(context.Background(), TaskRequest{
			Action:       TaskCreate,
			ExecutorName: string(name),
			ExecutorKind: string(kind),
			Spec: testObject{
				A: 1,
				B: "1",
			},
		})
		assert.Nil(t, err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	resp := task.Wait(ctx)
	assert.NotEqual(t, resp.err, errors.Errorf("failed to send task, timeout(10s)"))
}
