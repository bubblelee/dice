// +build !default

package k8sjob

import (
	"context"
	"github.com/stretchr/testify/assert"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/spec"
	"testing"
	"time"
)

var job = spec.Job{
	JobFromUser: spec.JobFromUser{
		Namespace: "default",
		Name:      "kube-job-test0",
		Cmd:       "sleep 10",
		CPU:       0.1,
		Memory:    256,
		Image:     "ubuntu",
		Env:       map[string]string{"testkey": "testvalue"},
		Binds: []spec.Bind{
			{
				ContainerPath: "/data",
				HostPath:      "/netdata/job-test/chronos-data1",
			},
		},
		Labels: map[string]string{"MATCH_TAGS": "job,pack", "EXCLUDE_TAGS": "locked,platform"},
	},
}

var kube = &k8sJob{
	name:   "kube-job-test",
	addr:   "127.0.0.1:8080",
	prefix: "/apis/batch/v1/namespaces/",
	client: httpclient.New(),
}

func TestK8sJob_Create(t *testing.T) {
	var ctx = context.Background()

	_, err := kube.Create(ctx, job)
	assert.Nil(t, err)
}

func TestK8sJob_Destroy(t *testing.T) {
	var ctx = context.Background()

	err := kube.Destroy(ctx, job)
	assert.Nil(t, err)
}

func TestK8sJob_Status(t *testing.T) {
	var ctx = context.Background()

	_, err := kube.Create(ctx, job)
	assert.Nil(t, err)

	status, err := kube.Status(ctx, job)
	assert.Nil(t, err)
	t.Logf("Test k8s job status1: %+v", status)

	// Running
	time.Sleep(5 * time.Second)
	status, err = kube.Status(ctx, job)
	assert.Nil(t, err)
	assert.Equal(t, spec.StatusRunning, status.Status)
	t.Logf("Test k8s job status2: %+v", status)

	// Success
	time.Sleep(15 * time.Second)
	status, err = kube.Status(ctx, job)
	assert.Nil(t, err)
	assert.Equal(t, spec.StatusStoppedOnOK, status.Status)
	t.Logf("Test k8s job status3: %+v", status)
}

func TestK8sJob_Remove(t *testing.T) {
	var ctx = context.Background()

	err := kube.Remove(ctx, job)
	assert.Nil(t, err)
}

func TestK8sJob_StatusOnFailed(t *testing.T) {
	var ctx = context.Background()

	var obj spec.Job = spec.Job{
		JobFromUser: spec.JobFromUser{
			Namespace: "default",
			Name:      "kube-job-test0",
			Cmd:       "exit 1",
			CPU:       0.1,
			Memory:    256,
			Image:     "ubuntu",
			Env:       map[string]string{"testkey": "testvalue"},
			Binds: []spec.Bind{
				{
					ContainerPath: "/data",
					HostPath:      "/netdata/job-test/chronos-data1",
				},
			},
			Labels: map[string]string{"MATCH_TAGS": "job,pack", "EXCLUDE_TAGS": "locked,platform"},
		},
	}

	_, err := kube.Create(ctx, obj)
	assert.Nil(t, err)

	// failed
	time.Sleep(5 * time.Second)
	status, err := kube.Status(ctx, obj)
	assert.Nil(t, err)
	assert.Equal(t, spec.StatusStoppedOnFailed, status.Status)
	t.Logf("Test k8s job status: %+v", status)

	// remove
	err = kube.Remove(ctx, obj)
	assert.Nil(t, err)
}
