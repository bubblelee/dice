package k8sjob

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"
)

const (
	kind   = "K8SJOB"
	prefix = "/apis/batch/v1/namespaces/"
)

var (
	parallelism int32 = 1
	completions int32 = 1
	// k8s job 默认是有 6 次重试的机会，
	// 基于现有业务，重试次数设置成 0 次，要么成功要么失败
	defaultBackoffLimit int32 = 0
)

// k8s job plugin's configure
//
// EXECUTOR_K8SJOB_K8SJOBFORTERMINUS_ADDR=http://127.0.0.1:8080
// EXECUTOR_K8SJOB_K8SJOBFORJOBTERMINUS_BASICAUTH=admin:Terminus1234
func init() {
	executortypes.Register(kind, func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		addr, ok := options["ADDR"]
		if !ok {
			return nil, errors.Errorf("not found k8s address in env variables")
		}

		client := httpclient.New()
		basicAuth, ok := options["BASICAUTH"]
		if ok {
			userPasswd := strings.Split(basicAuth, ":")
			if len(userPasswd) == 2 {
				client.BasicAuth(userPasswd[0], userPasswd[1])
			}
		}

		return &k8sJob{
			name:    name,
			options: options,
			addr:    addr,
			prefix:  prefix,
			client:  client,
		}, nil
	})
}

type k8sJob struct {
	name    executortypes.Name
	options map[string]string
	addr    string
	prefix  string
	client  *httpclient.HTTPClient
}

func (k *k8sJob) Kind() executortypes.Kind {
	return kind
}

func (k *k8sJob) Name() executortypes.Name {
	return k.name
}

func (k *k8sJob) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	var b bytes.Buffer

	kubeJob, err := generateKubeJob(specObj)
	if err != nil {
		return nil, errors.New("create k8s job: invalid job spec")
	}

	if tmp, err := json.Marshal(kubeJob); err != nil {
		logrus.Debugf("create k8s job json: %s", string(tmp))
	}

	ns := kubeJob.Metadata.Namespace
	name := kubeJob.Metadata.Name
	fullName := ns + "/" + name

	// POST /apis/batch/v1/namespaces/{namespace}/jobs
	resp, err := k.client.Post(k.addr).
		Path(k.prefix+ns+"/jobs").
		Header("Content-Type", "application/json").
		JSONBody(kubeJob).
		Do().
		Body(&b)
	if err != nil {
		return nil, errors.Wrapf(err, "create k8s job: %s", fullName)
	}

	if resp == nil {
		return nil, errors.Errorf("resp is null")
	}

	if !resp.IsOK() {
		logrus.Errorf("[k8s job] Failed to create job: %s, resp body: %v", fullName, b.String())
		return nil, errors.Errorf("failed to create k8s job: %s, statusCode: %d, body: %v",
			fullName, resp.StatusCode(), b.String())
	}

	return nil, nil
}

// 这里的 Destory 意为 Stop
// 通过设置 job.spec.parallelism = 0 来停止 job
func (k *k8sJob) Destroy(ctx context.Context, specObj interface{}) error {
	// TODO: same as Remove
	return k.Remove(ctx, specObj)
}

func (k *k8sJob) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	var statusDesc spec.StatusDesc
	var job KubeJob
	var b bytes.Buffer

	kubeJob, err := generateKubeJob(specObj)
	if err != nil {
		return statusDesc, errors.New("k8s job Status: invalid job spec")
	}

	if tmp, err := json.Marshal(kubeJob); err != nil {
		logrus.Debugf("k8s job status json: %s", string(tmp))
	}

	ns := kubeJob.Metadata.Namespace
	name := kubeJob.Metadata.Name
	fullName := ns + "/" + name

	// GET /apis/batch/v1/namespaces/{namespace}/jobs/{name}
	resp, err := k.client.Get(k.addr).
		Path(k.prefix+ns+"/jobs/"+name).
		Header("Content-Type", "application/json").
		Do().
		Body(&b)
		//JSON(&job)
	if err != nil {
		return statusDesc, errors.Wrapf(err, "k8s job status: %s")
	}

	if resp == nil {
		return statusDesc, errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		logrus.Errorf("[k8s job] Failed to get the status of job: %s, statusCode: %d, body: %s",
			fullName, resp.StatusCode(), b.String())
		return statusDesc, errors.Errorf("failed to get the status of k8s job: %s, statusCode: %d, body: %s",
			fullName, resp.StatusCode(), b.String())
	}

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(&job); err != nil {
		return statusDesc, err
	}
	statusDesc = generateKubeJobStatus(&job)

	return statusDesc, nil
}

func (k *k8sJob) Remove(ctx context.Context, specObj interface{}) error {
	var b bytes.Buffer

	kubeJob, err := generateKubeJob(specObj)
	if err != nil {
		return errors.New("destory k8s job: invalid job spec")
	}

	if tmp, err := json.Marshal(kubeJob); err != nil {
		logrus.Debugf("destory kube job json: %s", string(tmp))
	}

	ns := kubeJob.Metadata.Namespace
	name := kubeJob.Metadata.Name
	fullName := ns + "/" + name

	// DELETE /apis/batch/v1/namespaces/{namespace}/jobs/{name}
	resp, err := k.client.Delete(k.addr).
		Path(k.prefix+ns+"/jobs/"+name).
		Header("Content-Type", "application/json").
		JSONBody(deleteOptions).
		Do().
		Body(&b)
	if err != nil {
		return errors.Wrapf(err, "remove k8s job: %s", fullName)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if !resp.IsOK() && resp.StatusCode() != 404 {
		logrus.Errorf("[k8s job] Failed to remove job: %s, statusCode: %d, body: %s", fullName, resp.StatusCode(), b.String())
		return errors.Errorf("failed to remove k8s job: %s, statusCode: %d, body: %s", fullName, resp.StatusCode(), b.String())
	}

	return nil
}

func (k *k8sJob) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	var b bytes.Buffer

	kubeJob, err := generateKubeJob(specObj)
	if err != nil {
		return nil, errors.New("destory k8s job: invalid job spec")
	}

	if tmp, err := json.Marshal(kubeJob); err != nil {
		logrus.Debugf("destory kube job json: %s", string(tmp))
	}

	ns := kubeJob.Metadata.Namespace
	name := kubeJob.Metadata.Name
	fullName := ns + "/" + name

	// PUT /apis/batch/v1/namespaces/{namespace}/jobs/{name}
	resp, err := k.client.Put(k.addr).
		Path(k.prefix+ns+"/jobs/"+name).
		Header("Content-Type", "application/json").
		JSONBody(kubeJob).
		Do().
		Body(&b)
	if err != nil {
		return nil, errors.Wrapf(err, "update k8s job: %s", fullName)
	}

	if resp == nil {
		return nil, errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		logrus.Errorf("[k8s job] Failed to update job: %s, statusCode: %d, body: %s", fullName, resp.StatusCode(), b.String())
		return nil, errors.Errorf("failed to update k8s job: %s, statusCode: %d, body: %s", fullName, resp.StatusCode(), b.String())
	}

	return nil, nil
}

func (k *k8sJob) Inspect(ctx context.Context, specObj interface{}) (interface{}, error) {
	return nil, nil
}

func generateKubeJob(specObj interface{}) (*KubeJob, error) {
	job, ok := specObj.(spec.Job)
	if !ok {
		return nil, errors.New("Kube Job Create: invalid job spec")
	}

	kubeJob := &KubeJob{
		Kind:       "Job",
		APIVersion: "batch/v1",
		Metadata: ObjectMeta{
			Name:      job.Name,
			Namespace: job.Namespace,
			// TODO: 现在无法直接使用job.Labels，不符合k8s labels的规则
			//Labels:    job.Labels,
		},
		Spec: JobSpec{
			Parallelism: &parallelism,
			// Completions = nil, 即表示只要有一次成功，就算completed
			Completions: &completions,
			// TODO: add ActiveDeadlineSeconds
			//ActiveDeadlineSeconds: &defaultActiveDeadlineSeconds,
			// default value: 6
			BackoffLimit: &defaultBackoffLimit,
			// TODO: selector
			Template: PodTemplateSpec{
				Metadata: ObjectMeta{
					Name:      job.Name,
					Namespace: job.Namespace,
					//Labels:    job.Labels,
				},
				Spec: PodSpec{
					Containers: []Container{
						{
							Name: job.Name,
							// TODO:
							// tmp hacking for registry
							Image: strings.Replace(job.Image,
								"docker-registry.registry.marathon.mesos:5000",
								"hub.docker.test.terminus.io:80",
								-1),
							// TODO: split ？
							//Command: strings.Split(job.Cmd, " "),
							Command: []string{"sh", "-c", job.Cmd},
							Resources: Resources{
								Requests: Requests{
									Cpu:    strconv.Itoa(int(job.CPU*1000)) + "m",
									Memory: strconv.Itoa(int(job.Memory)) + "Mi",
									//Storage: strconv.Itoa(int(service.Resources.Disk)) + "M",
								},
							},
						},
					},
					RestartPolicy: RestartPolicyNever,
				},
			},
		},
	}

	pod := &kubeJob.Spec.Template
	// 按当前业务，只支持一个 Pod 一个 Container
	container := &pod.Spec.Containers[0]

	// envs
	for k, v := range job.Env {
		container.Env = append(container.Env, EnvVar{
			Name:  k,
			Value: v,
		})
	}

	// volumes
	for i, bind := range job.Binds {
		logrus.Debugf("kube job bind volume, number: %d, hostPath: %s, containerPath: %s, readOnly: %v",
			i, bind.HostPath, bind.ContainerPath, bind.ReadOnly)
		if len(bind.HostPath) == 0 || len(bind.ContainerPath) == 0 {
			logrus.Errorf("kube job bind volume error, invalid params: hostPath: %s, containerPath: %s",
				bind.HostPath, bind.ContainerPath)

		}
		pod.Spec.Volumes = append(pod.Spec.Volumes, Volume{
			Name: "volume" + strconv.Itoa(i),
			HostPath: &HostPathVolumeSource{
				Path: bind.HostPath,
			},
		})

		container.VolumeMounts = append(container.VolumeMounts, VolumeMount{
			Name:      "volume" + strconv.Itoa(i),
			MountPath: bind.ContainerPath,
			ReadOnly:  bind.ReadOnly,
		})
	}

	logrus.Debugf("generate kube job struct: %+v", kubeJob)
	return kubeJob, nil
}

func generateKubeJobStatus(job *KubeJob) spec.StatusDesc {
	var statusDesc spec.StatusDesc

	// job controller 都还未处理该 Job
	if job.Status.StartTime == nil {
		statusDesc.Status = spec.StatusUnschedulable
		return statusDesc
	}

	if job.Status.Failed > 0 {
		statusDesc.Status = spec.StatusStoppedOnFailed
		for _, condition := range job.Status.Conditions {
			if condition.Type == JobFailed {
				statusDesc.LastMessage = condition.Message
			}
		}
	} else if job.Status.CompletionTime == nil {
		if job.Status.Active > 0 {
			statusDesc.Status = spec.StatusRunning
		} else {
			statusDesc.Status = spec.StatusUnschedulable
		}

	} else {
		// TODO: 如何判断 job 是被 stopped?
		if job.Status.Succeeded >= *job.Spec.Completions {
			statusDesc.Status = spec.StatusStoppedOnOK
		} else {
			statusDesc.Status = spec.StatusStoppedOnFailed
			for _, condition := range job.Status.Conditions {
				if condition.Type == JobFailed {
					statusDesc.LastMessage = condition.Message
				}
			}
		}
	}

	return statusDesc
}
