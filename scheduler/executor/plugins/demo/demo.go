package demo

import (
	"context"
	"time"

	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/sirupsen/logrus"
)

func init() {
	executortypes.Register("DEMO", func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		return &Demo{
			name: name,
		}, nil
	})
}

type Demo struct {
	name executortypes.Name
}

func (d *Demo) Kind() executortypes.Kind {
	return "DEMO"
}

func (d *Demo) Name() executortypes.Name {
	return d.name
}

func (d *Demo) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	logrus.Infof("demo create ...")

	time.Sleep(time.Second * 5)
	return nil, nil
}

func (d *Demo) Destroy(ctx context.Context, spec interface{}) error {
	logrus.Infof("demo destroy ...")

	time.Sleep(time.Second * 5)
	return nil
}

func (d *Demo) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	logrus.Infof("demo status ...")

	time.Sleep(time.Second * 5)
	return spec.StatusDesc{
		Status: spec.StatusStoppedOnOK,
	}, nil
}

func (d *Demo) Remove(ctx context.Context, spec interface{}) error {
	logrus.Infof("demo remove ...")

	time.Sleep(time.Second * 5)
	return nil
}

func (d *Demo) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	logrus.Infof("demo update ...")

	time.Sleep(time.Second * 5)
	return nil, nil
}

func (d *Demo) Inspect(ctx context.Context, spec interface{}) (interface{}, error) {
	logrus.Infof("demo inspect ...")

	time.Sleep(time.Second * 5)
	return nil, nil
}
