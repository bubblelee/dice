package metronome

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/executor/util"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	kind = "METRONOME"
)

// Metronome plugin's configure, Format: EXECUTOR_{kind}_{name}_{key}
//
// EXECUTOR_METRONOME_METRONOMEFORTERMINUS_ADDR="{host}:{port}/service/metronome"
// EXECUTOR_METRONOME_METRONOMEFORTERMINUS_BASICAUTH="admin:Terminus1234"
// EXECUTOR_METRONOME_METRONOMEFORTERMINUS_ENABLETAG=true
func init() {
	executortypes.Register(kind, func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		addr, ok := options["ADDR"]
		if !ok {
			return nil, errors.Errorf("not found metronome address in env variables")
		}

		client := httpclient.New()

		if _, ok := options["CA_CRT"]; ok {
			logrus.Infof("metronome executor(%s) addr for https: %v", name, addr)
			client = httpclient.New(httpclient.WithHttpsCertFromJSON([]byte(options["CLIENT_CRT"]),
				[]byte(options["CLIENT_KEY"]),
				[]byte(options["CA_CRT"])))
		}

		if basicAuth, ok := options["BASICAUTH"]; ok {
			ba := strings.Split(basicAuth, ":")
			if len(ba) == 2 {
				client = client.BasicAuth(ba[0], ba[1])
			}
		}

		go util.GetAndSetTokenAuth(client, string(name))

		enableTag, err := util.ParseEnableTagOption(options, "ENABLETAG", false)
		if err != nil {
			return nil, err
		}

		return &Metronome{
			name:      name,
			options:   options,
			addr:      addr,
			client:    client,
			enableTag: enableTag,
		}, nil
	})
}

func generateFetcherFromEnv(env map[string]string) []Artifact {
	artifacts := []Artifact{}
	for key, value := range env {
		if strings.HasPrefix(key, "MESOS_FETCHER_URI") {
			artifact := Artifact{
				Uri:        value,
				Executable: false,
				Extract:    false,
				Cache:      false,
			}
			artifacts = append(artifacts, artifact)
		}
	}
	return artifacts
}

type Metronome struct {
	name      executortypes.Name
	options   map[string]string
	addr      string
	client    *httpclient.HTTPClient
	enableTag bool
}

func (c *Metronome) Kind() executortypes.Kind {
	return kind
}

func (c *Metronome) Name() executortypes.Name {
	return c.name
}

func (c *Metronome) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	mJob, err := c.generateMetronomeJob(specObj)
	if err != nil {
		logrus.Errorf("generateMetronomeJob error: %v", err)
		return nil, err
	}

	var respMetronomeJob MetronomeJob

	if bs, e := json.Marshal(mJob); e == nil {
		logrus.Debugf("mjob json: %v", string(bs))
	} else {
		logrus.Errorf("marshal mjob error: %v", err)
		return nil, errors.Errorf("Create: marshal mjob error: %v", err)
	}

	// create that job
	var b bytes.Buffer
	resp, err := c.client.Post(c.addr).
		Path("/v1/jobs").
		Header("Content-Type", "application/json").
		JSONBody(mJob).
		Do().
		Body(&b)

	if err != nil {
		return nil, errors.Errorf("post metronome job(%s) error: %v", mJob.Id, err)
	}

	if !resp.IsOK() {
		return nil, errors.Errorf("failed to create metronome job: %s, statusCode=%d, body: %v", mJob.Id, resp.StatusCode(), b.String())
	}

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(&respMetronomeJob); err != nil {
		return nil, err
	}

	b.Reset()
	// post a run to run that job
	resp, err = c.client.Post(c.addr).
		Path("/v1/jobs/"+respMetronomeJob.Id+"/runs").
		Header("Content-Type", "application/json").
		JSONBody(nil).
		Do().
		Body(&b)

	if err != nil {
		return nil, errors.Errorf("post metronome job(%s) runs error: %v", mJob.Id, err)
	}

	if !resp.IsOK() {
		return nil, errors.Errorf("failed to create a run for metronome job: %s, statusCode=%d, body: %v", mJob.Id, resp.StatusCode(), b.String())
	}

	return nil, nil
}

func (c *Metronome) Destroy(ctx context.Context, specObj interface{}) error {
	job, ok := specObj.(spec.Job)
	if !ok {
		return errors.New("invalid job spec")
	}

	jobName := job.Namespace + "." + job.Name
	runs := make([]Run, 0)

	// first step, kill all runs for a job
	resp, err := c.client.Get(c.addr).
		Path("/v1/jobs/" + url.PathEscape(jobName) + "/runs").
		Do().JSON(&runs)
	if err != nil {
		return errors.Wrapf(err, "metronome get runs for job(%s) failed, err: %v", jobName, err)
	}
	if !resp.IsOK() {
		return errors.Errorf("failed to get runs of job(%v) failed: %s, statusCode=%d", jobName, resp.StatusCode())
	}

	for _, run := range runs {
		resp, err = c.client.Post(c.addr).
			Path("/v1/jobs/" + url.PathEscape(jobName) + "/runs/" + run.Id + "/actions/stop").
			Do().
			DiscardBody()
		if err != nil {
			return errors.Wrapf(err, "metronome stopping run(%s) for the job(%s) failed: %s", run.Id, jobName)
		}
		if !resp.IsOK() {
			return errors.Errorf("failed to stop metronome run(%s) for the job(%s), statusCode=%d", run.Id, jobName, resp.StatusCode())
		}
	}

	// second step, delete that job
	return c.removeJob(ctx, specObj)
}

func (c *Metronome) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	jobStatus := spec.StatusDesc{Status: spec.StatusUnkonw}

	job, ok := specObj.(spec.Job)
	if !ok {
		return jobStatus, errors.New("invalid job spec")
	}

	jobName := job.Namespace + "." + job.Name
	var getJob MetronomeJobResult
	resp, err := c.client.Get(c.addr).
		Path("/v1/jobs/"+jobName).
		Param("embed", "activeRuns").
		Param("embed", "history").
		Do().
		JSON(&getJob)

	if err != nil {
		return jobStatus, errors.Wrapf(err, "metronome get job(%s) failed", jobName)
	}
	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			// 1. create接口进来的，会忽略这个状态继续走下去
			// 2. 真正的get接口进来的，正常场景可以归纳成StatusStoppedByKilled状态，即该job已被删除
			jobStatus.Status = spec.StatusStoppedByKilled
			return jobStatus, nil
		}
		return jobStatus, errors.Errorf("failed to get metronome job(%s), statusCode=%d", jobName, resp.StatusCode())
	}

	// TODO: improve the logic to decide the status
	// 当前的设定，一个job里只会有一个run
	runs := make([]RunResult, 0)
	resp, err = c.client.Get(c.addr).
		Path("/v1/jobs/" + url.PathEscape(jobName) + "/runs").
		Do().
		JSON(&runs)
	// there is no active runs
	if len(runs) == 0 {
		if len(getJob.History.SuccessfulFinishedRuns) > 0 && len(getJob.History.FailedFinishedRuns) == 0 {
			jobStatus.Status = spec.StatusStoppedOnOK
			jobStatus.LastMessage = string(spec.StatusStoppedOnOK)
		} else if len(getJob.History.SuccessfulFinishedRuns) == 0 && len(getJob.History.FailedFinishedRuns) > 0 {
			jobStatus.Status = spec.StatusStoppedOnFailed
			jobStatus.LastMessage = string(spec.StatusStoppedOnFailed)
		}
	} else {
		run := runs[0]
		// 目前观察到的状态包括
		// INITIAL, 资源未到位, "INITIAL"是从api取得的状态, 在dcos页面上对应的显示是"Starting"
		// ACTIVE, 正在运行, 对应dcos页面上显示"Running"
		// STARTING
		// SUCCESS
		// FAILED
		switch run.Status {
		case "INITIAL":
			jobStatus.Status = spec.StatusUnschedulable
		case "ACTIVE", "STARTING":
			jobStatus.Status = spec.StatusRunning
		case "SUCCESS":
			jobStatus.Status = spec.StatusStoppedOnOK
		case "FAILED":
			jobStatus.Status = spec.StatusStoppedOnFailed
		default:
			jobStatus.Status = spec.StatusUnkonw
		}
		jobStatus.LastMessage = run.Status
	}
	logrus.Debugf("metronome job(%s) status: %+v", jobName, jobStatus)

	return jobStatus, nil
}

// Attention: DO NOT delete Metronome job that had active runs in it,
// if DELETE a metronome with active runs, there would be a 409 Error, with following info:
// "message":"There are active job runs. Override with stopCurrentJobRuns=true",
// Use DESTROY method instead
func (c *Metronome) Remove(ctx context.Context, specObj interface{}) error {
	return c.Destroy(ctx, specObj)
}

func (c *Metronome) removeJob(ctx context.Context, specObj interface{}) error {
	job, ok := specObj.(spec.Job)
	if !ok {
		return errors.New("invalid job spec")
	}

	jobName := job.Namespace + "." + job.Name

	// remove job
	resp, err := c.client.Delete(c.addr).
		Path("/v1/jobs/" + url.PathEscape(jobName)).
		Do().
		DiscardBody()
	if err != nil {
		return errors.Wrapf(err, "metronome delete job: %s", jobName)
	}
	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			// 按照调度层的接口，如果一create就来delete是有问题的，
			// 因为create只会在etcd存一下jobid, metronome看来job就是不存在的
			// 其他的情况也可认为删除成功
			return nil
		}
		return errors.Errorf("failed to delete metronome job: %s, statusCode=%d", jobName, resp.StatusCode())
	}

	return nil
}

func (c *Metronome) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	return nil, errors.New("job(metronome) not support update action")
}

func (c *Metronome) Inspect(ctx context.Context, spec interface{}) (interface{}, error) {
	return nil, errors.New("job(metronome) not support inspect action")
}

func (c *Metronome) generateMetronomeJob(specObj interface{}) (*MetronomeJob, error) {
	job, ok := specObj.(spec.Job)
	if !ok {
		return nil, errors.New("invalid job spec")
	}

	mJob := &MetronomeJob{
		Id: job.Namespace + "." + job.Name,
		// TODO: following undefied fields(Description, Label)
		Description: "hello-metronome",
		Label:       make(map[string]string),
		Run: Run{
			Artifacts: make([]Artifact, 0),
			Cmd:       job.Cmd,
			Cpus:      job.CPU,
			Mem:       job.Memory,
			Env:       job.Env,
			Restart: Restart{
				Policy: "NEVER",
			},
			Docker: Docker{
				Image: job.Image,
				// TODO: following undefied fields
				ForcePullImage: true,
			},
			Volumes: make([]Volume, 0),
			// TODO: following undefied fields
			MaxLaunchDelay: 3600,
			Disk:           0,
			Placement:      c.buildMetronomePlacement(job.Labels),
		},
	}

	// container volumes
	for _, bind := range job.Binds {
		var mode string

		if bind.ReadOnly {
			mode = "RO"
		} else {
			mode = "RW"
		}
		mJob.Run.Volumes = append(mJob.Run.Volumes,
			Volume{
				ContainerPath: bind.ContainerPath,
				HostPath:      bind.HostPath,
				Mode:          mode,
			})
	}

	// contruct artifact
	if artifacts := generateFetcherFromEnv(job.Env); len(artifacts) != 0 {
		mJob.Run.Artifacts = artifacts
	}

	return mJob, nil
}

func (c *Metronome) buildMetronomePlacement(labels map[string]string) *Placement {
	dcosCons := util.BuildDcosConstraints(c.enableTag, labels, nil, nil)
	if dcosCons == nil || len(dcosCons) == 0 {
		return nil
	}
	var metroCons []Constraints
	for _, one := range dcosCons {
		if len(one) != 3 {
			continue
		}
		metroCons = append(metroCons, Constraints{Attribute: one[0], Operator: one[1], Value: one[2]})
	}
	if len(metroCons) == 0 {
		return nil
	}
	return &Placement{metroCons}
}
