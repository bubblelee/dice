package metronome

import (
	"context"
	"os"
	"reflect"
	"testing"
	"time"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/stretchr/testify/assert"
)

var metronome *Metronome

var obj spec.Job = spec.Job{
	JobFromUser: spec.JobFromUser{
		Namespace: "test",
		Name:      "mytest9",
		Cmd:       "sleep 10",
		CPU:       0.1,
		Memory:    64,
		Image:     "ubuntu",
		Env:       map[string]string{"testkey": "testvalue"},
		Labels:    map[string]string{"MATCH_TAGS": "pack,job", "EXCLUDE_TAGS": "platform,locked"},
	},
}

func initMetronome() {
	os.Setenv("DEFAULT_JOB_EXECUTOR", "METRONOME")
	defer func() {
		os.Unsetenv("DEFAULT_JOB_EXECUTOR")
	}()

	metronome = &Metronome{
		name:   "METRONOME",
		addr:   "dcos.test.terminus.io/service/metronome",
		client: httpclient.New().BasicAuth("admin", "Terminus1234"),
	}
}

func TestMain(m *testing.M) {
	initMetronome()
	ret := m.Run()
	os.Exit(ret)
}

func TestMetronome_generateFetcherFromEnv(t *testing.T) {
	envMap := map[string]string{
		"MESOS_FETCHER_URI_TEST": "http://foo.example.com/test.jar",
		"MESOS_FETCHER_URI_TOOL": "http://foo.example.com/tool.tar",
		"LOG_LEVEL":              "DEBUG",
	}
	expectedArtifacts := []Artifact{
		{
			Uri:        "http://foo.example.com/test.jar",
			Executable: false,
			Extract:    false,
			Cache:      false,
		},
		{
			Uri:        "http://foo.example.com/tool.tar",
			Executable: false,
			Extract:    false,
			Cache:      false,
		},
	}
	artifacts := metronome.generateFetcherFromEnv(envMap)

	if !reflect.DeepEqual(artifacts, expectedArtifacts) {
		t.Errorf("Expected artifacts %v, but got %v", expectedArtifacts, artifacts)
	}
}

func TestMetronome_Create(t *testing.T) {
	ctx := context.Background()
	_, err := metronome.Create(ctx, obj)
	assert.Nil(t, err)
}

func TestMetronome_Status(t *testing.T) {
	//initMetronome(t)
	ctx := context.Background()

	for i := 1; i < 10; i++ {
		result, err := metronome.Status(ctx, obj)
		if err == nil &&
			(result.Status == spec.StatusStoppedOnOK ||
				result.Status == spec.StatusStoppedOnFailed ||
				result.Status == spec.StatusStoppedByKilled) ||
			len(result.LastMessage) > 0 {
			t.Logf("successfully get job status: %s, message: %s", result.Status, result.LastMessage)
			return
		}
		time.Sleep(1 * time.Second)
	}
	t.Error("failed to get job status")
}

func TestMetronome_Destroy(t *testing.T) {
	ctx := context.Background()
	err := metronome.Destroy(ctx, obj)
	assert.Nil(t, err)
}

func TestMetronome_Remove(t *testing.T) {
	ctx := context.Background()
	err := metronome.Remove(ctx, obj)
	assert.Nil(t, err)
}

func TestMetronome_buildMetronomePlacement(t *testing.T) {
	metronome.enableTag = false
	assert.Nil(t, metronome.buildMetronomePlacement(nil))
	assert.Nil(t, metronome.buildMetronomePlacement(map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}))
	metronome.enableTag = true
	assert.Equal(t, &Placement{[]Constraints{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
	}}, metronome.buildMetronomePlacement(map[string]string{}))
	assert.Equal(t, &Placement{[]Constraints{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
	}}, metronome.buildMetronomePlacement(nil))
	assert.Equal(t, &Placement{[]Constraints{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
		{"dice_tags", "UNLIKE", `.*\blocked\b.*`},
		{"dice_tags", "UNLIKE", `.*\bplatform\b.*`},
	}}, metronome.buildMetronomePlacement(map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}))
	assert.Equal(t, &Placement{[]Constraints{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
		{"dice_tags", "UNLIKE", `.*\block1\b.*`},
	}}, metronome.buildMetronomePlacement(map[string]string{
		"EXCLUDE_TAGS": "lock1",
	}))
	assert.Equal(t, &Placement{[]Constraints{
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack1\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack2\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack3\b.*`},
		{"dice_tags", "UNLIKE", `.*\block1\b.*`},
		{"dice_tags", "UNLIKE", `.*\block2\b.*`},
		{"dice_tags", "UNLIKE", `.*\block3\b.*`},
	}}, metronome.buildMetronomePlacement(map[string]string{
		"MATCH_TAGS":   "pack1,pack2,pack3",
		"EXCLUDE_TAGS": "lock1,lock2,lock3",
	}))
}
