package marathon

import (
	"context"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/executor/util"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	kind = "MARATHON"
	// default version is 1.6
	defaultVersion = "1.6"
	// mode for version >= 1.5
	defaultNetworkMode = "container"
	// mode for version 1.4.7, BRIDGE, HOST, USER
	defaultNetworkMode147  = "USER"
	defaultNetworkName     = "dcos"
	defaultPrefix          = "/runtimes/v1"
	defaultContainerType   = "DOCKER"
	defaultVipSuffix       = ".marathon.l4lb.thisdcos.directory"
	defaultConstraints     = "dice-role:UNLIKE:platform"
	defaultFetchUris       = ""
	defaultInternalLb      = "marathon-lb-internal-lb.marathon.mesos"
	defaultPublicIp        = "none"
	defaultPublicIpGroup   = "internal"
	defaultCloudVolumePath = "/netdata/volumes"

	// 100000 是 /sys/fs/cgroup/cpu/cpu.cfs_period_us 默认值
	CPU_CFS_PERIOD_US int = 100000
)

// marathon plugin's configure
//
// EXECUTOR_MARATHON_MARATHONFORSERVICE_ADDR=http://dice.test.terminus.io/service/marathon
// EXECUTOR_MARATHON_MARATHONFORSERVICE_PREFIX=/runtimes/v1
// EXECUTOR_MARATHON_MARATHONFORSERVICE_FETCHURIS=
// EXECUTOR_MARATHON_MARATHONFORSERVICE_VERSION=1.6
// EXECUTOR_MARATHON_MARATHONFORSERVICE_BASICAUTH=admin:Terminus1234
// EXECUTOR_MARATHON_MARATHONFORSERVICE_PUBLICIP=public-ip.app.terminus.io
// EXECUTOR_MARATHON_MARATHONFORSERVICE_ENABLETAG=true
// EXECUTOR_MARATHON_MARATHONFORSERVICE_PRESERVEPROJECTS=1,2,3
func init() {
	executortypes.Register(kind, func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		addr, ok := options["ADDR"]
		if !ok {
			return nil, errors.Errorf("not found marathon address in env variables")
		}
		prefix, ok := options["PREFIX"]
		if !ok {
			prefix = defaultPrefix
		}
		version, ok := options["VERSION"]
		if !ok {
			version = defaultVersion
		}
		ver, err := parseVersion(version)
		if err != nil {
			return nil, err
		}
		publicIp, ok := options["PUBLICIP"]
		if !ok {
			publicIp = defaultPublicIp
		}
		publicIpGroup, ok := options["PUBLICIPGROUP"]
		if !ok {
			publicIpGroup = defaultPublicIpGroup
		}

		client := httpclient.New()

		if _, ok := options["CA_CRT"]; ok {
			logrus.Infof("marathon executor(%s) addr for https: %v", name, addr)
			client = httpclient.New(httpclient.WithHttpsCertFromJSON([]byte(options["CLIENT_CRT"]),
				[]byte(options["CLIENT_KEY"]),
				[]byte(options["CA_CRT"])))
		}

		basicAuth, ok := options["BASICAUTH"]
		if ok {
			namePassword := strings.Split(basicAuth, ":")
			if len(namePassword) == 2 {
				client.BasicAuth(namePassword[0], namePassword[1])
			}
		}
		backOff, _ := options["BACKOFF"]
		isUnique := false
		unique_, ok := options["UNIQUE"]
		if ok && unique_ == "true" {
			isUnique = true
		}
		isDisableAutoRestart := false
		disableAutoRestart_, ok := options["ADDONS_DISABLE_AUTO_RESTART"]
		if ok && disableAutoRestart_ == "true" {
			isDisableAutoRestart = true
		}

		cpuSubscribeRatio := 1
		if cpuSubscribeRatio_, ok := options["CPU_SUBSCRIBE_RATIO"]; ok && len(cpuSubscribeRatio_) > 0 {
			if ratio, err := strconv.Atoi(cpuSubscribeRatio_); err == nil && ratio > 0 {
				cpuSubscribeRatio = ratio
				logrus.Debugf("executor(%s) cpuSubscribeRatio set to %v", name, ratio)
			}
		}

		cpuNumQuota := -1
		if cpuNumQuota_, ok := options["CPU_NUM_QUOTA"]; ok && len(cpuNumQuota_) > 0 {
			if num, err := strconv.Atoi(cpuNumQuota_); err == nil && num >= 0 {
				cpuNumQuota = num
				logrus.Debugf("executor(%s) cpuNumQuota set to %v", name, cpuNumQuota)
			}
		}

		go util.GetAndSetTokenAuth(client, string(name))

		enableTag, err := util.ParseEnableTagOption(options, "ENABLETAG", false)
		if err != nil {
			return nil, err
		}
		preserveProjects := util.ParsePreserveProjects(options, "PRESERVEPROJECTS")
		workspaceTags := util.ParsePreserveProjects(options, "WORKSPACETAGS")

		evCh := make(chan *spec.StatusEvent, 200)

		m := &Marathon{
			name:             name,
			options:          options,
			addr:             addr,
			prefix:           prefix,
			pubIp:            publicIp,
			pubGrp:           publicIpGroup,
			version:          ver,
			client:           client,
			enableTag:        enableTag,
			preserveProjects: preserveProjects,
			workspaceTags:    workspaceTags,
			evCh:             evCh,
			backoff:          backOff,
			unique:           isUnique,
			// set addons' service restarting period 3600s
			addonsDisableAutoRestart: isDisableAutoRestart,
			cpuSubscribeRatio:        cpuSubscribeRatio,
			cpuNumQuota:              cpuNumQuota,
		}

		if disableEvent, ok := options["DISABLE_EVENT"]; ok && disableEvent == "true" {
			return m, nil
		}
		// key是{runtimeNamespace}/{runtimeName}, value是对应event结构的地址
		lstore := &sync.Map{}

		registerEventChanAndLocalStore(string(name), evCh, lstore)

		go m.WaitEvent(options)

		go m.initEventAndPeriodSync(string(name), lstore)

		return m, nil
	})
}

type Marathon struct {
	name             executortypes.Name
	options          map[string]string
	addr             string
	prefix           string
	version          Ver
	pubIp            string
	pubGrp           string
	client           *httpclient.HTTPClient
	enableTag        bool
	preserveProjects map[string]struct{}
	workspaceTags    map[string]struct{}
	evCh             chan *spec.StatusEvent
	// 为特定的集群设置marathon的backoff factor
	backoff string
	// 为特定的集群设置均衡实例调度
	unique bool
	// 为特定的集群设置服务挂了后不被marathon拉起(通过backoff time设置为3600实现)
	// 生效需要同时满足以下两个条件：
	// 1, 集群配置中设置 ADDONS_DISABLE_AUTO_RESTART 为 "true"
	// 2，ServiceGroup(Runtime)的 label 中传入 SERVICE_TYPE : ADDONS
	addonsDisableAutoRestart bool
	// 将上层实际设置的 cpu 除以一个比例, 传递给集群调度, 默认为1
	cpuSubscribeRatio int
	// 将 cpu quota 值设置为 cpuNumQuota 个 cpu 的 quota, 默认-1，即上层设置的真实 cpu 个数
	cpuNumQuota int
}

type Ver []int

func (m *Marathon) Kind() executortypes.Kind {
	return kind
}

func (m *Marathon) Name() executortypes.Name {
	return m.name
}

func (m *Marathon) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("invalid service spec")
	}
	if runtime.ServiceDiscoveryKind == "PROXY" {
		err := m.probeProxyPortsForCreate(http.MethodPost, &runtime)
		if err != nil {
			return nil, err
		}
		// retrieve and append proxyPorts if needed
		err = m.resolveProxyPorts(&runtime)
		if err != nil {
			return nil, err
		}
		// TODO: refactor it, double put the proxyPort env
		err = m.probeProxyPortsForCreate(http.MethodPut, &runtime)
		if err != nil {
			return nil, err
		}
	}
	// construct marathon Group entity
	mGroup, err := m.buildMarathonGroup(runtime)
	if err != nil {
		return nil, errors.Wrap(err, "build marathon group failed")
	}
	// POST the constructed Group to marathon
	var method string
	if runtime.ServiceDiscoveryKind == "PROXY" {
		// already created group
		method = http.MethodPut
	} else {
		method = http.MethodPost
	}
	ret, err := m.putGroup(method, mGroup, runtime.Force)
	if err != nil {
		return nil, errors.Wrap(err, "post marathon group failed")
	}
	// TODO: return ret
	_ = ret
	return nil, nil
}

func (m *Marathon) Destroy(ctx context.Context, specObj interface{}) error {
	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return errors.New("invalid service spec")
	}
	// marathon GroupId
	mGroupId := buildMarathonGroupId(m.prefix, runtime.Namespace, runtime.Name)
	ret, err := m.deleteGroup(mGroupId, runtime.Force)
	if err != nil {
		return errors.Wrapf(err, "cleanup marathon group failed, groupId: %s", mGroupId)
	}
	// TODO: use this ret to track deployment success
	_ = ret
	return nil
}

func (m *Marathon) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	var status spec.StatusDesc
	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return status, errors.New("invalid service spec")
	}
	// marathon GroupId
	mGroupId := buildMarathonGroupId(m.prefix, runtime.Namespace, runtime.Name)
	mGroup, err := m.getGroup(mGroupId,
		"group.apps",
		"group.apps.tasks",
		"group.apps.counts",
		"group.apps.deployments",
		"group.apps.readiness",
		"group.apps.lastTaskFailure",
		"group.apps.taskStats")
	if err != nil {
		return status, errors.Wrapf(err, "get marathon group failed")
	}
	queue, err := m.getQueue()
	if err != nil {
		return status, errors.Wrapf(err, "get marathon queue failed")
	}
	mAllAppStatus := getAllAppStatus(&mGroup.Apps, queue)
	return combineRuntimeStatus(mAllAppStatus, &mGroup.Apps), nil
}

func (m *Marathon) Remove(ctx context.Context, specObj interface{}) error {
	// TODO: currently as same as Destroy
	return m.Destroy(ctx, specObj)
}

func (m *Marathon) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("invalid service spec")
	}
	if runtime.ServiceDiscoveryKind == "PROXY" {
		// retrieve and append proxyPorts if needed
		err := m.resolveProxyPorts(&runtime)
		if err != nil {
			return nil, err
		}
	}
	// construct marathon Group entity
	mGroup, err := m.buildMarathonGroup(runtime)
	if err != nil {
		return nil, errors.Wrap(err, "build marathon group failed")
	}
	// POST the constructed Group to marathon
	ret, err := m.putGroup(http.MethodPut, mGroup, runtime.Force)
	if err != nil {
		return nil, errors.Wrap(err, "put marathon group failed")
	}
	// TODO: return ret
	_ = ret
	return nil, nil
}

func (m *Marathon) Inspect(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("invalid service spec")
	}
	// marathon GroupId
	mGroupId := buildMarathonGroupId(m.prefix, runtime.Namespace, runtime.Name)
	mGroup, err := m.getGroup(mGroupId,
		"group.apps",
		"group.apps.tasks",
		"group.apps.counts",
		"group.apps.deployments",
		"group.apps.readiness",
		"group.apps.lastTaskFailure",
		"group.apps.taskStats")
	if err != nil {
		return nil, errors.Wrapf(err, "get marathon group failed")
	}
	queue, err := m.getQueue()
	if err != nil {
		return nil, errors.Wrapf(err, "get marathon queue failed")
	}
	mAllAppStatus := getAllAppStatus(&mGroup.Apps, queue)
	runtime.StatusDesc = combineRuntimeStatus(mAllAppStatus, &mGroup.Apps)
	for i := range runtime.Services {
		service := &runtime.Services[i]
		mAppId := buildMarathonAppId(mGroupId, service.Name)
		service.StatusDesc = calculateServiceStatus(mAllAppStatus[mAppId])
		mAppLevelVip := buildMarathonVipAppLevelPart(mAppId)
		service.Vip = buildMarathonVip(mAppLevelVip)

		index := -1
		for idx, app := range mGroup.Apps {
			if app.Id == mAppId {
				index = idx
				break
			}
		}

		if index >= 0 {
			service.InstanceInfos = make([]spec.InstanceInfo, len(mGroup.Apps[index].Tasks))
			for j, task := range mGroup.Apps[index].Tasks {
				var instance spec.InstanceInfo

				instance.Id = task.Id
				instance.Status = task.State
				if len(task.InstanceIpAddresses) > 0 {
					instance.Ip = task.InstanceIpAddresses[0].InstanceIp
				}

				service.InstanceInfos[j] = instance
			}
		}
	}

	if runtime.ServiceDiscoveryKind == "PROXY" {
		err := m.resolveProxyPorts(&runtime)
		if err != nil {
			return nil, err
		}
	}
	// TODO: currently only status to inspect, support tasks list later
	return &runtime, nil
}

func getAllAppStatus(mApps *[]App, queue *Queue) map[string]AppStatus {
	ret := make(map[string]AppStatus)
	for i := range *mApps {
		mApp := (*mApps)[i]
		// https://mesosphere.github.io/marathon/docs/marathon-ui.html
		// https://github.com/mesosphere/marathon-ui/blob/master/src/js/stores/AppsStore.js
		var mAppStatus AppStatus
		for _, offer := range queue.Queue {
			if mApp.Id == offer.App.Id {
				if offer.Delay.Overdue {
					mAppStatus = AppStatusWaiting
				} else {
					mAppStatus = AppStatusDelayed
				}
				break
			}
		}
		if mAppStatus == "" {
			if len(mApp.Deployments) > 0 {
				mAppStatus = AppStatusDeploying
				// TODO: support readiness ?
			} else if mApp.Instances == 0 && mApp.TasksRunning == 0 {
				mAppStatus = AppStatusDeploying
			} else if mApp.TasksRunning > 0 {
				mAppStatus = AppStatusRunning
			}
		}
		// TODO: also check app health status
		ret[mApp.Id] = mAppStatus
	}
	return ret
}

func calculateServiceStatus(mAppStatus AppStatus) spec.StatusDesc {
	switch mAppStatus {
	case AppStatusDelayed:
		return spec.StatusDesc{
			Status:      spec.StatusProgressing,
			LastMessage: "service failed",
		}
	case AppStatusWaiting:
		return spec.StatusDesc{
			Status:      spec.StatusProgressing,
			LastMessage: "service have not resource yet",
		}
	case AppStatusSuspended:
		return spec.StatusDesc{
			Status:      spec.StatusProgressing,
			LastMessage: "service stopped",
		}
	case AppStatusDeploying:
		return spec.StatusDesc{
			Status:      spec.StatusProgressing,
			LastMessage: "we are progressing, soon to ready!",
		}
	case AppStatusRunning:
		return spec.StatusDesc{
			Status:      spec.StatusReady,
			LastMessage: "service is ready to serve",
		}
	default:
		return spec.StatusDesc{
			Status:      spec.StatusUnkonw,
			LastMessage: "service unknown condition",
		}
	}
}

func combineRuntimeStatus(mAllAppStatus map[string]AppStatus, mApps *[]App) spec.StatusDesc {
	counts := make(map[AppStatus]int)
	for _, v := range mAllAppStatus {
		counts[v] += 1
	}
	if counts[AppStatusDelayed] > 0 {
		return spec.StatusDesc{
			Status:      spec.StatusFailing,
			LastMessage: "some service failed",
		}
	}
	if counts[AppStatusWaiting] > 0 {
		return spec.StatusDesc{
			Status:      spec.StatusFailing,
			LastMessage: "some service have not resource yet",
		}
	}
	if counts[AppStatusSuspended] > 0 {
		return spec.StatusDesc{
			Status:      spec.StatusFailing,
			LastMessage: "some service stopped",
		}
	}
	if counts[AppStatusDeploying] > 0 {
		return spec.StatusDesc{
			Status:      spec.StatusProgressing,
			LastMessage: "we are progressing, soon to ready!",
		}
	}
	if counts[AppStatusRunning] == len(*mApps) {
		return spec.StatusDesc{
			Status:      spec.StatusReady,
			LastMessage: "services are ready to serve",
		}
	}
	return spec.StatusDesc{
		Status:      spec.StatusUnkonw,
		LastMessage: "count of running services not match",
	}
}

func (m *Marathon) probeProxyPortsForCreate(method string, runtime *spec.Runtime) error {
	// construct marathon Group entity
	mGroup, err := m.buildMarathonGroup(*runtime)
	if err != nil {
		return errors.Wrap(err, "build marathon group failed")
	}
	for i := range mGroup.Apps {
		mGroup.Apps[i].Instances = 0
	}
	_, err = m.putGroup(method, mGroup, runtime.Force)
	return err
}

func (m *Marathon) resolveProxyPorts(runtime *spec.Runtime) error {
	mGroupId := buildMarathonGroupId(m.prefix, runtime.Namespace, runtime.Name)
	mGroup, err := m.getGroup(mGroupId)
	if err != nil {
		return err
	}
	mp := make(map[string][]int)
	for _, mApp := range mGroup.Apps {
		var portMappings []AppContainerPortMapping
		if lessThan(m.version, Ver{1, 5, 0}) {
			portMappings = mApp.Container.Docker.PortMappings
		} else {
			portMappings = mApp.Container.PortMappings
		}
		proxyPorts := make([]int, 0)
		for _, mapping := range portMappings {
			proxyPorts = append(proxyPorts, mapping.ServicePort)
		}
		mp[mApp.Id] = proxyPorts
	}
	for i := range runtime.Services {
		mAppId := buildMarathonAppId(mGroupId, runtime.Services[i].Name)
		runtime.Services[i].ProxyIp = defaultInternalLb
		runtime.Services[i].ProxyPorts = mp[mAppId]
		if runtime.Services[i].Labels["X_ENABLE_PUBLIC_IP"] == "true" {
			runtime.Services[i].PublicIp = m.pubIp
		}
	}
	return nil
}

func (m *Marathon) putGroup(method string, mGroup *Group, force bool) (*GroupPutResponse, error) {
	// do POST (create) or PUT (update) over marathon api
	var req *httpclient.Request
	if method == http.MethodPost {
		req = m.client.Post(m.addr).Path("/v2/groups/" + mGroup.Id).JSONBody(mGroup)
	} else if method == http.MethodPut {
		req = m.client.Put(m.addr).Path("/v2/groups/" + mGroup.Id).JSONBody(mGroup)
	} else {
		return nil, errors.Errorf("method(%v) not supported, only POST or PUT available", method)
	}

	if force {
		// + "?force=true"
		req = req.Param("force", "true")
	}
	var obj GroupPutResponse
	resp, err := req.Do().JSON(&obj)
	if err != nil {
		return nil, errors.Wrapf(err, "marathon schedule service: %s", mGroup.Id)
	}
	// handle response
	return processGroupPutResponse(&obj, resp.StatusCode())
}

func (m *Marathon) deleteGroup(mGroupId string, force bool) (*GroupPutResponse, error) {
	req := m.client.Delete(m.addr).Path("/v2/groups/" + mGroupId)
	if force {
		req = req.Param("force", "true")
	}
	var obj GroupPutResponse
	resp, err := req.Do().JSON(&obj)
	if err != nil {
		return nil, errors.Wrapf(err, "marathon delete failed, groupId: %s", mGroupId)
	}
	// handle response
	return processGroupPutResponse(&obj, resp.StatusCode())
}

func (m *Marathon) getGroup(mGroupId string, embed ...string) (*Group, error) {
	req := m.client.Get(m.addr).Path("/v2/groups/" + mGroupId)
	for _, em := range embed {
		req = req.Param("embed", em)
	}
	var obj GroupHTTPResult
	resp, err := req.Do().JSON(&obj)
	if err != nil {
		return nil, errors.Wrapf(err, "marathon get group failed, groupId: %s", mGroupId)
	}
	switch resp.StatusCode() {
	case http.StatusOK:
		return &(obj.Group), nil
	default:
		return nil, errors.New(obj.GetErrorResponse.Message)
	}
}

func (m *Marathon) getQueue() (*Queue, error) {
	var obj QueueHTTPResult

	resp, err := m.client.Get(m.addr).Path("/v2/queue").Do().JSON(&obj)
	if err != nil {
		return nil, errors.Wrap(err, "marathon get queue failed")
	}
	switch resp.StatusCode() {
	case http.StatusOK:
		return &(obj.Queue), nil
	default:
		return nil, errors.New(obj.GetErrorResponse.Message)
	}
}

func processGroupPutResponse(resp *GroupPutResponse, code int) (*GroupPutResponse, error) {
	switch code {
	case http.StatusOK, http.StatusCreated:
		// TODO: returns unified version
		return resp, nil
	case http.StatusBadRequest,
		http.StatusUnauthorized,
		http.StatusForbidden,
		http.StatusConflict,
		http.StatusUnprocessableEntity:
		logrus.Errorf("marathon put group failed, error=%v", resp)
		return nil, errors.New(resp.Message)
	default:
		logrus.Errorf("marathon put group failed, error=%v", resp)
		return nil, errors.New(resp.Message)
	}
}

func (m *Marathon) buildMarathonGroup(runtime spec.Runtime) (*Group, error) {
	// marathon GroupId
	mGroupId := buildMarathonGroupId(m.prefix, runtime.Namespace, runtime.Name)
	fetchUris, ok := m.options["FETCHURIS"]
	if !ok {
		fetchUris = defaultFetchUris
	}
	var mFetch []AppFetch
	if fetchUris != "" {
		uris := strings.Split(fetchUris, ",")
		for _, f := range uris {
			mFetch = append(mFetch, AppFetch{
				Uri: f,
			})
		}
	}
	// build apps first
	mApps := make([]App, len(runtime.Services))
	for i, service := range runtime.Services {
		// marathon AppId
		mAppId := buildMarathonAppId(mGroupId, service.Name)
		// appLevelPart VIP
		mAppLevelVip := buildMarathonVipAppLevelPart(mAppId)
		mVip := buildMarathonVip(mAppLevelVip)
		// save back vip into service spec for next work: depends resolve
		runtime.Services[i].Vip = mVip
		mApps[i] = App{
			Id:        mAppId,
			Instances: service.Scale,
			Cmd:       service.Cmd,
			Cpus:      service.Resources.Cpu,
			Mem:       service.Resources.Mem,
			Disk:      service.Resources.Disk,
			Container: AppContainer{
				Type: defaultContainerType,
				Docker: AppContainerDocker{
					Image: service.Image,
					// TODO: support a parameter to control force pull image
					ForcePullImage: false,
					Parameters: []AppContainerDockerParameter{
						{Key: "log-driver", Value: "json-file"},
						{Key: "log-opt", Value: "max-size=100m"},
						{Key: "log-opt", Value: "max-file=10"},
						{Key: "cap-add", Value: "SYS_PTRACE"},
						//{Key: "security-opt", Value: "seccomp:unconfined"},
					},
				},
				Volumes: convertVolumes(service.Binds),
			},
			Dependencies: convertDepends(mGroupId, service.Depends),
			Labels:       service.Labels,
			// backoff config: initial 10 seconds, with a factor 4, up to 3600 seconds
			BackoffSeconds:        15,
			BackoffFactor:         4,
			MaxLaunchDelaySeconds: 3600,
			// fetch uris config
			Fetch: mFetch,
		}

		originCpu := mApps[i].Cpus
		if m.cpuSubscribeRatio > 1 {
			mApps[i].Cpus = mApps[i].Cpus / float64(m.cpuSubscribeRatio)
		}

		cpuQuotaValue := int(originCpu * float64(CPU_CFS_PERIOD_US))
		if m.cpuNumQuota == 0 {
			cpuQuotaValue = 0
		} else if m.cpuNumQuota > 0 {
			cpuQuotaValue = m.cpuNumQuota * CPU_CFS_PERIOD_US
		}

		quota := strconv.Itoa(cpuQuotaValue)
		mApps[i].Container.Docker.Parameters = append(
			mApps[i].Container.Docker.Parameters,
			AppContainerDockerParameter{"cpu-quota", quota},
		)
		logrus.Debugf("app(%s) set cpu from %v to %v, cpu quota: %v", mAppId, originCpu, mApps[i].Cpus, quota)

		if len(m.backoff) > 0 {
			if b, err := strconv.ParseFloat(m.backoff, 32); err == nil {
				mApps[i].BackoffFactor = float32(b)
				logrus.Debugf("executor(%s) app(%s) backoff factor set to %v", m.name, mApps[i].Id, b)
			}
		}

		if runtime.Labels["SERVICE_TYPE"] == "ADDONS" && m.addonsDisableAutoRestart {
			mApps[i].BackoffSeconds = 3600
			logrus.Debugf("executor(%s) app(%s) backoff seconds set to 3600", m.name, mApps[i].Id)
		}

		// constraints
		serviceLabels := util.CombineLabels(runtime.Labels, service.Labels)
		mApps[i].Constraints = util.BuildDcosConstraints(m.enableTag, serviceLabels, m.preserveProjects, m.workspaceTags)
		if m.unique {
			mApps[i].Constraints = append(mApps[i].Constraints, []string{"hostname", "UNIQUE"})
		}

		// multiple version compatibility
		if lessThan(m.version, Ver{1, 5, 0}) {
			// check version lessThan 1.5.0
			mApps[i].Container.Docker.Network = defaultNetworkMode147
			mApps[i].Container.Docker.PortMappings = convertPortToPortMapping(service.Ports, mAppLevelVip)
			mApps[i].IpAddress = &AppIpAddress{
				NetworkName: defaultNetworkName,
				Labels:      make(map[string]string, 0),
				Discovery:   AppIpAddressDiscovery{Ports: []AppIpAddressDiscoveryPort{}},
				Groups:      []string{},
			}
		} else {
			mApps[i].Networks = []AppNetwork{
				{
					Mode: defaultNetworkMode,
					Name: defaultNetworkName,
				},
			}
			mApps[i].Container.PortMappings = convertPortToPortMapping(service.Ports, mAppLevelVip)
		}
		// TODO: add custom host using add-host
		vipDnsSearch := buildMarathonVipDnsSearch(mVip, service.Name)
		if vipDnsSearch != "" {
			mApps[i].Container.Docker.Parameters = append(mApps[i].Container.Docker.Parameters, AppContainerDockerParameter{
				Key: "dns-search", Value: vipDnsSearch,
			})
		}
		// TODO: refactor it, we just inject self_host (vip) to 127.0.0.1
		mApps[i].Container.Docker.Parameters = append(mApps[i].Container.Docker.Parameters, AppContainerDockerParameter{
			Key: "add-host", Value: mVip + ":127.0.0.1",
		}, AppContainerDockerParameter{
			Key: "add-host", Value: service.Name + ":127.0.0.1",
		})
		if service.Hosts != nil {
			addHosts := parseAddHost(service.Hosts)
			if addHosts != nil {
				for _, addHost := range addHosts {
					mApps[i].Container.Docker.Parameters = append(mApps[i].Container.Docker.Parameters, addHost)
				}
			}
		}
		// convert health check
		hc, err := convertHealthCheck(service, m.version)
		if err != nil {
			return nil, err
		}
		if hc != nil {
			mApps[i].HealthChecks = []AppHealthCheck{*hc}
		}
		// build basic env
		env := make(map[string]string)
		// force rotate size, default 2MB is too small
		env["CONTAINER_LOGGER_LOGROTATE_MAX_STDERR_SIZE"] = "100MB"
		env["CONTAINER_LOGGER_LOGROTATE_MAX_STDOUT_SIZE"] = "100MB"
		env["CONTAINER_LOGGER_LOGROTATE_STDERR_OPTIONS"] = "rotate 9"
		env["CONTAINER_LOGGER_LOGROTATE_STDOUT_OPTIONS"] = "rotate 9"
		for key, value := range service.Env {
			env[key] = value
		}
		if runtime.ServiceDiscoveryKind == "PROXY" {
			if mApps[i].Labels == nil {
				mApps[i].Labels = make(map[string]string)
			}
			e, ok := mApps[i].Labels["HAPROXY_GROUP"]
			if ok {
				if e == "external" {
					mApps[i].Labels["HAPROXY_GROUP"] = "external,internal"
				}
			} else {
				if runtime.Services[i].Labels["X_ENABLE_PUBLIC_IP"] == "true" && m.pubGrp != "internal" {
					mApps[i].Labels["HAPROXY_GROUP"] = m.pubGrp + ",internal"
				} else {
					mApps[i].Labels["HAPROXY_GROUP"] = "internal"
				}
			}
		}
		var selfIp string
		var selfPorts []int
		if runtime.ServiceDiscoveryKind == "PROXY" {
			selfIp = runtime.Services[i].ProxyIp
			selfPorts = runtime.Services[i].ProxyPorts
		} else {
			selfIp = runtime.Services[i].Vip
			selfPorts = runtime.Services[i].Ports
		}
		env["SELF_HOST"] = selfIp
		for i, port := range selfPorts {
			portStr := strconv.Itoa(port)
			if i == 0 {
				// special port env, SELF_PORT == SELF_PORT0
				env["SELF_PORT"] = portStr
				// TODO: we should deprecate this SELF_URL
				// TODO: we don't care what http is
				env["SELF_URL"] = "http://" + selfIp + ":" + portStr
			}
			env["SELF_PORT"+strconv.Itoa(i)] = portStr
		}
		mApps[i].Env = env
		// TODO: dirty approach for restart runtime, disable it later!
		lastRestartTime := runtime.Extra["lastRestartTime"]
		if lastRestartTime != "" {
			if mApps[i].Labels == nil {
				mApps[i].Labels = make(map[string]string)
			}
			mApps[i].Labels["LAST_RESTART_TIME"] = lastRestartTime
		}
	}
	// do resolve depends
	err := resolveDepends(&mApps, &runtime)
	if err != nil {
		return nil, err
	}
	// try expand env
	expandEnv(&mApps)
	// construct marathon Group entity
	mGroup := Group{
		Id:   mGroupId,
		Apps: mApps,
	}
	return &mGroup, nil
}

// an O(n^2) (n = len(mApps)) to resolve depends and injects env
func resolveDepends(mApps *[]App, runtime *spec.Runtime) error {
	if len(*mApps) != len(runtime.Services) {
		return errors.New("illegal args, not match len of mApps and runtime.Services")
	}
	// build dependency env
	serviceTable := make(map[string]*spec.Service)
	mAppTable := make(map[string]*App)
	serviceNames := make([]string, 0)
	for i := range runtime.Services {
		service := &runtime.Services[i]
		serviceTable[service.Name] = service
		mAppTable[service.Name] = &(*mApps)[i]
		serviceNames = append(serviceNames, service.Name)
	}
	okSet := make(map[string]bool)
	for {
		var dead = true
		for i := range runtime.Services {
			service := &runtime.Services[i]
			if okSet[service.Name] {
				// skip if already resolved depends
				continue
			}
			mApp, exists := mAppTable[service.Name]
			if !exists {
				return errors.New("illegal args, no mApp exist though the relating service existing")
			}
			if containsAll(okSet, service.Depends) {
				dead = false
				okSet[service.Name] = true
				// following the additional work, to handle env injection
				var envInjectList []string
				if runtime.ServiceDiscoveryMode == "GLOBAL" {
					envInjectList = serviceNames
				} else {
					envInjectList = service.Depends
				}
				for _, dep := range envInjectList {
					depService := serviceTable[dep]
					env := resolveDependsEnv(runtime.ServiceDiscoveryKind, depService, service)
					for k, v := range env {
						mApp.Env[k] = v
					}
				}
			}
		}
		if dead {
			return errors.New("unresolved depends")
		}
		if len(okSet) == len(*mApps) {
			break
		}
	}
	return nil
}

// TODO: need refactor, remove `thisService`s
func resolveDependsEnv(serviceDiscoveryKind string, depService *spec.Service, thisService *spec.Service) map[string]string {
	depEnvPrefix := buildServiceDiscoveryEnvPrefix(depService.Name)
	var depIp string
	var depPorts []int
	if serviceDiscoveryKind == "PROXY" {
		depIp = depService.ProxyIp
		depPorts = depService.ProxyPorts
	} else {
		depIp = depService.Vip
		depPorts = depService.Ports
	}
	env := make(map[string]string)
	env[depEnvPrefix+"_HOST"] = depIp
	for i, port := range depPorts {
		portStr := strconv.Itoa(port)
		if i == 0 {
			if thisService.Labels["IS_ENDPOINT"] == "true" {
				// check real in dep
				if containsInArray(thisService.Depends, depService.Name) {
					// TODO: we should deprecate BACKEND_URL usage
					env["BACKEND_URL"] = depIp + ":" + portStr
				}
			}
			env[depEnvPrefix+"_PORT"] = portStr
		}
		env[depEnvPrefix+"_PORT"+strconv.Itoa(i)] = portStr
	}
	return env
}

func expandEnv(mApps *[]App) {
	for i := range *mApps {
		env := &(*mApps)[i].Env
		for k, v := range *env {
			(*env)[k] = expandOneEnv(v, env)
		}
	}
}

func expandOneEnv(v string, env *map[string]string) string {
	for {
		lh := strings.Index(v, "${")
		if lh < 0 {
			break
		}
		rh := strings.Index(v, "}")
		if rh < 0 || !(lh+2 < rh) {
			// invalid env
			break
		}
		inner := v[lh+2 : rh]
		innerValue := (*env)[inner]
		v = v[:lh] + innerValue + v[rh+1:]
	}
	return v
}

func containsAll(set map[string]bool, values []string) bool {
	for _, v := range values {
		if !set[v] {
			return false
		}
	}
	return true
}

func containsInArray(array []string, value string) bool {
	for _, v := range array {
		if v == value {
			return true
		}
	}
	return false
}

func buildMarathonGroupId(prefix, namespace, name string) string {
	return strings.ToLower(prefix + "/" + namespace + "/" + name)
}

func buildMarathonAppId(mGroupId, serviceName string) string {
	return strings.ToLower(mGroupId + "/" + serviceName)
}

func buildMarathonVipAppLevelPart(mAppId string) string {
	var ret string
	for _, part := range strings.Split(mAppId, "/") {
		if part == "" {
			continue
		}
		if ret == "" {
			ret = part
		} else {
			ret = part + "." + ret
		}
	}
	return ret
}

func buildMarathonVip(appLevelPartVip string) string {
	return appLevelPartVip + defaultVipSuffix
}

func buildMarathonVipDnsSearch(vip, serviceName string) string {
	cutFrom := len(serviceName) + 1 // plus 1 to remove the dot (.)
	if cutFrom > len(vip) {
		return ""
	}
	return vip[cutFrom:]
}

func buildServiceDiscoveryEnvPrefix(serviceName string) string {
	return strings.ToUpper(strings.NewReplacer("-", "_", "/", "").Replace(serviceName))
}

func convertPortToPortMapping(ports []int, mAppLevelVip string) []AppContainerPortMapping {
	mapping := make([]AppContainerPortMapping, len(ports))
	for i, port := range ports {
		mapping[i] = AppContainerPortMapping{
			ContainerPort: port,
			// TODO: static servicePort
			// TODO: support config other protocol like udp
			Protocol: "tcp",
			// always assign a VIP whether use it or not
			Labels: map[string]string{
				"VIP_" + strconv.Itoa(i): mAppLevelVip + ":" + strconv.Itoa(port),
			},
		}
	}
	return mapping
}

func convertDepends(mGroupId string, depends []string) []string {
	deps := make([]string, len(depends))
	for i, dep := range depends {
		deps[i] = buildMarathonAppId(mGroupId, dep)
	}
	return deps
}

func convertVolumes(binds []spec.ServiceBind) []AppContainerVolume {
	volumes := make([]AppContainerVolume, len(binds))
	for i, bind := range binds {
		var mode string
		if bind.ReadOnly {
			mode = "RO"
		} else {
			mode = "RW"
		}
		volumes[i] = AppContainerVolume{
			ContainerPath: bind.ContainerPath,
			HostPath:      bind.HostPath,
			Mode:          mode,
			Persistent:    bind.Persistent,
		}
	}
	return volumes
}

// convert health check to marathon format
func convertHealthCheck(service spec.Service, ver Ver) (hc *AppHealthCheck, err error) {
	if service.NewHealthCheck != nil {
		logrus.Infof("in newhealthCheck")
		hc = new(AppHealthCheck)
		hc.GracePeriodSeconds = 0
		// 每次health check的间隔
		hc.IntervalSeconds = 15
		hc.TimeoutSeconds = 10
		// kill容器前连续的health check失败次数
		hc.MaxConsecutiveFailures = 9
		// 等待DelaySeconds秒开始健康检查
		hc.DelaySeconds = 0
		if service.NewHealthCheck.HttpHealthCheck != nil {
			hc.Protocol = "MESOS_HTTP"
			hc.Path = service.NewHealthCheck.HttpHealthCheck.Path
			hc.Port = service.NewHealthCheck.HttpHealthCheck.Port
			if service.NewHealthCheck.HttpHealthCheck.Duration > 0 {
				hc.MaxConsecutiveFailures = 1 + service.NewHealthCheck.HttpHealthCheck.Duration/hc.IntervalSeconds
			}
		} else if service.NewHealthCheck.ExecHealthCheck != nil {
			hc.Protocol = "COMMAND"
			hc.Command = &AppHealthCheckCommand{Value: service.NewHealthCheck.ExecHealthCheck.Cmd}
			if service.NewHealthCheck.ExecHealthCheck.Duration > 0 {
				hc.MaxConsecutiveFailures = 1 + service.NewHealthCheck.ExecHealthCheck.Duration/hc.IntervalSeconds
			}
		}
		return hc, nil
	}

	if service.HealthCheck == nil {
		return nil, nil
	}

	hc = new(AppHealthCheck)
	// allow app running hc.GracePeriodSeconds first, then we start health checking, and count failures
	hc.GracePeriodSeconds = 0
	hc.DelaySeconds = 0
	hc.IntervalSeconds = 15
	hc.TimeoutSeconds = 10
	hc.MaxConsecutiveFailures = 9

	switch service.HealthCheck.Kind {
	case "HTTP", "HTTPS":
		hc.Protocol = "MESOS_" + service.HealthCheck.Kind
		hc.Path = service.HealthCheck.Path
		hc.Port = service.HealthCheck.Port
	case "TCP":
		if lessThan(ver, Ver{1, 5, 0}) {
			hc.Protocol = "TCP"
		} else {
			hc.Protocol = "MESOS_TCP"
		}
		hc.Port = service.HealthCheck.Port
	default:
		hc.Protocol = "COMMAND"
		hc.Command = &AppHealthCheckCommand{Value: service.HealthCheck.Command}
	}
	return hc, err
}

func findPortIndex(ports []int, port int) (int, error) {
	for i, p := range ports {
		if p == port {
			return i, nil
		}
	}
	return -1, errors.New("port not found")
}

func parseVersion(strV string) (Ver, error) {
	v := make(Ver, 0)
	for _, x := range strings.Split(strV, ".") {
		i, err := strconv.Atoi(x)
		if err != nil {
			return nil, errors.Wrapf(err, "parse marathon version failed, %s", strV)
		}
		v = append(v, i)
	}
	return v, nil
}

func lessThan(v1, v2 Ver) bool {
	var l1, l2 = len(v1), len(v2)
	var minLen = l1
	if l2 < minLen {
		minLen = l2
	}
	for i := 0; i < minLen; i++ {
		if v1[i] == v2[i] {
			continue
		}
		return v1[i] < v2[i]
	}
	return l1 < l2
}

func parseAddHost(hosts []string) (addHosts []AppContainerDockerParameter) {
	for _, record := range hosts {
		parts := strings.SplitN(record, " ", 2)
		if len(parts) != 2 {
			// fail tolerance
			logrus.Warnf("failed to parse add-host")
			continue
		}
		addHosts = append(addHosts, AppContainerDockerParameter{"add-host", parts[1] + ":" + parts[0]})
	}
	return
}
