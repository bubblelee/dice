// +build !default

package marathon

import (
	"context"
	"encoding/json"
	"os"
	"strings"
	"testing"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/stretchr/testify/assert"
)

var marathon executortypes.Executor
var (
	specObj     spec.Runtime
	specObjDice spec.Runtime
	specObjBlog spec.Runtime
)

func TestMain(m *testing.M) {
	initMarathon()
	ret := m.Run()
	os.Exit(ret)
}

func initMarathon() {
	marathon = &Marathon{
		name:   "MARATHONFORSERVICE",
		prefix: "/zjt/runtimes/v1",
		addr:   "dcos.test.terminus.io/service/marathon",
		options: map[string]string{
			"CONSTRAINS": "dice-role:UNLIKE:platform",
		},
		version: Ver{1, 6, 0},
		client:  httpclient.New().BasicAuth("admin", "Terminus1234"),
	}
	specObj = spec.Runtime{
		Force: true,
		Dice: spec.Dice{
			Name:                 "dice-test",
			Namespace:            "default",
			ServiceDiscoveryKind: "VIP",
			Services: []spec.Service{
				{
					Name: "web",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  256,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						8080,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/dice-testweb-0229f3bada35f5e437ae0b4ddcbcb4b61524619410171",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 8080,
					},
					Env: map[string]string{
						"APP_DIR":      "/web",
						"TERMINUS_APP": "web",
					},
					Labels: map[string]string{
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "zjt-test.app.terminus.io",
					},
				},
			},
		},
	}
	specObjDice = spec.Runtime{
		Force: true,
		Dice: spec.Dice{
			Name:                 "dice",
			Namespace:            "default",
			ServiceDiscoveryKind: "VIP",
			Services: []spec.Service{
				{
					Name: "ui",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  256,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						80,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/diceui-0e63c9044c9cf8aa85b482161831a4311524808186712",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 80,
					},
					Env: map[string]string{
						"CONSOLE_URL":          "http://${CONSOLE_HOST}:${CONSOLE_PORT}",
						"ADDON_PLATFORM_URL":   "http://${ADDON_PLATFORM_HOST}:${ADDON_PLATFORM_PORT}",
						"JOB_URL":              "http://${JOB_HOST}:${JOB_PORT}",
						"MONITOR_URL":          "http://spotmonitor.marathon.l4lb.thisdcos.directory:8080",
						"OFFICIAL_ADDONS_URL":  "http://${ADDONS_HOST}:${ADDONS_PORT}",
						"PLATFORM_INSIGHT_URL": "http://${PLATFORM_INSIGHT_HOST}:${PLATFORM_INSIGHT_PORT}",
						"SSO_URL":              "http://account0.app.terminus.io",
					},
					Labels: map[string]string{
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "ui.zjt-dice.app.terminus.io",
					},
					Depends: []string{
						"console",
						//"addon-platform",
						//"addons",
						//"job",
					},
				},
				{
					Name: "console",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  512,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						8080,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/diceaddon-monitor_addon-platform_addons_console_job_platform-insight-3cfc40f613d5d3c8f197b86840c969ec1524808186712",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 8080,
					},
					Env: map[string]string{
						"APP_DIR":                "/web",
						"GITTAR_PACKER_MOBILE":   "18000000000",
						"GITTAR_URL":             "http://analyzer:dice@dicegittar.marathon.l4lb.thisdcos.directory:5566",
						"GITTAR_URL_FOR_CONSOLE": "http://gittar.app.terminus.io",
						"MONITOR_URL":            "http://monitor-web.distracted-tepig.runtimes.marathon.l4lb.thisdcos.directory:8080",
					},
				},
			},
		},
	}
	specObjBlog = spec.Runtime{
		Force: true,
		Extra: map[string]string{
			"lastRestartTime": "123",
		},
		Dice: spec.Dice{
			Name:                 "blog-test",
			Namespace:            "default",
			ServiceDiscoveryKind: "PROXY",
			ServiceDiscoveryMode: "GLOBAL",
			Labels: map[string]string{
				"MATCH_TAGS":   "service-stateless",
				"EXCLUDE_TAGS": "locked",
			},
			Services: []spec.Service{
				{
					Name: "showcase-Front",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  128,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						12300,
						8080,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/pampas-blogshowcase-front-27c4462ec7c3e5f194ae6d934874b5191527760214124",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 12300,
					},
					Env: map[string]string{
						"BACKEND_URL": "http://${BLOG_WEB_HOST}:${BLOG_WEB_PORT}",
					},
					Labels: map[string]string{
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "zjt-test-blog.test.terminus.io",
						"IS_ENDPOINT":     "true",
					},
					Depends: []string{
						"blog-web",
					},
				},
				{
					Name: "blog-web",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  384,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						12300,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/pampas-blogblog-service_user-service_blog-web-d0fda83887bef5c5f7322858e9905a571527760214121",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 12300,
					},
					Env: map[string]string{
						"APP_DIR": "/blog-web",
					},
					Depends: []string{
						"blog-service",
						"user-service",
					},
					Labels: map[string]string{
						"IS_ENDPOINT": "true",
					},
				},
				{
					Name: "blog-service",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  512,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						20880,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/pampas-blogblog-service_user-service_blog-web-d0fda83887bef5c5f7322858e9905a571527760214121",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 20880,
					},
					Env: map[string]string{
						"APP_DIR": "/blog-service/blog-service-impl",
					},
					Depends: []string{
						"user-service",
					},
				},
				{
					Name: "user-service",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  512,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						20880,
					},
					Image: "docker-registry.registry.marathon.mesos:5000/org-default/pampas-blogblog-service_user-service_blog-web-d0fda83887bef5c5f7322858e9905a571527760214121",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 20880,
					},
					Env: map[string]string{
						"APP_DIR": "/user-service/user-service-impl",
					},
				},
			},
		},
	}
	blogGlobalEnvs := map[string]string{
		"MYSQL_HOST":        "a5b76982fe584dbb99cb75691fe420b1.88.mysql.addons.marathon.l4lb.thisdcos.directory",
		"MYSQL_PORT":        "3306",
		"MYSQL_DATABASE":    "blog",
		"MYSQL_USERNAME":    "root",
		"MYSQL_PASSWORD":    "5LdLMKp7BXhAE4f2",
		"REDIS_HOST":        "464ee4073334409abb7052a7bcfec0c7.88.redis.addons.marathon.l4lb.thisdcos.directory",
		"REDIS_PORT":        "6379",
		"REDIS_PASSWORD":    "c07EbrChTCsvRuTK",
		"ZOOKEEPER_HOST":    "38591d3a10b343f5aa3d2149b93b6477.88.zookeeper.addons.marathon.l4lb.thisdcos.directory",
		"ZOOKEEPER_PORT":    "2181",
		"JAVA_OPTS":         " -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1617 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false ",
		"TERMINUS_APP_NAME": "PAMPAS_BLOG",
		"TRACE_SAMPLE":      "1",
	}
	for i := range specObjBlog.Services {
		for k, v := range blogGlobalEnvs {
			specObjBlog.Services[i].Env[k] = v
		}
	}
}

func TestCreate(t *testing.T) {
	ctx := context.Background()
	_, err := marathon.Create(ctx, specObj)
	if err != nil {
		t.Error(err)
	}
}

func TestCreatePvExplore(t *testing.T) {
	ctx := context.Background()
	str := `{
  "executor": "MARATHONFORTERMINUS",
  "version": "",
  "name": "mysql-addon-fail",
  "namespace": "test",
  "services": [
    {
      "name": "mysql",
      "image": "registry.cn-hangzhou.aliyuncs.com/terminus/dice-mysql:1.3.0",
      "ports": [
        3306
      ],
      "scale": 1,
      "resources": {
        "cpu": 0.01,
        "mem": 512,
        "disk": 0
      },
      "env": {
        "MYSQL_DATABASE": "dice",
        "MYSQL_ROOT_PASSWORD": "Hello1234"
      },
      "binds": [
        {
          "containerPath": "/var/lib/mysql",
          "hostPath": "mysql-data"
        },
        {
          "containerPath": "mysql-data",
          "persistent": {
            "type": "root",
            "size": 1024
          }
        }
      ],
      "healthCheck": {
        "Kind": "TCP",
        "Port": 3306
      }
    }
  ],
  "serviceDiscoveryKind": "PROXY",
  "serviceDiscoveryMode": "GLOBAL"
}
`

	var obj spec.Runtime
	json.NewDecoder(strings.NewReader(str)).Decode(&obj)
	//marathon.Destroy(ctx, obj)
	_, err := marathon.Create(ctx, obj)
	assert.NoError(t, err)
}

func TestDestroy(t *testing.T) {
	ctx := context.Background()
	err := marathon.Destroy(ctx, specObj)
	if err != nil {
		t.Error(err)
	}
}

func TestUpdateSingle(t *testing.T) {
	ctx := context.Background()
	resp, err := marathon.Update(ctx, specObj)
	if err != nil {
		t.Error(err)
	}
	assert.Nil(t, resp)
}

func TestUpdateMulti(t *testing.T) {
	ctx := context.Background()
	resp, err := marathon.Update(ctx, specObjDice)
	if err != nil {
		t.Error(err)
	}
	assert.Nil(t, resp)
}

func TestCreateBlog(t *testing.T) {
	ctx := context.Background()
	resp, err := marathon.Create(ctx, specObjBlog)
	assert.NoError(t, err)
	assert.Nil(t, resp)
}

func TestUpdateBlog(t *testing.T) {
	ctx := context.Background()
	resp, err := marathon.Update(ctx, specObjBlog)
	assert.NoError(t, err)
	assert.Nil(t, resp)
}

func TestDestroyBlog(t *testing.T) {
	ctx := context.Background()
	err := marathon.Destroy(ctx, specObjBlog)
	if err != nil {
		t.Error(err)
	}
}

func TestQueryStatusBlog(t *testing.T) {
	ctx := context.Background()
	status, err := marathon.Status(ctx, specObjBlog)
	if err != nil {
		t.Error(err)
	}
	t.Log(status)
	assert.NotNil(t, status)
}

func TestInspectBlog(t *testing.T) {
	ctx := context.Background()
	ret, err := marathon.Inspect(ctx, specObjBlog)
	if err != nil {
		t.Error(err)
	}
	t.Log(ret)
	t.Log(ret.(spec.Runtime).StatusDesc)
	for _, s := range ret.(spec.Runtime).Services {
		t.Log(s.Name, s.StatusDesc)
	}
	assert.NotNil(t, ret)
}

func TestExpandOneEnv(t *testing.T) {
	ret := expandOneEnv("http://${CONSOLE_HOST}:${CONSOLE_PORT}", &map[string]string{
		"CONSOLE_HOST": "localhost",
		"CONSOLE_PORT": "8081",
	})
	assert.Equal(t, "http://localhost:8081", ret)
}

func TestConvertVolumes(t *testing.T) {
	binds := []spec.ServiceBind{
		{
			Bind: spec.Bind{
				ContainerPath: "/var",
				HostPath:      "/tmp/var",
				ReadOnly:      false,
			},
			Persistent: &spec.PersistentVolume{
				Type: "mount",
				Size: 100,
			},
		},
	}
	expect := []AppContainerVolume{
		{
			ContainerPath: "/var",
			HostPath:      "/tmp/var",
			Mode:          "RW",
			Persistent: &spec.PersistentVolume{
				Type: "mount",
				Size: 100,
			},
		},
	}
	assert.Equal(t, expect, convertVolumes("/runtimes/v1/services/xxx", binds, nil))

	// case 2: using local volume
	volumes := []spec.Volume{
		{
			VolumeType:    "local",
			ContainerPath: "/var/volume/mysql",
			Storage:       "5Gi",
		},
	}
	expect = append(expect, AppContainerVolume{
		ContainerPath: "/var/volume/mysql",
		HostPath:      "varvolumemysql",
		Mode:          "RW",
	}, AppContainerVolume{
		ContainerPath: "varvolumemysql",
		Mode:          "RW",
		Persistent: &spec.PersistentVolume{
			Type: "root",
			Size: 5120,
		},
	})
	assert.Equal(t, expect, convertVolumes("/runtimes/v1/services/xxx", binds, volumes))

	// case 3: append cloud volume
	volumes = append(volumes, spec.Volume{
		VolumeType:    "cloud",
		ContainerPath: "/var/cloud/mysql",
		Storage:       "7Ti",
	})
	expect = append(expect, AppContainerVolume{
		ContainerPath: "/var/cloud/mysql",
		HostPath:      "/netdata/volumes/runtimes/v1/services/xxx/var/cloud/mysql",
		Mode:          "RW",
	})
	assert.Equal(t, expect, convertVolumes("/runtimes/v1/services/xxx", binds, volumes))
}

func TestParseMarathonVersion(t *testing.T) {
	var ver, err = parseVersion("1.5.0")
	assert.NoError(t, err)
	assert.Equal(t, Ver{1, 5, 0}, ver)

	ver, err = parseVersion("1.6.222")
	assert.NoError(t, err)
	assert.Equal(t, Ver{1, 6, 222}, ver)

	ver, err = parseVersion("1.3")
	assert.NoError(t, err)
	assert.Equal(t, Ver{1, 3}, ver)

	ver, err = parseVersion("1")
	assert.NoError(t, err)
	assert.Equal(t, Ver{1}, ver)

	ver, err = parseVersion("1..")
	assert.Error(t, err)
}

func TestCompareVersion(t *testing.T) {
	assert.True(t, lessThan(Ver{1, 3, 2}, Ver{1, 3, 3}))
	assert.False(t, lessThan(Ver{1, 4, 0}, Ver{1, 4, 0}))
	assert.False(t, lessThan(Ver{1, 4, 1}, Ver{1, 4, 0}))

	assert.True(t, lessThan(Ver{1, 2}, Ver{1, 2, 1}))
	assert.True(t, lessThan(Ver{1, 2, 9}, Ver{1, 3}))
}

func TestBuildMarathonGroupIdAndAppId(t *testing.T) {
	assert.Equal(t, "/runtime/v1/kdjkla/thename", buildMarathonGroupId("/RunTime/V1", "kdjKla", "theName"))
	assert.Equal(t, "/mygroupid/myservicename", buildMarathonAppId("/MyGroupId", "MyServiceName"))
}

func TestConvertPortToPortMapping(t *testing.T) {
	mappings := convertPortToPortMapping([]int{8080, 8090, 9090}, "test.vip.haha.com")
	assert.Equal(t, []AppContainerPortMapping{
		{
			Labels: map[string]string{
				"VIP_0": "test.vip.haha.com:8080",
			},
			Protocol:      "tcp",
			ContainerPort: 8080,
		},
		{
			Labels: map[string]string{
				"VIP_1": "test.vip.haha.com:8090",
			},
			Protocol:      "tcp",
			ContainerPort: 8090,
		},
		{
			Labels: map[string]string{
				"VIP_2": "test.vip.haha.com:9090",
			},
			Protocol:      "tcp",
			ContainerPort: 9090,
		},
	}, mappings)
}

func TestConvertHealthCheck(t *testing.T) {
	ahc, err := convertHealthCheck(spec.Service{
		HealthCheck: &spec.HealthCheck{
			Kind: "TCP",
		},
	}, Ver{1, 4, 7})
	assert.NoError(t, err)
	assert.Equal(t, AppHealthCheck{
		GracePeriodSeconds:     120,
		IntervalSeconds:        5,
		MaxConsecutiveFailures: 3,
		TimeoutSeconds:         10,
		Protocol:               "TCP",
	}, *ahc)

	ahc2, err2 := convertHealthCheck(spec.Service{
		HealthCheck: &spec.HealthCheck{
			Kind: "TCP",
		},
	}, Ver{1, 6, 0})
	assert.NoError(t, err2)
	assert.Equal(t, AppHealthCheck{
		GracePeriodSeconds:     120,
		IntervalSeconds:        5,
		MaxConsecutiveFailures: 3,
		TimeoutSeconds:         10,
		Protocol:               "MESOS_TCP",
	}, *ahc2)
}

func TestParseAddHost(t *testing.T) {
	assert.Nil(t, parseAddHost(nil))

	assert.Nil(t, parseAddHost([]string{}))

	assert.Equal(t, []AppContainerDockerParameter{
		{"add-host", "dns.google.com:8.8.8.8"},
	}, parseAddHost([]string{"8.8.8.8 dns.google.com"}))

	assert.Equal(t, []AppContainerDockerParameter{
		{"add-host", "baidu.com google.com:127.0.0.1"},
	}, parseAddHost([]string{"127.0.0.1 baidu.com google.com"}))

	assert.Equal(t, []AppContainerDockerParameter{
		{"add-host", "dns.google.com:8.8.8.8"},
		{"add-host", "baidu.com google.com:127.0.0.1"},
	}, parseAddHost([]string{
		"8.8.8.8 dns.google.com",
		"127.0.0.1 baidu.com google.com"}))
}

func TestNumericalize(t *testing.T) {
	ret, err := numericalize("128974848")
	assert.NoError(t, err)
	assert.Equal(t, int64(128974848), ret)

	ret, err = numericalize("129K")
	assert.NoError(t, err)
	assert.Equal(t, int64(129000), ret)

	ret, err = numericalize("123Ki")
	assert.NoError(t, err)
	assert.Equal(t, int64(125952), ret)

	ret, err = numericalize("129M")
	assert.NoError(t, err)
	assert.Equal(t, int64(129000000), ret)

	ret, err = numericalize("123Mi")
	assert.NoError(t, err)
	assert.Equal(t, int64(128974848), ret)

	ret, err = numericalize("129G")
	assert.NoError(t, err)
	assert.Equal(t, int64(129000000000), ret)

	ret, err = numericalize("123Gi")
	assert.NoError(t, err)
	assert.Equal(t, int64(132070244352), ret)

	ret, err = numericalize("129T")
	assert.NoError(t, err)
	assert.Equal(t, int64(129000000000000), ret)

	ret, err = numericalize("123Ti")
	assert.NoError(t, err)
	assert.Equal(t, int64(135239930216448), ret)

	ret, err = numericalize("129P")
	assert.NoError(t, err)
	assert.Equal(t, int64(129000000000000000), ret)

	ret, err = numericalize("123Pi")
	assert.NoError(t, err)
	assert.Equal(t, int64(138485688541642752), ret)

	ret, err = numericalize("1E")
	assert.NoError(t, err)
	assert.Equal(t, int64(1000000000000000000), ret)

	ret, err = numericalize("1Ei")
	assert.NoError(t, err)
	assert.Equal(t, int64(1152921504606846976), ret)
}

func TestBuildVolumeCloudHostPath(t *testing.T) {
	assert.Equal(t, "/netdata/volumes/haha/hehe", buildVolumeCloudHostPath("/netdata/volumes", "haha", "hehe"))
	assert.Equal(t, "/netdata/volumes/haha/hehe", buildVolumeCloudHostPath("/netdata/volumes", "haha", "/hehe"))
}
