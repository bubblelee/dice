package marathon

import (
	"bufio"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/pkg/jsonstore/storetypes"
	"terminus.io/dice/dice/pkg/loop"
	_ "terminus.io/dice/dice/pkg/monitor"
	"terminus.io/dice/dice/scheduler/events"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type MIpas struct {
	IpAddress string `json:"ipAddress"`
}

type MarathonStatusUpdateEvent struct {
	TaskStatus  string  `json:"taskStatus"`
	IpAddresses []MIpas `json:"ipAddresses"`
	AppId       string  `json:"appId"`
	TaskId      string  `json:"taskId"`
}

type MarathonHealthStatusChangedEvent struct {
	InstanceId string `json:"instanceId"`
	Alive      bool   `json:"alive"`
}

// TODO: 需要考虑marathon集群重启、异常等情况重新建连
// TODO: 定时从marathon api上获取状态（聚合计算层实现）
func (m *Marathon) WaitEvent(options map[string]string) {
	// 等待5到30秒开始监听事件
	waitSeconds := 2 + rand.Intn(5)
	logrus.Infof("executor(name:%s, addr:%s) in WaitEvent, sleep %vs", m.name, m.addr, waitSeconds)
	time.Sleep(time.Duration(waitSeconds) * time.Second)

	defer logrus.Infof("executor(%s) WaitEvent exited", m.name)
	// 不用m.client，避开其超时时间等制约条件
	var req *http.Request
	var err error
	req, err = http.NewRequest("GET", "http://"+m.addr+"/v2/events", nil)
	if err != nil {
		logrus.Errorf("construct WaitEvent http request err: %v", err)
		return
	}

	client := http.DefaultClient

	if _, ok := options["CA_CRT"]; ok {
		logrus.Infof("marathon event got executor(%s) addr: %v", m.name, m.addr)
		keyPair, ca := withHttpsCertFromJSON([]byte(options["CLIENT_CRT"]),
			[]byte(options["CLIENT_KEY"]),
			[]byte(options["CA_CRT"]))

		client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      ca,
				Certificates: []tls.Certificate{keyPair},
			},
		}

		req, err = http.NewRequest("GET", m.addr+"/v2/events", nil)
		if err != nil {
			logrus.Errorf("construct WaitEvent https request err: %v", err)
			return
		}
		req.Proto = "https"
	}

	q := req.URL.Query()
	q.Add("event_type", "status_update_event")
	q.Add("event_type", "health_status_changed_event")
	req.URL.RawQuery = q.Encode()

	logrus.Infof("event req.URL is: %s", req.URL.String())

	basicAuth := options["BASICAUTH"]
	if len(basicAuth) > 0 {
		basticStr := base64.StdEncoding.EncodeToString([]byte(basicAuth))
		req.Header.Set("Authorization", "Basic "+basticStr)
	}

	req.Header.Set("Accept", "text/event-stream")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Transfer-Encoding", "chunked")

	eventHandler := func() error {
		// default client is no timeout
		resp, err := client.Do(req)
		if err != nil {
			logrus.Errorf("[alert] executor(%s) failed to request the marathon events (%v)", m.name, err)
			return nil
		}

		reader := bufio.NewReader(resp.Body)
		// marathon事件一次来两行，格式为
		// event: status_update_event
		// data: {"slaveId":"2aa23eb3-fa5c-4248-a203-ba4ddb9fb562-S2",...}
		// 除开事件本身会一直有空行进来
		var eventType string
		for {
			line, err := reader.ReadBytes('\n')
			if err != nil {
				logrus.Errorf("[alert] executor(%s) failed to read data from marathon (%v)", m.name, err)
				break
			}

			// 先判断出事件类型
			if len(line) > 7 && string(line[:5]) == "event" {
				eventType = string(line[7:])
				eventType = strings.TrimSuffix(eventType, "\r\n")
				//logrus.Infof("next eventType is %s", eventType)
				continue
			}

			if len(line) <= 6 || string(line[:4]) != "data" {
				//logrus.Errorf("in parsing event comes illegal line: %s", string(line))
				continue
			}

			// 解析出事件内容
			content := line[6:]

			switch eventType {
			case "status_update_event":
				ev := MarathonStatusUpdateEvent{}
				if err := json.Unmarshal(content, &ev); err != nil {
					logrus.Errorf("unmarshal event string(%s) error: %v", string(content), err)
					continue
				}

				// 丢弃非用户服务的事件
				// console告知丢弃addon事件，后续addon事件会是不同的url
				// 或者状态为unknown事件/unreachable事件
				//if !strings.HasPrefix(ev.TaskId, "runtimes") || strings.Contains(ev.TaskId, "runtimes_v1_addons") {
				if !strings.HasPrefix(ev.TaskId, "runtimes") ||
					strings.HasPrefix(ev.TaskId, "runtimes_v1_addons") ||
					ev.TaskStatus == events.UNKNOWN ||
					ev.TaskStatus == events.UNREACHABLE {
					//logrus.Infof("drop marathon ev: %v", ev)
					continue
				}

				statusEvent := spec.StatusEvent{
					Type:    eventType,
					ID:      ev.AppId,
					Status:  ev.TaskStatus,
					TaskId:  ev.TaskId,
					Cluster: string(m.name),
				}
				if len(ev.IpAddresses) > 0 {
					statusEvent.IP = ev.IpAddresses[0].IpAddress
				}
				logrus.Debugf("going to send status update ev: %+v", statusEvent)
				m.evCh <- &statusEvent

			case "health_status_changed_event":
				ev := MarathonHealthStatusChangedEvent{}
				if err := json.Unmarshal(content, &ev); err != nil {
					logrus.Errorf("unmarshal event string(%s) error: %v", string(content), err)
					continue
				}

				// 丢弃非用户服务的事件
				if !strings.HasPrefix(ev.InstanceId, "runtimes") {
					//logrus.Infof("drop marathon ev: %v", ev)
					continue
				}

				// health_status_changed_event 的 instanceId 格式：
				// "instanceId":"runtimes_v1_services_mydev-590_user-service.marathon-56153805-9004-11e8-aaac-70b3d5800001"

				// status_update_event 的 taskId格式
				// "taskId":"runtimes_v1_services_mydev-590_user-service.56153805-9004-11e8-aaac-70b3d5800001"
				// 所以需要将health_status_changed_event 的 instanceId的格式转化一下，即去掉"marathon-"
				statusEvent := spec.StatusEvent{
					Type:    eventType,
					TaskId:  modifyHealthEventId(ev.InstanceId),
					Cluster: string(m.name),
				}
				if ev.Alive {
					statusEvent.Status = "Healthy"
				} else {
					statusEvent.Status = "UnHealthy"
				}
				logrus.Debugf("going to send health status changed ev: %+v", statusEvent)
				m.evCh <- &statusEvent
			}
		}

		resp.Body.Close()
		return nil
	}

	retryloop := loop.New(loop.WithDeclineRatio(2), loop.WithDeclineLimit(time.Second*60))
	retryloop.Do(eventHandler)
}

func modifyHealthEventId(eid string) string {
	if strings.Contains(eid, "marathon-") {
		return strings.Replace(eid, "marathon-", "", 1)
	}
	logrus.Errorf("health_status_changed_event instanceId(%s) not contains marathon- ", eid)
	return eid
}

func withHttpsCertFromJSON(certFile, keyFile, cacrt []byte) (tls.Certificate, *x509.CertPool) {
	pair, err := tls.X509KeyPair(certFile, keyFile)
	if err != nil {
		logrus.Fatal("LoadX509KeyPair: %v", err)
	}

	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(cacrt)

	return pair, pool
}

// 对etcd里的原始key做个处理
// /dice/service/services/staging-77 -> "services/staging-77"
func etcdKeyToMapKey(eKey string) string {
	fields := strings.Split(eKey, "/")
	if l := len(fields); l > 2 {
		return fields[l-2] + "/" + fields[l-1]
	}
	return ""
}

func registerEventChanAndLocalStore(name string, evCh chan *spec.StatusEvent, lstore *sync.Map) {
	// watch 特定etcd目录的事件的处理函数
	syncRuntimeToEvent := func(key string, value interface{}, t storetypes.ChangeType) error {
		//logrus.Infof("executor(%s) going to sync key: %s", name, key)

		runtimeName := etcdKeyToMapKey(key)
		if len(runtimeName) == 0 {
			return nil
		}

		// 先处理delete的事件
		if t == storetypes.Del {
			// TODO: 先不删除key，删除了的话事件来了找不到对应的结构体，不好发事件
			// TODO: 后面可以置个标志位，然后开个协程定时清理
			// lstore.Delete(runtimeName)
			de_, ok := lstore.Load(runtimeName)
			if ok {
				de := de_.(events.RuntimeEvent)
				de.IsDeleted = true
				lstore.Store(runtimeName, de)
			}
			logrus.Infof("key(%s) in deleted event", key)
			return nil
		}

		run := value.(*spec.Runtime)

		// 过滤不属于本executor的事件
		if run.Executor != name {
			return nil
		}

		event := events.RuntimeEvent{
			RuntimeName: runtimeName,
		}

		switch t {
		// Added event
		case storetypes.Add:
			logrus.Debugf("key(%s) in added event", key)
			event.ServiceStatuses = make([]events.ServiceStatus, len(run.Services))
			for i, srv := range run.Services {
				event.ServiceStatuses[i].ServiceName = srv.Name
				event.ServiceStatuses[i].Replica = srv.Scale
				// 当前实例信息不可知
				event.ServiceStatuses[i].InstanceStatuses = make([]events.InstanceStatus, len(srv.InstanceInfos))
			}

			lstore.Store(runtimeName, event)

		// Updated event
		case storetypes.Update:
			logrus.Debugf("key(%s) in updated event", key)
			oldEvent, ok := lstore.Load(runtimeName)
			if !ok {
				logrus.Errorf("key(%s) updated but not found related key in lstore", key)
				return nil
			}
			// 判断service个数是否改变，如考虑新增N个，删除M个情形
			for _, newS := range run.Services {
				found := false
				for _, oldS := range oldEvent.(events.RuntimeEvent).ServiceStatuses {
					if oldS.ServiceName == newS.Name {
						found = true
						event.ServiceStatuses = append(event.ServiceStatuses, oldS)
						// 判断是否是任何一个service里的instance个数发生变化, 只有扩容、缩容两种情况
						// 扩容
						if oldS.Replica < newS.Scale {
							i := 0
							for i < newS.Scale-oldS.Replica {
								i++
								event.ServiceStatuses[len(event.ServiceStatuses)-1].InstanceStatuses = append(
									event.ServiceStatuses[len(event.ServiceStatuses)-1].InstanceStatuses, events.InstanceStatus{})
							}
							event.ServiceStatuses[len(event.ServiceStatuses)-1].Replica = newS.Scale
						} else if len(oldS.InstanceStatuses) > newS.Scale { // 缩容
							// 根据marathon的策略"killSelection": "YOUNGEST_FIRST", 新的实例会先被杀
							// 而比较新的实例会被添加在slice的后面
							event.ServiceStatuses[len(event.ServiceStatuses)-1].Replica = newS.Scale
						}
						break
					}
				}

				// 未找到说明newS是需要新增的service, 不需考虑instance个数变化
				if !found {
					event.ServiceStatuses = append(event.ServiceStatuses, events.ServiceStatus{
						ServiceName:      newS.Name,
						Replica:          newS.Scale,
						InstanceStatuses: make([]events.InstanceStatus, newS.Scale),
					})
				}
			}

			lstore.Store(runtimeName, event)
		}

		if t != storetypes.Del {
			logrus.Debugf("executor(%s) stored runtime(%s) event to lstore: %+v", name, runtimeName, event)
		}

		return nil
	}

	// 将注册来的executor的name及其event channel对应起来
	getEvChanFn := func(executorName executortypes.Name) (chan *spec.StatusEvent, *sync.Map, error) {
		logrus.Infof("in RegisterEvChan executor(%s)", name)
		if string(executorName) == name {
			return evCh, lstore, nil
		}
		return nil, nil, errors.Errorf("this is for %s executor, not %s", executorName, name)
	}

	executortypes.RegisterEvChan(executortypes.Name(name), getEvChanFn, syncRuntimeToEvent)
}

func (m *Marathon) initEventAndPeriodSync(name string, lstore *sync.Map) error {
	start := time.Now()
	defer func() {
		logrus.Infof("in initEventAndPeriodSync executor(%s) took %v", name, time.Since(start))
	}()
	eventAddr, ok := os.LookupEnv("CONSOLE_EVENT_ADDR")
	if !ok {
		eventAddr = events.CONSOLE_EVENT_ADDR
	}
	notifier, err := api.New(name+events.SUFFIX_INIT, map[string]interface{}{"HTTP": []string{eventAddr}})
	if err != nil {
		return errors.Errorf("new eventbox api error when executor(%s) init and send batch events", name)
	}

	isInit := true
	// 第一次初始化和周期性更新
	initRuntimeEventStore := func(k string, v interface{}) error {
		r := v.(*spec.Runtime)
		if r.Executor != string(name) {
			return nil
		}
		r2_, err := m.Inspect(context.Background(), *r)
		if err != nil {
			logrus.Errorf("executor(%s)'s key(%s) get status error: %v", name, k, err)
			return nil
		}
		r2 := r2_.(*spec.Runtime)
		reKey := etcdKeyToMapKey(k)
		if len(reKey) == 0 {
			return nil
		}

		var e events.RuntimeEvent
		e.RuntimeName = reKey
		e.ServiceStatuses = make([]events.ServiceStatus, len(r2.Services))
		for i := range r2.Services {
			e.ServiceStatuses[i].ServiceName = r2.Services[i].Name
			e.ServiceStatuses[i].Replica = r2.Services[i].Scale
			e.ServiceStatuses[i].ServiceStatus = string(r2.Services[i].Status)
			e.ServiceStatuses[i].InstanceStatuses = make([]events.InstanceStatus, len(r2.Services[i].InstanceInfos))

			if r2.Services[i].Scale != len(r2.Services[i].InstanceInfos) {
				logrus.Errorf("service(%s) scale(%v) not matched its instances number(%v)",
					r2.Services[i].Name, r2.Services[i].Scale, len(r2.Services[i].InstanceInfos))
			}

			for j, instance := range r2.Services[i].InstanceInfos {

				// instance.Id格式 runtimes_v1_services_staging-821_web.ca3113d1-9531-11e8-ad54-70b3d5800001
				// runtimes_v1_services_staging-821_web.ca3113d1-9531-11e8-ad54-70b3d5800001.1
				e.ServiceStatuses[i].InstanceStatuses[j].ID = instance.Id
				e.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = convertStatus(instance.Status)
				e.ServiceStatuses[i].InstanceStatuses[j].Ip = instance.Ip
			}
		}

		e.EventType = events.EVENTS_TOTAL

		if isInit {
			go notifier.Send(e)
		} else {
			go notifier.Send(e, api.WithSender(string(name)+events.SUFFIX_PERIOD))
		}

		lstore.Store(reKey, e)
		//logrus.Debugf("executor(%s) stored event(%s) in initEventAndPeriodSync and going to send that ev, lstore: %p", name, e.RuntimeName, lstore)

		return nil
	}

	// 每个executor被初始化的时候去jsonstore里初始化一把元数据作用：
	// 1, 拿到隶属于该executor的所有runtime的状态信息, 作为后续计算增量事件的结构基础
	// 2, 发送初始化状态事件(全量事件)
	em := events.GetEventManager()

	if err = em.MemEtcdStore.ForEach(context.Background(), "/dice/service/", spec.Runtime{}, initRuntimeEventStore); err != nil {
		logrus.Errorf("executor(%s) foreach initRuntimeEventStore error: %v", name, err)
	}

	// 定期补偿状态
	go func() {
		isInit = false
		for {
			select {
			case <-time.After(10 * time.Minute):
				logrus.Infof("executor(%s) periodically sync status form Marathon Status interface", name)

				if err = em.MemEtcdStore.ForEach(context.Background(), "/dice/service/", spec.Runtime{}, initRuntimeEventStore); err != nil {
					logrus.Errorf("executor(%s) foreach initRuntimeEventStore error: %v", name, err)
				}
			}
		}
	}()

	return nil
}

// Status接口通过marathon拿到实例的原生状态，转成RuntimeEvent中对应实例的状态
func convertStatus(taskStatus string) string {
	switch taskStatus {
	case events.RUNNING:
		// TODO: Healthy or Running?
		return "Healthy"

	case events.KILLED:
		return "Killed"

	case events.FINISHED:
		return "Finished"

	case events.FAILED:
		return "Failed"

	case events.KILLING:
		return "Killing"

	case events.STARTING:
		return "Starting"

	case events.STAGING:
		return "Staging"

	default:
		if len(taskStatus) > 0 {
			return taskStatus
		}
		return "Unknown"
	}
}
