package marathon

import (
	"bufio"
	"encoding/base64"
	"net/http"
	"testing"

	"github.com/sirupsen/logrus"
)

func TestRetry(t *testing.T) {
	req, err := http.NewRequest("GET", "http://dcos.z.terminus.io/service/marathon/v2/events", nil)
	if err != nil {
		logrus.Errorf("construct WaitEvent http request err: %v", err)
		return
	}
	q := req.URL.Query()
	q.Add("event_type", "status_update_event")
	q.Add("event_type", "health_status_changed_event")
	req.URL.RawQuery = q.Encode()

	logrus.Infof("event req.URL is: %s", req.URL.String())

	basicAuth := "admin:Terminus1234"
	if len(basicAuth) > 0 {
		basticStr := base64.StdEncoding.EncodeToString([]byte(basicAuth))
		req.Header.Set("Authorization", "Basic "+basticStr)
	}

	req.Header.Set("Accept", "text/event-stream")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Transfer-Encoding", "chunked")

	retryTimes := 0

	for retryTimes < 5 {
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			logrus.Errorf("do req err: %v", err)
			retryTimes++
			continue
		}

		reader := bufio.NewReader(resp.Body)
		for {
			_, err := reader.ReadBytes('\n')
			if err != nil {
				retryTimes++
				break
			}
		}
		resp.Body.Close()
	}

}
