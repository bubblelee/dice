package k8s

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/executor/util"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	kind = "K8S"
)

var deleteOptions = &CascadingDeleteOptions{
	Kind:       "DeleteOptions",
	ApiVersion: "v1",
	// 'Foreground' - a cascading policy that deletes all dependents in the foreground
	// e.g. if you delete a deployment, this option would delete related replicaSets and pods
	// See more: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#delete-24
	PropagationPolicy: "Foreground",
}

// K8S plugin's configure
//
// EXECUTOR_K8S_K8SFORSERVICE_ADDR=http://127.0.0.1:8080
func init() {
	executortypes.Register(kind, func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		addr, ok := options["ADDR"]
		if !ok {
			return nil, errors.Errorf("not found k8s address in env variables")
		}

		client := httpclient.New()
		basicAuth, ok := options["BASICAUTH"]
		if ok {
			namePassword := strings.Split(basicAuth, ":")
			if len(namePassword) == 2 {
				client.BasicAuth(namePassword[0], namePassword[1])
			}
		}

		return &Kubernetes{
			name:    name,
			options: options,
			addr:    addr,
			client:  client,
		}, nil
	})
}

type Kubernetes struct {
	name    executortypes.Name
	options map[string]string
	addr    string
	client  *httpclient.HTTPClient
}

func (m *Kubernetes) Kind() executortypes.Kind {
	return kind
}

func (m *Kubernetes) Name() executortypes.Name {
	return m.name
}

func tmpHack(runtime *spec.Runtime) {
	logrus.Debugf("in tmpHack")
	for _, svc := range runtime.Services {
		svc.Image = strings.Replace(svc.Image,
			"docker-registry.registry.marathon.mesos:5000",
			"hub.docker.test.terminus.io:80",
			-1)
		logrus.Debugf("tmpHack image: %s", svc.Image)
	}
}

// TODO: Complete me.
func validateAndGetRuntime(specObj interface{}, action string) (*spec.Runtime, error) {
	runtime, ok := specObj.(spec.Runtime)
	if !ok || len(runtime.Services) == 0 {
		return nil, errors.Errorf("%s: invalid service spec", action)
	}

	if strings.Contains(runtime.Namespace, "--") {
		return nil, errors.Errorf("%s: runtime(%s)'s namespace(%s) cannot contain consecutive dash(--)",
			action, runtime.Name, runtime.Namespace)
	}
	k8sNS := generateK8SNamespace(&runtime)
	// init runtime.Services' namespace for later usage
	for i := range runtime.Services {
		runtime.Services[i].Namespace = k8sNS
	}
	return &runtime, nil
}

func (m *Kubernetes) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, err := validateAndGetRuntime(specObj, "Create")
	if err != nil {
		return nil, err
	}

	tmpHack(runtime)
	if _, err = m.createRuntime(runtime); err != nil {
		return nil, err
	}

	logrus.Debugf("Create runtime(%s/%s) successfully", runtime.Namespace, runtime.Name)
	return nil, nil
}

func (m *Kubernetes) Destroy(ctx context.Context, specObj interface{}) error {
	runtime, err := validateAndGetRuntime(specObj, "Destroy")
	if err != nil {
		return err
	}

	k8sNS := generateK8SNamespace(runtime)
	err, existed := m.destroyRuntime(k8sNS)
	if err != nil {
		logrus.Errorf("destroy k8s namespace(%s) failed, err: %v", k8sNS, err)
		return err
	}
	if !existed {
		logrus.Debugf("k8s namespace(%s) not found or already deleted", k8sNS)
	}
	logrus.Debugf("Destroy runtime(%s/%s) successfully", runtime.Namespace, runtime.Name)
	return nil
}

func (m *Kubernetes) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	var runtimeStatus spec.StatusDesc
	runtime, err := validateAndGetRuntime(specObj, "Status")
	if err != nil {
		return runtimeStatus, err
	}

	// init "unknown" status for each service
	for i := range runtime.Services {
		runtime.Services[i].Status = spec.StatusUnkonw
	}

	for i, svc := range runtime.Services {
		// 要区分出下面的异常：
		// 1, 创建过程中发生错误，整个runtime被删掉了后再来查询
		// 2, 其他
		status, err := m.getDeploymentStatus(&svc)
		if err != nil {
			// TODO: 状态可改成"Error"..
			runtimeStatus.Status = spec.StatusUnkonw

			if err.Error() == string(DeploymentNotFound) {
				k8sNS := generateK8SNamespace(runtime)
				existed, err := m.getNamespaceExisted(k8sNS)
				if err != nil {
					logrus.Errorf("Status: getNamespaceExisted error: %v", err)
					runtimeStatus.LastMessage = fmt.Sprintf("get namespace(%s) failed with err: %v", k8sNS, err)
					return runtimeStatus, err
				}

				// namespace不存在说明创建途中有错误，runtime已被调度器删除
				if !existed {
					runtimeStatus.Status = spec.StatusErrorAndDeleted
					runtimeStatus.LastMessage = fmt.Sprintf("Namespace(%s) not found, probably deleted", k8sNS)
				} else {
					// 理论上只会出现在删除namespace过程中，某个deployment已被删除而namespace处于terminating状态并即将被删除
					runtimeStatus.Status = spec.StatusUnkonw
					runtimeStatus.LastMessage = fmt.Sprintf("Namespace(%s) exists but deployment(%s) not found",
						svc.Namespace, svc.Name)
				}
			}
			return runtimeStatus, err
		}
		if status.Status != spec.StatusReady {
			runtimeStatus.Status = spec.StatusFailing
			runtimeStatus.LastMessage = fmt.Sprintf("deployment(%s) status is %v, with err:%v",
				runtime.Name, status, err)
			return runtimeStatus, nil
		}

		runtime.Services[i].Status = spec.StatusReady
	}

	runtimeStatus.Status = spec.StatusReady
	return runtimeStatus, nil
}

func (m *Kubernetes) Remove(ctx context.Context, specObj interface{}) error {
	// TODO: currently as same as Destroy
	return m.Destroy(ctx, specObj)
}

// 暂不支持更新云盘(pvc)
func (m *Kubernetes) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, err := validateAndGetRuntime(specObj, "Update")
	if err != nil {
		return nil, err
	}

	// Update需要区分出普通的更新和中途创建失败后系统删除了runtime(namespace)后"重新分析"的更新，
	// 这种更新其实就是创建runtime了, 通过namespace是否存在去判断
	k8sNS := generateK8SNamespace(runtime)
	existed, err := m.getNamespaceExisted(k8sNS)
	if err != nil {
		logrus.Errorf("Update: getNamespaceExisted error: %v", err)
		return nil, err
	}
	// namespace不存在，此更新相当于创建
	if !existed {
		if _, err = m.createRuntime(runtime); err != nil {
			return nil, err
		}

		logrus.Debugf("Update: create runtime(%s/%s) successfully", runtime.Namespace, runtime.Name)
		return nil, nil
	}

	// namespace存在，走普通更新的流程
	// Update提供两种实现
	// 1, updateAllWithForce, 全量删除再创建
	// 2, updateOneByOne, 分类出要创建的服务，要更新的服务，要删除的服务等三类并逐一处理

	// 目前采用updateOneByOne
	if err = m.updateOneByOne(runtime); err != nil {
		return nil, err
	}

	return nil, nil
}

func (m *Kubernetes) Inspect(ctx context.Context, specObj interface{}) (interface{}, error) {
	runtime, err := validateAndGetRuntime(specObj, "Inspect")
	if err != nil {
		return nil, err
	}

	// 元数据信息从上层传入，这里只需要拿到runtime的状态并拼装到runtime里返回
	status, err := m.Status(ctx, specObj)
	if err != nil {
		return nil, err
	}

	logrus.Debugf("Inspect runtime(%s) status: %+v", runtime.Name, status)
	runtime.Status = status.Status
	runtime.LastMessage = status.LastMessage

	k8sNS := generateK8SNamespace(runtime)

	for i, svc := range runtime.Services {
		if len(svc.Ports) > 0 {
			clusterIP, err := m.getClusterIP(k8sNS, svc.Name)
			if err != nil {
				logrus.Errorf("get k8s serivce(%s/%s) clusterip failed", k8sNS, svc.Name)
			}
			runtime.Services[i].ProxyIp = clusterIP
			runtime.Services[i].Vip = clusterIP
			runtime.Services[i].ProxyPorts = svc.Ports
		}
	}

	return runtime, nil
}

func (m *Kubernetes) createRuntime(runtime *spec.Runtime) (interface{}, error) {
	k8sNS := generateK8SNamespace(runtime)
	// return error if this namespace exists
	if err := m.createNamespace(k8sNS); err != nil {
		return nil, err
	}

	layers, err := util.ParseServiceDependency(runtime)
	if err != nil {
		return nil, err
	}

	for _, svcLayer := range layers {
		// services in one layer could be create in parallel, BUT NO NEED
		for _, service := range svcLayer {
			service.Namespace = k8sNS
			logrus.Debugf("in Create, going to create service(%s/%s)", service.Namespace, service.Name)
			// 只要其中某个服务创建失败，那么就清空已成功创建的服务
			// 这种情况要新建状态，返回给上层
			if err := m.createOneDeploymentAndService(service, runtime); err != nil {
				logrus.Errorf("in Create serivce(%s) error: %v, going to destroy runtime(%s/%s)",
					err, service.Name, runtime.Namespace, runtime.Name)

				defer func() {
					existed := false
					var err2 error
					if err2, existed = m.destroyRuntime(k8sNS); err2 != nil {
						// 会有资源残留，需要人工运维
						logrus.Errorf("in Create, destroy namespace(%s) failed, err: %v", k8sNS, err2)
						return
					}
					if !existed {
						logrus.Infof("destroy ns(%s) but not found, "+
							"this would not happen in theory, check it", k8sNS)
					}
					logrus.Infof("Delete namespace(%s) successfully", k8sNS)
				}()

				return nil, err
			}
		}
	}
	return nil, nil
}

// 每一个runtime对应到k8s上是一个k8s namespace,
// 格式为 ${runtimeNamespace}--${runtimeName}
func generateK8SNamespace(runtime *spec.Runtime) string {
	return runtime.Namespace + "--" + runtime.Name
}

func generatePVCName(runtime *spec.Runtime, service *spec.Service, index int) string {
	return runtime.Name + "-" + service.Name + "-" + strconv.Itoa(index)
}

func (m *Kubernetes) generateDeploymentFromUpperService(service *spec.Service, runtime *spec.Runtime) (*Deployment, error) {
	deployment := &Deployment{
		ApiVersion: "apps/v1beta1",
		Kind:       "Deployment",
		Metadata: ObjectMeta{
			Name:      service.Name,
			Namespace: service.Namespace,
			Labels:    make(map[string]string),
		},
		Spec: DeploymentSpec{
			Replicas: int32(service.Scale),
			Template: PodTemplateSpec{
				Metadata: ObjectMeta{
					Name:   service.Name,
					Labels: make(map[string]string),
				},
				Spec: PodSpec{
					Containers: []Container{
						{
							// TODO, container name e.g. redis-1528180634
							// Name:  service.Name + "-" + strconv.FormatInt(time.Now().Unix(), 10),
							Name: service.Name,
							// TODO:
							// tmp hacking for registry
							Image: strings.Replace(service.Image,
								"docker-registry.registry.marathon.mesos:5000",
								"hub.docker.test.terminus.io:80",
								-1),
							//Command: []string{service.Cmd},
							Resources: Resources{
								Requests: Requests{
									Cpu:    strconv.Itoa(int(service.Resources.Cpu*1000)) + "m",
									Memory: strconv.Itoa(int(service.Resources.Mem)) + "Mi",
									//Storage: strconv.Itoa(int(service.Resources.Disk)) + "M",
								},
							},
						},
					},
				},
			},
		},
	}

	for k, v := range service.Labels {
		// TODO: temporary modifications
		if k != "HAPROXY_0_VHOST" {
			deployment.Metadata.Labels[k] = v
			deployment.Spec.Template.Metadata.Labels[k] = v
		}
	}

	// k8s deployment 必须得有app的label来辖管pod
	deployment.Metadata.Labels["app"] = service.Name
	deployment.Spec.Template.Metadata.Labels["app"] = service.Name

	// 按当前的设定，一个pod中只有一个用户的容器
	if service.Cmd != "" {
		for i := range deployment.Spec.Template.Spec.Containers {
			//TODO:
			//cmds := strings.Split(service.Cmd, " ")
			cmds := []string{"sh", "-c", service.Cmd}
			deployment.Spec.Template.Spec.Containers[i].Command = cmds
		}
	}

	//add health check
	if service.HealthCheck != nil {
		// Containers is array of objects, instead of pointers
		for i := range deployment.Spec.Template.Spec.Containers {
			if service.HealthCheck.Kind == "TCP" {
				deployment.Spec.Template.Spec.Containers[i].LivenessProbe = &Probe{
					Handler: Handler{
						TCPSocket: &TCPSocketAction{
							Port: service.HealthCheck.Port,
						},
					},

					// TODO: Modify me
					FailureThreshold: 8,
				}

				// TODO: 额外加readinessProbe, 暂时与livenessProbe一样
				deployment.Spec.Template.Spec.Containers[i].ReadinessProbe = &Probe{
					Handler: Handler{
						TCPSocket: &TCPSocketAction{
							Port: service.HealthCheck.Port,
						},
					},

					// TODO: Modify me
					FailureThreshold: 8,
				}
			}
		}
	}

	if err := m.generateDeploymentEnv(service, runtime, deployment); err != nil {
		return nil, err
	}

	// mount the volume if need
	mountServiceVolume(service, runtime, deployment)

	return deployment, nil
}

func mountServiceVolume(service *spec.Service, runtime *spec.Runtime, deployment *Deployment) {
	if len(service.Volumes) == 0 {
		return
	}

	deployment.Spec.Template.Spec.Volumes = make([]Volume, 0)

	// 注意上面提到的设定，一个pod中只有一个container
	deployment.Spec.Template.Spec.Containers[0].VolumeMounts = make([]VolumeMount, 0)

	// hostPath类型
	for i, bind := range service.Binds {
		if len(bind.HostPath) == 0 || len(bind.ContainerPath) == 0 {
			continue
		}
		deployment.Spec.Template.Spec.Volumes = append(deployment.Spec.Template.Spec.Volumes,
			Volume{
				Name: "volume" + "-hostPath-" + strconv.Itoa(i),
				HostPath: &HostPathVolumeSource{
					Path: bind.HostPath,
				},
			})

		deployment.Spec.Template.Spec.Containers[0].VolumeMounts = append(deployment.Spec.Template.Spec.Containers[0].VolumeMounts,
			VolumeMount{
				Name:      "volume" + "-hostPath-" + strconv.Itoa(i),
				MountPath: bind.ContainerPath,
				ReadOnly:  bind.ReadOnly,
			})
	}

	// TODO: FIX ME
	// 如果是本地盘"local"也按云盘来实现
	for i, bind := range service.Volumes {
		logrus.Debugf("volume bind i:%v, volume type:%v, containerPath:%v", i, bind.VolumeType, bind.ContainerPath)
		deployment.Spec.Template.Spec.Volumes = append(deployment.Spec.Template.Spec.Volumes,
			Volume{
				Name: "volume-" + bind.VolumeType + "-" + strconv.Itoa(i),
				PersistentVolumeClaim: &PersistentVolumeClaimVolumeSource{
					// pvc's name, 参考createPVC()函数
					ClaimName: generatePVCName(runtime, service, i),
				},
			})

		deployment.Spec.Template.Spec.Containers[0].VolumeMounts = append(deployment.Spec.Template.Spec.Containers[0].VolumeMounts,
			VolumeMount{
				Name:      "volume-" + bind.VolumeType + "-" + strconv.Itoa(i),
				MountPath: bind.ContainerPath,
			})
	}
}

func generateK8SServiceFromRuntimeService(service *spec.Service) *Service {
	if len(service.Ports) == 0 {
		return nil
	}
	k8sService := &Service{
		ApiVersion: "v1",
		Kind:       "Service",
		Metadata: ObjectMeta{
			Name:      service.Name,
			Namespace: service.Namespace,
		},
		Spec: ServiceSpec{
			// TODO: type?
			//Type: ServiceTypeLoadBalancer,
			Selector: make(map[string]string),
		},
	}

	for k, v := range service.Labels {
		// TODO: temporary modifications
		// HAPROXY_0_VHOST 指的是域名，太长有可能超过63字符
		// 后面放到deployment和service里的label要统一规划
		if k != "HAPROXY_0_VHOST" {
			k8sService.Spec.Selector[k] = v
		}
	}

	// 用来管理后端pod
	k8sService.Spec.Selector["app"] = service.Name

	for i, port := range service.Ports {
		k8sService.Spec.Ports = append(k8sService.Spec.Ports, ServicePort{
			// TODO: name?
			Name: "http" + strconv.Itoa(i),
			Port: int32(port),
			// Dice上用户只填写Port, 即Port(service暴露的端口)和targetPort(容器暴露的端口)相同
			TargetPort: int32(port),
		})
	}
	return k8sService
}

func (m *Kubernetes) generateDeploymentEnv(service *spec.Service, runtime *spec.Runtime, deployment *Deployment) error {
	var envs []EnvVar
	k8sNS := generateK8SNamespace(runtime)
	// 用户注入的环境变量
	if len(service.Env) > 0 {
		for k, v := range service.Env {
			envs = append(envs, EnvVar{
				Name:  k,
				Value: v,
			})
		}
	}

	addEnv := func(svc *spec.Service, envs *[]EnvVar, useClusterIP bool) error {
		var ip string
		var err error
		if useClusterIP {
			ip, err = m.getClusterIP(svc.Namespace, svc.Name)
			if err != nil {
				return err
			}
		} else {
			// use SHORT dns, service's name is equal to SHORT dns
			ip = svc.Name
		}
		// 添加{serviceName}_HOST
		*envs = append(*envs, EnvVar{
			Name:  makeEnvVariableName(svc.Name) + "_HOST",
			Value: ip,
		})

		// {serviceName}_PORT 指代第一个端口
		if len(svc.Ports) > 0 {
			*envs = append(*envs, EnvVar{
				Name:  makeEnvVariableName(svc.Name) + "_PORT",
				Value: strconv.Itoa(svc.Ports[0]),
			})
		}

		// 有多个端口的情况下，依次使用{serviceName}_PORT0,{serviceName}_PORT1,...
		for i, port := range svc.Ports {
			*envs = append(*envs, EnvVar{
				Name:  makeEnvVariableName(svc.Name) + "_PORT" + strconv.Itoa(i),
				Value: strconv.Itoa(port),
			})
		}
		return nil
	}

	// 所有容器都有所有service的环境变量
	if runtime.ServiceDiscoveryMode == "GLOBAL" {
		// 由于某些服务可能尚未创建，所以注入dns
		// TODO: 也可以先将runtime的所有service的k8s service先创建出来
		for _, svc := range runtime.Services {
			// 无端口暴露的服务无{serviceName}_HOST,{serviceName}_PORT等环境变量
			if len(svc.Ports) == 0 {
				continue
			}
			svc.Namespace = k8sNS
			// dns环境变量
			if err := addEnv(&svc, &envs, false); err != nil {
				return err
			}
		}
	} else {
		// 按约定注入依赖的服务的环境变量，比如服务A依赖服务B，
		// 则将{B}_HOST, {B}_PORT 注入到对A可见的容器环境变量中
		var backendUrlEnv EnvVar
		for _, name := range service.Depends {
			var depSvc *spec.Service
			for _, svc := range runtime.Services {
				if svc.Name == name {
					depSvc = &svc
					break
				}
			}
			// this would not happen as if this situations exist,
			// we would find out it in the stage of util.ParseServiceDependency
			if depSvc == nil {
				return errors.New(fmt.Sprintf("cannot find service(%s) in runtime", name))
			}

			if len(depSvc.Ports) == 0 {
				continue
			}

			// 注入{B}_HOST, {B}_PORT等
			if err := addEnv(depSvc, &envs, true); err != nil {
				return err
			}

			// 适配上层逻辑, 如果IS_ENDPOINT被赋为true, 增加BACKEND_URL环境变量
			if service.Labels["IS_ENDPOINT"] == "true" && len(depSvc.Ports) > 0 {
				bip, err := m.getClusterIP(depSvc.Namespace, depSvc.Name)
				if err != nil {
					return err
				}
				bport := depSvc.Ports[0]
				backendUrlEnv = EnvVar{
					Name:  "BACKEND_URL",
					Value: bip + ":" + strconv.Itoa(bport),
				}
			}
		}

		if len(backendUrlEnv.Name) > 0 {
			envs = append(envs, backendUrlEnv)
			logrus.Debugf("service(%s/%s) BACKEND_URL is %v", service.Namespace, service.Name, backendUrlEnv.Value)
		}

		// 注入服务自身的环境变量，即{A}_HOST, {A}_PORT等
		if len(service.Ports) > 0 {
			if err := addEnv(service, &envs, true); err != nil {
				return err
			}
		}
	}

	for i := range deployment.Spec.Template.Spec.Containers {
		deployment.Spec.Template.Spec.Containers[i].Env = envs
	}
	return nil
}

func (m *Kubernetes) getNamespaceExisted(ns string) (bool, error) {
	resp, err := m.client.Get(m.addr).
		Path("/api/v1/namespaces/" + ns).
		Do().
		DiscardBody()

	if err != nil {
		return false, err
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			return false, nil
		}
		return false, errors.Errorf("get namespace(%s) statusCode: %v", ns, resp.StatusCode())
	}

	return true, nil
}

func (m *Kubernetes) createNamespace(ns string) error {
	existed, err := m.getNamespaceExisted(ns)
	if err != nil {
		return errors.Errorf("try to get namespaces(%s) failed, err: %v", ns, err)
	}

	if existed {
		return errors.Errorf("crateNamespace failed as namespace(%s) already exists", ns)
	}

	namespace := &Namespace{
		ApiVersion: "v1",
		Kind:       "Namespace",
		Metadata: ObjectMeta{
			Name: ns,
		},
	}

	var b bytes.Buffer
	resp, err := m.client.Post(m.addr).
		Path("/api/v1/namespaces").
		JSONBody(namespace).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s create namespace(%s) failed", ns)
	}

	if !resp.IsOK() {
		logrus.Errorf("create namespace(%s) resp body: %v", ns, b.String())
		return errors.Errorf("k8s create namespace(%s) status code: %v, body: %v", ns, resp.StatusCode(), b.String())
	}

	logrus.Infof("Namespace(%s) created successfully", ns)
	return nil
}

// 两个接口可能会调用此函数
// 1, Create
// 2, Update
func (m *Kubernetes) createOneDeploymentAndService(service *spec.Service, runtime *spec.Runtime) error {
	if service == nil {
		return errors.Errorf("try to create service(%s/%s) but found empty", service.Namespace, service.Name)
	}

	var err error

	// Step 1. Firstly create service
	// 只对填写了暴露端口的服务创建k8s service
	if len(service.Ports) > 0 {
		if err = m.createService(service); err != nil {
			return err
		}
	}

	if err = m.createPVC(service, runtime); err != nil {
		return err
	}

	// Step 2. Create related deployment
	if err = m.createDeployment(service, runtime); err != nil {
		return err
	}

	// TODO: Wait for the deployment running well ?
	// status, err := m.getDeploymentInfo(service)

	return nil
}

// 1, create k8s service
// 2, call func to create k8s ingress
func (m *Kubernetes) createService(service *spec.Service) error {
	var b bytes.Buffer
	svc := generateK8SServiceFromRuntimeService(service)

	logrus.Debugf("going to create service: %s/%s", service.Namespace, service.Name)

	resp, err := m.client.Post(m.addr).
		Path("/api/v1/namespaces/" + service.Namespace + "/services").
		JSONBody(svc).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s create service(%s) failed", service.Name)
	}

	if !resp.IsOK() {
		logrus.Errorf("create service(%s) resp body: %v", service.Name, b.String())
		return errors.Errorf("k8s create service(%s) status code: %v, body: %v", service.Name, resp.StatusCode(), b.String())
	}

	if err = m.createIngress(service); err != nil {
		return err
	}
	return nil
}

func (m *Kubernetes) createIngress(rSvc *spec.Service) error {
	// 需要对公网暴露的服务
	if rSvc.Labels["IS_ENDPOINT"] == "true" {
		// 将label中HAPROXY_0_VHOST对应的域名/vip集合都转发到该服务的第0个端口上
		publicHosts := strings.Split(rSvc.Labels["HAPROXY_0_VHOST"], ",")
		if len(publicHosts) == 0 {
			return errors.Errorf("service(%s) set label IS_ENDPOINT true but label HAPROXY_0_VHOST empty", rSvc.Name)
		}
		// 创建ingress
		rules := make([]IngressRule, len(publicHosts))
		for i, host := range publicHosts {
			rules[i].Host = host
			rules[i].HTTP = &HTTPIngressRuleValue{
				Paths: []HTTPIngressPath{
					{
						//TODO: add Path
						// Path:
						Backend: IngressBackend{
							ServiceName: rSvc.Name,
							ServicePort: rSvc.Ports[0],
						},
					},
				},
			}
		}

		ingress := &Ingress{
			ApiVersion: "extensions/v1beta1",
			Kind:       "Ingress",
			Metadata: ObjectMeta{
				Name:      rSvc.Name,
				Namespace: rSvc.Namespace,
			},
			Spec: IngressSpec{
				Rules: rules,
			},
		}

		var b bytes.Buffer
		resp, err := m.client.Post(m.addr).
			Path("/apis/extensions/v1beta1/namespaces/" + rSvc.Namespace + "/ingresses").
			JSONBody(ingress).
			Do().
			Body(&b)

		if err != nil {
			return errors.Wrapf(err, "k8s create ingress(%s) failed", rSvc.Name)
		}

		if !resp.IsOK() {
			logrus.Errorf("create ingress resp body: %v", b.String())
			return errors.Errorf("k8s create ingress status code: %v, body: %v", resp.StatusCode(), b.String())
		}
	}
	return nil
}

// TODO: do real update?
func (m *Kubernetes) updateIngress(rSvc *spec.Service) error {
	if err := m.deleteIngress(rSvc.Namespace, rSvc.Name); err != nil {
		logrus.Errorf("delete ingress(%s) failed in updateIngress", rSvc.Name)
		return err
	}

	if err := m.createIngress(rSvc); err != nil {
		return err
	}
	return nil
}

func (m *Kubernetes) createDeployment(service *spec.Service, runtime *spec.Runtime) error {
	var b bytes.Buffer
	deployment, err := m.generateDeploymentFromUpperService(service, runtime)
	if err != nil {
		return errors.Wrapf(err, "before k8s create deployment(%s), get deployment struct failed", service.Name)
	}
	resp, err := m.client.Post(m.addr).
		Path("/apis/apps/v1beta1/namespaces/" + service.Namespace + "/deployments").
		JSONBody(deployment).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s create deployment(%s) failed", service.Name)
	}

	if !resp.IsOK() {
		logrus.Errorf("create deployment resp body: %v", b.String())
		return errors.Errorf("k8s create deployment status code: %v, body: %v", resp.StatusCode(), b.String())
	}
	return nil
}

func (m *Kubernetes) createPVC(service *spec.Service, runtime *spec.Runtime) error {
	// 不需要创建
	if len(service.Binds) == 0 {
		return nil
	}

	for i, volume := range service.Volumes {
		// 本地数据盘先按云盘实现，除本地盘和云盘外的类型暂不申请动态存储资源
		if volume.VolumeType != "local" && volume.VolumeType != "cloud" {
			continue
		}

		pvcName := runtime.Name + "-" + service.Name + "-" + strconv.Itoa(i)
		k8sNS := generateK8SNamespace(runtime)
		// TODO: 后续提供引用其他类型sc的功能
		storageClassNameCommon := "alicloud-disk-common"

		pvc := &PersistentVolumeClaim{
			ApiVersion: "v1",
			Kind:       "PersistentVolumeClaim",
			Metadata: ObjectMeta{
				Name:      generatePVCName(runtime, service, i),
				Namespace: k8sNS,
			},
			Spec: PersistentVolumeClaimSpec{
				Resources: Resources{
					Requests: Requests{
						Storage: volume.Storage,
					},
				},
				// 引用阿里k8s服务的sc, 类型为普通云盘
				StorageClassName: &storageClassNameCommon,
			},
		}

		var b bytes.Buffer
		resp, err := m.client.Post(m.addr).
			Path("/api/v1/namespaces/" + k8sNS + "/persistentvolumeclaims").
			JSONBody(pvc).
			Do().
			Body(&b)

		if err != nil {
			logrus.Errorf("create pvc(%s) failed, err: %v", pvcName, err)
			return err
		}
		if !resp.IsOK() {
			logrus.Errorf("create pvc(%s) status code: %v, body: %v", pvcName, resp.StatusCode(), b.String())
			return errors.Errorf("create pvc(%s) status code: %v", pvcName, resp.StatusCode())
		}
	}
	return nil
}

// not sure with whether this service exists
// bool 变量表示是否真正删除
// 在以下情况发生,
// 1, Update接口分析出的删除，此时无法确保k8s的资源是否存在
func (m *Kubernetes) tryDelete(namespace, name string) error {
	var wg sync.WaitGroup
	var err1, err2, err3 error
	wg.Add(3)
	go func() {
		err1, _ = m.tryDeleteDeployment(namespace, name)
		wg.Done()
	}()
	go func() {
		err2, _ = m.tryDeleteService(namespace, name)
		wg.Done()
	}()
	go func() {
		err3, _ = m.tryDeleteIngress(namespace, name)
		wg.Done()
	}()
	wg.Wait()
	if err1 != nil || err2 != nil || err3 != nil {
		return errors.Errorf("delete deploy err: %v, delete service err: %v, delete ingress err:%v", err1, err2, err3)
	}

	return nil
}

func (m *Kubernetes) tryDeleteDeployment(namespace, name string) (error, bool) {
	_, statusCode, err := m.getDeploymentInfo(namespace, name)

	if statusCode == http.StatusNotFound {
		logrus.Debugf("Update: try to delete deployment(%s/%s), but found not existed", namespace, name)
		return nil, false
	} else {
		// 非404的error都作为错误返回
		if err != nil {
			return err, false
		}
	}

	err = m.deleteDeployment(namespace, name)
	if err != nil {
		return err, false
	}
	return nil, true
}

func (m *Kubernetes) tryDeleteService(namespace, name string) (error, bool) {
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/api/v1/namespaces/" + namespace + "/services/" + name).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete service(%s) failed", name), false
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			logrus.Debugf("Update: try to delete service(%s/%s), but found not existed", namespace, name)
			return nil, false
		}
		return errors.Errorf("k8s delete service(%s) status code: %v, resp body: %v", name, resp.StatusCode(), b.String()), false
	}

	return nil, true
}

func (m *Kubernetes) tryDeleteIngress(namespace, name string) (error, bool) {
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/apis/extensions/v1beta1/namespaces/" + namespace + "/ingresses/" + name).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete service(%s) failed", name), false
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			logrus.Debugf("Update: try to delete ingress(%s/%s), but found not existed", namespace, name)
			return nil, false
		}
		return errors.Errorf("k8s delete ingress(%s) status code: %v, resp body: %v", name, resp.StatusCode(), b.String()), false
	}

	return nil, true
}

func (m *Kubernetes) getDeploymentStatus(service *spec.Service) (spec.StatusDesc, error) {
	var status spec.StatusDesc
	// in version 1.10.3, the following two apis are equal
	// http://localhost:8080/apis/extensions/v1beta1/namespaces/default/deployments/myk8stest6
	// http://localhost:8080/apis/extensions/v1beta1/namespaces/default/deployments/myk8stest6/status
	deployment, statusCode, err := m.getDeploymentInfo(service.Namespace, service.Name)
	if err != nil {
		if statusCode == http.StatusNotFound {
			logrus.Errorf("k8s deployment(%s/%s) Not Found", service.Namespace, service.Name)
			return status, errors.New(string(DeploymentNotFound))
		}
		return status, err
	}

	dps := deployment.Status
	// 刚刚开始创建的时候可能获取到该状态
	if len(dps.Conditions) == 0 {
		status.Status = spec.StatusUnkonw
		status.LastMessage = "could not get status condition"
		return status, nil
	}

	for _, c := range dps.Conditions {
		if c.Type == DeploymentReplicaFailure && c.Status == "True" {
			status.Status = spec.StatusFailing
			return status, nil
		}
		if c.Type == DeploymentAvailable && c.Status == "False" {
			status.Status = spec.StatusFailing
			return status, nil
		}
	}

	status.Status = spec.StatusUnkonw

	if dps.Replicas == dps.ReadyReplicas &&
		dps.Replicas == dps.AvailableReplicas &&
		dps.Replicas == dps.UpdatedReplicas {
		if dps.Replicas > 0 {
			status.Status = spec.StatusReady
			status.LastMessage = fmt.Sprintf("deployment(%s) is running", deployment.Metadata.Name)
		} else {
			status.LastMessage = fmt.Sprintf("deployment(%s) replica is 0, been deleting", deployment.Metadata.Name)
		}
	}
	return status, nil
}

func (m *Kubernetes) getDeploymentInfo(namespace, name string) (*Deployment, int, error) {
	deployment := &Deployment{}
	var b bytes.Buffer
	resp, err := m.client.Get(m.addr).
		Path("/apis/apps/v1beta1/namespaces/" + namespace + "/deployments/" + name).
		Do().
		Body(&b)

	if err != nil {
		return nil, -1, errors.Wrapf(err, "k8s get deployment(%s) failed", name)
	}

	if !resp.IsOK() {
		return nil, resp.StatusCode(),
			errors.Errorf("k8s get deployment(%s) failed, status code: %v, body: %v", name, resp.StatusCode(), b.String())
	}

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(deployment); err != nil {
		return nil, -1, err
	}

	return deployment, http.StatusOK, nil
}

func (m *Kubernetes) putDeployment(deployment *Deployment) error {
	var b bytes.Buffer
	resp, err := m.client.Put(m.addr).
		Path("/apis/apps/v1beta1/namespaces/" + deployment.Metadata.Namespace + "/deployments/" + deployment.Metadata.Name).
		JSONBody(deployment).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s put deployment(%s) failed", deployment.Metadata.Name)
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s put deployment(%s) status code: %v, resp body: %v",
			deployment.Metadata.Name, resp.StatusCode(), b.String())
	}
	return nil
}

func (m *Kubernetes) putService(desiredSvc *Service, currentSvc *Service, rSvc *spec.Service) error {
	var b bytes.Buffer
	namespace := desiredSvc.Metadata.Namespace
	name := desiredSvc.Metadata.Name
	resp, err := m.client.Put(m.addr).
		Path("/api/v1/namespaces/" + namespace + "/services/" + name).
		JSONBody(desiredSvc).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s put service(%s) failed", name)
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s put service(%s) status code: %v, resp body: %v",
			desiredSvc.Metadata.Name, resp.StatusCode(), b.String())
	}
	// deal with ingress
	// 当前的service有vhost
	if currentSvc.Spec.Selector["IS_ENDPOINT"] == "true" {
		// 期望的service有vhost
		if desiredSvc.Spec.Selector["IS_ENDPOINT"] == "true" {
			// 目前只关心第0个端口
			if currentSvc.Spec.Ports[0] == desiredSvc.Spec.Ports[0] &&
				currentSvc.Spec.Selector["HAPROXY_0_VHOST"] == desiredSvc.Spec.Selector["HAPROXY_0_VHOST"] {
				// no change to ingress
				return nil
			} else {
				// update ingress
				if err = m.updateIngress(rSvc); err != nil {
					logrus.Errorf("updateK8SService -> putService -> updateIngress (%s/%s) err: %v", namespace, name, err)
				}
			}
		} else {
			// 期望的service无vhost
			// delete ingress
			if err = m.deleteIngress(namespace, name); err != nil {
				logrus.Errorf("updateK8SService -> putService -> deleteIngress (%s/%s) err: %v", namespace, name, err)
				return err
			}
		}
	} else {
		// 当前的service无vhost, 期望的service有vhost
		// create ingress
		if desiredSvc.Spec.Selector["IS_ENDPOINT"] == "true" {
			if err = m.createIngress(rSvc); err != nil {
				logrus.Errorf("updateK8SService -> putService -> createIngress (%s/%s) err: %v", namespace, name, err)
				return err
			}
		}
		// else, no change to ingress
	}
	return nil
}

func (m *Kubernetes) getServiceInfo(namespace, name string) (*Service, int, error) {
	svc := &Service{}
	var b bytes.Buffer
	resp, err := m.client.Get(m.addr).
		Path("/api/v1/namespaces/" + namespace + "/services/" + name).
		Do().
		Body(&b)

	if err != nil {
		return nil, -1, errors.Errorf("k8s get svc(%s) failed, err: %v", name, err)
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			return nil, http.StatusNotFound, errors.Errorf("service(%s) not found", name)
		}
		return nil, resp.StatusCode(), errors.Errorf("get service(%s) statuscode: %v, body: %v",
			name, resp.StatusCode(), b.String())
	}

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(svc); err != nil {
		return nil, -1, err
	}

	return svc, http.StatusOK, nil
}

func (m *Kubernetes) deleteService(service *spec.Service) error {
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/api/v1/namespaces/" + service.Namespace + "/services/" + service.Name).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete service(%s) failed", service.Name)
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s delete service(%s) status code: %v, resp body: %v", service.Name, resp.StatusCode(), b.String())
	}

	if service.Labels["IS_ENDPOINT"] == "true" {
		if err = m.deleteIngress(service.Namespace, service.Name); err != nil {
			logrus.Errorf("deleteService -> deleteIngress (%s/%s) err: %v", service.Namespace, service.Name, err)
			return err
		}
	}
	return nil
}

// 删除ingress资源
func (m *Kubernetes) deleteIngress(namespace, name string) error {
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/apis/extensions/v1beta1/namespaces/" + namespace + "/ingresses/" + name).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete ingress(%s) failed", name)
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s delete ingress(%s) status code: %v, resp body: %v", name, resp.StatusCode(), b.String())
	}
	return nil
}

func (m *Kubernetes) deleteDeployment(namespace, name string) error {
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/apis/apps/v1beta1/namespaces/" + namespace + "/deployments/" + name).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete deployment(%s) failed", name)
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s delete deployment(%s) status code: %v, resp body: %v", name, resp.StatusCode(), b.String())
	}
	return nil
}

// 服务描述中的端口变化会导致services及ingress的变化
func (m *Kubernetes) updateK8SService(rSvc *spec.Service) error {
	svc, statusCode, err := m.getServiceInfo(rSvc.Namespace, rSvc.Name)
	// rSvc.Ports为空表明期望无service
	if len(rSvc.Ports) == 0 {
		if err != nil {
			if statusCode == http.StatusNotFound {
				// 更新前没有service, 期望无service
				return nil
			}
			return errors.Wrap(err, fmt.Sprintf("failed to get service(%s) info", rSvc.Name))
		}

		// 更新前有service, 期望无service, 则删除service
		return m.deleteService(rSvc)
	}

	if err != nil {
		if err.Error() == strconv.Itoa(http.StatusNotFound) {
			// 更新前没有service, 期望有service, 则创建service
			return m.createService(rSvc)
		}
		return errors.Wrap(err, fmt.Sprintf("failed to get service(%s) info", rSvc.Name))
	}

	// 更新前有service，期望有service，则更新service
	desiredService := generateK8SServiceFromRuntimeService(rSvc)
	if diffK8SServiceMetadata(desiredService, svc) {
		desiredService.Metadata.ResourceVersion = svc.Metadata.ResourceVersion
		desiredService.Spec.ClusterIP = svc.Spec.ClusterIP
		return m.putService(desiredService, svc, rSvc)
	}
	// TODO: Update ingress
	return nil
}

// TODO: Complete me.
func diffK8SServiceMetadata(left *Service, right *Service) bool {
	// compare the fields of Metadata and Spec
	if !reflect.DeepEqual(left.Metadata.Labels, right.Metadata.Labels) {
		return true
	}

	if !reflect.DeepEqual(left.Spec.Ports, right.Spec.Ports) {
		return true
	}

	if !reflect.DeepEqual(left.Spec.Selector, right.Spec.Selector) {
		return true
	}

	return false
}

func makeEnvVariableName(str string) string {
	return strings.ToUpper(strings.Replace(str, "-", "_", -1))
}

func (m *Kubernetes) getClusterIP(namespace, name string) (string, error) {
	svc, _, err := m.getServiceInfo(namespace, name)
	if err != nil {
		return "", err
	}
	return svc.Spec.ClusterIP, nil
}

// actually get deployment's names list, as k8s service would not be created
// if no ports exposed
func (m *Kubernetes) getServiceNameList(namespace string) ([]string, error) {
	strs := make([]string, 0)
	var b bytes.Buffer
	resp, err := m.client.Get(m.addr).
		Path("/apis/apps/v1/namespaces/" + namespace + "/deployments").
		Do().
		Body(&b)

	if err != nil {
		return strs, errors.Wrapf(err, "k8s get deployment list failed")
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			return strs, nil
		}
		return strs, errors.Errorf("k8s get deployment list(%s) status code: %v, resp body: %v",
			namespace, resp.StatusCode(), b.String())
	}

	r := bytes.NewReader(b.Bytes())
	var deployList DeploymentList
	if err := json.NewDecoder(r).Decode(&deployList); err != nil {
		return strs, err
	}

	for _, item := range deployList.Items {
		strs = append(strs, item.Metadata.Name)
	}

	return strs, nil
}

// bool为false表示该ns不存在，为true表示存在并已删除
func (m *Kubernetes) destroyRuntime(k8sNS string) (error, bool) {
	logrus.Debugf("in destroy, going to delete namespace(%s)", k8sNS)
	var b bytes.Buffer
	resp, err := m.client.Delete(m.addr).
		Path("/api/v1/namespaces/" + k8sNS).
		JSONBody(&deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete namespace(%s) failed", k8sNS), false
	}

	if !resp.IsOK() {
		if resp.StatusCode() == http.StatusNotFound {
			logrus.Debugf("in destroyRuntime, namespace(%s) not found", k8sNS)
			return nil, false
		}
		return errors.Errorf("k8s delete namespace(%s) status code: %v, resp body: %v", k8sNS, resp.StatusCode(), b.String()), false
	}

	logrus.Infof("Namespace(%s) deleted successfully", k8sNS)
	return nil, true
}

func (m *Kubernetes) updateAllWithForce(runtime *spec.Runtime) error {
	k8sNS := generateK8SNamespace(runtime)
	if err, _ := m.destroyRuntime(k8sNS); err != nil {
		logrus.Errorf("updateAllWithForce -> destroyRuntime error: %v", err)
		return err
	}

	if _, err := m.createRuntime(runtime); err != nil {
		logrus.Errorf("updateAllWithForce -> createRuntime error: %v", err)
		return err
	}
	return nil
}

// 创建操作需要先于更新操作完成，因为新创建的service有可能是要更新的服务的依赖
// TODO: 后面要抽象出updateOne函数
func (m *Kubernetes) updateOneByOne(runtime *spec.Runtime) error {
	k8sNS := generateK8SNamespace(runtime)

	visited := make([]string, 0)
	oldSpecServices, err := m.getServiceNameList(k8sNS)
	if err != nil {
		//TODO:
		return err
	}

	// runtime.Services are services with desired states
	for _, svc := range runtime.Services {
		svc.Namespace = k8sNS
		// find it in oldServices
		found := false
		for _, s := range oldSpecServices {
			if svc.Name == s {
				found = true
				break
			}
		}
		if found {
			// 已存在老的service集合中，做put操作
			// visited记录已更新过的service
			visited = append(visited, svc.Name)
		} else {
			// 不存在于老的service集合中，做create操作
			// TODO: what to do if errors in Create ? before k8s create deployment ?
			logrus.Debugf("in Update interface, going to create service(%s/%s)", k8sNS, svc.Name)
			if err := m.createOneDeploymentAndService(&svc, runtime); err != nil {
				logrus.Errorf("in Update interface, failed to create service(%s), err: %v", svc.Name, err)
				return err
			}
		}
	}

	// update 操作
	for _, s := range visited {
		for _, svc := range runtime.Services {
			if s == svc.Name {
				// firstly update the service
				// service同deployment不一样，只对暴露端口的服务才会创建service
				if err := m.updateK8SService(&svc); err != nil {
					return err
				}

				// then update the deployment
				desiredDeployment, err := m.generateDeploymentFromUpperService(&svc, runtime)
				if err != nil {
					return err
				}
				if err = m.putDeployment(desiredDeployment); err != nil {
					logrus.Debugf("in Update interface, failed to update deployment(%s), err: %v", svc.Name, err)
					return err
				}
				break
			}
		}
	}

	// 老的service中除去已被更新的service即需要删除的service
	if len(visited) != len(oldSpecServices) {
		goingToBeDeleted := make([]string, 0)
		for _, s := range oldSpecServices {
			existed := false
			for _, v := range visited {
				if s == v {
					existed = true
					break
				}
			}
			if !existed {
				goingToBeDeleted = append(goingToBeDeleted, s)
			}
		}

		for _, svcName := range goingToBeDeleted {
			logrus.Debugf("in Update interface, going to delete service(%s/%s)", k8sNS, svcName)
			// TODO: what to do if errors in DELETE ?
			if err := m.tryDelete(k8sNS, svcName); err != nil {
				logrus.Errorf("in Update interface, failed to delete k8s service(%s/%s), err: %v", k8sNS, svcName, err)
				return err
			}
		}
	}
	return nil
}
