package k8s

import (
	"context"
	"os"
	"testing"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/sirupsen/logrus"
)

var kubernetes executortypes.Executor
var (
	specObj     spec.Runtime // self-defined runtime
	specObjWeb  spec.Runtime // dice-test web, simplest example
	specObjBlog spec.Runtime // pampas blog
	specs       []spec.Runtime
)

func TestMain(m *testing.M) {
	logrus.Infof("in testmain")
	initK8S()
	ret := m.Run()
	os.Exit(ret)
}

func initK8S() {
	specObj = spec.Runtime{
		Force: true,
		Dice: spec.Dice{
			Name:      "k8s2",
			Namespace: "services-1",
			Services: []spec.Service{
				{
					Name: "web2",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  256,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						80,
					},
					Image: "hub.c.163.com/public/nginx:1.2.1",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 80,
					},
					Env: map[string]string{
						"APP_DIR":      "/web",
						"TERMINUS_APP": "web",
					},
					Labels: map[string]string{
						"app":             "web2",
						"IS_ENDPOINT":     "true",
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "mymytest.k8s.test.terminus.io",
					},
					Depends: []string{
						"mytest1",
						"mytest2",
					},
				},

				{
					Name: "mytest1",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  256,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						6379,
					},
					Image: "redis",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 6379,
					},
					Labels: map[string]string{
						"app": "mytest1",
					},
					Depends: []string{
						"mytest3",
					},
				},

				{
					Name: "mytest2",
					Resources: spec.Resources{
						Cpu:  0.05,
						Mem:  128,
						Disk: 0,
					},
					Scale: 1,
					Image: "hub.c.163.com/public/nginx:1.2.1",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 80,
					},
					Labels: map[string]string{
						"app": "mytest2",
					},
					Depends: []string{},
				},

				{
					Name: "mytest3",
					Resources: spec.Resources{
						Cpu:  0.05,
						Mem:  128,
						Disk: 0,
					},
					Scale: 1,
					Image: "hub.c.163.com/public/ubuntu:14.04-tools",
					Ports: []int{
						22,
					},
					Labels: map[string]string{
						"app": "mytest3",
					},
					Depends: []string{},
				},

				{
					Name: "mytest4",
					Resources: spec.Resources{
						Cpu:  0.05,
						Mem:  128,
						Disk: 0,
					},
					Scale: 1,
					Cmd:   "/bin/sleep 90d",
					//Image: "ubuntu",
					Image: "ubuntu:test",
					Labels: map[string]string{
						"app": "mytest4",
					},
					Env: map[string]string{
						"TERM": "xterm",
					},
					Depends: []string{},
				},
			},
		},
	}

	specObjWeb = spec.Runtime{
		Force: true,
		Dice: spec.Dice{
			Name:                 "dice-test",
			Namespace:            "services-2",
			ServiceDiscoveryKind: "VIP",
			Services: []spec.Service{
				{
					Name: "web",
					Resources: spec.Resources{
						Cpu:  0.5,
						Mem:  256,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						8080,
					},
					Image: "dice-testweb:test1",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 8080,
					},
					Env: map[string]string{
						"APP_DIR":      "/web",
						"TERMINUS_APP": "web",
					},
					Labels: map[string]string{
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "zjt-test.app.terminus.io",
					},
				},
			},
		},
	}

	specObjBlog = spec.Runtime{
		Force: true,
		Extra: map[string]string{
			"lastRestartTime": "123",
		},
		Dice: spec.Dice{
			Name:                 "blog-test",
			Namespace:            "services-3",
			ServiceDiscoveryKind: "PROXY",
			ServiceDiscoveryMode: "GLOBAL",
			Services: []spec.Service{
				{
					Name: "showcase-front",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  128,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						12300,
						8080,
					},
					Image: "showcase-front:test",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 12300,
					},
					Env: map[string]string{
						"BACKEND_URL": "http://${BLOG_WEB_HOST}:${BLOG_WEB_PORT}",
					},
					Labels: map[string]string{
						"HAPROXY_GROUP":   "external",
						"HAPROXY_0_VHOST": "myblog.k8s.test.terminus.io",
						"IS_ENDPOINT":     "true",
					},
					Depends: []string{
						"blog-web",
					},
				},
				{
					Name: "blog-web",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  384,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						12300,
					},
					Image: "blog-web:test",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 12300,
					},
					Env: map[string]string{
						"APP_DIR": "/blog-web",
					},
					Depends: []string{
						"blog-service",
						"user-service",
					},
					Labels: map[string]string{
						"IS_ENDPOINT": "true",
					},
				},
				{
					Name: "blog-service",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  512,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						20880,
					},
					Image: "blog-service:test",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 20880,
					},
					Env: map[string]string{
						"APP_DIR": "/blog-service/blog-service-impl",
					},
					Depends: []string{
						"user-service",
					},
				},
				{
					Name: "user-service",
					Resources: spec.Resources{
						Cpu:  0.1,
						Mem:  512,
						Disk: 0,
					},
					Scale: 1,
					Ports: []int{
						20880,
					},
					Image: "user-service:test",
					HealthCheck: &spec.HealthCheck{
						Kind: "TCP",
						Port: 20880,
					},
					Env: map[string]string{
						"APP_DIR": "/user-service/user-service-impl",
					},
				},
			},
		},
	}
	blogGlobalEnvs := map[string]string{
		"MYSQL_HOST":        "172.17.0.4",
		"MYSQL_PORT":        "3306",
		"MYSQL_DATABASE":    "mydb",
		"MYSQL_USERNAME":    "root",
		"MYSQL_PASSWORD":    "root",
		"REDIS_HOST":        "172.17.0.2",
		"REDIS_PORT":        "6379",
		"REDIS_PASSWORD":    "root",
		"ZOOKEEPER_HOST":    "172.17.0.5",
		"ZOOKEEPER_PORT":    "2181",
		"JAVA_OPTS":         " -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=1617 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false ",
		"TERMINUS_APP_NAME": "PAMPAS_BLOG",
		"TRACE_SAMPLE":      "1",
	}
	for i := range specObjBlog.Services {
		for k, v := range blogGlobalEnvs {
			specObjBlog.Services[i].Env[k] = v
		}
	}

	kubernetes = &Kubernetes{
		name: "K8SFORSERVICE",
		//addr: "127.0.0.1:8080",
		addr:    "10.173.32.53:8080",
		options: map[string]string{"a": "b"},
		client:  httpclient.New(),
	}

	specs = []spec.Runtime{specObj, specObjWeb, specObjBlog}
}

func TestCreate(t *testing.T) {
	ctx := context.Background()
	_, err := kubernetes.Create(ctx, specs[2])
	if err != nil {
		t.Error(err)
	}
}

func TestDestroy(t *testing.T) {
	ctx := context.Background()
	err := kubernetes.Destroy(ctx, specs[2])
	if err != nil {
		t.Error(err)
	}
}

func TestStatus(t *testing.T) {
	ctx := context.Background()
	status, err := kubernetes.Status(ctx, specs[2])
	if err != nil {
		t.Error(err)
	}
	logrus.Infof("deployment status: %+v", status)
}

func TestInspect(t *testing.T) {
	ctx := context.Background()
	runtime, err := kubernetes.Inspect(ctx, specs[2])
	if err != nil {
		t.Error(err)
	}
	logrus.Infof("runtime: %+v", runtime)
}

func TestUpdate(t *testing.T) {
	ctx := context.Background()
	resp, err := kubernetes.Update(ctx, specs[2])
	if err != nil {
		t.Error(err)
	}
	logrus.Infof("updated runtime: %+v", resp)
}
