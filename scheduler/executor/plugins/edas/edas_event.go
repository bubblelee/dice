package edas

import (
	"context"
	"os"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/pkg/jsonstore/storetypes"
	"terminus.io/dice/dice/scheduler/events"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func registerEventChanAndLocalStore(name string, evCh chan *spec.StatusEvent, lstore *sync.Map) {
	eventAddr, ok := os.LookupEnv("CONSOLE_EVENT_ADDR")
	if !ok {
		eventAddr = events.CONSOLE_EVENT_ADDR
	}
	notifier, err := api.New(name+events.SUFFIX_EDAS, map[string]interface{}{"HTTP": []string{eventAddr}})
	if err != nil {
		return
	}

	logrus.Infof("edas registerEventChanAndLocalStore, name: %s", name)

	// watch 特定etcd目录的事件的处理函数
	syncRuntimeToLstore := func(key string, value interface{}, t storetypes.ChangeType) error {
		runtimeName := etcdKeyToMapKey(key)
		if len(runtimeName) == 0 {
			return nil
		}

		// 先处理delete的事件
		if t == storetypes.Del {
			_, ok := lstore.Load(runtimeName)
			if ok {
				var e events.RuntimeEvent
				e.RuntimeName = runtimeName
				e.IsDeleted = true

				lstore.Delete(runtimeName)
				notifier.Send(e)
			} else {
				logrus.Infof("weird not found runtimeKey(%s) in lstore: %p", runtimeName, lstore)
			}
			return nil
		}

		run := value.(*spec.Runtime)

		// 过滤不属于本executor的事件
		if run.Executor != name {
			return nil
		}

		switch t {
		// Added event
		case storetypes.Add:
			logrus.Debugf("edas executor(%s) runtimeName(%s) in added event, lstore addr: %p", name, runtimeName, lstore)
			lstore.Store(runtimeName, *run)

		// Updated event
		case storetypes.Update:
			logrus.Debugf("edas executor(%s) runtimeName(%s) in updated event, lstore addr: %p", name, runtimeName, lstore)
			lstore.Store(runtimeName, *run)
		}

		if strings.Contains(name, "EDAS") {
			logrus.Infof("edas executor(%s) lstore stored key: %s", name, key)
		}
		return nil
	}

	// 将注册来的executor的name及其event channel对应起来
	getEvChanFn := func(executorName executortypes.Name) (chan *spec.StatusEvent, *sync.Map, error) {
		logrus.Infof("in RegisterEvChan executor(%s)", name)
		if string(executorName) == name {
			return evCh, lstore, nil
		}
		return nil, nil, errors.Errorf("this is for %s executor, not %s", executorName, name)
	}

	executortypes.RegisterEvChan(executortypes.Name(name), getEvChanFn, syncRuntimeToLstore)
}

// 目前edas没有使用事件驱动机制，因而每次用轮询去模拟
func (m *EDAS) WaitEvent(lstore *sync.Map) {
	eventAddr, ok := os.LookupEnv("CONSOLE_EVENT_ADDR")
	if !ok {
		eventAddr = events.CONSOLE_EVENT_ADDR
	}

	notifier, err := api.New(string(m.name)+events.SUFFIX_EDAS, map[string]interface{}{"HTTP": []string{eventAddr}})
	if err != nil {
		logrus.Errorf("new eventbox api error when executor(%s) init and handle events", m.name)
		return
	}

	logrus.Infof("executor(%s) in waitEvent", m.name)

	initStore := func(k string, v interface{}) error {
		reKey := etcdKeyToMapKey(k)
		if len(reKey) == 0 {
			return nil
		}

		r := v.(*spec.Runtime)
		if r.Executor != string(m.name) {
			return nil
		}
		lstore.Store(reKey, *r)
		return nil
	}

	em := events.GetEventManager()
	if err = em.MemEtcdStore.ForEach(context.Background(), "/dice/service/", spec.Runtime{}, initStore); err != nil {
		logrus.Errorf("executor(%s) foreach initStore error: %v", m.name, err)
	}

	keys := make([]string, 0)

	f := func(k, v interface{}) bool {
		r := v.(spec.Runtime)

		//logrus.Debugf("in edas loop inside f, key: %s, r.Executor: %s, m.name: %s", k.(string), r.Executor, m.name)
		// double check
		if r.Executor != string(m.name) {
			return true
		}

		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()

		var err error
		c := make(chan struct{}, 1)

		go func() {
			start := time.Now()
			defer func() {
				logrus.Infof("edas executor(%s) get status for key(%s) took %v", m.name, k.(string), time.Since(start))
			}()
			_, err = m.Status(ctx, r)
			c <- struct{}{}
		}()

		keys = append(keys, k.(string))
		//logrus.Debugf("in edas loop inside f before status, key: %s", k.(string))
		select {
		case <-c:
			if err != nil {
				logrus.Errorf("executor(%s)'s key(%s) for edas get status error: %v", m.name, k, err)
				return true
			}

			logrus.Infof("edas got key(%s) ", k.(string))

			var e events.RuntimeEvent
			e.EventType = events.EVENTS_TOTAL
			e.RuntimeName = k.(string)
			e.ServiceStatuses = make([]events.ServiceStatus, len(r.Services))
			for i, srv := range r.Services {
				e.ServiceStatuses[i].ServiceName = srv.Name
				e.ServiceStatuses[i].Replica = srv.Scale
				e.ServiceStatuses[i].ServiceStatus = string(srv.Status)
				// 状态为空字符串
				if len(e.ServiceStatuses[i].ServiceStatus) == 0 {
					e.ServiceStatuses[i].ServiceStatus = string(spec.StatusProgressing)
				}
				/*
					e.ServiceStatuses[i].InstanceStatuses = make([]events.InstanceStatus, len(srv.InstanceInfos))
					for j := range srv.InstanceInfos {
						e.ServiceStatuses[i].InstanceStatuses[j].Ip = srv.InstanceInfos[j].Ip
						e.ServiceStatuses[i].InstanceStatuses[j].ID = srv.InstanceInfos[j].Id
						if srv.InstanceInfos[j].Status == "Running" {
							e.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = "Healthy"
						} else {
							//TODO. modify this
							e.ServiceStatuses[i].InstanceStatuses[j].InstanceStatus = srv.InstanceInfos[j].Status
						}
					}
				*/
			}

			go notifier.Send(e)
			return true

		case <-ctx.Done():
			logrus.Errorf("executor(%s)'s key(%s) get status timeout", m.name, k)
			return true
		}
	}

	for {
		select {
		case <-time.After(10 * time.Second):
			lstore.Range(f)
		}

		logrus.Infof("lstore addr: %p, edas keys: %v", lstore, keys)
		keys = nil
	}
}

// 对etcd里的原始key做个处理
// /dice/service/services/staging-77 -> "services/staging-77"
func etcdKeyToMapKey(eKey string) string {
	fields := strings.Split(eKey, "/")
	if l := len(fields); l > 2 {
		return fields[l-2] + "/" + fields[l-1]
	}
	return ""
}
