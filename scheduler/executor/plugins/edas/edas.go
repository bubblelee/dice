package edas

import (
	"bytes"
	"context"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"terminus.io/dice/dice/pkg/dlock"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/executor/plugins/k8s"
	"terminus.io/dice/dice/scheduler/executor/util"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
	api "github.com/aliyun/alibaba-cloud-sdk-go/services/edas"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	kind = "EDAS"
	// 查询部署结果的循环次数
	loopCount = 2 * 60
	// edas k8s service namespace
	defaultNamespace = "default"
	// json prefix key
	prefixKey = "/dice/plugins/edas/"
	lockKey   = "/dice/dlock/edas/lock"
	// k8s min ready seconds
	minReadySeconds = 30
	notFound        = "not found"
)

var deleteOptions = &k8s.CascadingDeleteOptions{
	Kind:       "DeleteOptions",
	ApiVersion: "v1",
	// 'Foreground' - a cascading policy that deletes all dependents in the foreground
	// e.g. if you delete a deployment, this option would delete related replicaSets and pods
	// See more: https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#delete-24
	PropagationPolicy: "Foreground",
}

// EDAS plugin's configure
//
// EXECUTOR_EDAS_EDASFORSERVICE_ADDR="edas.cn-hangzhou.aliyuncs.com"
// EXECUTOR_EDAS_EDASFORSERVICE_ACCESSKEY="LTAIH0Ebt2Vdi9Kf"
// EXECUTOR_EDAS_EDASFORSERVICE_ACCESSSECRET="h1HpanNIR3eQjRHoRDLsK3OtVAbtt1"
// EXECUTOR_EDAS_EDASFORSERVICE_CLUSTERID="b0c2d9e3-bb0e-426e-8eea-8a4843b79c62"
// EXECUTOR_EDAS_EDASFORSERVICE_REGIONID="cn-hangzhou"
// EXECUTOR_EDAS_EDASFORSERVICE_KUBEADDR="127.0.0.1:8080"
// EXECUTOR_EDAS_EDASFORSERVICE_KUBEBASICAUTH=""
// EXECUTOR_EDAS_EDASFORSERVICE_REGADDR=""
func init() {
	executortypes.Register(kind, func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		addr, ok := options["ADDR"]
		if !ok {
			return nil, errors.Errorf("not found edas address in env variables")
		}
		accessKey, ok := options["ACCESSKEY"]
		if !ok {
			return nil, errors.Errorf("not found edas accessKey in env variables")
		}
		accessSecret, ok := options["ACCESSSECRET"]
		if !ok {
			return nil, errors.Errorf("not found edas accessKey in env variables")
		}
		clusterId, ok := options["CLUSTERID"]
		if !ok {
			return nil, errors.Errorf("not found edas clusterId in env variables")
		}
		regionId, ok := options["REGIONID"]
		if !ok {
			regionId = "cn-hangzhou"
		}

		client, err := api.NewClientWithAccessKeyAndDisableCompression(regionId, accessKey, accessSecret)
		if err != nil {
			return nil, errors.Wrap(err, "failed to new edas client with accessKey")
		}

		kubeAddr, ok := options["KUBEADDR"]
		if !ok {
			return nil, errors.Errorf("not found edas k8s addr in env variables")
		}

		kubeClient := httpclient.New()
		kubeBasicAuth, ok := options["KUBEBASICAUTH"]
		if !ok {
			userPasswd := strings.Split(kubeBasicAuth, ":")
			if len(userPasswd) == 2 {
				kubeClient.BasicAuth(userPasswd[0], userPasswd[1])
			}
		}

		regAddr, ok := options["REGADDR"]
		if !ok {
			return nil, errors.Errorf("not found dice registry addr in env variables")
		}

		store, err := jsonstore.New()
		if err != nil {
			return nil, errors.Errorf("failed to new json store: %v", err)
		}

		lock, err := dlock.New(lockKey)
		if err != nil {
			return nil, errors.Errorf("failed to new dlock: %s, error: %v", lockKey, err)
		}

		edas := &EDAS{
			name:         name,
			options:      options,
			addr:         addr,
			kubeAddr:     kubeAddr,
			regAddr:      regAddr,
			regionId:     regionId,
			accessKey:    accessKey,
			accessSecret: accessSecret,
			clusterId:    clusterId,
			lock:         lock,
			client:       client,
			kubeClient:   kubeClient,
			store:        store,
		}

		go edas.loopInspect()

		if disableEvent, ok := options["DISABLE_EVENT"]; ok && disableEvent == "true" {
			return edas, nil
		}

		evCh := make(chan *spec.StatusEvent, 10)
		// key是{runtimeNamespace}/{runtimeName}, value是对应spec.Runtime
		lstore := &sync.Map{}

		registerEventChanAndLocalStore(string(edas.name), evCh, lstore)

		go edas.WaitEvent(lstore)

		return edas, nil
	})
}

type EDAS struct {
	name     executortypes.Name
	options  map[string]string
	addr     string
	kubeAddr string
	// TODO: hack
	// dice 平台的 docker registry addr
	// 因为不同集群内部 DNS 无法访问，需要 hack 一把
	regAddr string
	// default: "cn-hangzhou"
	regionId string
	// 访问 EDAS openAPI 的key & secret
	accessKey    string
	accessSecret string
	// EDAS 集群 ID, 需要提前创建
	clusterId string
	// edas pop client
	client     *api.Client
	kubeClient *httpclient.HTTPClient
	store      jsonstore.JsonStore
	// 分布式锁
	lock *dlock.DLock
}

func (e *EDAS) Kind() executortypes.Kind {
	return kind
}

func (e *EDAS) Name() executortypes.Name {
	return e.name
}

func makeEdasKey(namespace, name string) string {
	return "/dice/plugins/edas/" + namespace + "/" + name
}

// TODO: 异步创建 runtime
func (e *EDAS) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	var err error
	var errChan chan error

	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("edas k8s create: invalid runtime spec")
	}

	logrus.Debugf("[EDAS] Create runtime, object: %+v", runtime)

	group := runtime.Namespace + "-" + runtime.Name

	flows, err := util.ParseServiceDependency(&runtime)
	if err != nil {
		return nil, errors.Wrapf(err, "parse service flow, runtime: %s", group)
	}

	go func() {
		if err = e.runAppFlow(context.Background(), flows, &runtime); err != nil {
			logrus.Errorf("[EDAS] failed to run runtime service flow: %v", err)

			errChan <- err

			// remove runtime & ignore error
			e.Remove(ctx, specObj)
			return
		}
	}()

	unlock, err := lock(ctx, e.lock)
	if err != nil {
		logrus.Errorf("[EDAS] Failed to lock by create runtime: %s, error: %v", group, err)
	} else {
		key := makeEdasKey(runtime.Namespace, runtime.Name)
		if err := e.store.Put(ctx, key, runtime); err != nil {
			logrus.Errorf("[EDAS] Failed to put json store(%s) by remove runtime: %s, error: %v", key, group, err)
		}
	}
	unlock()

	select {
	case err := <-errChan:
		return nil, err
	case <-ctx.Done():
	case <-time.After(5 * time.Second):
	}

	return nil, nil
}

func (e *EDAS) Destroy(ctx context.Context, specObj interface{}) error {

	if err := e.Remove(ctx, specObj); err != nil {
		return err
	}

	return nil
}

func (e *EDAS) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	var status spec.StatusDesc

	// 初始化一把，防止上层 console 无法识别
	status.Status = spec.StatusUnkonw

	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return status, errors.New("edas k8s status: invalid runtime spec")
	}

	group := runtime.Namespace + "-" + runtime.Name

	for i, svc := range runtime.Services {
		svc.Namespace = runtime.Namespace
		// init status
		runtime.Services[i].Status = spec.StatusUnkonw

		// check k8s deployment status
		status, _, err := e.getDeploymentStatus(group, &svc)
		if err != nil {
			//return status, errors.Wrapf(err, "get deployment(%s) status", svc.Name)
			continue
		}
		if status.Status != spec.StatusReady {
			status.LastMessage = fmt.Sprintf("deployment(%s) status is %v", runtime.Name, status)
			status.Status = spec.StatusError
			//return status, nil
			//continue
		}

		appName := group + "-" + svc.Name
		/*
			podStatuses, err := e.getPodsStatus(defaultNamespace, appName)
			if err != nil {
				logrus.Errorf("get deployment(%s/%s) pod status error: %v", defaultNamespace, svc.Name, err)
				continue
			}

			logrus.Debugf("in runtime(%s) Status interface, appName: %s, srv: %s, podStatus: %+v", group, appName, svc.Name, podStatuses)

			for _, ps := range podStatuses {
				runtime.Services[i].InstanceInfos = append(runtime.Services[i].InstanceInfos, spec.InstanceInfo{
					Id:     ps.Metadata.Name,
					Status: ps.Status.Phase,
					Ip:     ps.Status.PodIP,
				})
			}
		*/

		// check k8s service status
		if len(svc.Ports) != 0 {
			if srv, err := e.getK8sService(appName); err == nil {
				if len(srv.Status.LoadBalancer.Ingress) == 0 {
					status.LastMessage = fmt.Sprintf("deployment(%s): service(%s) is not ready", runtime.Name, appName)
					status.Status = spec.StatusError
					//return status, nil
					continue
				}
			} else {
				status.LastMessage = fmt.Sprintf("deployment(%s): service(%s) is not found or error", runtime.Name, appName)
				status.Status = spec.StatusError
				//return status, nil
				continue
			}
		}

		// check k8s ingress status
		if svc.Labels["IS_ENDPOINT"] == "true" {
			if ingress, err := e.getK8sIngress(appName); err == nil {
				if len(ingress.Status.LoadBalancer.Ingress) == 0 {
					status.LastMessage = fmt.Sprintf("deployment(%s): ingress(%s) is not ready", runtime.Name, appName)
					status.Status = spec.StatusError
					//return status, nil
					continue
				}
			} else {
				status.LastMessage = fmt.Sprintf("deployment(%s): ingress(%s) is not found or error", runtime.Name, appName)
				status.Status = spec.StatusError
				//return status, nil
				continue
			}
		}

		runtime.Services[i].Status = spec.StatusReady
	}

	isReady := true
	for _, s := range runtime.Services {
		if s.Status != spec.StatusReady {
			isReady = false
			break
		}
	}

	// 所有service都ready则runtime置为ready,否则状态保持为计算过程中的值
	if isReady {
		status.Status = spec.StatusReady
	}

	return status, nil
}

// 因为 edas 删除接口需要花很长的时间，超过了 console 设置的 timeout，所以并行删除 app
// 忽略删除错误。
func (e *EDAS) Remove(ctx context.Context, specObj interface{}) error {
	var err error
	var errChan chan error

	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return errors.New("edas k8s remove: invalid runtime spec")
	}

	group := runtime.Namespace + "-" + runtime.Name

	unlock, err := lock(ctx, e.lock)
	if err != nil {
		logrus.Errorf("[EDAS] Failed to lock by remove runtime: %s, error: %v", group, err)
	} else {
		key := makeEdasKey(runtime.Namespace, runtime.Name)
		if err := e.store.Remove(ctx, key, &runtime); err != nil {
			logrus.Errorf("[EDAS] Failed to remove json store(%s) by remove runtime: %s, error: %v", key, group, err)
		}
	}
	unlock()

	for _, srv := range runtime.Services {
		go func(s spec.Service) {
			// TODO: how to handle the error
			if err = e.removeService(ctx, group, &s); err != nil {
				errChan <- err
				return
			}
		}(srv)
	}

	// HACK: edas api 必然超时(> 10s), 这里需要等待 5s 的原因是让子弹飞一会儿。
	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
	case <-time.After(5 * time.Second):
	}

	return nil
}

func (e *EDAS) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	var err error
	var oldRun spec.Runtime

	newRun, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("edas k8s update: invalid runtime spec")
	}

	logrus.Debugf("[EDAS] Update runtime, object: %+v", newRun)

	group := newRun.Namespace + "-" + newRun.Name
	key := makeEdasKey(newRun.Namespace, newRun.Name)
	if err = e.store.Get(ctx, key, &oldRun); err != nil {
		logrus.Warnf("[EDAS] Failed to update runtime: %s, error: %v", group, err)
		if _, err := e.removeAndCreateRuntime(ctx, &newRun); err != nil {
			return nil, err
		}
	} else {
		if err = e.cyclicUpdateService(ctx, &newRun, &oldRun); err != nil {
			return nil, err
		}
	}

	return nil, nil
}

func (e *EDAS) updateCache(ctx context.Context, runtime *spec.Runtime) error {
	var err error
	var unlock func()

	group := runtime.Namespace + "-" + runtime.Name

	unlock, err = lock(ctx, e.lock)
	if err != nil {
		logrus.Errorf("[EDAS] Failed to lock by update runtime: %s, error: %v", group, err)
	} else {
		key := makeEdasKey(runtime.Namespace, runtime.Name)
		if err := e.store.Put(ctx, key, runtime); err != nil {
			logrus.Errorf("[EDAS] Failed to put json store(%s) by update runtime: %s, error: %v", key, group, err)
		}
	}
	unlock()

	return err
}

func (e *EDAS) Inspect(ctx context.Context, specObj interface{}) (interface{}, error) {

	runtime, ok := specObj.(spec.Runtime)
	if !ok {
		return nil, errors.New("edas k8s inspect: invalid runtime spec")
	}

	key := makeEdasKey(runtime.Namespace, runtime.Name)

	runTmp := &spec.Runtime{}
	if err := e.store.Get(ctx, key, runTmp); err == nil {
		return runTmp, nil
	}

	return &runtime, nil
}

func (e *EDAS) inspect(ctx context.Context, runtime *spec.Runtime) error {
	var status spec.StatusDesc

	group := runtime.Namespace + "-" + runtime.Name

	// 元数据信息从上层传入，这里只需要拿到runtime的状态并拼装到runtime里返回
	status, err := e.Status(ctx, *runtime)
	if err != nil {
		return err
	}

	logrus.Infof("[EDAS] Inspect runtime(%s) status: %+v", runtime.Name, status)
	runtime.Status = status.Status
	runtime.LastMessage = status.LastMessage

	for i, svc := range runtime.Services {
		appName := group + "-" + svc.Name

		if len(svc.Ports) > 0 {
			kubeSvc, err := e.getK8sService(appName)
			if err != nil {
				logrus.Warnf("[EDAS] Failed to inspect runtime(%s), service name: %s, error: %v",
					group, appName, err)
			} else {
				svcRecord := kubeSvc.Metadata.Name + ".default.svc.cluster.local"
				runtime.Services[i].ProxyIp = svcRecord
				runtime.Services[i].ProxyPorts = svc.Ports
				runtime.Services[i].Vip = svcRecord
			}
		}
	}

	return nil
}

func (e *EDAS) runAppFlow(ctx context.Context, flows [][]*spec.Service, runtime *spec.Runtime) error {

	group := runtime.Namespace + "-" + runtime.Name

	for i, batch := range flows {
		logrus.Infof("[EDAS] create runtime: %s run batch %d %+v", group, i+1, batch)

		for _, s := range batch {
			go func() {
				if err := e.createService(ctx, runtime, s); err != nil {
					logrus.Errorf("[EDAS] failed to create service: %s, error: %v",
						group+"-"+s.Name, err)
				}
			}()
			time.Sleep(1 * time.Second)
		}

		if err := e.waitRuntimeRunningOnBatch(ctx, batch, group); err != nil {
			return errors.Wrap(err, "wait service flow on batch")
		}
	}
	return nil
}

func (e *EDAS) removeAndCreateService(ctx context.Context, runtime *spec.Runtime, service *spec.Service) error {

	group := runtime.Namespace + "-" + runtime.Name

	// TODO: how to handle the error
	_ = e.removeService(ctx, group, service)

	return e.createService(ctx, runtime, service)
}

func (e *EDAS) createService(ctx context.Context, runtime *spec.Runtime, s *spec.Service) error {
	var err error

	serviceSpec, err := e.fillServiceSpec(s, runtime)
	if err != nil {
		return errors.Wrap(err, "fill service spec")
	}

	// 创建应用
	if err = e.insertApp(serviceSpec); err != nil {
		return errors.Wrap(err, "edas create app")
	}

	group := runtime.Namespace + "-" + runtime.Name

	// 暴露公网服务
	if err = e.createK8sIngress(group, s); err != nil {
		appName := group + "-" + s.Name
		err = e.deleteApp(appName)
		if err != nil {
			logrus.Errorf("[EDAS] failed to delete app(%s) by rollback: %v", appName, err)
		}
		return errors.Wrap(err, "edas create k8s ingress")
	}

	return nil
}

// TODO: how to handle the error
func (e *EDAS) removeService(ctx context.Context, group string, s *spec.Service) error {
	var err error

	appName := group + "-" + s.Name
	err = e.deleteApp(appName)
	if err != nil {
		// retry
		if err1 := e.deleteApp(appName); err1 != nil {
			logrus.Errorf("[EDAS] Failed to delete app(%s): %v", appName, err)
		}
	}

	// HACK: 不论调用 edas api 删除服务是否成功，都再尝试直接通过 k8s 删除相关的服务
	// 忽略错误
	// edas 接口即使调用成功，也会出现后台服务没有删掉的情况
	if err = e.deleteDeploymentAndService(group, s); err != nil {
		logrus.Warnf("Failed to delete deployments and service, appName: %s, error: %v", appName, err)
	}

	if s.Labels["IS_ENDPOINT"] == "true" {
		if err = e.deleteK8sIngress(appName); err != nil {
			logrus.Errorf("[EDAS] Failed to remove runtime(%s): %v", group, err)
		}
	}

	return nil
}

func (e *EDAS) cyclicUpdateService(ctx context.Context, newRuntime, oldRuntime *spec.Runtime) error {
	var errChan chan error

	group := newRuntime.Namespace + "-" + newRuntime.Name

	go func() {
		for _, newSvc := range newRuntime.Services {
			svcName := newSvc.Name
			oldSvc, err := findService(svcName, oldRuntime)
			// 新增的 service
			if err != nil {
				if err = e.createService(ctx, newRuntime, &newSvc); err != nil {
					logrus.Errorf("[EDAS] Failed to create service: %s, error: %v", svcName, err)
					errChan <- err
					return
				}
				continue
			}
			// ports & envs & cmd & mem 更新，需要重建 service
			if !intSliceEqual(newSvc.Ports, oldSvc.Ports) ||
				!stringMapEqual(newSvc.Env, oldSvc.Env) ||
				newSvc.Resources.Mem != oldSvc.Resources.Mem ||
				newSvc.Cmd != oldSvc.Cmd {
				if err = e.removeAndCreateService(ctx, newRuntime, &newSvc); err != nil {
					logrus.Errorf("[EDAS] Failed to remove and create service: %s, error: %v", svcName, err)
					errChan <- err
					return
				}
				continue
			}

			appName := group + "-" + svcName

			// 更新 image
			if newSvc.Image != oldSvc.Image {
				// 若不存在的话，则新建
				if _, err = e.getAppID(appName); err != nil {
					if err.Error() == notFound {
						logrus.Errorf("[EDAS] app(%s) not found, will create it ", appName)
						if err = e.createService(ctx, newRuntime, &newSvc); err != nil {
							logrus.Errorf("[EDAS] failed to create service(%s): %v", appName, err)
							errChan <- err
							return
						}
					} else {
						logrus.Errorf("[edas] failed to query app(%s) beforce update image: %s", appName, err)
						errChan <- err
						return
					}
				} else {
					if err = e.deployApp(appName, newSvc.Image); err != nil {
						logrus.Errorf("[EDAS] Failed to deploy app: %s, error: %v", appName, err)
						errChan <- err
						return
					}
				}
			}

			// 更新实例数
			if newSvc.Scale != oldSvc.Scale {
				if err = e.scaleApp(appName, newSvc.Scale); err != nil {
					logrus.Errorf("[EDAS] Failed to scale app: %s, error: %v", appName, err)
					errChan <- err
					return
				}
			}

			// 增删 ingress
			if newSvc.Labels["IS_ENDPOINT"] != oldSvc.Labels["IS_ENDPOINT"] {
				if newSvc.Labels["IS_ENDPOINT"] == "true" {
					if err := e.createK8sIngress(group, &newSvc); err != nil {
						logrus.Errorf("[EDAS] Failed to create k8s ingress, app name: %s, error: %v", appName, err)
						errChan <- err
						return
					}
				} else {
					if err := e.deleteK8sIngress(appName); err != nil {
						logrus.Errorf("[EDAS] Failed to delete k8s ingress, app name: %s, error: %v", appName, err)
						errChan <- err
						return
					}
				}
				continue
			}

			// 更新 ingress
			if newSvc.Labels["IS_ENDPOINT"] == "true" &&
				newSvc.Labels["HAPROXY_0_VHOST"] != oldSvc.Labels["HAPROXY_0_VHOST"] {
				if err := e.updateK8sIngress(group, &newSvc); err != nil {
					logrus.Errorf("[EDAS] Failed to update k8s ingress, app name: %s, error: %v", appName, err)
					errChan <- err
					return
				}
			}
		}

		// FIXME: ignore error
		_ = e.updateCache(ctx, newRuntime)
	}()

	// HACK: edas api 必然超时(> 10s), 这里需要等待 5s 的原因是便于该 runtime 的状态更新。
	// 防止异步还未执行时，上层就来查询状态。
	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
	case <-time.After(5 * time.Second):
	}

	return nil
}

func findService(svcName string, runtime *spec.Runtime) (*spec.Service, error) {
	if len(svcName) == 0 || runtime == nil {
		return nil, errors.Errorf("find service: invalid params")
	}

	for _, svc := range runtime.Services {
		if svcName == svc.Name {
			return &svc, nil
		}
	}

	return nil, errors.Errorf(notFound)
}

func intSliceEqual(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}

	if (a == nil) != (b == nil) {
		return false
	}

	for i, v := range a {
		if v != b[i] {
			return false
		}
	}

	return true
}

func stringMapEqual(a, b map[string]string) bool {
	if len(a) != len(b) {
		return false
	}

	for k, av := range a {
		if bv, ok := b[k]; !ok || bv != av {
			return false
		}
	}
	return true
}

func (e *EDAS) removeAndCreateRuntime(ctx context.Context, runtime *spec.Runtime) (interface{}, error) {
	var err error
	var errChan chan error

	group := runtime.Namespace + "-" + runtime.Name

	// 深度拷贝
	// Remove() 会进行回写，导致指针指向的底层数据被修改
	var buf bytes.Buffer
	var deepRun spec.Runtime
	if err := gob.NewEncoder(&buf).Encode(runtime); err != nil {
		return nil, err
	} else {
		if err := gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(&deepRun); err != nil {
			return nil, err
		}
	}

	go func() {
		if err = e.Remove(ctx, deepRun); err != nil {
			logrus.Errorf("[EDAS] Failed to update runtime: %s, error: %v", group, err)
			errChan <- err
		}

		if _, err = e.Create(ctx, *runtime); err != nil {
			logrus.Errorf("[EDAS] Failed to update runtime: %s, error: %v", group, err)
			errChan <- err
			return
		}
		// FIXME: ignore error
		_ = e.updateCache(ctx, runtime)
	}()

	// HACK: edas api 必然超时(> 10s), 这里需要等待 5s 的原因是便于该 runtime 的状态更新。
	// 防止异步还未执行时，上层就来查询状态。
	select {
	case err := <-errChan:
		return nil, err
	case <-ctx.Done():
	case <-time.After(5 * time.Second):
	}

	return nil, nil
}

func (e *EDAS) deleteDeploymentAndService(group string, service *spec.Service) error {

	appName := group + "-" + service.Name

	// 只有对外暴露端口的服务才有对应的k8s service
	if len(service.Ports) > 0 {
		if err := e.deleteK8sService(group, service); err != nil {
			logrus.Warnf("[EDAS] Failed to delete k8s service, appName: %s, error: %v", appName, err)
		}
	}

	return e.deleteK8sDeployment(group, service)
}

func (e *EDAS) deleteK8sService(group string, s *spec.Service) error {
	var b bytes.Buffer
	var kubeSvc *k8s.Service
	var err error

	appName := group + "-" + s.Name

	if kubeSvc, err = e.getK8sService(appName); err != nil {
		return errors.Wrapf(err, "get k8s service: %s", appName)
	}

	svcName := kubeSvc.Metadata.Name

	resp, err := e.kubeClient.Delete(e.kubeAddr).
		Path("/api/v1/namespaces/" + defaultNamespace + "/services/" + svcName).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete service(%s) failed", svcName)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s delete service(%s) status code: %v, resp body: %v", svcName, resp.StatusCode(), b.String())
	}

	return nil
}

func (e *EDAS) deleteK8sDeployment(group string, s *spec.Service) error {
	var b bytes.Buffer

	dep, err := e.getDeploymentInfo(group, s)
	if err != nil {
		return err
	}

	depName := dep.Metadata.Name

	resp, err := e.kubeClient.Delete(e.kubeAddr).
		Path("/apis/apps/v1beta1/namespaces/" + defaultNamespace + "/deployments/" + depName).
		JSONBody(deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return errors.Wrapf(err, "k8s delete deployment(%s) failed", depName)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		return errors.Errorf("k8s delete deployment: %s, status code: %v, resp body: %v",
			depName, resp.StatusCode(), b.String())
	}

	return nil
}

func (e *EDAS) createK8sIngress(group string, s *spec.Service) error {
	var kubeSvc *k8s.Service
	var err error

	// 需要对公网暴露服务
	if s.Labels["IS_ENDPOINT"] == "true" && len(s.Ports) > 0 {
		pHosts := strings.Split(s.Labels["HAPROXY_0_VHOST"], ",")
		if len(pHosts) == 0 {
			return errors.Errorf("[EDAS] service(%s) set label IS_ENDPOINT true but label HAPROXY_0_VHOST empty",
				s.Name)
		}

		appName := group + "-" + s.Name

		logrus.Infof("[EDAS] Start to create ingress for service: %s", appName)

		if kubeSvc, err = e.getK8sService(appName); err != nil {
			return errors.Wrapf(err, "get k8s service: %s", appName)
		}

		// 创建 ingress
		rules := make([]k8s.IngressRule, len(pHosts))
		for i, host := range pHosts {
			rules[i].Host = host
			rules[i].HTTP = &k8s.HTTPIngressRuleValue{
				Paths: []k8s.HTTPIngressPath{
					{
						Backend: k8s.IngressBackend{
							ServiceName: kubeSvc.Metadata.Name,
							ServicePort: s.Ports[0],
						},
					},
				},
			}
		}

		ingress := &k8s.Ingress{
			ApiVersion: "extensions/v1beta1",
			Kind:       "Ingress",
			Metadata: k8s.ObjectMeta{
				Name:      appName,
				Namespace: defaultNamespace,
			},
			Spec: k8s.IngressSpec{
				Rules: rules,
			},
		}

		resp, err := e.kubeClient.Post(e.kubeAddr).
			Path("/apis/extensions/v1beta1/namespaces/" + defaultNamespace + "/ingresses").
			JSONBody(ingress).
			Do().
			DiscardBody()

		if err != nil {
			logrus.Errorf("[EDAS] Failed to create k8s ingress(%s): %v", appName, err)
			return errors.Wrapf(err, "k8s create ingress(%s) failed", appName)
		}

		if resp == nil {
			logrus.Error("[EDAS] Failed to create k8s ingress(%s): response is null", appName)
			return errors.Errorf("response is null")
		}

		if !resp.IsOK() {
			logrus.Errorf("[EDAS] Failed to create k8s ingress: %s, statusCode: %v", appName, resp.StatusCode())
			return errors.Errorf("edas k8s create ingress, statusCode: %v", resp.StatusCode())
		}

		logrus.Infof("[EDAS] Successfully to create ingress for service: %s", appName)
	}

	return nil
}

func (e *EDAS) updateK8sIngress(group string, s *spec.Service) error {
	var ingress *k8s.Ingress
	var err error

	// 需要对公网暴露服务
	if s.Labels["IS_ENDPOINT"] == "true" {
		pHosts := strings.Split(s.Labels["HAPROXY_0_VHOST"], ",")
		if len(pHosts) == 0 {
			return errors.Errorf("[EDAS] service(%s) set label IS_ENDPOINT true but label HAPROXY_0_VHOST empty",
				s.Name)
		}

		appName := group + "-" + s.Name

		logrus.Infof("[EDAS] Start to update ingress for service: %s", appName)

		if ingress, err = e.getK8sIngress(appName); err != nil {
			return errors.Wrapf(err, "get k8s ingress: %s", appName)
		}

		// FIXME: 暂时只支持配置一个 host
		ingress.Spec.Rules[0].Host = pHosts[0]

		resp, err := e.kubeClient.Put(e.kubeAddr).
			Path("/apis/extensions/v1beta1/namespaces/" + defaultNamespace + "/ingresses/" + appName).
			JSONBody(ingress).
			Do().
			DiscardBody()

		if err != nil {
			logrus.Errorf("[EDAS] Failed to update k8s ingress(%s): %v", appName, err)
			return errors.Wrapf(err, "k8s update ingress(%s) failed", appName)
		}

		if resp == nil {
			logrus.Error("[EDAS] Failed to update k8s ingress(%s): response is null", appName)
			return errors.Errorf("response is null")
		}

		if !resp.IsOK() {
			logrus.Errorf("[EDAS] Failed to update k8s ingress: %s, statusCode: %v", appName, resp.StatusCode())
			return errors.Errorf("edas k8s update ingress, statusCode: %v", resp.StatusCode())
		}

		logrus.Infof("[EDAS] Successfully to update ingress for service: %s", appName)
	}

	return nil
}

func (e *EDAS) deleteK8sIngress(name string) error {
	var err error

	if len(name) == 0 {
		return errors.Errorf("delete ingress: invalid name")
	}

	logrus.Infof("[EDAS] Start to delete ingress for service: %s", name)

	resp, err := e.kubeClient.Delete(e.kubeAddr).
		Path("/apis/extensions/v1beta1/namespaces/" + defaultNamespace + "/ingresses/" + name).
		JSONBody(deleteOptions).
		Do().
		DiscardBody()

	if err != nil {
		logrus.Errorf("[EDAS] Failed to delete k8s ingress(%s): %v", name, err)
		return errors.Wrapf(err, "k8s delete ingress(%s) failed", name)
	}

	if resp == nil {
		logrus.Error("[EDAS] Failed to delete k8s ingress(%s): response is null", name)
		return errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		logrus.Errorf("[EDAS] Failed to delete k8s ingress(%s), statusCode: %v", name, resp.StatusCode())
		return errors.Errorf("edas k8s delete ingress, statusCode: %v", resp.StatusCode())
	}

	logrus.Infof("[EDAS] Successfully to delete ingress for service: %s", name)

	return nil
}

func (e *EDAS) getK8sIngress(name string) (*k8s.Ingress, error) {
	var err error
	var ingress k8s.Ingress

	if len(name) == 0 {
		return nil, errors.Errorf("get ingress: invalid name")
	}

	resp, err := e.kubeClient.Get(e.kubeAddr).
		Path("/apis/extensions/v1beta1/namespaces/" + defaultNamespace + "/ingresses/" + name).
		Do().
		JSON(&ingress)
	if err != nil {
		logrus.Errorf("[EDAS] Failed to get k8s ingress(%s): %v", name, err)
		return nil, errors.Wrapf(err, "k8s delete ingress(%s) failed", name)
	}

	if resp == nil {
		logrus.Error("[EDAS] Failed to get k8s ingress(%s): response is null", name)
		return nil, errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		logrus.Errorf("[EDAS] Failed to get k8s ingress(%s), statusCode: %v", name, resp.StatusCode())
		return nil, errors.Errorf("edas k8s get ingress, statusCode: %v", resp.StatusCode())
	}

	return &ingress, nil
}

// 创建应用
// InsertK8sApplication
// 问题1：edas 容器服务k8s集群，暂不支持私有镜像部署
// 问题2：服务名不支持"_"
func (e *EDAS) insertApp(spec *ServiceSpec) error {
	var req *api.InsertK8sApplicationRequest
	var resp *api.InsertK8sApplicationResponse

	logrus.Infof("[EDAS] Start to insert app: %s", spec.Name)

	req = api.CreateInsertK8sApplicationRequest()
	req.Headers["Pragma"] = "no-cache"
	req.Headers["Cache-Control"] = "no-cache"
	req.Headers["Connection"] = "keep-alive"
	req.SetDomain(e.addr)
	req.ClusterId = e.clusterId

	req.AppName = spec.Name
	req.Replicas = requests.NewInteger(spec.Instances)
	req.ImageUrl = spec.Image

	// TODO: edas 可配置的最小为1cpu，暂时先进行cpu共享
	// set request == limit
	//req.RequestsCpu = requests.NewInteger(int(spec.Cpu))
	//req.LimitCpu = requests.NewInteger(int(spec.Cpu))
	req.RequestsMem = requests.NewInteger(int(spec.Mem))
	req.LimitMem = requests.NewInteger(int(spec.Mem))

	// command & args
	// sh -c xxxxx
	if len(spec.Cmd) != 0 {
		req.Command = "sh"
		inputArgs := strings.Split("-c "+spec.Cmd, " ")

		type cArg struct {
			Argument string `json:"argument"`
		}
		var cArgs []cArg
		for _, inputArg := range inputArgs {
			cArgs = append(cArgs, cArg{Argument: inputArg})
		}

		body, err := json.Marshal(cArgs)
		if err != nil {
			return errors.Wrapf(err, "json marshal service cmd")
		}

		logrus.Debugf("[EDAS] insertApp command args: %s", string(body))
		req.CommandArgs = string(body)
	}

	if len(spec.Env) != 0 {
		envString, err := envToString(spec.Env)
		if err != nil {
			return err
		}
		req.Envs = envString
	}

	// Intranet Slb
	// edas 自动新购私网slb，类型为"性能共享型"
	if len(spec.Ports) > 0 {
		req.IntranetSlbProtocol = "TCP"
		req.IntranetSlbPort = requests.NewInteger(spec.Ports[0])
		req.IntranetTargetPort = requests.NewInteger(spec.Ports[0])
	}

	// TODO: repoId 怎么取？
	//req.RepoId = ??

	// TODO: health check, edas 暂不支持

	// InsertK8sApplication
	resp, err := e.client.InsertK8sApplication(req)
	if err != nil {
		return errors.Errorf("edas insert app, response http context: %s, error: %v", resp.GetHttpContentString(), err)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return errors.Errorf("failed to insert app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	// check edas app status
	if len(resp.ApplicationInfo.ChangeOrderId) != 0 {
		status, err := e.loopTerminationStatus(resp.ApplicationInfo.ChangeOrderId)
		if err != nil {
			return errors.Wrapf(err, "get insert status by loop")
		}

		if status != CHANGE_ORDER_STATUS_SUCC {
			return errors.Errorf("failed to get the change order of inserting app, status: %s", ChangeOrderStatusString[status])
		}
	}

	// 需要确认 k8s service 是否创建，来判断服务是否创建成功
	if !e.loopCheckK8sServiceIsCreated(spec) {
		logrus.Errorf("[EDAS] Failed to insert app: %s, k8s service is not ready.", spec.Name)
		return errors.Errorf("k8s service is not ready.")
	}

	logrus.Infof("[EDAS] Successfully to insert app: %s", spec.Name)
	return nil
}

// 删除应用
func (e *EDAS) deleteApp(appName string) error {
	var req *api.DeleteK8sApplicationRequest
	var resp *api.DeleteK8sApplicationResponse

	logrus.Infof("[EDAS] Start to delete app: %s", appName)

	// 获取appId
	appID, err := e.getAppID(appName)
	if err != nil {
		if err.Error() == notFound {
			return nil
		}
		return err
	}

	req = api.CreateDeleteK8sApplicationRequest()
	req.Headers["Pragma"] = "no-cache"
	req.Headers["Cache-Control"] = "no-cache"
	req.Headers["Connection"] = "keep-alive"
	req.SetDomain(e.addr)
	req.AppId = appID

	// DeleteApplicationRequest
	resp, err = e.client.DeleteK8sApplication(req)
	if err != nil {
		return errors.Errorf("response http context: %s, error: %v", resp.GetHttpContentString(), err)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return errors.Errorf("failed to delete app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	if len(resp.ChangeOrderId) != 0 {
		status, err := e.loopTerminationStatus(resp.ChangeOrderId)
		if err != nil {
			return errors.Wrapf(err, "get delete status by loop")
		}

		if status != CHANGE_ORDER_STATUS_SUCC {
			return errors.Errorf("failed to get the status of deleting app, status = %s", ChangeOrderStatusString[status])
		}
	}

	logrus.Infof("[EDAS] Successfully to delete app: %s", appName)
	return nil
}

// 获取应用ID
func (e *EDAS) getAppID(name string) (appid string, err error) {
	var req *api.ListApplicationRequest
	var resp *api.ListApplicationResponse

	req = api.CreateListApplicationRequest()
	req.SetDomain(e.addr)

	// 获取应用列表
	resp, err = e.client.ListApplication(req)
	if err != nil {
		return "", errors.Wrap(err, "edas list app")
	}

	if resp == nil {
		return "", errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return "", errors.Errorf("failed to list app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	for _, app := range resp.ApplicationList.Application {
		if name == app.Name {
			return app.AppId, nil
		}
	}

	return "", errors.Errorf(notFound)
}

//

// 循环获取任务发布单结果
func (e *EDAS) loopTerminationStatus(orderId string) (ChangeOrderStatus, error) {
	var status ChangeOrderStatus
	var err error

	retry := 2
	for i := 0; i < loopCount; i++ {
		time.Sleep(5 * time.Second)

		status, err = e.getChangeOrderInfo(orderId)
		if err != nil {
			return status, err
		}

		if status == CHANGE_ORDER_STATUS_PENDING || status == CHANGE_ORDER_STATUS_EXECUTING {
			continue
		}

		if status == CHANGE_ORDER_STATUS_SUCC || retry <= 0 {
			return status, nil
		}
		retry--
	}

	return status, errors.Errorf("get change order info timeout.")
}

// 获取检查 k8s service 是否创建成功
func (e *EDAS) loopCheckK8sServiceIsCreated(spec *ServiceSpec) bool {

	// 没有配置端口，则跳过 k8s service 的检查
	if len(spec.Ports) <= 0 {
		return true
	}

	// 循环检查 k8s service
	for i := 0; i < 6; i++ {
		if _, err := e.getK8sService(spec.Name); err == nil {
			return true
		}
		time.Sleep(5 * time.Second)
	}

	return false
}

// 查询变更详情
func (e *EDAS) getChangeOrderInfo(orderId string) (ChangeOrderStatus, error) {
	var req *api.GetChangeOrderInfoRequest
	var resp *api.GetChangeOrderInfoResponse
	var err error

	req = api.CreateGetChangeOrderInfoRequest()
	req.SetDomain(e.addr)
	req.ChangeOrderId = orderId

	if resp, err = e.client.GetChangeOrderInfo(req); err != nil {
		return CHANGE_ORDER_STATUS_ERROR, errors.Wrap(err, "edas get change order info")
	}

	if resp == nil {
		return CHANGE_ORDER_STATUS_ERROR, errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return CHANGE_ORDER_STATUS_ERROR, errors.Errorf("failed to get change order info, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	status := ChangeOrderStatus(resp.ChangeOrderInfo.Status)
	return status, nil
}

// 查询发布单历史列表
func (e *EDAS) listRecentChangeOrderInfo(appName string) (*api.ChangeOrderList, error) {
	var req *api.ListRecentChangeOrderRequest
	var resp *api.ListRecentChangeOrderResponse

	// 获取appId
	appID, err := e.getAppID(appName)
	if err != nil {
		return nil, err
	}

	req = api.CreateListRecentChangeOrderRequest()
	req.SetDomain(e.addr)
	req.AppId = appID

	if resp, err = e.client.ListRecentChangeOrder(req); err != nil {
		return nil, errors.Wrap(err, "edas list recent change order info")
	}

	if resp == nil {
		return nil, errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return nil, errors.Errorf("failed to list recent change order info, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	return &resp.ChangeOrderList, nil
}

func (e *EDAS) queryAppStatus(appName string) (AppStatus, error) {
	var req *api.QueryApplicationStatusRequest
	var resp *api.QueryApplicationStatusResponse
	var state = AppStatusRunning

	appID, err := e.getAppID(appName)
	if err != nil {
		return state, err
	}

	req = api.CreateQueryApplicationStatusRequest()
	req.SetDomain(e.addr)
	req.AppId = appID

	resp, err = e.client.QueryApplicationStatus(req)
	if err != nil {
		return state, err
	}

	if resp == nil {
		return state, errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return state, errors.Errorf("failed to query edas app(name: %s) status, edasCode: %d, message: %s", appName, resp.Code, resp.Message)
	}

	var orderList *api.ChangeOrderList
	if orderList, err = e.listRecentChangeOrderInfo(appName); err != nil {
		return state, errors.Wrap(err, "list recent change order info")
	}

	sort.Sort(ByCreateTime(orderList.ChangeOrder))
	lastOrderType := ChangeType(orderList.ChangeOrder[len(orderList.ChangeOrder)-1].CoType)

	if len(resp.AppInfo.EccList.Ecc) == 0 {
		state = AppStatusStopped
	} else {
		// 可能会有多实例，只要有一个不处于running，就返回
		for _, ecc := range resp.AppInfo.EccList.Ecc {
			appState := AppState(ecc.AppState)
			taskState := TaskState(ecc.TaskState)
			if appState == APP_STATE_AGENT_OFF || appState == APP_STATE_RUNNING_BUT_URL_FAILED {
				if taskState == TASK_STATE_PROCESSING {
					state = AppStatusDeploying
				} else {
					state = AppStatusFailed
				}
				break
			}
			if appState == APP_STATE_STOPPED {
				if taskState == TASK_STATE_PROCESSING {
					state = AppStatusDeploying
				} else if taskState == TASK_STATE_FAILED {
					state = AppStatusFailed
					break
				} else if taskState == TASK_STATE_UNKNOWN {
					state = AppStatusUnknown
					break
				} else if taskState == TASK_STATE_SUCCESS {
					if lastOrderType != CHANGE_TYPE_CREATE {
						state = AppStatusStopped
						break
					} else {
						state = AppStatusDeploying
					}
				}
			}
		}
	}

	logrus.Infof("[EDAS] Successfully to query app status: %v, appName: %s", state, appName)
	return state, nil
}

// 容器服务k8s service dns record: svc.Name + ".default.svc.cluster.local"
func (e *EDAS) getK8sService(name string) (*k8s.Service, error) {
	var err error

	prefix := "intranet-" + name
	if len(name) == 0 {
		return nil, errors.Errorf("get k8s service: invalid params")
	}

	svcList := &k8s.ServiceList{}
	// TODO: use selector
	resp, err := e.kubeClient.Get(e.kubeAddr).
		Path("/api/v1/namespaces/"+defaultNamespace+"/services").
		Header("Content-Type", "application/json").
		Do().
		JSON(svcList)
	if err != nil {
		return nil, errors.Wrapf(err, "get k8s service, prefix: %s", prefix)
	}

	if resp == nil {
		return nil, errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		return nil, errors.Errorf("failed to get k8s service, prefix: %s, statusCode: %s",
			prefix, resp.StatusCode())
	}

	for _, svc := range svcList.Items {
		if svc.Spec.Type == k8s.ServiceTypeLoadBalancer &&
			strings.Compare(svc.Spec.Selector["edas-domain"], "edas-admin") == 0 &&
			strings.HasPrefix(svc.Metadata.Name, prefix) {
			return &svc, nil
		}
	}

	return nil, errors.Errorf("not found k8s service")
}

func (e *EDAS) getDeploymentStatus(group string, srv *spec.Service) (spec.StatusDesc, string, error) {
	var status spec.StatusDesc

	status.Status = spec.StatusUnkonw

	dep, err := e.getDeploymentInfo(group, srv)
	if err != nil {
		return status, "", err
	}

	dps := dep.Status
	// this would not happen in theory
	if len(dps.Conditions) == 0 {
		status.Status = spec.StatusUnkonw
		status.LastMessage = "could not get status condition"
		return status, "", nil
	}

	for _, c := range dps.Conditions {
		if c.Type == k8s.DeploymentReplicaFailure && c.Status == "True" {
			status.Status = spec.StatusFailing
			return status, "", nil
		}
		if c.Type == k8s.DeploymentAvailable && c.Status == "False" {
			status.Status = spec.StatusFailing
			return status, "", nil
		}
	}

	if dps.Replicas == dps.ReadyReplicas &&
		dps.Replicas == dps.AvailableReplicas &&
		dps.Replicas == dps.UpdatedReplicas {
		if dps.Replicas > 0 {
			status.Status = spec.StatusReady
			status.LastMessage = fmt.Sprintf("deployment(%s) is running", dep.Metadata.Name)
		} else {
			// 只有在被删除的一瞬间才有这种状态
			status.LastMessage = fmt.Sprintf("deployment(%s) replica is 0", dep.Metadata.Name)
		}
	}

	labels := dep.Metadata.Labels["edas.appid"] + "," + dep.Metadata.Labels["edas.groupid"]
	return status, labels, nil
}

func (e *EDAS) getDeploymentInfo(group string, srv *spec.Service) (*k8s.Deployment, error) {
	deps := &k8s.DeploymentList{}

	prefix := group + "-" + srv.Name

	// TODO: use label selector
	resp, err := e.kubeClient.Get(e.kubeAddr).
		Path("/apis/apps/v1/namespaces/"+defaultNamespace+"/deployments").
		Header("Content-Type", "application/json").
		Do().
		JSON(&deps)
	if err != nil {
		return nil, err
	}

	if resp == nil {
		return nil, errors.Errorf("response is null")
	}

	if !resp.IsOK() {
		return nil, errors.Errorf("failed to get deployment info, namespace: %s, service name: %s, statusCode: %s",
			srv.Namespace, srv.Name, resp.StatusCode())
	}

	for _, dep := range deps.Items {
		if strings.HasPrefix(dep.Metadata.Name, prefix) &&
			strings.Compare(dep.Metadata.Labels["edas-domain"], "edas-admin") == 0 {
			return &dep, nil
		}
	}

	return nil, errors.Errorf("not found k8s deployment")
}

func (e *EDAS) generateServiceEnvs(s *spec.Service, runtime *spec.Runtime) (map[string]string, error) {
	var envs map[string]string

	group := runtime.Namespace + "-" + runtime.Name

	envs = s.Env

	addEnv := func(s *spec.Service, envs *map[string]string) error {
		appName := group + "-" + s.Name
		kubeSvc, err := e.getK8sService(appName)
		if err != nil {
			return err
		}

		svcRecord := kubeSvc.Metadata.Name + ".default.svc.cluster.local"
		// 添加{serviceName}_HOST
		key := makeEnvVariableName(s.Name) + "_HOST"
		(*envs)[key] = svcRecord

		// {serviceName}_PORT 指向第一个端口
		if len(s.Ports) > 0 {
			key := makeEnvVariableName(s.Name) + "_PORT"
			(*envs)[key] = strconv.Itoa(s.Ports[0])
		}

		// 有多个端口的情况下，依次使用{serviceName}_PORT0, {serviceName}_PORT1,...
		for i, port := range s.Ports {
			key := makeEnvVariableName(s.Name) + "_PORT" + strconv.Itoa(i)
			(*envs)[key] = strconv.Itoa(port)
		}

		return nil
	}

	// TODO: 所有容器都有所有service的环境变量,用于addon部署
	// 由于edas是需要在创建完app之后，才能确定 service name, 导致无法预先塞入全量环境变量
	// edas 不部署addon, 暂不支持 GLOBAL；但是需要部署低配版addon 进行测试，所以不返回错误。
	if runtime.ServiceDiscoveryMode == "GLOBAL" {
		//return nil, errors.Errorf("not support ServiceDiscoveryMode: %v", runtime.ServiceDiscoveryMode)
	} else {
		for _, name := range s.Depends {
			var depSvc *spec.Service
			for _, svc := range runtime.Services {
				if svc.Name == name {
					depSvc = &svc
					break
				}
			}
			// not found
			if depSvc == nil {
				return nil, errors.Errorf("not find service: %s", name)
			}

			if len(depSvc.Ports) == 0 {
				continue
			}

			// 注入{depSvc}_HOST, {depSvc}_PORT等
			if err := addEnv(depSvc, &envs); err != nil {
				return nil, err
			}

			depAppName := group + "-" + depSvc.Name
			if s.Labels["IS_ENDPOINT"] == "true" && len(depSvc.Ports) > 0 {
				kubeSvc, err := e.getK8sService(depAppName)
				if err != nil {
					return nil, err
				}
				svcRecord := kubeSvc.Metadata.Name + ".default.svc.cluster.local"
				port := depSvc.Ports[0]
				envs["BACKEND_URL"] = svcRecord + ":" + strconv.Itoa(port)
			}
		}
	}

	return envs, nil
}

func (e *EDAS) fillServiceSpec(s *spec.Service, runtime *spec.Runtime) (*ServiceSpec, error) {
	var envs map[string]string
	var err error

	group := runtime.Namespace + "-" + runtime.Name
	appName := group + "-" + s.Name

	svcSpec := &ServiceSpec{
		Name: appName,
		// TODO:
		// tmp hacking for registry
		Image: strings.Replace(s.Image,
			"docker-registry.registry.marathon.mesos:5000",
			e.regAddr, -1),
		Instances: s.Scale,
		Mem:       int(s.Resources.Mem),
		Ports:     s.Ports,
	}

	// CPU 配置最低1核起步，且不支持小数点
	if s.Resources.Cpu > 1.0 {
		cpu := math.Floor(s.Resources.Cpu + 0.5)
		svcSpec.Cpu = int(cpu)
	} else {
		svcSpec.Cpu = 1
	}

	if envs, err = e.generateServiceEnvs(s, runtime); err != nil {
		return nil, err
	}

	svcSpec.Env = envs

	logrus.Infof("[EDAS] fill service spec: %+v", svcSpec)

	return svcSpec, nil
}

func (e *EDAS) waitRuntimeRunningOnBatch(ctx context.Context, batch []*spec.Service, group string) error {
	for {
		var (
			err    error
			status AppStatus
		)
		done := map[string]struct{}{}

		time.Sleep(5 * time.Second)
		for _, srv := range batch {
			if _, ok := done[srv.Name]; ok {
				continue
			}
			appName := group + "-" + srv.Name
			// 1. 从 edas 确认 app status
			if status, err = e.queryAppStatus(appName); err != nil {
				logrus.Errorf("[EDAS] failed to query app(name: %s) status: %v", appName, err)
				continue
			}

			if status == AppStatusRunning {
				// 2. app status 等于 running之后，再确认 k8s service 是否已经就绪
				if len(srv.Ports) != 0 {
					if kubeSrv, err := e.getK8sService(appName); err == nil {
						if len(kubeSrv.Status.LoadBalancer.Ingress) != 0 {
							done[srv.Name] = struct{}{}
						}
					}
				}
			}
		}

		if len(done) == len(batch) {
			logrus.Infof("[EDAS] Successfully to wait runtime running on batch: %v", batch)
			return nil
		}
	}
	return nil
}

func makeEnvVariableName(str string) string {
	return strings.ToUpper(strings.Replace(str, "-", "_", -1))
}

func (e *EDAS) loopInspect() {
	var keys []string
	var err error
	var ctx = context.Background()

	for {
		time.Sleep(10 * time.Second)

		if keys, err = e.store.ListKeys(ctx, prefixKey); err != nil {
			logrus.Errorf("[EDAS] Failed to list edas runtimes's key: %v", err)
			continue
		}

		for _, key := range keys {
			time.Sleep(3 * time.Second)

			runtime := &spec.Runtime{}
			if err = e.store.Get(ctx, key, runtime); err != nil {
				logrus.Warnf("[EDAS] Failed to get store by key: %s, error: %v", key, err)
				continue
			} else if err = e.inspect(ctx, runtime); err != nil {
				logrus.Warnf("[EDAS] Failed to inspect runtime(%s), error: %v", runtime.Name, err)
				continue
			}

			unlock, err := lock(ctx, e.lock)
			if err != nil {
				logrus.Errorf("[EDAS] Failed to lock in loopInspect: %v", err)
				unlock()
				continue
			}

			if err = e.store.Put(ctx, key, *runtime); err != nil {
				logrus.Errorf("[EDAS] Failed to put store by key: %s, error: %v", key, err)
			}
			unlock()
		}
	}
}

func lock(ctx context.Context, lock *dlock.DLock) (func(), error) {
	var isCanceled bool
	var locked bool

	cleanup := func() {
		if isCanceled {
			return
		}
		logrus.Infof("[EDAS] etcdlock: unlock, key: %s", lock.Key())
		if err := lock.Unlock(); err != nil {
			logrus.Errorf("[EDAS] etcdlock unlock err: %v", err)
			return
		}

	}

	go func() {
		time.Sleep(3 * time.Second)
		if !locked && !isCanceled {
			logrus.Warnf("[EDAS] not get lock yet after 3s")
		}
	}()
	if err := lock.Lock(ctx); err != nil {
		if err == context.Canceled {
			isCanceled = true
			logrus.Infof("[EDAS] etcdlock: %v", err)
			return cleanup, nil
		}
		return cleanup, err
	}
	locked = true
	logrus.Infof("[EDAS] etcdlock: lock, key: %s", lock.Key())

	return cleanup, nil
}

func envToString(env map[string]string) (string, error) {
	type appEnv struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	}
	var appEnvs []appEnv

	for k, v := range env {
		appEnvs = append(appEnvs, appEnv{
			Name:  k,
			Value: v,
		})
	}

	res, err := json.Marshal(appEnvs)
	if err != nil {
		return "", errors.Wrapf(err, "failed to json marshal map env")
	}

	return string(res), nil
}

// 部署应用
// 作用：该接口的作用为replace，暂时只支持image tag的更新
func (e *EDAS) deployApp(name, image string) error {
	var req *api.DeployK8sApplicationRequest
	var resp *api.DeployK8sApplicationResponse
	var imageTag string

	if len(name) == 0 || len(image) == 0 {
		return errors.Errorf("deploy app: invalid params")
	}

	// examples:
	// 1. hub.docker.app.terminus.io/terminus-consul-test
	// 2. hub.docker.app.terminus.io/terminus-consul-test:v1.0.0
	// 3. docker-registry.registry.marathon.mesos:5000/terminus-consul-test
	// 4. docker-registry.registry.marathon.mesos:5000/terminus-consul-test:v1.0.0
	fileds := strings.Split(image, ":")
	switch len(fileds) {
	case 1:
		imageTag = "latest"
	case 2:
		suffix := fileds[1]
		if strings.Contains(suffix, "/") {
			imageTag = "latest"
		} else {
			imageTag = suffix
		}
	case 3:
		imageTag = fileds[2]
	default:
		return errors.Errorf("image url is invalid: %s", image)
	}

	logrus.Debugf("deploy app image tag: %s", imageTag)

	// 获取appId
	appID, err := e.getAppID(name)
	if err != nil {
		return err
	}

	req = api.CreateDeployK8sApplicationRequest()
	req.Headers["Pragma"] = "no-cache"
	req.Headers["Cache-Control"] = "no-cache"
	req.Headers["Connection"] = "keep-alive"
	req.SetDomain(e.addr)

	req.AppId = appID
	req.ImageTag = imageTag

	// HACK: edas don't support k8s container probe
	// 该值等价于 k8s min-ready-seconds, 进行粗粒度控制
	// https://kubernetes.io/docs/concepts/workloads/controllers/deployment/?spm=a2c4g.11186623.2.3.7N5Zxk#min-ready-seconds
	req.BatchWaitTime = requests.NewInteger(minReadySeconds)

	// TODO: edas don't support envs
	//if len(spec.Env) != 0 {
	//	envString, err := envToString(spec.Env)
	//	if err != nil {
	//		return err
	//	}
	//	req.Envs = envString
	//	logrus.Debugf("[EDAS] deploy app envs: %s", req.Envs)
	//}

	resp, err = e.client.DeployK8sApplication(req)
	if err != nil {
		return errors.Errorf("response http context: %s, error: %v", resp.GetHttpContentString(), err)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return errors.Errorf("failed to deploy app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	if len(resp.ChangeOrderId) != 0 {
		status, err := e.loopTerminationStatus(resp.ChangeOrderId)
		if err != nil {
			return errors.Wrapf(err, "failed to get the status of deploying app")
		}

		if status != CHANGE_ORDER_STATUS_SUCC {
			return errors.Errorf("failed to get the status of deploying app, status: %s", ChangeOrderStatusString[status])
		}
	}

	return nil
}

// 实例伸缩
// ScaleK8sApplicationRequest
func (e *EDAS) scaleApp(name string, scale int) error {
	var req *api.ScaleK8sApplicationRequest
	var resp *api.ScaleK8sApplicationResponse

	if len(name) == 0 || scale < 0 {
		return errors.Errorf("edas scale app: invalid param")
	}

	// 获取appId
	appID, err := e.getAppID(name)
	if err != nil {
		return err
	}

	req = api.CreateScaleK8sApplicationRequest()
	req.Headers["Pragma"] = "no-cache"
	req.Headers["Cache-Control"] = "no-cache"
	req.Headers["Connection"] = "keep-alive"
	req.SetDomain(e.addr)

	req.AppId = appID
	req.Replicas = requests.NewInteger(scale)

	resp, err = e.client.ScaleK8sApplication(req)
	if err != nil {
		return errors.Errorf("response http context: %s, error: %v", resp.GetHttpContentString(), err)
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return errors.Errorf("failed to scale app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	// TODO: 等待 edas 修复 ChangeOrderId 返回 struct{} 的问题
	//if len(resp.ChangeOrderId) != 0 {
	//	status, err := e.loopTerminationStatus(resp.ChangeOrderId)
	//	if err != nil {
	//		return errors.Wrapf(err, "failed to get the status of scaling app")
	//	}
	//
	//	if status != CHANGE_ORDER_STATUS_SUCC {
	//		return errors.Errorf("failed to get the status of scaling app, status: %s", ChangeOrderStatusString[status])
	//	}
	//}

	return nil
}

// 更新 service resoures，暂时只支持配置 mem
// cpu 使用了共享模式
// FIXME: 该接口只支持配置 cpu & mem limit 值，改完之后会把 request 值设置为 0
func (e *EDAS) updateAppResources(name string, mem int) error {
	var req *api.UpdateK8sApplicationConfigRequest
	var resp *api.UpdateK8sApplicationConfigResponse
	var err error

	if len(name) == 0 || mem <= 0 {
		return errors.Errorf("edas update resources: invalid param")
	}

	appID, err := e.getAppID(name)
	if err != nil {
		return err
	}

	req = api.CreateUpdateK8sApplicationConfigRequest()
	req.Headers["Pragma"] = "no-cache"
	req.Headers["Cache-Control"] = "no-cache"
	req.Headers["Connection"] = "keep-alive"
	req.SetDomain(e.addr)

	req.AppId = appID
	req.ClusterId = e.clusterId
	// 共享 cpu
	req.CpuLimit = strconv.Itoa(0)
	req.MemoryLimit = strconv.Itoa(mem)

	resp, err = e.client.UpdateK8sApplicationConfig(req)
	if err != nil {
		return err
	}

	if resp == nil {
		return errors.Errorf("response is null")
	}

	if resp.Code != 200 {
		return errors.Errorf("failed to scale app, edasCode: %d, message: %s", resp.Code, resp.Message)
	}

	if len(resp.ChangeOrderId) != 0 {
		status, err := e.loopTerminationStatus(resp.ChangeOrderId)
		if err != nil {
			return errors.Wrapf(err, "failed to get the status of scaling app")
		}

		if status != CHANGE_ORDER_STATUS_SUCC {
			return errors.Errorf("failed to get the status of scaling app, status: %s", ChangeOrderStatusString[status])
		}
	}

	return nil
}

// 适用于edas的label，edas.appid 和 edas.groupid
func (e *EDAS) getPodsStatus(namespace string, label string) ([]k8s.PodItem, error) {
	var pi []k8s.PodItem
	var b bytes.Buffer

	resp, err := e.kubeClient.Get(e.kubeAddr).
		Path("/api/v1/namespaces/"+namespace+"/pods").
		Param("labelSelector", "app="+label).
		JSONBody(&deleteOptions).
		Do().
		Body(&b)

	if err != nil {
		return pi, errors.Wrapf(err, "k8s get podStatuses by label(%s) err: %v", err, label)
	}

	if !resp.IsOK() {
		return pi, errors.Errorf("k8s get podStatuses by label(%s) status code: %v, resp body: %v", resp.StatusCode(), b.String())
	}

	var pl k8s.PodList
	if err := json.Unmarshal(b.Bytes(), &pl); err != nil {
		return pi, err
	}
	for _, item := range pl.Items {
		pi = append(pi, k8s.PodItem{
			Metadata: item.Metadata,
			Status:   item.Status,
		})
	}
	return pi, nil
}
