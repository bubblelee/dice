// +build !default

package edas

import (
	"context"
	"sort"
	"testing"
	"time"

	"terminus.io/dice/dice/pkg/dlock"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/scheduler/spec"

	api "github.com/aliyun/alibaba-cloud-sdk-go/services/edas"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

const (
	//appImage = "registry.cn-hangzhou.aliyuncs.com/terminus/edas-yhp-test:1.0-RC24"
	appImage = "nginx"
)

var runtime = spec.Runtime{
	Dice: spec.Dice{
		Namespace:            "default",
		Name:                 "runtime",
		ServiceDiscoveryMode: "DEPEND",
		Services: []spec.Service{
			{
				Name:  "lbl-k8s-test1",
				Image: appImage,
				Scale: 1,
				Resources: spec.Resources{
					Cpu: 1,
					Mem: 1024,
				},
				Ports: []int{
					80,
				},
				Env: map[string]string{
					"key1": "value1",
				},
				Labels: map[string]string{
					"IS_ENDPOINT":     "true",
					"HAPROXY_0_VHOST": "lbl-test1.sale.vatti.com.cn",
				},
			},
			{
				Name:  "lbl-k8s-test2",
				Image: appImage,
				Scale: 1,
				Resources: spec.Resources{
					Cpu: 1,
					Mem: 1024,
				},
				Ports: []int{
					80,
				},
				Env: map[string]string{
					"key2": "value2",
				},
				Depends: []string{
					"lbl-k8s-test1",
				},
			},
			{
				Name:  "lbl-k8s-test3",
				Image: appImage,
				Scale: 1,
				Resources: spec.Resources{
					Cpu: 1,
					Mem: 1024,
				},
				Ports: []int{
					80,
				},
				Env: map[string]string{
					"key3": "value3",
				},
				Depends: []string{
					"lbl-k8s-test1",
					"lbl-k8s-test2",
				},
			},
		},
	},
}

func newEDAS() (*EDAS, error) {
	var err error
	var e = &EDAS{
		// 华南
		addr:         "edas.cn-shenzhen.aliyuncs.com",
		regionId:     "cn-shenzhen",
		accessKey:    "LTAIPtrrZgl4RMCz",
		accessSecret: "h86gb6CqhS86A4GEDjyqdpvVxzr6xs",
		clusterId:    "fd040d8a-c39f-4e2f-b2ed-dbea3ef31d3d",

		// 华东
		//addr:         "edas.cn-hangzhou.aliyuncs.com",
		//regionId:     "cn-hangzhou",
		//accessKey:    "LTAIH0Ebt2Vdi9Kf",
		//accessSecret: "h1HpanNIR3eQjRHoRDLsK3OtVAbtt1",
		//clusterId:    "b0c2d9e3-bb0e-426e-8eea-8a4843b79c62",
		kubeAddr: "127.0.0.1:8080",
	}

	e.client, err = api.NewClientWithAccessKeyAndDisableCompression(e.regionId, e.accessKey, e.accessSecret)
	if err != nil {
		return e, errors.Wrap(err, "new edas client with access key")
	}

	e.kubeClient = httpclient.New()

	e.store, err = jsonstore.New()
	if err != nil {
		return nil, errors.Errorf("failed to new json store: %v", err)
	}

	e.lock, err = dlock.New(lockKey)
	if err != nil {
		return nil, errors.Errorf("failed to new dlock: %s, error: %v", lockKey, err)
	}

	return e, nil
}

func TestCreate(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	_, err = e.Create(ctx, runtime)
	assert.Nil(t, err)

	closeCh := make(chan struct{})
	go func() {
		for i := 0; i < 100; i++ {
			time.Sleep(10 * time.Second)
			status, err := e.Status(ctx, runtime)
			if err == nil {
				t.Logf("Test get status: %v", status)
			}
			if status.Status == spec.StatusReady {
				t.Logf("Test create runtime is ready.")
				break
			}
			if status.Status == spec.StatusFailing {
				t.Fatal("Test create runtime is failed.")
			}

		}
		closeCh <- struct{}{}
	}()

	<-closeCh
}

func TestStatus(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	status, err := e.Status(ctx, runtime)
	assert.Nil(t, err)

	t.Logf("Test get runtime status: %v", status)
}

func TestUpdate(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	for i := 0; i < len(runtime.Services); i++ {
		runtime.Services[i].Image = "nginx:1.15"
		runtime.Services[i].Scale = 2
		runtime.Services[i].Resources.Mem = 1024
	}

	_, err = e.Update(ctx, runtime)
	assert.Nil(t, err)
}

func TestInspect(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	result, err := e.Inspect(ctx, runtime)
	assert.Nil(t, err)

	t.Logf("Test inspect runtime: %+v", result)
}

func TestDestory(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	err = e.Destroy(ctx, runtime)
	assert.Nil(t, err)
}

func TestRemove(t *testing.T) {
	var err error
	var ctx = context.Background()

	e, err := newEDAS()
	assert.Nil(t, err)

	err = e.Remove(ctx, runtime)
	assert.Nil(t, err)
}

func TestInsertApp(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	body, err := e.fillServiceSpec(&runtime.Services[0], &runtime)
	assert.Nil(t, err)
	t.Logf("Test fill service spec: %v", body)

	err = e.insertApp(body)
	assert.Nil(t, err)
}

func TestListRecentChangeOrderInfo(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	resp, err := e.listRecentChangeOrderInfo(fullName)
	assert.Nil(t, err)
	t.Logf("Test list recent change order info: %v", resp)

	sort.Sort(ByCreateTime(resp.ChangeOrder))
	t.Logf("Test sort result: %v", resp.ChangeOrder)
}

func TestGetApp(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	appId, err := e.getAppId(fullName)
	assert.Nil(t, err)

	t.Logf("Test get app: name = %s, id = %s\n", fullName, appId)
}

func TestQueryAppStatus(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	state, err := e.queryAppStatus(fullName)
	assert.Nil(t, err)

	t.Logf("Test query app(%s) status: %v", fullName, state)
}

func TestGetK8SServiceRecord(t *testing.T) {
	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name
	kubSvc, err := e.getK8sService(fullName)
	assert.Nil(t, err)

	t.Logf("Test get k8s service: %v", kubSvc)
}

func TestCreateIngress(t *testing.T) {
	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name

	err = e.createK8sIngress(group, &runtime.Services[0])
	assert.Nil(t, err)
}

func TestDeleteIngress(t *testing.T) {
	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name
	err = e.deleteK8sIngress(fullName)
	assert.Nil(t, err)
}

func TestDeleteApp(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	err = e.deleteApp(fullName)
	assert.Nil(t, err)
}

func TestScaleApp(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	err = e.scaleApp(fullName, 2)
	assert.Nil(t, err)
}

func TestDeployApp(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name
	image := "ningx:1.15"

	err = e.deployApp(fullName, image)
	assert.Nil(t, err)
}

func TestUpdateAppResources(t *testing.T) {
	var err error

	e, err := newEDAS()
	assert.Nil(t, err)

	group := runtime.Namespace + "-" + runtime.Name
	fullName := group + "-" + runtime.Services[0].Name

	err = e.updateAppResources(fullName, 512)
	assert.Nil(t, err)
}
