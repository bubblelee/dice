// +build !default

package chronos

import (
	"context"
	"testing"
	"time"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/stretchr/testify/assert"
)

var chronos Chronos

var obj spec.Job = spec.Job{
	JobFromUser: spec.JobFromUser{
		Namespace: "default",
		Name:      "job-test0",
		Cmd:       "sleep 10",
		CPU:       0.1,
		Memory:    256,
		Image:     "ubuntu",
		Env:       map[string]string{"testkey": "testvalue"},
		Binds: []spec.Bind{
			{
				ContainerPath: "/data",
				HostPath:      "/netdata/job-test/chronos-data1",
			},
		},
		Labels: map[string]string{"MATCH_TAGS": "job,pack", "EXCLUDE_TAGS": "locked,platform"},
	},
}

func initChronos(t *testing.T) {
	var err error
	chronos = Chronos{
		name:    "chronos",
		addr:    "chronos.test.terminus.io",
		network: "USER",
		client:  httpclient.New().BasicAuth("admin", "Terminus1234"),
	}
	assert.Nil(t, err)
}

func TestChronosCreate(t *testing.T) {
	initChronos(t)

	ctx := context.Background()
	_, err := chronos.Create(ctx, obj)
	assert.Nil(t, err)
}

func TestChronosStatus(t *testing.T) {
	initChronos(t)
	ctx := context.Background()

	for i := 1; i < 20; i++ {
		result, err := chronos.Status(ctx, obj)
		if err == nil &&
			(result.Status == spec.StatusStoppedOnOK || result.Status == spec.StatusStoppedOnFailed || result.Status == spec.StatusStoppedByKilled) {
			t.Logf("successfully get job status: %s", result.Status)
			return
		}
		time.Sleep(1 * time.Second)
	}
	t.Error("failed to get job status")
}

func TestChronosDeleteAndStatus(t *testing.T) {
	ctx := context.Background()
	err := chronos.Destroy(ctx, obj)
	TestChronosStatus(t)
	assert.Nil(t, err)
}

func TestChronosRemove(t *testing.T) {
	ctx := context.Background()
	err := chronos.Remove(ctx, obj)
	assert.Nil(t, err)
}
