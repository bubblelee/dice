package executor

import (
	"os"
	"testing"

	"terminus.io/dice/dice/scheduler/executor/executortypes"

	"github.com/stretchr/testify/assert"
)

func TestManagerInitialize(t *testing.T) {
	executortypes.Register("EMPTY", func(name executortypes.Name, options map[string]string) (executortypes.Executor, error) {
		assert.Equal(t, "VALUE1", options["KEY1"])
		assert.Equal(t, "VALUE2", options["KEY2"])
		return &Empty{}, nil
	})

	os.Setenv("EXECUTOR_EMPTY_DICE1_KEY1", "VALUE1")
	os.Setenv("EXECUTOR_EMPTY_DICE1_KEY2", "VALUE2")

	mgr := GetManager()

	defer func() {
		os.Unsetenv("EXECUTOR_EMPTY_DICE1_KEY1")
		os.Unsetenv("EXECUTOR_EMPTY_DICE1_KEY2")
	}()

	err := mgr.Initialize()
	assert.Nil(t, err)

	assert.Equal(t, 5, len(mgr.factory)) // chronos, demo, k8s, marathon, metronome
	assert.Equal(t, 1, len(mgr.executors))

	c := mgr.executors["DICE1"]
	assert.NotNil(t, c)
	kind, name := c.Kind(), c.Name()
	assert.Equal(t, executortypes.Kind(""), kind)
	assert.Equal(t, executortypes.Name(""), name)

}

func TestGet(t *testing.T) {
	name := executortypes.Name("DEMOEXEC")
	kind := executortypes.Kind("DEMO")

	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")
	os.Setenv("EXECUTOR_K8S_K8SEXEC_KEY", "VALUE")

	mgr := GetManager()

	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
		os.Unsetenv("EXECUTOR_K8S_K8SEXEC_KEY")
	}()

	err := mgr.Initialize()
	assert.Nil(t, err)

	exec, err := mgr.Get(name)
	assert.Nil(t, err)

	assert.Equal(t, kind, exec.Kind())
	assert.Equal(t, name, exec.Name())

	execs := mgr.GetByKind(kind)

	assert.Equal(t, 1, len(execs))
	assert.Equal(t, kind, execs[0].Kind())
	assert.Equal(t, name, execs[0].Name())
}

func TestGetOnError(t *testing.T) {
	name := executortypes.Name("DEMOEXEC")
	kind := executortypes.Kind("DEMO")

	os.Setenv("EXECUTOR_DEMo_DEMOExEC_KEY", "VALUE")
	os.Setenv("EXECUTOR_k8S_K8SEXEC_KEY", "VALUE")

	mgr := GetManager()

	defer func() {
		os.Unsetenv("EXECUTOR_DEMo_DEMOExEC_KEY")
		os.Unsetenv("EXECUTOR_k8S_K8SEXEC_KEY")
	}()

	err := mgr.Initialize()
	assert.Nil(t, err)

	exec, err := mgr.Get(name)
	assert.NotNil(t, err)
	assert.Nil(t, exec)

	execs := mgr.GetByKind(kind)

	assert.Equal(t, 0, len(execs))
}
