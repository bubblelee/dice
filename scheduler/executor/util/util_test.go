package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParsePreserveProjects(t *testing.T) {
	key := "PRESERVEPROJECTS"

	assert.Equal(t, map[string]struct{}{
		"1": {}, "2": {}, "3": {},
	}, ParsePreserveProjects(map[string]string{
		key: "1,2,3",
	}, key))

	assert.Equal(t, map[string]struct{}{}, ParsePreserveProjects(nil, key))
}

func TestBuildDcosConstraints(t *testing.T) {
	assert.Equal(t, [][]string{}, BuildDcosConstraints(false, nil, nil))
	assert.Equal(t, [][]string{}, BuildDcosConstraints(false, map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}, nil))
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
	}, BuildDcosConstraints(true, nil, nil))
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
	}, BuildDcosConstraints(true, map[string]string{}, nil))
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
		{"dice_tags", "UNLIKE", `.*\blocked\b.*`},
		{"dice_tags", "UNLIKE", `.*\bplatform\b.*`},
	}, BuildDcosConstraints(true, map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}, nil))
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bany\b.*`},
		{"dice_tags", "UNLIKE", `.*\block1\b.*`},
	}, BuildDcosConstraints(true, map[string]string{
		"EXCLUDE_TAGS": "lock1",
	}, nil))
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack1\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack2\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bpack3\b.*`},
		{"dice_tags", "UNLIKE", `.*\block1\b.*`},
		{"dice_tags", "UNLIKE", `.*\block2\b.*`},
		{"dice_tags", "UNLIKE", `.*\block3\b.*`},
	}, BuildDcosConstraints(true, map[string]string{
		"MATCH_TAGS":   "pack1,pack2,pack3",
		"EXCLUDE_TAGS": "lock1,lock2,lock3",
	}, nil))

	// test preserve project
	assert.Equal(t, [][]string{
		{"dice_tags", "LIKE", `.*\bproject-32\b.*`},
		{"dice_tags", "LIKE", `.*\bt1\b.*`},
		{"dice_tags", "LIKE", `.*\bt2\b.*`},
		{"dice_tags", "LIKE", `.*\bt3\b.*`},
		{"dice_tags", "UNLIKE", `.*\be1\b.*`},
		{"dice_tags", "UNLIKE", `.*\be2\b.*`},
		{"dice_tags", "UNLIKE", `.*\be3\b.*`},
	}, BuildDcosConstraints(true, map[string]string{
		"MATCH_TAGS":   "t1,t2,t3",
		"EXCLUDE_TAGS": "e1,e2,e3",
		"DICE_PROJECT": "32",
	}, map[string]struct{}{
		"32": {},
	}))

	// test not preserve project
	assert.Equal(t, [][]string{
		{"dice_tags", "UNLIKE", `.*\bproject-[^,]+\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bt1\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bt2\b.*`},
		{"dice_tags", "LIKE", `.*\bany\b.*|.*\bt3\b.*`},
		{"dice_tags", "UNLIKE", `.*\be1\b.*`},
		{"dice_tags", "UNLIKE", `.*\be2\b.*`},
		{"dice_tags", "UNLIKE", `.*\be3\b.*`},
	}, BuildDcosConstraints(true, map[string]string{
		"MATCH_TAGS":   "t1,t2,t3",
		"EXCLUDE_TAGS": "e1,e2,e3",
		"DICE_PROJECT": "32",
	}, map[string]struct{}{}))
}

func TestCombineLabels(t *testing.T) {
	assert.Equal(t, map[string]string{}, CombineLabels(nil, nil))

	assert.Equal(t, map[string]string{
		"A": "v1",
		"B": "v4",
		"C": "v3",
	}, CombineLabels(map[string]string{
		"A": "v1",
		"B": "v2",
	}, map[string]string{
		"C": "v3",
		"B": "v4",
	}))
}
