package executor

import (
	_ "terminus.io/dice/dice/scheduler/executor/plugins/chronos"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/demo"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/edas"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/k8s"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/k8sjob"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/marathon"
	_ "terminus.io/dice/dice/scheduler/executor/plugins/metronome"
)
