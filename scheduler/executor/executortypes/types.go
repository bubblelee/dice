package executortypes

import (
	"context"
	"regexp"
	"sync"

	"terminus.io/dice/dice/pkg/jsonstore/storetypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const kindNameFormat = `^[A-Z0-9]+$`

var formater *regexp.Regexp = regexp.MustCompile(kindNameFormat)

// Name represents a executor's name.
type Name string

func (s Name) String() string {
	return string(s)
}

func (s Name) Validate() bool {
	return formater.MatchString(string(s))
}

// Kind represents a executor's type.
type Kind string

func (s Kind) String() string {
	return string(s)
}

func (s Kind) Validate() bool {
	return formater.MatchString(string(s))
}

// Create be used to create a executor instance.
type CreateFn func(Name, map[string]string) (Executor, error)

// return executor's event channel according to executor's name
type GetEventChanFn func(Name) (chan *spec.StatusEvent, *sync.Map, error)

type EventCbFn func(k string, v interface{}, t storetypes.ChangeType) error

// Executor defines the all interfaces that must be implemented by a executor instance.
type Executor interface {
	Kind() Kind
	Name() Name

	Create(ctx context.Context, spec interface{}) (interface{}, error)
	Destroy(ctx context.Context, spec interface{}) error
	Status(ctx context.Context, spec interface{}) (spec.StatusDesc, error)
	Remove(ctx context.Context, spec interface{}) error
	Update(ctx context.Context, spec interface{}) (interface{}, error)
	// TODO: will it be deprecated ?
	Inspect(ctx context.Context, spec interface{}) (interface{}, error)

	// TODO
}

var Factory = map[Kind]CreateFn{}
var EvFuncMap = map[Name]GetEventChanFn{}
var EvCbMap = map[Name]EventCbFn{}

// Register add a executor's create function.
func Register(kind Kind, create CreateFn) error {
	if !kind.Validate() {
		return errors.Errorf("invalid kind: %s", kind)
	}
	if _, ok := Factory[kind]; ok {
		return errors.Errorf("duplicate to register executor: %s", kind)
	}
	Factory[kind] = create
	return nil
}

// Get a GetEventChanFn according to an executor's name
func RegisterEvChan(name Name, get GetEventChanFn, cb EventCbFn) error {
	logrus.Debugf("in RegisterEvChan going to register executor: %s", name)
	if _, ok := EvFuncMap[name]; ok {
		return errors.Errorf("duplicate to register executor's event channel: %s", name)
	}
	EvFuncMap[name] = get
	EvCbMap[name] = cb
	return nil
}
