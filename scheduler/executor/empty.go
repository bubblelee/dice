package executor

import (
	"context"

	"terminus.io/dice/dice/scheduler/executor/executortypes"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/sirupsen/logrus"
)

type Empty struct {
}

func (e *Empty) Kind() executortypes.Kind {
	logrus.Infof("Kind on empty")
	return ""
}

func (e *Empty) Name() executortypes.Name {
	logrus.Infof("Name on empty")
	return ""
}

func (e *Empty) Create(ctx context.Context, specObj interface{}) (interface{}, error) {
	logrus.Infof("Create: %v", specObj)
	return nil, nil
}

func (e *Empty) Destroy(ctx context.Context, spec interface{}) error {
	logrus.Infof("Destroy on empty")
	return nil
}

func (e *Empty) Status(ctx context.Context, specObj interface{}) (spec.StatusDesc, error) {
	logrus.Infof("Status on empty")
	return spec.StatusDesc{}, nil
}

func (e *Empty) Remove(ctx context.Context, spec interface{}) error {
	logrus.Infof("Remove on empty")
	return nil
}

func (e *Empty) Update(ctx context.Context, specObj interface{}) (interface{}, error) {
	logrus.Infof("Update: %v", specObj)
	return nil, nil
}

func (e *Empty) Inspect(ctx context.Context, spec interface{}) (interface{}, error) {
	logrus.Infof("Inspect on empty")
	return nil, nil
}
