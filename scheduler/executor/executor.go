package executor

import (
	"terminus.io/dice/dice/pkg/goroutinepool"
	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/events"
	"terminus.io/dice/dice/scheduler/executor/executortypes"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const defaultGoPool = "defaultpool"

var mgr Manager

func GetManager() *Manager {
	return &mgr
}

// Manager is a executor manager, it holds the all executor instances.
type Manager struct {
	factory   map[executortypes.Kind]executortypes.CreateFn
	executors map[executortypes.Name]executortypes.Executor
	pools     map[executortypes.Name]*goroutinepool.GoroutinePool
	evChanMap map[executortypes.Name]executortypes.GetEventChanFn
	evCbMap   map[executortypes.Name]executortypes.EventCbFn
}

func (m *Manager) Initialize(configs chan conf.ExecutorConfig) error {
	m.pools = make(map[executortypes.Name]*goroutinepool.GoroutinePool)
	m.pools[defaultGoPool] = goroutinepool.New(conf.PoolSize)
	m.pools[defaultGoPool].Start()

	m.executors = make(map[executortypes.Name]executortypes.Executor)
	m.factory = executortypes.Factory
	m.evChanMap = executortypes.EvFuncMap
	m.evCbMap = executortypes.EvCbMap

	logrus.Infof("executor config: %v", configs)

	for len(configs) > 0 {
		config := <-configs

		create, ok := m.factory[executortypes.Kind(config.Kind)]
		if !ok {
			continue
		}

		name := executortypes.Name(config.Name)

		executor, err := create(name, config.Options)
		if err != nil {
			return err
		}
		m.executors[name] = executor
		m.pools[name] = goroutinepool.New(conf.PoolSize)
		m.pools[name].Start()
		logrus.Infof("created executor: %s", name)

		if getEvChanFn, ok := m.evChanMap[name]; ok {
			logrus.Infof("evChanMap got executor(%s)'s chanfunc", name)

			ch, lstore, err := getEvChanFn(name)
			if err != nil {
				logrus.Errorf("getEvChanFn for executor(%s) err: %v", err)
			}

			logrus.Infof("executor(%s) found event channel, lstore addr: %p", name, lstore)

			cb, ok := m.evCbMap[name]
			if !ok {
				logrus.Errorf("executor(%s) cannot find event cb")
			}
			go events.HandleOneExecutorEvent(string(name), ch, lstore, cb)

		} else {
			logrus.Infof("executor(%s) cannot find ev channel in evChanMap, map len:%v", name, len(m.evChanMap))
		}

	}

	return nil
}

func (m *Manager) Pool(name executortypes.Name) *goroutinepool.GoroutinePool {
	p, ok := m.pools[name]
	if !ok {
		return m.pools[defaultGoPool]
	}
	return p
}

// Get returns the executor with name.
func (m *Manager) Get(name executortypes.Name) (executortypes.Executor, error) {
	c, ok := m.executors[name]
	if !ok {
		return nil, errors.Errorf("not found executor: %s", name)
	}
	return c, nil
}

// GetByKind returns the executor instances with specify kind.
func (m *Manager) GetByKind(kind executortypes.Kind) []executortypes.Executor {
	executors := make([]executortypes.Executor, 0, len(m.executors))
	for _, c := range m.executors {
		if c.Kind() == kind {
			executors = append(executors, c)
		}
	}
	return executors
}

// ListExecutors returns the all executor instances.
func (m *Manager) ListExecutors() []executortypes.Executor {
	executors := make([]executortypes.Executor, 0, len(m.executors))
	for _, c := range m.executors {
		executors = append(executors, c)
	}
	return executors
}

func (m *Manager) PrintPoolUsage() {
	for exc, pool := range m.pools {
		stat := pool.Statistics()
		total := stat[1]
		inuse := total - stat[0]
		logrus.Infof("[%s] pool: total worker num: %d, inuse worker num: %d", exc, total, inuse)
	}
}

// GetJobExecutorKindByName return executor Kind, e.g. METRONOME, according to user-defined job specification.
func GetJobExecutorKindByName(name string) string {
	e, err := GetManager().Get(executortypes.Name(name))
	if err != nil {
		logrus.Warningf("Failed to get executor by name %s, default executor %s will be used", name, conf.DefaultJobExecutor)
		return conf.DefaultJobExecutor
	}
	return string(e.Kind())
}
