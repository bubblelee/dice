package main

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	jobspec "terminus.io/dice/dice/pkg/job/spec"
	jobutils "terminus.io/dice/dice/pkg/job/utils"
	"terminus.io/dice/dice/scheduler/executor"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

func getUUID() string {
	uuidV4 := uuid.NewV4()
	uuidStr := hex.EncodeToString(uuidV4.Bytes())
	return uuidStr
}

// prepareJobForJobFlow initialize the job of jobflow
func prepareJobForJobFlow(job *spec.JobFromUser, flow *jobspec.JobFlowRequest) {
	// ID of job is created so that request for creating jobflow can be returned
	// asynchronously before job resource is created.
	// prepareJob complete common initialization for jobs
	prepareJob(job)

	// job inheritate callbackurl from that of jobflow
	if job.CallBackUrls == nil {
		job.CallBackUrls = flow.CallBackUrls
	}

	// job can be either originated from cronjobflow or jobflow, or just job itself
	// if the job has no owner label, it is not originated from cronjobflow, and
	// as a result, label owner property as jobflow
	if _, ok := job.Labels[jobspec.REFERENCE_KIND]; !ok {
		job.Labels[jobspec.REFERENCE_KIND] = string(jobspec.JobFlowOwner)
	}

}

func (s *Server) epJobFlowCreate(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	flow := jobspec.JobFlowRequest{}
	if err := json.NewDecoder(r.Body).Decode(&flow); err != nil {
		return HTTPResponse{Status: http.StatusBadRequest}, err
	}
	if len(flow.Jobs) > 10 {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.JobFlowResponse{
				Error: "too many jobs",
			},
		}, nil
	}
	if !validateJobFlowID(flow.Name) {
		return HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.JobFlowResponse{
				Error: "invalid job flow name",
			},
		}, nil
	}

	var namespace string
	jobs := make([]spec.Job, 0, len(flow.Jobs))
	for _, j := range flow.Jobs {
		namespace = j.Namespace
		if namespace == "" {
			namespace = "default"
		}
		if !validateJobNamespace(namespace) || !validateJobName(j.Name) {
			return HTTPResponse{
				Status: http.StatusBadRequest,
				Content: jobspec.JobFlowResponse{
					Error: "invalid job namespace or name",
				},
			}, nil
		}

		prepareJobForJobFlow(&j, &flow)

		job := spec.Job{
			JobFromUser: j,
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusCreated,
			},
			CreatedTime: time.Now().Unix(),
			LastModify:  time.Now().String(),
		}
		jobs = append(jobs, job)
	}

	flows, err := s.parseJobFlow(jobs)
	if err != nil {
		return nil, err
	}

	logrus.Infof("job flow: %s, %+v", flow.Name, flows)

	jobFlow := jobspec.JobFlow{
		Name:          flow.Name,
		CallBackUrls:  flow.CallBackUrls,
		Jobs:          jobs,
		Labels:        flow.Labels,
		Extra:         flow.Extra,
		ID:            getUUID(),
		LastStartTime: time.Now().Unix(),

		StatusDesc: spec.StatusDesc{
			Status: spec.StatusRunning,
		},
	}

	if err := s.store.Put(ctx, makeJobFlowKey(namespace, flow.Name), jobFlow); err != nil {
		return nil, err
	}

	// 异步去执行整个 job flow.
	go func(jobs []spec.Job, flows [][]*spec.Job, namespace string, jobFlow *jobspec.JobFlow) {
		if err := s.runJobFlow(context.Background(), jobs, flows, namespace, jobFlow); err != nil {
			logrus.Errorf("failed to run job flow: %v", err)
		}
	}(jobs, flows, namespace, &jobFlow)

	return HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.JobFlowResponse{
			JobFlow: jobFlow,
		},
	}, nil
}

func (s *Server) epJobFlowStatus(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	namespace := vars["namespace"]
	id := vars["name"]

	var jobFlow jobspec.JobFlow
	if err := s.store.Get(ctx, makeJobFlowKey(namespace, id), &jobFlow); err != nil {
		if strings.Contains(err.Error(), "not found") {
			response := HTTPResponse{
				Status: http.StatusNotFound,
			}
			return response, nil
		}
		return nil, err
	}

	jobs := jobFlow.Jobs

	var (
		created int
		running int
		failed  int
		success int
		other   int
	)

	for _, job := range jobs {
		switch job.Status {
		case spec.StatusCreated:
			created += 1
		case spec.StatusRunning:
			running += 1
		case spec.StatusStoppedOnOK:
			success += 1
		case spec.StatusError, spec.StatusStoppedByKilled, spec.StatusStoppedOnFailed:
			failed += 1
		default:
			other += 1
		}
	}

	if failed > 0 {
		jobFlow.Status = spec.StatusStoppedOnFailed
	}

	if failed == 0 && created+running > 0 {
		jobFlow.Status = spec.StatusRunning
	}

	if success == len(jobs) {
		jobFlow.Status = spec.StatusStoppedOnOK

	}
	jobFlowResponse := &jobspec.JobFlowResponse{
		JobFlow: jobFlow,
	}
	response := HTTPResponse{
		Status:  http.StatusOK,
		Content: jobFlowResponse,
	}
	return response, nil
}

func (s *Server) epJobFlowDelete(ctx context.Context, r *http.Request, vars map[string]string) (Responser, error) {
	namespace := vars["namespace"]
	name := vars["name"]

	var jobFlow jobspec.JobFlow
	if err := s.store.Remove(ctx, makeJobFlowKey(namespace, name), &jobFlow); err != nil {
		return nil, err
	}
	jobs := jobFlow.Jobs
	// delete jobs unsafely
	go s.deleteJobs(context.Background(), jobs, namespace, name)

	return HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.JobFlowResponse{
			JobFlow: jobFlow,
		},
	}, nil
}

func (s *Server) deleteJobs(ctx context.Context, jobs []spec.Job, namespace, flowName string) {
	for _, job := range jobs {
		_, err := s.sched.Send(ctx, TaskRequest{
			ExecutorKind: executor.GetJobExecutorKindByName(job.Executor),
			ExecutorName: job.Executor,
			Action:       TaskDestroy,
			Spec:         job,
		})
		if err != nil {
			logrus.Errorf("failed to delete job %s/%s of %s: %v", namespace, job.Name, flowName, err)
		}
	}
}

// jobDepends 的 key 是依赖的 job name.
type jobDepends map[string]struct{}

// parseJobFlow 解析多任务的依赖关系，返回值的第一层数组代表了执行的顺序,
// 第二层数组代表可以并发执行的任务.
func (s *Server) parseJobFlow(jobs []spec.Job) ([][]*spec.Job, error) {
	// store the pair<jobName, jobAddr> into map
	jobMap := make(map[string]*spec.Job)

	tmp := make(map[string]jobDepends, len(jobs))
	for i, job := range jobs {
		if _, ok := tmp[job.Name]; ok {
			return nil, errors.New("duplicate job name in flow")
		}
		depends := jobDepends{}
		for _, d := range job.Depends {
			depends[d] = struct{}{}
		}
		tmp[job.Name] = depends
		jobMap[job.Name] = &jobs[i]
	}

	// 检查依赖是否在 job 范围内
	for _, depends := range tmp {
		for d := range depends {
			if _, ok := tmp[d]; !ok {
				return nil, errors.Errorf("not found job: %s, it's in depends", d)
			}
		}
	}

	handleDepends := func(jobs map[string]jobDepends) ([]string, error) {
		batch := make([]string, 0, 2)

		// 找出没有依赖的 job
		for name, depends := range jobs {
			if len(depends) == 0 {
				batch = append(batch, name)
			}
		}

		// 如果找不到一个无依赖的 job，说明存在死循环.
		if len(batch) == 0 {
			return nil, errors.New("dead loop in the job flow")
		}

		// 清除这一批无依赖的 job
		for _, name := range batch {
			delete(jobs, name)
		}

		// 清理依赖
		for _, name := range batch {
			for job, depends := range jobs {
				delete(depends, name)
				jobs[job] = depends
			}
		}

		return batch, nil
	}

	flows := make([][]*spec.Job, 0, len(jobs))
	for len(tmp) != 0 {
		batch, err := handleDepends(tmp)
		if err != nil {
			return nil, err
		}

		batchJob := make([]*spec.Job, 0, len(batch))
		for _, name := range batch {
			if jobAddr, ok := jobMap[name]; ok {
				batchJob = append(batchJob, jobAddr)
			}
		}
		if len(batchJob) != 0 {
			flows = append(flows, batchJob)
		}
	}

	return flows, nil
}

func (s *Server) runJobFlow(ctx context.Context, jobs []spec.Job, flows [][]*spec.Job, namespace string, jobFlow *jobspec.JobFlow) error {
	jobFlow.Status = spec.StatusStoppedOnOK
	defer func() {
		// result of jobflow is only notified when it is not originated from cronjobflow
		if _, ok := jobFlow.Labels[jobspec.REFERENCE_KIND]; !ok {
			go jobutils.TryToNotify(*jobFlow, nil, s.notifier)
		}
	}()
	for i, batch := range flows {
		logrus.Infof("job flow: %s run batch %d %+v", jobFlow.Name, i+1, batch)

		batchFailed := false

		// 按 job 编排的优先级分批处理，需要等一批执行完了才能执行下一批.
		for _, job := range batch {
			// build job match tags & exclude tags
			job.Labels = appendJobTags(job.Labels)

			task, err := s.sched.Send(ctx, TaskRequest{
				ExecutorKind: executor.GetJobExecutorKindByName(job.Executor),
				ExecutorName: job.Executor,
				Action:       TaskCreate,
				Spec:         *job,
			})
			job.LastStartTime = time.Now().Unix()

			ownerReference := jobspec.OwnerReference{
				ReferenceKind: jobspec.JobFlowOwner,
				Name:          jobFlow.Name,
				ID:            jobFlow.ID,
				Extra:         jobFlow.Extra,
			}
			if err != nil {
				logrus.Errorf("failed to handle task: %v", err)
				batchFailed = true
				job.Status = spec.StatusError
				job.LastMessage = err.Error()
				if _, ok := jobFlow.Labels[jobspec.REFERENCE_KIND]; !ok {
					go jobutils.TryToNotify(*job, &ownerReference, s.notifier)
				}
				break
			}

			result := task.Wait(ctx)
			if err = result.Err(); err != nil {
				logrus.Errorf("failed to wait task: %v", err)
				batchFailed = true
				job.Status = spec.StatusError
				job.LastMessage = err.Error()
				// If job is originated from cronjob flow, result won't be notified here.
				if _, ok := jobFlow.Labels[jobspec.REFERENCE_KIND]; !ok {
					go jobutils.TryToNotify(*job, &ownerReference, s.notifier)
				}
				break
			}
			job.Status = spec.StatusRunning
		}
		// 更新 job flow 的状态
		jobFlow.Jobs = jobs
		if err := s.store.Put(ctx, makeJobFlowKey(namespace, jobFlow.Name), jobFlow); err != nil {
			return errors.Wrap(err, "put job flow")
		}

		if batchFailed {
			jobFlow.Status = spec.StatusStoppedOnFailed
			return nil
		}

		// 等待这一批 job 执行完成
		err, failed := s.waitJobFlowFinishOnBatch(ctx, jobs, batch, namespace, jobFlow)
		if err != nil || failed {
			jobFlow.Status = spec.StatusStoppedOnFailed
			return errors.Wrap(err, "wait job flow on batch")
		}
	}
	return nil
}

func (s *Server) waitJobFlowFinishOnBatch(ctx context.Context, jobs []spec.Job, batch []*spec.Job, namespace string, jobFlow *jobspec.JobFlow) (error, bool) {
	jobFailed := false
	for {
		var (
			update bool
			err    error
		)
		done := map[string]struct{}{}
		ownerReference := jobspec.OwnerReference{
			ReferenceKind: jobspec.JobFlowOwner,
			Name:          jobFlow.Name,
			ID:            jobFlow.ID,
			Extra:         jobFlow.Extra,
		}
		for _, job := range batch {
			if _, ok := done[job.Name]; ok {
				continue
			}
			if update, err = s.fetchJobStatus(ctx, job); err != nil {
				logrus.Errorf("failed to fetch status: %v", err)
			}
			if job.Status != spec.StatusRunning && job.Status != spec.StatusUnschedulable {
				if kind, ok := job.Labels[jobspec.REFERENCE_KIND]; kind == string(jobspec.JobFlowOwner) || !ok {
					go jobutils.TryToNotify(*job, &ownerReference, s.notifier)
				}
				done[job.Name] = struct{}{}

				if job.Status != spec.StatusStoppedOnOK {
					jobFailed = true
				}
			}
		}
		if update {
			if err := s.store.Put(ctx, makeJobFlowKey(namespace, jobFlow.Name), jobFlow); err != nil {
				logrus.Errorf("failed to update job flow status: %v", err)
			}
		}
		if len(done) == len(batch) {
			return nil, jobFailed
		}
		time.Sleep(time.Second * 2)
	}
	return nil, jobFailed
}
