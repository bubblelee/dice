package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"terminus.io/dice/dice/scheduler/conf"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/gavv/httpexpect"
	"github.com/stretchr/testify/assert"
)

func TestRuntime(t *testing.T) {
	os.Setenv("DEFAULT_RUNTIME_EXECUTOR", "DEMO")
	os.Setenv("EXECUTOR_DEMO_DEMOEXEC_KEY", "VALUE")
	defer func() {
		os.Unsetenv("EXECUTOR_DEMO_DEMOEXEC_KEY")
		os.Unsetenv("DEFAULT_RUNTIME_EXECUTOR")
	}()

	err := conf.Parse()
	assert.Nil(t, err)

	server := NewServer("")
	server.initEndpoints()

	testserver := httptest.NewServer(server.router)
	defer testserver.Close()

	e := httpexpect.New(t, testserver.URL)

	runtimes := []spec.Runtime{
		{
			Executor: "DEMOEXEC",
			Dice: spec.Dice{
				Name:      "testx1",
				Namespace: "services",
				Services: []spec.Service{
					{
						Name: "xxx",
					},
				},
			},
		},
		{
			Executor: "DEMOEXEC",
			Dice: spec.Dice{
				Name:      "testy2",
				Namespace: "default",
				Services: []spec.Service{
					{
						Name: "yyy",
					},
				},
			},
		},

		{
			Executor: "DEMOEXEC",
			Dice: spec.Dice{
				Name:      "testz3",
				Namespace: "default",
				Services: []spec.Service{
					{
						Name: "zzz",
					},
				},
			},
		},
	}
	for _, r := range runtimes {
		// create runtime
		e.POST("/v1/runtime/create").
			WithJSON(r).
			Expect().
			Status(http.StatusOK)

		r.Version = "x"

		// update runtime
		e.PUT("/v1/runtime/"+r.Namespace+"/"+r.Name+"/update").
			WithJSON(r).
			Expect().
			Status(http.StatusOK).
			JSON().
			Object().
			ValueEqual("name", r.Name)

		// get runtime info
		e.GET("/v1/runtime/" + r.Namespace + "/" + r.Name).
			Expect().
			Status(http.StatusOK)
		//JSON().
		//Object().
		//ValueEqual("name", r.Name)

		if r.Namespace == "services" {
			e.GET("/v1/runtimeinfo/" + "testx").Expect().Status(http.StatusOK)
		} else {
			e.GET("/v1/runtimeinfo/" + r.Namespace + "/testy").Expect().Status(http.StatusOK)
		}

		// restart runtime
		e.POST("/v1/runtime/" + r.Namespace + "/" + r.Name + "/restart").
			Expect().
			Status(http.StatusOK)
	}

	// "test"前缀能匹配testy2和testz3两个key
	e.GET("/v1/runtimeinfo/default/test").Expect().Status(http.StatusBadRequest)

	for _, r := range runtimes {
		// delete runtime
		e.DELETE("/v1/runtime/" + r.Namespace + "/" + r.Name + "/delete").
			Expect().
			Status(http.StatusOK)
	}
}

func TestAppendServiceTags(t *testing.T) {
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}, appendServiceTags(nil))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "",
		"EXCLUDE_TAGS": "locked,platform",
	}, appendServiceTags(map[string]string{}))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "service-stateless",
		"EXCLUDE_TAGS": "locked,platform",
		"SERVICE_TYPE": "STATELESS",
	}, appendServiceTags(map[string]string{
		"SERVICE_TYPE": "STATELESS",
	}))
	assert.Equal(t, map[string]string{
		"MATCH_TAGS":   "service-stateful",
		"EXCLUDE_TAGS": "locked,platform",
		"SERVICE_TYPE": "STATEFUL",
	}, appendServiceTags(map[string]string{
		"SERVICE_TYPE": "STATEFUL",
	}))
}
