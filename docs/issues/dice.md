# dice issues

# 集群安装
- [ ] 按照环境添加机器，部署按照环境部署
- [ ] 集群多 master 演进
- [ ] DC/OS升级
- [ ] Glusterfs 
- [ ] kubernetes 支持
- [ ] Volume 清理
- [ ] Gittar 清理
- [ ] 磁盘资源默认自动释放问题
- [ ] 单机 32 个 ip 问题
- [ ] ip routing missing

# 集群运维
- [ ] 集群详情页数据

# 应用分析
- [ ] 分析页面404
- [ ] 分析页总是出现400
- [ ] analyser 失败，提示400
# 应用打包
- [ ] 分模块打包没有按照集群进行判断
- [ ] 打包或部署失败时，错误日志不够全面
- [ ] bp功能不完善，导致分支较多，容易混乱
- [ ] 打包部署记录看不到commit相关信息
- [ ] 打包应该成功，因为oplogger被打爆，打包失败

# 应用部署
- [ ] mysql migration 报错 No consistent leadership
- [ ] dice界面配置服务环境变量失败
- [ ] metronome 回调异常
- [ ] marathon update group异常，服务一直处于deploying，update无法生效dependence
- [ ] 如果dcos日志切文件后，dice 无法查看运行服务的完整日志
- [ ] 容量调整报错，但是能自动出发部署。dice overlay 不允许resource 为空
- [ ] 部署日志点不开
- [ ] 进入中间件详情，监控报错
- [ ] 容器重启后之前的日志丢失
- [ ] 页面跳转504
- [ ] 更新snapshot包后marathon json 没有变，需要重启才能更新服务
- [ ] 第一次打包成功、部署失败后不能直接部署： no successful deployment exist

# 其他
- [ ] dice session 失效，然后自动跳转完成登陆
- [ ] 页面错误提示直接抛出异常
- [ ] 果品web console connection timeout


