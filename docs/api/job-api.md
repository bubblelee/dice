# job rest-api说明

## common
- data encode: “application/json”

## api
1. 创建job元数据

- Method：PUT
- URI：/v1/job/create
- request ：

```
{
    "name": string,
    "image": string,
    "cmd": string,
    "cpu": float64,
    "memory": float64,
    "namespace": string,
    "extra": {
        "Key1": "value1",
        "Key2": "value2",
        ...
    },
    "env": {
        "env1": "value1",
        "env2": "value2",
        ...
    },
    "labels": {
        "lable1": "value1",
        "lable2": "value2",
        ...
    },
    "binds": [
        {
            "containerPath": string,
            "hostPath": string,
            // Optional: Defaults to false (read/write)
            "readOnly": bool
        }
    ]
}
```
- response:
    - example
    
   ```
   {
       "name": string,
       "error": string
   }
   ```

    - return
        - 200: succ
        - 400: “invalid job name” or “bad input json”
        - 409: “failed to create job, the job is existed”

2. 启动job任务

- Method：POST
- URI：/v1/job/{namespace}/{name}/start
- request ：NULL
- response:
    - example
    
    ```
    {
        "name": string,
        "error": string,
        "job":
        {
          "name": string,
          "image": string,
          "cmd": string,
          "cpu": float64,
          "memory": float64,
          "namespace": string,
          "extra": {
              "Key1": "value1",
              "Key2": "value2",
              ...
          },
          "env": {
               "env1": "value1",
               "env2": "value2",
               ...
          },
          "labels": {
               "lable1": "value1",
               "lable2": "value2",
               ...
          },
          "binds": [
            {
              "containerPath": string,
              "hostPath": string,
              // Optional: Defaults to false (read/write)
              "readOnly": bool
            }
          ],
          "created_time": int64,
          // e.g. Created / Running / StoppedOnOK / StoppedOnFailed / StoppedByKilled
          "status": string,
          "last_message": string,
          "last_start_time": int64,
          "last_finish_time": int64
        }
    }
    ```

    - return
        - 200: succ
        - ！200

3. 停止job任务

- Method：POST
- URI：/v1/job/{namespace}/{name}/stop
- request ：NULL
- response:
    - example
    
    ```
    {
        "name": string,
        "error": string
    }
    ```
    
    - return
        - 200: succ ,  返回的json中的status = Stopped
        - ！200

4. 删除job元数据

- Method：DELETE
- URI：/v1/job/{namespace}/{name}/delete
- request ：NULL
- response:
    - example
    
    ```
    {
        "name": string,
        "error": string
    }
    ```
    
    - return
        - 200: succ
        - ！200

5. 获取指定job任务信息

- Method：GET
- URI：/v1/job/{namespace}/{name}
- request ：NULL
- response:
    - example
    
    ```
    {
        "name": string,
        "error": string,
        "job":
        {
          "name": string,
          "image": string,
          "cmd": string,
          "cpu": float64,
          "memory": float64,
          "namespace": string,
          "extra": {
              "Key1": "value1",
              "Key2": "value2",
              ...
          },
          "env": {
               "env1": "value1",
               "env2": "value2",
               ...
          },
          "labels": {
               "lable1": "value1",
               "lable2": "value2",
               ...
          },
          "binds": [
            {
              "containerPath": string,
              "hostPath": string,
              // Optional: Defaults to false (read/write)
              "readOnly": bool
            }
          ],
          "created_time": int64,
          // e.g. Created / Running / StoppedOnOK / StoppedOnFailed / StoppedByKilled
          "status": string,
          "last_message": string,
          "last_start_time": int64,
          "last_finish_time": int64
        }
    }
    ```
    
    - return
        - 200: succ
        - ！200

6. 串行jobs

- Method：POST
- URI：/v1/job/{namespace}/pipeline
- request ：[]string， 用于传入所有串行job的名字
- response:
    - example
    
    ```
    {
        "names":
        [
           "string1",
           "string2",
           ...
        ],
        "error": string,
        "jobs": 
        [
         {
           "name": string,
           "image": string,
           "cmd": string,
           "cpu": float64,
           "memory": float64,
           "namespace": string,
           "extra":
           [
              "Key1": "value1",
              "Key2": "value2",
              ...
           ],
           "env":
           [
               "env1": "value1",
               "env2": "value2",
               ...
           ],
           "labels":
           [
               "lable1": "value1",
               "lable2": "value2",
               ...
           ],
           "binds": [
             {
               "containerPath": string,
               "hostPath": string,
               // Optional: Defaults to false (read/write)
               "readOnly": bool
             }
           ],
           "created_time": int64,
           // e.g. Created / Running / StoppedOnOK / StoppedOnFailed / StoppedByKilled
           "status": string,
           "last_message": string,
           "last_start_time": int64,
           "last_finish_time": int64
         },
         ...
        ]

    ```
    
    - return
        - 200: succ
        - 500:  "failed to pipeline jobs"
        - 400:  "the jobs are too many, max number is 10."
        - !200

7. 并发jobs

- Method：POST
- URI：/v1/job/{namespace}/concurrent
- request ：[]string， 传入所有并行job的名字
- response:
    - example
    
    ```
    {
        "names":
        [
           "string1",
           "string2",
           ...
        ],
        "error": string,
        "jobs": 
        [
         {
           "name": string,
           "image": string,
           "cmd": string,
           "cpu": float64,
           "memory": float64,
           "namespace": string,
           "extra": 
           [
              "Key1": "value1",
              "Key2": "value2",
              ...
           ],
           "env":
           [
               "env1": "value1",
               "env2": "value2",
               ...
           ],
           "labels":
           [
               "lable1": "value1",
               "lable2": "value2",
               ...
           ],
           "binds": [
             {
               "containerPath": string,
               "hostPath": string,
               // Optional: Defaults to false (read/write)
               "readOnly": bool
             }
           ],
           "created_time": int64,
           // e.g. Created / Running / StoppedOnOK / StoppedOnFailed / StoppedByKilled
           "status": string,
           "last_message": string,
           "last_start_time": int64,
           "last_finish_time": int64
         },
           ...
        ]
    }
    ```
    
    - return
        - 200: succ
        - 500:  "failed to concurrent jobs"
        - 400:  "the jobs are too many, max number is 10."
        - !200

8. list jobs

- Method：GET
- URI：/v1/job/{namespace}
- request ：NULL
- response:
    - example
    
    ```
    {
        [
         {
           "name": string,
           "image": string,
           "cmd": string,
           "cpu": float64,
           "memory": float64,
           "namespace": string,
           "extra": 
           [
              "Key1": "value1",
              "Key2": "value2",
              ...
           ],
           "env":
           [
               "env1": "value1",
               "env2": "value2",
               ...
           ],
           "labels":
           [
               "lable1": "value1",
               "lable2": "value2",
               ...
           ],
           "binds": [
             {
               "containerPath": string,
               "hostPath": string,
               // Optional: Defaults to false (read/write)
               "readOnly": bool
             }
           ],
           "created_time": int64,
           // e.g. Created / Running / StoppedOnOK / StoppedOnFailed / StoppedByKilled
           "status": string,
           "last_message": string,
           "last_start_time": int64,
           "last_finish_time": int64
         },
         ...
        ]
    }
    ```
    
    - return
        - 200: succ
        - !200
