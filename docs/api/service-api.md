# Scheduler Services API

## Terminology

- Service:
  
  minimal scheduling unit

- Runtime:
  
  bundle of services, with dependencies each other

- Action & Version:
  
  every action make a new version of Runtime

![](terminology.png)

## Notes

- 局部操作
- 接口原子
- `dice.json`

## Runtime spec (dice.json)

We choose dice.json as our `Runtime` spec.

```json
{
    "name": "dice",
    "namespace": "default",
    "labels": {
        "l1": "v1",
        "l2": "v2"
    },
    "services": [
        {
            "name": "console",
            "cmd": "",
            "ports": [
                8080, 8081
            ],
            "scale": 1,
            "resources": {
                "cpu": 1.0,
                "mem": 128.0,
                "disk": 1024.0
            },
            // container
            "image": "java",
            // dependencies
            "depends": [
                "addons",
                "addon-platform"
            ],
            "env": {
                "key1": "value1",
                "key2": "value2"
            },
            "labels": {
                "l1": "v1",
                // standard framework affect health check, port export, env and etc.
                "appFramework": "SPRINGBOOT", // SPRINGBOOT, HERD, SPA, CUSTOM
            }
        },
        {
            "name": "addons",
            // ...
        },
        {
            "name": "addon-platform",
            // ...
        },
        {
            "name": "ui",
            "labels": {
                "appFramework": "SPA"
            }
        }
    ]
}
```

## APIs

### Common
Content-Type: application/json

### Create runtime

all-in-one create api

- method: POST
- URI: /v1/runtime/create
- request:

    see `dice.json`

- response:

    ```json
    {
        "version": "xxx"
    }
    ```

### Update runtime

all-in-one update api, update everything include 'restart required' and 'restart not required'

- method: PUT
- URI: /v1/runtime/{namespace}/{name}/update
- request:

    see `dice.json`

- response:

    ```json
    {
        "version": "xxx"
    }
    ```

### Update single service

- method: PUT
- URI: /v1/runtime/{namespace}/{name}/service/{serviceName}/update
- request:

    ```json
    {
        // part of dice.json, only service
    }
    ```

- response:

    ```json
    {
        "version": "xxx"
    }
    ```

### Restart runtime

- method: POST
- URI: /v1/runtime/{namespace}/{name}/restart
- request:

```json
{
    "services": [
        {
            "name": "console"
        },
        {
            "name": "ui"
        }
    ],
}
```

### Cancel current action by version

- method: POST
- URI: /v1/runtime/{namespace}/{name}/cancel
- request:

    ```json
    {
        "version": "xxx" 
    }
    ```

- response:

    ```json
    {
        "version": "yyy"
    }
    ```

### Delete runtime

- method: DELETE
- URI: /v1/runtime/{namespace}/{name}/delete
- request:

    `empty`

- response:

    `empty`

### Query runtime status

- method: GET
- URI: /v1/runtime/{namespace}/{name}
- request:

    `empty`

- response:

    ```json
    {
        "name": "dice",
        "namespace": "",
        "version": "xxx",
        "services": [
            {
                "name": "console",
                "status": "RUNNING",
                "tasks": [
                    {
                        "taskId": "xxx",
                        "ipAddress": "xxx",
                        // ...
                    }
                ],
                // ... other information
            }
        ]
    }
    ```

### Query runtime versions

- method: GET
- URI: /v1/service/{namespace}/{name}/versions
- request:

    `empty`

- response:

    ```json
    {
        "versions": [
            {
                "version": "xxx",
                "status": "FINISHED"
            }
        ]
    }
    ```

### [WIP] Query runtime version status

- method: GET
- URI: /v1/runtime/{namespace}/{name}/version/{version}
- request:

    `empty`

- response:

    ```json
    {
        "version": "xxx",
        "status": "FINISHED", // RUNNING
        "message": "deployed successfully"
    }
    ```

### [WIP] Rollback

- method: POST
- URI: /v1/runtime/{namespace}/{name}/rollback
- request:

    ```json
    {
        "version": "xxx"
    }
    ```

- response:

    ```json
    {
        "version": "xx" // the new version (rollback is also an action)
    }
    ```
