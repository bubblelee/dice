package monitor

import (
	"fmt"
	"os"
	"strings"

	"terminus.io/dice/dice/eventbox/api"

	"github.com/sirupsen/logrus"
)

var (
	notifier api.Notifier
	err      error
)

func init() {
	dingding := os.Getenv("DINGDING")
	if dingding == "" {
		// dingding = "https://oapi.dingtalk.com/robot/send?access_token=f3a772140f484d7da84849c903145ab79184c8def66821ce51a63fd27d70fe6e"
		return
	}
	sender := os.Getenv("MONITOR_FROM")
	if sender == "" {
		sender = "platform-monitor"
	}
	hook := &MonitorHook{
		name:   sender,
		levels: []logrus.Level{logrus.FatalLevel, logrus.ErrorLevel},
		dest: map[string]interface{}{
			// FIXME 临时方案，后面不应该在这里提供通知目标，应该走 eventbox 的 hook 机制来处理这类消息
			"DINGDING": []string{dingding},
		},
	}

	if notifier, err = api.New(hook.name, hook.dest); err != nil {
		fmt.Printf("failed to init the eventbox notifier (%v)\n", err)
		return
	}

	logrus.AddHook(hook)
}

// MonitorHook 是 logrus 日志库的一个 Hook 实现，将 Fatal 级别的日志消息发送到 eventbox 进行报警处理
type MonitorHook struct {
	name   string
	levels []logrus.Level
	dest   map[string]interface{}
}

func (h *MonitorHook) Levels() []logrus.Level {
	return h.levels
}

func (h *MonitorHook) Fire(entry *logrus.Entry) error {
	if !strings.HasPrefix(entry.Message, "[alert]") {
		return nil
	}
	if err := notifier.Send(fmt.Sprintf("%s %s", entry.Time.Format("2006-01-02 15:04:05"), entry.Message)); err != nil {
		fmt.Printf("failed to send message to eventbox (%v)\n", err)
		return err
	}
	return nil
}
