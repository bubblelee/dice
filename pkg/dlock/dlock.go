// distributed-lock base etcd
// usage:
// lock, _ := dlock.New(dlock.WithTTL(5))
// lock.Lock(ctx)
// // do something...
// lock.Unlock()
// //see also dlock_test.go

package dlock

import (
	"context"
	"errors"
	"os"
	"strings"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/clientv3/concurrency"
)

const (
	// The short keepalive timeout and interval have been chosen to aggressively
	// detect a failed etcd server without introducing much overhead.
	keepaliveTime    = 30 * time.Second
	keepaliveTimeout = 10 * time.Second

	// default ttl, time to live when current process exit without unlock (eg. panic)
	defaultTTL = 5
)

type DLock struct {
	client  *clientv3.Client
	session *concurrency.Session
	mutex   *concurrency.Mutex
	option  *option
	lockKey string
}

type option struct {
	ttl int
}
type OpOption func(*option)

func WithTTL(ttl int) OpOption {
	return func(op *option) {
		op.ttl = ttl
	}
}

func applyOptions(ops []OpOption, option *option) error {
	for _, op := range ops {
		op(option)
	}
	if option.ttl <= 0 {
		return errors.New("illegal ttl value, must greater than 0")
	}
	return nil
}

func New(lockKey string, ops ...OpOption) (*DLock, error) {
	option := &option{ttl: defaultTTL}

	if err := applyOptions(ops, option); err != nil {
		return nil, err
	}

	var endpoints []string
	env := os.Getenv("ETCD_ENDPOINTS")
	if env == "" {
		endpoints = []string{"http://127.0.0.1:2379"}
	} else {
		endpoints = strings.Split(env, ",")
	}
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:            endpoints,
		DialKeepAliveTime:    keepaliveTime,
		DialKeepAliveTimeout: keepaliveTimeout,
	})
	if err != nil {
		return nil, err
	}
	session, err := concurrency.NewSession(cli, concurrency.WithTTL(option.ttl))
	if err != nil {
		return nil, err
	}
	mutex := concurrency.NewMutex(session, lockKey)

	return &DLock{
		client:  cli,
		session: session,
		mutex:   mutex,
		lockKey: lockKey,
		option:  option,
	}, nil

}

// it's cancelable
func (l *DLock) Lock(ctx context.Context) error {
	return l.mutex.Lock(ctx)
}

func (l *DLock) Unlock() error {
	return l.mutex.Unlock(context.Background())
}

// return locked key belong to this locker: <lockKey>/<lease-ID>
func (l *DLock) Key() string {
	return l.mutex.Key()
}

func (l *DLock) IsOwner() (bool, error) {
	r, err := l.client.Txn(context.Background()).If(l.mutex.IsOwner()).Commit()
	if err != nil {
		return false, err
	}
	return r.Succeeded, nil
}
