package dlock

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCancel(t *testing.T) {
	l1, err := New("dlock-testcancel", WithTTL(5))
	assert.Nil(t, err)
	l2, err := New("dlock-testcancel")
	assert.Nil(t, err)
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		time.Sleep(1 * time.Second)
		cancel()
	}()

	ch := make(chan struct{})
	ch2 := make(chan struct{})
	go func() {
		ti := time.NewTimer(2 * time.Second)
		select {
		case <-ch: // fine
			ch2 <- struct{}{}
		case <-ti.C:
			assert.Nil(t, 1)
			ch2 <- struct{}{}
		}
	}()

	l1.Lock(context.Background())
	l2.Lock(ctx)
	ch <- struct{}{}
	<-ch2
	l1.Unlock()
	l2.Unlock()
}

func TestMultiLock(t *testing.T) {
	l, err := New("dlock-multilock")
	assert.Nil(t, err)
	l.Lock(context.Background())
	l.Lock(context.Background())
	l.Lock(context.Background())
	l.Lock(context.Background())
	b, err := l.IsOwner()
	assert.Nil(t, err)
	assert.True(t, b)
	l.Unlock()
	b, err = l.IsOwner()
	assert.Nil(t, err)
	assert.False(t, b)
}
