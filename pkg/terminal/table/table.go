/* example:
1. NewTable().Header([]string{"h1", "h2"}).Data([][]string{{"d1", "d2"}, {"d3","d4"}}).Flush()
>>>
H1   H2
d1   d2
d3   d4

2. 指定输出
   NewTable(WithWriter(os.Stderr))

// TODO: 对中文的对齐有问题
*/
package table

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"text/tabwriter"
)

var (
	ErrDataHeaderLength = errors.New("len(header) != len(data[i])")
)

type Table struct {
	option tableOption
	data   [][]string
	header []string

	strData   []string
	strHeader string

	err error
}

type tableOption struct {
	w io.Writer
}
type OpOption func(*tableOption)

func WithWriter(w io.Writer) OpOption {
	return func(op *tableOption) {
		op.w = w
	}
}

func initOption(ops []OpOption) tableOption {
	opt := tableOption{}
	for _, op := range ops {
		op(&opt)
	}
	if opt.w == nil {
		opt.w = os.Stdout
	}
	return opt
}

func NewTable(ops ...OpOption) *Table {
	opt := initOption(ops)
	return &Table{option: opt}
}

func (t *Table) Data(data [][]string) *Table {
	if t.err != nil {
		return t
	}
	if t.header != nil && len(data) > 0 {
		if len(t.header) != len(data[0]) {
			t.err = ErrDataHeaderLength
			return t
		}
	}
	for i, d := range data {
		data[i] = replaceEmptyStr(d)
	}
	t.data = data
	for _, d := range data {
		converted := joinTab(d)
		t.strData = append(t.strData, converted)
	}
	return t
}

func (t *Table) Header(header []string) *Table {
	if t.err != nil {
		return t
	}
	if t.data != nil && header != nil {
		if len(header) != len(t.data[0]) {
			t.err = ErrDataHeaderLength
			return t
		}
	}
	for i, e := range header {
		header[i] = strings.Title(e)
	}
	header = replaceEmptyStr(header)
	t.header = header
	t.strHeader = joinTab(header)
	return t
}

func (t *Table) Flush() error {
	if t.err != nil {
		return t.err
	}
	w := tabwriter.NewWriter(t.option.w, 0, 0, 3, ' ', 0)

	if t.strHeader != "" {
		if _, err := fmt.Fprintln(w, t.strHeader); err != nil {
			return err
		}
	}
	for _, d := range t.strData {
		if _, err := fmt.Fprintln(w, d); err != nil {
			return err
		}
	}
	w.Flush()
	return nil
}

func joinTab(data []string) string {
	return strings.Join(data, "\t") + "\t"
}

func replaceEmptyStr(data []string) []string {
	r := []string{}
	for _, d := range data {
		if d == "" {
			r = append(r, "<nil>")
		} else {
			r = append(r, d)
		}
	}
	return r
}
