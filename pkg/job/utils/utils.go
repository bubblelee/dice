package utils

import (
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"time"

	"terminus.io/dice/dice/eventbox/api"
	notifierapi "terminus.io/dice/dice/eventbox/api"
	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/robfig/cron"
	"github.com/sirupsen/logrus"
)

// TODO 此正则待确认
const jobNameNamespaceFormat = `^[a-zA-Z0-9_\-]+$`

var jobFormater *regexp.Regexp = regexp.MustCompile(jobNameNamespaceFormat)

func ValidateJobName(name string) bool {
	return jobFormater.MatchString(name)
}

func ValidateJobNamespace(namespace string) bool {
	return jobFormater.MatchString(namespace)
}

func ValidateJobFlowID(id string) bool {
	return jobFormater.MatchString(id)
}

func MakeJobFlowKey(namespace, id string) string {
	return "/dice/jobflow/" + namespace + "/" + id
}

// conjob flow info contains all info of some cronjob flow
func MakeCronJobFlowInfoKey(namespace, id string) string {
	return "/dice/cronjobflow/info/" + namespace + "/" + id
}

// flowskey contains list of flows created by cronjob flow
func MakeCronJobFlowFlowsKey(namespace, id string) string {
	return "/dice/cronjobflow/" + "flows/" + namespace + "/" + id
}

// GetCronJobFlowInfoPrefix return path for cronjob flows of specified namespace or
// path of all cronjob flows if namespaces is passed.
// Currently only one namespace is supported.
func GetCronJobFlowInfoPrefix(namespaces ...string) string {
	basePath := "/dice/cronjobflow/info/"
	if len(namespaces) == 0 {
		return basePath
	}
	namespacedPath := fmt.Sprintf("%s%s/", basePath, namespaces[0])
	return namespacedPath
}

// jobDepends 的 key 是依赖的 job name.
type jobDepends map[string]struct{}

// parseJobFlow 解析多任务的依赖关系，返回值的第一层数组代表了执行的顺序,
// 第二层数组代表可以并发执行的任务.
func ParseJobFlow(jobs []spec.Job) ([][]*spec.Job, error) {
	tmp := make(map[string]jobDepends, len(jobs))
	for _, job := range jobs {
		if _, ok := tmp[job.Name]; ok {
			return nil, errors.New("duplicate job name in flow")
		}
		depends := jobDepends{}
		for _, d := range job.Depends {
			depends[d] = struct{}{}
		}
		tmp[job.Name] = depends
	}

	// 检查依赖是否在 job 范围内
	for _, depends := range tmp {
		for d := range depends {
			if _, ok := tmp[d]; !ok {
				return nil, errors.Errorf("not found job: %s, it's in depends", d)
			}
		}
	}

	handleDepends := func(jobs map[string]jobDepends) ([]string, error) {
		batch := make([]string, 0, 2)

		// 找出没有依赖的 job
		for name, depends := range jobs {
			if len(depends) == 0 {
				batch = append(batch, name)
			}
		}

		// 如果找不到一个无依赖的 job，说明存在死循环.
		if len(batch) == 0 {
			return nil, errors.New("dead loop in the job flow")
		}

		// 清除这一批无依赖的 job
		for _, name := range batch {
			delete(jobs, name)
		}

		// 清理依赖
		for _, name := range batch {
			for job, depends := range jobs {
				delete(depends, name)
				jobs[job] = depends
			}
		}

		return batch, nil
	}

	flows := make([][]*spec.Job, 0, len(jobs))
	for len(tmp) != 0 {
		batch, err := handleDepends(tmp)
		if err != nil {
			return nil, err
		}

		batchJob := make([]*spec.Job, 0, len(batch))
		for _, name := range batch {
			for i := range jobs {
				if name == jobs[i].Name {
					batchJob = append(batchJob, &jobs[i])
				}
			}
		}
		if len(batchJob) != 0 {
			flows = append(flows, batchJob)
		}
	}

	return flows, nil
}

// ValidateCron valid whether a cron expression is valid and period should be in least period defined.
// TODO 校验未通过，可返回error, 不用返回bool
func ValidateCron(cronExpresion string, leastPeriod time.Duration) (bool, error) {
	if scheduler, err := cron.Parse(cronExpresion); err != nil {
		return false, err
	} else {
		next := scheduler.Next(time.Now())
		nextNext := scheduler.Next(next)
		if nextNext.Sub(next) < leastPeriod {
			return false, nil
		}
		return true, nil
	}

}

func getJobNotificationContent(resource interface{}, ownerReference *jobspec.OwnerReference) *jobspec.Notification {
	switch v := resource.(type) {
	case spec.Job:
		unixTime := time.Unix(v.LastStartTime, 0)
		strTime := unixTime.Format(time.RFC3339)
		return &jobspec.Notification{
			Name:          v.Name,
			ID:            v.ID,
			Kind:          jobspec.JobNotificationKind,
			Namespace:     v.Namespace,
			Reference:     ownerReference,
			LastStartTime: strTime,
			Status:        v.Status,
			Extra:         v.Extra,
		}
	case jobspec.JobFlow:
		namespace := v.Jobs[0].Namespace
		unixTime := time.Unix(v.LastStartTime, 0)
		strTime := unixTime.Format(time.RFC3339)
		return &jobspec.Notification{
			Name:          v.Name,
			ID:            v.ID,
			Kind:          jobspec.JobFlowNotificationKind,
			Namespace:     namespace,
			Reference:     ownerReference,
			LastStartTime: strTime,
			Status:        v.Status,
			Extra:         v.Extra,
		}
	}
	return nil
}

func getStrJobNotificationContent(resource interface{}, ownerReference *jobspec.OwnerReference) (string, error) {
	notification := getJobNotificationContent(resource, ownerReference)
	if notification == nil {
		return "", fmt.Errorf("No matched type for resource %v", resource)
	}
	strNotifcation, err := json.Marshal(notification)
	if err != nil {
		return "", err
	}
	return string(strNotifcation), nil
}

// TryToNotify notifies the result of job to callbackurls through eventbox
func TryToNotify(resource interface{}, ownerReference *jobspec.OwnerReference, notifier notifierapi.Notifier) {
	notificationContent, err := getStrJobNotificationContent(resource, ownerReference)
	if err != nil {
		logrus.Errorf("Failed to notifiy the result of %v for %v", resource, err)
		return
	}
	callBackUrls := reflect.ValueOf(resource).FieldByName("CallBackUrls").Interface().([]string)
	if callBackUrls != nil {
		notfiyDst := api.WithDest(map[string]interface{}{"HTTP": callBackUrls})
		notifier.Send(notificationContent, notfiyDst)
	}
}
