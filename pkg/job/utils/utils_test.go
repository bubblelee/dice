package utils

import (
	"encoding/hex"
	"reflect"
	"testing"
	"time"

	notifierapi "terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/eventbox/types"
	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

const leastPeriod = 5 * time.Minute

type FakeNotifier struct {
	Ch chan struct{}
}

func (notifier *FakeNotifier) Send(content interface{}, options ...notifierapi.OpOperation) error {
	notifier.Ch <- struct{}{}
	return nil
}

func (notifier *FakeNotifier) SendRaw(message *types.Message) error {
	notifier.Ch <- struct{}{}
	return nil
}

func NewFakeNotifier() *FakeNotifier {
	return &FakeNotifier{
		Ch: make(chan struct{}, 10),
	}
}

func TestValidateCronOK(t *testing.T) {
	var valid bool
	var err error
	valid, err = ValidateCron("0 */5 * * *", leastPeriod)
	if err != nil || !valid {
		t.Errorf("Cron expression 0 */5 * * * shoud be valid but is not valid")
	}

	valid, err = ValidateCron("0 */1 * * *", leastPeriod)
	if valid {
		t.Errorf("Cron expression 0 */1 * * * shoud not be valid")
	}

	valid, err = ValidateCron("* * * * *", leastPeriod)
	if valid {
		t.Errorf("Cron expression * * * * * shoud not be valid")
	}

	valid, err = ValidateCron("100 * * * *", leastPeriod)
	if valid {
		t.Errorf("Cron expression 100 * * * * shoud not be valid")
	}
}

func getFakeJob() *spec.Job {
	unixTime := time.Now().Unix()
	uuidV4 := uuid.NewV4()
	jobID := hex.EncodeToString(uuidV4.Bytes())
	jobName := "test"
	namespace := "test"
	return &spec.Job{
		LastStartTime: unixTime,
		StatusDesc: spec.StatusDesc{
			Status: spec.StatusStoppedOnOK,
		},
		JobFromUser: spec.JobFromUser{
			Name:         jobName,
			ID:           jobID,
			Namespace:    namespace,
			CallBackUrls: []string{"callbackserver:80/callback"},
		},
	}
}

func Test_getJobNotificationContent(t *testing.T) {
	fakeJob := getFakeJob()
	unixTime := time.Unix(fakeJob.LastStartTime, 0)
	strTime := unixTime.Format(time.RFC3339)
	ownerReference := jobspec.OwnerReference{
		Name:          "test",
		ReferenceKind: jobspec.CronJobFlowOwner,
	}
	expectedNotfication := jobspec.Notification{
		Name:          fakeJob.Name,
		ID:            fakeJob.ID,
		Kind:          jobspec.JobNotificationKind,
		Namespace:     fakeJob.Namespace,
		LastStartTime: strTime,
		Reference:     &ownerReference,
		Status:        fakeJob.Status,
	}
	notification := getJobNotificationContent(*fakeJob, &ownerReference)

	if !reflect.DeepEqual(*notification, expectedNotfication) {
		t.Errorf("Expected notification %v, but got %v", expectedNotfication, notification)
	}
}

func TestTryToNotify(t *testing.T) {
	fakeJob := getFakeJob()
	ownerReference := jobspec.OwnerReference{
		Name:          "test",
		ReferenceKind: jobspec.CronJobFlowOwner,
	}
	notifier := NewFakeNotifier()
	TryToNotify(*fakeJob, &ownerReference, notifier)
	select {
	case <-time.After(1 * time.Second):
		t.Errorf("Did not receive notification")
	case <-notifier.Ch:
		break
	}
}

func TestGetCronJobFlowInfoPrefix(t *testing.T) {
	namespace1 := "test"
	expectedPath1 := "/dice/cronjobflow/info/test/"
	assert.Equal(t, expectedPath1, GetCronJobFlowInfoPrefix(namespace1))

	expectedPath2 := "/dice/cronjobflow/info/"
	assert.Equal(t, expectedPath2, GetCronJobFlowInfoPrefix())
}
