package spec

import (
	"terminus.io/dice/dice/scheduler/spec"
)

// CronjobFlow returns all infos needed by a cronjobflow request
// TODO 将Job里的Namespace提取至 CronjobFlow
type CronjobFlow struct {
	Name         string             `json:"name"`
	Jobs         []spec.JobFromUser `json:"jobs,omitempty"`
	Schedule     string             `json:"schedule,omitempty"`
	Depends      []string           `json:"depends,omitempty"`
	CallBackUrls []string           `json:"callbackurls,omitempty"`
	Extra        map[string]string  `json:"extra,omitempty"`
}

// CronjobFlowFull contains all info cronjobflow needs
type CronjobFlowFull struct {
	CronjobFlow
	ID                  string   `json:"id"`
	LastScheduledTime   string   `json:"lastscheduledtime,omitempty"` // TODO 最好调整为LatestScheduledTime
	CreatedTime         string   `json:"createdtime,omitempty"`
	LastCreatedJobFlows []string `json:"lastcreatedjobflows,omitempty"` // TODO 最好调整为LatestCreatedJobFlows
	spec.StatusDesc              // value: Stopped/Running
}

// CronjobFlowResponse is responsible for http response of cronjobflow.
type CronjobFlowResponse struct {
	CronjobFlowFull
	Error string `json:"error"`
	spec.StatusDesc
}
