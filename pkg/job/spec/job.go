package spec

import (
	"terminus.io/dice/dice/scheduler/spec"
)

// JobFlowRequest 定义批量 Job 的流程编排请求.
type JobFlowRequest struct {
	Name         string             `json:"name"`
	CallBackUrls []string           `json:"callbackurls,omitempty"`
	Jobs         []spec.JobFromUser `json:"jobs,omitempty"`
	Labels       map[string]string  `json:"labels,omitempty"`
	Extra        map[string]string  `json:"extra,omitempty"`
}

type JobFlowResponse struct {
	JobFlow
	Error string `json:"error"`
}

type JobFlow struct {
	Name          string            `json:"name"`
	CallBackUrls  []string          `json:"callbackurls,omitempty"`
	Jobs          []spec.Job        `json:"jobs,omitempty"`
	Labels        map[string]string `json:"labels,omitempty"`
	Extra         map[string]string `json:"extra,omitempty"`
	ID            string            `json:"id"`
	LastStartTime int64             `json:"last_start_time"`
	spec.StatusDesc
}

// REFERENCE_KIND is string constant to identify reference key
const REFERENCE_KIND = "ReferenceKind"

// ReferenceKind defines what kind of owner can job/jobflow belong to
type ReferenceKind string

const (
	// CronJobFlowOwner means the owner is cronjobflow
	CronJobFlowOwner ReferenceKind = "CronJobFlow"

	// JobFlowOwner means the owner is jobflow
	JobFlowOwner ReferenceKind = "JobFlow"
)

// NotificationKind represents the result of which resource is notified
type NotificationKind string

const (
	// JobNotificationKind means the execution result of job is notified
	JobNotificationKind NotificationKind = "Job"

	// CronJobFlowNotificationKind means execution result of cronjob flow is notified
	CronJobFlowNotificationKind NotificationKind = "CronJobFlow"

	// JobFlowNotificationKind means execution result ofjobflow is notified
	JobFlowNotificationKind NotificationKind = "JobFlow"
)

// OwnerReference contains metadata that describes the owner
type OwnerReference struct {
	// the kind of owner
	ReferenceKind ReferenceKind `json:"referenceKind"`

	// name of owner
	Name string `json:"name"`

	// ID of owner
	ID string `json:"id"`

	// extra info of owner defined by user
	Extra map[string]string `json:"extra"`
}

// Notification contains data to describe what resource is ended up with what result
type Notification struct {
	Name          string            `json:"name"`
	ID            string            `json:"id"`
	Kind          NotificationKind  `json:"kind"`
	Namespace     string            `json:"namespace"`
	Reference     *OwnerReference   `json:"ownerReference,omitempty"`
	LastStartTime string            `json:"last_start_time"`
	Status        spec.StatusCode   `json:"status"`
	Extra         map[string]string `json:"extra"`
}
