package etcd

import (
	"context"
	"os"
	"strings"
	"time"

	"terminus.io/dice/dice/pkg/jsonstore/storetypes"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"
	"github.com/pkg/errors"
)

var (
	etcdPutTimeout time.Duration
	etcdGetTimeout time.Duration
)

// The short keepalive timeout and interval have been chosen to aggressively
// detect a failed etcd server without introducing much overhead.
var (
	keepaliveTime    = 30 * time.Second
	keepaliveTimeout = 10 * time.Second
)

const (
	// 对etcd操作的超时时间
	parentCtxTimeout = 10 * time.Second

	// 客户端第一次等待etcd请求返回的超时时间
	firstTryEtcdRequestTimeout = 5 * time.Second

	// 客户端第二次等待etcd请求返回的超时时间
	secondTryEtcdRequestTimeout = 3 * time.Second

	// etcd客户端连接池容量
	etcdClientBufferCapacity = 20

	// 当前etcd客户端连接池中连接数数量
	etcdClientBufferLen = 3

	// WatchChan 的默认 buffer size
	defaultWatchChanBufferSize = 100
)

func init() {
}

type EtcdStore struct {
	cli  *clientv3.Client
	pool chan *clientv3.Client
}

// New creates a etcd store with etcd client, be used to access the etcd cluster.
func New() (*EtcdStore, error) {
	var endpoints []string
	env := os.Getenv("ETCD_ENDPOINTS")
	if env == "" {
		endpoints = []string{"http://127.0.0.1:2379"}
	} else {
		endpoints = strings.Split(env, ",")
	}
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:            endpoints,
		DialKeepAliveTime:    keepaliveTime,
		DialKeepAliveTimeout: keepaliveTimeout,
	})
	if err != nil {
		return nil, err
	}

	pool := make(chan *clientv3.Client, etcdClientBufferCapacity)
	for i := 0; i < etcdClientBufferLen; i++ {
		c, err := clientv3.New(clientv3.Config{
			Endpoints:            endpoints,
			DialKeepAliveTime:    keepaliveTime,
			DialKeepAliveTimeout: keepaliveTimeout,
		})
		if err != nil {
			return nil, err
		}

		pool <- c
	}

	store := &EtcdStore{
		cli:  cli,
		pool: pool,
	}
	return store, nil
}

func (s *EtcdStore) getClient() *clientv3.Client {
	c := <-s.pool
	s.pool <- c
	return c
}

func (s *EtcdStore) retry(do func(cli *clientv3.Client) (interface{}, error)) (interface{}, error) {
	cli := s.getClient()

	errC1 := make(chan error, 1)
	respC1 := make(chan interface{}, 1)

	go func() {
		resp, err := do(cli)
		if err != nil {
			errC1 <- err
			return
		}
		respC1 <- resp
	}()

	select {
	case err := <-errC1:
		return nil, err
	case resp := <-respC1:
		return resp, nil
	case <-time.After(firstTryEtcdRequestTimeout):
		// 超时后，就换一个 client 实例
		cli = s.getClient()
	}

	// 超时重试第二次, 注意上面的 goroutine 可能还在运行
	errC2 := make(chan error, 1)
	respC2 := make(chan interface{}, 1)

	go func() {
		resp, err := do(cli)
		if err != nil {
			errC2 <- err
			return
		}
		respC2 <- resp
	}()

	select {
	case err := <-errC1:
		return nil, err
	case err := <-errC2:
		return nil, err
	case resp := <-respC1:
		return resp, nil
	case resp := <-respC2:
		return resp, nil
	case <-time.After(secondTryEtcdRequestTimeout):
		return nil, errors.New("time out")
	}
}

// Put writes the keyvalue pair into etcd.
func (s *EtcdStore) Put(pctx context.Context, key, value string) error {
	_, err := s.PutWithRev(pctx, key, value)
	return err
}

func (s *EtcdStore) PutWithRev(ctx context.Context, key, value string) (int64, error) {
	put := func(cli *clientv3.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.Background(), parentCtxTimeout)
		defer cancel()
		return cli.Put(ctx, key, value)
	}

	result, err := s.retry(put)
	if err != nil {
		return 0, err
	}
	resp, ok := result.(*clientv3.PutResponse)
	if !ok {
		return 0, errors.New("invalid response type")
	}
	return resp.Header.GetRevision(), nil
}

// Get returns the value of the key.
func (s *EtcdStore) Get(pctx context.Context, key string) (storetypes.KeyValue, error) {
	get := func(cli *clientv3.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.Background(), parentCtxTimeout)
		defer cancel()
		return cli.Get(ctx, key)
	}

	result, err := s.retry(get)
	if err != nil {
		return storetypes.KeyValue{}, err
	}
	resp, ok := result.(*clientv3.GetResponse)
	if !ok {
		return storetypes.KeyValue{}, errors.New("invalid response type")
	}

	if len(resp.Kvs) != 0 {
		return storetypes.KeyValue{
			Key:         resp.Kvs[0].Key,
			Value:       resp.Kvs[0].Value,
			Revision:    resp.Header.GetRevision(),
			ModRevision: resp.Kvs[0].ModRevision,
		}, nil
	}
	return storetypes.KeyValue{}, errors.Errorf("not found")
}

// PrefixGet returns the all key value with specify prefix.
func (s *EtcdStore) PrefixGet(pctx context.Context, prefix string) ([]storetypes.KeyValue, error) {
	resp, err := s.prefixGet(pctx, prefix, false)
	if err != nil {
		return nil, err
	}
	kvs := make([]storetypes.KeyValue, len(resp.Kvs))
	for i, kv := range resp.Kvs {
		kvs[i] = storetypes.KeyValue{
			Key:         kv.Key,
			Value:       kv.Value,
			Revision:    resp.Header.GetRevision(),
			ModRevision: kv.ModRevision,
		}
	}
	return kvs, nil
}

func (s *EtcdStore) PrefixGetKey(pctx context.Context, prefix string) ([]storetypes.Key, error) {
	resp, err := s.prefixGet(pctx, prefix, true)
	if err != nil {
		return nil, err
	}
	ks := make([]storetypes.Key, len(resp.Kvs))
	for i, kv := range resp.Kvs {
		ks[i] = storetypes.Key(kv.Key)
	}
	return ks, nil
}

func (s *EtcdStore) prefixGet(pctx context.Context, prefix string, keyOnly bool) (*clientv3.GetResponse, error) {
	prefixGet := func(cli *clientv3.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.Background(), parentCtxTimeout)
		defer cancel()
		options := []clientv3.OpOption{clientv3.WithPrefix()}
		if keyOnly {
			options = append(options, clientv3.WithKeysOnly())
		}

		return cli.Get(ctx, prefix, options...)
	}

	result, err := s.retry(prefixGet)
	if err != nil {
		return nil, err
	}
	resp, ok := result.(*clientv3.GetResponse)
	if !ok {
		return nil, errors.New("invalid response type")
	}
	return resp, nil
}

// Remove 删除一个 keyvalue, 同时返回被删除的 kv 对象.
func (s *EtcdStore) Remove(pctx context.Context, key string) (*storetypes.KeyValue, error) {
	remove := func(cli *clientv3.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.Background(), parentCtxTimeout)
		defer cancel()
		return cli.Delete(ctx, key, clientv3.WithPrevKV())
	}

	result, err := s.retry(remove)
	if err != nil {
		return nil, err
	}
	resp, ok := result.(*clientv3.DeleteResponse)
	if !ok {
		return nil, errors.New("invalid response type")
	}

	if len(resp.PrevKvs) == 1 {
		return &storetypes.KeyValue{
			Key:         resp.PrevKvs[0].Key,
			Value:       resp.PrevKvs[0].Value,
			Revision:    resp.Header.GetRevision(),
			ModRevision: resp.PrevKvs[0].ModRevision,
		}, nil
	}
	return nil, nil
}

func (s *EtcdStore) prefixRemove(pctx context.Context, prefix string) (*clientv3.DeleteResponse, error) {
	prefixRemove := func(cli *clientv3.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.Background(), parentCtxTimeout)
		defer cancel()
		options := []clientv3.OpOption{clientv3.WithPrefix(), clientv3.WithPrevKV()}

		return cli.Delete(ctx, prefix, options...)
	}

	result, err := s.retry(prefixRemove)
	if err != nil {
		return nil, err
	}
	resp, ok := result.(*clientv3.DeleteResponse)
	if !ok {
		return nil, errors.New("invalid response type")
	}
	return resp, nil
}

func (s *EtcdStore) PrefixRemove(pctx context.Context, prefix string) ([]storetypes.KeyValue, error) {
	resp, err := s.prefixRemove(pctx, prefix)
	if err != nil {
		return nil, err
	}

	kvs := make([]storetypes.KeyValue, len(resp.PrevKvs))
	for i, kv := range resp.PrevKvs {
		kvs[i] = storetypes.KeyValue{
			Key:   kv.Key,
			Value: kv.Value,
		}
	}

	return kvs, nil
}

func (s *EtcdStore) Watch(ctx context.Context, key string, isPrefix bool, filterDelete bool) (storetypes.WatchChan, error) {
	op := []clientv3.OpOption{}
	if isPrefix {
		op = append(op, clientv3.WithPrefix())
	}
	if filterDelete {
		op = append(op, clientv3.WithFilterDelete())
	}
	ch := s.cli.Watch(ctx, key, op...)
	watchCh := make(chan storetypes.WatchResponse, defaultWatchChanBufferSize)

	go func() {
		for r := range ch {
			if err := r.Err(); err != nil {
				watchCh <- storetypes.WatchResponse{
					[]storetypes.KeyValueWithChangeType{},
					err,
				}
				close(watchCh)
				return
			}
			kvs := []storetypes.KeyValueWithChangeType{}
			for _, e := range r.Events {
				t := storetypes.Update
				if e.Type == mvccpb.DELETE {
					t = storetypes.Del
				} else if e.Type == mvccpb.PUT && e.Kv.CreateRevision == e.Kv.ModRevision {
					t = storetypes.Add
				}
				kvs = append(kvs, storetypes.KeyValueWithChangeType{
					KeyValue: storetypes.KeyValue{
						Key:         e.Kv.Key,
						Value:       e.Kv.Value,
						Revision:    r.Header.GetRevision(),
						ModRevision: e.Kv.ModRevision,
					},
					T: t,
				})
			}
			watchCh <- storetypes.WatchResponse{kvs, nil}
		}
		close(watchCh)
	}()
	return watchCh, nil
}
