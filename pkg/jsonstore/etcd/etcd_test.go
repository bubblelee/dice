package etcd

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCancelWatch(t *testing.T) {
	s, err := New()
	assert.Nil(t, err)
	ctx, cancel := context.WithCancel(context.Background())
	ch, err := s.Watch(ctx, "/etcd_test", true, false)
	assert.Nil(t, err)
	c := make(chan struct{})
	go func() {
		for range ch {

		}
		c <- struct{}{}
	}()
	time.Sleep(1 * time.Second)
	cancel()
	<-c
}
