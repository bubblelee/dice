package storetypes

import (
	"context"
)

type ChangeType int

const (
	Add ChangeType = iota
	Del
	Update
)

type Key []byte

type KeyValue struct {
	Key         []byte
	Value       []byte
	Revision    int64
	ModRevision int64
}

type KeyValueWithChangeType struct {
	KeyValue
	T ChangeType
}

type WatchResponse struct {
	Kvs []KeyValueWithChangeType
	Err error
}

type WatchChan <-chan WatchResponse

type Store interface {
	Put(ctx context.Context, key, value string) error
	Get(ctx context.Context, key string) (KeyValue, error)
	Remove(ctx context.Context, key string) (*KeyValue, error)
	PrefixRemove(pctx context.Context, prefix string) ([]KeyValue, error)
	PrefixGet(ctx context.Context, prefix string) ([]KeyValue, error)
	PrefixGetKey(ctx context.Context, prefix string) ([]Key, error)
}

type StoreWithWatch interface {
	Store
	Watch(ctx context.Context, key string, isPrefix bool, filterDelete bool) (WatchChan, error)
}
