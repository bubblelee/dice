package jsonstore

import (
	"context"
	"encoding/json"
	"reflect"

	"terminus.io/dice/dice/pkg/jsonstore/etcd"
	"terminus.io/dice/dice/pkg/jsonstore/lru"
	"terminus.io/dice/dice/pkg/jsonstore/mem"
	"terminus.io/dice/dice/pkg/jsonstore/memetcd"
	"terminus.io/dice/dice/pkg/jsonstore/storetypes"
	"terminus.io/dice/dice/pkg/jsonstore/timeout"

	"github.com/sirupsen/logrus"
)

type BackendType = int

const (
	EtcdStore BackendType = iota
	MemStore
	MemEtcdStore
)

var defaultOption Option = Option{backend: EtcdStore, isLru: false}

type OptionOperator func(*Option)

type Option struct {
	backend BackendType

	// lru
	isLru  bool
	lruCap int

	// timeout-store
	isTimeout bool
	timeout   int

	// memetcd
	ctx      context.Context
	etcdDir  string
	callback func(k string, obj interface{}, t storetypes.ChangeType)
	cbobj    interface{} // callback's value type
}

func (op *Option) Apply(opts []OptionOperator) {
	for _, opt := range opts {
		opt(op)
	}
}

func (op *Option) GetBackend() (storetypes.Store, error) {
	var backend storetypes.Store
	var err error
	switch op.backend {
	case EtcdStore:
		backend, err = etcd.New()
		if err != nil {
			return backend, err
		}
	case MemStore:
		backend, err = mem.New()
		if err != nil {
			return backend, err
		}
	case MemEtcdStore:
		var cb func(k, v string, t storetypes.ChangeType) = nil
		if op.callback != nil {
			objTp := reflect.TypeOf(op.cbobj)
			cb = func(k, v string, t storetypes.ChangeType) {
				v_ := reflect.New(objTp).Interface()
				if t != storetypes.Del {
					if err := json.Unmarshal([]byte(v), v_); err != nil {
						logrus.Errorf("MemEtcdStore: unmarshal failed: %v: %v", err, v)
						// skip
					}
				} else {
					v_ = nil
				}
				op.callback(k, v_, t)
			}
		}
		backend, err = memetcd.New(op.ctx, op.etcdDir, cb)
		if err != nil {
			return backend, err
		}
	}
	if op.isLru {
		backend, err = lru.New(op.lruCap, backend)
	}
	if op.isTimeout {
		backend, err = timeout.New(op.timeout, backend)
	}
	return backend, err
}

func UseEtcdStore() OptionOperator {
	return func(op *Option) {
		op.backend = EtcdStore
	}
}

func UseMemStore() OptionOperator {
	return func(op *Option) {
		op.backend = MemStore
	}
}

func UseLruStore(cap int) OptionOperator {
	return func(op *Option) {
		op.isLru = true
		op.lruCap = cap
	}
}

func UseMemEtcdStore(ctx context.Context, etcdDir string, cb func(k string, v interface{}, t storetypes.ChangeType), cbobj interface{}) OptionOperator {
	return func(op *Option) {
		op.backend = MemEtcdStore
		op.ctx = ctx
		op.etcdDir = etcdDir
		op.callback = cb
		op.cbobj = cbobj
	}
}

// timeout: second
func UseTimeoutStore(timeout int) OptionOperator {
	return func(op *Option) {
		op.isTimeout = true
		op.timeout = timeout
	}
}
