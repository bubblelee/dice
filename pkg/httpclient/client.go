package httpclient

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"io"
	"net"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	// DialTimeout 建立 tcp 连接的超时时间
	DialTimeout = 15 * time.Second
	// ClientDefaultTimeout 从建立 tcp 到读完 response body 超时时间
	ClientDefaultTimeout = 30 * time.Second
)

type BasicAuth struct {
	name     string
	password string
}

type HTTPClient struct {
	option *Option
	cli    *http.Client
	proto  string
	// set basic auth if necessary
	basicAuth *BasicAuth
	// add token auth if set, e.g. dcos cluster
	tokenAuth string
}
type Option struct {
	isHTTPS       bool
	ca            *x509.CertPool
	keyPair       tls.Certificate
	debugWriter   io.Writer
	tracer        Tracer
	checkRedirect func(req *http.Request, via []*http.Request) error
}

type OpOption func(*Option)

func WithHTTPS() OpOption {
	return func(op *Option) {
		op.isHTTPS = true
	}
}

func WithHttpsCertFromJSON(certFile, keyFile, cacrt []byte) OpOption {
	pair, err := tls.X509KeyPair(certFile, keyFile)
	if err != nil {
		logrus.Fatal("LoadX509KeyPair: %v", err)
	}

	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(cacrt)

	return func(op *Option) {
		op.isHTTPS = true
		op.ca = pool
		op.keyPair = pair
	}
}
func WithDebug(w io.Writer) OpOption {
	return func(op *Option) {
		op.debugWriter = w
	}
}

func WithTracer(w io.Writer, tracer Tracer) OpOption {
	return func(op *Option) {
		op.debugWriter = w
		op.tracer = tracer
	}
}

func WithCompleteRedirect() OpOption {
	return func(op *Option) {
		op.checkRedirect = func(req *http.Request, via []*http.Request) error {
			logrus.Infof("origin: %+v", req)
			oldest := via[0] // oldest first
			req.Header = oldest.Header
			req.Method = oldest.Method
			var err error
			if oldest.GetBody != nil {
				req.Body, err = oldest.GetBody()
				if err != nil {
					return err
				}
			}
			logrus.Infof("modified: %+v", req)
			return nil
		}
	}
}

func New(ops ...OpOption) *HTTPClient {
	option := &Option{}
	for _, op := range ops {
		op(option)
	}
	tr := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout: DialTimeout,
		}).DialContext,
		MaxIdleConns: 2,
	}
	proto := "http"
	if option.isHTTPS {
		proto = "https"
		if option.ca != nil {
			tr.TLSClientConfig = &tls.Config{
				RootCAs:      option.ca,
				Certificates: []tls.Certificate{option.keyPair},
			}
		} else {
			tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
		}
	}

	return &HTTPClient{
		proto: proto,
		cli: &http.Client{
			Transport:     tr,
			Timeout:       ClientDefaultTimeout,
			CheckRedirect: option.checkRedirect,
		},
		option: option,
	}
}

func (c *HTTPClient) BasicAuth(user, password string) *HTTPClient {
	c.basicAuth = &BasicAuth{user, password}
	return c
}

func (c *HTTPClient) TokenAuth(token string) *HTTPClient {
	c.tokenAuth = token
	return c
}

func (c *HTTPClient) Get(host string) *Request {
	req := c.newRequest()
	req.method = http.MethodGet
	req.host = host
	return req
}

func (c *HTTPClient) Post(host string) *Request {
	req := c.newRequest()
	req.method = http.MethodPost
	req.host = host
	return req
}

func (c *HTTPClient) Delete(host string) *Request {
	req := c.newRequest()
	req.method = http.MethodDelete
	req.host = host
	return req
}

func (c *HTTPClient) Put(host string) *Request {
	req := c.newRequest()
	req.method = http.MethodPut
	req.host = host
	return req
}

func (c *HTTPClient) Patch(host string) *Request {
	req := c.newRequest()
	req.method = http.MethodPatch
	req.host = host
	return req
}

func (c *HTTPClient) newRequest() *Request {
	header := make(map[string]string)
	if c.basicAuth != nil {
		header["Authorization"] = "Basic " + constructBasicAuth(c.basicAuth.name, c.basicAuth.password)
	}

	if len(c.tokenAuth) > 0 {
		header["Authorization"] = "token=" + c.tokenAuth
	}

	request := &Request{
		cli:    c.cli,
		proto:  c.proto,
		params: make(map[string][]string),
		header: header,
		option: c.option,
	}
	if request.option.debugWriter != nil {
		tracer := Tracer(NewDefaultTracer(request.option.debugWriter))
		if request.option.tracer != nil {
			tracer = request.option.tracer
		}
		request.tracer = tracer
	}
	return request

}

func constructBasicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
