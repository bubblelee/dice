package httpclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
)

type Request struct {
	path     string
	header   map[string]string
	cookie   []*http.Cookie
	params   map[string][]string
	body     io.Reader
	err      error
	method   string
	host     string
	proto    string
	cli      *http.Client
	internal *http.Request

	option *Option

	tracer Tracer
}

func (r *Request) Do() *Request {
	if r.err != nil {
		return r
	}
	if r.host == "" {
		r.err = errors.New("not found host")
		return r
	}
	if r.path == "" {
		r.err = errors.New("not found path")
		return r
	}

	values := url.Values(r.params)
	if strings.HasPrefix(r.host, "https://") {
		r.proto = "https"
		r.host = strings.TrimPrefix(r.host, "https://")
	} else if strings.HasPrefix(r.host, "http://") {
		r.proto = "http"
		r.host = strings.TrimPrefix(r.host, "http://")
	}
	url := fmt.Sprintf("%s://%s%s?%s", r.proto, r.host, r.path, values.Encode())

	req, err := http.NewRequest(r.method, url, r.body)
	if err != nil {
		r.err = err
		return r
	}

	for k, v := range r.header {
		req.Header.Set(k, v)
	}
	for _, v := range r.cookie {
		req.AddCookie(v)
	}

	r.internal = req

	return r
}

func (r *Request) JSON(o interface{}) (*Response, error) {
	if r.err != nil {
		return nil, r.err
	}

	if r.tracer != nil {
		r.tracer.TraceRequest(r.internal)
	}
	resp, err := r.cli.Do(r.internal)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if r.tracer != nil {
		r.tracer.TraceResponse(resp)
	}

	// check content-type before decode body
	contentType := resp.Header.Get("Content-Type")
	if !contentTypeIsJson(contentType) {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return nil, errors.Errorf("failed to request a rest api, status code: %v, content-type: %s, raw body: %s",
			resp.StatusCode, contentType, string(body))
	}

	if err := json.NewDecoder(resp.Body).Decode(o); err != nil {
		return nil, err
	}

	return &Response{
		internal: resp,
	}, nil
}

// 适用于a) 如果成功，不关心body内容及其结构体；
//   并且b) 如果失败，需要把body内容封装进error里返回给上层定位错误
func (r *Request) Body(b *bytes.Buffer) (*Response, error) {
	if r.err != nil {
		return nil, r.err
	}
	if r.tracer != nil {
		r.tracer.TraceRequest(r.internal)
	}

	resp, err := r.cli.Do(r.internal)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if r.tracer != nil {
		r.tracer.TraceResponse(resp)
	}

	if _, err = io.Copy(b, resp.Body); err != nil {
		return nil, err
	}

	return &Response{
		internal: resp,
	}, nil
}

func (r *Request) DiscardBody() (*Response, error) {
	if r.err != nil {
		return nil, r.err
	}
	if r.tracer != nil {
		r.tracer.TraceRequest(r.internal)
	}

	resp, err := r.cli.Do(r.internal)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if r.tracer != nil {
		r.tracer.TraceResponse(resp)
	}

	io.Copy(ioutil.Discard, resp.Body)

	return &Response{
		internal: resp,
	}, nil
}

func (r *Request) Path(path string) *Request {
	r.path = path
	return r
}

func (r *Request) Param(k, v string) *Request {
	//r.params[k] = v
	if _, ok := r.params[k]; !ok {
		r.params[k] = []string{v}
	} else {
		r.params[k] = append(r.params[k], v)
	}
	return r
}

func (r *Request) JSONBody(o interface{}) *Request {
	b, err := json.Marshal(o)
	if err != nil {
		r.err = err
	}
	r.body = bytes.NewReader(b)
	return r
}

func (r *Request) RawBody(body io.Reader) *Request {
	r.body = body
	return r
}

func (r *Request) FormBody(form url.Values) *Request {
	r.body = strings.NewReader(form.Encode())
	r.Header("Content-Type", "application/x-www-form-urlencoded")
	return r
}

func (r *Request) Header(k, v string) *Request {
	r.header[k] = v
	return r
}

func (r *Request) Cookie(v *http.Cookie) *Request {
	r.cookie = append(r.cookie, v)
	return r
}

func contentTypeIsJson(contentType string) bool {
	return strings.HasPrefix(contentType, "application/json")
}
