package httpclient

import (
	"net/http"
)

type Response struct {
	internal *http.Response
}

func (r *Response) StatusCode() int {
	return r.internal.StatusCode
}

func (r *Response) IsOK() bool {
	return r.StatusCode()/100 == 2
}

func (r *Response) IsNotfound() bool {
	return r.StatusCode() == http.StatusNotFound
}
