package httpclient

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContentTypeIsJson(t *testing.T) {
	assert.True(t, contentTypeIsJson("application/json"))
	assert.True(t, contentTypeIsJson("application/json; charset=UTF-8"))
	assert.True(t, contentTypeIsJson("application/json; charset=UTF-8; qs=2"))
	assert.True(t, contentTypeIsJson("application/json; qs=2"))

	assert.False(t, contentTypeIsJson("text/plain"))
	assert.False(t, contentTypeIsJson("text/html"))
	assert.False(t, contentTypeIsJson("application/xml"))
	assert.False(t, contentTypeIsJson("json"))
	assert.False(t, contentTypeIsJson("whatever"))
	assert.False(t, contentTypeIsJson("another"))
	assert.False(t, contentTypeIsJson("qs=2; application/json"))
}

func TestProto(t *testing.T) {
	r_ := Request{
		cli:    &http.Client{},
		host:   "https://www.baidu.com",
		path:   "/",
		proto:  "http",
		method: http.MethodGet,
	}
	r := r_.Do()
	assert.Equal(t, r.internal.URL.Scheme, "https")
	assert.Equal(t, r.internal.URL.Host, "www.baidu.com")
	resp, _ := r.DiscardBody()
	assert.Equal(t, resp.StatusCode(), http.StatusOK)

	r_ = Request{
		host:   "http://dice.test.terminus.io/",
		path:   "/",
		proto:  "whatever",
		method: http.MethodGet,
	}
	r = r_.Do()
	assert.Equal(t, r.internal.URL.Scheme, "http")
	assert.Equal(t, r.internal.URL.Host, "dice.test.terminus.io")

	r_ = Request{
		cli:    &http.Client{},
		host:   "dice.test.terminus.io",
		path:   "/",
		proto:  "http",
		method: http.MethodGet,
	}
	r = r_.Do()
	assert.Equal(t, r.internal.URL.Scheme, "http")
	assert.Equal(t, r.internal.URL.Host, "dice.test.terminus.io")
	resp, _ = r.DiscardBody()
	assert.Equal(t, resp.StatusCode(), http.StatusOK)
}
