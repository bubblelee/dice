package loop

import (
	"math"
	"time"
)

type Loop struct {
	maxTimes      uint64
	declineRatio  int
	declineLimit  time.Duration
	interval      time.Duration
	lastSleepTime time.Duration
}

type Option func(*Loop)

func New(options ...Option) *Loop {
	loop := &Loop{
		interval:     time.Second,
		maxTimes:     math.MaxUint64,
		declineRatio: 1,
		declineLimit: 0,
	}

	for _, op := range options {
		op(loop)
	}

	loop.lastSleepTime = loop.interval

	return loop
}

func (l *Loop) Do(f func() error) error {
	var i uint64
	for i = 0; i < l.maxTimes; i++ {
		if err := f(); err != nil {
			return err
		}

		// sleep 上次睡眠的时间乘以衰退比例
		l.lastSleepTime = l.lastSleepTime * time.Duration(l.declineRatio)
		if l.declineLimit > 0 && l.lastSleepTime > l.declineLimit {
			l.lastSleepTime = l.declineLimit
		}
		time.Sleep(l.lastSleepTime)
	}
	return nil
}

// WithMaxTimes 设置循环的最大次数
func WithMaxTimes(n uint64) Option {
	return func(l *Loop) {
		l.maxTimes = n
	}
}

// WithDeclineRatio 设置衰退延迟的比例，默认是 1
func WithDeclineRatio(n int) Option {
	return func(l *Loop) {
		if n < 1 {
			return
		}
		l.declineRatio = n
	}
}

// WithDeclineLimit 设置衰退延迟的最大值，默认不限制最大值
func WithDeclineLimit(t time.Duration) Option {
	return func(l *Loop) {
		if t < 0 {
			return
		}
		l.declineLimit = t
	}
}

// WithInterval 设置每次循环的间隔时间
func WithInterval(t time.Duration) Option {
	return func(l *Loop) {
		if t < time.Second {
			return
		}
		l.interval = t
	}
}
