package loop

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLoopMaxTimes(t *testing.T) {
	count := 0
	l := New(WithMaxTimes(20))
	l.Do(func() error {
		count += 1
		return nil
	})
	assert.Equal(t, 20, count)
}

func TestLoopInterval(t *testing.T) {
	l := New(WithMaxTimes(10), WithInterval(time.Second*2))

	start := time.Now()
	l.Do(func() error {
		return nil
	})
	duration := time.Now().Sub(start)
	if duration < 20*time.Second {
		t.Error("interval is not good working")
	}
}
