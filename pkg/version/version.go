package version

import (
	"fmt"
	"runtime"
)

var (
	GitCommit string
	Built     string
	GoVersion string = runtime.Version()
)

func String() string {
	return fmt.Sprintf("GitCommit: %s, Built: %s, GoVersion: %s", GitCommit, Built, GoVersion)
}
