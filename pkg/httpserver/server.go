package httpserver

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

const (
	ReadTimeout       = 60
	WriteTimeout      = 60
	ReadHeaderTimeout = 60
)

// Server provides ability to run a http server and handler registered URL paths.
type Server struct {
	Router      *mux.Router
	ListenAddr  string
	BindAddress string
}

// Endpoint contains URL path and corresponding handler
type Endpoint struct {
	Path    string
	Method  string
	Handler func(context.Context, *http.Request, map[string]string) (Responser, error)
}

// NewServer returns an http server.
func NewServer(addr string) *Server {

	server := &Server{
		Router:     mux.NewRouter(),
		ListenAddr: addr,
	}

	return server
}

// ListenAndServe boot the server to lisen and accept requests.
func (s *Server) ListenAndServe() error {

	srv := &http.Server{
		Addr:              s.ListenAddr,
		Handler:           s.Router,
		ReadTimeout:       ReadTimeout * time.Second,
		WriteTimeout:      WriteTimeout * time.Second,
		ReadHeaderTimeout: ReadHeaderTimeout * time.Second,
	}
	err := srv.ListenAndServe()
	if err != nil {
		logrus.Errorf("Failed to listen and serve: %s", err)
	}
	return err
}

// RegisterEndpoint match URL path to corresponding handler
func (s *Server) RegisterEndpoint(endpoints []Endpoint) {
	for _, ep := range endpoints {
		s.Router.Path(ep.Path).Methods(ep.Method).HandlerFunc(s.internal(ep.Handler))
		logrus.Infof("Added endpoint: %s %s", ep.Method, ep.Path)
	}
}

func (s *Server) internal(handler func(context.Context, *http.Request, map[string]string) (Responser, error)) http.HandlerFunc {
	pctx := context.Background()

	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithCancel(pctx)
		defer cancel()
		defer r.Body.Close()

		start := time.Now()
		logrus.Infof("Start %s %s", r.Method, r.URL.String())
		defer func() {
			logrus.Infof("Finished handle request %s %s (took %v)", r.Method, r.URL.String(), time.Since(start))
		}()

		response, err := handler(ctx, r, mux.Vars(r))
		if err != nil {
			logrus.Errorf("Failed to handle request: %s (%v)", r.URL.String(), err)

			if response != nil {
				w.WriteHeader(response.GetStatus())
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}
			io.WriteString(w, err.Error())
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(response.GetStatus())

		if err := json.NewEncoder(w).Encode(response.GetContent()); err != nil {
			logrus.Errorf("Failed to send response: %s (%v)", r.URL.String(), err)
			return
		}
	}
}
