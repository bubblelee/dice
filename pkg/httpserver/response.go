package httpserver

// Responser is an interface for http response
type Responser interface {
	GetStatus() int
	GetContent() interface{}
}

// HTTPResponse is a struct contains status code and content body
type HTTPResponse struct {
	Status  int
	Content interface{}
}

// GetStatus returns http status code.
func (r HTTPResponse) GetStatus() int {
	return r.Status
}

// GetContent returns http content body
func (r HTTPResponse) GetContent() interface{} {
	return r.Content
}
