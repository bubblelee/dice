package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"terminus.io/dice/dice/cronjob-scheduler/cmd/app"
	"terminus.io/dice/dice/pkg/version"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.Infof(version.String())

	rand.Seed(time.Now().UTC().UnixNano())

	command := app.NewSchedulerCommand()

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: "2006-01-02 15:04:05.000000000",
	})

	if err := command.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}
