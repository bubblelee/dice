package app

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"

	"terminus.io/dice/dice/pkg/httpclient"
	jobSpec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/bouk/monkey"
)

type FakeSchedulerProxy struct {
}

func NewFakeSchedulerProxy() Proxy {
	return &FakeSchedulerProxy{}
}

func (proxy *FakeSchedulerProxy) EnsureCreateJobFlow(cronJobFlow *jobSpec.CronjobFlowFull) error {
	return nil
}
func (proxy *FakeSchedulerProxy) EnsureGetJobFlow(namespace string, jobFlowName string) (*jobSpec.JobFlowResponse, error) {
	return &jobSpec.JobFlowResponse{
		JobFlow: jobSpec.JobFlow{
			Name:          "jobflowname",
			CallBackUrls:  []string{"http://localhost:8081/flow"},
			ID:            "uuid",
			LastStartTime: 1536732600,
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusRunning,
			},
			Jobs: []spec.Job{
				{
					JobFromUser: spec.JobFromUser{
						Name:         "11",
						Image:        "nginx",
						CPU:          0.1,
						Memory:       512,
						Namespace:    "test",
						Executor:     "METRONOME",
						CallBackUrls: []string{"http://localhost:8081/job"},
						ID:           "jobuuid",
					},
				},
			},
		},
	}, nil
}
func (proxy *FakeSchedulerProxy) EnsureDeleteJobFlow(namespace string, jobFlowName string) error {
	return nil
}

func getFakeProxy() *SchedulerProxy {
	return &SchedulerProxy{
		baseURL: "127.0.0.1:8080",
		client:  httpclient.New(),
	}
}

func getCronJobFlow() jobSpec.CronjobFlowFull {
	cronJobFlow := jobSpec.CronjobFlowFull{
		LastCreatedJobFlows: []string{"test-abc-1257894000"},
		CronjobFlow: jobSpec.CronjobFlow{
			Name:     "test-abc",
			Schedule: "24 7 * * *",

			Jobs: []spec.JobFromUser{
				{
					Name:   "test-name",
					Image:  "ubuntu",
					Cmd:    "sleep 3600",
					CPU:    0.1,
					Memory: 1234,
					Binds: []spec.Bind{
						{
							ContainerPath: "/data",
							HostPath:      "/netdata/jobtest/data1",
							ReadOnly:      true,
						},
					},
					Labels: map[string]string{},
				},
				{
					Name:   "test-name1",
					Image:  "ubuntu1",
					Cmd:    "sleep 3600",
					CPU:    0.1,
					Memory: 1234,
					Labels: map[string]string{},
				},
				{
					Name:    "test-name2",
					Image:   "ubuntu2",
					Cmd:     "sleep 3600",
					CPU:     0.1,
					Memory:  1234,
					Labels:  map[string]string{},
					Depends: []string{"test-name2"},
				},
			},
		},
	}
	return cronJobFlow
}

func TestProxyCreateJobFlow(t *testing.T) {
	proxy := getFakeProxy()
	var client *http.Client

	monkey.PatchInstanceMethod(reflect.TypeOf(client), "Do", func(_ *http.Client, _ *http.Request) (*http.Response, error) {
		resp := &http.Response{
			StatusCode: http.StatusOK,
		}

		resp.Body = ioutil.NopCloser(bytes.NewReader([]byte("status: Running")))

		return resp, nil
	})
	cronJobFlow := getCronJobFlow()

	err := proxy.createJobFlow(&cronJobFlow)
	if err != nil {
		t.Errorf("Create cronjob flow should be successful, but failed, %v", err)
	}

}

func TestProxyDeleteJobFlow(t *testing.T) {
	proxy := getFakeProxy()
	var client *http.Client
	monkey.PatchInstanceMethod(reflect.TypeOf(client), "Do", func(_ *http.Client, _ *http.Request) (*http.Response, error) {
		resp := &http.Response{
			StatusCode: http.StatusOK,
		}
		rawbyes :=
			`{
                {
                    Name:   "test-name",
                    Image:  "ubuntu",
                    Cmd:    "sleep 3600",
                    CPU:    0.1,
                    Memory: 1234,
                    Binds: []spec.Bind{
                        {
                            ContainerPath: "/data",
                            HostPath:      "/netdata/jobtest/data1",
                            ReadOnly:      true,
                        },
                    },
                },
                {
                    Name:   "test-name1",
                    Image:  "ubuntu1",
                    Cmd:    "sleep 3600",
                    CPU:    0.1,
                    Memory: 1234,
                },
                {
                    Name:   "test-name2",
                    Image:  "ubuntu2",
                    Cmd:    "sleep 3600",
                    CPU:    0.1,
                    Memory: 1234,
                },
            },
            `
		resp.Body = ioutil.NopCloser(bytes.NewReader([]byte(rawbyes)))

		return resp, nil
	})
	namespace := "default"
	flowID := "test-def"
	if err := proxy.deleteJobFlow(namespace, flowID); err != nil {
		t.Errorf("Failed to Delete job flow %v", err)
	}
}

func TestProxyGetJobFlow(t *testing.T) {
	proxy := getFakeProxy()
	var client *http.Client
	monkey.PatchInstanceMethod(reflect.TypeOf(client), "Do", func(_ *http.Client, _ *http.Request) (*http.Response, error) {
		resp := &http.Response{
			StatusCode: http.StatusOK,
			Header:     make(http.Header, 0),
		}
		resp.Header.Set("Content-Type", "application/json")
		rawbytes :=
			`{
              "jobs": [
              {
                  "Name":   "test-name",
                  "Image":  "ubuntu",
                  "Cmd":    "sleep 3600",
                  "CPU":    0.1,
                  "Memory": 1234,
                  "Binds": [
                      {
                          "ContainerPath": "/data",
                          "HostPath":      "/netdata/jobtest/data1",
                          "ReadOnly":      true
                      }
                  ]
              },
              {
                  "Name":   "test-name1",
                  "Image":  "ubuntu1",
                  "Cmd":    "sleep 3600",
                  "CPU":    0.1,
                  "Memory": 1234
              },
              {
                  "Name":   "test-name2",
                  "Image":  "ubuntu2",
                  "Cmd":    "sleep 3600",
                  "CPU":    0.1,
                  "Memory": 1234
              }
          ]
          }`

		resp.Body = ioutil.NopCloser(bytes.NewReader([]byte(rawbytes)))
		return resp, nil
	})

	jobs := []spec.Job{
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name",
				Image:  "ubuntu",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
				Binds: []spec.Bind{
					{
						ContainerPath: "/data",
						HostPath:      "/netdata/jobtest/data1",
						ReadOnly:      true,
					},
				},
			},
		},
		{
			JobFromUser: spec.JobFromUser{
				Name:   "test-name1",
				Image:  "ubuntu1",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		}, {
			JobFromUser: spec.JobFromUser{
				Name:   "test-name2",
				Image:  "ubuntu2",
				Cmd:    "sleep 3600",
				CPU:    0.1,
				Memory: 1234,
			},
		}}
	namespace := "default"
	flowID := "test-def"
	resp, _ := proxy.getJobFlow(namespace, flowID)

	for _, jobOutter := range resp.Jobs {

		notMatch := true
		for _, jobInner := range jobs {

			if reflect.DeepEqual(jobInner, jobOutter) {
				notMatch = false
			}
		}
		if notMatch {
			t.Errorf("Expected from response job list %v, but got %v", jobs, resp.Jobs)
			return
		}
	}

}

func jobNameMatch(newJobName string, jobMap map[string]*spec.JobFromUser) bool {
	jobNameSlice := strings.Split(newJobName, "-")
	if len(jobNameSlice) < 2 {
		return false
	}
	oldJobName := strings.Join(jobNameSlice[0:len(jobNameSlice)-1], "-")
	if _, ok := jobMap[oldJobName]; !ok {
		return false
	}
	return true
}

func Test_modifyJobName(t *testing.T) {
	cronJobFlow := getCronJobFlow()
	jobMap := map[string]*spec.JobFromUser{}
	for _, oldJob := range cronJobFlow.Jobs {
		jobMap[oldJob.Name] = &oldJob
	}
	newJobs := modifyJobName(cronJobFlow.Jobs)

	for _, job := range newJobs {
		if !jobNameMatch(job.Name, jobMap) {
			t.Errorf("New job name %s is not corrent", job.Name)
		}

		for _, dependedJob := range job.Depends {
			if !jobNameMatch(dependedJob, jobMap) {
				t.Errorf("Depended job name %s is not corrent", job.Name)
			}
		}
	}
}
