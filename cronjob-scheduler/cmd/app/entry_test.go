package app

import (
	"sort"
	"testing"
	"time"
)

func newEntryByNextTime(next time.Time) *Entry {
	return &Entry{
		Next: next,
	}
}

func TestSort(t *testing.T) {
	layout := "2006-01-02T15:04:05.000Z"
	entries := make([]*Entry, 0, 2)
	nextTime0, _ := time.Parse(layout, "2014-11-12T11:46:26.371Z")
	nextTime1, _ := time.Parse(layout, "2014-11-12T11:45:26.371Z")
	entry1 := newEntryByNextTime(nextTime0)
	entry2 := newEntryByNextTime(nextTime1)
	entries = append(entries, entry1)
	entries = append(entries, entry2)
	sort.Sort(byTime(entries))
	if !entries[0].Next.Equal(nextTime1) || !entries[1].Next.Equal(nextTime0) {
		t.Errorf("Unexepcted sequence after sorting %v", entries)
	}

}
