package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/pkg/job/spec"
	jobspec "terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Proxy interface {
	EnsureCreateJobFlow(cronJobFlow *spec.CronjobFlowFull) error
	EnsureGetJobFlow(namespace string, jobFlowName string) (*spec.JobFlowResponse, error)
	EnsureDeleteJobFlow(namespace string, jobFlowName string) error
}

// Proxy is used to proxy request from cronjob scheduler to dice scheduler
type SchedulerProxy struct {
	baseURL         string
	client          *httpclient.HTTPClient
	requestTimeOut  time.Duration
	requestInterval time.Duration
}

func modifyJobName(jobs []jobspec.JobFromUser) []jobspec.JobFromUser {
	newJobs := []jobspec.JobFromUser{}
	jobNameSuffix := strconv.FormatInt(time.Now().Unix(), 10)
	for _, job := range jobs {
		job.Name = fmt.Sprintf("%s-%s", job.Name, jobNameSuffix)
		job.Labels[spec.REFERENCE_KIND] = string(spec.CronJobFlowOwner)
		newDependJobs := []string{}
		for _, dependedJob := range job.Depends {
			newDependJobs = append(newDependJobs, fmt.Sprintf("%s-%s", dependedJob, jobNameSuffix))
		}
		job.Depends = newDependJobs
		newJobs = append(newJobs, job)
	}
	return newJobs
}

func (s *SchedulerProxy) createJobFlow(cronJobFlow *spec.CronjobFlowFull) error {
	logrus.Debugf("Begin to create jobflow %s/%s", cronJobFlow.Jobs[0].Namespace, cronJobFlow.LastCreatedJobFlows[0])
	defer func() {
		logrus.Debugf("Finish to create jobflow %s/%s", cronJobFlow.Jobs[0].Namespace, cronJobFlow.LastCreatedJobFlows[0])
	}()

	newJobs := modifyJobName(cronJobFlow.Jobs)
	request := spec.JobFlowRequest{
		// Name of JobFlow should be originated from that of CronJobFlow
		Name: cronJobFlow.LastCreatedJobFlows[0],
		Jobs: newJobs,
		Labels: map[string]string{
			spec.REFERENCE_KIND: string(spec.CronJobFlowOwner),
		},
		CallBackUrls: cronJobFlow.CallBackUrls,
	}

	var jobflowResponse spec.JobFlowResponse
	var b bytes.Buffer
	resp, err := s.client.Post(s.baseURL).
		Path("/v1/jobs/flow/create").
		Header("Content-Type", "application/json").
		JSONBody(request).
		Do().
		Body(&b)

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(&jobflowResponse); err != nil {
		jobflowResponse.Error = ""
	}

	if err != nil {
		return errors.Errorf("Failed to Create Job flow %s error: %v", request.Name, err)
	}

	if !resp.IsOK() {
		logrus.Errorf("Failed to Create Job flow %s, statusCode=%d, %s", request.Name, resp.StatusCode(), jobflowResponse.Error)
	}
	return nil
}

func (s *SchedulerProxy) deleteJobFlow(namespace string, jobFlowName string) error {

	logrus.Debugf("Begin to delete jobflow %s/%s", namespace, jobFlowName)
	defer func() {
		logrus.Debugf("Finished to delete jobflow %s/%s", namespace, jobFlowName)
	}()
	path := fmt.Sprintf("/v1/jobs/flow/%s/%s/delete", namespace, jobFlowName)

	var jobflowResponse spec.JobFlowResponse
	var b bytes.Buffer
	resp, err := s.client.Delete(s.baseURL).
		Path(path).
		Do().
		Body(&b)

	r := bytes.NewReader(b.Bytes())
	if err := json.NewDecoder(r).Decode(&jobflowResponse); err != nil {
		jobflowResponse.Error = err.Error()
	}

	if err != nil {
		return err
	}
	if resp.StatusCode() == http.StatusNotFound {
		logrus.Infof("Jobflow %s/%s do not exist", namespace, jobFlowName)
		return nil
	}

	if !resp.IsOK() {
		logrus.Errorf("Failed to delete job flow %s/%s , statusCode=%d, %s", namespace, jobFlowName, resp.StatusCode(), jobflowResponse.Error)
	}
	return nil
}

func (s *SchedulerProxy) getJobFlow(namespace string, jobFlowName string) (*spec.JobFlowResponse, error) {
	logrus.Debugf("Begin to get jobflow %s/%s", namespace, jobFlowName)
	defer func() {
		logrus.Debugf("Finished to get jobflow %s/%s", namespace, jobFlowName)
	}()

	path := fmt.Sprintf("/v1/jobs/flow/%s/%s", namespace, jobFlowName)
	var jobflowResponse spec.JobFlowResponse
	resp, err := s.client.Get(s.baseURL).
		Path(path).
		Do().
		JSON(&jobflowResponse)

	if err != nil {
		return nil, errors.Errorf("Failed to get jobflow %s/%s for %v", namespace, jobFlowName, err)
	}

	if resp.IsNotfound() {
		return nil, fmt.Errorf("Jobflow %s/%s is not found", namespace, jobFlowName)
	}

	if !resp.IsOK() {
		logrus.Errorf("Failed to get jobflow %s/%s, statusCode=%d", namespace, jobFlowName, resp.StatusCode())
		// return nil, errors.Errorf("Failed to get jobflow %s/%s, statusCode=%d", namespace, jobFlowName, resp.StatusCode())
		return nil, nil
	}
	return &jobflowResponse, nil

}

func (s *SchedulerProxy) EnsureCreateJobFlow(cronJobFlow *spec.CronjobFlowFull) error {
	deadline := time.Now().Add(s.requestTimeOut)
	var attempt float64 = 1
	var err error
	for {
		err = s.createJobFlow(cronJobFlow)
		if err == nil {
			break
		}
		if interval := s.exponentialSleep(deadline, attempt); interval == 0 {
			logrus.Errorf("Failed to create job flow after %g attempts.", attempt)
			return err
		}
		attempt++

	}

	return nil
}

func (s *SchedulerProxy) EnsureGetJobFlow(namespace string, jobFlowName string) (*spec.JobFlowResponse, error) {
	deadline := time.Now().Add(s.requestTimeOut)
	var attempt float64 = 1
	var flowResponse *spec.JobFlowResponse
	var err error
	for {
		flowResponse, err = s.getJobFlow(namespace, jobFlowName)
		if err == nil {
			break
		}
		if strings.Contains(err.Error(), "not found") {
			return nil, err
		}

		logrus.Warningf("Failed to get job flow %s/%s, for %v", namespace, jobFlowName, err)

		if interval := s.exponentialSleep(deadline, attempt); interval == 0 {
			logrus.Errorf("Failed to get job flow %s/%s after %g attempts.", namespace, jobFlowName, attempt)
			return nil, err
		}
		attempt++

	}
	return flowResponse, nil
}

func (s *SchedulerProxy) EnsureDeleteJobFlow(namespace string, jobFlowName string) error {
	deadline := time.Now().Add(s.requestTimeOut)
	var attempt float64 = 1
	var err error
	for {
		err = s.deleteJobFlow(namespace, jobFlowName)
		if err == nil {
			break
		}
		if interval := s.exponentialSleep(deadline, attempt); interval == 0 {
			logrus.Errorf("Failed to delete job flow after %g attempts.", attempt)
			return err
		}
		attempt++
	}
	return nil
}

func (s *SchedulerProxy) exponentialSleep(deadline time.Time, attempt float64) int {
	now := time.Now()
	secondsLeft := deadline.Sub(now)
	if secondsLeft <= 0 {
		return 0
	}
	multipleTimes := rand.Intn(int(math.Pow(2, attempt-1))) + 1
	sleepInterval := time.Duration(multipleTimes) * s.requestInterval
	logrus.Debugf("Another retry will be after %v", sleepInterval)
	time.Sleep(sleepInterval)
	return multipleTimes
}

// NewProxy return a proxy with dice scheduler endpoint
func NewProxy(endpoint string) Proxy {
	client := httpclient.New()
	return &SchedulerProxy{
		baseURL:         endpoint,
		client:          client,
		requestTimeOut:  60 * time.Second,
		requestInterval: 3 * time.Second,
	}
}
