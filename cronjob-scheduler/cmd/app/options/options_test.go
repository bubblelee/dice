package options

import (
	"os"
	"testing"
)

func getNewSchedulerOption() *SchedulerOption {
	return NewSchedulerOption()
}

func TestOverwriteParamsByEnv(t *testing.T) {
	option := getNewSchedulerOption()
	os.Setenv("BIND_PORT", "8080")
	os.Setenv("BIND_ADDRESS", "192.168.0.3")
	os.Setenv("DEBUG", "true")
	os.Setenv("DICE_SCHEDULER_ENDPOINT", "dicescheduler.marathon.l4lb.thisdcos.directory:9091")
	option.OverwriteParamsByEnv()
	if option.BindPort != 8080 || option.BindAddress != "192.168.0.3" || option.Debug != true || option.DiceSchedulerEndpoint != "dicescheduler.marathon.l4lb.thisdcos.directory:9091" {
		t.Errorf("Option(s) are not overwriteen by environment")
	}
}
