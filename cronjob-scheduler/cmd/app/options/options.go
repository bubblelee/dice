package options

import (
	"os"
	"strconv"

	"github.com/spf13/pflag"
)

const (
	// BindPort is the default port that dice scheduler servers on
	BindPort = 8081
)

// SchedulerOption is an collection of parameters required by cronjonb scheduler
type SchedulerOption struct {
	BindPort              int
	BindAddress           string
	Debug                 bool
	DiceSchedulerEndpoint string
	Postpone              int
}

// NewSchedulerOption returns SchedulerOption with default values
func NewSchedulerOption() *SchedulerOption {
	return &SchedulerOption{
		BindPort:    BindPort,
		BindAddress: "0.0.0.0",
		Debug:       false,
		DiceSchedulerEndpoint: "127.0.0.1:9091",
		Postpone:              300,
	}
}

// AddFlags adds flags for a specific scheduler option to the specified FlagSet
func (s *SchedulerOption) AddFlags(fs *pflag.FlagSet) {
	fs.IntVar(&s.BindPort, "port", s.BindPort, "The port that the Cronjob Scheduler's http service runs on")

	fs.StringVar(&s.BindAddress, "bind-address", s.BindAddress, ""+
		"The IP address on which to listen for the  port. The "+
		"associated interface(s) must be reachable by the rest of the server, and by CLI/web "+
		"clients. If blank, all interfaces will be used (0.0.0.0).")

	fs.BoolVar(&s.Debug, "debug", s.Debug, "Swith on turning debug log on")

	fs.StringVar(&s.DiceSchedulerEndpoint, "dice-scheduler-endpoint", s.DiceSchedulerEndpoint, "URL that dice scheduler servers")

	fs.IntVar(&s.Postpone, "default-postpone-time", s.Postpone, "How long in seconds will one cronjob flow be scheduled when the parent flow is not finished.")

}

// OverwriteParamsByEnv overwrite parameters by environment
func (s *SchedulerOption) OverwriteParamsByEnv() error {
	var err error
	if bindPort := os.Getenv("BIND_PORT"); bindPort != "" {
		if s.BindPort, err = strconv.Atoi(bindPort); err != nil {
			return err
		}
	}
	if bindAddress := os.Getenv("BIND_ADDRESS"); bindAddress != "" {
		s.BindAddress = bindAddress
	}
	if debug := os.Getenv("DEBUG"); debug != "" {
		if s.Debug, err = strconv.ParseBool(debug); err != nil {
			return err
		}
	}

	if diceSchedulerEndpoint := os.Getenv("DICE_SCHEDULER_ENDPOINT"); diceSchedulerEndpoint != "" {
		s.DiceSchedulerEndpoint = diceSchedulerEndpoint
	}

	if postpone := os.Getenv("POSTPONE"); postpone != "" {
		if s.Postpone, err = strconv.Atoi(postpone); err != nil {
			return err
		}
	}
	return nil
}
