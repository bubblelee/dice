package app

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"
	"testing"
	"time"

	notifierapi "terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/eventbox/types"
	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/pkg/job/utils"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/bouk/monkey"
	"github.com/robfig/cron"
)

type FakeNotifier struct {
	Ch chan struct{}
}

func (notifier *FakeNotifier) Send(content interface{}, options ...notifierapi.OpOperation) error {
	notifier.Ch <- struct{}{}
	return nil
}

func (notifier *FakeNotifier) SendRaw(message *types.Message) error {
	notifier.Ch <- struct{}{}
	return nil
}

func NewFakeNotifier() *FakeNotifier {
	return &FakeNotifier{
		Ch: make(chan struct{}, 10),
	}
}

func GetFakeServer() *Server {
	store, _ := jsonstore.New()
	proxy := NewFakeSchedulerProxy()
	notifier := NewFakeNotifier()
	return &Server{
		store:    store,
		proxy:    proxy,
		notifier: notifier,
	}
}

func getServer() *Server {
	store, _ := jsonstore.New()
	return &Server{
		store:          store,
		entries:        make(map[string]*Entry),
		addChan:        make(chan map[string]*jobspec.CronjobFlowFull, 10),
		deleteChan:     make(chan string, 10),
		childrenFlow:   make(map[string][]string),
		parentsFlow:    make(map[string][]string),
		monitorWorkers: make(map[string]*MonitorWorker),
	}
}

func TestFindChildren(t *testing.T) {
	s := getServer()
	s.childrenFlow = map[string][]string{
		"/dice/cronjobflow/info/test/flow1": {"flow2", "flow3"},
		"/dice/cronjobflow/info/test/flow2": {"flow3"},
		"/dice/cronjobflow/info/test/flow4": {"flow5"},
		"/dice/cronjobflow/info/test/flow3": {"flow6"},
		"/dice/cronjobflow/info/test/flow6": {"flow7"},
	}
	if !s.findChildren("test", "flow1", "flow3") {
		t.Errorf("flow3 should be a child of flow1")
	}

	if s.findChildren("test", "flow1", "flow4") {
		t.Errorf("flow4 is not a child of flow1")
	}

	if !s.findChildren("test", "flow1", "flow7") {
		t.Errorf("flow7 is not a child of flow1")
	}

}

func TestValidateCronJobFlowDependiency(t *testing.T) {
	s := getServer()
	s.childrenFlow = map[string][]string{
		"/dice/cronjobflow/info/test/flow1": {"flow2", "flow3"},
		"/dice/cronjobflow/info/test/flow2": {"flow3"},
		"/dice/cronjobflow/info/test/flow4": {"flow5"},
		"/dice/cronjobflow/info/test/flow3": {"flow6"},
		"/dice/cronjobflow/info/test/flow6": {"flow7"},
		"/dice/cronjobflow/info/test/flow7": {},
	}
	s.entries = map[string]*Entry{
		"/dice/cronjobflow/info/test/flow1": {},
		"/dice/cronjobflow/info/test/flow2": {},
		"/dice/cronjobflow/info/test/flow3": {},
		"/dice/cronjobflow/info/test/flow4": {},
		"/dice/cronjobflow/info/test/flow5": {},
		"/dice/cronjobflow/info/test/flow6": {},
		"/dice/cronjobflow/info/test/flow7": {},
	}
	cronJobFlow := jobspec.CronjobFlow{
		Name:    "flow6",
		Depends: []string{"flow9"},
		Jobs: []spec.JobFromUser{
			{
				Namespace: "test",
				Name:      "abc",
			},
		},
	}
	if err := s.validateCronJobFlowDependiency(&cronJobFlow); err == nil {
		t.Errorf("cronjobFlow should be raise not exist error, but no error occurs")
	}
	if err := s.validateCronJobFlowDependiency(&cronJobFlow); err != nil && !strings.Contains(err.Error(), "not exist") {
		t.Errorf("cronJobFlow no exist should be raised, but %s occurs", err.Error())
	}

	cronJobFlow = jobspec.CronjobFlow{
		Name:    "flow3",
		Depends: []string{"flow7"},
		Jobs: []spec.JobFromUser{
			{
				Namespace: "test",
				Name:      "abc",
			},
		},
	}
	if err := s.validateCronJobFlowDependiency(&cronJobFlow); err == nil {
		t.Errorf("cronjobFlow should be raise loop was found, but no error occurs")
	}
	if err := s.validateCronJobFlowDependiency(&cronJobFlow); err != nil && !strings.Contains(err.Error(), "Loop was found") {
		t.Errorf("Loop was found should be raised, but %s occurs", err.Error())
	}
}

func TestCreateCronjobFlowInner(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Put", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, _ interface{}) error {
		return nil
	})
	s := getServer()
	//ctx context.Context, cronJobFlow *jobspec.CronjobFlow
	ctx := context.Background()

	_ = s.createCronjobFlowInner(ctx, &jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name:    "flow1",
			Depends: []string{},
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		},
	})

	_ = s.createCronjobFlowInner(ctx, &jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name:    "flow2",
			Depends: []string{"flow1"},
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		},
	})

	if !reflect.DeepEqual(s.parentsFlow["/dice/cronjobflow/info/test/flow2"], []string{"flow1"}) {
		t.Errorf("Parent flow is expected to be %v, but got %v", []string{"flow1"}, s.parentsFlow["test/flow2"])
	}
	if !reflect.DeepEqual(s.childrenFlow["/dice/cronjobflow/info/test/flow1"], []string{"flow2"}) {
		t.Errorf("Children flow is expected to be %v, but got %v", []string{"flow2"}, s.childrenFlow["test/flow2"])
	}

	_ = s.createCronjobFlowInner(ctx, &jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name:    "flow3",
			Depends: []string{"flow1"},
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		},
	})
	if !reflect.DeepEqual(s.parentsFlow["/dice/cronjobflow/info/test/flow3"], []string{"flow1"}) {
		t.Errorf("Parent flow is expected to be %v, but got %v", []string{"flow1"}, s.parentsFlow["test/flow3"])
	}
	if !reflect.DeepEqual(s.childrenFlow["/dice/cronjobflow/info/test/flow1"], []string{"flow2", "flow3"}) {
		t.Errorf("Children flow is expected to be %v, but got %v", []string{"flow2", "flow3"}, s.childrenFlow["test/flow2"])
	}

	// update without changing dependency will not affect parentsFlow and childrenFlow
	_ = s.createCronjobFlowInner(ctx, &jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name:    "flow3",
			Depends: []string{"flow1"},
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		},
	})

	if !reflect.DeepEqual(s.parentsFlow["/dice/cronjobflow/info/test/flow3"], []string{"flow1"}) {
		t.Errorf("Parent flow is expected to be %v, but got %v", []string{"flow1"}, s.parentsFlow["test/flow3"])
	}
	if !reflect.DeepEqual(s.childrenFlow["/dice/cronjobflow/info/test/flow1"], []string{"flow2", "flow3"}) {
		t.Errorf("Children flow is expected to be %v, but got %v", []string{"flow2", "flow3"}, s.childrenFlow["test/flow2"])
	}
}

func TestUpdateCronjobFlowNotFound(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Get", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, _ interface{}) error {
		return errors.New("not found")
	})
	s := getServer()
	ctx := context.Background()
	vars := map[string]string{
		"namespace": "test",
		"name":      "flow1",
	}
	rawbytes :=
		`{
			"Name": "flow",
			"schedule": "* */1 * * *",
			"depends" : [],
          	"jobs": [
          		{
		            "Name":   "test-name",
		            "Image":  "ubuntu",
		            "Cmd":    "sleep 3600",
		            "CPU":    0.1,
		            "Memory": 1234,
		            "Namespace": "test",
		            "Binds": [
		                {
		                    "ContainerPath": "/data",
                      		"HostPath":      "/netdata/jobtest/data1",
		                    "ReadOnly":      true
		                  }
		              ]
          		},
          		{
          			"Name":   "test-name1",
	          	  	"Image":  "ubuntu1",
	    	   	    "Cmd":    "sleep 3600",
	 	         	"CPU":    0.1,
	 	         	"Namespace": "test",
	  	       	   	"Memory": 1234
	        	},
	        	{
	            	"Name":   "test-name2",
	      	  	    "Image":  "ubuntu2",
	      	  	    "Cmd":    "sleep 3600",
	     	  	    "CPU":    0.1,
	     	  	    "Namespace": "test",
	        	    "Memory": 1234
	        	}
	        ]
	    }`
	body := ioutil.NopCloser(bytes.NewReader([]byte(rawbytes)))
	r, _ := http.NewRequest("PUT", "/test", body)
	resp, err := s.updateCronjobFlow(ctx, r, vars)
	if err != nil {
		t.Errorf("err %v", err.Error())
	}
	if resp.GetStatus() != http.StatusNotFound {
		t.Errorf("Expect 404, but %d got", resp.GetStatus())
	}
}

func TestUpdateCronjobFlow(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Get", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, _ interface{}) error {
		return nil
	})
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Put", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, _ interface{}) error {
		return nil
	})
	s := getServer()

	s.parentsFlow = map[string][]string{
		"/dice/cronjobflow/info/test/flow4": {"flow2"},
		"/dice/cronjobflow/info/test/flow2": {"flow1"},
		"/dice/cronjobflow/info/test/flow3": {"flow1"},
		"/dice/cronjobflow/info/test/flow1": {},
		"/dice/cronjobflow/info/test/flow5": {"flow4"},
	}
	s.childrenFlow = map[string][]string{
		"/dice/cronjobflow/info/test/flow1": {"flow2", "flow3"},
		"/dice/cronjobflow/info/test/flow2": {"flow4"},
		"/dice/cronjobflow/info/test/flow3": {},
		"/dice/cronjobflow/info/test/flow4": {"flow5"},
		"/dice/cronjobflow/info/test/flow5": {},
	}

	s.entries["/dice/cronjobflow/info/test/flow1"] = &Entry{}
	s.entries["/dice/cronjobflow/info/test/flow2"] = &Entry{}
	s.entries["/dice/cronjobflow/info/test/flow3"] = &Entry{}
	s.entries["/dice/cronjobflow/info/test/flow4"] = &Entry{}
	s.entries["/dice/cronjobflow/info/test/flow5"] = &Entry{}

	ctx := context.Background()
	vars := map[string]string{
		"namespace": "test",
		"name":      "flow4",
	}
	rawbytes :=
		`{
			"Name": "flow4",
			"schedule": "0 */5 * * *",
			"depends" : ["flow3"],
          	"jobs": [
          		{
		            "Name":   "test-name",
		            "Image":  "ubuntu",
		            "Cmd":    "sleep 3600",
		            "CPU":    0.1,
		            "Memory": 1234,
		            "Namespace": "test",
		            "Binds": [
		                {
		                    "ContainerPath": "/data",
                      		"HostPath":      "/netdata/jobtest/data1",
		                    "ReadOnly":      true
		                  }
		              ]
          		},
          		{
          			"Name":   "test-name1",
	          	  	"Image":  "ubuntu1",
	    	   	    "Cmd":    "sleep 3600",
	 	         	"CPU":    0.1,
	 	         	"Namespace": "test",
	  	       	   	"Memory": 1234
	        	},
	        	{
	            	"Name":   "test-name2",
	      	  	    "Image":  "ubuntu2",
	      	  	    "Cmd":    "sleep 3600",
	     	  	    "CPU":    0.1,
	     	  	    "Namespace": "test",
	        	    "Memory": 1234
	        	}
	        ]
	    }`
	body := ioutil.NopCloser(bytes.NewReader([]byte(rawbytes)))
	r, _ := http.NewRequest("PUT", "/test", body)
	if _, err := s.updateCronjobFlow(ctx, r, vars); err != nil {
		t.Errorf("Failed to update cronjob flow for %v", err)
	}

	expectedParentsFlow := map[string][]string{
		"/dice/cronjobflow/info/test/flow4": {"flow3"},
		"/dice/cronjobflow/info/test/flow2": {"flow1"},
		"/dice/cronjobflow/info/test/flow3": {"flow1"},
		"/dice/cronjobflow/info/test/flow1": {},
		"/dice/cronjobflow/info/test/flow5": {"flow4"},
	}
	expectedChildrenFlow := map[string][]string{
		"/dice/cronjobflow/info/test/flow1": {"flow2", "flow3"},
		"/dice/cronjobflow/info/test/flow2": {},
		"/dice/cronjobflow/info/test/flow3": {"flow4"},
		"/dice/cronjobflow/info/test/flow4": {"flow5"},
		"/dice/cronjobflow/info/test/flow5": {},
	}

	if !reflect.DeepEqual(s.parentsFlow, expectedParentsFlow) {
		t.Errorf("Expected parents flow %v, but got %v", s.parentsFlow, expectedParentsFlow)
	}
	if !reflect.DeepEqual(s.childrenFlow, expectedChildrenFlow) {
		t.Errorf("Expected children flow %v, but got %v", s.childrenFlow, expectedChildrenFlow)
	}

}

func TestDeleteCronjobFlowNotFound(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Get", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, _ interface{}) error {
		return errors.New("not found")
	})
	s := getServer()
	vars := map[string]string{
		"namespace": "test",
		"name":      "flow1",
	}
	r, _ := http.NewRequest("DELETE", "/test", nil)
	ctx := context.Background()
	resp, err := s.deleteCronjobFlow(ctx, r, vars)
	if err != nil {
		t.Errorf("err %v", err.Error())
	}
	if resp.GetStatus() != http.StatusOK {
		t.Errorf("Expect 200, but %d got", resp.GetStatus())
	}
}

func TestShouldPostponeLastSchedulerTimeNil(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Get", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, cronjobFlowFull interface{}) error {
		values := reflect.ValueOf(cronjobFlowFull).Elem()
		cronjobFlow := &jobspec.CronjobFlow{
			Name: "flow1",
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		}
		values.FieldByName("CronjobFlow").Set(reflect.ValueOf(cronjobFlow).Elem())
		return nil
	})
	s := getServer()
	s.childrenFlow["/dice/cronjobflow/info/test/flow1"] = []string{"flow2"}
	s.parentsFlow["/dice/cronjobflow/info/test/flow2"] = []string{"flow1"}
	dependkeypath := utils.MakeCronJobFlowInfoKey("test", "flow1")

	cronJobFlow := jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name: "flow2",
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "def",
				},
			},
			Depends: []string{"flow1"},
		},
	}

	scheduler, _ := cron.Parse("1 2 3 */1 *")
	newEntry := &Entry{
		Schedule: scheduler,
	}

	newEntry.Next = newEntry.Schedule.Next(time.Now())
	s.entries[dependkeypath] = newEntry
	if !s.shouldPostpone(&cronJobFlow, time.Now()) {
		t.Errorf("Cronjob flow should not be postponed")
	}
}

func TestShouldPostponeNotPostpone(t *testing.T) {
	var store *jsonstore.JsonStoreImpl
	monkey.PatchInstanceMethod(reflect.TypeOf(store), "Get", func(_ *jsonstore.JsonStoreImpl, _ context.Context, _ string, cronjobFlowFull interface{}) error {
		values := reflect.ValueOf(cronjobFlowFull).Elem()
		cronjobFlow := &jobspec.CronjobFlow{
			Name: "flow1",
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "abc",
				},
			},
		}
		values.FieldByName("CronjobFlow").Set(reflect.ValueOf(cronjobFlow).Elem())
		values.FieldByName("LastScheduledTime").SetString(time.Now().Format(time.RFC3339))
		values.FieldByName("LastCreatedJobFlows").Set(reflect.ValueOf([]string{"flow1-test"}))
		return nil
	})

	var proxy *SchedulerProxy
	monkey.PatchInstanceMethod(reflect.TypeOf(proxy), "EnsureGetJobFlow", func(_ *SchedulerProxy, _, _ string) (*jobspec.JobFlowResponse, error) {
		return &jobspec.JobFlowResponse{
			JobFlow: jobspec.JobFlow{
				StatusDesc: spec.StatusDesc{
					Status: spec.StatusStoppedOnOK,
				},
			},
		}, nil
	})
	s := getServer()
	s.proxy = &SchedulerProxy{}
	s.childrenFlow["/dice/cronjobflow/info/test/flow1"] = []string{"flow2"}
	s.parentsFlow["/dice/cronjobflow/info/test/flow2"] = []string{"flow1"}
	dependkeypath := utils.MakeCronJobFlowInfoKey("test", "flow1")

	cronJobFlow := jobspec.CronjobFlowFull{
		CronjobFlow: jobspec.CronjobFlow{
			Name: "flow2",
			Jobs: []spec.JobFromUser{
				{
					Namespace: "test",
					Name:      "def",
				},
			},
			Depends: []string{"flow1"},
		},
	}

	scheduler, _ := cron.Parse("1 2 3 */1 *")
	newEntry := &Entry{
		Schedule: scheduler,
	}

	newEntry.Next = newEntry.Schedule.Next(time.Now())
	s.entries[dependkeypath] = newEntry
	if s.shouldPostpone(&cronJobFlow, time.Now()) {
		t.Errorf("Cronjob flow should not be postponed")
	}
}
