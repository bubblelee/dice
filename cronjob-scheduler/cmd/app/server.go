package app

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	"terminus.io/dice/dice/cronjob-scheduler/cmd/app/options"
	notifierapi "terminus.io/dice/dice/eventbox/api"
	"terminus.io/dice/dice/pkg/httpserver"
	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/pkg/job/utils"
	"terminus.io/dice/dice/pkg/jsonstore"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/pkg/errors"
	"github.com/robfig/cron"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

const (
	// timer of cron scheduler will expires at least in shortestSchedule.
	shortestSchedule          = "0 */5 * * *"
	shortestScheduleEntryName = "shortestSchedule"
	leastPeriod               = 5 * time.Minute
	DefaultDelay              = 5 * time.Minute
)

// NewSchedulerCommand creates a *cobra.Command object with default parameters
func NewSchedulerCommand() *cobra.Command {
	opts := options.NewSchedulerOption()

	cmd := &cobra.Command{
		Use:  "cronjob-scheduler",
		Long: "cronjob-scheudler is used to schedule cronjobs and cronjobflows to appointed executor",
		Run: func(cmd *cobra.Command, args []string) {

			if opts.Debug {
				logrus.SetLevel(logrus.DebugLevel)
			}

			server, err := NewServer(opts)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to create cronjob server for: %v\n", err)
				os.Exit(1)
			}
			go server.run()
			if err := server.Server.ListenAndServe(); err != nil {
				fmt.Fprintf(os.Stderr, "%v\n", err)
				os.Exit(1)
			}
		},
	}
	opts.AddFlags(cmd.Flags())
	if err := opts.OverwriteParamsByEnv(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}

	logrus.Infof("Options got from command line and environment: %v", opts)

	return cmd
}

// Server serves as a http server and execute cron job flows.
type Server struct {
	*httpserver.Server
	endpoints           []httpserver.Endpoint
	store               jsonstore.JsonStore
	addChan             chan map[string]*jobspec.CronjobFlowFull
	deleteChan          chan string
	childrenFlow        map[string][]string // childrenFlow maps namespace/cronjobflow to those cronjobflows depend on this cronjobflow
	parentsFlow         map[string][]string // childrenFlow maps namespace/cronjobflow to those cronjobflows that this cronjobflow depends on
	postpone            map[string]bool     // postpone decides whether one cronjob flow should be postponed
	delayed             map[string]bool
	defaultPostponeTime time.Duration // defaultPostponeTime decides how long should one cronjob should be postponed
	entries             map[string]*Entry
	proxy               Proxy
	notifier            notifierapi.Notifier
	monitorWorkers      map[string]*MonitorWorker
}

func getUUID() string {
	uuidV4 := uuid.NewV4()
	uuidStr := hex.EncodeToString(uuidV4.Bytes())
	return uuidStr
}

// NewServer return an cronjob server.
func NewServer(opts *options.SchedulerOption) (*Server, error) {
	addr := fmt.Sprintf("%s:%d", opts.BindAddress, opts.BindPort)
	httpServer := httpserver.NewServer(addr)
	store, err := jsonstore.New()
	if err != nil {
		return nil, err
	}
	notifier, err := notifierapi.New("cronjob-scheduler", nil)
	if err != nil {
		return nil, err
	}

	proxy := NewProxy(opts.DiceSchedulerEndpoint)
	server := &Server{
		Server:              httpServer,
		store:               store,
		proxy:               proxy,
		entries:             make(map[string]*Entry),
		addChan:             make(chan map[string]*jobspec.CronjobFlowFull, 10),
		deleteChan:          make(chan string, 10),
		childrenFlow:        make(map[string][]string), // key: CronjobFlow Name, value: CronjobFlow's dependents
		parentsFlow:         make(map[string][]string),
		postpone:            make(map[string]bool),
		delayed:             make(map[string]bool),
		defaultPostponeTime: time.Duration(opts.Postpone) * time.Second,
		notifier:            notifier,
		monitorWorkers:      make(map[string]*MonitorWorker),
	}
	if err := server.initServer(); err != nil {
		return nil, err
	}
	server.Server.RegisterEndpoint(server.endpoints)

	return server, nil

}

func (s *Server) initServer() error {
	if err := s.initEnpoint(); err != nil {
		return err
	}
	if err := s.initflows(); err != nil {
		return err
	}
	return nil
}

func (s *Server) initEnpoint() error {
	endpoints := []httpserver.Endpoint{
		{"/info", http.MethodGet, s.epInfo},
		{"/v1/cronjobs/flow/create", http.MethodPost, s.createCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}/{name}/start", http.MethodPost, s.startCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}/{name}/update", http.MethodPut, s.updateCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}/{name}/delete", http.MethodDelete, s.deleteCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}/{name}/stop", http.MethodPost, s.stopCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}/{name}", http.MethodGet, s.getCronjobFlow},
		{"/v1/cronjobs/flow/{namespace}", http.MethodGet, s.getCronjobFlows},
	}
	s.endpoints = endpoints

	for _, endpoint := range endpoints {
		logrus.Infof("Register endpoint %s", endpoint.Path)
	}
	return nil
}

// shortest period
func (s *Server) getShortestPeriodTask() (*Entry, error) {
	schedule, err := cron.Parse(shortestSchedule)
	if err != nil {
		return nil, err
	}
	return &Entry{
		Schedule: schedule,
	}, nil
}

// initflows should run before httpserver serves.
func (s *Server) initflows() error {
	logrus.Info("--- Loading cronjob flows from store(Etcd) ---")
	defer logrus.Info("--- Loaded cronjob flows from store(Etcd) ---")

	cronJobFlows := []*jobspec.CronjobFlowFull{}

	// get all cronjob flow
	err := s.store.ForEach(context.Background(), utils.GetCronJobFlowInfoPrefix(), jobspec.CronjobFlowFull{}, func(key string, j interface{}) error {
		cronJobFlow := j.(*jobspec.CronjobFlowFull)
		namespace := cronJobFlow.Jobs[0].Namespace
		flowKey := utils.MakeCronJobFlowInfoKey(namespace, cronJobFlow.Name)
		for _, parentsFlow := range cronJobFlow.Depends {
			parentsFlowKey := utils.MakeCronJobFlowInfoKey(namespace, parentsFlow)
			s.childrenFlow[parentsFlowKey] = append(s.childrenFlow[parentsFlowKey], cronJobFlow.Name)
		}
		s.parentsFlow[flowKey] = cronJobFlow.Depends

		s.postpone[flowKey] = false
		s.delayed[flowKey] = false

		cronJobFlows = append(cronJobFlows, cronJobFlow)

		s.monitorWorkers[flowKey] = NewMonitorWorker(namespace, cronJobFlow, s)
		// FIXME(chenzhi): this may result in duplicated notification
		go s.monitorWorkers[flowKey].monitor()

		logrus.Infof("Init cronjobflow %v", cronJobFlow.Name)
		return nil
	})
	if err != nil {
		return err
	}

	for _, cronJobFlow := range cronJobFlows {
		now := time.Now()
		keypath := utils.MakeCronJobFlowInfoKey(cronJobFlow.Jobs[0].Namespace, cronJobFlow.Name)
		logrus.Infof("Add cronjobflow %s to cache entries", keypath)
		schedule, err := cron.Parse(cronJobFlow.Schedule)
		if err != nil {
			logrus.Errorf("Failed to parse schedule: %s, and %s will not be scheduled", cronJobFlow.Schedule, cronJobFlow.Name)
			continue
		}
		newEntry := &Entry{
			Flow:     cronJobFlow,
			Schedule: schedule,
		}
		newEntry.Next = newEntry.Schedule.Next(now)
		s.entries[keypath] = newEntry
	}

	// shortest period task makes sure cron scheduler will schedule tasks at least in shortest period.
	/*if shortestEntry, err := s.getShortestPeriodTask(); err != nil {
		return err
	} else {
		s.entries[shortestScheduleEntryName] = shortestEntry
	}*/

	return nil
}

func (s *Server) run() {
	logrus.Infof("Start to schedule cronjob flows by lastest execution time")
	// 1. calculate next activation time for entry jobflow
	now := time.Now()
	for _, entry := range s.entries {
		entry.Next = entry.Schedule.Next(now)
	}

	for {
		now = time.Now()
		// 2. sort by the time of next execution
		entries := make([]*Entry, 0, len(s.entries))
		for flowKey, value := range s.entries {
			if s.postpone[flowKey] {
				value.Next = time.Now().Add(s.defaultPostponeTime)
				s.postpone[flowKey] = false
			}
			// flow can be scheduled only when it's in running state
			if value.Flow != nil && value.Flow.Status != spec.StatusRunning {
				continue
			}
			entries = append(entries, value)
		}
		sort.Sort(byTime(entries))
		// 3. execute the most urgent task or sleep for no work
		var timer *time.Timer
		var duration time.Duration
		if len(entries) == 0 || entries[0].Next.IsZero() {
			// If there are no entries yet, just sleep - it still handles new entries
			// and stop requests.
			duration = 100000 * time.Hour
			timer = time.NewTimer(duration)
		} else {
			duration = entries[0].Next.Sub(now)
			timer = time.NewTimer(duration)
		}

		logrus.Infof("Next scheulder will be after %v", duration)
		select {
		// execute all the tasks on time.
		case now = <-timer.C:
			for _, e := range entries {
				if e.Next.After(now) || e.Next.IsZero() {
					break
				}
				if e.Flow == nil {
					e.Prev = e.Next
					e.Next = e.Schedule.Next(now)
					continue
				}
				logrus.Debugf("Cronjob flow %s is scheduled", e.Flow.Name)
				// TODO(chenzhi): get corresponding exsiting flow to judge cocurrenct policy, e.g. forbidden, replace or coexist
				go s.judgeAndCreateJobFlow(e.Flow)
				e.Prev = e.Next
				e.Next = e.Schedule.Next(now)
			}
		// add new cronjobflow to schedule list
		case entryMap := <-s.addChan:
			timer.Stop()
			now = time.Now()
			for key, flow := range entryMap {
				logrus.Debugf("Add cronjob flow %s to cache entries", key)
				schedule, err := cron.Parse(flow.Schedule)
				if err != nil {
					logrus.Errorf("Failed to parse schedule: %s, and %s will not be scheduled", flow.Schedule, flow.Name)
					continue
				}
				newEntry := &Entry{
					Flow:     flow,
					Schedule: schedule,
				}
				newEntry.Next = newEntry.Schedule.Next(now)
				s.entries[key] = newEntry
			}

		case key := <-s.deleteChan:
			timer.Stop()
			logrus.Debugf("Delete cronjob flow %s from cache entries", key)
			delete(s.entries, key)
		}
	}
}

func (s *Server) getJobFlowNameByScheduledTime(cronJobFlowName string, lastScheduleTime time.Time) string {
	jobFlowName := fmt.Sprintf("%s-%d", cronJobFlowName, lastScheduleTime.Unix())
	return jobFlowName
}

// delete not running jobflow if exists and create new one
func (s *Server) judgeAndCreateJobFlow(cronJobFlow *jobspec.CronjobFlowFull) {
	now := time.Now()
	namespace := cronJobFlow.Jobs[0].Namespace
	flowKey := utils.MakeCronJobFlowInfoKey(namespace, cronJobFlow.Name)
	if s.shouldPostpone(cronJobFlow, now) {
		logrus.Infof("Cronjob flow %s/%s is postponed", namespace, cronJobFlow.Name)
		s.postpone[flowKey] = true
		return
	}

	// 删除上次已创建的jobflow
	if cronJobFlow.LastCreatedJobFlows != nil && len(cronJobFlow.LastCreatedJobFlows) != 0 {
		flowName := cronJobFlow.LastCreatedJobFlows[0]
		// cronJobFlow is ensured with at least one job
		jobflow, err := s.proxy.EnsureGetJobFlow(namespace, flowName)
		if err != nil {
			logrus.Errorf("Failed to get jobflow %s/%s, error:%v", namespace, flowName, err)
		}
		// delete not running job flows
		if jobflow != nil {
			if jobflow.Status == spec.StatusRunning && !s.delayed[flowKey] {
				logrus.Infof("Jobflow: %s delay: %v", jobflow.Name, DefaultDelay)
				s.entries[flowKey].Next = now.Add(DefaultDelay)
				s.delayed[flowKey] = true
				return
			}
			if monitor, ok := s.monitorWorkers[flowKey]; ok {
				monitor.stopFlow(&jobflow.JobFlow)
			}
			logrus.Debugf("Recycling jobflow: %s, status: %s", flowName, jobflow.Status)
			if err := s.proxy.EnsureDeleteJobFlow(namespace, flowName); err != nil {
				logrus.Errorf("Failed to delete jobflow %s/%s, error:%v", namespace, flowName, err)
			}
		}
	}
	s.delayed[flowKey] = false

	// 创建新jobflow
	lastScheduleTime := time.Now()
	jobFlowName := s.getJobFlowNameByScheduledTime(cronJobFlow.Name, lastScheduleTime)
	cronJobFlow.LastScheduledTime = lastScheduleTime.Format(time.RFC3339)
	cronJobFlow.LastCreatedJobFlows = []string{jobFlowName}
	if err := s.proxy.EnsureCreateJobFlow(cronJobFlow); err != nil {
		logrus.Errorf("Failed to create jobflow %s/%s, error:%v", namespace, jobFlowName, err)
		// TODO 失败可直接返回
	}

	// TODO 创建成功可不用再次获取一次，直接发送通知即可
	// notify user that job flow is started
	jobflow, err := s.proxy.EnsureGetJobFlow(namespace, jobFlowName)
	if err != nil {
		logrus.Errorf("Failed to get jobflow %s/%s, error:%v", namespace, jobFlowName, err)
	}
	if jobflow != nil {
		if monitor, ok := s.monitorWorkers[flowKey]; ok {
			monitor.startFlow(&jobflow.JobFlow)
		}
	}

	// currently only one job flow serves cronjob flow at one time, so created job flow list is overwritten by
	// newly created job flow
	if err := s.store.Put(context.Background(), flowKey, cronJobFlow); err != nil {
		logrus.Errorf("Failed to update cronjobflow %s, with %s", flowKey, cronJobFlow)
	}
	s.entries[flowKey].Flow = cronJobFlow

}

// findChildren returns true if dstCronJobFlowName depends on curCronJobFlowName directly or indirectly
func (s *Server) findChildren(namespace, curCronJobFlowName, dstCronJobFlowName string) bool {
	if curCronJobFlowName == dstCronJobFlowName {
		return true
	}
	flowKey := utils.MakeCronJobFlowInfoKey(namespace, curCronJobFlowName)
	for _, child := range s.childrenFlow[flowKey] {
		if s.findChildren(namespace, child, dstCronJobFlowName) {
			return true
		}
	}
	return false

}

func (s *Server) validateCronJobFlowDependiency(cronJobFlow *jobspec.CronjobFlow) error {
	namespace := cronJobFlow.Jobs[0].Namespace
	for _, parentCronJobFlowName := range cronJobFlow.Depends {
		parentsFlowKey := utils.MakeCronJobFlowInfoKey(namespace, parentCronJobFlowName)
		if _, ok := s.entries[parentsFlowKey]; !ok {
			return errors.Errorf("CronJobFlow %s/%s does not exist", namespace, parentCronJobFlowName)
		}
		if s.findChildren(namespace, cronJobFlow.Name, parentCronJobFlowName) {
			return errors.Errorf("Loop was found")
		}
	}
	return nil
}

// A flow should be postponed when next period of depending flows is behind current scheduled time,
func (s *Server) shouldPostpone(cronJobFlow *jobspec.CronjobFlowFull, scheduleredTime time.Time) bool {
	namespace := cronJobFlow.Jobs[0].Namespace
	ctx := context.Background()
	var dependCronJobFlow jobspec.CronjobFlowFull
	for _, depend := range cronJobFlow.CronjobFlow.Depends {

		dependFlowPath := utils.MakeCronJobFlowInfoKey(namespace, depend)
		if err := s.store.Get(ctx, dependFlowPath, &dependCronJobFlow); err != nil {
			logrus.Errorf("Failed to get %s, skip this check, for %v", dependFlowPath, err)
			continue
		}
		if dependCronJobFlow.LastScheduledTime == "" {
			logrus.Infof("Cronjob flow %s/%s has not been scheduled, cronjob flow %s will be postponed", dependFlowPath, namespace, cronJobFlow.Name)
			return true
		}
		dependNextScheduledTime := s.entries[dependFlowPath].Schedule.Next(scheduleredTime)

		if dependNextScheduledTime.Before(scheduleredTime) {
			logrus.Debugf("Next scheduled time %v of depending flow is before current scheduled time %v", dependNextScheduledTime, scheduleredTime)
			return true
		}

		createdJobFlowName := dependCronJobFlow.LastCreatedJobFlows[0]
		var createJobFlow *jobspec.JobFlowResponse
		var err error
		if createJobFlow, err = s.proxy.EnsureGetJobFlow(namespace, createdJobFlowName); err != nil {
			logrus.Errorf("Failed to get last created job flow %s/%s, skip this check for %v ", namespace, createdJobFlowName, err)
			continue
		}

		// TODO 依赖任务失败，直接返回，不用 continue
		if createJobFlow.Status != spec.StatusRunning {
			continue
		} else {
			logrus.Debugf("Cronjob flow %s/%s will be postpone for wating for depending %s/%s to be done", namespace, cronJobFlow.Name, namespace, depend)
			return true
		}

	}

	return false
}

func (s *Server) validateCronJobFlow(cronJobFlow *jobspec.CronjobFlow) error {
	if len(cronJobFlow.Jobs) == 0 {
		return errors.Errorf("No job is provided")
	}
	if len(cronJobFlow.Jobs) > 10 {
		return errors.Errorf("too many jobs")
	}

	if !utils.ValidateJobFlowID(cronJobFlow.Name) {
		return errors.Errorf("invalid cronjob flow Name")
	}

	if cronExpOk, err := utils.ValidateCron(cronJobFlow.Schedule, leastPeriod); !cronExpOk {
		if err != nil {
			return errors.Errorf("Cron expression is not valid for %v", err.Error())
		} else {
			return errors.Errorf("Period of cron job flow should be in %v", leastPeriod)
		}
	}

	var namespace string
	jobs := make([]spec.Job, 0, len(cronJobFlow.Jobs))

	for _, j := range cronJobFlow.Jobs {
		namespace = j.Namespace
		if namespace == "" {
			namespace = "default"
		}
		if !utils.ValidateJobNamespace(namespace) || !utils.ValidateJobName(j.Name) {
			return errors.Errorf("invalid job namespace or name")
		}

		job := spec.Job{
			JobFromUser: j,
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusCreated,
			},
			CreatedTime: time.Now().Unix(),
			LastModify:  time.Now().Format(time.RFC3339),
		}
		jobs = append(jobs, job)
	}
	// validate flows dependencies
	_, err := utils.ParseJobFlow(jobs)
	if err != nil {
		return errors.Errorf("Failed to parse job flow for %v", err.Error())
	}
	return nil
}

func (s *Server) createCronjobFlowInner(ctx context.Context, cronJobFlow *jobspec.CronjobFlowFull) error {
	namespace := cronJobFlow.Jobs[0].Namespace

	flowKey := utils.MakeCronJobFlowInfoKey(namespace, cronJobFlow.Name)
	if err := s.store.Put(ctx, flowKey, cronJobFlow); err != nil {
		return errors.Errorf("Failed to store job flow %v", err.Error())
	}

	// create monitor worker to monitor execution result of jobs/jobflows origintated from this cronjoblfow
	// FIXME: updating cronjob flow will should update monitor worker for extra, currently is used in worker for notification
	if s.monitorWorkers[flowKey] == nil {
		s.monitorWorkers[flowKey] = NewMonitorWorker(namespace, cronJobFlow, s)
		go s.monitorWorkers[flowKey].monitor()
	}

	// 判断当前flow是否已经加到parentflow的children里
	for _, parentsFlow := range cronJobFlow.Depends {
		findChild := false

		parentsFlowKey := utils.MakeCronJobFlowInfoKey(namespace, parentsFlow)
		for _, child := range s.childrenFlow[parentsFlowKey] {
			if child == cronJobFlow.Name {
				findChild = true
				break
			}
		}

		if !findChild {
			s.childrenFlow[parentsFlowKey] = append(s.childrenFlow[parentsFlowKey], cronJobFlow.Name)
		}
	}

	s.parentsFlow[flowKey] = cronJobFlow.Depends
	s.addChan <- map[string]*jobspec.CronjobFlowFull{
		flowKey: cronJobFlow,
	}

	return nil
}

func (s *Server) startCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	namespace := vars["namespace"]
	name := vars["name"]
	logrus.Debugf("Start cron job flow %s/%s", namespace, name)
	defer logrus.Debugf("Finished to start cronjob flow %s/%s", namespace, name)
	jobKeyPath := utils.MakeCronJobFlowInfoKey(namespace, name)

	var cronJobFlow jobspec.CronjobFlowFull
	if err := s.store.Get(ctx, jobKeyPath, &cronJobFlow); err != nil {
		logrus.Infof("Failed to get cronjob flow %s from store, err: %v", jobKeyPath, err)

		if strings.Contains(err.Error(), "not found") {
			return httpserver.HTTPResponse{
				Status: http.StatusNotFound,
			}, nil
		}
		return nil, err
	}

	cronJobFlow.Status = spec.StatusRunning
	if err := s.store.Put(ctx, jobKeyPath, cronJobFlow); err != nil {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: fmt.Sprintf("Failed to store job flow %s", err.Error()),
			},
		}, nil
	}
	s.entries[jobKeyPath].Flow.Status = spec.StatusRunning

	// 重新开始通知
	if monitor, ok := s.monitorWorkers[jobKeyPath]; ok {
		logrus.Infof("CronjobFlow: %s started monitor...", jobKeyPath)
		monitor.startCronjobFlow()
	}

	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusRunning,
			},
		},
	}, nil
}

func (s *Server) stopCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	namespace := vars["namespace"]
	name := vars["name"]

	logrus.Debugf("Stop cron job flow %s/%s", namespace, name)
	defer logrus.Debugf("Finished to stop cronjob flow %s/%s", namespace, name)

	jobKeyPath := utils.MakeCronJobFlowInfoKey(namespace, name)

	var cronJobFlow jobspec.CronjobFlowFull
	if err := s.store.Get(ctx, jobKeyPath, &cronJobFlow); err != nil {
		logrus.Infof("Failed to get cronjob flow %s from store, err: %v", jobKeyPath, err)

		if strings.Contains(err.Error(), "not found") {
			return httpserver.HTTPResponse{
				Status: http.StatusNotFound,
			}, nil
		}
		return nil, err
	}

	// delete job flow first to ensure job flow is deleted before status of cronjobflow is persisted.
	if cronJobFlow.LastCreatedJobFlows != nil && len(cronJobFlow.LastCreatedJobFlows) != 0 {
		flowName := cronJobFlow.LastCreatedJobFlows[0]
		if err := s.proxy.EnsureDeleteJobFlow(namespace, flowName); err != nil {
			logrus.Infof("Failed to delete jobflow %s/%s when stopping cronjobflow %s/%s",
				namespace, flowName, namespace, name)
		}
	}

	cronJobFlow.Status = spec.StatusStopped
	if err := s.store.Put(ctx, jobKeyPath, cronJobFlow); err != nil {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: fmt.Sprintf("Failed to store job flow %s", err.Error()),
			},
		}, nil
	}
	s.entries[jobKeyPath].Flow.Status = spec.StatusStopped

	// 重新开始通知
	if monitor, ok := s.monitorWorkers[jobKeyPath]; ok {
		logrus.Infof("CronjobFlow: %s started monitor...", jobKeyPath)
		monitor.startCronjobFlow()
	}

	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusStopped,
			},
		},
	}, nil
}

func (s *Server) createCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {

	flow := jobspec.CronjobFlow{}
	if err := json.NewDecoder(r.Body).Decode(&flow); err != nil {
		logrus.Errorf("Request body is invalid: %v", err)
		return httpserver.HTTPResponse{Status: http.StatusBadRequest}, err
	}
	defer logrus.Infof("Finished to handle request for creating cronJobs flow %s", flow.Name)
	logrus.Infof("Received request for creating cronJob flow %s", flow.Name)

	if err := s.validateCronJobFlowDependiency(&flow); err != nil {
		logrus.Errorf("Request for creating %s is not valid for %v", flow.Name, err)
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil
	}

	if err := s.validateCronJobFlow(&flow); err != nil {
		logrus.Errorf("Request for creating %s is not valid for %v", flow.Name, err)
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil
	}
	// initialize status of cronjob flow to be "Running"
	cronjobflow := jobspec.CronjobFlowFull{
		ID:          getUUID(),
		CronjobFlow: flow,
		StatusDesc:  spec.StatusDesc{Status: spec.StatusRunning},
		CreatedTime: time.Now().Format(time.RFC3339),
	}
	if err := s.createCronjobFlowInner(ctx, &cronjobflow); err != nil {
		logrus.Errorf("Request for creating %s is not valid for %v", flow.Name, err)
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil
	}
	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			CronjobFlowFull: jobspec.CronjobFlowFull{
				ID: cronjobflow.ID,
				CronjobFlow: jobspec.CronjobFlow{
					Name: cronjobflow.CronjobFlow.Name,
				},
			},
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusRunning,
			},
		},
	}, nil
}

// updateCronjobFlow will not stop currently running job flows, and update will taken effect next period.
func (s *Server) updateCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	//validate input
	namespace := vars["namespace"]
	name := vars["name"]

	jobKeyPath := utils.MakeCronJobFlowInfoKey(namespace, name)

	defer logrus.Infof("Finished to handler request for updating flow %s", jobKeyPath)
	logrus.Infof("Received request for updating flow %s", jobKeyPath)

	cronJobFlow := jobspec.CronjobFlow{}
	if err := json.NewDecoder(r.Body).Decode(&cronJobFlow); err != nil {
		return httpserver.HTTPResponse{Status: http.StatusBadRequest}, err
	}

	var tmpCronJobFlow jobspec.CronjobFlowFull
	if err := s.store.Get(ctx, jobKeyPath, &tmpCronJobFlow); err != nil {
		if strings.Contains(err.Error(), "not found") {
			response := httpserver.HTTPResponse{
				Status: http.StatusNotFound,
			}
			return response, nil
		}
		return nil, err
	}
	if err := s.validateCronJobFlowDependiency(&cronJobFlow); err != nil {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil

	}

	if err := s.validateCronJobFlow(&cronJobFlow); err != nil {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil

	}

	// remove child if child do not depend on parent
	flowKey := utils.MakeCronJobFlowInfoKey(namespace, cronJobFlow.Name)
	for _, parent := range s.parentsFlow[flowKey] {
		parentsFlowKey := utils.MakeCronJobFlowInfoKey(namespace, parent)

		// TODO 逻辑待优化 label
		disinheritance := true
		for _, depend := range cronJobFlow.Depends {
			if parent == depend {
				disinheritance = false
				break
			}
		}
		if disinheritance {
			tmpChildren := []string{}
			for _, child := range s.childrenFlow[parentsFlowKey] {
				if child != cronJobFlow.Name {
					tmpChildren = append(tmpChildren, child)
				}
			}
			s.childrenFlow[parentsFlowKey] = tmpChildren
		}
	}

	tmpCronJobFlow.CronjobFlow = cronJobFlow
	if err := s.createCronjobFlowInner(ctx, &tmpCronJobFlow); err != nil {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: err.Error(),
			},
		}, nil
	}
	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			StatusDesc: spec.StatusDesc{
				Status: spec.StatusRunning,
			},
		},
	}, nil
}

func (s *Server) deleteCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	namespace := vars["namespace"]
	name := vars["name"]

	flowKey := utils.MakeCronJobFlowInfoKey(namespace, name)

	defer logrus.Infof("Finished to handle request for deleting flow %s", flowKey)
	logrus.Infof("Received request for deleting flow %s", flowKey)

	var cronJobFlow jobspec.CronjobFlowFull
	if err := s.store.Get(ctx, flowKey, &cronJobFlow); err != nil {
		logrus.Infof("Failed to get cronjob flow %s from store, err: %v", flowKey, err)

		if strings.Contains(err.Error(), "not found") {
			return httpserver.HTTPResponse{
				Status: http.StatusOK,
			}, nil
		}
		return nil, err
	}

	if len(s.childrenFlow[flowKey]) > 0 {
		return httpserver.HTTPResponse{
			Status: http.StatusBadRequest,
			Content: jobspec.CronjobFlowResponse{
				Error: fmt.Sprintf("%s cannot be deleted, for %v depends on it", cronJobFlow.Name, s.childrenFlow[flowKey]),
			},
		}, nil
	}

	// delete job flow first to ensure job flow is deleted before status of cronjobflow is persisted.
	if cronJobFlow.LastCreatedJobFlows != nil && len(cronJobFlow.LastCreatedJobFlows) != 0 {
		flowName := cronJobFlow.LastCreatedJobFlows[0]
		if err := s.proxy.EnsureDeleteJobFlow(namespace, flowName); err != nil {
			logrus.Infof("Failed to delete jobflow %s/%s when stopping cronjobflow %s/%s",
				namespace, flowName, namespace, name)
		}
	}

	// TODO(chenchi): delete should endure process restart
	if err := s.store.Remove(ctx, flowKey, &cronJobFlow); err != nil {
		return nil, err
	}

	for _, parent := range cronJobFlow.Depends {
		var tmpList []string
		for _, child := range s.childrenFlow[parent] {
			if child != cronJobFlow.Name {
				tmpList = append(tmpList, child)
			}
		}
		parentFlowKey := utils.MakeCronJobFlowInfoKey(namespace, parent)
		s.childrenFlow[parentFlowKey] = tmpList
	}

	delete(s.parentsFlow, flowKey)
	delete(s.childrenFlow, flowKey)
	delete(s.postpone, flowKey)
	delete(s.delayed, flowKey)

	if monitor, ok := s.monitorWorkers[flowKey]; ok {
		monitor.deleteCronjobFlow()
		delete(s.monitorWorkers, flowKey)
	}

	s.deleteChan <- flowKey

	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			CronjobFlowFull: cronJobFlow,
		},
	}, nil
}

func (s *Server) getCronjobFlow(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	namespace := vars["namespace"]
	name := vars["name"]
	jobKeyPath := utils.MakeCronJobFlowInfoKey(namespace, name)

	var cronJobFlow jobspec.CronjobFlowFull
	if err := s.store.Get(ctx, jobKeyPath, &cronJobFlow); err != nil {
		if strings.Contains(err.Error(), "not found") {
			response := httpserver.HTTPResponse{
				Status: http.StatusNotFound,
			}
			return response, nil
		}
		return nil, err
	}

	return httpserver.HTTPResponse{
		Status: http.StatusOK,
		Content: jobspec.CronjobFlowResponse{
			CronjobFlowFull: cronJobFlow,
		},
	}, nil
}

// epInfo may return the statistics of  all the cronjob flows hosted by the scheduler
func (s *Server) epInfo(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	return httpserver.HTTPResponse{
		Status:  http.StatusOK,
		Content: "cronjob Scheduler schedules cron jobs and cron job flows, build on top of dice scheduler.",
	}, nil
}

// TODO(chenzhi): get cronjob flows of specified namespace
func (s *Server) getCronjobFlows(ctx context.Context, r *http.Request, vars map[string]string) (httpserver.Responser, error) {
	namespace := vars["namespace"]
	defer logrus.Infof("Finished to handle request for listing flow of namespace %s", namespace)
	logrus.Infof("Received request for listing flows of namespace %s", namespace)
	cronJobFlows := []jobspec.CronjobFlowFull{}
	var err error
	logrus.Errorf("list %s %s", namespace, utils.GetCronJobFlowInfoPrefix(namespace))
	err = s.store.ForEach(ctx, utils.GetCronJobFlowInfoPrefix(namespace), jobspec.CronjobFlowFull{}, func(key string, j interface{}) error {
		cronJobFlow := j.(*jobspec.CronjobFlowFull)
		cronJobFlows = append(cronJobFlows, *cronJobFlow)
		return nil
	})
	if err != nil {
		return nil, err
	}
	return httpserver.HTTPResponse{
		Status:  http.StatusOK,
		Content: cronJobFlows,
	}, nil
}
