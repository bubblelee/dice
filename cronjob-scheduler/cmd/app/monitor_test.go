package app

import (
	"fmt"
	"strconv"
	"testing"
	"time"

	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/scheduler/spec"
)

func Test_getBaseJobName(t *testing.T) {
	baseJobName1 := "testJobName"
	jobNameSuffix := strconv.FormatInt(time.Now().Unix(), 10)
	jobName1 := fmt.Sprintf("%s-%s", baseJobName1, jobNameSuffix)
	ret1 := getBaseJobName(jobName1)
	if ret1 != baseJobName1 {
		t.Errorf("Execpted to get baseJobName %s, but got %s", baseJobName1, ret1)
	}

	baseJobName2 := "testJobName-abc"
	jobName2 := fmt.Sprintf("%s-%s", baseJobName2, jobNameSuffix)
	ret2 := getBaseJobName(jobName2)
	if ret2 != baseJobName2 {
		t.Errorf("Execpted to get baseJobName %s, but got %s", baseJobName2, ret2)
	}
}

func GetTestMonitorWorker() *MonitorWorker {
	server := GetFakeServer()
	cronjobFlow := &jobspec.CronjobFlowFull{
		ID: "cronjobuuid",
		CronjobFlow: jobspec.CronjobFlow{
			Name:         "cronjob",
			Schedule:     "0 0/5 * * * ?",
			CallBackUrls: []string{"http://localhost:8081/flow"},
			Jobs: []spec.JobFromUser{
				{
					Name:         "job",
					ID:           "jobuuid",
					CallBackUrls: []string{"http://localhost:8081/job"},
					Image:        "nginx",
					CPU:          0.1,
					Memory:       512,
					Namespace:    "test",
					Executor:     "metronome",
				},
			},
		},
	}
	monitorWorker := NewMonitorWorker("test", cronjobFlow, server)
	return monitorWorker
}

func Test_notifyJobFlow(t *testing.T) {
	monitorWorker := GetTestMonitorWorker()
	jobflow := &jobspec.JobFlow{
		Name:         "cronjob-12345",
		CallBackUrls: []string{"http://localhost:8081/flow"},
		Jobs: []spec.Job{
			{
				JobFromUser: spec.JobFromUser{Namespace: "test"},
			},
		},
		StatusDesc: spec.StatusDesc{
			Status: spec.StatusRunning,
		},
	}
	monitorWorker.notifyJobFlow(jobflow)
	notifier := monitorWorker.server.notifier.(*FakeNotifier)
	select {
	case <-notifier.Ch:
		return
	case <-time.After(1 * time.Second):
		t.Errorf("Did not receive notificatoin")
	}
}
