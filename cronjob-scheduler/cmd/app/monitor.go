package app

import (
	"context"
	"fmt"
	"strings"
	"time"

	notifierapi "terminus.io/dice/dice/eventbox/api"
	jobspec "terminus.io/dice/dice/pkg/job/spec"
	"terminus.io/dice/dice/pkg/job/utils"
	"terminus.io/dice/dice/scheduler/spec"

	"github.com/sirupsen/logrus"
)

const PeriodSeconds = 10

type cronJobFlowIdentifier struct {
	cronJobFlowName string
	cronJobFlowID   string
	extra           map[string]string
}

type MonitorWorker struct {
	cronJobFlowIdentifier
	namespace             string
	cronjobFlowRunningCh  chan bool             // CronjobFlow is running or stopped
	cronjobFlowDeleteCh   chan struct{}         // Cronjobflow删除时通知
	jobflowStartCh        chan *jobspec.JobFlow // jobflow创建时通知
	jobflowDeleteCh       chan *jobspec.JobFlow // jobflow删除时通知
	server                *Server
	notifier              notifierapi.Notifier
	resourceNotified      map[string]bool // 标记job/jobflow是否已通知, key: jobName/jobflowName
	cronjobFlowRunning    bool            // 存储cronjobFlow is running or stopped
	currentJobFlowName    string
	currentJobflowRunning bool // jobflow是否运行中，非running的jobflow不再轮询状态
}

// TODO remove namespace
// One CronjobFlow one monitorWorker
func NewMonitorWorker(namespace string, cronJobFlow *jobspec.CronjobFlowFull, s *Server) *MonitorWorker {
	cronjobFlowRunning := true
	if cronJobFlow.Status == spec.StatusStopped {
		cronjobFlowRunning = false
	}
	return &MonitorWorker{
		cronJobFlowIdentifier: cronJobFlowIdentifier{
			cronJobFlowName: cronJobFlow.Name,
			cronJobFlowID:   cronJobFlow.ID,
			extra:           cronJobFlow.Extra,
		},
		namespace:             namespace,
		cronjobFlowRunningCh:  make(chan bool, 1),
		cronjobFlowDeleteCh:   make(chan struct{}, 1),
		jobflowStartCh:        make(chan *jobspec.JobFlow, 1),
		jobflowDeleteCh:       make(chan *jobspec.JobFlow, 1),
		server:                s,
		notifier:              s.notifier,
		resourceNotified:      map[string]bool{},
		currentJobFlowName:    "",
		cronjobFlowRunning:    cronjobFlowRunning,
		currentJobflowRunning: true,
	}
}

func (w *MonitorWorker) monitor() {
	probeTickerPeriod := time.Duration(PeriodSeconds) * time.Second
	probeTicker := time.NewTicker(probeTickerPeriod)
	for w.probeAndNotify() {
		select {
		case <-probeTicker.C:
			continue

		case cronjobFlowRunning := <-w.cronjobFlowRunningCh:
			w.cronjobFlowRunning = cronjobFlowRunning
		case <-w.cronjobFlowDeleteCh:
			return

		case jobflow := <-w.jobflowStartCh:
			if w.currentJobFlowName != jobflow.Name {
				// 清除上个周期的通知标记，新的周期重新赋值
				w.resourceNotified = map[string]bool{}
				w.currentJobFlowName = jobflow.Name
			}
			w.notifyJobFlow(jobflow)
		case jobflow := <-w.jobflowDeleteCh:
			// FIXME probeAndNotify()方法里会再次查询jobflow，可能会查到新的周期.此处可改为直接通知，无需检测
			w.probeAndNotify()
			w.notifyJobFlow(jobflow)
		}
	}
}

func (w *MonitorWorker) startCronjobFlow() {
	select {
	case w.cronjobFlowRunningCh <- true:
	default:
	}
}

func (w *MonitorWorker) stopCronjobFlow() {
	select {
	case w.cronjobFlowRunningCh <- false:
	default:
	}
}

func (w *MonitorWorker) deleteCronjobFlow() {
	select {
	case w.cronjobFlowDeleteCh <- struct{}{}:
	default:
	}
}

func (w *MonitorWorker) stopFlow(jobflow *jobspec.JobFlow) {
	select {
	case w.jobflowDeleteCh <- jobflow:
	default:
	}
}

func (w *MonitorWorker) startFlow(jobflow *jobspec.JobFlow) {
	select {
	case w.jobflowStartCh <- jobflow:
	default:
	}
}

// jobName for cronjobflow is join by job name and timestamp suffix with "-"
// delete unix timestamp to get the base job name
func getBaseJobName(jobName string) string {
	nameList := strings.Split(jobName, "-")
	baseName := strings.Join(nameList[:len(nameList)-1], "-")
	return baseName
}

func getJobKey(namespace, jobName string) string {
	jobKey := fmt.Sprintf("job/%s/%s", namespace, jobName)
	return jobKey
}

func (w *MonitorWorker) notifyJobByJobFlow(jobflow *jobspec.JobFlow, cronjobflow *jobspec.CronjobFlowFull) {
	namespace := jobflow.Jobs[0].Namespace
	// report the results of finished jobs
	for _, job := range jobflow.Jobs {
		jobName := getJobKey(namespace, job.Name)
		if w.resourceNotified[jobName] {
			continue
		}
		ownerReference := jobspec.OwnerReference{
			ReferenceKind: jobspec.CronJobFlowOwner,
			Name:          cronjobflow.Name,
			Extra:         cronjobflow.Extra,
			ID:            cronjobflow.ID,
		}
		switch job.Status {
		case spec.StatusError, spec.StatusStoppedByKilled, spec.StatusStoppedOnFailed, spec.StatusStoppedOnOK:
			job.Name = getBaseJobName(job.Name)
			utils.TryToNotify(job, &ownerReference, w.notifier)
			w.resourceNotified[jobName] = true
		default:
			continue
		}
	}
}

func getjobFlowKey(namespace, jobFlowName string, finished bool) string {
	var jobFlowKey string
	if finished {
		jobFlowKey = fmt.Sprintf("finishedjobflow/%s/%s", namespace, jobFlowName)
	} else {
		jobFlowKey = fmt.Sprintf("runningjobflow/%s/%s", namespace, jobFlowName)
	}
	return jobFlowKey
}

// notifyJobFlow tries to notify the start or end of an jobflow once
func (w *MonitorWorker) notifyJobFlow(jobflow *jobspec.JobFlow) {
	namespace := jobflow.Jobs[0].Namespace
	ownerReference := jobspec.OwnerReference{
		ReferenceKind: jobspec.CronJobFlowOwner,
		Name:          w.cronJobFlowName,
		Extra:         w.extra,
		ID:            w.cronJobFlowID,
	}
	jobflow.Name = getBaseJobName(jobflow.Name)
	var jobFlowKey string
	switch jobflow.Status {
	case spec.StatusStoppedOnFailed, spec.StatusStoppedOnOK, spec.StatusError:
		jobFlowKey = getjobFlowKey(namespace, jobflow.Name, true)
		if w.resourceNotified[jobFlowKey] {
			return
		}
	case spec.StatusRunning:
		jobFlowKey = getjobFlowKey(namespace, jobflow.Name, false)
		if w.resourceNotified[jobFlowKey] {
			return
		}
	}
	utils.TryToNotify(*jobflow, &ownerReference, w.notifier)
	w.resourceNotified[jobFlowKey] = true
}

// 功能:
// 	  1. 判断cronjobflow是否已删除，若删除，则停止监听
//    2. 定期通知job
func (w *MonitorWorker) probeAndNotify() bool {
	// CronjobFlow状态为Stopped时，不再获取状态，暂不通知
	if !w.cronjobFlowRunning {
		return true
	}

	// TODO 若业务方的job通知不依赖cronJobFlow, 此处逻辑可去除,  10s一次轮询无意义
	cronjobFlowKey := utils.MakeCronJobFlowInfoKey(w.namespace, w.cronJobFlowName)
	var cronJobFlow jobspec.CronjobFlowFull
	if err := w.server.store.Get(context.Background(), cronjobFlowKey, &cronJobFlow); err != nil {
		logrus.Infof("Failed to get cronjob flow %s from store, and monitor probe will be skipped, err: %v", cronjobFlowKey, err)
		if strings.Contains(err.Error(), "not found") {
			return false
		}
		return true
	}

	if len(cronJobFlow.LastCreatedJobFlows) > 0 {

		// jobflow status为running时查询才有意义，避免非running情况下查询jobflow信息
		jobflowKey := utils.MakeJobFlowKey(w.namespace, w.currentJobFlowName)
		if !w.currentJobflowRunning {
			logrus.Infof("Jobflow: %s is not running", jobflowKey)
			return true
		}

		jobflowResponse, err := w.server.proxy.EnsureGetJobFlow(w.namespace, w.currentJobFlowName)
		if err != nil || jobflowResponse == nil {
			logrus.Infof("Failed to get jobflow %s/%s, error:%v", w.namespace, w.currentJobFlowName, err)
			return true
		}
		if jobflowResponse.JobFlow.Status != spec.StatusRunning {
			logrus.Infof("Jobflow: %s is not running, status: %s", jobflowKey, jobflowResponse.JobFlow.Status)
			w.currentJobflowRunning = false
			// 如果此jobflow未发送过stooped通知，则发送
			w.notifyJobFlow(&jobflowResponse.JobFlow)
		}

		w.notifyJobByJobFlow(&jobflowResponse.JobFlow, &cronJobFlow)
	}
	return true
}
