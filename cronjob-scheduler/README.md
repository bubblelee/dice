## Intro
cronjob Scheduler schedules cron jobs and cron job flows, build on top of dice scheduler.

## Usage
### Build
1. checkout build path of cronjob-scheduler
`cd cronjob-scheduler/build`

2. build linux executable binary to ensure repo can build successfully
`make bulid-linux`

3. build docker image locally
`make container`

4. push image to docker registry, you mush ensure first you have the right
crenditial to push images to the registry.
`make push`

In fact, you can just use one command, `make push`, to build binary and images locally, 
and push image to docker reistry.

### Run container
1. command line parameter
set container run command  manually as follows:
cronjob-scheduler --port 8081 --bind-address 192.168.0.3 --debug true --dice-scheduler-endpoint dicescheduler.marathon.l4lb.thisdcos.directory:9091

2. get options from environment
BIND_PORT=8081
BIND_ADDRESS=192.168.0.3
DEBUG=true
DICE_SCHEDULER_ENDPOINT=dicescheduler.marathon.l4lb.thisdcos.directory:9091
