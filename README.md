## Develop

### build

 make

### code check

make check

### unit tests

make unit-test

### tips 
When You are ready to commit your code, You must execute 'make check' and 'make unit-test'.


## Intro

#### dice/scheduler 

PaaS 的核心服务之一，支持短时 Job 和在线 Service 两种任务的调度管理；实现了对接各种底层集群 (eg: K8S，DC/OS 等）以及一些调度策略，集群的支持可以采用 Plugin 的方式开发，实现了完全的抽象和解耦。

#### dice/autoinit


#### dice/metacenter

