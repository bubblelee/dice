1. Install              // 1: SASS安装, 2: 私有化安装
2. Cpus, Mem, Instances // 集群资源配置, Instances=[0, 0]时会自动注入集群slave数量
	[
		低配资源配置,    // 不高可用, 集群资源较少, 一般作为测试集群, 资源配置应尽量低, 保证服务能正常运行即可
		高配资源配置     // 高可用, 集群资源足够作为生产集群, 资源配置可适当提高
	]
3. Host                // 是否自动分配一个节点并注入[constraints hostname LIKE Host], 此时Instances=[1, 1]即可
4. Image               // 镜像地址, 应设置为仅镜像版本号
5. Vars                // 字符串键值对, 在Definition中可以引用, 无需添加env和labels值为空的键
6. Definition          // Marathon APP定义, 默认的配置不要保留, 尽量减少其中的配置项

以下配置会自动处理, Marathon APP定义中无需添加
	acceptedResourceRoles
	[constraints dice_platform LIKE true]
	forcePullImage
	file:///netdata/docker-registry-aliyun/password.tar.gz
