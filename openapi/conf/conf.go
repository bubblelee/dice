package conf
import (
        "sync"
        "os"
)
type Conf struct {
	ListenAddr           string `default:":9529", env:"LISTEN_ADDR"`
	RedisAddr            string `default:"127.0.0.1:6379", env:"REDIS_ADDR"`
	RedisPwd             string `default:"anywhere", env:"REDIS_PWD"`
	UCAddr               string `default:"uc.terminus.io", env:"UC_ADDR"`
	UCRedirectHost       string `default:"openapi.test.terminus.io", env:"UC_REDIRECT_HOST"`
	UCClientID           string `default:"dice", env:"UC_CLIENT_ID"`
	UCClientSecret       string `default:"secret", env:"UC_CLIENT_SECRET"`
	RedirectAfterLogin   string `default:"//dice.test.terminus.io/", env:"REDIRECT_AFTER_LOGIN"`
	AdminCurrentUserHost string `default:"console.dice.marathon.l4lb.thisdcos.directory:8080", env:"USER_SCOPE_HOST"`
}
var (
        C Conf
        ListenAddrLock sync.Once
        RedisAddrLock sync.Once
        RedisPwdLock sync.Once
        UCAddrLock sync.Once
        UCRedirectHostLock sync.Once
        UCClientIDLock sync.Once
        UCClientSecretLock sync.Once
        RedirectAfterLoginLock sync.Once
        AdminCurrentUserHostLock sync.Once
)

func ListenAddr() string {
	ListenAddrLock.Do(func() {
		e := os.Getenv("LISTEN_ADDR")
		if e == "" {
			e = ":9529"
		}
                
                i := e
                
		C.ListenAddr = i
	})
	return C.ListenAddr
}

func RedisAddr() string {
	RedisAddrLock.Do(func() {
		e := os.Getenv("REDIS_ADDR")
		if e == "" {
			e = "127.0.0.1:6379"
		}
                
                i := e
                
		C.RedisAddr = i
	})
	return C.RedisAddr
}

func RedisPwd() string {
	RedisPwdLock.Do(func() {
		e := os.Getenv("REDIS_PWD")
		if e == "" {
			e = "anywhere"
		}
                
                i := e
                
		C.RedisPwd = i
	})
	return C.RedisPwd
}

func UCAddr() string {
	UCAddrLock.Do(func() {
		e := os.Getenv("UC_ADDR")
		if e == "" {
			e = "uc.terminus.io"
		}
                
                i := e
                
		C.UCAddr = i
	})
	return C.UCAddr
}

func UCRedirectHost() string {
	UCRedirectHostLock.Do(func() {
		e := os.Getenv("UC_REDIRECT_HOST")
		if e == "" {
			e = "openapi.test.terminus.io"
		}
                
                i := e
                
		C.UCRedirectHost = i
	})
	return C.UCRedirectHost
}

func UCClientID() string {
	UCClientIDLock.Do(func() {
		e := os.Getenv("UC_CLIENT_ID")
		if e == "" {
			e = "dice"
		}
                
                i := e
                
		C.UCClientID = i
	})
	return C.UCClientID
}

func UCClientSecret() string {
	UCClientSecretLock.Do(func() {
		e := os.Getenv("UC_CLIENT_SECRET")
		if e == "" {
			e = "secret"
		}
                
                i := e
                
		C.UCClientSecret = i
	})
	return C.UCClientSecret
}

func RedirectAfterLogin() string {
	RedirectAfterLoginLock.Do(func() {
		e := os.Getenv("REDIRECT_AFTER_LOGIN")
		if e == "" {
			e = "//dice.test.terminus.io/"
		}
                
                i := e
                
		C.RedirectAfterLogin = i
	})
	return C.RedirectAfterLogin
}

func AdminCurrentUserHost() string {
	AdminCurrentUserHostLock.Do(func() {
		e := os.Getenv("USER_SCOPE_HOST")
		if e == "" {
			e = "console.dice.marathon.l4lb.thisdcos.directory:8080"
		}
                
                i := e
                
		C.AdminCurrentUserHost = i
	})
	return C.AdminCurrentUserHost
}
