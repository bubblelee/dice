package http

import (
	"net/http"
	"net/http/httputil"

	"terminus.io/dice/dice/openapi/api"
)

func NewReverseProxy(director func(*http.Request),
	modifyResponse func(*http.Response) error) http.Handler {
	return &httputil.ReverseProxy{
		Director:       director,
		ModifyResponse: modifyResponse,
	}
}

type ReverseProxyWithCustom struct {
	reverseProxy http.Handler
}

func NewReverseProxyWithCustom(director func(*http.Request),
	modifyResponse func(*http.Response) error) http.Handler {
	r := NewReverseProxy(director, modifyResponse)
	return &ReverseProxyWithCustom{reverseProxy: r}
}

func (r *ReverseProxyWithCustom) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	spec := api.API.Find(req)
	if spec != nil && spec.Custom != nil {
		spec.Custom(rw, req)
		return
	}
	r.reverseProxy.ServeHTTP(rw, req)
}
