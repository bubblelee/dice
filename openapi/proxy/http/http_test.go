package http

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestHTTP(t *testing.T) {
	kill, err := RunAHTTPServer(12345, "content")
	assert.Nil(t, err)
	defer kill()
	director := func(r *http.Request) {
		r.URL.Scheme = "http"
		r.URL.Host = "127.0.0.1:12345"
		r.Header.Set("Origin", "http://127.0.0.1:12345")
	}
	p := NewReverseProxy(director, nil)
	go http.ListenAndServe("127.0.0.1:6666", p)
	time.Sleep(500 * time.Millisecond)
	r, err := http.Get("http://127.0.0.1:6666")
	assert.Nil(t, err)
	body, err := ioutil.ReadAll(r.Body)
	assert.Nil(t, err)
	assert.Equal(t, "content", string(body))
}

func TestCustom(t *testing.T) {
	director := func(r *http.Request) {
		r.URL.Scheme = "http"
		r.URL.Host = "127.0.0.1:12345"
		r.Header.Set("Origin", "http://127.0.0.1:12345")
	}

	p := NewReverseProxyWithCustom(director, nil)
	go http.ListenAndServe("127.0.0.1:6667", p)
	time.Sleep(500 * time.Millisecond)
	r, err := http.Get("http://127.0.0.1:6667/aaa/bbb/cc")
	assert.Nil(t, err)
	c, _ := ioutil.ReadAll(r.Body)
	assert.Equal(t, "/aaa/bbb/cc", string(c))
}

// return (kill_function, error)
func RunAHTTPServer(port int, content string) (func() error, error) {
	content_ := strings.Replace(content, "\"", "\\\"", -1)
	socat := fmt.Sprintf("socat tcp-listen:%d,reuseaddr,fork \"exec:printf \\'HTTP/1.0 200 OK\\r\\nContent-Type\\:application/json\\r\\n\\r\\n%s\\'\"", port, content_)
	cmd := exec.Command("sh", "-c", socat)
	err := cmd.Start()
	time.Sleep(500 * time.Millisecond)
	return cmd.Process.Kill, err
}
