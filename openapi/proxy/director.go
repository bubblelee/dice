package proxy

import (
	"net/http"
	"strings"

	"terminus.io/dice/dice/openapi/api"
)

func NewDirector() func(*http.Request) {
	return func(r *http.Request) {
		path := r.URL.Path
		if !strings.HasPrefix(path, "/") {
			path = "/" + path
		}
		spec := api.API.Find(r)

		if spec == nil {
			// not found
			panic("should not be here")
		}
		r.URL.Scheme = spec.Scheme.String()
		r.URL.Host = spec.Host
		r.Host = spec.Host
		r.Header.Set("Origin-Path", r.URL.Path)
		r.URL.Path = spec.BackendPath.Fill(spec.Path.Vars(path))
		r.Header.Set("Origin", "http://"+spec.Host)
	}
}
