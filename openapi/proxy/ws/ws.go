package ws

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net"
	"net/http"
	"strings"
)

type ReverseProxy struct {
	Director func(*http.Request)
}

func NewReverseProxy(director func(*http.Request)) http.Handler {
	return &ReverseProxy{
		Director: director,
	}
}

func (p *ReverseProxy) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	p.Director(req)
	host := req.Host
	if !strings.Contains(host, ":") {
		host = host + ":80"
	}
	logrus.Infof("tcp dial: %v", host)
	conn, err := net.Dial("tcp", host)
	if err != nil {
		errStr := fmt.Sprintf("dial with host[%s] failed", host)
		logrus.Error(errStr)
		http.Error(rw, errStr, 500)
		return
	}
	defer conn.Close()
	clientConn, _, err := rw.(http.Hijacker).Hijack()
	if err != nil {
		logrus.Error("hijack failed")
		http.Error(rw, "hijack failed", 500)
		return
	}
	defer clientConn.Close()
	if err := req.Write(conn); err != nil {
		errStr := fmt.Sprintf("write request to backend conn failed: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, 500)
		return
	}
	done := make(chan struct{})
	copy := func(dst io.Writer, src io.Reader) {
		io.Copy(dst, src)
		done <- struct{}{}

	}
	go copy(conn, clientConn)
	go copy(clientConn, conn)
	<-done
}
