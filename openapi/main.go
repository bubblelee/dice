package main

import (
	"terminus.io/dice/dice/pkg/version"

	"github.com/sirupsen/logrus"
)

func main() {
	logrus.Infof(version.String())

	srv, err := NewServer()
	if err != nil {
		panic(err)
	}
	if err := srv.ListenAndServe(); err != nil {
		panic(err)
	}
}
