package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"terminus.io/dice/dice/openapi/api"
	apispec "terminus.io/dice/dice/openapi/api/spec"
	"terminus.io/dice/dice/openapi/auth"
	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/openapi/proxy"
	phttp "terminus.io/dice/dice/openapi/proxy/http"
	"terminus.io/dice/dice/openapi/proxy/ws"
	validatehttp "terminus.io/dice/dice/openapi/validate/http"

	"github.com/sirupsen/logrus"
)

type ReverseProxyWithAuth struct {
	httpProxy http.Handler
	wsProxy   http.Handler
	auth      *auth.Auth
}

func NewReverseProxyWithAuth(auth *auth.Auth) (http.Handler, error) {
	director := proxy.NewDirector()
	httpProxy := phttp.NewReverseProxyWithCustom(director, modifyResponse)
	wsProxy := ws.NewReverseProxy(director)
	return &ReverseProxyWithAuth{httpProxy: httpProxy, wsProxy: wsProxy, auth: auth}, nil
}

func (r *ReverseProxyWithAuth) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	logrus.Infof("handle request: %v", req.URL)
	spec := api.API.Find(req)
	if spec == nil {
		errStr := fmt.Sprintf("not found path: %v", req.URL.Path)
		logrus.Error(errStr)
		http.Error(rw, errStr, 404)
		return
	}
	if err := r.auth.Auth(spec, req); err != nil {
		errStr := fmt.Sprintf("auth failed: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, 401)
		return
	}
	switch spec.Scheme {
	case apispec.HTTP:
		_, err := validatehttp.ValidateRequest(req)
		if err != nil {
			logrus.Warnf("validate request[%v] fail: %v", req.URL, err)
		}
		r.httpProxy.ServeHTTP(rw, req)
	case apispec.WS:
		r.wsProxy.ServeHTTP(rw, req)
	default:
		errStr := fmt.Sprintf("not support scheme: %v", spec.Scheme)
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusBadRequest)
		return
	}
}

func modifyResponse(res *http.Response) error {
	spec := api.API.FindBackend(res.Request)
	if spec == nil {
		// should not be here
		logrus.Errorf("modifyResponse: not found spec (should not be here):%v", res.Request.URL)
		return nil
	}
	var err error
	if spec.CustomResponse != nil {
		err = spec.CustomResponse(res)
	}
	if err != nil {
		logrus.Errorf("modifyResponse: %v", err)
	}
	return err
}

type LoginServer struct {
	r    http.Handler
	auth *auth.Auth
}

func NewLoginServer() (*LoginServer, error) {
	auth, err := auth.NewAuth()
	if err != nil {
		return nil, err
	}
	h, err := NewReverseProxyWithAuth(auth)
	if err != nil {
		return nil, err
	}
	return &LoginServer{r: h, auth: auth}, nil
}

type OAuthToken struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	Scope        string `json:"scope"`
	Jti          string `json:"jti"`
}

func (s *LoginServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	// TODO: move there to api/apis/
	if req.URL.Path == "/api/openapi/login" {
		s.Login(rw, req)
	} else if req.URL.Path == "/api/openapi/logincb" || req.URL.Path == "/logincb" {
		s.LoginCB(rw, req)
	} else if req.URL.Path == "/api/openapi/login" || req.URL.Path == "/login" {
		s.PwdLogin(rw, req)
	} else if req.URL.Path == "/api/openapi/logout" || req.URL.Path == "/logout" {
		s.Logout(rw, req)
	} else if req.URL.Path == "/api/openapi/userinfo" || req.URL.Path == "/userinfo" {
		s.UserInfo(rw, req)
	} else {
		s.r.ServeHTTP(rw, req)
	}
}

var ucURL = fmt.Sprintf("https://%s/oauth/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=public_profile", conf.UCAddr(), conf.UCClientID(), url.QueryEscape("https://"+conf.UCRedirectHost()+"/logincb"))

// http://uc.app.terminus.io/oauth/authorize?response_type=code&client_id=dice&redirect_uri=http%3A%2F%2Fopenapi.test.terminus.io%2Flogincb&scope=public_profile
func (s *LoginServer) Login(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	var u struct {
		URL string `json:"url"`
	}
	u.URL = ucURL
	// replace HTTP(s)
	isHTTPS, err := IsHTTPS(req)
	if err != nil {
		logrus.Errorf("Login: no Referer Header in request")
	} else if isHTTPS {
		u.URL = strings.Replace(u.URL, "https", "http", -1)
		u.URL = strings.Replace(u.URL, "http", "https", -1)
	} else {
		u.URL = strings.Replace(u.URL, "https", "http", -1)
	}
	var content bytes.Buffer
	d := json.NewEncoder(&content)
	if err := d.Encode(u); err != nil {
		logrus.Error(err)
		http.Error(rw, err.Error(), http.StatusBadGateway)
		return
	}
	rw.Write([]byte(content.String()))
}

func (s *LoginServer) LoginCB(rw http.ResponseWriter, req *http.Request) {
	code := req.URL.Query().Get("code")
	user := auth.NewUser(s.auth.RedisCli)
	isHTTPS, err := IsHTTPS(req)
	if err != nil {
		logrus.Errorf("LoginCB: no Referer Header in request")
		isHTTPS = true
	}
	sessionID, err := user.Login(code, isHTTPS)
	if err != nil {
		logrus.Errorf("login fail: %v", err)
		http.Error(rw, err.Error(), http.StatusUnauthorized)
		return
	}
	http.SetCookie(rw, &http.Cookie{
		Name:     auth.SessionIDCookieName,
		Value:    sessionID,
		Domain:   ".terminus.io",
		HttpOnly: true,
		Expires:  time.Now().Add(15 * 24 * time.Hour),
	})
	http.Redirect(rw, req, conf.RedirectAfterLogin(), http.StatusFound)
}

type PwdLoginResponse struct {
	SessionID string `json:"sessionid"`
	auth.UserInfo
}

func (s *LoginServer) PwdLogin(rw http.ResponseWriter, req *http.Request) {
	username := req.FormValue("username")
	passwd := req.FormValue("password")
	if username == "" || passwd == "" {
		errStr := "empty username or passwd"
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusUnauthorized)
		return
	}
	user := auth.NewUser(s.auth.RedisCli)
	sessionID, err := user.PwdLogin(username, passwd)
	if err != nil {
		errStr := fmt.Sprintf("pwdlogin fail: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusUnauthorized)
		return
	}
	info, err := user.GetInfo(nil)
	if err != nil {
		errStr := fmt.Sprintf("pwdlogin getInfo fail: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusUnauthorized)
		return
	}
	rw.WriteHeader(http.StatusOK)
	res := PwdLoginResponse{
		SessionID: sessionID,
		UserInfo:  info,
	}
	resJson, err := json.Marshal(res)
	if err != nil {
		errStr := fmt.Sprintf("marshal PwdLoginResponse fail: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusUnauthorized)
		return
	}
	rw.Write(resJson)
}

func (s *LoginServer) Logout(rw http.ResponseWriter, req *http.Request) {
	user := auth.NewUser(s.auth.RedisCli)
	if err := user.Logout(req); err != nil {
		errStr := fmt.Sprintf("logout: %v", err)
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusBadGateway)
		return
	}
	http.SetCookie(rw, &http.Cookie{Name: auth.SessionIDCookieName, Value: "", Path: "/", Expires: time.Unix(0, 0), MaxAge: -1, Domain: ".terminus.io", HttpOnly: true})

	var v struct {
		URL string `json:"url"`
	}
	v.URL = "https://" + conf.UCAddr() + "/logout?redirectUrl=" + url.QueryEscape(url.QueryEscape(fmt.Sprintf("https://%s/oauth/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=public_profile", conf.UCAddr(), conf.UCClientID(), "https://"+conf.UCRedirectHost()+"/logincb")))
	isHTTPS, err := IsHTTPS(req)
	if err != nil {
		logrus.Errorf("Logout: no Referer Header in request")
	} else if isHTTPS {
		v.URL = strings.Replace(v.URL, "https", "http", -1)
		v.URL = strings.Replace(v.URL, "http", "https", -1)
	} else {
		v.URL = strings.Replace(v.URL, "https", "http", -1)
	}
	var content bytes.Buffer
	if err := json.NewEncoder(&content).Encode(v); err != nil {
		http.Error(rw, err.Error(), http.StatusBadGateway)
		return
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(content.String()))
}

func (s *LoginServer) UserInfo(rw http.ResponseWriter, req *http.Request) {
	user := auth.NewUser(s.auth.RedisCli)
	logrus.Infof("userinfo: %v", req.Cookies())
	info, err := user.GetInfo(req)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusUnauthorized)
		return
	}
	cinfo := convertUserInfo(&info)
	content, err := json.Marshal(cinfo)
	if err != nil {
		errStr := "marshal user info fail"
		logrus.Error(errStr)
		http.Error(rw, errStr, http.StatusBadGateway)
		return
	}
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(content)
}

type ConsoleUserInfo struct {
	UserID              string            `json:"user_id"`
	Name                string            `json:"name"`
	GivenName           string            `json:"given_name"`
	FamilyName          string            `json:"family_name"`
	MiddleName          string            `json:"middle_name"`
	NickName            string            `json:"nickname"`
	UserName            string            `json:"username"`
	Profile             string            `json:"profile"`
	AvatarURL           string            `json:"avatar_url"`
	Website             string            `json:"website"`
	Email               string            `json:"email"`
	EmailVerified       bool              `json:"email_verified"`
	Gender              string            `json:"gender"`
	BirthDate           string            `json:"birthdate"`
	ZoneInfo            string            `json:"zoneinfo"`
	Locale              string            `json:"locale"`
	PhoneNumber         string            `json:"phone_number"`
	PhoneNumberVerified bool              `json:"phone_number_verified"`
	Address             map[string]string `json:"address"`
	Metadata            map[string]string `json:"metadata"`
}

func convertUserInfo(info *auth.UserInfo) *ConsoleUserInfo {
	cinfo := new(ConsoleUserInfo)
	cinfo.UserID = strconv.Itoa(info.ID)
	cinfo.Email = info.Email
	cinfo.BirthDate = info.Birthday
	cinfo.PhoneNumber = info.Phone
	cinfo.AvatarURL = info.AvatarUrl
	cinfo.UserName = info.UserName
	cinfo.NickName = info.NickName
	return cinfo
}

// http://uc.app.terminus.io/oauth/authorize?response_type=code&client_id=dice&redirect_uri=http%3A%2F%2Fopenapi.test.terminus.io%2Flogincb&scope=public_profile
func NewServer() (*http.Server, error) {
	s, err := NewLoginServer()
	if err != nil {
		return nil, err
	}
	srv := &http.Server{
		Addr:              conf.ListenAddr(),
		Handler:           s,
		ReadHeaderTimeout: 60 * time.Second, // TODO: test whether will timeout option affect websocket
	}
	return srv, nil
}

func IsHTTPS(req *http.Request) (bool, error) {
	referrer := req.Header.Get("Referer")
	if referrer == "" {
		return false, fmt.Errorf("no Referer header")
	}
	return strings.HasPrefix(referrer, "https:"), nil
}
