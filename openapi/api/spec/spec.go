package spec

import (
	"net/http"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type APIs []Spec

// TODO: add cache
func (o APIs) Find(req *http.Request) *Spec {
	for _, spec := range o {
		m := NewMatcher(&spec)
		if m.MatchMethod(req.Method) && m.MatchPath(req.URL.Path) {
			return &spec
		}
	}
	return nil
}

func (o APIs) FindBackend(req *http.Request) *Spec {
	for _, spec := range o {
		m := NewMatcher(&spec)
		if m.MatchMethod(req.Method) && m.MatchBackendPath(req.URL.Path) {
			return &spec
		}
	}
	return nil

}

type Matcher struct {
	spec *Spec
}

func NewMatcher(spec *Spec) *Matcher {
	return &Matcher{spec}
}

func (m *Matcher) MatchMethod(method string) bool {
	return m.spec.Method == "" || strings.ToLower(method) == strings.ToLower(m.spec.Method)
}

func (m *Matcher) MatchPath(path string) bool {
	return m.spec.Path.Match(path)
}
func (m *Matcher) MatchBackendPath(path string) bool {
	return m.spec.BackendPath.Match(path)
}

type Scheme int

const (
	WS Scheme = iota
	HTTP
)

func (t Scheme) String() string {
	switch t {
	case HTTP:
		return "http"
	case WS:
		return "ws"
	default:
		panic("should not be here")
	}
}
func SchemeFromString(s string) (Scheme, error) {
	switch s {
	case "http":
		return HTTP, nil
	case "ws":
		return WS, nil
	default:
		return HTTP, errors.New("illegal scheme")
	}
}

type Spec struct {
	Path           *Path
	BackendPath    *Path
	Host           string
	Scheme         Scheme
	Method         string
	Custom         func(rw http.ResponseWriter, req *http.Request)
	CustomResponse func(*http.Response) error
	CheckLogin     bool
	CheckToken     bool
	CheckBasicAuth bool
}

func (s *Spec) Validate() error {
	return nil
}

/*
Path:
由<xxx>组成url中可变部分
比如:
/dice/<company>/<project>/login
*/
var replaceElem = regexp.MustCompile("<[^*]*?>")

type Path struct {
	path      string
	parts     map[int]string // TODO: better name
	regexPath *regexp.Regexp
}

func NewPath(path string) *Path {
	path = polishPath(path)
	p := &Path{path: path, parts: map[int]string{}}
	p.parse()
	return p
}

// parse /a/<b>/<c> -> {"b":1, "c":2}
func (p *Path) parse() {
	parts := strings.Split(p.path, "/")
	for i, part := range parts {
		if strings.HasPrefix(part, "<") && strings.HasSuffix(part, ">") {
			p.parts[i] = part[1 : len(part)-1]
		}
	}
	regexpath := "^" + replaceElem.ReplaceAllString(p.path, "[^/]+?") + "$"
	regexpath = strings.Replace(regexpath, "<*>", ".+", -1)
	p.regexPath = regexp.MustCompile(regexpath)
}

func (p *Path) String() string {
	return p.path
}

func (p *Path) Vars(realPath string) map[string]string {
	r := map[string]string{}
	realPath = polishPath(realPath)
	idx := 0

	for {
		idx++
		realPath = strings.TrimLeft(realPath, "/")
		parts := strings.SplitN(realPath, "/", 2)

		part, ok := p.parts[idx]
		if !ok {
			if len(parts) == 2 {
				realPath = parts[1]
			}
		}

		if part == "*" {
			r[part] = realPath
			break
		} else if len(parts) == 2 {
			r[part] = parts[0]
			realPath = parts[1]
		} else if len(parts) == 1 {
			r[part] = parts[0]
			break
		}
	}
	return r
}

func (p *Path) Match(realPath string) bool {
	realPath = polishPath(realPath)
	return p.regexPath.MatchString(realPath)
}
func (p *Path) Fill(vars map[string]string) string {
	r := p.path
	for k, v := range vars {
		r = strings.Replace(r, "<"+k+">", v, -1)
	}
	return r
}

func polishPath(path string) string {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	return strings.TrimSuffix(path, "/")
}
