package apis

var ADMIN_ORG_SHOW = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/show",
	BackendPath: "/api/orgs/<orgId>/show",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
