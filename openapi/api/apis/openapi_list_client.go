package apis

import (
	"bytes"
	"fmt"
	"net/http"

	"terminus.io/dice/dice/openapi/auth"
	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/sirupsen/logrus"
)

var OPENAPI_LIST_CLIENT = ApiSpec{
	Path:   "/api/openapi/manager/clients",
	Scheme: "http",
	Method: "GET",
	Custom: func(rw http.ResponseWriter, req *http.Request) {
		token, err := auth.GetDiceClientToken()
		if err != nil {
			errStr := fmt.Sprintf("get token fail: %v", err)
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		logrus.Infof("diceclienttoken: %+v", token)
		var body bytes.Buffer
		r, err := httpclient.New(httpclient.WithCompleteRedirect()).Get(conf.UCAddr()).Path("/api/open-client/manager/clients").
			Header("Authorization", "Bearer "+token.AccessToken).Do().Body(&body)
		if err != nil {
			errStr := fmt.Sprintf("list client fail: %v", err)
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		if !r.IsOK() {
			errStr := fmt.Sprintf("list client fail, statuscode: %d, body: %v", r.StatusCode(), body.String())
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		rw.Write([]byte(body.String()))
	},
	CheckLogin: false, // TODO:
	Doc: `
summary: 获取client列表
description: 认证： 通过 Authorization 头信息进行认证。 格式为“Bearer <token>”, 注意空格
produces:
  - application/json
`,
}
