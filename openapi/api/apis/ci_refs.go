package apis

var CI_REFS = ApiSpec{
	Path:        "/api/ci/refs",
	BackendPath: "/api/ci/refs",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取git refs",
}
