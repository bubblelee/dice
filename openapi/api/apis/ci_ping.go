package apis

var CI_PING = ApiSpec{
	Path:        "/api/ci/ping",
	BackendPath: "/api/ci/ping",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  false,
	Doc:         "summary: ci get env",
}
