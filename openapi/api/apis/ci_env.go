package apis

var CI_ENV = ApiSpec{
	Path:        "/api/ci/env-branches/<projectId>",
	BackendPath: "/api/ci/env-branches/<projectId>",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: ci get env",
}
