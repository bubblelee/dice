package apis

var CMDB_ONE_ADDON_OLD = ApiSpec{
	Path:        "/api/cmdb/addon/<cluster>/<addon>",
	BackendPath: "/api/dice/addon/<cluster>/<addon>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定addon的容器信息",
}
