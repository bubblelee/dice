package apis

var ADMIN_APPLICATION_CREATE = ApiSpec{
	Path:        "/api/admin/applications",
	BackendPath: "/api/applications",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取组织",
}
