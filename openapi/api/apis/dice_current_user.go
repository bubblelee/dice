package apis

import (
	"bytes"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

var DICE_CURRENT_USER = ApiSpec{
	Path:        "/api/openapi/users/current",
	BackendPath: "/api/users/current",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 返回当前用户的信息",
	CustomResponse: func(r *http.Response) error {
		content, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logrus.Error(err)
		}
		logrus.Infof("DICE_CURRENT_USER request: %v", r.Request)
		logrus.Infof("DICE_CURRENT_USER resp: %v", r.Header)
		logrus.Infof("DICE_CURRENT_USER:%+v", string(content[:100])) // debug print
		r.Body = ioutil.NopCloser(bytes.NewReader(content))
		return nil
	},
}
