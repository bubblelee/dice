package apis

var IMAGEHUB_IMAGE_DELETE = ApiSpec{
	Path:        "/api/imagehub/images/{imageIdOrImage}",
	BackendPath: "/api/images/{imageIdOrImage}",
	Host:        "imagehub.dice.marathon.l4lb.thisdcos.directory:10000",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	Doc:         `
summary: 删除镜像
parameters:
  - in: path
    name: imageIdOrImage
    type: string
    required: true
    description: 镜像Id或镜像地址
produces:
  - application/json
responses:
  '200':
    description: OK
`,
}
