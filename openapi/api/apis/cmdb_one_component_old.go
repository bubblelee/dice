package apis

var CMDB_ONE_COMPONENT_OLD = ApiSpec{
	Path:        "/api/cmdb/component/<cluster>/<component>",
	BackendPath: "/api/dice/component/<cluster>/<component>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定平台组件的容器信息",
}
