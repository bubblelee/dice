package apis

var CMDB_CLUSTER_USAGE = ApiSpec{
	Path:        "/api/cmdb/clusters/{cluster}/usage",
	BackendPath: "/api/clusters/{cluster}/usage",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取指定集群的资源使用情况
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
      type: object
      properties:
        total_memory: 
          type: number
          format: double
          description: 总内存数（GB）
        total_cpu: 
          type: number
          format: double
          description: 总内存数（GB）
        total_disk: 
          type: number
          format: double
          description: 总内存数（GB）
        used_memory: 
          type: number
          format: double
          description: 分配的内存（GB）
        used_cpu: 
          type: number
          format: double
          description: 分配的cpu数
        used_disk: 
          type: number
          format: double
          description: 分配的磁盘空间（GB）
`,
}
