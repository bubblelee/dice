package apis

var CMDB_BUILD_DYNAMIC = ApiSpec{
	Path:        "/api/cmdb/statistics/build-dynamic",
	BackendPath: "/api/statistics/build-dynamic",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取指定范围的构建动态
parameters:
  - in: query
    name: type
    type: string
    required: true
    enum: [platform, org, project]
    description: 获取指定类型的部署统计信息
  - in: path
    name: org
    type: string
    description: 当type等于[org, project]时，需要配置org参数，用于指定组织名或者ID
  - in: path
    name: project
    type: string
    description: 当type等于project时，需要配置project参数，用于指定项目名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          org_id:
            type: integer
            description: 组织ID
          project_id:
            type: integer
            format: int64
            description: 项目ID
          application_id:
            type: integer
            format: int64
            description: 应用ID
          runtime_id:
            type: integer
            format: int64
            description: runtime ID
          user_id:
            type: integer
            format: int64
            description: 用户ID
          build_id:
            type: integer
            format: int64
            description: 构建ID
          application_name:
            type: string
            description: 应用名
          git_branch:
            type: string
            description: git 分支名
          action:
            type: string
            enum: [build, deploy]
            description: 动态类型
            example: build
          status:
            type: string
            enum: [running, success, failed, canceled]
            description: 动态状态
            example: success
`,
}
