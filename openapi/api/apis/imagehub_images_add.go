package apis

var IMAGEHUB_IMAGE_ADD = ApiSpec{
	Path:        "/api/imagehub/images",
	BackendPath: "/api/images",
	Host:        "imagehub.dice.marathon.l4lb.thisdcos.directory:10000",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc: `
summary: 创建镜像
parameters:
  - in: body
    name: request body
    description: request body format
    schema:
      type: object
      properties:
        imageName:
          type: string
          required: true
        imageTag:
          type: string
          required: true
        imageUrl:
          type: string
          required: true
          description: 镜像地址,eg:docker-registry.registry.marathon.l4lbcos.directory:5000/ubuntu:18.04
        orgId:
          type: integer
          format: int64
          required: true
        clusterName:
          type: string
          required: true
        projectId:
          type: integer
          format: int64
          required: true
        appId:
          type: integer
          format: int64
          required: true
        appName:
          type: string
          required: true
        gitRepo:
          type: string
          required: true
          description: 代码仓库地址
        gitBranch:
          type: string
          required: true
        gitCommitId:
          type: string
          required: true
        env:
          type: string
          required: true
          description: 开发环境:DEV/TEST/STAGING/PROD
        operatorId:
          type: integer
          format: int64
          required: true
          description: 操作用户Id
        operator:
          type: string
          required: true
          description: 操作用户名称
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
      type: object
      properties:
        imageId: 
          type: integer
          format: int64
          description: int64
`,
}
