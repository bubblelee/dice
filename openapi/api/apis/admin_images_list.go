package apis

var ADMIN_IMAGE_LIST = ApiSpec{
	Path:        "/api/admin/images",
	BackendPath: "/api/images",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
