package apis

var CMDB_ORG_BUILD_DYNAMIC_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/dynamic/build/org/<orgId>",
	BackendPath: "/api/dice/overview/dynamic/build/org/<orgId>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定org下的构建动态",
}
