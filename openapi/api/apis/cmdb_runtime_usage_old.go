package apis

var CMDB_RUNTIME_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/<project>/<application>/runtime",
	BackendPath: "/api/dice/usage/<cluster>/<project>/<application>/runtime",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定app下所有runtimes的资源使用情况",
}
