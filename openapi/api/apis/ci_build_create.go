package apis

var CI_BUILD_CREATE = ApiSpec{
	Path:        "/api/ci/builds",
	BackendPath: "/api/ci/builds",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc:         "summary: ci build create",
}
