package apis

var CMDB_DEPLOY_STATISTICS = ApiSpec{
	Path:        "/api/cmdb/statistics/deployment",
	BackendPath: "/api/statistics/deployment",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取指定范围的部署统计信息
parameters:
  - in: query
    name: type
    type: string
    required: true
    description: 获取指定类型的部署统计信息
    enum: [platform, org, project]
  - in: path
    name: org
    type: string
    description: 当type等于[org, project]时，需要配置org参数，用于指定组织名或者ID
  - in: path
    name: project
    type: string
    description: 当type等于project时，需要配置project参数，用于指定项目名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          success:
            type: integer
            format: int64
            description: 成功的数量
          failed:
            type: integer
            format: int64
            description: 失败的数量
          running:
            type: integer
            format: int64
            description: 运行中的数量
          succ_rate:
            type: number
            format: float32
            description: 当前成功率
          history_succ_rate:
            type: number
            format: float32
            description: 历史成功率
          details:
            type: object
            properties:
              time:
                type: array
                items:
                  type: integer
                  format: int64
                description: 时间戳
                example: 1536659078
              results:
                type: array
                items:
                  type: object
                  properties:
                    type:
                      type: string
                      description: 数据类型
                      enum: [deploySuccNum, deployFailedNum]
                      example: deploySuccNum
                    data:
                      type: array
                      items:
                        type: integer
                        format: int64
                      description: 数据
                      example: [10, 20]
`,
}
