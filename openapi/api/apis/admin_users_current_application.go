package apis

var ADMIN_USER_CURRENT_APPLICATION = ApiSpec{
	Path:        "/api/admin/users/current/applications",
	BackendPath: "/api/users/current/applications",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
