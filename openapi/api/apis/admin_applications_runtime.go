package apis

var ADMIN_APPLICATION_RUNTIME = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/runtimes/<runtimeId>",
	BackendPath: "/api/applications/<applicationId>/runtimes/<runtimeId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取application下的应用",
}
