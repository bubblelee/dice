package apis

var ADMIN_USER_LOCK = ApiSpec{
	Path:        "/api/admin/users/<userId>/lock",
	BackendPath: "/api/users/<userId>/lock",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
