package apis

var CMDB_APP_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/<project>/application",
	BackendPath: "/api/dice/usage/<cluster>/<project>/application",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定project下所有apps的资源使用情况",
}
