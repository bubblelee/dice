package apis

var ADMIN_ORG_FETCH = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>",
	BackendPath: "/api/orgs/<orgId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
