package apis

var CMDB_PROJECT_DEPLOY_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/deploy/project/<project>",
	BackendPath: "/api/dice/overview/deploy/project/<project>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定project下的部署总览信息",
}
