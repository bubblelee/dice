package apis

var CMDB_TASK_STATISTICS = ApiSpec{
	Path:        "/api/cmdb/statistics/task",
	BackendPath: "/api/statistics/task",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取指定范围的实例信息
parameters:
  - in: query
    name: type
    type: string
    required: true
    description: 获取指定类型的部署统计信息
    enum: [platform, org, project]
  - in: path
    name: org
    type: string
    description: 当type等于[org, project]时，需要配置org参数，用于指定组织名或者ID
  - in: path
    name: project
    type: string
    description: 当type等于project时，需要配置project参数，用于指定项目名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          running:
            type: integer
            format: int64
            description: 运行中的数量
            example: 10
          abnormal:
            type: integer
            format: int64
            description: 异常的数量
          details:
            type: object
            properties:
              time:
                type: array
                items:
                  type: integer
                  format: int64
                description: 时间戳
                example: [1536658726]
              results:
                type: array
                items:
                  type: object
                  properties:
                    type:
                      type: string
                      description: 数据类型
                      example: taskNum
                    data:
                      type: array
                      items:
                        type: integer
                      description: 数据
                      example: [1, 2]
`,
}
