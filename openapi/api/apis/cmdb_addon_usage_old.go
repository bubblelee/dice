package apis

var CMDB_ADDON_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/addon",
	BackendPath: "/api/dice/usage/<cluster>/addon",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下所有addons的资源使用情况",
}
