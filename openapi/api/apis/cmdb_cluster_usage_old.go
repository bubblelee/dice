package apis

var CMDB_CLUSTER_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>",
	BackendPath: "/api/dice/usage/<cluster>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群的资源使用情况",
}
