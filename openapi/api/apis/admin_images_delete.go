package apis

var ADMIN_IMAGE_DELETE = ApiSpec{
	Path:        "/api/admin/images/<imageId>",
	BackendPath: "/api/images/<imageId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
