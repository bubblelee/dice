package apis

var CMDB_INSTANCE_USAGE = ApiSpec{
	Path:        "/api/cmdb/clusters/<cluster>/instances/<instance>/usage",
	BackendPath: "/api/clusters/<cluster>/instances/<instance>/usage",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取某个集群下指定实例的资源使用情况
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
  - in: path
    name: instance
    type: string
    required: true
    description: 容器ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          id:
            type: string
            description: 容器ID
          memory:
            type: number
            format: double
            description: 分配的内存大小单位（MB）
          cpu:
            type: number
            format: double
            description: 分配的CPU核数
          disk:
            type: number
            format: double
            description: 分配的磁盘大小单位（MB）
`,
}
