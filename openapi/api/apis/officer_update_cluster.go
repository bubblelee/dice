package apis

var OFFICER_UPDATE_CLUSTER = ApiSpec{
	Path:        "/api/clusters",
	BackendPath: "/api/clusters",
	Method:      "PUT",
	Host:        "dice.colony-officer.marathon.l4lb.thisdcos.directory:9029",
	Scheme:      "http",
	CheckLogin:  true,
	Doc:         "summary: 更新集群",
}
