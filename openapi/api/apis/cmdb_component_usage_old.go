package apis

var CMDB_COMPONENT_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/component",
	BackendPath: "/api/dice/usage/<cluster>/component",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下所有平台组件的资源使用情况",
}
