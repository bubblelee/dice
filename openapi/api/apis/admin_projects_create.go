package apis

var ADMIN_PROJECT_CREATE = ApiSpec{
	Path:        "/api/admin/group-projects",
	BackendPath: "/api/group-projects",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
