package apis

var CMDB_RESOURCE_STATISTICS = ApiSpec{
	Path:        "/api/cmdb/statistics/resource",
	BackendPath: "/api/statistics/resource",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 统计项目或应用的资源使用情况
parameters:
  - in: query
    name: type
    type: string
    required: true
    description: 获取指定类型的资源统计信息
    enum: [platform, oject, application]
  - in: query
    name: org
    type: string
    description: 当type等于[org, project]时，需要配置org参数，用于指定组织名或者ID
  - in: query
    name: project
    type: string
    description: 当type等于project时，需要配置project参数，用于指定项目名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: array
        items:
          type: object
          properties:
            name:
              type: string
              description: project 或者 application 名称
            totalCPU:
              type: number
              format: float32
              description: 总的 cpu 核数
            totalMemory:
              type: integer
              format: int64
              description: 总的 memory 使用量
            cpuPercent:
              type: number
              format: float32
              description: 该project或者app在总的CPU池子中占的比例
            memPercent:
              type: number
              format: float32
              description: 该project或者app在总的MEM池子中占的比例
            details:
              type: array
              items:
                type: object
                properties:
                  name:
                    type: string
                    description: 查询的是project的资源使用情况的话，则该name表示 application；查询的是application的话，则该name表示 runtime
                  values:
                    type: array
                    items:
                      type: object
                      properties:
                        cpu:
                          type: number
                          format: float32
                          description: cpu 核数
                        memory:
                          type: integer
                          format: int64
                          description: 内存占用量
                        cpuPercent:
                          type: number
                          format: float32
                          description: cpu 占用比例
                        memPercent:
                          type: number
                          format: float32
                          description: mem 占用比例  
`,
}
