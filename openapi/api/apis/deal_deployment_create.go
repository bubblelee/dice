package apis

var DEAL_DEPLOYMENT_CREATE = ApiSpec{
	Path:        "/api/deal/deployments",
	BackendPath: "/api/deployments",
	Host:        "open.dice.marathon.l4lb.thisdcos.directory:8081",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  false,
	Doc:         "summary: 创建部署",
}
