package apis

var EVENTBOX_WEBHOOK_PUT = ApiSpec{
	Path:        "/api/eventbox/webhook",
	BackendPath: "/api/dice/eventbox/register",
	Host:        "diceeventbox.marathon.l4lb.thisdcos.directory:9528",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	Doc: `
summary: create webhook
consumes:
  - application/json
parameters:
  - in: body
    description: webhook to create
    schema:
      type: object
      properties:
        key:
          type: string
        labels:
          type: object
          description: map[string]interface{}, 这里interface{}是因为不同label有不同类型, httpcallback和钉钉都是 []string
produces:
  - application/json
responses:
  '200':
    description: ok
    
  '400':
    description: bad request
`,
}
