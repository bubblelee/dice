package apis

var OFFICER_ADD_CLUSTER = ApiSpec{
	Path:        "/api/clusters",
	BackendPath: "/api/clusters",
	Method:      "POST",
	Host:        "dice.colony-officer.marathon.l4lb.thisdcos.directory:9029",
	Scheme:      "http",
	CheckLogin:  true,
	Doc:         "summary: 添加集群",
}
