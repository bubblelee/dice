package apis

var CMDB_PROJECT_BUILD_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/build/project/<project>",
	BackendPath: "/api/dice/overview/build/project/<project>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定project下的构建总览信息",
}
