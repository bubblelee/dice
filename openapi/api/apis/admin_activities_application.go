package apis

var ADMIN_ACTIVITIES_APPLICATION = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/activities",
	BackendPath: "/api/applications/<applicationId>/activities",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前用户的活动",
}
