package apis

var CMDB_CLUSTER_BUILD_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/build",
	BackendPath: "/api/dice/overview/build",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的构建总览信息",
}
