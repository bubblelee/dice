package apis

var EVENTBOX_WEBHOOK_GET = ApiSpec{
	Path:        "/api/eventbox/webhook",
	BackendPath: "/api/dice/eventbox/register",
	Host:        "diceeventbox.marathon.l4lb.thisdcos.directory:9528",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: list webhooks
parameters:
  - in: query
    name: key
    type: string
    required: true
produces:
  - application/json
responses:
  '200':
    description: map[string]interface{}, 这里interface{}是因为不同label有不同类型, httpcallback和钉钉都是 []string
  '400':
    description: bad request
`,
}
