package apis

var CMDB_ONE_HOST = ApiSpec{
	Path:        "/api/cmdb/clusters/<cluster>/hosts/<host>",
	BackendPath: "/api/clusters/<cluster>/hosts/<host>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取某集群下指定宿主机信息
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
  - in: path
    name: host
    type: string
    required: true
    description: 宿主机名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          hostname:
            type: string
            description: 主机名
          cluster_full_name:
            type: string
            description: 集群ID
          cpus:
            type: number
            format: double
            description: 总CPU个数
          memory:
            type: integer
            format: int64
            description: 总内存数（字节）
          disk:
            type: integer
            format: int64
            description: 磁盘大小（字节）
          private_addr:
            type: string
            description: 内网地址
          labels:
            type: string
            description: 环境标签
          os:
            type: string
            description: 操作系统类型
          kernel_version:
            type: string
            description: 内核版本
          system_time:
            type: string
            description: 系统时间
          created_at:
            type: integer
            format: int64
            description: 创建时间（operator定义）
          deleted:
            type: boolean
            description: 资源是否被删除
          timestamp:
            type: integer
            format: int64
            description: 消息本身的时间戳
`,
}
