package apis

var ADMIN_ORG_MEMBER_ROLE_SET = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/members/<userId>/role",
	BackendPath: "/api/orgs/<orgId>/members/<userId>/role",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
