package apis

var ADMIN_MESSAGE_TYPE_READ = ApiSpec{
	Path:        "/api/admin/messages/type/<messageType>/read",
	BackendPath: "/api/messages/type/<messageType>/read",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
