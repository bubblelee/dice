package apis

var ADMIN_ORG_CONFIG_PROJECT = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/projects",
	BackendPath: "/api/orgs/<orgId>/projects",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
