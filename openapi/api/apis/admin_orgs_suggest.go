package apis

var ADMIN_ORG_SUGGEST = ApiSpec{
	Path:        "/api/admin/orgs/suggest",
	BackendPath: "/api/orgs/suggest",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
