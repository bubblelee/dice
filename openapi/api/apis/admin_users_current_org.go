package apis

var ADMIN_USER_CURRENT_ORG = ApiSpec{
	Path:        "/api/admin/users/current/orgs",
	BackendPath: "/api/users/current/orgs",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
