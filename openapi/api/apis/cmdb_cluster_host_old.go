package apis

var CMDB_CLUSTER_HOST_OLD = ApiSpec{
	Path:        "/api/cmdb/host/<cluster>",
	BackendPath: "/api/dice/host/<cluster>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的所有host信息",
}
