package apis

var CMDB_RNNTIME_CONTAINER_OLD = ApiSpec{
	Path:        "/api/cmdb/container/<cluster>/<project>/<application>/<runtime>",
	BackendPath: "/api/dice/container/<cluster>/<project>/<application>/<runtime>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定runtime下的所有容器信息",
}
