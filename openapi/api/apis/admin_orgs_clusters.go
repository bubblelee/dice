package apis

var ADMIN_ORG_MEMBER_CLUSTERS = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/clusters",
	BackendPath: "/api/orgs/<orgId>/clusters",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
