package apis

var DEAL_DEPLOYMENT_QUERY_STATUS = ApiSpec{
	Path:        "/api/deal/deployments/<deploymentId>/status",
	BackendPath: "/api/deployments/<deploymentId>",
	Host:        "open.dice.marathon.l4lb.thisdcos.directory:8081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  false,
	Doc:         "summary: 查询部署状态",
}
