package apis

var CI_WS = ApiSpec{
	Path:        "/api/ci/ws/<*>",
	BackendPath: "/api/ci/ws/<*>",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "ws",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: ci websocket",
}
