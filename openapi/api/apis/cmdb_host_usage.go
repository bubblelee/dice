package apis

var CMDB_HOST_USAGE = ApiSpec{
	Path:        "/api/cmdb/clusters/<cluster>/hosts/<host>/usage",
	BackendPath: "/api/clusters/<cluster>/hosts/<host>/usage",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取某个集群下指定宿主机的资源使用情况
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
  - in: path
    name: host
    type: string
    required: true
    description: 宿主机名或者ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          host_name:
            type: string
            description: 宿主机名称
          ip_address:
            type: string
            description: 宿主机IP
          total_memory:
            type: number
            format: double
            description: 总的内存大小
          total_cpu:
            type: number
            format: double
            description: 总的CPU核数
          total_disk:
            type: number
            format: double
            description: 总的磁盘大小
          used_memory:
            type: number
            format: double
            description: 已使用内存大小
          used_cpu:
            type: number
            format: double
            description: 已使用CPU核数
          used_disk:
            type: number
            format: double
            description: 已使用磁盘大小
          labels:
            type: string
            description: 宿主机标签
          tasks:
            type: integer
            description: 运行的实例数量
          created_at:
            type: integer
            format: int64
            description: 创建时间
`,
}
