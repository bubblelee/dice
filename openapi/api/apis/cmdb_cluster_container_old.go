package apis

var CMDB_CLUSTER_CONTAINER_OLD = ApiSpec{
	Path:        "/api/cmdb/container/<cluster>",
	BackendPath: "/api/dice/container/<cluster>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的所有容器信息",
}
