package apis

var ADMIN_ACTIVITIES_CURRENT = ApiSpec{
	Path:        "/api/admin/users/current/activities",
	BackendPath: "/api/users/current/activities",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前用户的活动",
}
