package apis

import (
	"net/http"
)

// 转换成 openapi.api.Spec，方便用户写的类型
type ApiSpec struct {
	Path           string
	BackendPath    string
	Method         string
	Host           string
	Scheme         string
	Custom         func(rw http.ResponseWriter, req *http.Request) // 如果是 websocket，没意义，在 generator 里检查
	CustomResponse func(*http.Response) error                      // 如果是 websocket，没意义，在 generator 里检查
	CheckLogin     bool
	CheckToken     bool
	CheckBasicAuth bool
	Doc            string
}
