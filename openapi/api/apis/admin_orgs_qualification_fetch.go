package apis

var ADMIN_ORG_APPLY_FETCH = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/qualification",
	BackendPath: "/api/orgs/<orgId>/qualification",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
