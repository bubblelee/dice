package apis

var ADMIN_APPLICATION_MEMBER_CHANGE_ROLE = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/members/<userId>",
	BackendPath: "/api/applications/<applicationId>/members/<userId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 更改成员角色",
}
