package apis

var CI_TASK = ApiSpec{
	Path:        "/api/ci/task/<taskId>",
	BackendPath: "/api/ci/task/<taskId>",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "PATCH",
	CheckLogin:  true,
	Doc:         "summary: ci update task",
}
