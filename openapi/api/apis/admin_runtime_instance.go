package apis

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/sirupsen/logrus"
)

var ADMIN_RUNTIME_INSTANCE_USAGE = ApiSpec{
	Path:       "/api/admin/applications/<applicationId>/runtimes/<runtimeId>/services/<serviceName>",
	Method:     "GET",
	Scheme:     "http",
	Custom:     getInstances,
	CheckLogin: true,
	Doc:        "summary: 获取服务实例列表",
}

func getInstances(rw http.ResponseWriter, req *http.Request) {
	// TODO URI param extraction fix
	params := strings.Split(req.RequestURI, "/")
	applicaitonId := params[4]
	runtimeId := params[6]
	serviceName := params[8]

	rw.Header().Set("Content-Type", "application/json")

	// Get instances from admin
	adminInstanceUri := fmt.Sprintf("/api/v2/projects/%s/runtimes/%s/services", applicaitonId, runtimeId)
	client := httpclient.New()
	var instances AdminInstances
	adminResponse, err := client.Get("console.dice.marathon.l4lb.thisdcos.directory:8080").
		Path(adminInstanceUri).
		Param("serviceName", serviceName).
		Do().
		JSON(&instances)
	if err != nil {
		logrus.Errorf("failed to handle request: %s (%v)", adminInstanceUri, err)
		errorResponse := fillResponse(false, nil, "CS0001", err.Error(), nil)
		errorBody, _ := json.Marshal(*errorResponse)
		rw.Write(errorBody)
		return
	}
	if adminResponse.StatusCode() != http.StatusOK {
		logrus.Errorf("get instance list error, code: %s", adminResponse.StatusCode())
		errorResponse := fillResponse(false, nil, "CS0001", "", nil)
		errorBody, _ := json.Marshal(*errorResponse)
		rw.Write(errorBody)
		return
	}

	// Get instances from CMDB
	var cmdbInstances CmdbInstances
	cmdbUrl := fmt.Sprintf("/api/clusters/%d/instances", instances.ClusterId)
	cmdbResponse, err := client.Get("cmdb.dice.marathon.l4lb.thisdcos.directory:9093").
		Path(cmdbUrl).
		Header("Accept", "application/vnd.dice+json; version=1.0").
		Header("Content-Type", "application/json").
		Param("type", "service").
		Param("runtime", runtimeId).
		Param("service", serviceName).
		Do().
		JSON(&cmdbInstances)
	if err != nil {
		logrus.Errorf("failed to handle request: %s (%v)", "/api/dice/containers", err)
		errorResponse := fillResponse(false, nil, "CS0001", err.Error(), nil)
		errorBody, _ := json.Marshal(*errorResponse)
		rw.Write(errorBody)
		return
	}
	if cmdbResponse.StatusCode() != http.StatusOK {
		logrus.Errorf("get cmdb instances error, code: %s", adminResponse.StatusCode())
		errorResponse := fillResponse(false, nil, "CS0001", "", nil)
		errorBody, _ := json.Marshal(*errorResponse)
		rw.Write(errorBody)
		return
	}

	// 填充ContainerId & Host IP
	for i := range instances.Runs {
		for _, cmdbInstance := range cmdbInstances.Result {
			if instances.Runs[i].IpAddress == cmdbInstance.IPAddress {
				instances.Runs[i].Host = cmdbInstance.HostPrivateIPAddr
				instances.Runs[i].ContainerId = cmdbInstance.ID
			}
		}
	}

	response := fillResponse(true, instances, "", "", nil)
	responseJson, err := json.Marshal(*response)
	if err != nil {
		logrus.Errorf("failed to marshal instances, error: %v", err)
		errorResponse := fillResponse(false, nil, "CS0001", err.Error(), nil)
		errorBody, _ := json.Marshal(*errorResponse)
		rw.Write(errorBody)
		return
	}

	rw.Write(responseJson)
}

func fillResponse(success bool, data interface{}, code string, msg string, ctx interface{}) *DiceResponse {
	if success {
		return &DiceResponse{
			Success: success,
			Data:    data,
		}
	} else {
		err := ErrorMsg{
			Code:    code,
			Msg:     msg,
			Context: ctx,
		}
		return &DiceResponse{
			Success: success,
			Data:    data,
			Err:     err,
		}
	}
}

type AdminInstances struct {
	Runs          []AdminInstance `json:"runs,omitempty"`
	CompletedRuns []AdminInstance `json:"completedRuns,omitempty"`
	ClusterId     int64           `json:"clusterId"`
}

type AdminInstance struct {
	Id          string `json:"id,omitempty"`          // Task Id
	ContainerId string `json:"containerId,omitempty"` // Container Id
	IpAddress   string `json:"ipAddress,omitempty"`
	Host        string `json:"host,omitempty"`
	Status      string `json:"status,omitempty"`
	StartedAt   string `json:"startedAt,omitempty"`
	UpdatedAt   string `json:"updatedAt,omitempty"`
}

type CmdbInstances struct {
	Result []CmdbInstance `json:"result"`
}

// TODO 后续json需调整为camelCase形式
type CmdbInstance struct {
	ID                  string  `json:"id"`                    // 容器ID
	Deleted             bool    `json:"deleted"`               // 资源是否被删除
	StartedAt           string  `json:"started_at"`            // 容器启动时间
	Cluster             string  `json:"cluster_full_name"`     // 集群名
	HostPrivateIPAddr   string  `json:"host_private_addr"`     // 宿主机内网地址
	IPAddress           string  `json:"ip_addr"`               // 容器IP地址
	Image               string  `json:"image_name"`            // 容器镜像名
	CPU                 float64 `json:"cpu"`                   // 分配的cpu
	Memory              int64   `json:"memory"`                // 分配的内存（字节）
	Disk                int64   `json:"disk"`                  // 分配的磁盘空间（字节）
	DiceOrg             string  `json:"dice_org"`              // 所在的组织
	DiceProject         string  `json:"dice_project"`          // 所在大项目
	DiceApplication     string  `json:"dice_application"`      // 所在项目
	DiceRuntime         string  `json:"dice_runtime"`          // 所在runtime
	DiceService         string  `json:"dice_service"`          // 所属应用
	DiceProjectName     string  `json:"dice_project_name"`     // 所在大项目名称
	DiceApplicationName string  `json:"dice_application_name"` // 所在项目
	DiceRuntimeName     string  `json:"dice_runtime_name"`     // 所在runtime
	DiceComponent       string  `json:"dice_component"`        // 组件名
	DiceAddon           string  `json:"dice_addon"`            // 中间件名
	Status              string  `json:"status"`                // 前期定义为docker状态（后期期望能表示服务状态）
	TimeStamp           int64   `json:"timestamp"`             // 消息本身的时间戳
}

type DiceResponse struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data,omitempty"`
	Err     ErrorMsg    `json:"err,omitempty"`
}
type ErrorMsg struct {
	Code    string      `json:"code,omitempty"`
	Msg     string      `json:"msg,omitempty"`
	Context interface{} `json:"ctx,omitempty"`
}
