package apis

var CMDB_HOST_USAGE_OLD_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/host",
	BackendPath: "/api/dice/usage/<cluster>/host",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下所有宿主机的资源使用情况",
}
