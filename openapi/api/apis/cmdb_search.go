package apis

var CMDB_CLUSTER_SEARCH = ApiSpec{
	Path:        "/api/cmdb/clusters/<cluster>/search",
	BackendPath: "/api/clusters/<cluster>/search",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc: `
summary: 提供指定集群下的关键字搜索功能
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
  - in: body
    name: keyword
    description: search key
    schema:
      type: object
      required:
        - keyword
      properties:
        keyword:
          type: string
          description: keyword 使用冒号进行分割，左key右value
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
      type: string
      example: 参考 https://yuque.antfin-inc.com/terminus_paas_dev/paas/gosn9b#zngglt
`,
}
