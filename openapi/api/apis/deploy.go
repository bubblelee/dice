package apis

var DEPLOY = ApiSpec{
	Path:        "/dice/deployments",
	BackendPath: "/api/deployments",
	Host:        "open.dice.marathon.l4lb.thisdcos.directory:8081",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc:         "summary: 部署应用(runtime)",
}
