package apis

var OPENAPI_VERIFY_CLIENT_TOKEN = ApiSpec{
	Path:        "/api/openapi/client-token",
	BackendPath: "/api/open-client/authorization",
	Host:        "uc.terminus.io",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  false,
	Doc: `
summary: 验证client token
description: 认证：通过 Authorization 头信息进行认证。 格式为“Bearer <token>”, 注意空格。openapi实际验证token不会用这个接口，而是直接调用用户中心的接口
`,
}
