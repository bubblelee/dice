package apis

var ADMIN_USER_UNLOCK = ApiSpec{
	Path:        "/api/admin/users/<userId>/unlock",
	BackendPath: "/api/users/<userId>/unlock",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
