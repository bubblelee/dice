package apis

var CMDB_CLUSTER_DEPLOY_DYNAMIC_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/dynamic/deploy",
	BackendPath: "/api/dice/overview/dynamic/deploy",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的部署动态",
}
