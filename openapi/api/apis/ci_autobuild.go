package apis

var CI_AUTOBUILD = ApiSpec{
	Path:        "/api/ci/outer/builds",
	BackendPath: "/api/ci/outer/builds",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "PATCH",
	CheckLogin:  false,
	Doc:         "summary: update ci task",
}
