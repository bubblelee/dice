package apis

var ADMIN_ORG_CONFIG = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/configs",
	BackendPath: "/api/orgs/<orgId>/configs",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
