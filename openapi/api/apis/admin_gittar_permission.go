package apis

var ADMIN_GITTAR_PERMISSION = ApiSpec{
	Path:        "/api/admin/gittar/permission/check",
	BackendPath: "/api/gittar/permission/check",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckBasicAuth: true,
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前用户的活动",
}
