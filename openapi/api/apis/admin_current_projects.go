package apis

var ADMIN_CURRENT_PROJECTS = ApiSpec{
	Path:        "/api/admin/users/current/projects",
	BackendPath: "/api/users/current/projects",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc: `
summary: list joined projects
produces:
  - application/json
responses:
  '200':
    description: list joined projects
    schema:
      type: array
      items:
        type: object
        properties:
          creator:
            type: string
          desc:
            type: string
          displayName:
            type: string
          id:
            type: integer
          logo:
            type: string
          name:
            type: string
          orgId:
            type: integer
          stats:
            type: object
            properties:
              countApplications:
                type: integer
              countMembers:
                type: integer
              countRuntimes:
                type: integer
              timeLastModified:
                type: string
                format: date-time
`,
}
