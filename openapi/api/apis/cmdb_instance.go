package apis

var CMDB_ONE_INSTANCE = ApiSpec{
	Path:        "/api/cmdb/clusters/<cluster>/instances/<instance>",
	BackendPath: "/api/clusters/<cluster>/instances/<instance>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc: `
summary: 获取某集群下的指定实例信息
parameters:
  - in: path
    name: cluster
    type: string
    required: true
    description: 集群名或者ID
  - in: path
    name: instance
    type: string
    required: true
    description: 实例ID
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
        type: object
        properties:
          id:
            type: string
            description: 容器ID
          deleted:
            type: boolean
            description: 资源是否被删除
          started_at:
            type: string
            description: 容器启动时间
          cluster_full_name:
            type: string
            description: 集群ID
          host_private_addr:
            type: string
            description: 宿主机内网地址
          ip_addr:
            type: string
            description: 容器IP地址
          image_name:
            type: string
            description: 容器镜像名
          cpu:
            type: number
            format: double
            description: 分配的cpu
          memory:
            type: integer
            format: int64
            description: 分配的内存（字节）
          disk:
            type: integer
            format: int64
            description: 分配的cpu
          dice_org:
            type: string
            description: 所在的组织
          dice_project:
            type: string
            description: 所在的项目
          dice_application:
            type: string
            description: 所在的应用
          dice_runtime:
            type: string
            description: 所在的runtime
          dice_service:
            type: string
            description: 所在的service
          dice_project_name:
            type: string
            description: 所在的项目名称
          dice_application_name:
            type: string
            description: 所在的应用名称
          dice_runtime_name:
            type: string
            description: 所在的runtime名称
          dice_component:
            type: string
            description: 组件名
          dice_addon:
            type: string
            description: 中间件名
          status:
            type: string
            description: 实例状态
          timestamp:
            type: integer
            format: int64
            description: 消息本身的时间戳
`,
}
