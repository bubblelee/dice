package apis

var ADMIN_MESSAGE_READ = ApiSpec{
	Path:        "/api/admin/messages/<messageId>/read",
	BackendPath: "/api/messages/<messageId>/read",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
