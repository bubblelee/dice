package apis

var ADMIN_PROJECT_WORKSPACE_FETCH = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/workspaces",
	BackendPath: "/api/group-projects/<projectId>/workspaces",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
