package apis

var ADMIN_USER_MESSAGE_COUNT = ApiSpec{
	Path:        "/api/admin/users/current/messages/unread",
	BackendPath: "/api/users/current/messages/unread",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
