package apis

var CMDB_CLUSTER_SEARCH_OLD = ApiSpec{
	Path:        "/api/cmdb/resource/<cluster>/search",
	BackendPath: "/api/dice/resource/<cluster>/search",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc:         "summary: 提供指定集群下的关键字搜索功能",
}
