package apis

import (
	"bytes"
	"fmt"
	"net/http"

	"terminus.io/dice/dice/openapi/auth"
	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/sirupsen/logrus"
	"io/ioutil"
)

var OPENAPI_NEW_CLIENT = ApiSpec{
	Path:   "/api/openapi/manager/clients",
	Scheme: "http",
	Method: "POST",
	Custom: func(rw http.ResponseWriter, req *http.Request) {
		token, err := auth.GetDiceClientToken()
		if err != nil {
			errStr := fmt.Sprintf("get token fail: %v", err)
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		var body bytes.Buffer
		rbody, _ := ioutil.ReadAll(req.Body)
		fmt.Printf("%+v\n", string(rbody))     // debug print
		fmt.Printf("uc: %+v\n", conf.UCAddr()) // debug print

		r, err := httpclient.New(httpclient.WithCompleteRedirect()).Post(conf.UCAddr()).Path("/api/open-client/manager/client").
			Header("Content-Type", "application/json").
			Header("Authorization", "Bearer "+token.AccessToken).RawBody(bytes.NewReader(rbody)).Do().Body(&body)
		if err != nil {
			errStr := fmt.Sprintf("new client fail: %v", err)
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		if !r.IsOK() {
			errStr := fmt.Sprintf("new client fail, statuscode: %d, body: %v", r.StatusCode(), body.String())
			logrus.Error(errStr)
			http.Error(rw, errStr, http.StatusForbidden)
			return
		}
		rw.Write([]byte(body.String()))
	},
	CheckLogin: false, // TODO:
	Doc: `
summary: 创建新client
description: 认证： 通过 Authorization 头信息进行认证。 格式为“Bearer <token>”, 注意空格
parameters:
  - in: body
    name: request-json
    description: request json body
    schema:
      type: object
      properties:
        accessTokenValiditySeconds:
          type: integer
        autoApprove:
          type: boolean
        clientId:
          type: string
        clientLogoUrl:
          type: string
        clientName:
          type: string
        clientSecret:
          type: string
        refreshTokenValiditySeconds:
          type: integer
        userId:
          type: int

produces:
  - application/json

responses:
  '200':
    description: OK
`,
}
