package apis

var ADMIN_ORG_LIST = ApiSpec{
	Path:        "/api/admin/orgs/all",
	BackendPath: "/api/orgs/all",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
