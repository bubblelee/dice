package apis

var CMDB_ONE_HOST_OLD = ApiSpec{
	Path:        "/api/cmdb/host/<cluster>/<host_ip>",
	BackendPath: "/api/dice/host/<cluster>/<host_ip>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的某个host信息",
}
