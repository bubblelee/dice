package apis

var ADMIN_ORG_UPDATE = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>",
	BackendPath: "/api/orgs/<orgId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
