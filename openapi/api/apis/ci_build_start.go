package apis

var CI_BUILD_START = ApiSpec{
	Path:        "/api/ci/builds/<buildId>/start",
	BackendPath: "/api/ci/builds/<buildId>/start",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	Doc:         "summary: ci build start",
}
