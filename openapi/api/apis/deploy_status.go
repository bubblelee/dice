package apis

var DEPLOY_STATUS = ApiSpec{
	Path:        "/dice/deployments/<deploymentID>",
	BackendPath: "/api/deployments/<deploymentID>",
	Host:        "open.dice.marathon.l4lb.thisdcos.directory:8081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 查询部署状态",
}
