package apis

var CMDB_CLUSTER_DEPLOY_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/deploy",
	BackendPath: "/api/dice/overview/deploy",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的部署总览信息",
}
