package apis

var ADMIN_ORG_MEMBER_REMOVE = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/members/<userId>",
	BackendPath: "/api/orgs/{orgId}/members/{userId}",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
