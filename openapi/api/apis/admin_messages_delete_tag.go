package apis

var ADMIN_MESSAGE_DELETE_TAG = ApiSpec{
	Path:        "/api/admin/messages/tags/<messageTag>",
	BackendPath: "/api/messages/tags/<messageTag>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
