package apis

var ADMIN_META_SITE = ApiSpec{
	Path:        "/api/admin/meta/site",
	BackendPath: "/api/meta/site",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
