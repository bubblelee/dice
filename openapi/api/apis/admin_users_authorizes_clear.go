package apis

var ADMIN_USER_AUTHORIZES_CLEAR = ApiSpec{
	Path:        "/api/admin/users/current/actions/clear-authorizes",
	BackendPath: "/api/users/current/actions/clear-authorizes",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
