package apis

var ADMIN_APPLICATION_RUNTIMES = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/runtimes",
	BackendPath: "/api/applications/<applicationId>/runtimes",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取application下的应用",
}
