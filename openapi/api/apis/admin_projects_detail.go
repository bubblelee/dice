package apis

var ADMIN_PROJECT_DETAIL = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>",
	BackendPath: "/api/group-projects/<projectId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
