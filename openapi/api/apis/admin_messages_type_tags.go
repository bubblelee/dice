package apis

var ADMIN_MESSAGE_TYPE_TAGS = ApiSpec{
	Path:        "/api/admin/messages/type/<messageType>/tags",
	BackendPath: "/api/messages/type/<messageType>/tags",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
