package apis

var CMDB_ORG_TASK_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/task/org/<orgId>",
	BackendPath: "/api/dice/overview/task/org/<orgId>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定org下的实例统计信息",
}
