package apis

var CI_BUILD_BRANCH = ApiSpec{
	Path:        "/api/ci/builds/branches",
	BackendPath: "/api/ci/builds/branches",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: ci websocket",
}
