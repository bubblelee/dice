package apis

var ADMIN_ACTIVITIES_RUNTIME = ApiSpec{
	Path:        "/api/admin/runtimes/<runtimeId>/activities",
	BackendPath: "/api/runtimes/<runtimeId>/activities",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前runtime的活动",
}
