package apis

var ADMIN_USER_CURRENT_ORG_CHANGE = ApiSpec{
	Path:        "/api/admin/users/current/actions/change-current-org",
	BackendPath: "/api/users/current/actions/change-current-org",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
