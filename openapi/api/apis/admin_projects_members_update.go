package apis

var ADMIN_PROJECT_MEMBERS_UPDATE = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/members/<userId>",
	BackendPath: "/api/group-projects/<projectId>/members/<userId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
