package apis

var CMDB_HOST_CONTAINER_OLD = ApiSpec{
	Path:        "/api/cmdb/host/<cluster>/<host_ip>/container",
	BackendPath: "/api/dice/host/<cluster>/<host_ip>/container",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的指定host上的所有容器信息",
}
