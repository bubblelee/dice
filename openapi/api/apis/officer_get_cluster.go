package apis

var OFFICER_GET_CLUSTER = ApiSpec{
	Path:        "/api/clusters/<idOrName>",
	BackendPath: "/api/clusters/<idOrName>",
	Method:      "GET",
	Host:        "dice.colony-officer.marathon.l4lb.thisdcos.directory:9029",
	Scheme:      "http",
	CheckLogin:  true,
	Doc:         "summary: 根据集群ID或名称获取集群详细信息",
}
