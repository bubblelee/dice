package apis

var CMDB_PROJECT_TASK_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/task/project/<project>",
	BackendPath: "/api/dice/overview/task/project/<project>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定project下的实例统计信息",
}
