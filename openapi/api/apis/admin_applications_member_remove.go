package apis

var ADMIN_APPLICATION_MEMBER_REMOVE = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/members/<userId>",
	BackendPath: "/api/applications/<applicationId>/members/<userId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 移除应用成员",
}
