package apis

var CI_BUILD_BP_UPDATE = ApiSpec{
	Path:        "/api/ci/builds/<buildId>/bpargs",
	BackendPath: "/api/ci/builds/<buildId>/bpargs",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "PATCH",
	CheckLogin:  true,
	Doc:         "summary: ci build update",
}
