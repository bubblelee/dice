package apis

var CMDB_CLUSTER_TASK_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/task",
	BackendPath: "/api/dice/overview/task",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的实例统计信息",
}
