package apis

var ADMIN_PROJECT_APPLICATIONS = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/applications",
	BackendPath: "/api/group-projects/<projectId>/applications",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc: `
summary: list applications belong to <projectId>
parameters:
  - in: path
    name: projectId
    type: integer
    required: true
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
      type: array
      items:
        type: object
        properties:
          config:
            type: object        # ??? 具体是啥
          desc:
            type: string
          id:
            type: integer
          logo:
            type: string
          name:
            type: string
          orgId:
            type: integer
          projectId:
            type: integer
          projectName:
            type: string
          repo:
            type: string
          stats:
            type: object
            properties:
              countApplications:
                type: integer
              countMembers:
                type: integer
              countRuntimes:
                type: integer
              timeLastModified:
                type: string
                format: date-time

  `,
}
