package apis

var CMDB_CLUSTER_COMPONENT_OLD = ApiSpec{
	Path:        "/api/cmdb/component/<cluster>",
	BackendPath: "/api/dice/component/<cluster>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下所有平台组件的容器信息",
}
