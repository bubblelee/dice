package apis

var CMDB_SERVICE_USAGE_OLD = ApiSpec{
	Path:        "/api/cmdb/usage/<cluster>/<project>/<application>/<runtime>/service",
	BackendPath: "/api/dice/usage/<cluster>/<project>/<application>/<runtime>/service",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定runtime下所有services的资源使用情况",
}
