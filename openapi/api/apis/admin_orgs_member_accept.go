package apis

var ADMIN_ORG_MEMBER_ACCEPT = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/members/<userId>/accept",
	BackendPath: "/api/orgs/<orgId>/members/<userId>/accept",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
