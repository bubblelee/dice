package apis

var ADMIN_ORG_MEMBER_SEARCH = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/members/search",
	BackendPath: "/api/orgs/<orgId>/members/search",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
