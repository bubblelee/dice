package apis

var ADMIN_APPLICATION_MEMBER_ADD = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>/members",
	BackendPath: "/api/applications/<applicationId>/members",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 列出application成员",
}
