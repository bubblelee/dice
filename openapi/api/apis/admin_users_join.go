package apis

var ADMIN_USER_JOIN = ApiSpec{
	Path:        "/api/admin/users/current/orgs/<orgId>/apply",
	BackendPath: "/api/users/current/orgs/<orgId>/apply",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
