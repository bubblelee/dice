package apis

var ADMIN_ORG_CONFIG_UPDATE = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/configs/builder",
	BackendPath: "/api/orgs/<orgId>/configs/builder",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
