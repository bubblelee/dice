package apis

var FakeWS = ApiSpec{
	Path:        "/<company>/<project>/test/ws",
	BackendPath: "/ws",
	Host:        "127.0.0.1:8080",
	Scheme:      "ws",
	Doc:         "summary: test websocket",
}
