package apis

var ADMIN_PROJECT_MEMBERS_ADD = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/members",
	BackendPath: "/api/group-projects/<projectId>/members",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
