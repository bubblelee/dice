package apis

var CMDB_ORG_DEPLOY_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/deploy/org/<orgId>",
	BackendPath: "/api/dice/overview/deploy/org/<orgId>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定org下的部署总览信息",
}
