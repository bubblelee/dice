package apis

var ADMIN_PROJECT_WORKSPACE_APPLICATIONS = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/applications",
	BackendPath: "/api/group-projects/<projectId>/applications",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
