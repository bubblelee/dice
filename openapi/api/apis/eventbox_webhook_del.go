package apis

var EVENTBOX_WEBHOOK_DEL = ApiSpec{
	Path:        "/api/eventbox/webhook",
	BackendPath: "/api/dice/eventbox/register",
	Host:        "diceeventbox.marathon.l4lb.thisdcos.directory:9528",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	Doc: `
summary: delete webhook
consumes:
  - application/json
parameters:
  - in: body
    description: webhook to delete
    schema:
      type: object
      properties:
        key:
          type: string
responses:
  '200':
    description: ok
  '400':
    description: bad request
`,
}
