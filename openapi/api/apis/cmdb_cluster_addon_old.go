package apis

var CMDB_CLUSTER_ADDON_OLD = ApiSpec{
	Path:        "/api/cmdb/addon/<cluster>",
	BackendPath: "/api/dice/addon/<cluster>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下所有addons的容器信息",
}
