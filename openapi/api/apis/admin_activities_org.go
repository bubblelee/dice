package apis

var ADMIN_ACTIVITIES_ORG = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/activities",
	BackendPath: "/api/orgs/<orgId>/activities",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前组织的活动",
}
