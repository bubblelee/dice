package apis

var CI_BUILD_RECORDS = ApiSpec{
	Path:        "/api/ci/builds",
	BackendPath: "/api/ci/builds",
	Host:        "devops.ci.marathon.l4lb.thisdcos.directory:3081",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: ci build records",
}
