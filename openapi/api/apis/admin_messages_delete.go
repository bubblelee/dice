package apis

var ADMIN_MESSAGE_DELETE = ApiSpec{
	Path:        "/api/admin/messages/<messageId>",
	BackendPath: "/api/messages/tags/<messageId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
