package apis

var ADMIN_ORG_MEMBER_CLUSTERS_DETAIL = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/clusters/<clusterId>",
	BackendPath: "/api/orgs/<orgId>/clusters/<clusterId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
