package apis

import (
	"encoding/json"
	"net/http"

	"terminus.io/dice/dice/openapi/auth"
)

var OPENAPI_GEN_CLIENT_TOKEN = ApiSpec{
	Path:       "/api/openapi/client-token",
	Scheme:     "http",
	Method:     "POST",
	CheckLogin: false,
	Custom: func(rw http.ResponseWriter, req *http.Request) {
		basic := req.Header.Get("Authorization")
		if basic == "" {
			http.Error(rw, "not provide Basic Authorization header", http.StatusBadRequest)
			return
		}

		oauthToken, err := auth.GenClientToken(basic)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
		res, err := json.Marshal(oauthToken)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusForbidden)
			return
		}
		rw.Header().Set("Content-Type", "application/json")
		rw.Write(res)
	},
	Doc: `
summary: client token 发放接口
description: 通过 header Basic 认证. Basic Header："Basic " + base64(<clientid>+":"+<clientsecret>)
produces:
  - application/json
responses:
  '200':
    description: OK
    schema:
      type: object
      properties:
        access_token:
          type: string
        token_type:
          type: string
        refresh_token:
          type: string
        expires_in:
          type: int64
        scope:
          type: string
        jti:
          type: string

  '400':
    description: 没有提供 Authorization header`,
}
