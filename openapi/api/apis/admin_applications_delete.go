package apis

var ADMIN_APPLICATION_DELETE = ApiSpec{
	Path:        "/api/admin/applications/<applicationId>",
	BackendPath: "/api/applications/<applicationId>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取组织",
}
