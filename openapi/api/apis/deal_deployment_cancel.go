package apis

var DEAL_DEPLOYMENT_CANCEL = ApiSpec{
	Path:        "/api/deal/deployments/<deploymentId>/actions/cancel",
	BackendPath: "/api/deployments/cancel",
	Host:        "open.dice.marathon.l4lb.thisdcos.directory:8081",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  false,
	Doc:         "summary: 取消部署",
}
