package apis

var ADMIN_IMAGE_SEARCH = ApiSpec{
	Path:        "/api/admin/images/searches",
	BackendPath: "/api/images/searches",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
