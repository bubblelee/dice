package apis

var ADMIN_USER_SUGGEST = ApiSpec{
	Path:        "/api/admin/users/suggest",
	BackendPath: "/api/users/suggest",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
