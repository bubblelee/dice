package apis

var ADMIN_USER_JOIN_LEAVE = ApiSpec{
	Path:        "/api/admin/users/current/orgs/<orgId>/leave",
	BackendPath: "/api/users/current/orgs/<orgId>/leave",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "DELETE",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
