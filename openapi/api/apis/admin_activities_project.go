package apis

var ADMIN_ACTIVITIES_PROJECT = ApiSpec{
	Path:        "/api/admin/projects/<projectId>/activities",
	BackendPath: "/api/group-projects/<projectId>/activities",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取当前项目的活动",
}
