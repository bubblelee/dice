package apis

var ADMIN_PROJECT_WORKSPACE_UPDATE = ApiSpec{
	Path:        "/api/admin/group-projects/<projectId>/workspaces/<workspace>",
	BackendPath: "/api/group-projects/<projectId>/workspaces/<workspace>",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "PUT",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 获取消息",
}
