package apis

var ADMIN_ORG_AUDIT_POST = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/qualification/audit",
	BackendPath: "/api/orgs/<orgId>/qualification/audit",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
