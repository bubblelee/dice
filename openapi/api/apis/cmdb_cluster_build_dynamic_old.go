package apis

var CMDB_CLUSTER_BUILD_DYNAMIC_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/dynamic/build",
	BackendPath: "/api/dice/overview/dynamic/build",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定集群下的构建动态",
}
