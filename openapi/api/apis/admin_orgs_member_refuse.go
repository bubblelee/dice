package apis

var ADMIN_ORG_MEMBER_REFUSE = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/members/<userId>/refuse",
	BackendPath: "/api/orgs/<orgId>/members/<userId>/refuse",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
