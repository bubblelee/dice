package apis

var OFFICER_GET_ORG_CLUSTERS = ApiSpec{
	Path:        "/api/clusters",
	BackendPath: "/api/clusters",
	Method:      "GET",
	Host:        "dice.colony-officer.marathon.l4lb.thisdcos.directory:9029",
	Scheme:      "http",
	CheckLogin:  true,
	Doc:         "summary: 根据组织ID获取所属于该组织的集群列表",
}
