package apis

var CMDB_PROJECT_DEPLOY_DYNAMIC_OLD = ApiSpec{
	Path:        "/api/cmdb/overview/dynamic/deploy/project/<project>",
	BackendPath: "/api/dice/overview/dynamic/deploy/project/<project>",
	Host:        "cmdb.dice.marathon.l4lb.thisdcos.directory:9093",
	Scheme:      "http",
	Method:      "GET",
	CheckLogin:  true,
	Doc:         "summary: 获取指定project下的部署动态",
}
