package apis

var ADMIN_ORG_LOCK = ApiSpec{
	Path:        "/api/admin/orgs/<orgId>/lock",
	BackendPath: "/api/orgs/<orgId>/lock",
	Host:        "console.dice.marathon.l4lb.thisdcos.directory:8080",
	Scheme:      "http",
	Method:      "POST",
	CheckLogin:  true,
	CheckToken:  true,
	Doc:         "summary: 创建组织",
}
