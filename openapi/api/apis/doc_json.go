package apis

import (
	"encoding/json"
	"net/http"
	"os"
	"sync"

	yaml "gopkg.in/yaml.v2"

	"github.com/sirupsen/logrus"
)

var DOC_JSON = ApiSpec{
	Path:   "/api/openapi/swagger.json",
	Method: "GET",
	Scheme: "http",
	Custom: getDocJSON,
	Doc:    `summary: 返回 swagger.json`,
}

var (
	swaggerJSON     []byte
	swaggerJSONLock sync.Once
)

func getSwaggerJSON() []byte {
	swaggerJSONLock.Do(func() {
		f, err := os.Open("./swagger.yml")
		defer f.Close()
		if err != nil {
			logrus.Errorf("getSwaggerJSON: %v", err)
		}
		var v interface{}
		if err := yaml.NewDecoder(f).Decode(&v); err != nil {
			logrus.Errorf("getSwaggerJSON: %v", err)
		}
		v = convert(v)
		j, err := json.Marshal(v)
		if err != nil {
			logrus.Errorf("getSwaggerJSON: %v", err)
		}
		swaggerJSON = j
	})
	return swaggerJSON
}
func getDocJSON(rw http.ResponseWriter, req *http.Request) {
	j := getSwaggerJSON()
	rw.Header().Set("Content-Type", "application/json")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.WriteHeader(200)
	rw.Write(j)
}

func convert(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = convert(v)
		}
		return m2
	case []interface{}:
		for i, v := range x {
			x[i] = convert(v)
		}
	}
	return i
}
