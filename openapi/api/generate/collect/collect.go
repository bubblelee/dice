package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io"
	"os"
	"sort"
	"strings"
)

func main() {
	fmt.Println("generating collectAPIs.go")
	output, err := os.OpenFile("collectAPIs.go", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("generating collectAPIs.go fail: %v", err)
			os.Remove("collectAPIs.go")
		}
	}()
	if err != nil {
		panic(err)
	}
	pkgs := parseDir("../apis/")
	specs := []string{}
	for _, pkg := range pkgs {
		for fname, f := range pkg.Files {
			spec := parseFile(fname, f)
			specs = append(specs, spec)
		}
	}
	// make specs in stable order
	sort.Strings(specs)

	io.WriteString(output, "package main\n")
	io.WriteString(output, imports())
	writeSpecs(output, specs)
	writeSpecNames(output, specs)
}

func parseDir(path string) map[string]*ast.Package {
	fs := token.NewFileSet()
	pkgs, err := parser.ParseDir(fs, path, func(info os.FileInfo) bool {
		if info.Name() == "rawspec.go" {
			return false
		}
		if strings.HasSuffix(info.Name(), "_test.go") {
			return false
		}
		return true
	}, 0)
	if err != nil {
		panic(err)
	}
	return pkgs
}

func parseFile(fname string, f *ast.File) string {
	for _, decl := range f.Decls {
		gen, ok := decl.(*ast.GenDecl)
		if !ok {
			continue
		}
		if gen.Tok != token.VAR {
			continue
		}
		for _, spec := range gen.Specs {
			val, ok := spec.(*ast.ValueSpec)
			if !ok {
				continue
			}
			for idx, v := range val.Values {
				v_, ok := v.(*ast.CompositeLit)
				if !ok {
					continue
				}
				ident, ok := v_.Type.(*ast.Ident)
				if !ok {
					continue
				}
				if ident.Name != "ApiSpec" {
					continue
				}
				name := val.Names[idx].Name
				if strings.Title(name) != name {
					errStr := fmt.Sprintf("first letter should be uppercase to output it, [%s]", name)
					panic(errStr)
				}
				return name
			}
		}
	}
	errStr := fmt.Sprintf("not found ApiSpec in %s", fname)
	panic(errStr)
}

func imports() string {
	return `
import (
        . "terminus.io/dice/dice/openapi/api/apis"
)
`
}

func writeSpecs(w io.Writer, specs []string) {
	io.WriteString(w, `
var APIs = []ApiSpec{
`)
	defer io.WriteString(w, "}")
	for _, spec := range specs {
		io.WriteString(w, tab(1, spec))
		io.WriteString(w, ",\n")
	}

}

func writeSpecNames(w io.Writer, specs []string) {
	io.WriteString(w, `
var APINames = []string{
`)
	defer io.WriteString(w, "}")
	for _, spec := range specs {
		io.WriteString(w, tab(1, "\""+spec+"\""))
		io.WriteString(w, ",\n")
	}
}
func tab(n int, content string) string {
	var buf strings.Builder
	for i := 0; i < n; i++ {
		buf.WriteString("	")
	}
	buf.WriteString(content)
	return buf.String()
}
