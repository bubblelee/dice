package main

import (
	"fmt"
	"os"
	"strings"
	"text/template"

	tapis "terminus.io/dice/dice/openapi/api/apis"
)

var docTemplate = template.Must(template.New("spec").Parse(`swagger: "2.0"
info:
  title: OpenAPI
  description: OpenAPI 文档. 各个 API 的文档现在只需要写 Operation Object (见[swagger_spec](https://swagger.io/docs/specification/2-0/basic-structure/)
  version: "1.0"

paths:
{{- range $_, $v := .APIs}}
{{call $.Indent 1 $v.Path}}:
{{- range $_, $m := $v.Methods}}
{{call $.Indent 2 $m.Method}}:
{{call $.Indent 3 $m.Content}}
{{- end}}
{{- end}}
`))

type Method struct {
	Method  string
	Content string
}

type API struct {
	Path    string
	Methods []Method
}

func generateDoc() {
	apis := []API{}
	for _, api := range APIs {
		// FIXME : skip websocket now
		if api.Method == "" {
			continue
		}

		if err := validateDoc(api); err != nil {
			fmt.Printf("[Warning] %v\n", err)
		}

		idx := existPath(api, apis)
		if idx != -1 {
			apis[idx].Methods = append(apis[idx].Methods, Method{
				Method:  strings.ToLower(api.Method),
				Content: strings.TrimSpace(api.Doc),
			})
		} else {
			apis = append(apis, API{
				Path: replaceAngleBracket(api.Path),
				Methods: []Method{
					{Method: strings.ToLower(api.Method), Content: strings.TrimSpace(api.Doc)},
				},
			})
		}
	}
	var buf strings.Builder
	docTemplate.Execute(&buf, map[string]interface{}{
		"APIs":   apis,
		"Indent": indent,
	})
	docf, err := os.OpenFile("../swagger.yml", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}

	docf.WriteString(buf.String())
}

func replaceAngleBracket(path string) string {
	v1 := strings.Replace(path, "<", "{", -1)
	v2 := strings.Replace(v1, ">", "}", -1)
	return v2
}

func indent(n int, s string) string {
	space := ""
	for i := 0; i < n; i++ {
		space += "  "
	}
	return space + strings.Replace(s, "\n", "\n"+space, -1)
}

func convert(i interface{}) interface{} {
	switch x := i.(type) {
	case map[interface{}]interface{}:
		m2 := map[string]interface{}{}
		for k, v := range x {
			m2[k.(string)] = convert(v)
		}
		return m2
	case []interface{}:
		for i, v := range x {
			x[i] = convert(v)
		}
	}
	return i
}

// 存在Path相同，但是Method不同的API，
// 需要把它们写在一起，而不是分开，不然swagger会只显示一个
func existPath(api tapis.ApiSpec, apis []API) int {
	for idx, i := range apis {
		if i.Path == replaceAngleBracket(api.Path) {
			return idx
		}
	}
	return -1
}
