package main

import (
	"fmt"
	"io"
	"os"
	"strings"
	"text/template"
)

//go:generate go run collect/collect.go
//go:generate go run generate.go validate.go generate_doc.go collectAPIs.go
func main() {
	fmt.Println("generating api.go")
	fmt.Println("generating swagger.yml")

	var buf strings.Builder
	trivialBegin(&buf)
	for idx, api := range APIs {
		if err := validate(&api); err != nil {
			errStr := fmt.Sprintf("validate fail[%s]: %v", api.Path, err)
			panic(errStr)
		}
		SpecTemplate.Execute(&buf, map[string]interface{}{
			"Path":           quote(api.Path),
			"BackendPath":    quote(api.BackendPath),
			"Host":           quote(api.Host),
			"Method":         quote(strings.ToUpper(api.Method)),
			"Scheme":         strings.ToUpper(api.Scheme),
			"Custom":         APINames[idx] + ".Custom",
			"CustomResponse": APINames[idx] + ".CustomResponse",
			"CheckLogin":     api.CheckLogin,
			"CheckToken":     api.CheckToken,
			"CheckBasicAuth": api.CheckBasicAuth,
		})
	}
	trivialEnd(&buf)
	f, err := os.OpenFile("../api.go", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	f.WriteString(buf.String())
	generateDoc()
}

var SpecTemplate = template.Must(template.New("spec").Parse(`	{NewPath({{.Path}}), NewPath({{.BackendPath}}), {{.Host}}, {{.Scheme}}, {{.Method}}, {{.Custom}}, {{.CustomResponse}}, {{.CheckLogin}}, {{.CheckToken}}, {{.CheckBasicAuth}}},
`))

func trivialBegin(w io.Writer) {
	io.WriteString(w, "//generated file, DO NOT EDIT\n")
	io.WriteString(w, "package api\n\n")
	io.WriteString(w, `
import (
        . "terminus.io/dice/dice/openapi/api/apis"
        . "terminus.io/dice/dice/openapi/api/spec"
)
`)
	io.WriteString(w, "var API APIs = APIs{\n")
}

func trivialEnd(w io.Writer) {
	io.WriteString(w, "}")
}

func quote(s string) string {
	return "\"" + s + "\""
}

func method(m string) string {
	if m == "" {
		return "ALL"
	}
	return m
}
