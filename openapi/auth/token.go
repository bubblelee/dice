package auth

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/pkg/httpclient"
	"terminus.io/dice/dice/pkg/jsonstore"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var diceClientToken *OAuthToken
var diceClientTokenExpireTime = time.Now()

// 获取 dice 自己的token
func GetDiceClientToken() (OAuthToken, error) {
	if diceClientToken != nil && diceClientTokenExpireTime.After(time.Now().Add(10*time.Second)) {
		return *diceClientToken, nil
	}

	basic := "Basic " + base64.StdEncoding.EncodeToString([]byte(conf.UCClientID()+":"+conf.UCClientSecret()))
	token, err := GenClientToken(basic)
	if err != nil {
		return OAuthToken{}, err
	}
	diceClientToken = &token
	diceClientTokenExpireTime = time.Now().Add(time.Duration(token.ExpiresIn) * time.Second)
	logrus.Infof("dicetoken: %+v", *diceClientToken)
	return *diceClientToken, nil
}

func GenClientToken(basic string) (OAuthToken, error) {
	formBody := make(url.Values)
	formBody.Set("grant_type", "client_credentials")
	var body bytes.Buffer
	r, err := httpclient.New(httpclient.WithCompleteRedirect()).Post(conf.UCAddr()).Path("/oauth/token").
		Header("Authorization", basic).
		FormBody(formBody).Do().Body(&body)
	if err != nil {
		err = errors.Wrap(err, "getClientToken")
		logrus.Error(err)
		return OAuthToken{}, err
	}
	if !r.IsOK() {
		err = fmt.Errorf("getClientToken: statuscode: %d, body: %v", r.StatusCode(), body.String())
		logrus.Error(err)
		return OAuthToken{}, err
	}
	var oauthToken OAuthToken
	d := json.NewDecoder(&body)
	if err := d.Decode(&oauthToken); err != nil {
		buffered := d.Buffered()
		err = fmt.Errorf("getClientToken: %v, buffer: %+v", err, buffered)
		logrus.Error(err)
		return OAuthToken{}, err
	}
	return oauthToken, nil
}

type TokenClient struct {
	ID         int    `json:"id"`
	ClientID   string `json:"clientId"`
	ClientName string `json:"clientName"`
}

// cache token in openapi to reduce request frequence to uc
var clientTokenCache, _ = jsonstore.New(jsonstore.UseMemStore(), jsonstore.UseTimeoutStore(60))

// @return example:
// {"id":7,"userId":null,"clientId":"dice-test","clientName":"dice测试应用","clientLogoUrl":null,"clientSecret":null,"autoApprove":false,"scope":["public_profile","email"],"resourceIds":["shinda-maru"],"authorizedGrantTypes":["client_credentials"],"registeredRedirectUris":[],"autoApproveScopes":[],"authorities":["ROLE_CLIENT"],"accessTokenValiditySeconds":433200,"refreshTokenValiditySeconds":433200,"additionalInformation":{}}
func VerifyClientToken(token string) (TokenClient, error) {
	var result TokenClient
	if err := clientTokenCache.Get(context.Background(), token, &result); err != nil {
		var body bytes.Buffer
		r, err := httpclient.New(httpclient.WithCompleteRedirect()).Get(conf.UCAddr()).Path("/api/open-client/authorization").Header("Authorization", "Bearer "+token).Do().Body(&body)
		if err != nil {
			return TokenClient{}, err
		}
		if !r.IsOK() {
			return TokenClient{}, fmt.Errorf("verify token: statuscode: %d, body: %v", r.StatusCode(), body.String())
		}
		d := json.NewDecoder(&body)
		if err := d.Decode(&result); err != nil {
			logrus.Errorf("VerifyClientToken: %v, buffered: %v", err, d.Buffered())
			return TokenClient{}, err
		}
		if err := clientTokenCache.Put(context.Background(), token, result); err != nil {
			return TokenClient{}, err
		}
		return result, nil
	}
	return result, nil
}
