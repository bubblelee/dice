package auth

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"terminus.io/dice/dice/openapi/api/spec"
	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/openapi/monitor"

	"github.com/go-redis/redis"
)

type Auth struct {
	RedisCli *redis.Client
}

func NewAuth() (*Auth, error) {
	RedisCli := redis.NewClient(&redis.Options{
		Addr:     conf.RedisAddr(),
		Password: conf.RedisPwd(),
	})
	if _, err := RedisCli.Ping().Result(); err != nil {
		return nil, err
	}
	return &Auth{RedisCli: RedisCli}, nil
}

func (a *Auth) Auth(spec *spec.Spec, req *http.Request) error {
	var err error
	defer func() {
		if err != nil {
			monitor.Notify(monitor.MonitorInfo{
				Tp: monitor.AuthFail, Detail: err.Error(),
			})
		}
		monitor.Notify(monitor.MonitorInfo{
			Tp: monitor.AuthSucc, Detail: "",
		})
	}()
	var t checkType
	t, err = whichCheck(req, spec)
	if err != nil {
		return err
	}
	switch t {
	case NONE:
		break
	case LOGIN:
		user := NewUser(a.RedisCli)
		if err = a.checkLogin(req, user); err != nil {
			return err
		}
		if err := setUserInfoHeaders(req, user); err != nil {
			return err
		}
	case BASICAUTH:
		user := NewUser(a.RedisCli)
		if err := a.checkBasicAuth(req, user); err != nil {
			return err
		}
		if err := setUserInfoHeaders(req, user); err != nil {
			return err
		}

	case TOKEN:
		var client TokenClient
		client, err = a.checkToken(req)
		if err != nil {
			return err
		}
		req.Header.Set("Client-ID", client.ClientID)
		req.Header.Set("Client-Name", client.ClientName)
	}
	return err
}

func setUserInfoHeaders(req *http.Request, user *User) error {
	var userinfo UserInfo
	var err error
	userinfo, err = user.GetInfo(req)
	if err != nil {
		return err
	}
	var scopeinfo ScopeInfo
	scopeinfo, err = user.GetScopeInfo(req)
	if err != nil {
		return err
	}
	userInfoBytes, err := json.Marshal(userinfo)
	if err != nil {
		return err
	}
	userInfoBase64 := base64.StdEncoding.EncodeToString(userInfoBytes)
	// set User-ID and Org-ID, maybe Previleged
	req.Header.Set("User-ID", strconv.Itoa(userinfo.ID))
	req.Header.Set("Org-ID", strconv.Itoa(scopeinfo.OrgID))
	req.Header.Set("User-Info", userInfoBase64)
	for _, k := range scopeinfo.Authorizes {
		if k.Key == "systemManage" {
			req.Header.Set("Previleged", "true")
			break
		}
	}

	if _, err := req.Cookie(SessionIDCookieName); err != nil {
		req.AddCookie(&http.Cookie{Name: SessionIDCookieName, Value: user.sessionID})
	}
	return nil
}

type checkType int

const (
	LOGIN     checkType = iota
	BASICAUTH
	TOKEN
	NONE
)

func whichCheck(req *http.Request, spec *spec.Spec) (checkType, error) {
	session, err := req.Cookie(SessionIDCookieName)
	if spec.CheckLogin && session != nil && err == nil {
		return LOGIN, nil
	}
	auth := req.Header.Get("Authorization")
	if spec.CheckBasicAuth && strings.HasPrefix(auth, "Basic ") {
		return BASICAUTH, nil
	}
	if spec.CheckToken && auth != "" {
		return TOKEN, nil
	}
	if !spec.CheckToken && !spec.CheckLogin {
		return NONE, nil
	}
	return NONE, errors.New("lack of required auth header")
}

func (a *Auth) checkLogin(req *http.Request, user *User) error {
	return user.IsLogin(req)
}

func (a *Auth) checkToken(req *http.Request) (TokenClient, error) {
	return VerifyClientToken(req.Header.Get("Authorization"))
}

func (a *Auth) checkBasicAuth(req *http.Request, user *User) error {
	auth := req.Header.Get("Authorization")
	auth = strings.TrimPrefix(auth, "Basic ")
	userNameAndPwd, err := base64.StdEncoding.DecodeString(auth)
	if err != nil {
		return fmt.Errorf("checkBasicAuth: decode base64 fail: %v", err)
	}
	splitted := strings.SplitN(string(userNameAndPwd), ":", 2)
	if len(splitted) != 2 {
		return fmt.Errorf("checkBasicAuth: split username and password fail: %v", userNameAndPwd)
	}
	_, err = user.PwdLogin(splitted[0], splitted[1])
	return err
}
