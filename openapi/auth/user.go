package auth

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"terminus.io/dice/dice/openapi/conf"
	"terminus.io/dice/dice/pkg/httpclient"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

type GetUserState int
type SetUserState int

const (
	GetInit      GetUserState = iota
	GotSessionID
	GotToken
	GotInfo
	GotScopeInfo
)

const (
	SetInit      SetUserState = iota
	SetSessionID
)

const (
	SessionIDCookieName = "OPENAPISESSION"
)

var (
	ErrNotExist = errors.New("session not exist")
)

type ScopeInfo struct {
	OrgID int `json:"orgId"`
	Authorizes []struct {
		TargetID string `json:"targetId"`
		Key      string `json:"key"`
	} `json:"authorizes"`
	// dont care other fields
}

type OAuthToken struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	Scope        string `json:"scope"`
	Jti          string `json:"jti"`
}
type UserInfo struct {
	ID               int    `json:"id"`
	Email            string `json:"email"`
	EmailExist       bool   `json:"emailExist"`
	PasswordExist    bool   `json:"passwordExist"`
	PhoneExist       bool   `json:"phoneExist"`
	Birthday         string `json:"birthday"`
	PasswordStrength int    `json:"passwordStrength"`
	Phone            string `json:"phone"`
	AvatarUrl        string `json:"avatarUrl"`
	UserName         string `json:"username"`
	NickName         string `json:"nickName"`
	Enabled          bool   `json:"enabled"`
	CreatedAt        string `json:"createdAt"`
	UpdatedAt        string `json:"updatedAt"`
	LastLoginAt      string `json:"lastLoginAt"`
}

type User struct {
	sessionID string
	token     string
	info      UserInfo
	scopeInfo ScopeInfo
	state     GetUserState
	redisCli  *redis.Client
}

func NewUser(redisCli *redis.Client) *User {
	return &User{state: GetInit, redisCli: redisCli}
}

func (u *User) get(req *http.Request, state GetUserState) (interface{}, error) {
	switch u.state {
	case GetInit:
		logrus.Info(req.Cookies())
		session, err := req.Cookie(SessionIDCookieName)
		if err != nil {
			return nil, errors.Wrap(err, "User:GetInfo:Init")
		}
		u.sessionID = session.Value
		logrus.Infof("session value: %v", u.sessionID)
		u.state = GotSessionID
		fallthrough
	case GotSessionID:
		token, err := u.redisCli.Get(mkSessionKey(u.sessionID)).Result()
		if err == redis.Nil {
			return nil, errors.Wrap(ErrNotExist, "User:GetInfo:GotSessionID:not exist: "+u.sessionID)
		} else if err != nil {
			return nil, errors.Wrap(err, "User:GetInfo:GotSessionID")
		}
		u.token = token
		u.state = GotToken
		fallthrough
	case GotToken:
		if state == GotToken {
			return nil, nil
		}
		info, err := getInfo(u.token)
		if err != nil {
			return nil, err
		}
		u.info = info
		u.state = GotInfo
		fallthrough
	case GotInfo:
		if state == GotInfo {
			return u.info, nil
		}
		var body bytes.Buffer
		userInfoByte, err := json.Marshal(u.info)
		userInfoBase64 := base64.StdEncoding.EncodeToString(userInfoByte)
		if err != nil {
			return nil, err
		}
		r, err := httpclient.New().Get(conf.AdminCurrentUserHost()).
			Path("/api/users/current").Header("User-Info", userInfoBase64).
			Cookie(&http.Cookie{Name: "OPENAPISESSION", Value: u.sessionID}).Do().
			Body(&body)
		if err != nil {
			return nil, err
		}
		if !r.IsOK() {
			return nil, fmt.Errorf("GetScopeInfo: statuscode %d, body: %v", r.StatusCode(), body.String())
		}
		var scopeinfo ScopeInfo
		if err := json.Unmarshal([]byte(body.String()), &scopeinfo); err != nil {
			return nil, fmt.Errorf("GetScopeInfo: unmarshal failed: %v", err)
		}
		u.scopeInfo = scopeinfo
		u.state = GotScopeInfo
		fallthrough
	case GotScopeInfo:
		if state == GotScopeInfo {
			return u.scopeInfo, nil
		}
	}
	panic("should not be here")
}

func (u *User) IsLogin(req *http.Request) error {
	_, err := u.get(req, GotToken)
	return err
}

// 获取用户信息
func (u *User) GetInfo(req *http.Request) (UserInfo, error) {
	info, err := u.get(req, GotInfo)
	if err != nil {
		return UserInfo{}, err
	}
	return info.(UserInfo), nil
}

// 获取用户orgID
func (u *User) GetScopeInfo(req *http.Request) (ScopeInfo, error) {
	scopeinfo, err := u.get(req, GotScopeInfo)
	if err != nil {
		return ScopeInfo{}, err
	}
	return scopeinfo.(ScopeInfo), nil
}

func (u *User) Login(uccode string, isHTTPS bool) (string, error) {
	basic := "Basic " + base64.StdEncoding.EncodeToString([]byte(conf.UCClientID()+":"+conf.UCClientSecret()))
	formBody := make(url.Values)
	formBody.Set("grant_type", "authorization_code")
	formBody.Set("code", uccode)
	proto := "http://"
	if isHTTPS {
		proto = "https://"
	}
	formBody.Set("redirect_uri", proto+conf.UCRedirectHost()+"/logincb")
	var body bytes.Buffer
	r, err := httpclient.New(httpclient.WithCompleteRedirect()).Post(conf.UCAddr()).Path("/oauth/token").
		Header("Authorization", basic).
		FormBody(formBody).Do().Body(&body)
	if err != nil {
		err_ := errors.Wrap(err, "login: post /oauth/token fail")
		logrus.Error(err_)
		return "", err_
	}
	if !r.IsOK() {
		err_ := fmt.Errorf("login: /oauth/token statuscode: %d, body: %v", r.StatusCode(), body.String())
		logrus.Error(err_)
		return "", err_
	}
	var oauthToken OAuthToken
	if err := json.NewDecoder(&body).Decode(&oauthToken); err != nil {
		err_ := errors.Wrap(err, "login: decode fail")
		logrus.Error(err_)
		return "", err_
	}
	u.token = oauthToken.AccessToken
	userInfo, err := getInfo(u.token)
	if err != nil {
		return "", err
	}
	u.info = userInfo
	u.state = GotInfo
	if err := u.storeSession(oauthToken.AccessToken); err != nil {
		err_ := errors.Wrap(err, "login: storeSession fail")
		logrus.Error(err_)
		return "", err_
	}
	return u.sessionID, nil
}

func (u *User) PwdLogin(username, password string) (string, error) {
	basic := "Basic " + base64.StdEncoding.EncodeToString([]byte(conf.UCClientID()+":"+conf.UCClientSecret()))
	formBody := make(url.Values)
	formBody.Set("grant_type", "password")
	formBody.Set("username", username)
	formBody.Set("password", password)
	formBody.Set("scope", "public_profile")
	var body bytes.Buffer
	r, err := httpclient.New(httpclient.WithCompleteRedirect()).Post(conf.UCAddr()).Path("/oauth/token").
		Header("Authorization", basic).
		FormBody(formBody).Do().Body(&body)
	if err != nil {
		err_ := errors.Wrap(err, "pwdlogin: post /oauth/token fail")
		logrus.Error(err_)
		return "", err_
	}
	if !r.IsOK() {
		err_ := fmt.Errorf("pwdlogin: /oauth/token statuscode: %d, body: %v", r.StatusCode(), body.String())
		logrus.Error(err_)
		return "", err_
	}
	var oauthToken OAuthToken
	if err := json.NewDecoder(&body).Decode(&oauthToken); err != nil {
		err_ := errors.Wrap(err, "pwdlogin: decode fail")
		logrus.Error(err_)
		return "", err_
	}
	u.token = oauthToken.AccessToken
	userInfo, err := getInfo(u.token)
	if err != nil {
		return "", err
	}
	u.info = userInfo
	u.state = GotInfo
	if err := u.storeSession(oauthToken.AccessToken); err != nil {
		err_ := errors.Wrap(err, "pwdlogin: storeSession fail")
		logrus.Error(err_)
		return "", err_
	}
	return u.sessionID, nil
}

func getInfo(token string) (UserInfo, error) {
	bearer := "Bearer " + token
	var me bytes.Buffer
	r, err := httpclient.New(httpclient.WithCompleteRedirect()).Get(conf.UCAddr()).Path("/api/oauth/me").Header("Authorization", bearer).Do().Body(&me)
	if err != nil {
		err_ := errors.Wrap(err, "getInfo: /api/oauth/me fail")
		logrus.Error(err_)
		return UserInfo{}, err_
	}
	if !r.IsOK() {
		err_ := fmt.Errorf("getInfo: /api/oauth/me statuscode: %d, body: %v", r.StatusCode(), me.String())
		logrus.Error(err_)
		return UserInfo{}, err_
	}
	var info UserInfo
	logrus.Infof("me: %+v\n", me.String()) // debug print

	if err := json.NewDecoder(&me).Decode(&info); err != nil {
		err_ := errors.Wrap(err, "getInfo: decode fail")
		logrus.Error(err_)
		return UserInfo{}, err_
	}
	return info, nil
}

func (u *User) storeSession(token string) error {
	u.sessionID = genSessionID()
	_, err := u.redisCli.Set(mkSessionKey(u.sessionID), token, 15*24*time.Hour).Result()
	if err != nil {
		err_ := errors.Wrap(err, "storeSession: store redis fail")
		return err_
	}
	return nil
}

func (u *User) Logout(req *http.Request) error {
	c, err := req.Cookie(SessionIDCookieName)
	if err != nil {
		return err
	}
	if _, err := u.redisCli.Del(mkSessionKey(c.Value)).Result(); err != nil {
		return err
	}
	return nil
}

func mkSessionKey(sessionID string) string {
	return "openapi:sessionid:" + sessionID
}

func genSessionID() string {
	return uuid.NewV4().String()
}
