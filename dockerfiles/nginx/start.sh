#!/bin/bash

cp /root/conf.d/*      /etc/nginx/conf.d/
cp -rpf /root/pki      /etc/nginx

# 忽悠信号 
trap "" HUP INT QUIT TSTP

bash -c 'nginx -c /etc/nginx/nginx.conf -g "daemon off;"'
