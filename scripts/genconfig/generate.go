package main

import (
	"io"
	"os"
	"strings"
	"text/template"
)

func boolFromStringTemplate() string {
	return `
func boolFromString(s string) (bool, error) {
	switch strings.ToLower(s) {
	case "true", "t":
		return true, nil
	case "false", "f":
		return false, nil
	}
	return false, errors.New("illegal bool tag value")
}
`
}

func funcTemplate() string {
	return `
func {{.FuncName}}() {{.Type}} {
	{{.LockName}}.Do(func() {
		e := os.Getenv({{.Env}})
		if e == "" {
			e = {{.DefaultValue}}
		}
                {{if .isINT64}}
		i, err := strconv.ParseInt(e, 0, 64)
                if err != nil {
			panic("conf {{.FuncName}} parse failed!")
		}
                {{else if .isINT}}
                i, err := strconv.Atoi(e)
                if err != nil {
			panic("conf {{.FuncName}} parse failed!")
		}
                {{else if .isSTRING}}
                i := e
                {{else if .isBOOL}}
                i, err := boolFromString(e)
                if err != nil {
                        panic("conf {{.FuncName}} parse failed! " + err.Error())
                }
                {{end}}
		C.{{.Name}} = i
	})
	return C.{{.Name}}
}
`
}

func generate(confDecl *ConfDecl) error {
	var buf strings.Builder
	// package
	genPackage(&buf, confDecl.PackageName)
	// imports
	genImports(&buf, imports(confDecl))
	// types
	genTypes(&buf, confDecl)
	// vars
	vars := vars()
	for _, decl := range confDecl.Fields {
		lockName := mkLockName(decl.Name)
		vars = append(vars, [2]string{lockName, "sync.Once"})
	}
	genVars(&buf, vars)
	// funcs
	genHelperFuncs(&buf, confDecl)

	t := template.Must(template.New("func").Parse(funcTemplate()))

	for _, decl := range confDecl.Fields {
		funcName := mkFuncName(decl.Name)
		lockName := mkLockName(decl.Name)
		tp := decl.Type.String()
		defaultValue := decl.Default
		env := mkEnv(decl)
		isINT := false
		isINT64 := false
		isSTRING := false
		isBOOL := false
		switch decl.Type {
		case INT:
			isINT = true
		case INT64:
			isINT64 = true
		case STRING:
			isSTRING = true
		case BOOL:
			isBOOL = true
		}
		if err := t.Execute(&buf, map[string]interface{}{
			"Name":         decl.Name,
			"FuncName":     funcName,
			"LockName":     lockName,
			"Type":         tp,
			"DefaultValue": defaultValue,
			"Env":          env,
			"isINT64":      isINT64,
			"isINT":        isINT,
			"isSTRING":     isSTRING,
			"isBOOL":       isBOOL,
		}); err != nil {
			panic(err)
		}
	}
	f, err := os.OpenFile(outputFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}

	content := buf.String()
	_, err = f.WriteString(content)
	if err != nil {
		panic(err)
	}
	return nil
}

func imports(confDecl *ConfDecl) map[string]struct{} {
	r := map[string]struct{}{"sync": {}, "os": {}}
	for _, decl := range confDecl.Fields {
		if decl.Type == INT || decl.Type == INT64 {
			r["strconv"] = struct{}{}
			continue
		}
		if decl.Type == BOOL {
			r["strings"] = struct{}{}
			r["errors"] = struct{}{}
			continue
		}
	}
	return r
}

func genPackage(buf io.Writer, packageName string) {
	io.WriteString(buf, "package "+packageName+"\n")
}

func genImports(buf io.Writer, imports map[string]struct{}) {
	io.WriteString(buf, "import (\n")
	defer io.WriteString(buf, ")\n")
	for i := range imports {
		io.WriteString(buf, indent(1)+"\""+i+"\"\n")
	}
}

func genTypes(buf io.Writer, confDecl *ConfDecl) {
	io.WriteString(buf, confDecl.RawConfStructDef+"\n")
}

func genVars(buf io.Writer, vars [][2]string) {
	io.WriteString(buf, "var (\n")
	defer io.WriteString(buf, ")\n")
	for _, nameAndType := range vars {
		name := nameAndType[0]
		tp := nameAndType[1]
		io.WriteString(buf, indent(1)+name+" "+tp+"\n")
	}
}

func genHelperFuncs(buf io.Writer, confDecl *ConfDecl) {
	for _, decl := range confDecl.Fields {
		if decl.Type == BOOL {
			io.WriteString(buf, boolFromStringTemplate())
			break
		}
	}
}

func vars() [][2]string {
	return [][2]string{
		{"C", "Conf"},
	}
}

func mkFuncName(name string) string {
	return strings.ToUpper(name[0:1]) + name[1:]
}

func mkLockName(name string) string {
	return mkFuncName(name) + "Lock"
}

func mkEnv(field *Field) string {
	env := field.Env
	if env == "" {
		env = "\"" + mkFuncName(field.Name) + "\""
	}
	return env
}
func indent(n int) string {
	var b strings.Builder
	for i := 0; i < n; i++ {
		b.WriteString("        ")
	}
	return b.String()
}
