package main

import (
	"errors"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"strconv"
	"strings"
	"text/scanner"
)

type Type int

const (
	INT Type = iota
	INT64
	STRING
	BOOL
)

func (t Type) String() string {
	switch t {
	case INT:
		return "int"
	case INT64:
		return "int64"
	case STRING:
		return "string"
	case BOOL:
		return "bool"
	default:
		return "<unknown>"

	}
}
func fromString(s string) Type {
	switch s {
	case "int":
		return INT
	case "int64":
		return INT64
	case "string":
		return STRING
	case "bool":
		return BOOL
	default:
		panic("illegal type")
	}
}

type Field struct {
	Name    string
	Type    Type
	Default string
	Env     string
}

type ConfDecl struct {
	PackageName      string
	RawConfStructDef string
	Fields           []*Field
}

func parse(filePath string) (*ConfDecl, error) {
	fs := token.NewFileSet()
	file, err := parser.ParseFile(fs, filePath, nil, 0)
	if err != nil {
		return nil, err
	}
	confDecl := ConfDecl{}
	confDecl.PackageName = file.Name.Name
	for _, decl := range file.Decls {
		genDecl, ok := decl.(*ast.GenDecl)
		if !ok {
			continue
		}
		if genDecl.Tok != token.TYPE {
			continue
		}
		for _, spec := range genDecl.Specs {
			ts, ok := spec.(*ast.TypeSpec)
			if !ok {
				continue
			}
			if ts.Name.Name != "Conf" {
				continue
			}
			st, ok := ts.Type.(*ast.StructType)
			if !ok {
				continue
			}
			f, _ := os.Open(filePath)
			structLen := genDecl.End() - genDecl.Pos()
			structContent := make([]byte, structLen)
			f.ReadAt(structContent, int64(genDecl.Pos())-1)
			confDecl.RawConfStructDef = string(structContent)

			for _, field := range st.Fields.List {
				tp := fromString(field.Type.(*ast.Ident).Name)
				defaultV := zeroValueString(tp)
				env := ""
				if field.Tag != nil {
					defaultV_, err := parseDefaultValue(field.Tag.Value, tp)
					if err == IllegalTagErr {
						panic(err)
					}
					if err == nil {
						defaultV = defaultV_
					}
					env_, err := parseEnvValue(field.Tag.Value)
					if err == IllegalTagErr {
						panic(err)
					}
					if err == nil {
						env = env_
					}
				}

				confDecl.Fields = append(confDecl.Fields, &Field{
					Name:    field.Names[0].Name,
					Type:    tp,
					Default: defaultV,
					Env:     env,
				})
			}

		}
	}
	return &confDecl, nil
}

var (
	IllegalTagErr error = errors.New("illegal tag value")
	NoDefaultErr  error = errors.New("not found default value")
)

func parseTag(tag string) map[string]string {
	tag = tag[1 : len(tag)-1]
	r := map[string]string{}
	last := ""
	var s scanner.Scanner
	s.Init(strings.NewReader(tag))
	tok := s.Scan()
	for tok != scanner.EOF {
		if tok == scanner.Ident {
			last = s.TokenText()
			r[s.TokenText()] = ""
		}
		if tok == scanner.String {
			r[last] = s.TokenText()
		}
		tok = s.Scan()
	}

	return r
}

func parseEnvValue(tag string) (string, error) {
	kvs := parseTag(tag)
	for k, v := range kvs {
		if k == "env" {
			checkTagValue(v)
			return v, nil
		}
	}
	return "", NoDefaultErr
}

func parseDefaultValue(tag string, tp Type) (string, error) {
	kvs := parseTag(tag)
	for k, v := range kvs {
		if k == "default" {
			checkTagValue(v)
			if tp == BOOL {
				checkBoolValue(v)
			}
			if tp == INT || tp == INT64 {
				checkNumValue(v)
			}
			return v, nil
		}
	}
	return "", NoDefaultErr

}

func checkBoolValue(s string) {
	switch strings.ToLower(stripTagValue(s)) {
	case "true", "t", "false", "f":
		return
	default:
		panic("illegal bool default value")
	}
}

func checkNumValue(s string) {
	_, err := strconv.ParseInt(stripTagValue(s), 0, 64)
	if err != nil {
		panic(err)
	}
}

func stripTagValue(s string) string {
	if len(s) < 2 {
		return s
	}
	if s[0] == '"' && s[len(s)-1] == '"' {
		return s[1 : len(s)-1]
	}
	return s
}
func checkTagValue(s string) {
	if len(s) < 2 || '"' != s[0] {
		panic("illegal tag value, should be like `default:\"value\"`")
	}
}

func zeroValueString(tp Type) string {
	switch tp {
	case INT:
		return "\"0\""
	case INT64:
		return "\"0\""
	case STRING:
		return ""
	}
	return ""
}
