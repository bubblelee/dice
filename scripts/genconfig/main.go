package main

import (
	"flag"
)

var (
	filePath   string
	outputFile string
)

func init() {
	flag.StringVar(&filePath, "file-path", "conf.go", "config file path")
	flag.StringVar(&outputFile, "output-file-path", "conf.go", "generated config file")
	flag.Parse()
}

func main() {
	confDecl, err := parse(filePath)
	if err != nil {
		panic(err)
	}
	generate(confDecl)
}
