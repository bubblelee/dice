# Go parameters
GOCLEAN=go clean
GOTEST=go test
GOPACKAGES=$(shell go list ./... | grep -v /vendor/ | sed 's/^_//')
GitCommit=$(shell git rev-list -1 HEAD)
Built=$(shell date +"%Y-%m-%d %T%z")
GOBUILD=go build -ldflags "-X 'terminus.io/dice/dice/pkg/version.GitCommit=$(GitCommit)' -X 'terminus.io/dice/dice/pkg/version.Built=$(Built)'"

.PHONY: build
build: scheduler parser cronjob-scheduler cmdb eventbox  openapi cli officer soldier

.PHONY: scheduler
scheduler:
	$(GOBUILD) -o bin/scheduler terminus.io/dice/dice/scheduler

.PHONY: cronjob-scheduler
cronjob-scheduler:
	$(GOBUILD) -o bin/cronjob-scheduler terminus.io/dice/dice/cronjob-scheduler/cmd

.PHONY: eventbox
eventbox:
	GOOS=linux GOARCH=amd64	$(GOBUILD) -o bin/eventbox terminus.io/dice/dice/eventbox

.PHONY: eventbox-local
eventbox-local:
	$(GOBUILD) -o bin/eventbox terminus.io/dice/dice/eventbox

.PHONY: cmdb
cmdb:
	$(GOBUILD) -o bin/cmdb terminus.io/dice/dice/cmdb

.PHONY: parser
parser:
	$(GOBUILD) -o bin/parser terminus.io/dice/dice/parser && GOOS=linux GOARCH=amd64 $(GOBUILD) -o bin/parser-linux terminus.io/dice/dice/parser

.PHONY: openapi-local
openapi-local:
	cd openapi/api/generate && go generate
	$(GOBUILD) -o bin/openapi terminus.io/dice/dice/openapi
.PHONY: openapi
openapi:
	cd openapi/api/generate && go generate
	GOOS=linux GOARCH=amd64 $(GOBUILD) -o bin/openapi terminus.io/dice/dice/openapi

# produce bin/dice binary instead bin/cli
.PHONY: cli
cli:
	cd cli/command/generate && go generate
	$(GOBUILD) -o bin/dice terminus.io/dice/dice/cli
.PHONY: cli-linux
cli-linux:
	cd cli/command/generate && go generate
	GOOS=linux GOARCH=amd64	$(GOBUILD) -o bin/dice-linux terminus.io/dice/dice/cli

.PHONY: officer
officer:
	$(GOBUILD) -o bin/officer terminus.io/dice/dice/colony/officer

.PHONY: soldier
soldier:
	$(GOBUILD) -o bin/soldier terminus.io/dice/dice/colony/soldier

.PHONY: imagehub
imagehub:
	$(GOBUILD) -o bin/imagehub terminus.io/dice/dice/imagehub

.PHONY: release-online
release-online:
	docker build -t registry.cn-hangzhou.aliyuncs.com/terminus/${DICE_MODULE}:${DICE_VERSION} -f ${DICE_DOCKERFILE} .
	docker login --username=${TERMINUS_DOCKERHUB_ALIYUN_USERNAME} -p ${TERMINUS_DOCKERHUB_ALIYUN_PASSWORD} registry.cn-hangzhou.aliyuncs.com
	docker push registry.cn-hangzhou.aliyuncs.com/terminus/${DICE_MODULE}:${DICE_VERSION}

.PHONY: clean
clean:
	$(GOCLEAN)
	rm -rf bin/

.PHONY: check
check: fmt vet

.PHONY: fmt
fmt: ## run go fmt
	@echo $@
	@test -z "$$(gofmt -s -l . 2>/dev/null | grep -Fv 'vendor/' | grep -v ".pb.go$$" | tee /dev/stderr)" || \
		(echo "please format Go code with 'gofmt -s -w'" && false)
	@test -z "$$(find . -path ./vendor -prune -o ! -path ./extra -prune -o ! -name timestamp.proto ! -name duration.proto -name '*.proto' -type f -exec grep -Hn -e "^ " {} \; | tee /dev/stderr)" || \
		(echo "please indent proto files with tabs only" && false)
	@test -z "$$(find . -path ./vendor -prune -o ! -path ./extra -prune -o -name '*.proto' -type f -exec grep -Hn "Meta meta = " {} \; | grep -v '(gogoproto.nullable) = false' | tee /dev/stderr)" || \
		(echo "meta fields in proto files must have option (gogoproto.nullable) = false" && false)

.PHONY: lint
lint: ## run go lint
	@echo $@
	@test -z "$$(golint ./... | grep -Fv 'vendor/' | grep -v ".pb.go:" | tee /dev/stderr)"

.PHONY: vet
vet: # run go vet
	@echo $@

.PHONY: unit-test
unit-test:
	@echo $@
	$(GOTEST) -tags default $(GOPACKAGES)

.PHONY: unit-alltest
unit-alltest:
	@echo $@
	$(GOTEST) $(GOPACKAGES)
